Updating the sensorium site
===



- Create a directory to hold the entire project, for example /projects/Sensoduino

		mkdir /public/projects
		cd /public/projects
		git clone https://robvanderveer@bitbucket.org/robvanderveer/simplyduino.git

- Create a softline from your webroot folder to the sensorium site code.
	
		cd /volume1/web
		ln -s /public/projects/simplyduino/software/sensorium .
		
- Sensorium will work from a subdirectory just fine. If you like, you can add a virtual host.
- Make sure to adjust your **.htaccess**.
- To update the website code, all you need to do is execute a `git pull` on the folder (which one doesn't matter, they are soft linked):

		cd /volume1/web/sensorium
		git pull

> At the time of writing, the .htaccess file is still in the repository. Usually this isn't an issue because git will keep your local modifications.
