<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.0.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="FTDI_BASIC">
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="0.635" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<pad name="DTR" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="RXI" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="TXO" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="VCC" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="CTS" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="GND" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="1.1938" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<text x="0.635" y="1.905" size="1.27" layer="51" rot="R90">GRN</text>
<text x="13.335" y="1.905" size="1.27" layer="51" rot="R90">BLK</text>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X02">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X2">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="2.159" y="-4.445" size="1.27" layer="51">+</text>
<text x="-2.921" y="-4.445" size="1.27" layer="51">-</text>
</package>
<package name="1X02_BIG">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
</package>
<package name="JST-2-SMD-VERT">
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="2.54" y="-5.08" size="1.27" layer="25">&gt;Name</text>
<text x="2.24" y="3.48" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="R_SW_TH">
<wire x1="-1.651" y1="19.2532" x2="-1.651" y2="-1.3716" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="-1.3716" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="-1.651" y1="19.2532" x2="13.589" y2="19.2532" width="0.2032" layer="21"/>
<wire x1="13.589" y1="19.2532" x2="13.589" y2="-2.2352" width="0.2032" layer="21"/>
<wire x1="13.589" y1="-2.2352" x2="-1.651" y2="-2.2352" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.6002"/>
<pad name="P$2" x="0" y="16.9926" drill="1.6002"/>
<pad name="P$3" x="12.0904" y="15.494" drill="1.6002"/>
<pad name="P$4" x="12.0904" y="8.4582" drill="1.6002"/>
</package>
<package name="SCREWTERMINAL-5MM-2">
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="MOLEX-1X2_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
</package>
<package name="1X02_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-2-PTH">
<wire x1="-2" y1="0" x2="-2" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.6" x2="-2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.6" x2="2" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="1X02_XTRA_BIG">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
</package>
<package name="1X02_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;H3&gt;JST-2-PTH-KIT&lt;/h3&gt;
2-Pin JST, through-hole connector&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="4" size="0.4064" layer="27">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.127" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.127" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.127" layer="21"/>
</package>
<package name="JST-2-PTH-VERT">
<wire x1="-2.95" y1="-2.25" x2="-2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="2.25" x2="2.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="2.25" x2="2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-2.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-1" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="1" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-1.984" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="2.016" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="0.616" y="0.75" size="1.27" layer="51">+</text>
<text x="-1.384" y="0.75" size="1.27" layer="51">-</text>
</package>
<package name="1X03">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X3">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.413" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.413"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X3_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-PTH">
<wire x1="-3.95" y1="-1.6" x2="-3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="6" x2="3.95" y2="6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="6" x2="3.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="-1.6" x2="-3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-1.6" x2="3.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-1.6" x2="-3.3" y2="0" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.6" x2="3.3" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="0" drill="0.7" diameter="1.6256"/>
<pad name="2" x="0" y="0" drill="0.7" diameter="1.6256"/>
<pad name="3" x="2" y="0" drill="0.7" diameter="1.6256"/>
<text x="-1.27" y="5.24" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="3.97" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.67" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.67" size="1.27" layer="51">-</text>
<text x="1.7" y="0.87" size="0.8" layer="51">S</text>
</package>
<package name="1X03_PP_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.9" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
</package>
<package name="SCREWTERMINAL-5MM-3">
<wire x1="-3.1" y1="4.2" x2="13.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="4.2" x2="13.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="13.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="13.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="13.1" y1="4" x2="13.7" y2="4" width="0.2032" layer="51"/>
<wire x1="13.7" y1="4" x2="13.7" y2="3" width="0.2032" layer="51"/>
<wire x1="13.7" y1="3" x2="13.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.413" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.413"/>
<pad name="3" x="10" y="0" drill="1.3" diameter="2.413"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK_NO_SILK">
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-SMD">
<wire x1="-4.99" y1="-2.07" x2="-4.99" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.99" y1="-5.57" x2="-4.19" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-5.57" x2="-4.19" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="-4.19" y1="-3.07" x2="-2.99" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="-3.07" x2="4.21" y2="-3.07" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-3.07" x2="4.21" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="4.21" y1="-5.57" x2="5.01" y2="-5.57" width="0.2032" layer="21"/>
<wire x1="5.01" y1="-5.57" x2="5.01" y2="-2.07" width="0.2032" layer="21"/>
<wire x1="3.01" y1="1.93" x2="-2.99" y2="1.93" width="0.2032" layer="21"/>
<smd name="1" x="-1.99" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="2.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-4.39" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="4.41" y="0.43" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0.01" y="-4.77" dx="1" dy="4.6" layer="1"/>
<text x="-2.26" y="0.2" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.26" y="-1.07" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="1X03-1MM-RA">
<wire x1="-1" y1="-4.6" x2="1" y2="-4.6" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-0.35" width="0.254" layer="21"/>
<wire x1="1.75" y1="-0.35" x2="2.4997" y2="-0.35" width="0.254" layer="21"/>
<wire x1="2.4997" y1="-0.35" x2="2.4997" y2="-2" width="0.254" layer="21"/>
<wire x1="-2.5" y1="-0.35" x2="-1.75" y2="-0.35" width="0.254" layer="21"/>
<circle x="-2" y="0.3" radius="0.1414" width="0.4" layer="21"/>
<smd name="NC2" x="-2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="NC1" x="2.3" y="-3.675" dx="1.2" dy="2" layer="1"/>
<smd name="1" x="-1" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="2" x="0" y="0" dx="0.6" dy="1.35" layer="1"/>
<smd name="3" x="1" y="0" dx="0.6" dy="1.35" layer="1"/>
<text x="-1.73" y="1.73" size="0.4064" layer="25" rot="R180">&gt;NAME</text>
<text x="3.46" y="1.73" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
</package>
<package name="1X03_SMD_RA_FEMALE">
<wire x1="-3.935" y1="4.25" x2="-3.935" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="4.25" x2="-3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="3.935" y1="-4.25" x2="3.935" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-3.935" y1="-4.25" x2="3.935" y2="-4.25" width="0.1778" layer="21"/>
<rectangle x1="-0.32" y1="6.8" x2="0.32" y2="7.65" layer="51"/>
<rectangle x1="2.22" y1="6.8" x2="2.86" y2="7.65" layer="51"/>
<rectangle x1="-2.86" y1="6.8" x2="-2.22" y2="7.65" layer="51"/>
<smd name="3" x="2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="2" x="0" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<smd name="1" x="-2.54" y="7.225" dx="1.25" dy="3" layer="1" rot="R180"/>
<text x="-3.155" y="2.775" size="1" layer="27">&gt;Value</text>
<text x="-2.955" y="-3.395" size="1" layer="25">&gt;Name</text>
</package>
<package name="1X03_SMD_RA_MALE">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="2" x="0" y="5" dx="3" dy="1" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5" dx="3" dy="1" layer="1" rot="R90"/>
</package>
<package name="1X03_SMD_RA_MALE_POST">
<description>&lt;h3&gt;SMD 3-Pin Male Right-Angle Header w/ Alignment posts&lt;/h3&gt;

Matches 4UCONN part # 11026&lt;br&gt;
&lt;a href="http://www.4uconnector.com/online/object/4udrawing/11026.pdf"&gt;http://www.4uconnector.com/online/object/4udrawing/11026.pdf&lt;/a&gt;</description>
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.6"/>
<hole x="1.27" y="0" drill="1.6"/>
</package>
<package name="JST-3-PTH-VERT">
<description>This 3-pin connector mates with the JST cable sold on SparkFun.</description>
<wire x1="-3.95" y1="-2.25" x2="-3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-3.95" y1="2.25" x2="3.95" y2="2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="2.25" x2="3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="3.95" y1="-2.25" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-2.25" x2="-3.95" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="1" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.75" x2="1" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1" y1="-1.75" x2="-1" y2="-2.25" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="2" x="0" y="-0.55" drill="0.7" diameter="1.6256"/>
<pad name="3" x="2" y="-0.55" drill="0.7" diameter="1.6256"/>
<text x="-3" y="3" size="0.4064" layer="25">&gt;Name</text>
<text x="1" y="3" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="0.75" size="1.27" layer="51">+</text>
<text x="-0.4" y="0.75" size="1.27" layer="51">-</text>
<text x="1.7" y="0.95" size="0.8" layer="51">S</text>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLER">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3589"/>
<hole x="1.27" y="0" drill="1.3589"/>
</package>
<package name="1X03_SMD_RA_MALE_POST_SMALLEST">
<wire x1="3.81" y1="1.25" x2="-3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="-3.81" y1="1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="2.53" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="-0.01" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-2.55" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-3.81" y2="-1.25" width="0.1778" layer="51"/>
<wire x1="3.81" y1="-1.25" x2="3.81" y2="1.25" width="0.1778" layer="51"/>
<wire x1="2.53" y1="-1.25" x2="2.53" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-0.01" y1="-1.25" x2="-0.01" y2="-7.25" width="0.127" layer="51"/>
<wire x1="-2.55" y1="-1.25" x2="-2.55" y2="-7.25" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="4.15" x2="0.32" y2="5.95" layer="51"/>
<rectangle x1="-2.86" y1="4.15" x2="-2.22" y2="5.95" layer="51"/>
<rectangle x1="2.22" y1="4.15" x2="2.86" y2="5.95" layer="51"/>
<smd name="1" x="-2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="2" x="0" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="5.07" dx="2.5" dy="1.27" layer="1" rot="R90"/>
<hole x="-1.27" y="0" drill="1.3462"/>
<hole x="1.27" y="0" drill="1.3462"/>
</package>
<package name="1X08">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_LOCK">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_LOCK_LONGPADS">
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<wire x1="16.764" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="19.05" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.9906" x2="18.7706" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="19.05" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.9906" x2="18.7706" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="18.796" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.905" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
<rectangle x1="17.4879" y1="-0.2921" x2="18.0721" y2="0.2921" layer="51"/>
</package>
<package name="1X08_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-8">
<wire x1="-2.3" y1="3.4" x2="26.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="26.76" y1="3.4" x2="26.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-2.8" x2="26.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="26.76" y1="3.15" x2="27.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="27.16" y1="3.15" x2="27.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="27.16" y1="2.15" x2="26.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="7" x="21" y="0" drill="1.2" diameter="2.032"/>
<pad name="8" x="24.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD">
<wire x1="1.37" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="-1.25" x2="-19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="1.25" x2="-15.963" y2="1.25" width="0.127" layer="21"/>
<wire x1="-18.63" y1="-1.25" x2="-19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.537" y1="1.25" x2="-10.863" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-13.489" y1="-1.25" x2="-16.991" y2="-1.25" width="0.127" layer="21"/>
<smd name="7" x="-15.24" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="8" x="-17.78" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-19.05" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-19.05" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_ALT">
<wire x1="1.37" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-18.63" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="-1.25" x2="-15.963" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-13.403" y1="1.25" x2="-17.077" y2="1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.451" y1="-1.25" x2="-10.949" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="7" x="-15.24" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="8" x="-17.78" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-19.05" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-19.05" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_COMBINED">
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="19.15" y1="1.25" x2="19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-1.25" x2="-1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="19.15" y1="-1.25" x2="18.503" y2="-1.25" width="0.127" layer="21"/>
<wire x1="18.63" y1="1.25" x2="19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.723" y2="1.25" width="0.127" layer="21"/>
<wire x1="14.537" y1="-1.25" x2="13.403" y2="-1.25" width="0.127" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.127" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.127" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.127" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.127" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.127" layer="21"/>
<wire x1="13.489" y1="1.25" x2="14.451" y2="1.25" width="0.127" layer="21"/>
<wire x1="16.029" y1="1.25" x2="16.991" y2="1.25" width="0.127" layer="21"/>
<wire x1="17.077" y1="-1.25" x2="15.943" y2="-1.25" width="0.127" layer="21"/>
<wire x1="11.997" y1="-1.25" x2="10.863" y2="-1.25" width="0.127" layer="21"/>
<wire x1="9.457" y1="-1.25" x2="8.323" y2="-1.25" width="0.127" layer="21"/>
<wire x1="6.917" y1="-1.25" x2="5.783" y2="-1.25" width="0.127" layer="21"/>
<wire x1="4.377" y1="-1.25" x2="3.243" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.837" y1="-1.25" x2="0.703" y2="-1.25" width="0.127" layer="21"/>
<wire x1="17.78" y1="1.27" x2="17.78" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<smd name="7@2" x="15.24" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="5@2" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3@2" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="8@2" x="17.78" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6@2" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4@2" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2@2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="7" x="15.24" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="8" x="17.78" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1@2" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BM08B-SRSS-TB">
<description>JST crimp connector: 1mm pitch, top entry</description>
<wire x1="-5" y1="3.3" x2="5" y2="3.3" width="0.127" layer="51"/>
<wire x1="-5" y1="0.4" x2="-5" y2="3.3" width="0.127" layer="51"/>
<wire x1="5" y1="0.4" x2="5" y2="3.3" width="0.127" layer="51"/>
<wire x1="-5" y1="0.4" x2="5" y2="0.4" width="0.127" layer="51"/>
<wire x1="-4.1" y1="0.35" x2="-5.05" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="0.35" x2="-5.05" y2="1.35" width="0.2032" layer="21"/>
<wire x1="5.05" y1="0.35" x2="4.15" y2="0.35" width="0.2032" layer="21"/>
<wire x1="5.05" y1="0.35" x2="5.05" y2="1.35" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="3.4" x2="3.9" y2="3.4" width="0.2032" layer="21"/>
<circle x="-4.4" y="-0.35" radius="0.1118" width="0.4064" layer="21"/>
<smd name="1" x="-3.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="2" x="-2.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="3" x="-1.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="4" x="-0.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="5" x="0.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="6" x="1.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="7" x="2.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="8" x="3.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="P$9" x="4.8" y="2.525" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$10" x="-4.8" y="2.525" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<text x="-3.8" y="2.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.8" y="1.3" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="19.05" y2="-1.25" width="0.127" layer="51"/>
<wire x1="19.05" y1="-1.25" x2="19.05" y2="1.25" width="0.127" layer="51"/>
<wire x1="19.05" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="-1.25" x2="19.05" y2="1.25" width="0.1778" layer="21"/>
<circle x="15.24" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="17.78" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="14.92" y1="0" x2="15.56" y2="2.75" layer="51"/>
<rectangle x1="17.46" y1="-2.75" x2="18.1" y2="0" layer="51" rot="R180"/>
<smd name="7" x="15.24" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="8" x="17.78" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="16.002" y1="1.25" x2="17.018" y2="1.25" width="0.1778" layer="21"/>
<wire x1="17.018" y1="-1.25" x2="16.002" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="-1.25" x2="18.415" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="1.25" x2="18.415" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.462" y1="1.25" x2="14.478" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.478" y1="-1.25" x2="13.462" y2="-1.25" width="0.1778" layer="21"/>
</package>
<package name="1X08_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_PIN1_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="2X4">
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.8796" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="M06">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M03">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M08">
<wire x1="1.27" y1="-10.16" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="M04X2">
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-4.572" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-4.064" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FTDI_BASIC" prefix="JP">
<description>FTDI Basic header with labels</description>
<gates>
<gate name="G$1" symbol="M06" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="FTDI_BASIC">
<connects>
<connect gate="G$1" pin="1" pad="DTR"/>
<connect gate="G$1" pin="2" pad="RXI"/>
<connect gate="G$1" pin="3" pad="TXO"/>
<connect gate="G$1" pin="4" pad="VCC"/>
<connect gate="G$1" pin="5" pad="CTS"/>
<connect gate="G$1" pin="6" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M02" prefix="JP">
<description>Standard 2-pin 0.1" header. Use with &lt;br&gt;
- straight break away headers ( PRT-00116)&lt;br&gt;
- right angle break away headers (PRT-00553)&lt;br&gt;
- swiss pins (PRT-00743)&lt;br&gt;
- machine pins (PRT-00117)&lt;br&gt;
- female headers (PRT-00115)&lt;br&gt;&lt;br&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08433</description>
<gates>
<gate name="G$1" symbol="M02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08352"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROCKER" package="R_SW_TH">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-2-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 3&lt;/b&gt;
Standard 3-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="G$1" symbol="M03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="SKU" value="PRT-09915" constant="no"/>
</technology>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X03_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SCREW-5MM" package="SCREWTERMINAL-5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X03_LOCK_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-SMD" package="JST-3-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X03-1MM-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD_RA_FEMALE" package="1X03_SMD_RA_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10926"/>
<attribute name="VALUE" value="1x3 RA Female .1&quot;"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE" package="1X03_SMD_RA_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10925"/>
</technology>
</technologies>
</device>
<device name="SMD_RA_MALE_POST" package="1X03_SMD_RA_MALE_POST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-VERT" package="JST-3-PTH-VERT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLER" package="1X03_SMD_RA_MALE_POST_SMALLER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_SMD_RA_MALE_POST_SMALLEST" package="1X03_SMD_RA_MALE_POST_SMALLEST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M08" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 8&lt;/b&gt;
Standard 8-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).</description>
<gates>
<gate name="G$1" symbol="M08" x="-2.54" y="0"/>
</gates>
<devices>
<device name="1X08" package="1X08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X08_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X08_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X08_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-8" package="SCREWTERMINAL-3.5MM-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT" package="1X08_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT" package="1X08_SMD_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-COMBO" package="1X08_SMD_COMBINED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BM08B-SRSS-TB" package="BM08B-SRSS-TB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-MALE" package="1X08_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11292"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="1X08_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X08_PIN1_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M04X2">
<description>.1" header, two rows of four.</description>
<gates>
<gate name="G$1" symbol="M04X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VIN">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VIN" prefix="SUPPLY">
<description>Vin supply symbol</description>
<gates>
<gate name="G$1" symbol="VIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DigitalIC">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find all manner of digital ICs- microcontrollers, memory chips, logic chips, FPGAs, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TQFP32-08">
<description>&lt;B&gt;Thin Plasic Quad Flat Package&lt;/B&gt; Grid 0.8 mm</description>
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<circle x="-2.7432" y="2.7432" radius="0.3592" width="0.1524" layer="21"/>
<smd name="1" x="-4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="8" x="-4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="9" x="-2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="10" x="-2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="15" x="2" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="16" x="2.8" y="-4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="17" x="4.2926" y="-2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.5588" layer="1"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.5588" layer="1"/>
<smd name="24" x="4.2926" y="2.8" dx="1.27" dy="0.5588" layer="1"/>
<smd name="25" x="2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="26" x="2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="27" x="1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="28" x="0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="31" x="-2" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<smd name="32" x="-2.8" y="4.2926" dx="0.5588" dy="1.27" layer="1"/>
<text x="-3.175" y="5.08" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGAXX8-32PIN">
<description>Symbol for Atmega328/168/88/48 chips, 32-pin version</description>
<wire x1="-17.78" y1="27.94" x2="20.32" y2="27.94" width="0.254" layer="94"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="-35.56" width="0.254" layer="94"/>
<wire x1="20.32" y1="-35.56" x2="-17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="-17.78" y2="27.94" width="0.254" layer="94"/>
<text x="-17.78" y="-38.1" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="28.448" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PB5(SCK)" x="25.4" y="-33.02" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2)" x="-22.86" y="-5.08" length="middle"/>
<pin name="PB6(XTAL1/TOSC1)" x="-22.86" y="0" length="middle"/>
<pin name="GND@1" x="-22.86" y="-27.94" length="middle"/>
<pin name="GND@2" x="-22.86" y="-30.48" length="middle"/>
<pin name="VCC@1" x="-22.86" y="17.78" length="middle"/>
<pin name="VCC@2" x="-22.86" y="15.24" length="middle"/>
<pin name="AGND" x="-22.86" y="-25.4" length="middle"/>
<pin name="AREF" x="-22.86" y="10.16" length="middle"/>
<pin name="AVCC" x="-22.86" y="20.32" length="middle"/>
<pin name="PB4(MISO)" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2)" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B)" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="25.4" y="-22.86" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0)" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PD2(INT0)" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="ADC7" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="ADC6" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL)" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA)" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PC1(ADC1)" x="25.4" y="22.86" length="middle" rot="R180"/>
<pin name="PC0(ADC0)" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="PC6(/RESET)" x="-22.86" y="25.4" length="middle" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA328_SMT" prefix="U" uservalue="yes">
<description>32-Pin Atmega328 part&lt;BR&gt;
Commonly used on Arduino compatible boards&lt;br&gt;
Available in QFP and QFN packages&lt;br&gt;
TQFP is IC-09069
QFN is non-stock</description>
<gates>
<gate name="G$1" symbol="ATMEGAXX8-32PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP32-08">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AGND" pad="21"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND@1" pad="3"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="PB0(ICP)" pad="12"/>
<connect gate="G$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="G$1" pin="PB2(SS/OC1B)" pad="14"/>
<connect gate="G$1" pin="PB3(MOSI/OC2)" pad="15"/>
<connect gate="G$1" pin="PB4(MISO)" pad="16"/>
<connect gate="G$1" pin="PB5(SCK)" pad="17"/>
<connect gate="G$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="G$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="G$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="G$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="G$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="G$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="G$1" pin="PC4(ADC4/SDA)" pad="27"/>
<connect gate="G$1" pin="PC5(ADC5/SCL)" pad="28"/>
<connect gate="G$1" pin="PC6(/RESET)" pad="29"/>
<connect gate="G$1" pin="PD0(RXD)" pad="30"/>
<connect gate="G$1" pin="PD1(TXD)" pad="31"/>
<connect gate="G$1" pin="PD2(INT0)" pad="32"/>
<connect gate="G$1" pin="PD3(INT1)" pad="1"/>
<connect gate="G$1" pin="PD4(XCK/T0)" pad="2"/>
<connect gate="G$1" pin="PD5(T1)" pad="9"/>
<connect gate="G$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="G$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="G$1" pin="VCC@1" pad="4"/>
<connect gate="G$1" pin="VCC@2" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="IC-09069" constant="no"/>
<attribute name="VALUE" value="ATMEGA328P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="EIA3528">
<wire x1="-0.9" y1="-1.6" x2="-2.6" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-1.6" x2="-2.6" y2="1.55" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="1.55" x2="-0.9" y2="1.55" width="0.2032" layer="21"/>
<wire x1="1" y1="-1.55" x2="2.2" y2="-1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-1.55" x2="2.6" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.2" x2="2.6" y2="1.25" width="0.2032" layer="21"/>
<wire x1="2.6" y1="1.25" x2="2.2" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.55" x2="1" y2="1.55" width="0.2032" layer="21"/>
<wire x1="0.609" y1="1.311" x2="0.609" y2="-1.286" width="0.2032" layer="21" style="longdash"/>
<smd name="C" x="-1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<smd name="A" x="1.65" y="0" dx="2.5" dy="1.2" layer="1" rot="R90"/>
<text x="-2.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.24" y="-1.37" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="CAP_POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.1UF-25V(+80/-20%)(0603)" prefix="C" uservalue="yes">
<description>CAP-00810&lt;br&gt;
Ceramic&lt;br&gt;
Standard decoupling cap</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="47UF-10V-10%(TANT)" prefix="C" uservalue="yes">
<description>CAP-08310 &lt;BR&gt;
47uF Tantalum SMT&lt;br&gt;
 10V 10% (EIA-3528)</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EIA3528">
<connects>
<connect gate="G$1" pin="+" pad="A"/>
<connect gate="G$1" pin="-" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08310"/>
<attribute name="VALUE" value="47uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762" diameter="1.6764"/>
<pad name="2" x="2.5" y="0" drill="0.762" diameter="1.6764"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="AXIAL-0.1EZ">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="0" y="0" radius="1.016" width="0" layer="30"/>
<circle x="2.54" y="0" radius="1.016" width="0" layer="30"/>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.1">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT-KIT" package="AXIAL-0.1EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Electromechanical">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find anything that moves- switches, relays, buttons, potentiometers. Also, anything that goes on a board but isn't electrical in nature- screws, standoffs, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TACTILE-PTH">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="KSA_SEALED_TAC_SWITCH">
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.127" layer="21"/>
<wire x1="-5.08" y1="3.81" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.81" x2="-5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.81" x2="-5.08" y2="3.81" width="0.127" layer="21"/>
<pad name="P$1" x="-3.81" y="2.54" drill="1" shape="square"/>
<pad name="P$2" x="3.81" y="2.54" drill="1" shape="square"/>
<pad name="P$3" x="-3.81" y="-2.54" drill="1" shape="square"/>
<pad name="P$4" x="3.81" y="-2.54" drill="1" shape="square"/>
</package>
<package name="TACTILE-SWITCH-SMD">
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="BATTERY_20MM_PTH_COMPACT">
<wire x1="-3.7" y1="-9.9" x2="3.7" y2="-9.9" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-9.9" x2="-10.5" y2="-5.7" width="0.2032" layer="21"/>
<wire x1="3.7" y1="-9.9" x2="10.5" y2="-5.7" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="7.3" x2="5.4" y2="7.4" width="0.2032" layer="21" curve="94.031579"/>
<wire x1="10.5" y1="-5.7" x2="10.5" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="-5.7" x2="-10.5" y2="-3.5" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="7.3" x2="-10.5" y2="5.3" width="0.2032" layer="21" curve="139.635474"/>
<wire x1="10.5" y1="5.3" x2="5.3" y2="7.3" width="0.2032" layer="21" curve="137.002565"/>
<wire x1="-10.5" y1="5.3" x2="-10.5" y2="3.5" width="0.2032" layer="21"/>
<wire x1="10.5" y1="5.3" x2="10.5" y2="3.5" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="10" width="0.2032" layer="51"/>
<pad name="1" x="-10.5" y="0" drill="1.8542" shape="long" rot="R90"/>
<pad name="3" x="10.5" y="0" drill="1.8542" shape="long" rot="R90"/>
<smd name="2" x="0" y="0" dx="4.064" dy="4.064" layer="1"/>
<text x="-6.985" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-6.985" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="BATTERY-AAA">
<wire x1="-13.97" y1="3.81" x2="-13.97" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-13.97" y1="-3.81" x2="-23.495" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-23.495" y1="-3.81" x2="-23.495" y2="3.81" width="0.127" layer="51"/>
<wire x1="-23.495" y1="3.81" x2="-13.97" y2="3.81" width="0.127" layer="51"/>
<wire x1="23.368" y1="3.81" x2="13.97" y2="3.81" width="0.127" layer="51"/>
<wire x1="13.97" y1="3.81" x2="13.97" y2="-3.81" width="0.127" layer="51"/>
<wire x1="13.97" y1="-3.81" x2="23.368" y2="-3.81" width="0.127" layer="51"/>
<wire x1="23.368" y1="-3.81" x2="23.368" y2="3.81" width="0.127" layer="51"/>
<wire x1="-12.7" y1="3.81" x2="12.7" y2="3.81" width="0.127" layer="51"/>
<wire x1="12.7" y1="-3.81" x2="-12.7" y2="-3.81" width="0.127" layer="51"/>
<pad name="PWR@2" x="-13.97" y="0" drill="1.7018"/>
<pad name="PWR@1" x="-21.59" y="0" drill="1.7018"/>
<pad name="GND@2" x="13.97" y="0" drill="1.7018"/>
<pad name="GND@1" x="21.59" y="0" drill="1.7018"/>
<text x="-11.43" y="-1.27" size="2.54" layer="21" ratio="12">+</text>
<text x="8.89" y="-1.27" size="2.54" layer="21" ratio="12">-</text>
</package>
<package name="BATTERY">
<description>&lt;B&gt;BATTERY&lt;/B&gt;&lt;p&gt;
22 mm</description>
<wire x1="0.635" y1="2.54" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-3.175" x2="2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="1.905" y2="-3.81" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="11.43" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="10.2362" width="0.1524" layer="21"/>
<pad name="-" x="-5.715" y="0" drill="1.016" shape="long"/>
<pad name="+" x="9.525" y="-5.08" drill="1.016" shape="long"/>
<pad name="+@1" x="9.525" y="5.08" drill="1.016" shape="long"/>
<text x="-4.1656" y="6.35" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="3.81" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.635" y1="-1.27" x2="0" y2="1.27" layer="21"/>
</package>
<package name="BATTERY-AA">
<wire x1="-17.526" y1="-4.826" x2="-27.051" y2="-4.826" width="0.254" layer="41"/>
<wire x1="-27.051" y1="-4.826" x2="-27.051" y2="4.826" width="0.254" layer="41"/>
<wire x1="-27.051" y1="4.826" x2="-17.526" y2="4.826" width="0.254" layer="41"/>
<wire x1="27.051" y1="4.826" x2="17.526" y2="4.826" width="0.254" layer="21"/>
<wire x1="17.526" y1="-4.826" x2="27.051" y2="-4.826" width="0.254" layer="21"/>
<wire x1="27.051" y1="-4.826" x2="27.051" y2="4.826" width="0.254" layer="21"/>
<wire x1="-26.67" y1="7.62" x2="26.67" y2="7.62" width="0.127" layer="51"/>
<wire x1="26.67" y1="-7.62" x2="-26.67" y2="-7.62" width="0.127" layer="51"/>
<wire x1="-17.526" y1="4.826" x2="-17.526" y2="-4.826" width="0.254" layer="41"/>
<wire x1="17.526" y1="4.826" x2="17.526" y2="2.159" width="0.254" layer="21"/>
<wire x1="17.526" y1="-2.159" x2="17.526" y2="-4.826" width="0.254" layer="21"/>
<wire x1="-17.526" y1="-4.826" x2="-27.051" y2="-4.826" width="0.254" layer="21"/>
<wire x1="-27.051" y1="-4.826" x2="-27.051" y2="4.826" width="0.254" layer="21"/>
<wire x1="-27.051" y1="4.826" x2="-17.526" y2="4.826" width="0.254" layer="21"/>
<wire x1="-17.526" y1="4.826" x2="-17.526" y2="2.159" width="0.254" layer="21"/>
<wire x1="-17.526" y1="-2.159" x2="-17.526" y2="-4.826" width="0.254" layer="21"/>
<wire x1="17.526" y1="4.826" x2="27.051" y2="4.826" width="0.254" layer="41"/>
<wire x1="27.051" y1="4.826" x2="27.051" y2="-4.826" width="0.254" layer="41"/>
<wire x1="27.051" y1="-4.826" x2="17.526" y2="-4.826" width="0.254" layer="41"/>
<wire x1="17.526" y1="-4.826" x2="17.526" y2="4.826" width="0.254" layer="41"/>
<pad name="PWR@2" x="-17.78" y="0" drill="1.8542" rot="R90"/>
<pad name="PWR@1" x="-25.146" y="0" drill="1.8542"/>
<pad name="GND@2" x="17.78" y="0" drill="1.8542"/>
<pad name="GND@1" x="25.146" y="0" drill="1.8542"/>
<text x="-14.732" y="-1.27" size="2.54" layer="21" ratio="12">+</text>
<text x="12.7" y="-1.27" size="2.54" layer="21" ratio="12">-</text>
</package>
<package name="BATTCON_12MM_PTH">
<wire x1="-6.35" y1="-3.81" x2="-3.81" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-6.35" x2="3.81" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-6.35" x2="6.35" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-3.81" x2="6.35" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="-6.35" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="4.064" width="0.2032" layer="21"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="4.064" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="5.588" x2="3.175" y2="5.588" width="0.2032" layer="21" curve="102.56384"/>
<wire x1="-6.35" y1="4.064" x2="-3.175" y2="5.588" width="0.2032" layer="21" curve="-123.398919"/>
<wire x1="6.35" y1="4.064" x2="3.175" y2="5.588" width="0.2032" layer="21" curve="128.77667"/>
<pad name="VCC@2" x="-6.604" y="0" drill="1.8542" shape="square"/>
<pad name="VCC@1" x="6.604" y="0" drill="1.8542" shape="square"/>
<smd name="GND" x="0" y="0" dx="9" dy="9" layer="1" roundness="100" cream="no"/>
<text x="-3.81" y="-3.81" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="0.4064" layer="27">&gt;VALUE</text>
<circle x="0" y="0" radius="1" width="2" layer="31"/>
</package>
<package name="BATTCON_20MM">
<wire x1="-3.7" y1="-10.57" x2="3.7" y2="-10.57" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-10.57" x2="-10.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.7" y1="-10.57" x2="10.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="8.3" x2="5.3" y2="8.3" width="0.2032" layer="21" curve="69.999889"/>
<wire x1="10.5" y1="-4.5" x2="10.5" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="-4.5" x2="-10.5" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="6.3" x2="-5.3" y2="8.3" width="0.2032" layer="21" curve="-139.635474"/>
<wire x1="10.5" y1="6.3" x2="5.3" y2="8.3" width="0.2032" layer="21" curve="136.99875"/>
<wire x1="-10.5" y1="6.3" x2="-10.5" y2="3.4" width="0.2032" layer="21"/>
<wire x1="10.5" y1="6.3" x2="10.5" y2="3.4" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="10" width="0.2032" layer="51"/>
<smd name="2" x="0" y="0" dx="6.07" dy="6.07" layer="1"/>
<smd name="1" x="-12.5" y="0" dx="6.07" dy="6.07" layer="1"/>
<smd name="3" x="12.5" y="0" dx="6.07" dy="6.07" layer="1"/>
<text x="-6.985" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-6.985" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="BATTCON_245MM">
<wire x1="-6.24" y1="-12.44" x2="6.24" y2="-12.44" width="0.2032" layer="21"/>
<wire x1="-6.24" y1="-12.44" x2="-13.04" y2="-8.24" width="0.2032" layer="21"/>
<wire x1="6.24" y1="-12.44" x2="13.04" y2="-8.24" width="0.2032" layer="21"/>
<wire x1="-7.84" y1="12.38" x2="7.94" y2="12.48" width="0.2032" layer="21" curve="94.032201"/>
<wire x1="13.04" y1="-8.24" x2="13.04" y2="-3" width="0.2032" layer="21"/>
<wire x1="-13.04" y1="-8.24" x2="-13.04" y2="-3" width="0.2032" layer="21"/>
<wire x1="-13.04" y1="10.38" x2="-7.84" y2="12.38" width="0.2032" layer="21" curve="-139.635474"/>
<wire x1="13.04" y1="10.38" x2="7.84" y2="12.38" width="0.2032" layer="21" curve="136.99875"/>
<wire x1="-13.04" y1="10.38" x2="-13.04" y2="3" width="0.2032" layer="21"/>
<wire x1="13.04" y1="10.38" x2="13.04" y2="3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="12.25" width="0.2032" layer="51"/>
<smd name="2" x="0" y="0" dx="4.064" dy="4.064" layer="1"/>
<smd name="1" x="-15.367" y="0" dx="5.08" dy="5.08" layer="1"/>
<smd name="3" x="15.367" y="0" dx="5.08" dy="5.08" layer="1"/>
<text x="-9.525" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-9.525" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="BATTCOM_20MM_PTH">
<wire x1="10.34" y1="3.8" x2="13.32" y2="3.8" width="0.2032" layer="21"/>
<wire x1="13.32" y1="3.8" x2="13.32" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="13.32" y1="-3.8" x2="10.34" y2="-3.8" width="0.2032" layer="21"/>
<circle x="0.06" y="0.1" radius="10" width="0.2032" layer="51"/>
<pad name="2" x="-8.15" y="0" drill="1.3" rot="R90"/>
<pad name="1" x="11.85" y="0" drill="1.3" rot="R90"/>
<text x="-15.985" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-15.985" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="8.6" y="-0.7" size="1.27" layer="51">+</text>
<text x="-6.4" y="-0.7" size="1.27" layer="51">-</text>
<wire x1="-10.54" y1="3.8" x2="-10.54" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="10.34" y1="3.8" x2="-10.54" y2="3.8" width="0.2032" layer="21" curve="139.856795"/>
<wire x1="10.34" y1="-3.8" x2="-10.54" y2="-3.8" width="0.2032" layer="21" curve="-139.856795"/>
</package>
<package name="BATTCON_245MM_PTH">
<wire x1="-3.81" y1="-12.7" x2="3.81" y2="-12.7" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-12.7" x2="-12.7" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-12.7" x2="12.7" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="9.779" x2="7.62" y2="9.779" width="0.2032" layer="21" curve="63.785901"/>
<wire x1="12.7" y1="-6.35" x2="12.7" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-12.7" y1="-6.35" x2="-12.7" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-12.7" y1="6.35" x2="-7.62" y2="9.779" width="0.2032" layer="21" curve="-123.780121"/>
<wire x1="12.7" y1="6.35" x2="7.62" y2="9.779" width="0.2032" layer="21" curve="123.773101"/>
<wire x1="-12.7" y1="6.35" x2="-12.7" y2="2.54" width="0.2032" layer="21"/>
<wire x1="12.7" y1="6.35" x2="12.7" y2="2.54" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="12.25" width="0.2032" layer="51"/>
<pad name="1" x="-12.7" y="0" drill="1.8542" shape="square"/>
<pad name="3" x="12.7" y="0" drill="1.8542" shape="square"/>
<smd name="2" x="0" y="0" dx="4.064" dy="4.064" layer="1"/>
<text x="-9.525" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-9.525" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LIPO-1100MAH">
<wire x1="-17" y1="-26" x2="17" y2="-26" width="0.2032" layer="51"/>
<wire x1="17" y1="-26" x2="17" y2="26" width="0.2032" layer="51"/>
<wire x1="17" y1="26" x2="-17" y2="26" width="0.2032" layer="51"/>
<wire x1="-17" y1="26" x2="-17" y2="-26" width="0.2032" layer="51"/>
<wire x1="-1" y1="34" x2="-1" y2="30" width="0.127" layer="51"/>
<wire x1="1" y1="34" x2="1" y2="30" width="0.127" layer="51"/>
<pad name="+" x="-5" y="32" drill="1.397" diameter="2.54"/>
<pad name="-" x="5" y="32" drill="1.397" diameter="2.54"/>
</package>
<package name="BATTERY-AA-PANEL">
<wire x1="29.46" y1="8.255" x2="-29.46" y2="8.255" width="0.127" layer="51"/>
<wire x1="-29.46" y1="8.255" x2="-29.46" y2="-8.255" width="0.127" layer="51"/>
<wire x1="-29.46" y1="-8.255" x2="29.46" y2="-8.255" width="0.127" layer="51"/>
<wire x1="29.46" y1="-8.255" x2="29.46" y2="8.255" width="0.127" layer="51"/>
<wire x1="24" y1="1" x2="24" y2="-1" width="0.25" layer="21"/>
<wire x1="23" y1="0" x2="25" y2="0" width="0.25" layer="21"/>
<wire x1="-25" y1="1" x2="-25" y2="-1" width="0.25" layer="21"/>
<pad name="-" x="-27.43" y="0" drill="1.17" diameter="2.3"/>
<pad name="+" x="27.43" y="0" drill="1.17" diameter="2.3"/>
<text x="-29.5" y="8.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-29.5" y="-8.9" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-6" y="7" size="0.6096" layer="49">(layout parts on top layer)</text>
<hole x="17.27" y="0" drill="3.18"/>
<hole x="-17.27" y="0" drill="3.18"/>
<hole x="27.43" y="7.47" drill="1.7"/>
</package>
<package name="BATTCON_12MM">
<wire x1="-6.096" y1="4.318" x2="-3.81" y2="5.334" width="0.2032" layer="21" curve="-90"/>
<wire x1="3.81" y1="5.334" x2="6.096" y2="4.318" width="0.2032" layer="21" curve="-90"/>
<wire x1="6.096" y1="4.318" x2="6.096" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="6.096" y1="-3.302" x2="3.048" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="3.048" y1="-6.35" x2="-3.048" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="-6.35" x2="-6.096" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="-6.096" y1="-3.302" x2="-6.096" y2="4.318" width="0.2032" layer="21"/>
<wire x1="3.81" y1="5.334" x2="-3.81" y2="5.334" width="0.2032" layer="21" curve="-90"/>
<smd name="GND" x="0" y="0" dx="3.9624" dy="3.9624" layer="1"/>
<smd name="PWR@1" x="-7.874" y="0" dx="3.175" dy="3.175" layer="1"/>
<smd name="PWR@2" x="7.874" y="0" dx="3.175" dy="3.175" layer="1"/>
</package>
<package name="BATTCON_20MM_4LEGS">
<wire x1="-7.5" y1="7.35" x2="7.5" y2="7.35" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="7.35" x2="-10.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="7.5" y1="7.35" x2="10.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="10.55" y1="2.55" x2="10.55" y2="0.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="-0.55" x2="10.55" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="2.55" x2="-10.55" y2="0.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="-0.55" x2="-10.55" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="2.55" x2="11.45" y2="2.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="2.55" x2="11.45" y2="0.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="0.55" x2="10.55" y2="0.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="-0.55" x2="11.45" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="-0.55" x2="11.45" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="-2.55" x2="10.55" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="-2.55" x2="-11.45" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="-2.55" x2="-11.45" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="-0.55" x2="-10.55" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="0.55" x2="-11.45" y2="0.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="0.55" x2="-11.45" y2="2.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="2.55" x2="-10.55" y2="2.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="-4.55" x2="5.55" y2="-7.95" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-7.95" x2="-5.55" y2="-7.95" width="0.2032" layer="21" curve="62.415735"/>
<wire x1="-5.55" y1="-7.95" x2="-10.55" y2="-4.55" width="0.2032" layer="21"/>
<wire x1="10.55" y1="4.65" x2="10.55" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-10.55" y1="3.2" x2="-10.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-10.55" y1="-4.55" x2="-10.55" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="10.55" y1="-4.55" x2="10.55" y2="-3.2" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="10" width="0.2032" layer="51"/>
<smd name="1" x="-11" y="1.55" dx="2.54" dy="1.778" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="13" dy="8" layer="1" roundness="100" cream="no"/>
<smd name="3" x="11" y="1.55" dx="2.54" dy="1.778" layer="1" rot="R90"/>
<smd name="P$1" x="-11" y="-1.55" dx="2.54" dy="1.778" layer="1" rot="R90"/>
<smd name="P$2" x="11" y="-1.55" dx="2.54" dy="1.778" layer="1" rot="R90"/>
<text x="-0.889" y="5.969" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="4.699" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2" y1="-2" x2="2" y2="2" layer="31"/>
</package>
<package name="BATTERY-AA-KIT">
<description>&lt;h3&gt;BATTERY-AA-KIT&lt;/h3&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-17.526" y1="-4.826" x2="-27.051" y2="-4.826" width="0.254" layer="41"/>
<wire x1="-27.051" y1="-4.826" x2="-27.051" y2="4.826" width="0.254" layer="41"/>
<wire x1="-27.051" y1="4.826" x2="-17.526" y2="4.826" width="0.254" layer="41"/>
<wire x1="27.051" y1="4.826" x2="17.526" y2="4.826" width="0.254" layer="21"/>
<wire x1="17.526" y1="-4.826" x2="27.051" y2="-4.826" width="0.254" layer="21"/>
<wire x1="27.051" y1="-4.826" x2="27.051" y2="4.826" width="0.254" layer="21"/>
<wire x1="-26.67" y1="7.62" x2="26.67" y2="7.62" width="0.127" layer="51"/>
<wire x1="26.67" y1="-7.62" x2="-26.67" y2="-7.62" width="0.127" layer="51"/>
<wire x1="-17.526" y1="4.826" x2="-17.526" y2="-4.826" width="0.254" layer="41"/>
<wire x1="-17.526" y1="-4.826" x2="-27.051" y2="-4.826" width="0.254" layer="21"/>
<wire x1="-27.051" y1="-4.826" x2="-27.051" y2="4.826" width="0.254" layer="21"/>
<wire x1="-27.051" y1="4.826" x2="-17.526" y2="4.826" width="0.254" layer="21"/>
<wire x1="17.526" y1="4.826" x2="27.051" y2="4.826" width="0.254" layer="41"/>
<wire x1="27.051" y1="4.826" x2="27.051" y2="-4.826" width="0.254" layer="41"/>
<wire x1="27.051" y1="-4.826" x2="17.526" y2="-4.826" width="0.254" layer="41"/>
<wire x1="17.526" y1="-4.826" x2="17.526" y2="4.826" width="0.254" layer="41"/>
<wire x1="-24.0157" y1="-0.0482" x2="-18.9129" y2="-0.0457" width="0.4064" layer="49"/>
<wire x1="18.9103" y1="-0.0482" x2="24.0131" y2="-0.0457" width="0.4064" layer="49"/>
<wire x1="13.97" y1="2.54" x2="-12.7" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="2.54" x2="-12.7" y2="0.5842" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0.5842" x2="-12.7" y2="-0.6858" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="-0.6858" x2="-12.7" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="13.97" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="13.97" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0.5842" x2="-13.97" y2="0.5842" width="0.4064" layer="21"/>
<wire x1="-13.97" y1="0.5842" x2="-13.97" y2="-0.6858" width="0.4064" layer="21"/>
<wire x1="-13.97" y1="-0.6858" x2="-12.7" y2="-0.6858" width="0.4064" layer="21"/>
<wire x1="12.065" y1="0" x2="10.795" y2="0" width="0.4064" layer="21"/>
<wire x1="-9.525" y1="0" x2="-10.795" y2="0" width="0.4064" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.4064" layer="21"/>
<pad name="GND@1" x="25.146" y="0" drill="1.8542" stop="no"/>
<pad name="GND@2" x="18.034" y="0" drill="1.8542" stop="no"/>
<pad name="PWR@1" x="-25.146" y="0" drill="1.8542" stop="no"/>
<pad name="PWR@2" x="-18.034" y="0" drill="1.8542" rot="R90" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-23.8252" y="-0.0508" curve="90"/>
<vertex x="-25.146" y="1.3462" curve="90"/>
<vertex x="-26.4668" y="-0.0762" curve="90"/>
<vertex x="-25.146" y="-1.3462" curve="90"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-25.1206" y="-0.8636" curve="-90.090301"/>
<vertex x="-26.0096" y="-0.0508" curve="-90"/>
<vertex x="-25.1714" y="0.8636" curve="-89.987112"/>
<vertex x="-24.2824" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="26.4668" y="-0.0508" curve="90"/>
<vertex x="25.146" y="1.3462" curve="90"/>
<vertex x="23.8252" y="-0.0762" curve="90"/>
<vertex x="25.146" y="-1.3462" curve="90"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="25.1714" y="-0.8636" curve="-90.090301"/>
<vertex x="24.2824" y="-0.0508" curve="-90"/>
<vertex x="25.1206" y="0.8636" curve="-89.987112"/>
<vertex x="26.0096" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-16.7132" y="-0.0508" curve="90"/>
<vertex x="-18.034" y="1.3462" curve="90"/>
<vertex x="-19.3548" y="-0.0762" curve="90"/>
<vertex x="-18.034" y="-1.3462" curve="90"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-18.0086" y="-0.8636" curve="-90.090301"/>
<vertex x="-18.8976" y="-0.0508" curve="-90"/>
<vertex x="-18.0594" y="0.8636" curve="-89.987112"/>
<vertex x="-17.1704" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="19.3548" y="-0.0508" curve="90"/>
<vertex x="18.034" y="1.3462" curve="90"/>
<vertex x="16.7132" y="-0.0762" curve="90"/>
<vertex x="18.034" y="-1.3462" curve="90"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="18.0594" y="-0.8636" curve="-90.090301"/>
<vertex x="17.1704" y="-0.0508" curve="-90"/>
<vertex x="18.0086" y="0.8636" curve="-89.987112"/>
<vertex x="18.8976" y="0" curve="-90"/>
</polygon>
</package>
<package name="LIPO-110-TABS">
<wire x1="-6" y1="-11.5" x2="6" y2="-11.5" width="0.254" layer="51"/>
<wire x1="6" y1="-11.5" x2="6" y2="11.5" width="0.254" layer="51"/>
<wire x1="-6" y1="11.5" x2="-6" y2="-11.5" width="0.254" layer="51"/>
<wire x1="2.5" y1="11" x2="2.5" y2="10" width="0.254" layer="21"/>
<wire x1="-3" y1="10.5" x2="-2" y2="10.5" width="0.254" layer="21"/>
<wire x1="2" y1="10.5" x2="3" y2="10.5" width="0.254" layer="21"/>
<wire x1="-4.414" y1="11.5" x2="-6" y2="11.5" width="0.254" layer="21"/>
<wire x1="6" y1="11.5" x2="-6" y2="11.5" width="0.254" layer="51"/>
<wire x1="4.414" y1="11.5" x2="6" y2="11.5" width="0.254" layer="21"/>
<wire x1="0.586" y1="11.5" x2="-0.6" y2="11.5" width="0.254" layer="21"/>
<wire x1="-6" y1="-11.5" x2="6" y2="-11.5" width="0.254" layer="21"/>
<rectangle x1="-3.5" y1="11.5" x2="-1.5" y2="16.5" layer="51"/>
<rectangle x1="1.5" y1="11.5" x2="3.5" y2="16.5" layer="51"/>
<smd name="+" x="2.5" y="14.5" dx="3" dy="6" layer="1"/>
<smd name="-" x="-2.5" y="14.5" dx="3" dy="6" layer="1"/>
</package>
<package name="BATTCON_9V">
<pad name="+" x="0" y="0" drill="1.905"/>
<pad name="-" x="0" y="-12.954" drill="1.905"/>
<wire x1="-53.0098" y1="8.5598" x2="1.9304" y2="8.5598" width="0.1778" layer="21"/>
<wire x1="1.9304" y1="8.5598" x2="1.9304" y2="-21.4122" width="0.1778" layer="21"/>
<wire x1="1.9304" y1="-21.4122" x2="-53.0098" y2="-21.4122" width="0.1778" layer="21"/>
<wire x1="-53.0098" y1="-21.4122" x2="-53.0098" y2="8.5598" width="0.1778" layer="21"/>
<text x="-2.54" y="-10.16" size="1.27" layer="25" rot="R90">&gt;Name</text>
<text x="-0.635" y="-10.16" size="1.27" layer="27" rot="R90">&gt;Value</text>
<circle x="-39.2684" y="4.7498" radius="1.27" width="0.127" layer="51"/>
<circle x="-12.2936" y="4.7498" radius="1.27" width="0.127" layer="51"/>
<circle x="-25.781" y="-17.6022" radius="1.27" width="0.127" layer="51"/>
</package>
<package name="BATTCON_20MM_4LEGS_OVERPASTE">
<wire x1="-7.5" y1="7.35" x2="7.5" y2="7.35" width="0.2032" layer="21"/>
<wire x1="-7.5" y1="7.35" x2="-10.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="7.5" y1="7.35" x2="10.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="10.55" y1="2.55" x2="10.55" y2="0.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="-0.55" x2="10.55" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="2.55" x2="-10.55" y2="0.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="-0.55" x2="-10.55" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="2.55" x2="11.45" y2="2.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="2.55" x2="11.45" y2="0.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="0.55" x2="10.55" y2="0.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="-0.55" x2="11.45" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="-0.55" x2="11.45" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="11.45" y1="-2.55" x2="10.55" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="-2.55" x2="-11.45" y2="-2.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="-2.55" x2="-11.45" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="-0.55" x2="-10.55" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="-10.55" y1="0.55" x2="-11.45" y2="0.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="0.55" x2="-11.45" y2="2.55" width="0.2032" layer="51"/>
<wire x1="-11.45" y1="2.55" x2="-10.55" y2="2.55" width="0.2032" layer="51"/>
<wire x1="10.55" y1="-4.55" x2="5.55" y2="-7.95" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-7.95" x2="-5.55" y2="-7.95" width="0.2032" layer="21" curve="62.415735"/>
<wire x1="-5.55" y1="-7.95" x2="-10.55" y2="-4.55" width="0.2032" layer="21"/>
<wire x1="10.55" y1="4.65" x2="10.55" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-10.55" y1="3.2" x2="-10.55" y2="4.65" width="0.2032" layer="21"/>
<wire x1="-10.55" y1="-4.55" x2="-10.55" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="10.55" y1="-4.55" x2="10.55" y2="-3.2" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="10" width="0.2032" layer="51"/>
<smd name="1" x="-11" y="1.55" dx="2.54" dy="1.778" layer="1" rot="R90" cream="no"/>
<smd name="2" x="0" y="0" dx="13" dy="8" layer="1" roundness="100" cream="no"/>
<smd name="3" x="11" y="1.55" dx="2.54" dy="1.778" layer="1" rot="R90" cream="no"/>
<smd name="P$1" x="-11" y="-1.55" dx="2.54" dy="1.778" layer="1" rot="R90" cream="no"/>
<smd name="P$2" x="11" y="-1.55" dx="2.54" dy="1.778" layer="1" rot="R90" cream="no"/>
<text x="-0.889" y="5.969" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="4.699" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2" y1="-2" x2="2" y2="2" layer="31"/>
<rectangle x1="-13.97" y1="-3.048" x2="-9.906" y2="3.048" layer="31"/>
<rectangle x1="9.906" y1="-3.048" x2="13.97" y2="3.048" layer="31"/>
</package>
<package name="BATTERY-AAA-KIT">
<wire x1="-13.97" y1="3.81" x2="-13.97" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-13.97" y1="-3.81" x2="-23.495" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-23.495" y1="-3.81" x2="-23.495" y2="3.81" width="0.127" layer="51"/>
<wire x1="-23.495" y1="3.81" x2="-13.97" y2="3.81" width="0.127" layer="51"/>
<wire x1="23.4442" y1="3.81" x2="13.97" y2="3.81" width="0.127" layer="51"/>
<wire x1="13.97" y1="3.81" x2="13.97" y2="-3.81" width="0.127" layer="51"/>
<wire x1="13.97" y1="-3.81" x2="23.4442" y2="-3.81" width="0.127" layer="51"/>
<wire x1="23.4442" y1="-3.81" x2="23.4442" y2="3.81" width="0.127" layer="51"/>
<pad name="PWR@2" x="-13.97" y="0" drill="1.7018" stop="no"/>
<pad name="PWR@1" x="-21.59" y="0" drill="1.7018" stop="no"/>
<pad name="GND@2" x="13.97" y="0" drill="1.7018" stop="no"/>
<pad name="GND@1" x="21.59" y="0" drill="1.7018" stop="no"/>
<wire x1="-13.97" y1="-3.81" x2="-23.4442" y2="-3.81" width="0.254" layer="41"/>
<wire x1="-23.4442" y1="-3.81" x2="-23.4442" y2="3.81" width="0.254" layer="41"/>
<wire x1="-23.4442" y1="3.81" x2="-13.97" y2="3.81" width="0.254" layer="41"/>
<wire x1="23.4442" y1="3.81" x2="13.97" y2="3.81" width="0.254" layer="21"/>
<wire x1="13.97" y1="-3.81" x2="23.4442" y2="-3.81" width="0.254" layer="21"/>
<wire x1="23.4442" y1="-3.81" x2="23.4442" y2="3.81" width="0.254" layer="21"/>
<wire x1="-13.97" y1="3.81" x2="-13.97" y2="-3.81" width="0.254" layer="41"/>
<wire x1="-13.97" y1="-3.81" x2="-23.4442" y2="-3.81" width="0.254" layer="21"/>
<wire x1="-23.4442" y1="-3.81" x2="-23.4442" y2="3.81" width="0.254" layer="21"/>
<wire x1="-23.4442" y1="3.81" x2="-13.97" y2="3.81" width="0.254" layer="21"/>
<wire x1="13.97" y1="3.81" x2="23.4442" y2="3.81" width="0.254" layer="41"/>
<wire x1="23.4442" y1="3.81" x2="23.4442" y2="-3.81" width="0.254" layer="41"/>
<wire x1="23.4442" y1="-3.81" x2="13.97" y2="-3.81" width="0.254" layer="41"/>
<wire x1="13.97" y1="-3.81" x2="13.97" y2="3.81" width="0.254" layer="41"/>
<wire x1="-21.59" y1="0" x2="-13.97" y2="0" width="0.4064" layer="49"/>
<wire x1="13.97" y1="0" x2="21.59" y2="0" width="0.4064" layer="49"/>
<wire x1="11.43" y1="2.54" x2="-10.16" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-10.16" y1="2.54" x2="-10.16" y2="0.5842" width="0.4064" layer="21"/>
<wire x1="-10.16" y1="0.5842" x2="-10.16" y2="-0.6858" width="0.4064" layer="21"/>
<wire x1="-10.16" y1="-0.6858" x2="-10.16" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="11.43" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="11.43" y1="-2.54" x2="11.43" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-10.16" y1="0.5842" x2="-11.43" y2="0.5842" width="0.4064" layer="21"/>
<wire x1="-11.43" y1="0.5842" x2="-11.43" y2="-0.6858" width="0.4064" layer="21"/>
<wire x1="-11.43" y1="-0.6858" x2="-10.16" y2="-0.6858" width="0.4064" layer="21"/>
<wire x1="9.525" y1="0" x2="8.255" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.985" y1="0" x2="-8.255" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.4064" layer="21"/>
<circle x="13.97" y="0" radius="0.40160625" width="0.889" layer="29"/>
<circle x="13.97" y="0" radius="0.40160625" width="1.778" layer="30"/>
<circle x="21.59" y="0" radius="0.40160625" width="0.889" layer="29"/>
<circle x="21.59" y="0" radius="0.40160625" width="1.778" layer="30"/>
<circle x="-13.97" y="0" radius="0.40160625" width="0.889" layer="29"/>
<circle x="-13.97" y="0" radius="0.40160625" width="1.778" layer="30"/>
<circle x="-21.59" y="0" radius="0.40160625" width="1.778" layer="30"/>
<circle x="-21.59" y="0" radius="0.40160625" width="0.889" layer="29"/>
<wire x1="-22.86" y1="5.08" x2="22.86" y2="5.08" width="0.127" layer="51"/>
<wire x1="-22.86" y1="-5.08" x2="22.86" y2="-5.08" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SWITCH-MOMENTARY">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="3" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2"/>
</symbol>
<symbol name="BATTERY">
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.524" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TAC_SWITCH" prefix="S" uservalue="yes">
<description>&lt;b&gt;Momentary Switch&lt;/b&gt;&lt;br&gt;
Button commonly used for reset or general input.&lt;br&gt;
Spark Fun Electronics SKU : COM-00097&lt;br&gt;
SMT- SWCH-08247</description>
<gates>
<gate name="S" symbol="SWITCH-MOMENTARY" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="TACTILE-PTH">
<connects>
<connect gate="S" pin="1" pad="1"/>
<connect gate="S" pin="2" pad="2"/>
<connect gate="S" pin="3" pad="3"/>
<connect gate="S" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KSA_SEALED" package="KSA_SEALED_TAC_SWITCH">
<connects>
<connect gate="S" pin="1" pad="P$1"/>
<connect gate="S" pin="2" pad="P$2"/>
<connect gate="S" pin="3" pad="P$3"/>
<connect gate="S" pin="4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="TACTILE-SWITCH-SMD">
<connects>
<connect gate="S" pin="1" pad="1"/>
<connect gate="S" pin="2" pad="2"/>
<connect gate="S" pin="3" pad="3"/>
<connect gate="S" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATTERY" prefix="BAT" uservalue="yes">
<description>&lt;b&gt;Battery Holders&lt;/b&gt;&lt;br&gt;
Various common sizes : AA, AAA, 20mm coin cell and 12mm coin cell.&lt;br&gt;
20MM_4LEGS, BATT-10373</description>
<gates>
<gate name="G$1" symbol="BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="AAA" package="BATTERY-AAA">
<connects>
<connect gate="G$1" pin="+" pad="PWR@1"/>
<connect gate="G$1" pin="-" pad="GND@1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="BATT-08571" constant="no"/>
</technology>
</technologies>
</device>
<device name="20PTH2" package="BATTERY">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA" package="BATTERY-AA">
<connects>
<connect gate="G$1" pin="+" pad="PWR@1"/>
<connect gate="G$1" pin="-" pad="GND@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12PTH" package="BATTCON_12MM_PTH">
<connects>
<connect gate="G$1" pin="+" pad="VCC@1"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20SMD" package="BATTCON_20MM">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="245MM" package="BATTCON_245MM">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20PTH" package="BATTCOM_20MM_PTH">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="245PTH" package="BATTCON_245MM_PTH">
<connects>
<connect gate="G$1" pin="+" pad="3"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1100" package="LIPO-1100MAH">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA-PANEL" package="BATTERY-AA-PANEL">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FOB" package="BATTERY_20MM_PTH_COMPACT">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="12MM" package="BATTCON_12MM">
<connects>
<connect gate="G$1" pin="+" pad="PWR@1"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20MM_4LEGS" package="BATTCON_20MM_4LEGS">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="BATT-10373"/>
<attribute name="VALUE" value="20mm coincell" constant="no"/>
</technology>
</technologies>
</device>
<device name="AA-KIT" package="BATTERY-AA-KIT">
<connects>
<connect gate="G$1" pin="+" pad="PWR@1"/>
<connect gate="G$1" pin="-" pad="GND@1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="BATT-08316" constant="no"/>
</technology>
</technologies>
</device>
<device name="LIPO-TABS" package="LIPO-110-TABS">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="9V" package="BATTCON_9V">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="20MM_4LEGS_OVERPASTE" package="BATTCON_20MM_4LEGS_OVERPASTE">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AAA-KIT" package="BATTERY-AAA-KIT">
<connects>
<connect gate="G$1" pin="+" pad="PWR@1"/>
<connect gate="G$1" pin="-" pad="GND@1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="BATT-08571" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rveer">
<packages>
<package name="SOT23">
<smd name="1" x="0" y="0" dx="0.6" dy="1.1" layer="1" rot="R180"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.1" layer="1" rot="R180"/>
<smd name="3" x="1.9" y="0" dx="0.6" dy="1.1" layer="1" rot="R180"/>
<smd name="5" x="0" y="2.85" dx="0.6" dy="1.1" layer="1" rot="R180"/>
<smd name="4" x="1.9" y="2.85" dx="0.6" dy="1.1" layer="1" rot="R180"/>
<wire x1="-0.475" y1="1.9" x2="2.375" y2="1.9" width="0.127" layer="21"/>
<wire x1="2.375" y1="1.9" x2="2.375" y2="0.95" width="0.127" layer="21"/>
<wire x1="2.375" y1="0.95" x2="-0.475" y2="0.95" width="0.127" layer="21"/>
<wire x1="-0.475" y1="0.95" x2="-0.475" y2="1.9" width="0.127" layer="21"/>
</package>
<package name="LOGO_SIMPLYDUINO_SMALL">
<rectangle x1="3.934459375" y1="-0.002540625" x2="3.939540625" y2="0.002540625" layer="21"/>
<rectangle x1="3.9497" y1="-0.002540625" x2="3.95478125" y2="0.002540625" layer="21"/>
<rectangle x1="3.964940625" y1="-0.002540625" x2="3.97001875" y2="0.002540625" layer="21"/>
<rectangle x1="3.888740625" y1="0.002540625" x2="4.0259" y2="0.00761875" layer="21"/>
<rectangle x1="3.85318125" y1="0.00761875" x2="4.05638125" y2="0.0127" layer="21"/>
<rectangle x1="3.82778125" y1="0.0127" x2="4.08178125" y2="0.01778125" layer="21"/>
<rectangle x1="2.55778125" y1="0.01778125" x2="2.7813" y2="0.022859375" layer="21"/>
<rectangle x1="3.812540625" y1="0.01778125" x2="4.091940625" y2="0.022859375" layer="21"/>
<rectangle x1="2.5527" y1="0.022859375" x2="2.7813" y2="0.027940625" layer="21"/>
<rectangle x1="3.79221875" y1="0.022859375" x2="4.10718125" y2="0.027940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.027940625" x2="2.7813" y2="0.03301875" layer="21"/>
<rectangle x1="3.787140625" y1="0.027940625" x2="4.117340625" y2="0.03301875" layer="21"/>
<rectangle x1="2.55778125" y1="0.03301875" x2="2.7813" y2="0.0381" layer="21"/>
<rectangle x1="3.787140625" y1="0.03301875" x2="4.1275" y2="0.0381" layer="21"/>
<rectangle x1="2.55778125" y1="0.0381" x2="2.7813" y2="0.04318125" layer="21"/>
<rectangle x1="3.787140625" y1="0.0381" x2="4.13258125" y2="0.04318125" layer="21"/>
<rectangle x1="2.55778125" y1="0.04318125" x2="2.7813" y2="0.048259375" layer="21"/>
<rectangle x1="3.787140625" y1="0.04318125" x2="4.142740625" y2="0.048259375" layer="21"/>
<rectangle x1="2.55778125" y1="0.048259375" x2="2.7813" y2="0.053340625" layer="21"/>
<rectangle x1="3.79221875" y1="0.048259375" x2="4.14781875" y2="0.053340625" layer="21"/>
<rectangle x1="2.55778125" y1="0.053340625" x2="2.7813" y2="0.05841875" layer="21"/>
<rectangle x1="3.79221875" y1="0.053340625" x2="4.1529" y2="0.05841875" layer="21"/>
<rectangle x1="2.5527" y1="0.05841875" x2="2.7813" y2="0.0635" layer="21"/>
<rectangle x1="3.79221875" y1="0.05841875" x2="4.163059375" y2="0.0635" layer="21"/>
<rectangle x1="2.55778125" y1="0.0635" x2="2.7813" y2="0.06858125" layer="21"/>
<rectangle x1="3.79221875" y1="0.0635" x2="4.168140625" y2="0.06858125" layer="21"/>
<rectangle x1="2.55778125" y1="0.06858125" x2="2.7813" y2="0.073659375" layer="21"/>
<rectangle x1="3.79221875" y1="0.06858125" x2="4.168140625" y2="0.073659375" layer="21"/>
<rectangle x1="2.55778125" y1="0.073659375" x2="2.7813" y2="0.078740625" layer="21"/>
<rectangle x1="3.79221875" y1="0.073659375" x2="4.1783" y2="0.078740625" layer="21"/>
<rectangle x1="2.5527" y1="0.078740625" x2="2.7813" y2="0.08381875" layer="21"/>
<rectangle x1="3.7973" y1="0.078740625" x2="4.1783" y2="0.08381875" layer="21"/>
<rectangle x1="2.55778125" y1="0.08381875" x2="2.7813" y2="0.0889" layer="21"/>
<rectangle x1="3.7973" y1="0.08381875" x2="4.18338125" y2="0.0889" layer="21"/>
<rectangle x1="2.55778125" y1="0.0889" x2="2.7813" y2="0.09398125" layer="21"/>
<rectangle x1="3.7973" y1="0.0889" x2="4.188459375" y2="0.09398125" layer="21"/>
<rectangle x1="2.55778125" y1="0.09398125" x2="2.7813" y2="0.099059375" layer="21"/>
<rectangle x1="3.7973" y1="0.09398125" x2="4.193540625" y2="0.099059375" layer="21"/>
<rectangle x1="2.5527" y1="0.099059375" x2="2.7813" y2="0.104140625" layer="21"/>
<rectangle x1="3.7973" y1="0.099059375" x2="4.193540625" y2="0.104140625" layer="21"/>
<rectangle x1="2.55778125" y1="0.104140625" x2="2.7813" y2="0.10921875" layer="21"/>
<rectangle x1="3.7973" y1="0.104140625" x2="4.19861875" y2="0.10921875" layer="21"/>
<rectangle x1="2.55778125" y1="0.10921875" x2="2.7813" y2="0.1143" layer="21"/>
<rectangle x1="3.7973" y1="0.10921875" x2="4.19861875" y2="0.1143" layer="21"/>
<rectangle x1="2.55778125" y1="0.1143" x2="2.7813" y2="0.11938125" layer="21"/>
<rectangle x1="3.80238125" y1="0.1143" x2="4.2037" y2="0.11938125" layer="21"/>
<rectangle x1="2.5527" y1="0.11938125" x2="2.7813" y2="0.124459375" layer="21"/>
<rectangle x1="3.80238125" y1="0.11938125" x2="4.20878125" y2="0.124459375" layer="21"/>
<rectangle x1="2.5527" y1="0.124459375" x2="2.7813" y2="0.129540625" layer="21"/>
<rectangle x1="3.80238125" y1="0.124459375" x2="4.20878125" y2="0.129540625" layer="21"/>
<rectangle x1="2.55778125" y1="0.129540625" x2="2.7813" y2="0.13461875" layer="21"/>
<rectangle x1="3.80238125" y1="0.129540625" x2="4.213859375" y2="0.13461875" layer="21"/>
<rectangle x1="2.55778125" y1="0.13461875" x2="2.7813" y2="0.1397" layer="21"/>
<rectangle x1="3.80238125" y1="0.13461875" x2="4.213859375" y2="0.1397" layer="21"/>
<rectangle x1="2.55778125" y1="0.1397" x2="2.7813" y2="0.14478125" layer="21"/>
<rectangle x1="3.80238125" y1="0.1397" x2="4.218940625" y2="0.14478125" layer="21"/>
<rectangle x1="2.5527" y1="0.14478125" x2="2.7813" y2="0.149859375" layer="21"/>
<rectangle x1="3.80238125" y1="0.14478125" x2="4.218940625" y2="0.149859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.149859375" x2="2.7813" y2="0.154940625" layer="21"/>
<rectangle x1="3.807459375" y1="0.149859375" x2="4.22401875" y2="0.154940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.154940625" x2="2.7813" y2="0.16001875" layer="21"/>
<rectangle x1="3.807459375" y1="0.154940625" x2="4.22401875" y2="0.16001875" layer="21"/>
<rectangle x1="2.55778125" y1="0.16001875" x2="2.7813" y2="0.1651" layer="21"/>
<rectangle x1="3.807459375" y1="0.16001875" x2="4.22401875" y2="0.1651" layer="21"/>
<rectangle x1="2.55778125" y1="0.1651" x2="2.7813" y2="0.17018125" layer="21"/>
<rectangle x1="3.807459375" y1="0.1651" x2="4.2291" y2="0.17018125" layer="21"/>
<rectangle x1="2.55778125" y1="0.17018125" x2="2.7813" y2="0.175259375" layer="21"/>
<rectangle x1="3.807459375" y1="0.17018125" x2="4.2291" y2="0.175259375" layer="21"/>
<rectangle x1="2.55778125" y1="0.175259375" x2="2.7813" y2="0.180340625" layer="21"/>
<rectangle x1="3.812540625" y1="0.175259375" x2="4.23418125" y2="0.180340625" layer="21"/>
<rectangle x1="2.5527" y1="0.180340625" x2="2.7813" y2="0.18541875" layer="21"/>
<rectangle x1="3.812540625" y1="0.180340625" x2="4.23418125" y2="0.18541875" layer="21"/>
<rectangle x1="2.55778125" y1="0.18541875" x2="2.7813" y2="0.1905" layer="21"/>
<rectangle x1="3.812540625" y1="0.18541875" x2="4.239259375" y2="0.1905" layer="21"/>
<rectangle x1="2.55778125" y1="0.1905" x2="2.7813" y2="0.19558125" layer="21"/>
<rectangle x1="3.812540625" y1="0.1905" x2="3.883659375" y2="0.19558125" layer="21"/>
<rectangle x1="3.95478125" y1="0.1905" x2="4.239259375" y2="0.19558125" layer="21"/>
<rectangle x1="2.55778125" y1="0.19558125" x2="2.7813" y2="0.200659375" layer="21"/>
<rectangle x1="3.812540625" y1="0.19558125" x2="3.858259375" y2="0.200659375" layer="21"/>
<rectangle x1="3.98018125" y1="0.19558125" x2="4.239259375" y2="0.200659375" layer="21"/>
<rectangle x1="2.5527" y1="0.200659375" x2="2.7813" y2="0.205740625" layer="21"/>
<rectangle x1="3.81761875" y1="0.200659375" x2="3.84301875" y2="0.205740625" layer="21"/>
<rectangle x1="3.990340625" y1="0.200659375" x2="4.244340625" y2="0.205740625" layer="21"/>
<rectangle x1="2.55778125" y1="0.205740625" x2="2.7813" y2="0.21081875" layer="21"/>
<rectangle x1="3.812540625" y1="0.205740625" x2="3.82778125" y2="0.21081875" layer="21"/>
<rectangle x1="4.0005" y1="0.205740625" x2="4.244340625" y2="0.21081875" layer="21"/>
<rectangle x1="2.55778125" y1="0.21081875" x2="2.7813" y2="0.2159" layer="21"/>
<rectangle x1="4.00558125" y1="0.21081875" x2="4.24941875" y2="0.2159" layer="21"/>
<rectangle x1="2.55778125" y1="0.2159" x2="2.7813" y2="0.22098125" layer="21"/>
<rectangle x1="4.015740625" y1="0.2159" x2="4.24941875" y2="0.22098125" layer="21"/>
<rectangle x1="2.5527" y1="0.22098125" x2="2.7813" y2="0.226059375" layer="21"/>
<rectangle x1="4.02081875" y1="0.22098125" x2="4.24941875" y2="0.226059375" layer="21"/>
<rectangle x1="2.55778125" y1="0.226059375" x2="2.7813" y2="0.231140625" layer="21"/>
<rectangle x1="4.0259" y1="0.226059375" x2="4.2545" y2="0.231140625" layer="21"/>
<rectangle x1="2.55778125" y1="0.231140625" x2="2.7813" y2="0.23621875" layer="21"/>
<rectangle x1="4.0259" y1="0.231140625" x2="4.2545" y2="0.23621875" layer="21"/>
<rectangle x1="2.55778125" y1="0.23621875" x2="2.7813" y2="0.2413" layer="21"/>
<rectangle x1="4.03098125" y1="0.23621875" x2="4.25958125" y2="0.2413" layer="21"/>
<rectangle x1="2.5527" y1="0.2413" x2="2.7813" y2="0.24638125" layer="21"/>
<rectangle x1="4.036059375" y1="0.2413" x2="4.25958125" y2="0.24638125" layer="21"/>
<rectangle x1="2.5527" y1="0.24638125" x2="2.7813" y2="0.251459375" layer="21"/>
<rectangle x1="4.036059375" y1="0.24638125" x2="4.25958125" y2="0.251459375" layer="21"/>
<rectangle x1="2.55778125" y1="0.251459375" x2="2.7813" y2="0.256540625" layer="21"/>
<rectangle x1="4.041140625" y1="0.251459375" x2="4.264659375" y2="0.256540625" layer="21"/>
<rectangle x1="2.55778125" y1="0.256540625" x2="2.7813" y2="0.26161875" layer="21"/>
<rectangle x1="4.041140625" y1="0.256540625" x2="4.264659375" y2="0.26161875" layer="21"/>
<rectangle x1="2.55778125" y1="0.26161875" x2="2.7813" y2="0.2667" layer="21"/>
<rectangle x1="4.04621875" y1="0.26161875" x2="4.264659375" y2="0.2667" layer="21"/>
<rectangle x1="2.5527" y1="0.2667" x2="2.7813" y2="0.27178125" layer="21"/>
<rectangle x1="4.04621875" y1="0.2667" x2="4.269740625" y2="0.27178125" layer="21"/>
<rectangle x1="2.55778125" y1="0.27178125" x2="2.7813" y2="0.276859375" layer="21"/>
<rectangle x1="4.0513" y1="0.27178125" x2="4.269740625" y2="0.276859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.276859375" x2="2.7813" y2="0.281940625" layer="21"/>
<rectangle x1="4.0513" y1="0.276859375" x2="4.27481875" y2="0.281940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.281940625" x2="2.7813" y2="0.28701875" layer="21"/>
<rectangle x1="4.05638125" y1="0.281940625" x2="4.27481875" y2="0.28701875" layer="21"/>
<rectangle x1="2.55778125" y1="0.28701875" x2="2.7813" y2="0.2921" layer="21"/>
<rectangle x1="4.05638125" y1="0.28701875" x2="4.27481875" y2="0.2921" layer="21"/>
<rectangle x1="2.55778125" y1="0.2921" x2="2.7813" y2="0.29718125" layer="21"/>
<rectangle x1="4.05638125" y1="0.2921" x2="4.2799" y2="0.29718125" layer="21"/>
<rectangle x1="2.55778125" y1="0.29718125" x2="2.7813" y2="0.302259375" layer="21"/>
<rectangle x1="4.061459375" y1="0.29718125" x2="4.2799" y2="0.302259375" layer="21"/>
<rectangle x1="2.5527" y1="0.302259375" x2="2.7813" y2="0.307340625" layer="21"/>
<rectangle x1="4.061459375" y1="0.302259375" x2="4.28498125" y2="0.307340625" layer="21"/>
<rectangle x1="2.55778125" y1="0.307340625" x2="2.7813" y2="0.31241875" layer="21"/>
<rectangle x1="4.066540625" y1="0.307340625" x2="4.28498125" y2="0.31241875" layer="21"/>
<rectangle x1="2.55778125" y1="0.31241875" x2="2.7813" y2="0.3175" layer="21"/>
<rectangle x1="4.066540625" y1="0.31241875" x2="4.28498125" y2="0.3175" layer="21"/>
<rectangle x1="2.55778125" y1="0.3175" x2="2.7813" y2="0.32258125" layer="21"/>
<rectangle x1="4.066540625" y1="0.3175" x2="4.290059375" y2="0.32258125" layer="21"/>
<rectangle x1="0.3175" y1="0.32258125" x2="0.32258125" y2="0.327659375" layer="21"/>
<rectangle x1="0.327659375" y1="0.32258125" x2="0.332740625" y2="0.327659375" layer="21"/>
<rectangle x1="0.33781875" y1="0.32258125" x2="0.3429" y2="0.327659375" layer="21"/>
<rectangle x1="0.353059375" y1="0.32258125" x2="0.358140625" y2="0.327659375" layer="21"/>
<rectangle x1="0.3683" y1="0.32258125" x2="0.37338125" y2="0.327659375" layer="21"/>
<rectangle x1="2.5527" y1="0.32258125" x2="2.7813" y2="0.327659375" layer="21"/>
<rectangle x1="4.07161875" y1="0.32258125" x2="4.290059375" y2="0.327659375" layer="21"/>
<rectangle x1="0.2667" y1="0.327659375" x2="0.27178125" y2="0.332740625" layer="21"/>
<rectangle x1="0.276859375" y1="0.327659375" x2="0.42418125" y2="0.332740625" layer="21"/>
<rectangle x1="2.55778125" y1="0.327659375" x2="2.7813" y2="0.332740625" layer="21"/>
<rectangle x1="4.07161875" y1="0.327659375" x2="4.295140625" y2="0.332740625" layer="21"/>
<rectangle x1="0.23621875" y1="0.332740625" x2="0.459740625" y2="0.33781875" layer="21"/>
<rectangle x1="2.55778125" y1="0.332740625" x2="2.7813" y2="0.33781875" layer="21"/>
<rectangle x1="2.969259375" y1="0.332740625" x2="2.974340625" y2="0.33781875" layer="21"/>
<rectangle x1="2.97941875" y1="0.332740625" x2="2.9845" y2="0.33781875" layer="21"/>
<rectangle x1="2.98958125" y1="0.332740625" x2="3.00481875" y2="0.33781875" layer="21"/>
<rectangle x1="3.0099" y1="0.332740625" x2="3.03021875" y2="0.33781875" layer="21"/>
<rectangle x1="3.0353" y1="0.332740625" x2="3.04038125" y2="0.33781875" layer="21"/>
<rectangle x1="4.0767" y1="0.332740625" x2="4.295140625" y2="0.33781875" layer="21"/>
<rectangle x1="4.980940625" y1="0.332740625" x2="5.006340625" y2="0.33781875" layer="21"/>
<rectangle x1="5.01141875" y1="0.332740625" x2="5.026659375" y2="0.33781875" layer="21"/>
<rectangle x1="5.031740625" y1="0.332740625" x2="5.0419" y2="0.33781875" layer="21"/>
<rectangle x1="5.79881875" y1="0.332740625" x2="5.8039" y2="0.33781875" layer="21"/>
<rectangle x1="5.814059375" y1="0.332740625" x2="5.82421875" y2="0.33781875" layer="21"/>
<rectangle x1="5.8293" y1="0.332740625" x2="5.83438125" y2="0.33781875" layer="21"/>
<rectangle x1="5.839459375" y1="0.332740625" x2="5.85978125" y2="0.33781875" layer="21"/>
<rectangle x1="5.864859375" y1="0.332740625" x2="5.869940625" y2="0.33781875" layer="21"/>
<rectangle x1="7.846059375" y1="0.332740625" x2="7.851140625" y2="0.33781875" layer="21"/>
<rectangle x1="7.85621875" y1="0.332740625" x2="7.871459375" y2="0.33781875" layer="21"/>
<rectangle x1="7.876540625" y1="0.332740625" x2="7.89178125" y2="0.33781875" layer="21"/>
<rectangle x1="7.896859375" y1="0.332740625" x2="7.9121" y2="0.33781875" layer="21"/>
<rectangle x1="0.2159" y1="0.33781875" x2="0.480059375" y2="0.3429" layer="21"/>
<rectangle x1="2.55778125" y1="0.33781875" x2="2.7813" y2="0.3429" layer="21"/>
<rectangle x1="2.93878125" y1="0.33781875" x2="3.070859375" y2="0.3429" layer="21"/>
<rectangle x1="4.0767" y1="0.33781875" x2="4.295140625" y2="0.3429" layer="21"/>
<rectangle x1="4.9403" y1="0.33781875" x2="5.082540625" y2="0.3429" layer="21"/>
<rectangle x1="5.77341875" y1="0.33781875" x2="5.9055" y2="0.3429" layer="21"/>
<rectangle x1="7.8105" y1="0.33781875" x2="7.95781875" y2="0.3429" layer="21"/>
<rectangle x1="0.19558125" y1="0.3429" x2="0.50038125" y2="0.34798125" layer="21"/>
<rectangle x1="2.5527" y1="0.3429" x2="2.7813" y2="0.34798125" layer="21"/>
<rectangle x1="2.91338125" y1="0.3429" x2="3.09118125" y2="0.34798125" layer="21"/>
<rectangle x1="4.0767" y1="0.3429" x2="4.295140625" y2="0.34798125" layer="21"/>
<rectangle x1="4.91998125" y1="0.3429" x2="5.107940625" y2="0.34798125" layer="21"/>
<rectangle x1="5.74801875" y1="0.3429" x2="5.920740625" y2="0.34798125" layer="21"/>
<rectangle x1="7.79018125" y1="0.3429" x2="7.978140625" y2="0.34798125" layer="21"/>
<rectangle x1="0.180340625" y1="0.34798125" x2="0.5207" y2="0.353059375" layer="21"/>
<rectangle x1="2.55778125" y1="0.34798125" x2="2.7813" y2="0.353059375" layer="21"/>
<rectangle x1="2.893059375" y1="0.34798125" x2="3.10641875" y2="0.353059375" layer="21"/>
<rectangle x1="4.0767" y1="0.34798125" x2="4.30021875" y2="0.353059375" layer="21"/>
<rectangle x1="4.899659375" y1="0.34798125" x2="5.12318125" y2="0.353059375" layer="21"/>
<rectangle x1="5.73278125" y1="0.34798125" x2="5.93598125" y2="0.353059375" layer="21"/>
<rectangle x1="7.769859375" y1="0.34798125" x2="7.998459375" y2="0.353059375" layer="21"/>
<rectangle x1="0.16001875" y1="0.353059375" x2="0.535940625" y2="0.358140625" layer="21"/>
<rectangle x1="0.886459375" y1="0.353059375" x2="1.115059375" y2="0.358140625" layer="21"/>
<rectangle x1="1.267459375" y1="0.353059375" x2="1.49098125" y2="0.358140625" layer="21"/>
<rectangle x1="1.7145" y1="0.353059375" x2="1.93801875" y2="0.358140625" layer="21"/>
<rectangle x1="2.16661875" y1="0.353059375" x2="2.390140625" y2="0.358140625" layer="21"/>
<rectangle x1="2.55778125" y1="0.353059375" x2="2.7813" y2="0.358140625" layer="21"/>
<rectangle x1="2.87781875" y1="0.353059375" x2="3.121659375" y2="0.358140625" layer="21"/>
<rectangle x1="3.46201875" y1="0.353059375" x2="3.69061875" y2="0.358140625" layer="21"/>
<rectangle x1="4.0767" y1="0.353059375" x2="4.3053" y2="0.358140625" layer="21"/>
<rectangle x1="4.88441875" y1="0.353059375" x2="5.13841875" y2="0.358140625" layer="21"/>
<rectangle x1="5.285740625" y1="0.353059375" x2="5.37718125" y2="0.358140625" layer="21"/>
<rectangle x1="5.717540625" y1="0.353059375" x2="5.95121875" y2="0.358140625" layer="21"/>
<rectangle x1="6.07821875" y1="0.353059375" x2="6.174740625" y2="0.358140625" layer="21"/>
<rectangle x1="6.41858125" y1="0.353059375" x2="6.51001875" y2="0.358140625" layer="21"/>
<rectangle x1="6.74878125" y1="0.353059375" x2="6.8453" y2="0.358140625" layer="21"/>
<rectangle x1="7.241540625" y1="0.353059375" x2="7.33298125" y2="0.358140625" layer="21"/>
<rectangle x1="7.75461875" y1="0.353059375" x2="8.0137" y2="0.358140625" layer="21"/>
<rectangle x1="0.149859375" y1="0.358140625" x2="0.54101875" y2="0.36321875" layer="21"/>
<rectangle x1="0.891540625" y1="0.358140625" x2="1.10998125" y2="0.36321875" layer="21"/>
<rectangle x1="1.267459375" y1="0.358140625" x2="1.49098125" y2="0.36321875" layer="21"/>
<rectangle x1="1.7145" y1="0.358140625" x2="1.93801875" y2="0.36321875" layer="21"/>
<rectangle x1="2.16661875" y1="0.358140625" x2="2.390140625" y2="0.36321875" layer="21"/>
<rectangle x1="2.55778125" y1="0.358140625" x2="2.7813" y2="0.36321875" layer="21"/>
<rectangle x1="2.867659375" y1="0.358140625" x2="3.13181875" y2="0.36321875" layer="21"/>
<rectangle x1="3.46201875" y1="0.358140625" x2="3.685540625" y2="0.36321875" layer="21"/>
<rectangle x1="4.0767" y1="0.358140625" x2="4.3053" y2="0.36321875" layer="21"/>
<rectangle x1="4.86918125" y1="0.358140625" x2="5.153659375" y2="0.36321875" layer="21"/>
<rectangle x1="5.285740625" y1="0.358140625" x2="5.37718125" y2="0.36321875" layer="21"/>
<rectangle x1="5.70738125" y1="0.358140625" x2="5.96138125" y2="0.36321875" layer="21"/>
<rectangle x1="6.07821875" y1="0.358140625" x2="6.169659375" y2="0.36321875" layer="21"/>
<rectangle x1="6.4135" y1="0.358140625" x2="6.51001875" y2="0.36321875" layer="21"/>
<rectangle x1="6.753859375" y1="0.358140625" x2="6.8453" y2="0.36321875" layer="21"/>
<rectangle x1="7.241540625" y1="0.358140625" x2="7.338059375" y2="0.36321875" layer="21"/>
<rectangle x1="7.73938125" y1="0.358140625" x2="8.028940625" y2="0.36321875" layer="21"/>
<rectangle x1="0.13461875" y1="0.36321875" x2="0.556259375" y2="0.3683" layer="21"/>
<rectangle x1="0.886459375" y1="0.36321875" x2="1.115059375" y2="0.3683" layer="21"/>
<rectangle x1="1.267459375" y1="0.36321875" x2="1.49098125" y2="0.3683" layer="21"/>
<rectangle x1="1.7145" y1="0.36321875" x2="1.9431" y2="0.3683" layer="21"/>
<rectangle x1="2.16661875" y1="0.36321875" x2="2.390140625" y2="0.3683" layer="21"/>
<rectangle x1="2.5527" y1="0.36321875" x2="2.7813" y2="0.3683" layer="21"/>
<rectangle x1="2.85241875" y1="0.36321875" x2="3.147059375" y2="0.3683" layer="21"/>
<rectangle x1="3.4671" y1="0.36321875" x2="3.69061875" y2="0.3683" layer="21"/>
<rectangle x1="4.07161875" y1="0.36321875" x2="4.3053" y2="0.3683" layer="21"/>
<rectangle x1="4.85901875" y1="0.36321875" x2="5.16381875" y2="0.3683" layer="21"/>
<rectangle x1="5.285740625" y1="0.36321875" x2="5.37718125" y2="0.3683" layer="21"/>
<rectangle x1="5.69721875" y1="0.36321875" x2="5.971540625" y2="0.3683" layer="21"/>
<rectangle x1="6.07821875" y1="0.36321875" x2="6.174740625" y2="0.3683" layer="21"/>
<rectangle x1="6.4135" y1="0.36321875" x2="6.504940625" y2="0.3683" layer="21"/>
<rectangle x1="6.74878125" y1="0.36321875" x2="6.8453" y2="0.3683" layer="21"/>
<rectangle x1="7.24661875" y1="0.36321875" x2="7.338059375" y2="0.3683" layer="21"/>
<rectangle x1="7.72921875" y1="0.36321875" x2="8.0391" y2="0.3683" layer="21"/>
<rectangle x1="0.124459375" y1="0.3683" x2="0.56641875" y2="0.37338125" layer="21"/>
<rectangle x1="0.891540625" y1="0.3683" x2="1.115059375" y2="0.37338125" layer="21"/>
<rectangle x1="1.26238125" y1="0.3683" x2="1.49098125" y2="0.37338125" layer="21"/>
<rectangle x1="1.7145" y1="0.3683" x2="1.9431" y2="0.37338125" layer="21"/>
<rectangle x1="2.16661875" y1="0.3683" x2="2.390140625" y2="0.37338125" layer="21"/>
<rectangle x1="2.5527" y1="0.3683" x2="2.7813" y2="0.37338125" layer="21"/>
<rectangle x1="2.842259375" y1="0.3683" x2="3.152140625" y2="0.37338125" layer="21"/>
<rectangle x1="3.4671" y1="0.3683" x2="3.685540625" y2="0.37338125" layer="21"/>
<rectangle x1="4.07161875" y1="0.3683" x2="4.31038125" y2="0.37338125" layer="21"/>
<rectangle x1="4.84378125" y1="0.3683" x2="5.179059375" y2="0.37338125" layer="21"/>
<rectangle x1="5.285740625" y1="0.3683" x2="5.37718125" y2="0.37338125" layer="21"/>
<rectangle x1="5.687059375" y1="0.3683" x2="5.9817" y2="0.37338125" layer="21"/>
<rectangle x1="6.07821875" y1="0.3683" x2="6.174740625" y2="0.37338125" layer="21"/>
<rectangle x1="6.41858125" y1="0.3683" x2="6.51001875" y2="0.37338125" layer="21"/>
<rectangle x1="6.74878125" y1="0.3683" x2="6.8453" y2="0.37338125" layer="21"/>
<rectangle x1="7.241540625" y1="0.3683" x2="7.338059375" y2="0.37338125" layer="21"/>
<rectangle x1="7.71398125" y1="0.3683" x2="8.054340625" y2="0.37338125" layer="21"/>
<rectangle x1="0.1143" y1="0.37338125" x2="0.57658125" y2="0.378459375" layer="21"/>
<rectangle x1="0.886459375" y1="0.37338125" x2="1.10998125" y2="0.378459375" layer="21"/>
<rectangle x1="1.267459375" y1="0.37338125" x2="1.49098125" y2="0.378459375" layer="21"/>
<rectangle x1="1.7145" y1="0.37338125" x2="1.93801875" y2="0.378459375" layer="21"/>
<rectangle x1="2.16661875" y1="0.37338125" x2="2.390140625" y2="0.378459375" layer="21"/>
<rectangle x1="2.55778125" y1="0.37338125" x2="2.7813" y2="0.378459375" layer="21"/>
<rectangle x1="2.83718125" y1="0.37338125" x2="3.1623" y2="0.378459375" layer="21"/>
<rectangle x1="3.46201875" y1="0.37338125" x2="3.69061875" y2="0.378459375" layer="21"/>
<rectangle x1="4.066540625" y1="0.37338125" x2="4.31038125" y2="0.378459375" layer="21"/>
<rectangle x1="4.8387" y1="0.37338125" x2="5.184140625" y2="0.378459375" layer="21"/>
<rectangle x1="5.285740625" y1="0.37338125" x2="5.37718125" y2="0.378459375" layer="21"/>
<rectangle x1="5.68198125" y1="0.37338125" x2="5.98678125" y2="0.378459375" layer="21"/>
<rectangle x1="6.07821875" y1="0.37338125" x2="6.169659375" y2="0.378459375" layer="21"/>
<rectangle x1="6.4135" y1="0.37338125" x2="6.51001875" y2="0.378459375" layer="21"/>
<rectangle x1="6.753859375" y1="0.37338125" x2="6.84021875" y2="0.378459375" layer="21"/>
<rectangle x1="7.241540625" y1="0.37338125" x2="7.33298125" y2="0.378459375" layer="21"/>
<rectangle x1="7.7089" y1="0.37338125" x2="8.05941875" y2="0.378459375" layer="21"/>
<rectangle x1="0.104140625" y1="0.378459375" x2="0.586740625" y2="0.383540625" layer="21"/>
<rectangle x1="0.891540625" y1="0.378459375" x2="1.115059375" y2="0.383540625" layer="21"/>
<rectangle x1="1.267459375" y1="0.378459375" x2="1.49098125" y2="0.383540625" layer="21"/>
<rectangle x1="1.7145" y1="0.378459375" x2="1.93801875" y2="0.383540625" layer="21"/>
<rectangle x1="2.16661875" y1="0.378459375" x2="2.390140625" y2="0.383540625" layer="21"/>
<rectangle x1="2.55778125" y1="0.378459375" x2="2.7813" y2="0.383540625" layer="21"/>
<rectangle x1="2.82701875" y1="0.378459375" x2="3.16738125" y2="0.383540625" layer="21"/>
<rectangle x1="3.46201875" y1="0.378459375" x2="3.69061875" y2="0.383540625" layer="21"/>
<rectangle x1="4.066540625" y1="0.378459375" x2="4.31038125" y2="0.383540625" layer="21"/>
<rectangle x1="4.828540625" y1="0.378459375" x2="5.1943" y2="0.383540625" layer="21"/>
<rectangle x1="5.285740625" y1="0.378459375" x2="5.37718125" y2="0.383540625" layer="21"/>
<rectangle x1="5.6769" y1="0.378459375" x2="5.996940625" y2="0.383540625" layer="21"/>
<rectangle x1="6.07821875" y1="0.378459375" x2="6.174740625" y2="0.383540625" layer="21"/>
<rectangle x1="6.41858125" y1="0.378459375" x2="6.51001875" y2="0.383540625" layer="21"/>
<rectangle x1="6.74878125" y1="0.378459375" x2="6.8453" y2="0.383540625" layer="21"/>
<rectangle x1="7.24661875" y1="0.378459375" x2="7.338059375" y2="0.383540625" layer="21"/>
<rectangle x1="7.698740625" y1="0.378459375" x2="8.06958125" y2="0.383540625" layer="21"/>
<rectangle x1="0.09398125" y1="0.383540625" x2="0.5969" y2="0.38861875" layer="21"/>
<rectangle x1="0.886459375" y1="0.383540625" x2="1.10998125" y2="0.38861875" layer="21"/>
<rectangle x1="1.267459375" y1="0.383540625" x2="1.49098125" y2="0.38861875" layer="21"/>
<rectangle x1="1.7145" y1="0.383540625" x2="1.9431" y2="0.38861875" layer="21"/>
<rectangle x1="2.16661875" y1="0.383540625" x2="2.390140625" y2="0.38861875" layer="21"/>
<rectangle x1="2.55778125" y1="0.383540625" x2="2.7813" y2="0.38861875" layer="21"/>
<rectangle x1="2.821940625" y1="0.383540625" x2="3.177540625" y2="0.38861875" layer="21"/>
<rectangle x1="3.4671" y1="0.383540625" x2="3.69061875" y2="0.38861875" layer="21"/>
<rectangle x1="4.061459375" y1="0.383540625" x2="4.315459375" y2="0.38861875" layer="21"/>
<rectangle x1="4.81838125" y1="0.383540625" x2="5.204459375" y2="0.38861875" layer="21"/>
<rectangle x1="5.285740625" y1="0.383540625" x2="5.37718125" y2="0.38861875" layer="21"/>
<rectangle x1="5.666740625" y1="0.383540625" x2="6.00201875" y2="0.38861875" layer="21"/>
<rectangle x1="6.07821875" y1="0.383540625" x2="6.169659375" y2="0.38861875" layer="21"/>
<rectangle x1="6.4135" y1="0.383540625" x2="6.504940625" y2="0.38861875" layer="21"/>
<rectangle x1="6.753859375" y1="0.383540625" x2="6.8453" y2="0.38861875" layer="21"/>
<rectangle x1="7.241540625" y1="0.383540625" x2="7.338059375" y2="0.38861875" layer="21"/>
<rectangle x1="7.68858125" y1="0.383540625" x2="8.079740625" y2="0.38861875" layer="21"/>
<rectangle x1="0.08381875" y1="0.38861875" x2="0.60198125" y2="0.3937" layer="21"/>
<rectangle x1="0.891540625" y1="0.38861875" x2="1.115059375" y2="0.3937" layer="21"/>
<rectangle x1="1.267459375" y1="0.38861875" x2="1.49098125" y2="0.3937" layer="21"/>
<rectangle x1="1.71958125" y1="0.38861875" x2="1.9431" y2="0.3937" layer="21"/>
<rectangle x1="2.16661875" y1="0.38861875" x2="2.390140625" y2="0.3937" layer="21"/>
<rectangle x1="2.5527" y1="0.38861875" x2="2.7813" y2="0.3937" layer="21"/>
<rectangle x1="2.816859375" y1="0.38861875" x2="3.1877" y2="0.3937" layer="21"/>
<rectangle x1="3.4671" y1="0.38861875" x2="3.685540625" y2="0.3937" layer="21"/>
<rectangle x1="4.061459375" y1="0.38861875" x2="4.315459375" y2="0.3937" layer="21"/>
<rectangle x1="4.80821875" y1="0.38861875" x2="5.209540625" y2="0.3937" layer="21"/>
<rectangle x1="5.285740625" y1="0.38861875" x2="5.37718125" y2="0.3937" layer="21"/>
<rectangle x1="5.661659375" y1="0.38861875" x2="6.0071" y2="0.3937" layer="21"/>
<rectangle x1="6.07821875" y1="0.38861875" x2="6.174740625" y2="0.3937" layer="21"/>
<rectangle x1="6.4135" y1="0.38861875" x2="6.504940625" y2="0.3937" layer="21"/>
<rectangle x1="6.753859375" y1="0.38861875" x2="6.8453" y2="0.3937" layer="21"/>
<rectangle x1="7.24661875" y1="0.38861875" x2="7.338059375" y2="0.3937" layer="21"/>
<rectangle x1="7.67841875" y1="0.38861875" x2="8.0899" y2="0.3937" layer="21"/>
<rectangle x1="0.073659375" y1="0.3937" x2="0.612140625" y2="0.39878125" layer="21"/>
<rectangle x1="0.886459375" y1="0.3937" x2="1.115059375" y2="0.39878125" layer="21"/>
<rectangle x1="1.26238125" y1="0.3937" x2="1.49098125" y2="0.39878125" layer="21"/>
<rectangle x1="1.7145" y1="0.3937" x2="1.93801875" y2="0.39878125" layer="21"/>
<rectangle x1="2.16661875" y1="0.3937" x2="2.390140625" y2="0.39878125" layer="21"/>
<rectangle x1="2.55778125" y1="0.3937" x2="2.7813" y2="0.39878125" layer="21"/>
<rectangle x1="2.8067" y1="0.3937" x2="3.19278125" y2="0.39878125" layer="21"/>
<rectangle x1="3.46201875" y1="0.3937" x2="3.69061875" y2="0.39878125" layer="21"/>
<rectangle x1="4.061459375" y1="0.3937" x2="4.320540625" y2="0.39878125" layer="21"/>
<rectangle x1="4.803140625" y1="0.3937" x2="5.2197" y2="0.39878125" layer="21"/>
<rectangle x1="5.285740625" y1="0.3937" x2="5.37718125" y2="0.39878125" layer="21"/>
<rectangle x1="5.65658125" y1="0.3937" x2="6.01218125" y2="0.39878125" layer="21"/>
<rectangle x1="6.07821875" y1="0.3937" x2="6.169659375" y2="0.39878125" layer="21"/>
<rectangle x1="6.41858125" y1="0.3937" x2="6.51001875" y2="0.39878125" layer="21"/>
<rectangle x1="6.74878125" y1="0.3937" x2="6.8453" y2="0.39878125" layer="21"/>
<rectangle x1="7.241540625" y1="0.3937" x2="7.33298125" y2="0.39878125" layer="21"/>
<rectangle x1="7.673340625" y1="0.3937" x2="8.09498125" y2="0.39878125" layer="21"/>
<rectangle x1="0.06858125" y1="0.39878125" x2="0.61721875" y2="0.403859375" layer="21"/>
<rectangle x1="0.891540625" y1="0.39878125" x2="1.10998125" y2="0.403859375" layer="21"/>
<rectangle x1="1.267459375" y1="0.39878125" x2="1.49098125" y2="0.403859375" layer="21"/>
<rectangle x1="1.7145" y1="0.39878125" x2="1.93801875" y2="0.403859375" layer="21"/>
<rectangle x1="2.16661875" y1="0.39878125" x2="2.390140625" y2="0.403859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.39878125" x2="2.7813" y2="0.403859375" layer="21"/>
<rectangle x1="2.80161875" y1="0.39878125" x2="3.197859375" y2="0.403859375" layer="21"/>
<rectangle x1="3.4671" y1="0.39878125" x2="3.685540625" y2="0.403859375" layer="21"/>
<rectangle x1="4.05638125" y1="0.39878125" x2="4.320540625" y2="0.403859375" layer="21"/>
<rectangle x1="4.79298125" y1="0.39878125" x2="5.22478125" y2="0.403859375" layer="21"/>
<rectangle x1="5.285740625" y1="0.39878125" x2="5.37718125" y2="0.403859375" layer="21"/>
<rectangle x1="5.6515" y1="0.39878125" x2="6.022340625" y2="0.403859375" layer="21"/>
<rectangle x1="6.07821875" y1="0.39878125" x2="6.169659375" y2="0.403859375" layer="21"/>
<rectangle x1="6.4135" y1="0.39878125" x2="6.51001875" y2="0.403859375" layer="21"/>
<rectangle x1="6.74878125" y1="0.39878125" x2="6.8453" y2="0.403859375" layer="21"/>
<rectangle x1="7.24661875" y1="0.39878125" x2="7.338059375" y2="0.403859375" layer="21"/>
<rectangle x1="7.668259375" y1="0.39878125" x2="8.100059375" y2="0.403859375" layer="21"/>
<rectangle x1="0.0635" y1="0.403859375" x2="0.62738125" y2="0.408940625" layer="21"/>
<rectangle x1="0.886459375" y1="0.403859375" x2="1.115059375" y2="0.408940625" layer="21"/>
<rectangle x1="1.267459375" y1="0.403859375" x2="1.49098125" y2="0.408940625" layer="21"/>
<rectangle x1="1.7145" y1="0.403859375" x2="1.93801875" y2="0.408940625" layer="21"/>
<rectangle x1="2.16661875" y1="0.403859375" x2="2.390140625" y2="0.408940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.403859375" x2="2.7813" y2="0.408940625" layer="21"/>
<rectangle x1="2.796540625" y1="0.403859375" x2="3.202940625" y2="0.408940625" layer="21"/>
<rectangle x1="3.46201875" y1="0.403859375" x2="3.69061875" y2="0.408940625" layer="21"/>
<rectangle x1="4.05638125" y1="0.403859375" x2="4.320540625" y2="0.408940625" layer="21"/>
<rectangle x1="4.7879" y1="0.403859375" x2="5.229859375" y2="0.408940625" layer="21"/>
<rectangle x1="5.285740625" y1="0.403859375" x2="5.37718125" y2="0.408940625" layer="21"/>
<rectangle x1="5.64641875" y1="0.403859375" x2="6.02741875" y2="0.408940625" layer="21"/>
<rectangle x1="6.07821875" y1="0.403859375" x2="6.169659375" y2="0.408940625" layer="21"/>
<rectangle x1="6.41858125" y1="0.403859375" x2="6.504940625" y2="0.408940625" layer="21"/>
<rectangle x1="6.753859375" y1="0.403859375" x2="6.8453" y2="0.408940625" layer="21"/>
<rectangle x1="7.241540625" y1="0.403859375" x2="7.338059375" y2="0.408940625" layer="21"/>
<rectangle x1="7.6581" y1="0.403859375" x2="8.11021875" y2="0.408940625" layer="21"/>
<rectangle x1="0.053340625" y1="0.408940625" x2="0.632459375" y2="0.41401875" layer="21"/>
<rectangle x1="0.886459375" y1="0.408940625" x2="1.10998125" y2="0.41401875" layer="21"/>
<rectangle x1="1.26238125" y1="0.408940625" x2="1.49098125" y2="0.41401875" layer="21"/>
<rectangle x1="1.7145" y1="0.408940625" x2="1.9431" y2="0.41401875" layer="21"/>
<rectangle x1="2.161540625" y1="0.408940625" x2="2.390140625" y2="0.41401875" layer="21"/>
<rectangle x1="2.55778125" y1="0.408940625" x2="2.7813" y2="0.41401875" layer="21"/>
<rectangle x1="2.791459375" y1="0.408940625" x2="3.2131" y2="0.41401875" layer="21"/>
<rectangle x1="3.4671" y1="0.408940625" x2="3.685540625" y2="0.41401875" layer="21"/>
<rectangle x1="4.0513" y1="0.408940625" x2="4.32561875" y2="0.41401875" layer="21"/>
<rectangle x1="4.78281875" y1="0.408940625" x2="5.24001875" y2="0.41401875" layer="21"/>
<rectangle x1="5.285740625" y1="0.408940625" x2="5.37718125" y2="0.41401875" layer="21"/>
<rectangle x1="5.641340625" y1="0.408940625" x2="6.0325" y2="0.41401875" layer="21"/>
<rectangle x1="6.07821875" y1="0.408940625" x2="6.169659375" y2="0.41401875" layer="21"/>
<rectangle x1="6.41858125" y1="0.408940625" x2="6.51001875" y2="0.41401875" layer="21"/>
<rectangle x1="6.74878125" y1="0.408940625" x2="6.8453" y2="0.41401875" layer="21"/>
<rectangle x1="7.24661875" y1="0.408940625" x2="7.338059375" y2="0.41401875" layer="21"/>
<rectangle x1="7.65301875" y1="0.408940625" x2="8.1153" y2="0.41401875" layer="21"/>
<rectangle x1="0.048259375" y1="0.41401875" x2="0.637540625" y2="0.4191" layer="21"/>
<rectangle x1="0.891540625" y1="0.41401875" x2="1.115059375" y2="0.4191" layer="21"/>
<rectangle x1="1.267459375" y1="0.41401875" x2="1.49098125" y2="0.4191" layer="21"/>
<rectangle x1="1.7145" y1="0.41401875" x2="1.93801875" y2="0.4191" layer="21"/>
<rectangle x1="2.16661875" y1="0.41401875" x2="2.390140625" y2="0.4191" layer="21"/>
<rectangle x1="2.55778125" y1="0.41401875" x2="2.7813" y2="0.4191" layer="21"/>
<rectangle x1="2.791459375" y1="0.41401875" x2="3.2131" y2="0.4191" layer="21"/>
<rectangle x1="3.46201875" y1="0.41401875" x2="3.69061875" y2="0.4191" layer="21"/>
<rectangle x1="4.0513" y1="0.41401875" x2="4.32561875" y2="0.4191" layer="21"/>
<rectangle x1="4.777740625" y1="0.41401875" x2="5.2451" y2="0.4191" layer="21"/>
<rectangle x1="5.285740625" y1="0.41401875" x2="5.37718125" y2="0.4191" layer="21"/>
<rectangle x1="5.636259375" y1="0.41401875" x2="6.03758125" y2="0.4191" layer="21"/>
<rectangle x1="6.07821875" y1="0.41401875" x2="6.169659375" y2="0.4191" layer="21"/>
<rectangle x1="6.4135" y1="0.41401875" x2="6.504940625" y2="0.4191" layer="21"/>
<rectangle x1="6.753859375" y1="0.41401875" x2="6.8453" y2="0.4191" layer="21"/>
<rectangle x1="7.241540625" y1="0.41401875" x2="7.338059375" y2="0.4191" layer="21"/>
<rectangle x1="7.647940625" y1="0.41401875" x2="8.12038125" y2="0.4191" layer="21"/>
<rectangle x1="0.0381" y1="0.4191" x2="0.6477" y2="0.42418125" layer="21"/>
<rectangle x1="0.886459375" y1="0.4191" x2="1.115059375" y2="0.42418125" layer="21"/>
<rectangle x1="1.267459375" y1="0.4191" x2="1.49098125" y2="0.42418125" layer="21"/>
<rectangle x1="1.7145" y1="0.4191" x2="1.9431" y2="0.42418125" layer="21"/>
<rectangle x1="2.16661875" y1="0.4191" x2="2.390140625" y2="0.42418125" layer="21"/>
<rectangle x1="2.55778125" y1="0.4191" x2="2.7813" y2="0.42418125" layer="21"/>
<rectangle x1="2.78638125" y1="0.4191" x2="3.223259375" y2="0.42418125" layer="21"/>
<rectangle x1="3.4671" y1="0.4191" x2="3.685540625" y2="0.42418125" layer="21"/>
<rectangle x1="4.04621875" y1="0.4191" x2="4.3307" y2="0.42418125" layer="21"/>
<rectangle x1="4.772659375" y1="0.4191" x2="4.97078125" y2="0.42418125" layer="21"/>
<rectangle x1="5.057140625" y1="0.4191" x2="5.25018125" y2="0.42418125" layer="21"/>
<rectangle x1="5.285740625" y1="0.4191" x2="5.37718125" y2="0.42418125" layer="21"/>
<rectangle x1="5.63118125" y1="0.4191" x2="5.8039" y2="0.42418125" layer="21"/>
<rectangle x1="5.8801" y1="0.4191" x2="6.042659375" y2="0.42418125" layer="21"/>
<rectangle x1="6.07821875" y1="0.4191" x2="6.169659375" y2="0.42418125" layer="21"/>
<rectangle x1="6.41858125" y1="0.4191" x2="6.51001875" y2="0.42418125" layer="21"/>
<rectangle x1="6.753859375" y1="0.4191" x2="6.8453" y2="0.42418125" layer="21"/>
<rectangle x1="7.24661875" y1="0.4191" x2="7.33298125" y2="0.42418125" layer="21"/>
<rectangle x1="7.63778125" y1="0.4191" x2="7.8359" y2="0.42418125" layer="21"/>
<rectangle x1="7.84098125" y1="0.4191" x2="7.846059375" y2="0.42418125" layer="21"/>
<rectangle x1="7.927340625" y1="0.4191" x2="8.125459375" y2="0.42418125" layer="21"/>
<rectangle x1="0.03301875" y1="0.42418125" x2="0.65278125" y2="0.429259375" layer="21"/>
<rectangle x1="0.886459375" y1="0.42418125" x2="1.10998125" y2="0.429259375" layer="21"/>
<rectangle x1="1.267459375" y1="0.42418125" x2="1.49098125" y2="0.429259375" layer="21"/>
<rectangle x1="1.71958125" y1="0.42418125" x2="1.93801875" y2="0.429259375" layer="21"/>
<rectangle x1="2.16661875" y1="0.42418125" x2="2.390140625" y2="0.429259375" layer="21"/>
<rectangle x1="2.5527" y1="0.42418125" x2="3.228340625" y2="0.429259375" layer="21"/>
<rectangle x1="3.46201875" y1="0.42418125" x2="3.69061875" y2="0.429259375" layer="21"/>
<rectangle x1="4.04621875" y1="0.42418125" x2="4.3307" y2="0.429259375" layer="21"/>
<rectangle x1="4.7625" y1="0.42418125" x2="4.94538125" y2="0.429259375" layer="21"/>
<rectangle x1="5.082540625" y1="0.42418125" x2="5.25018125" y2="0.429259375" layer="21"/>
<rectangle x1="5.285740625" y1="0.42418125" x2="5.37718125" y2="0.429259375" layer="21"/>
<rectangle x1="5.63118125" y1="0.42418125" x2="5.78358125" y2="0.429259375" layer="21"/>
<rectangle x1="5.9055" y1="0.42418125" x2="6.047740625" y2="0.429259375" layer="21"/>
<rectangle x1="6.07821875" y1="0.42418125" x2="6.169659375" y2="0.429259375" layer="21"/>
<rectangle x1="6.4135" y1="0.42418125" x2="6.504940625" y2="0.429259375" layer="21"/>
<rectangle x1="6.74878125" y1="0.42418125" x2="6.8453" y2="0.429259375" layer="21"/>
<rectangle x1="7.241540625" y1="0.42418125" x2="7.338059375" y2="0.429259375" layer="21"/>
<rectangle x1="7.6327" y1="0.42418125" x2="7.81558125" y2="0.429259375" layer="21"/>
<rectangle x1="7.952740625" y1="0.42418125" x2="8.13561875" y2="0.429259375" layer="21"/>
<rectangle x1="0.027940625" y1="0.429259375" x2="0.657859375" y2="0.434340625" layer="21"/>
<rectangle x1="0.886459375" y1="0.429259375" x2="1.115059375" y2="0.434340625" layer="21"/>
<rectangle x1="1.267459375" y1="0.429259375" x2="1.49098125" y2="0.434340625" layer="21"/>
<rectangle x1="1.7145" y1="0.429259375" x2="1.9431" y2="0.434340625" layer="21"/>
<rectangle x1="2.16661875" y1="0.429259375" x2="2.390140625" y2="0.434340625" layer="21"/>
<rectangle x1="2.55778125" y1="0.429259375" x2="3.23341875" y2="0.434340625" layer="21"/>
<rectangle x1="3.46201875" y1="0.429259375" x2="3.69061875" y2="0.434340625" layer="21"/>
<rectangle x1="4.04621875" y1="0.429259375" x2="4.3307" y2="0.434340625" layer="21"/>
<rectangle x1="4.75741875" y1="0.429259375" x2="4.930140625" y2="0.434340625" layer="21"/>
<rectangle x1="5.102859375" y1="0.429259375" x2="5.260340625" y2="0.434340625" layer="21"/>
<rectangle x1="5.285740625" y1="0.429259375" x2="5.37718125" y2="0.434340625" layer="21"/>
<rectangle x1="5.6261" y1="0.429259375" x2="5.768340625" y2="0.434340625" layer="21"/>
<rectangle x1="5.920740625" y1="0.429259375" x2="6.047740625" y2="0.434340625" layer="21"/>
<rectangle x1="6.07821875" y1="0.429259375" x2="6.169659375" y2="0.434340625" layer="21"/>
<rectangle x1="6.41858125" y1="0.429259375" x2="6.51001875" y2="0.434340625" layer="21"/>
<rectangle x1="6.74878125" y1="0.429259375" x2="6.8453" y2="0.434340625" layer="21"/>
<rectangle x1="7.241540625" y1="0.429259375" x2="7.338059375" y2="0.434340625" layer="21"/>
<rectangle x1="7.62761875" y1="0.429259375" x2="7.800340625" y2="0.434340625" layer="21"/>
<rectangle x1="7.973059375" y1="0.429259375" x2="8.13561875" y2="0.434340625" layer="21"/>
<rectangle x1="0.022859375" y1="0.434340625" x2="0.662940625" y2="0.43941875" layer="21"/>
<rectangle x1="0.891540625" y1="0.434340625" x2="1.10998125" y2="0.43941875" layer="21"/>
<rectangle x1="1.26238125" y1="0.434340625" x2="1.49098125" y2="0.43941875" layer="21"/>
<rectangle x1="1.7145" y1="0.434340625" x2="1.93801875" y2="0.43941875" layer="21"/>
<rectangle x1="2.16661875" y1="0.434340625" x2="2.390140625" y2="0.43941875" layer="21"/>
<rectangle x1="2.55778125" y1="0.434340625" x2="3.23341875" y2="0.43941875" layer="21"/>
<rectangle x1="3.4671" y1="0.434340625" x2="3.69061875" y2="0.43941875" layer="21"/>
<rectangle x1="4.041140625" y1="0.434340625" x2="4.33578125" y2="0.43941875" layer="21"/>
<rectangle x1="4.75741875" y1="0.434340625" x2="4.9149" y2="0.43941875" layer="21"/>
<rectangle x1="5.11301875" y1="0.434340625" x2="5.26541875" y2="0.43941875" layer="21"/>
<rectangle x1="5.285740625" y1="0.434340625" x2="5.37718125" y2="0.43941875" layer="21"/>
<rectangle x1="5.62101875" y1="0.434340625" x2="5.75818125" y2="0.43941875" layer="21"/>
<rectangle x1="5.93598125" y1="0.434340625" x2="6.05281875" y2="0.43941875" layer="21"/>
<rectangle x1="6.07821875" y1="0.434340625" x2="6.169659375" y2="0.43941875" layer="21"/>
<rectangle x1="6.4135" y1="0.434340625" x2="6.51001875" y2="0.43941875" layer="21"/>
<rectangle x1="6.753859375" y1="0.434340625" x2="6.84021875" y2="0.43941875" layer="21"/>
<rectangle x1="7.24661875" y1="0.434340625" x2="7.33298125" y2="0.43941875" layer="21"/>
<rectangle x1="7.62761875" y1="0.434340625" x2="7.78001875" y2="0.43941875" layer="21"/>
<rectangle x1="7.98321875" y1="0.434340625" x2="8.14578125" y2="0.43941875" layer="21"/>
<rectangle x1="0.0127" y1="0.43941875" x2="0.66801875" y2="0.4445" layer="21"/>
<rectangle x1="0.886459375" y1="0.43941875" x2="1.115059375" y2="0.4445" layer="21"/>
<rectangle x1="1.267459375" y1="0.43941875" x2="1.49098125" y2="0.4445" layer="21"/>
<rectangle x1="1.7145" y1="0.43941875" x2="1.93801875" y2="0.4445" layer="21"/>
<rectangle x1="2.16661875" y1="0.43941875" x2="2.390140625" y2="0.4445" layer="21"/>
<rectangle x1="2.55778125" y1="0.43941875" x2="3.2385" y2="0.4445" layer="21"/>
<rectangle x1="3.4671" y1="0.43941875" x2="3.685540625" y2="0.4445" layer="21"/>
<rectangle x1="4.036059375" y1="0.43941875" x2="4.33578125" y2="0.4445" layer="21"/>
<rectangle x1="4.752340625" y1="0.43941875" x2="4.899659375" y2="0.4445" layer="21"/>
<rectangle x1="5.128259375" y1="0.43941875" x2="5.26541875" y2="0.4445" layer="21"/>
<rectangle x1="5.285740625" y1="0.43941875" x2="5.37718125" y2="0.4445" layer="21"/>
<rectangle x1="5.62101875" y1="0.43941875" x2="5.74801875" y2="0.4445" layer="21"/>
<rectangle x1="5.946140625" y1="0.43941875" x2="6.0579" y2="0.4445" layer="21"/>
<rectangle x1="6.07821875" y1="0.43941875" x2="6.169659375" y2="0.4445" layer="21"/>
<rectangle x1="6.41858125" y1="0.43941875" x2="6.51001875" y2="0.4445" layer="21"/>
<rectangle x1="6.74878125" y1="0.43941875" x2="6.8453" y2="0.4445" layer="21"/>
<rectangle x1="7.24661875" y1="0.43941875" x2="7.338059375" y2="0.4445" layer="21"/>
<rectangle x1="7.617459375" y1="0.43941875" x2="7.769859375" y2="0.4445" layer="21"/>
<rectangle x1="7.998459375" y1="0.43941875" x2="8.14578125" y2="0.4445" layer="21"/>
<rectangle x1="0.00761875" y1="0.4445" x2="0.66801875" y2="0.44958125" layer="21"/>
<rectangle x1="0.891540625" y1="0.4445" x2="1.115059375" y2="0.44958125" layer="21"/>
<rectangle x1="1.267459375" y1="0.4445" x2="1.49098125" y2="0.44958125" layer="21"/>
<rectangle x1="1.7145" y1="0.4445" x2="1.9431" y2="0.44958125" layer="21"/>
<rectangle x1="2.16661875" y1="0.4445" x2="2.390140625" y2="0.44958125" layer="21"/>
<rectangle x1="2.5527" y1="0.4445" x2="3.24358125" y2="0.44958125" layer="21"/>
<rectangle x1="3.46201875" y1="0.4445" x2="3.69061875" y2="0.44958125" layer="21"/>
<rectangle x1="4.036059375" y1="0.4445" x2="4.33578125" y2="0.44958125" layer="21"/>
<rectangle x1="4.74218125" y1="0.4445" x2="4.8895" y2="0.44958125" layer="21"/>
<rectangle x1="5.13841875" y1="0.4445" x2="5.2705" y2="0.44958125" layer="21"/>
<rectangle x1="5.285740625" y1="0.4445" x2="5.37718125" y2="0.44958125" layer="21"/>
<rectangle x1="5.615940625" y1="0.4445" x2="5.742940625" y2="0.44958125" layer="21"/>
<rectangle x1="5.9563" y1="0.4445" x2="6.06298125" y2="0.44958125" layer="21"/>
<rectangle x1="6.07821875" y1="0.4445" x2="6.169659375" y2="0.44958125" layer="21"/>
<rectangle x1="6.4135" y1="0.4445" x2="6.504940625" y2="0.44958125" layer="21"/>
<rectangle x1="6.753859375" y1="0.4445" x2="6.8453" y2="0.44958125" layer="21"/>
<rectangle x1="7.241540625" y1="0.4445" x2="7.338059375" y2="0.44958125" layer="21"/>
<rectangle x1="7.617459375" y1="0.4445" x2="7.7597" y2="0.44958125" layer="21"/>
<rectangle x1="8.00861875" y1="0.4445" x2="8.155940625" y2="0.44958125" layer="21"/>
<rectangle x1="0.002540625" y1="0.44958125" x2="0.67818125" y2="0.454659375" layer="21"/>
<rectangle x1="0.886459375" y1="0.44958125" x2="1.115059375" y2="0.454659375" layer="21"/>
<rectangle x1="1.26238125" y1="0.44958125" x2="1.49098125" y2="0.454659375" layer="21"/>
<rectangle x1="1.71958125" y1="0.44958125" x2="1.9431" y2="0.454659375" layer="21"/>
<rectangle x1="2.16661875" y1="0.44958125" x2="2.390140625" y2="0.454659375" layer="21"/>
<rectangle x1="2.55778125" y1="0.44958125" x2="3.248659375" y2="0.454659375" layer="21"/>
<rectangle x1="3.4671" y1="0.44958125" x2="3.685540625" y2="0.454659375" layer="21"/>
<rectangle x1="4.036059375" y1="0.44958125" x2="4.340859375" y2="0.454659375" layer="21"/>
<rectangle x1="4.74218125" y1="0.44958125" x2="4.879340625" y2="0.454659375" layer="21"/>
<rectangle x1="5.14858125" y1="0.44958125" x2="5.27558125" y2="0.454659375" layer="21"/>
<rectangle x1="5.285740625" y1="0.44958125" x2="5.37718125" y2="0.454659375" layer="21"/>
<rectangle x1="5.615940625" y1="0.44958125" x2="5.73278125" y2="0.454659375" layer="21"/>
<rectangle x1="5.966459375" y1="0.44958125" x2="6.06298125" y2="0.454659375" layer="21"/>
<rectangle x1="6.07821875" y1="0.44958125" x2="6.169659375" y2="0.454659375" layer="21"/>
<rectangle x1="6.41858125" y1="0.44958125" x2="6.504940625" y2="0.454659375" layer="21"/>
<rectangle x1="6.753859375" y1="0.44958125" x2="6.8453" y2="0.454659375" layer="21"/>
<rectangle x1="7.24661875" y1="0.44958125" x2="7.338059375" y2="0.454659375" layer="21"/>
<rectangle x1="7.61238125" y1="0.44958125" x2="7.749540625" y2="0.454659375" layer="21"/>
<rectangle x1="8.01878125" y1="0.44958125" x2="8.16101875" y2="0.454659375" layer="21"/>
<rectangle x1="-0.002540625" y1="0.454659375" x2="0.67818125" y2="0.459740625" layer="21"/>
<rectangle x1="0.886459375" y1="0.454659375" x2="1.10998125" y2="0.459740625" layer="21"/>
<rectangle x1="1.267459375" y1="0.454659375" x2="1.49098125" y2="0.459740625" layer="21"/>
<rectangle x1="1.7145" y1="0.454659375" x2="1.93801875" y2="0.459740625" layer="21"/>
<rectangle x1="2.16661875" y1="0.454659375" x2="2.390140625" y2="0.459740625" layer="21"/>
<rectangle x1="2.55778125" y1="0.454659375" x2="3.253740625" y2="0.459740625" layer="21"/>
<rectangle x1="3.46201875" y1="0.454659375" x2="3.69061875" y2="0.459740625" layer="21"/>
<rectangle x1="4.03098125" y1="0.454659375" x2="4.340859375" y2="0.459740625" layer="21"/>
<rectangle x1="4.7371" y1="0.454659375" x2="4.874259375" y2="0.459740625" layer="21"/>
<rectangle x1="5.158740625" y1="0.454659375" x2="5.27558125" y2="0.459740625" layer="21"/>
<rectangle x1="5.285740625" y1="0.454659375" x2="5.37718125" y2="0.459740625" layer="21"/>
<rectangle x1="5.610859375" y1="0.454659375" x2="5.73278125" y2="0.459740625" layer="21"/>
<rectangle x1="5.971540625" y1="0.454659375" x2="6.068059375" y2="0.459740625" layer="21"/>
<rectangle x1="6.07821875" y1="0.454659375" x2="6.169659375" y2="0.459740625" layer="21"/>
<rectangle x1="6.4135" y1="0.454659375" x2="6.51001875" y2="0.459740625" layer="21"/>
<rectangle x1="6.74878125" y1="0.454659375" x2="6.8453" y2="0.459740625" layer="21"/>
<rectangle x1="7.241540625" y1="0.454659375" x2="7.338059375" y2="0.459740625" layer="21"/>
<rectangle x1="7.6073" y1="0.454659375" x2="7.744459375" y2="0.459740625" layer="21"/>
<rectangle x1="8.028940625" y1="0.454659375" x2="8.16101875" y2="0.459740625" layer="21"/>
<rectangle x1="0.002540625" y1="0.459740625" x2="0.683259375" y2="0.46481875" layer="21"/>
<rectangle x1="0.886459375" y1="0.459740625" x2="1.115059375" y2="0.46481875" layer="21"/>
<rectangle x1="1.267459375" y1="0.459740625" x2="1.49098125" y2="0.46481875" layer="21"/>
<rectangle x1="1.7145" y1="0.459740625" x2="1.93801875" y2="0.46481875" layer="21"/>
<rectangle x1="2.161540625" y1="0.459740625" x2="2.390140625" y2="0.46481875" layer="21"/>
<rectangle x1="2.55778125" y1="0.459740625" x2="3.25881875" y2="0.46481875" layer="21"/>
<rectangle x1="3.46201875" y1="0.459740625" x2="3.69061875" y2="0.46481875" layer="21"/>
<rectangle x1="4.03098125" y1="0.459740625" x2="4.345940625" y2="0.46481875" layer="21"/>
<rectangle x1="4.73201875" y1="0.459740625" x2="4.8641" y2="0.46481875" layer="21"/>
<rectangle x1="5.16381875" y1="0.459740625" x2="5.280659375" y2="0.46481875" layer="21"/>
<rectangle x1="5.285740625" y1="0.459740625" x2="5.37718125" y2="0.46481875" layer="21"/>
<rectangle x1="5.610859375" y1="0.459740625" x2="5.72261875" y2="0.46481875" layer="21"/>
<rectangle x1="5.97661875" y1="0.459740625" x2="6.068059375" y2="0.46481875" layer="21"/>
<rectangle x1="6.073140625" y1="0.459740625" x2="6.169659375" y2="0.46481875" layer="21"/>
<rectangle x1="6.41858125" y1="0.459740625" x2="6.51001875" y2="0.46481875" layer="21"/>
<rectangle x1="6.74878125" y1="0.459740625" x2="6.8453" y2="0.46481875" layer="21"/>
<rectangle x1="7.241540625" y1="0.459740625" x2="7.33298125" y2="0.46481875" layer="21"/>
<rectangle x1="7.60221875" y1="0.459740625" x2="7.7343" y2="0.46481875" layer="21"/>
<rectangle x1="8.03401875" y1="0.459740625" x2="8.1661" y2="0.46481875" layer="21"/>
<rectangle x1="0.002540625" y1="0.46481875" x2="0.688340625" y2="0.4699" layer="21"/>
<rectangle x1="0.891540625" y1="0.46481875" x2="1.115059375" y2="0.4699" layer="21"/>
<rectangle x1="1.267459375" y1="0.46481875" x2="1.49098125" y2="0.4699" layer="21"/>
<rectangle x1="1.7145" y1="0.46481875" x2="1.93801875" y2="0.4699" layer="21"/>
<rectangle x1="2.16661875" y1="0.46481875" x2="2.390140625" y2="0.4699" layer="21"/>
<rectangle x1="2.5527" y1="0.46481875" x2="3.25881875" y2="0.4699" layer="21"/>
<rectangle x1="3.4671" y1="0.46481875" x2="3.69061875" y2="0.4699" layer="21"/>
<rectangle x1="4.0259" y1="0.46481875" x2="4.345940625" y2="0.4699" layer="21"/>
<rectangle x1="4.726940625" y1="0.46481875" x2="4.853940625" y2="0.4699" layer="21"/>
<rectangle x1="5.17398125" y1="0.46481875" x2="5.37718125" y2="0.4699" layer="21"/>
<rectangle x1="5.60578125" y1="0.46481875" x2="5.717540625" y2="0.4699" layer="21"/>
<rectangle x1="5.98678125" y1="0.46481875" x2="6.073140625" y2="0.4699" layer="21"/>
<rectangle x1="6.07821875" y1="0.46481875" x2="6.169659375" y2="0.4699" layer="21"/>
<rectangle x1="6.4135" y1="0.46481875" x2="6.504940625" y2="0.4699" layer="21"/>
<rectangle x1="6.753859375" y1="0.46481875" x2="6.8453" y2="0.4699" layer="21"/>
<rectangle x1="7.24661875" y1="0.46481875" x2="7.338059375" y2="0.4699" layer="21"/>
<rectangle x1="7.597140625" y1="0.46481875" x2="7.72921875" y2="0.4699" layer="21"/>
<rectangle x1="8.0391" y1="0.46481875" x2="8.17118125" y2="0.4699" layer="21"/>
<rectangle x1="0.00761875" y1="0.4699" x2="0.69341875" y2="0.47498125" layer="21"/>
<rectangle x1="0.886459375" y1="0.4699" x2="1.10998125" y2="0.47498125" layer="21"/>
<rectangle x1="1.267459375" y1="0.4699" x2="1.49098125" y2="0.47498125" layer="21"/>
<rectangle x1="1.7145" y1="0.4699" x2="1.9431" y2="0.47498125" layer="21"/>
<rectangle x1="2.16661875" y1="0.4699" x2="2.390140625" y2="0.47498125" layer="21"/>
<rectangle x1="2.55778125" y1="0.4699" x2="3.2639" y2="0.47498125" layer="21"/>
<rectangle x1="3.4671" y1="0.4699" x2="3.685540625" y2="0.47498125" layer="21"/>
<rectangle x1="4.0259" y1="0.4699" x2="4.35101875" y2="0.47498125" layer="21"/>
<rectangle x1="4.726940625" y1="0.4699" x2="4.848859375" y2="0.47498125" layer="21"/>
<rectangle x1="5.179059375" y1="0.4699" x2="5.37718125" y2="0.47498125" layer="21"/>
<rectangle x1="5.60578125" y1="0.4699" x2="5.717540625" y2="0.47498125" layer="21"/>
<rectangle x1="5.991859375" y1="0.4699" x2="6.169659375" y2="0.47498125" layer="21"/>
<rectangle x1="6.41858125" y1="0.4699" x2="6.51001875" y2="0.47498125" layer="21"/>
<rectangle x1="6.74878125" y1="0.4699" x2="6.8453" y2="0.47498125" layer="21"/>
<rectangle x1="7.24661875" y1="0.4699" x2="7.338059375" y2="0.47498125" layer="21"/>
<rectangle x1="7.597140625" y1="0.4699" x2="7.719059375" y2="0.47498125" layer="21"/>
<rectangle x1="8.049259375" y1="0.4699" x2="8.176259375" y2="0.47498125" layer="21"/>
<rectangle x1="0.0127" y1="0.47498125" x2="0.69341875" y2="0.480059375" layer="21"/>
<rectangle x1="0.891540625" y1="0.47498125" x2="1.115059375" y2="0.480059375" layer="21"/>
<rectangle x1="1.26238125" y1="0.47498125" x2="1.49098125" y2="0.480059375" layer="21"/>
<rectangle x1="1.7145" y1="0.47498125" x2="1.93801875" y2="0.480059375" layer="21"/>
<rectangle x1="2.16661875" y1="0.47498125" x2="2.390140625" y2="0.480059375" layer="21"/>
<rectangle x1="2.55778125" y1="0.47498125" x2="3.26898125" y2="0.480059375" layer="21"/>
<rectangle x1="3.46201875" y1="0.47498125" x2="3.69061875" y2="0.480059375" layer="21"/>
<rectangle x1="4.0259" y1="0.47498125" x2="4.35101875" y2="0.480059375" layer="21"/>
<rectangle x1="4.721859375" y1="0.47498125" x2="4.84378125" y2="0.480059375" layer="21"/>
<rectangle x1="5.18921875" y1="0.47498125" x2="5.37718125" y2="0.480059375" layer="21"/>
<rectangle x1="5.6007" y1="0.47498125" x2="5.712459375" y2="0.480059375" layer="21"/>
<rectangle x1="5.996940625" y1="0.47498125" x2="6.16458125" y2="0.480059375" layer="21"/>
<rectangle x1="6.4135" y1="0.47498125" x2="6.504940625" y2="0.480059375" layer="21"/>
<rectangle x1="6.753859375" y1="0.47498125" x2="6.8453" y2="0.480059375" layer="21"/>
<rectangle x1="7.241540625" y1="0.47498125" x2="7.33298125" y2="0.480059375" layer="21"/>
<rectangle x1="7.592059375" y1="0.47498125" x2="7.71398125" y2="0.480059375" layer="21"/>
<rectangle x1="8.054340625" y1="0.47498125" x2="8.176259375" y2="0.480059375" layer="21"/>
<rectangle x1="0.01778125" y1="0.480059375" x2="0.6985" y2="0.485140625" layer="21"/>
<rectangle x1="0.886459375" y1="0.480059375" x2="1.10998125" y2="0.485140625" layer="21"/>
<rectangle x1="1.267459375" y1="0.480059375" x2="1.49098125" y2="0.485140625" layer="21"/>
<rectangle x1="1.71958125" y1="0.480059375" x2="1.9431" y2="0.485140625" layer="21"/>
<rectangle x1="2.161540625" y1="0.480059375" x2="2.390140625" y2="0.485140625" layer="21"/>
<rectangle x1="2.55778125" y1="0.480059375" x2="3.26898125" y2="0.485140625" layer="21"/>
<rectangle x1="3.4671" y1="0.480059375" x2="3.685540625" y2="0.485140625" layer="21"/>
<rectangle x1="4.02081875" y1="0.480059375" x2="4.35101875" y2="0.485140625" layer="21"/>
<rectangle x1="4.71678125" y1="0.480059375" x2="4.8387" y2="0.485140625" layer="21"/>
<rectangle x1="5.1943" y1="0.480059375" x2="5.37718125" y2="0.485140625" layer="21"/>
<rectangle x1="5.6007" y1="0.480059375" x2="5.70738125" y2="0.485140625" layer="21"/>
<rectangle x1="6.00201875" y1="0.480059375" x2="6.169659375" y2="0.485140625" layer="21"/>
<rectangle x1="6.41858125" y1="0.480059375" x2="6.51001875" y2="0.485140625" layer="21"/>
<rectangle x1="6.753859375" y1="0.480059375" x2="6.8453" y2="0.485140625" layer="21"/>
<rectangle x1="7.24661875" y1="0.480059375" x2="7.338059375" y2="0.485140625" layer="21"/>
<rectangle x1="7.58698125" y1="0.480059375" x2="7.7089" y2="0.485140625" layer="21"/>
<rectangle x1="8.05941875" y1="0.480059375" x2="8.181340625" y2="0.485140625" layer="21"/>
<rectangle x1="0.022859375" y1="0.485140625" x2="0.70358125" y2="0.49021875" layer="21"/>
<rectangle x1="0.886459375" y1="0.485140625" x2="1.115059375" y2="0.49021875" layer="21"/>
<rectangle x1="1.267459375" y1="0.485140625" x2="1.49098125" y2="0.49021875" layer="21"/>
<rectangle x1="1.7145" y1="0.485140625" x2="1.93801875" y2="0.49021875" layer="21"/>
<rectangle x1="2.16661875" y1="0.485140625" x2="2.390140625" y2="0.49021875" layer="21"/>
<rectangle x1="2.5527" y1="0.485140625" x2="3.26898125" y2="0.49021875" layer="21"/>
<rectangle x1="3.46201875" y1="0.485140625" x2="3.69061875" y2="0.49021875" layer="21"/>
<rectangle x1="4.02081875" y1="0.485140625" x2="4.3561" y2="0.49021875" layer="21"/>
<rectangle x1="4.71678125" y1="0.485140625" x2="4.83361875" y2="0.49021875" layer="21"/>
<rectangle x1="5.19938125" y1="0.485140625" x2="5.37718125" y2="0.49021875" layer="21"/>
<rectangle x1="5.6007" y1="0.485140625" x2="5.7023" y2="0.49021875" layer="21"/>
<rectangle x1="6.0071" y1="0.485140625" x2="6.169659375" y2="0.49021875" layer="21"/>
<rectangle x1="6.4135" y1="0.485140625" x2="6.504940625" y2="0.49021875" layer="21"/>
<rectangle x1="6.74878125" y1="0.485140625" x2="6.8453" y2="0.49021875" layer="21"/>
<rectangle x1="7.241540625" y1="0.485140625" x2="7.338059375" y2="0.49021875" layer="21"/>
<rectangle x1="7.5819" y1="0.485140625" x2="7.70381875" y2="0.49021875" layer="21"/>
<rectangle x1="8.0645" y1="0.485140625" x2="8.18641875" y2="0.49021875" layer="21"/>
<rectangle x1="0.027940625" y1="0.49021875" x2="0.70358125" y2="0.4953" layer="21"/>
<rectangle x1="0.886459375" y1="0.49021875" x2="1.10998125" y2="0.4953" layer="21"/>
<rectangle x1="1.26238125" y1="0.49021875" x2="1.49098125" y2="0.4953" layer="21"/>
<rectangle x1="1.7145" y1="0.49021875" x2="1.9431" y2="0.4953" layer="21"/>
<rectangle x1="2.16661875" y1="0.49021875" x2="2.390140625" y2="0.4953" layer="21"/>
<rectangle x1="2.5527" y1="0.49021875" x2="3.274059375" y2="0.4953" layer="21"/>
<rectangle x1="3.4671" y1="0.49021875" x2="3.69061875" y2="0.4953" layer="21"/>
<rectangle x1="4.015740625" y1="0.49021875" x2="4.3561" y2="0.4953" layer="21"/>
<rectangle x1="4.7117" y1="0.49021875" x2="4.828540625" y2="0.4953" layer="21"/>
<rectangle x1="5.209540625" y1="0.49021875" x2="5.37718125" y2="0.4953" layer="21"/>
<rectangle x1="5.59561875" y1="0.49021875" x2="5.7023" y2="0.4953" layer="21"/>
<rectangle x1="6.01218125" y1="0.49021875" x2="6.169659375" y2="0.4953" layer="21"/>
<rectangle x1="6.4135" y1="0.49021875" x2="6.51001875" y2="0.4953" layer="21"/>
<rectangle x1="6.74878125" y1="0.49021875" x2="6.8453" y2="0.4953" layer="21"/>
<rectangle x1="7.24661875" y1="0.49021875" x2="7.33298125" y2="0.4953" layer="21"/>
<rectangle x1="7.5819" y1="0.49021875" x2="7.698740625" y2="0.4953" layer="21"/>
<rectangle x1="8.06958125" y1="0.49021875" x2="8.18641875" y2="0.4953" layer="21"/>
<rectangle x1="0.03301875" y1="0.4953" x2="0.708659375" y2="0.50038125" layer="21"/>
<rectangle x1="0.891540625" y1="0.4953" x2="1.115059375" y2="0.50038125" layer="21"/>
<rectangle x1="1.267459375" y1="0.4953" x2="1.49098125" y2="0.50038125" layer="21"/>
<rectangle x1="1.7145" y1="0.4953" x2="1.93801875" y2="0.50038125" layer="21"/>
<rectangle x1="2.16661875" y1="0.4953" x2="2.390140625" y2="0.50038125" layer="21"/>
<rectangle x1="2.55778125" y1="0.4953" x2="3.279140625" y2="0.50038125" layer="21"/>
<rectangle x1="3.46201875" y1="0.4953" x2="3.685540625" y2="0.50038125" layer="21"/>
<rectangle x1="4.015740625" y1="0.4953" x2="4.36118125" y2="0.50038125" layer="21"/>
<rectangle x1="4.70661875" y1="0.4953" x2="4.823459375" y2="0.50038125" layer="21"/>
<rectangle x1="5.209540625" y1="0.4953" x2="5.37718125" y2="0.50038125" layer="21"/>
<rectangle x1="5.59561875" y1="0.4953" x2="5.69721875" y2="0.50038125" layer="21"/>
<rectangle x1="6.017259375" y1="0.4953" x2="6.16458125" y2="0.50038125" layer="21"/>
<rectangle x1="6.41858125" y1="0.4953" x2="6.51001875" y2="0.50038125" layer="21"/>
<rectangle x1="6.753859375" y1="0.4953" x2="6.8453" y2="0.50038125" layer="21"/>
<rectangle x1="7.241540625" y1="0.4953" x2="7.338059375" y2="0.50038125" layer="21"/>
<rectangle x1="7.57681875" y1="0.4953" x2="7.693659375" y2="0.50038125" layer="21"/>
<rectangle x1="8.079740625" y1="0.4953" x2="8.1915" y2="0.50038125" layer="21"/>
<rectangle x1="0.0381" y1="0.50038125" x2="0.708659375" y2="0.505459375" layer="21"/>
<rectangle x1="0.886459375" y1="0.50038125" x2="1.115059375" y2="0.505459375" layer="21"/>
<rectangle x1="1.267459375" y1="0.50038125" x2="1.49098125" y2="0.505459375" layer="21"/>
<rectangle x1="1.7145" y1="0.50038125" x2="1.93801875" y2="0.505459375" layer="21"/>
<rectangle x1="2.16661875" y1="0.50038125" x2="2.390140625" y2="0.505459375" layer="21"/>
<rectangle x1="2.55778125" y1="0.50038125" x2="3.28421875" y2="0.505459375" layer="21"/>
<rectangle x1="3.4671" y1="0.50038125" x2="3.69061875" y2="0.505459375" layer="21"/>
<rectangle x1="4.010659375" y1="0.50038125" x2="4.36118125" y2="0.505459375" layer="21"/>
<rectangle x1="4.70661875" y1="0.50038125" x2="4.81838125" y2="0.505459375" layer="21"/>
<rectangle x1="5.21461875" y1="0.50038125" x2="5.37718125" y2="0.505459375" layer="21"/>
<rectangle x1="5.59561875" y1="0.50038125" x2="5.69721875" y2="0.505459375" layer="21"/>
<rectangle x1="6.017259375" y1="0.50038125" x2="6.169659375" y2="0.505459375" layer="21"/>
<rectangle x1="6.4135" y1="0.50038125" x2="6.51001875" y2="0.505459375" layer="21"/>
<rectangle x1="6.74878125" y1="0.50038125" x2="6.8453" y2="0.505459375" layer="21"/>
<rectangle x1="7.24661875" y1="0.50038125" x2="7.338059375" y2="0.505459375" layer="21"/>
<rectangle x1="7.57681875" y1="0.50038125" x2="7.68858125" y2="0.505459375" layer="21"/>
<rectangle x1="8.079740625" y1="0.50038125" x2="8.19658125" y2="0.505459375" layer="21"/>
<rectangle x1="0.04318125" y1="0.505459375" x2="0.713740625" y2="0.510540625" layer="21"/>
<rectangle x1="0.891540625" y1="0.505459375" x2="1.10998125" y2="0.510540625" layer="21"/>
<rectangle x1="1.267459375" y1="0.505459375" x2="1.49098125" y2="0.510540625" layer="21"/>
<rectangle x1="1.7145" y1="0.505459375" x2="1.9431" y2="0.510540625" layer="21"/>
<rectangle x1="2.16661875" y1="0.505459375" x2="2.390140625" y2="0.510540625" layer="21"/>
<rectangle x1="2.55778125" y1="0.505459375" x2="3.28421875" y2="0.510540625" layer="21"/>
<rectangle x1="3.46201875" y1="0.505459375" x2="3.69061875" y2="0.510540625" layer="21"/>
<rectangle x1="4.010659375" y1="0.505459375" x2="4.36118125" y2="0.510540625" layer="21"/>
<rectangle x1="4.701540625" y1="0.505459375" x2="4.8133" y2="0.510540625" layer="21"/>
<rectangle x1="5.2197" y1="0.505459375" x2="5.37718125" y2="0.510540625" layer="21"/>
<rectangle x1="5.59561875" y1="0.505459375" x2="5.69721875" y2="0.510540625" layer="21"/>
<rectangle x1="6.022340625" y1="0.505459375" x2="6.16458125" y2="0.510540625" layer="21"/>
<rectangle x1="6.41858125" y1="0.505459375" x2="6.504940625" y2="0.510540625" layer="21"/>
<rectangle x1="6.753859375" y1="0.505459375" x2="6.8453" y2="0.510540625" layer="21"/>
<rectangle x1="7.241540625" y1="0.505459375" x2="7.33298125" y2="0.510540625" layer="21"/>
<rectangle x1="7.571740625" y1="0.505459375" x2="7.6835" y2="0.510540625" layer="21"/>
<rectangle x1="8.08481875" y1="0.505459375" x2="8.19658125" y2="0.510540625" layer="21"/>
<rectangle x1="0.048259375" y1="0.510540625" x2="0.71881875" y2="0.51561875" layer="21"/>
<rectangle x1="0.886459375" y1="0.510540625" x2="1.115059375" y2="0.51561875" layer="21"/>
<rectangle x1="1.267459375" y1="0.510540625" x2="1.49098125" y2="0.51561875" layer="21"/>
<rectangle x1="1.71958125" y1="0.510540625" x2="1.9431" y2="0.51561875" layer="21"/>
<rectangle x1="2.16661875" y1="0.510540625" x2="2.390140625" y2="0.51561875" layer="21"/>
<rectangle x1="2.5527" y1="0.510540625" x2="3.2893" y2="0.51561875" layer="21"/>
<rectangle x1="3.4671" y1="0.510540625" x2="3.69061875" y2="0.51561875" layer="21"/>
<rectangle x1="4.010659375" y1="0.510540625" x2="4.366259375" y2="0.51561875" layer="21"/>
<rectangle x1="4.696459375" y1="0.510540625" x2="4.80821875" y2="0.51561875" layer="21"/>
<rectangle x1="5.22478125" y1="0.510540625" x2="5.37718125" y2="0.51561875" layer="21"/>
<rectangle x1="5.590540625" y1="0.510540625" x2="5.692140625" y2="0.51561875" layer="21"/>
<rectangle x1="6.02741875" y1="0.510540625" x2="6.169659375" y2="0.51561875" layer="21"/>
<rectangle x1="6.4135" y1="0.510540625" x2="6.504940625" y2="0.51561875" layer="21"/>
<rectangle x1="6.753859375" y1="0.510540625" x2="6.8453" y2="0.51561875" layer="21"/>
<rectangle x1="7.24661875" y1="0.510540625" x2="7.338059375" y2="0.51561875" layer="21"/>
<rectangle x1="7.571740625" y1="0.510540625" x2="7.67841875" y2="0.51561875" layer="21"/>
<rectangle x1="8.0899" y1="0.510540625" x2="8.201659375" y2="0.51561875" layer="21"/>
<rectangle x1="0.048259375" y1="0.51561875" x2="0.71881875" y2="0.5207" layer="21"/>
<rectangle x1="0.886459375" y1="0.51561875" x2="1.10998125" y2="0.5207" layer="21"/>
<rectangle x1="1.26238125" y1="0.51561875" x2="1.49098125" y2="0.5207" layer="21"/>
<rectangle x1="1.7145" y1="0.51561875" x2="1.93801875" y2="0.5207" layer="21"/>
<rectangle x1="2.16661875" y1="0.51561875" x2="2.390140625" y2="0.5207" layer="21"/>
<rectangle x1="2.55778125" y1="0.51561875" x2="3.2893" y2="0.5207" layer="21"/>
<rectangle x1="3.46201875" y1="0.51561875" x2="3.685540625" y2="0.5207" layer="21"/>
<rectangle x1="4.00558125" y1="0.51561875" x2="4.366259375" y2="0.5207" layer="21"/>
<rectangle x1="4.696459375" y1="0.51561875" x2="4.803140625" y2="0.5207" layer="21"/>
<rectangle x1="5.229859375" y1="0.51561875" x2="5.37718125" y2="0.5207" layer="21"/>
<rectangle x1="5.590540625" y1="0.51561875" x2="5.692140625" y2="0.5207" layer="21"/>
<rectangle x1="6.02741875" y1="0.51561875" x2="6.16458125" y2="0.5207" layer="21"/>
<rectangle x1="6.41858125" y1="0.51561875" x2="6.51001875" y2="0.5207" layer="21"/>
<rectangle x1="6.74878125" y1="0.51561875" x2="6.84021875" y2="0.5207" layer="21"/>
<rectangle x1="7.241540625" y1="0.51561875" x2="7.338059375" y2="0.5207" layer="21"/>
<rectangle x1="7.566659375" y1="0.51561875" x2="7.673340625" y2="0.5207" layer="21"/>
<rectangle x1="8.09498125" y1="0.51561875" x2="8.201659375" y2="0.5207" layer="21"/>
<rectangle x1="0.053340625" y1="0.5207" x2="0.71881875" y2="0.52578125" layer="21"/>
<rectangle x1="0.886459375" y1="0.5207" x2="1.115059375" y2="0.52578125" layer="21"/>
<rectangle x1="1.267459375" y1="0.5207" x2="1.49098125" y2="0.52578125" layer="21"/>
<rectangle x1="1.7145" y1="0.5207" x2="1.93801875" y2="0.52578125" layer="21"/>
<rectangle x1="2.16661875" y1="0.5207" x2="2.390140625" y2="0.52578125" layer="21"/>
<rectangle x1="2.55778125" y1="0.5207" x2="3.29438125" y2="0.52578125" layer="21"/>
<rectangle x1="3.4671" y1="0.5207" x2="3.69061875" y2="0.52578125" layer="21"/>
<rectangle x1="4.00558125" y1="0.5207" x2="4.366259375" y2="0.52578125" layer="21"/>
<rectangle x1="4.696459375" y1="0.5207" x2="4.798059375" y2="0.52578125" layer="21"/>
<rectangle x1="5.234940625" y1="0.5207" x2="5.37718125" y2="0.52578125" layer="21"/>
<rectangle x1="5.590540625" y1="0.5207" x2="5.687059375" y2="0.52578125" layer="21"/>
<rectangle x1="6.0325" y1="0.5207" x2="6.16458125" y2="0.52578125" layer="21"/>
<rectangle x1="6.4135" y1="0.5207" x2="6.51001875" y2="0.52578125" layer="21"/>
<rectangle x1="6.74878125" y1="0.5207" x2="6.8453" y2="0.52578125" layer="21"/>
<rectangle x1="7.24661875" y1="0.5207" x2="7.33298125" y2="0.52578125" layer="21"/>
<rectangle x1="7.56158125" y1="0.5207" x2="7.673340625" y2="0.52578125" layer="21"/>
<rectangle x1="8.100059375" y1="0.5207" x2="8.206740625" y2="0.52578125" layer="21"/>
<rectangle x1="0.05841875" y1="0.52578125" x2="0.7239" y2="0.530859375" layer="21"/>
<rectangle x1="0.891540625" y1="0.52578125" x2="1.115059375" y2="0.530859375" layer="21"/>
<rectangle x1="1.267459375" y1="0.52578125" x2="1.49098125" y2="0.530859375" layer="21"/>
<rectangle x1="1.7145" y1="0.52578125" x2="1.93801875" y2="0.530859375" layer="21"/>
<rectangle x1="2.16661875" y1="0.52578125" x2="2.390140625" y2="0.530859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.52578125" x2="3.29438125" y2="0.530859375" layer="21"/>
<rectangle x1="3.46201875" y1="0.52578125" x2="3.69061875" y2="0.530859375" layer="21"/>
<rectangle x1="4.0005" y1="0.52578125" x2="4.366259375" y2="0.530859375" layer="21"/>
<rectangle x1="4.69138125" y1="0.52578125" x2="4.798059375" y2="0.530859375" layer="21"/>
<rectangle x1="5.24001875" y1="0.52578125" x2="5.37718125" y2="0.530859375" layer="21"/>
<rectangle x1="5.590540625" y1="0.52578125" x2="5.687059375" y2="0.530859375" layer="21"/>
<rectangle x1="6.0325" y1="0.52578125" x2="6.169659375" y2="0.530859375" layer="21"/>
<rectangle x1="6.4135" y1="0.52578125" x2="6.504940625" y2="0.530859375" layer="21"/>
<rectangle x1="6.753859375" y1="0.52578125" x2="6.8453" y2="0.530859375" layer="21"/>
<rectangle x1="7.241540625" y1="0.52578125" x2="7.338059375" y2="0.530859375" layer="21"/>
<rectangle x1="7.56158125" y1="0.52578125" x2="7.668259375" y2="0.530859375" layer="21"/>
<rectangle x1="8.100059375" y1="0.52578125" x2="8.206740625" y2="0.530859375" layer="21"/>
<rectangle x1="0.0635" y1="0.530859375" x2="0.7239" y2="0.535940625" layer="21"/>
<rectangle x1="0.886459375" y1="0.530859375" x2="1.115059375" y2="0.535940625" layer="21"/>
<rectangle x1="1.26238125" y1="0.530859375" x2="1.49098125" y2="0.535940625" layer="21"/>
<rectangle x1="1.7145" y1="0.530859375" x2="1.9431" y2="0.535940625" layer="21"/>
<rectangle x1="2.161540625" y1="0.530859375" x2="2.390140625" y2="0.535940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.530859375" x2="2.918459375" y2="0.535940625" layer="21"/>
<rectangle x1="2.92861875" y1="0.530859375" x2="2.9337" y2="0.535940625" layer="21"/>
<rectangle x1="2.943859375" y1="0.530859375" x2="2.948940625" y2="0.535940625" layer="21"/>
<rectangle x1="2.9591" y1="0.530859375" x2="3.29438125" y2="0.535940625" layer="21"/>
<rectangle x1="3.4671" y1="0.530859375" x2="3.685540625" y2="0.535940625" layer="21"/>
<rectangle x1="4.0005" y1="0.530859375" x2="4.371340625" y2="0.535940625" layer="21"/>
<rectangle x1="4.69138125" y1="0.530859375" x2="4.79298125" y2="0.535940625" layer="21"/>
<rectangle x1="5.24001875" y1="0.530859375" x2="5.37718125" y2="0.535940625" layer="21"/>
<rectangle x1="5.590540625" y1="0.530859375" x2="5.687059375" y2="0.535940625" layer="21"/>
<rectangle x1="6.03758125" y1="0.530859375" x2="6.16458125" y2="0.535940625" layer="21"/>
<rectangle x1="6.4135" y1="0.530859375" x2="6.51001875" y2="0.535940625" layer="21"/>
<rectangle x1="6.74878125" y1="0.530859375" x2="6.8453" y2="0.535940625" layer="21"/>
<rectangle x1="7.24661875" y1="0.530859375" x2="7.338059375" y2="0.535940625" layer="21"/>
<rectangle x1="7.56158125" y1="0.530859375" x2="7.66318125" y2="0.535940625" layer="21"/>
<rectangle x1="8.105140625" y1="0.530859375" x2="8.21181875" y2="0.535940625" layer="21"/>
<rectangle x1="0.06858125" y1="0.535940625" x2="0.72898125" y2="0.54101875" layer="21"/>
<rectangle x1="0.891540625" y1="0.535940625" x2="1.10998125" y2="0.54101875" layer="21"/>
<rectangle x1="1.267459375" y1="0.535940625" x2="1.49098125" y2="0.54101875" layer="21"/>
<rectangle x1="1.7145" y1="0.535940625" x2="1.93801875" y2="0.54101875" layer="21"/>
<rectangle x1="2.16661875" y1="0.535940625" x2="2.390140625" y2="0.54101875" layer="21"/>
<rectangle x1="2.55778125" y1="0.535940625" x2="2.88798125" y2="0.54101875" layer="21"/>
<rectangle x1="2.98958125" y1="0.535940625" x2="3.299459375" y2="0.54101875" layer="21"/>
<rectangle x1="3.46201875" y1="0.535940625" x2="3.69061875" y2="0.54101875" layer="21"/>
<rectangle x1="3.99541875" y1="0.535940625" x2="4.37641875" y2="0.54101875" layer="21"/>
<rectangle x1="4.6863" y1="0.535940625" x2="4.79298125" y2="0.54101875" layer="21"/>
<rectangle x1="5.2451" y1="0.535940625" x2="5.37718125" y2="0.54101875" layer="21"/>
<rectangle x1="5.590540625" y1="0.535940625" x2="5.68198125" y2="0.54101875" layer="21"/>
<rectangle x1="6.03758125" y1="0.535940625" x2="6.16458125" y2="0.54101875" layer="21"/>
<rectangle x1="6.41858125" y1="0.535940625" x2="6.504940625" y2="0.54101875" layer="21"/>
<rectangle x1="6.753859375" y1="0.535940625" x2="6.8453" y2="0.54101875" layer="21"/>
<rectangle x1="7.241540625" y1="0.535940625" x2="7.33298125" y2="0.54101875" layer="21"/>
<rectangle x1="7.5565" y1="0.535940625" x2="7.66318125" y2="0.54101875" layer="21"/>
<rectangle x1="8.11021875" y1="0.535940625" x2="8.21181875" y2="0.54101875" layer="21"/>
<rectangle x1="0.073659375" y1="0.54101875" x2="0.327659375" y2="0.5461" layer="21"/>
<rectangle x1="0.383540625" y1="0.54101875" x2="0.38861875" y2="0.5461" layer="21"/>
<rectangle x1="0.3937" y1="0.54101875" x2="0.72898125" y2="0.5461" layer="21"/>
<rectangle x1="0.886459375" y1="0.54101875" x2="1.115059375" y2="0.5461" layer="21"/>
<rectangle x1="1.267459375" y1="0.54101875" x2="1.49098125" y2="0.5461" layer="21"/>
<rectangle x1="1.71958125" y1="0.54101875" x2="1.9431" y2="0.5461" layer="21"/>
<rectangle x1="2.16661875" y1="0.54101875" x2="2.390140625" y2="0.5461" layer="21"/>
<rectangle x1="2.55778125" y1="0.54101875" x2="2.867659375" y2="0.5461" layer="21"/>
<rectangle x1="3.00481875" y1="0.54101875" x2="3.299459375" y2="0.5461" layer="21"/>
<rectangle x1="3.4671" y1="0.54101875" x2="3.685540625" y2="0.5461" layer="21"/>
<rectangle x1="3.99541875" y1="0.54101875" x2="4.37641875" y2="0.5461" layer="21"/>
<rectangle x1="4.6863" y1="0.54101875" x2="4.7879" y2="0.5461" layer="21"/>
<rectangle x1="5.25018125" y1="0.54101875" x2="5.37718125" y2="0.5461" layer="21"/>
<rectangle x1="5.585459375" y1="0.54101875" x2="5.687059375" y2="0.5461" layer="21"/>
<rectangle x1="6.042659375" y1="0.54101875" x2="6.16458125" y2="0.5461" layer="21"/>
<rectangle x1="6.4135" y1="0.54101875" x2="6.51001875" y2="0.5461" layer="21"/>
<rectangle x1="6.753859375" y1="0.54101875" x2="6.8453" y2="0.5461" layer="21"/>
<rectangle x1="7.24661875" y1="0.54101875" x2="7.338059375" y2="0.5461" layer="21"/>
<rectangle x1="7.5565" y1="0.54101875" x2="7.6581" y2="0.5461" layer="21"/>
<rectangle x1="8.11021875" y1="0.54101875" x2="8.21181875" y2="0.5461" layer="21"/>
<rectangle x1="0.078740625" y1="0.5461" x2="0.302259375" y2="0.55118125" layer="21"/>
<rectangle x1="0.4191" y1="0.5461" x2="0.734059375" y2="0.55118125" layer="21"/>
<rectangle x1="0.886459375" y1="0.5461" x2="1.115059375" y2="0.55118125" layer="21"/>
<rectangle x1="1.267459375" y1="0.5461" x2="1.49098125" y2="0.55118125" layer="21"/>
<rectangle x1="1.7145" y1="0.5461" x2="1.93801875" y2="0.55118125" layer="21"/>
<rectangle x1="2.16661875" y1="0.5461" x2="2.390140625" y2="0.55118125" layer="21"/>
<rectangle x1="2.5527" y1="0.5461" x2="2.85241875" y2="0.55118125" layer="21"/>
<rectangle x1="3.020059375" y1="0.5461" x2="3.304540625" y2="0.55118125" layer="21"/>
<rectangle x1="3.46201875" y1="0.5461" x2="3.69061875" y2="0.55118125" layer="21"/>
<rectangle x1="3.99541875" y1="0.5461" x2="4.37641875" y2="0.55118125" layer="21"/>
<rectangle x1="4.68121875" y1="0.5461" x2="4.78281875" y2="0.55118125" layer="21"/>
<rectangle x1="5.25018125" y1="0.5461" x2="5.37718125" y2="0.55118125" layer="21"/>
<rectangle x1="5.585459375" y1="0.5461" x2="5.68198125" y2="0.55118125" layer="21"/>
<rectangle x1="6.047740625" y1="0.5461" x2="6.16458125" y2="0.55118125" layer="21"/>
<rectangle x1="6.41858125" y1="0.5461" x2="6.504940625" y2="0.55118125" layer="21"/>
<rectangle x1="6.74878125" y1="0.5461" x2="6.8453" y2="0.55118125" layer="21"/>
<rectangle x1="7.241540625" y1="0.5461" x2="7.338059375" y2="0.55118125" layer="21"/>
<rectangle x1="7.55141875" y1="0.5461" x2="7.65301875" y2="0.55118125" layer="21"/>
<rectangle x1="8.1153" y1="0.5461" x2="8.2169" y2="0.55118125" layer="21"/>
<rectangle x1="0.08381875" y1="0.55118125" x2="0.28701875" y2="0.556259375" layer="21"/>
<rectangle x1="0.434340625" y1="0.55118125" x2="0.734059375" y2="0.556259375" layer="21"/>
<rectangle x1="0.886459375" y1="0.55118125" x2="1.10998125" y2="0.556259375" layer="21"/>
<rectangle x1="1.267459375" y1="0.55118125" x2="1.49098125" y2="0.556259375" layer="21"/>
<rectangle x1="1.7145" y1="0.55118125" x2="1.9431" y2="0.556259375" layer="21"/>
<rectangle x1="2.16661875" y1="0.55118125" x2="2.390140625" y2="0.556259375" layer="21"/>
<rectangle x1="2.55778125" y1="0.55118125" x2="2.847340625" y2="0.556259375" layer="21"/>
<rectangle x1="3.03021875" y1="0.55118125" x2="3.304540625" y2="0.556259375" layer="21"/>
<rectangle x1="3.46201875" y1="0.55118125" x2="3.69061875" y2="0.556259375" layer="21"/>
<rectangle x1="3.990340625" y1="0.55118125" x2="4.3815" y2="0.556259375" layer="21"/>
<rectangle x1="4.68121875" y1="0.55118125" x2="4.78281875" y2="0.556259375" layer="21"/>
<rectangle x1="5.255259375" y1="0.55118125" x2="5.37718125" y2="0.556259375" layer="21"/>
<rectangle x1="5.585459375" y1="0.55118125" x2="5.68198125" y2="0.556259375" layer="21"/>
<rectangle x1="6.047740625" y1="0.55118125" x2="6.16458125" y2="0.556259375" layer="21"/>
<rectangle x1="6.4135" y1="0.55118125" x2="6.51001875" y2="0.556259375" layer="21"/>
<rectangle x1="6.74878125" y1="0.55118125" x2="6.8453" y2="0.556259375" layer="21"/>
<rectangle x1="7.24661875" y1="0.55118125" x2="7.338059375" y2="0.556259375" layer="21"/>
<rectangle x1="7.55141875" y1="0.55118125" x2="7.65301875" y2="0.556259375" layer="21"/>
<rectangle x1="8.1153" y1="0.55118125" x2="8.2169" y2="0.556259375" layer="21"/>
<rectangle x1="0.0889" y1="0.556259375" x2="0.27178125" y2="0.561340625" layer="21"/>
<rectangle x1="0.44958125" y1="0.556259375" x2="0.734059375" y2="0.561340625" layer="21"/>
<rectangle x1="0.891540625" y1="0.556259375" x2="1.115059375" y2="0.561340625" layer="21"/>
<rectangle x1="1.26238125" y1="0.556259375" x2="1.49098125" y2="0.561340625" layer="21"/>
<rectangle x1="1.7145" y1="0.556259375" x2="1.93801875" y2="0.561340625" layer="21"/>
<rectangle x1="2.16661875" y1="0.556259375" x2="2.390140625" y2="0.561340625" layer="21"/>
<rectangle x1="2.55778125" y1="0.556259375" x2="2.83718125" y2="0.561340625" layer="21"/>
<rectangle x1="3.0353" y1="0.556259375" x2="3.30961875" y2="0.561340625" layer="21"/>
<rectangle x1="3.4671" y1="0.556259375" x2="3.69061875" y2="0.561340625" layer="21"/>
<rectangle x1="3.990340625" y1="0.556259375" x2="4.3815" y2="0.561340625" layer="21"/>
<rectangle x1="4.676140625" y1="0.556259375" x2="4.777740625" y2="0.561340625" layer="21"/>
<rectangle x1="5.260340625" y1="0.556259375" x2="5.37718125" y2="0.561340625" layer="21"/>
<rectangle x1="5.585459375" y1="0.556259375" x2="5.68198125" y2="0.561340625" layer="21"/>
<rectangle x1="6.047740625" y1="0.556259375" x2="6.16458125" y2="0.561340625" layer="21"/>
<rectangle x1="6.4135" y1="0.556259375" x2="6.51001875" y2="0.561340625" layer="21"/>
<rectangle x1="6.753859375" y1="0.556259375" x2="6.84021875" y2="0.561340625" layer="21"/>
<rectangle x1="7.241540625" y1="0.556259375" x2="7.338059375" y2="0.561340625" layer="21"/>
<rectangle x1="7.546340625" y1="0.556259375" x2="7.647940625" y2="0.561340625" layer="21"/>
<rectangle x1="8.12038125" y1="0.556259375" x2="8.22198125" y2="0.561340625" layer="21"/>
<rectangle x1="0.09398125" y1="0.561340625" x2="0.256540625" y2="0.56641875" layer="21"/>
<rectangle x1="0.459740625" y1="0.561340625" x2="0.734059375" y2="0.56641875" layer="21"/>
<rectangle x1="0.886459375" y1="0.561340625" x2="1.10998125" y2="0.56641875" layer="21"/>
<rectangle x1="1.267459375" y1="0.561340625" x2="1.49098125" y2="0.56641875" layer="21"/>
<rectangle x1="1.7145" y1="0.561340625" x2="1.93801875" y2="0.56641875" layer="21"/>
<rectangle x1="2.16661875" y1="0.561340625" x2="2.390140625" y2="0.56641875" layer="21"/>
<rectangle x1="2.55778125" y1="0.561340625" x2="2.8321" y2="0.56641875" layer="21"/>
<rectangle x1="3.045459375" y1="0.561340625" x2="3.30961875" y2="0.56641875" layer="21"/>
<rectangle x1="3.4671" y1="0.561340625" x2="3.685540625" y2="0.56641875" layer="21"/>
<rectangle x1="3.985259375" y1="0.561340625" x2="4.3815" y2="0.56641875" layer="21"/>
<rectangle x1="4.676140625" y1="0.561340625" x2="4.777740625" y2="0.56641875" layer="21"/>
<rectangle x1="5.260340625" y1="0.561340625" x2="5.37718125" y2="0.56641875" layer="21"/>
<rectangle x1="5.585459375" y1="0.561340625" x2="5.68198125" y2="0.56641875" layer="21"/>
<rectangle x1="6.05281875" y1="0.561340625" x2="6.16458125" y2="0.56641875" layer="21"/>
<rectangle x1="6.4135" y1="0.561340625" x2="6.51001875" y2="0.56641875" layer="21"/>
<rectangle x1="6.74878125" y1="0.561340625" x2="6.8453" y2="0.56641875" layer="21"/>
<rectangle x1="7.24661875" y1="0.561340625" x2="7.33298125" y2="0.56641875" layer="21"/>
<rectangle x1="7.546340625" y1="0.561340625" x2="7.647940625" y2="0.56641875" layer="21"/>
<rectangle x1="8.12038125" y1="0.561340625" x2="8.22198125" y2="0.56641875" layer="21"/>
<rectangle x1="0.099059375" y1="0.56641875" x2="0.251459375" y2="0.5715" layer="21"/>
<rectangle x1="0.4699" y1="0.56641875" x2="0.739140625" y2="0.5715" layer="21"/>
<rectangle x1="0.891540625" y1="0.56641875" x2="1.115059375" y2="0.5715" layer="21"/>
<rectangle x1="1.267459375" y1="0.56641875" x2="1.49098125" y2="0.5715" layer="21"/>
<rectangle x1="1.71958125" y1="0.56641875" x2="1.9431" y2="0.5715" layer="21"/>
<rectangle x1="2.16661875" y1="0.56641875" x2="2.390140625" y2="0.5715" layer="21"/>
<rectangle x1="2.5527" y1="0.56641875" x2="2.821940625" y2="0.5715" layer="21"/>
<rectangle x1="3.050540625" y1="0.56641875" x2="3.30961875" y2="0.5715" layer="21"/>
<rectangle x1="3.46201875" y1="0.56641875" x2="3.69061875" y2="0.5715" layer="21"/>
<rectangle x1="3.985259375" y1="0.56641875" x2="4.38658125" y2="0.5715" layer="21"/>
<rectangle x1="4.676140625" y1="0.56641875" x2="4.772659375" y2="0.5715" layer="21"/>
<rectangle x1="5.26541875" y1="0.56641875" x2="5.37718125" y2="0.5715" layer="21"/>
<rectangle x1="5.585459375" y1="0.56641875" x2="5.6769" y2="0.5715" layer="21"/>
<rectangle x1="6.05281875" y1="0.56641875" x2="6.16458125" y2="0.5715" layer="21"/>
<rectangle x1="6.41858125" y1="0.56641875" x2="6.504940625" y2="0.5715" layer="21"/>
<rectangle x1="6.753859375" y1="0.56641875" x2="6.8453" y2="0.5715" layer="21"/>
<rectangle x1="7.241540625" y1="0.56641875" x2="7.338059375" y2="0.5715" layer="21"/>
<rectangle x1="7.546340625" y1="0.56641875" x2="7.642859375" y2="0.5715" layer="21"/>
<rectangle x1="8.125459375" y1="0.56641875" x2="8.22198125" y2="0.5715" layer="21"/>
<rectangle x1="0.099059375" y1="0.5715" x2="0.2413" y2="0.57658125" layer="21"/>
<rectangle x1="0.47498125" y1="0.5715" x2="0.739140625" y2="0.57658125" layer="21"/>
<rectangle x1="0.891540625" y1="0.5715" x2="1.115059375" y2="0.57658125" layer="21"/>
<rectangle x1="1.26238125" y1="0.5715" x2="1.49098125" y2="0.57658125" layer="21"/>
<rectangle x1="1.7145" y1="0.5715" x2="1.9431" y2="0.57658125" layer="21"/>
<rectangle x1="2.16661875" y1="0.5715" x2="2.390140625" y2="0.57658125" layer="21"/>
<rectangle x1="2.55778125" y1="0.5715" x2="2.816859375" y2="0.57658125" layer="21"/>
<rectangle x1="3.05561875" y1="0.5715" x2="3.3147" y2="0.57658125" layer="21"/>
<rectangle x1="3.4671" y1="0.5715" x2="3.685540625" y2="0.57658125" layer="21"/>
<rectangle x1="3.98018125" y1="0.5715" x2="4.38658125" y2="0.57658125" layer="21"/>
<rectangle x1="4.676140625" y1="0.5715" x2="4.772659375" y2="0.57658125" layer="21"/>
<rectangle x1="5.26541875" y1="0.5715" x2="5.37718125" y2="0.57658125" layer="21"/>
<rectangle x1="5.58038125" y1="0.5715" x2="5.6769" y2="0.57658125" layer="21"/>
<rectangle x1="6.0579" y1="0.5715" x2="6.16458125" y2="0.57658125" layer="21"/>
<rectangle x1="6.4135" y1="0.5715" x2="6.504940625" y2="0.57658125" layer="21"/>
<rectangle x1="6.74878125" y1="0.5715" x2="6.8453" y2="0.57658125" layer="21"/>
<rectangle x1="7.241540625" y1="0.5715" x2="7.338059375" y2="0.57658125" layer="21"/>
<rectangle x1="7.541259375" y1="0.5715" x2="7.642859375" y2="0.57658125" layer="21"/>
<rectangle x1="8.125459375" y1="0.5715" x2="8.227059375" y2="0.57658125" layer="21"/>
<rectangle x1="0.104140625" y1="0.57658125" x2="0.231140625" y2="0.581659375" layer="21"/>
<rectangle x1="0.485140625" y1="0.57658125" x2="0.739140625" y2="0.581659375" layer="21"/>
<rectangle x1="0.886459375" y1="0.57658125" x2="1.10998125" y2="0.581659375" layer="21"/>
<rectangle x1="1.267459375" y1="0.57658125" x2="1.49098125" y2="0.581659375" layer="21"/>
<rectangle x1="1.7145" y1="0.57658125" x2="1.93801875" y2="0.581659375" layer="21"/>
<rectangle x1="2.16661875" y1="0.57658125" x2="2.390140625" y2="0.581659375" layer="21"/>
<rectangle x1="2.55778125" y1="0.57658125" x2="2.81178125" y2="0.581659375" layer="21"/>
<rectangle x1="3.0607" y1="0.57658125" x2="3.3147" y2="0.581659375" layer="21"/>
<rectangle x1="3.46201875" y1="0.57658125" x2="3.69061875" y2="0.581659375" layer="21"/>
<rectangle x1="3.98018125" y1="0.57658125" x2="4.391659375" y2="0.581659375" layer="21"/>
<rectangle x1="4.671059375" y1="0.57658125" x2="4.772659375" y2="0.581659375" layer="21"/>
<rectangle x1="5.2705" y1="0.57658125" x2="5.37718125" y2="0.581659375" layer="21"/>
<rectangle x1="5.585459375" y1="0.57658125" x2="5.6769" y2="0.581659375" layer="21"/>
<rectangle x1="6.0579" y1="0.57658125" x2="6.16458125" y2="0.581659375" layer="21"/>
<rectangle x1="6.41858125" y1="0.57658125" x2="6.51001875" y2="0.581659375" layer="21"/>
<rectangle x1="6.753859375" y1="0.57658125" x2="6.8453" y2="0.581659375" layer="21"/>
<rectangle x1="7.24661875" y1="0.57658125" x2="7.33298125" y2="0.581659375" layer="21"/>
<rectangle x1="7.541259375" y1="0.57658125" x2="7.642859375" y2="0.581659375" layer="21"/>
<rectangle x1="8.130540625" y1="0.57658125" x2="8.227059375" y2="0.581659375" layer="21"/>
<rectangle x1="0.10921875" y1="0.581659375" x2="0.22098125" y2="0.586740625" layer="21"/>
<rectangle x1="0.49021875" y1="0.581659375" x2="0.739140625" y2="0.586740625" layer="21"/>
<rectangle x1="0.891540625" y1="0.581659375" x2="1.115059375" y2="0.586740625" layer="21"/>
<rectangle x1="1.267459375" y1="0.581659375" x2="1.49098125" y2="0.586740625" layer="21"/>
<rectangle x1="1.7145" y1="0.581659375" x2="1.93801875" y2="0.586740625" layer="21"/>
<rectangle x1="2.16661875" y1="0.581659375" x2="2.390140625" y2="0.586740625" layer="21"/>
<rectangle x1="2.55778125" y1="0.581659375" x2="2.8067" y2="0.586740625" layer="21"/>
<rectangle x1="3.06578125" y1="0.581659375" x2="3.3147" y2="0.586740625" layer="21"/>
<rectangle x1="3.46201875" y1="0.581659375" x2="3.69061875" y2="0.586740625" layer="21"/>
<rectangle x1="3.9751" y1="0.581659375" x2="4.391659375" y2="0.586740625" layer="21"/>
<rectangle x1="4.671059375" y1="0.581659375" x2="4.76758125" y2="0.586740625" layer="21"/>
<rectangle x1="5.2705" y1="0.581659375" x2="5.37718125" y2="0.586740625" layer="21"/>
<rectangle x1="5.58038125" y1="0.581659375" x2="5.6769" y2="0.586740625" layer="21"/>
<rectangle x1="6.0579" y1="0.581659375" x2="6.16458125" y2="0.586740625" layer="21"/>
<rectangle x1="6.4135" y1="0.581659375" x2="6.51001875" y2="0.586740625" layer="21"/>
<rectangle x1="6.74878125" y1="0.581659375" x2="6.8453" y2="0.586740625" layer="21"/>
<rectangle x1="7.24661875" y1="0.581659375" x2="7.338059375" y2="0.586740625" layer="21"/>
<rectangle x1="7.541259375" y1="0.581659375" x2="7.63778125" y2="0.586740625" layer="21"/>
<rectangle x1="8.130540625" y1="0.581659375" x2="8.227059375" y2="0.586740625" layer="21"/>
<rectangle x1="0.1143" y1="0.586740625" x2="0.2159" y2="0.59181875" layer="21"/>
<rectangle x1="0.4953" y1="0.586740625" x2="0.74421875" y2="0.59181875" layer="21"/>
<rectangle x1="0.886459375" y1="0.586740625" x2="1.10998125" y2="0.59181875" layer="21"/>
<rectangle x1="1.267459375" y1="0.586740625" x2="1.49098125" y2="0.59181875" layer="21"/>
<rectangle x1="1.7145" y1="0.586740625" x2="1.93801875" y2="0.59181875" layer="21"/>
<rectangle x1="2.16661875" y1="0.586740625" x2="2.390140625" y2="0.59181875" layer="21"/>
<rectangle x1="2.5527" y1="0.586740625" x2="2.80161875" y2="0.59181875" layer="21"/>
<rectangle x1="3.070859375" y1="0.586740625" x2="3.31978125" y2="0.59181875" layer="21"/>
<rectangle x1="3.4671" y1="0.586740625" x2="3.69061875" y2="0.59181875" layer="21"/>
<rectangle x1="3.9751" y1="0.586740625" x2="4.391659375" y2="0.59181875" layer="21"/>
<rectangle x1="4.66598125" y1="0.586740625" x2="4.76758125" y2="0.59181875" layer="21"/>
<rectangle x1="5.27558125" y1="0.586740625" x2="5.37718125" y2="0.59181875" layer="21"/>
<rectangle x1="5.58038125" y1="0.586740625" x2="5.6769" y2="0.59181875" layer="21"/>
<rectangle x1="6.0579" y1="0.586740625" x2="6.16458125" y2="0.59181875" layer="21"/>
<rectangle x1="6.41858125" y1="0.586740625" x2="6.504940625" y2="0.59181875" layer="21"/>
<rectangle x1="6.753859375" y1="0.586740625" x2="6.8453" y2="0.59181875" layer="21"/>
<rectangle x1="7.241540625" y1="0.586740625" x2="7.338059375" y2="0.59181875" layer="21"/>
<rectangle x1="7.541259375" y1="0.586740625" x2="7.63778125" y2="0.59181875" layer="21"/>
<rectangle x1="8.130540625" y1="0.586740625" x2="8.232140625" y2="0.59181875" layer="21"/>
<rectangle x1="0.11938125" y1="0.59181875" x2="0.21081875" y2="0.5969" layer="21"/>
<rectangle x1="0.50038125" y1="0.59181875" x2="0.74421875" y2="0.5969" layer="21"/>
<rectangle x1="0.891540625" y1="0.59181875" x2="1.115059375" y2="0.5969" layer="21"/>
<rectangle x1="1.267459375" y1="0.59181875" x2="1.49098125" y2="0.5969" layer="21"/>
<rectangle x1="1.71958125" y1="0.59181875" x2="1.9431" y2="0.5969" layer="21"/>
<rectangle x1="2.16661875" y1="0.59181875" x2="2.390140625" y2="0.5969" layer="21"/>
<rectangle x1="2.55778125" y1="0.59181875" x2="2.80161875" y2="0.5969" layer="21"/>
<rectangle x1="3.075940625" y1="0.59181875" x2="3.31978125" y2="0.5969" layer="21"/>
<rectangle x1="3.4671" y1="0.59181875" x2="3.685540625" y2="0.5969" layer="21"/>
<rectangle x1="3.9751" y1="0.59181875" x2="4.396740625" y2="0.5969" layer="21"/>
<rectangle x1="4.66598125" y1="0.59181875" x2="4.7625" y2="0.5969" layer="21"/>
<rectangle x1="5.27558125" y1="0.59181875" x2="5.37718125" y2="0.5969" layer="21"/>
<rectangle x1="5.58038125" y1="0.59181875" x2="5.6769" y2="0.5969" layer="21"/>
<rectangle x1="6.06298125" y1="0.59181875" x2="6.16458125" y2="0.5969" layer="21"/>
<rectangle x1="6.4135" y1="0.59181875" x2="6.51001875" y2="0.5969" layer="21"/>
<rectangle x1="6.753859375" y1="0.59181875" x2="6.8453" y2="0.5969" layer="21"/>
<rectangle x1="7.24661875" y1="0.59181875" x2="7.338059375" y2="0.5969" layer="21"/>
<rectangle x1="7.53618125" y1="0.59181875" x2="7.6327" y2="0.5969" layer="21"/>
<rectangle x1="8.13561875" y1="0.59181875" x2="8.232140625" y2="0.5969" layer="21"/>
<rectangle x1="0.124459375" y1="0.5969" x2="0.200659375" y2="0.60198125" layer="21"/>
<rectangle x1="0.50038125" y1="0.5969" x2="0.74421875" y2="0.60198125" layer="21"/>
<rectangle x1="0.886459375" y1="0.5969" x2="1.115059375" y2="0.60198125" layer="21"/>
<rectangle x1="1.26238125" y1="0.5969" x2="1.49098125" y2="0.60198125" layer="21"/>
<rectangle x1="1.7145" y1="0.5969" x2="1.93801875" y2="0.60198125" layer="21"/>
<rectangle x1="2.16661875" y1="0.5969" x2="2.390140625" y2="0.60198125" layer="21"/>
<rectangle x1="2.55778125" y1="0.5969" x2="2.796540625" y2="0.60198125" layer="21"/>
<rectangle x1="3.08101875" y1="0.5969" x2="3.31978125" y2="0.60198125" layer="21"/>
<rectangle x1="3.46201875" y1="0.5969" x2="3.69061875" y2="0.60198125" layer="21"/>
<rectangle x1="3.97001875" y1="0.5969" x2="4.396740625" y2="0.60198125" layer="21"/>
<rectangle x1="4.66598125" y1="0.5969" x2="4.7625" y2="0.60198125" layer="21"/>
<rectangle x1="5.27558125" y1="0.5969" x2="5.37718125" y2="0.60198125" layer="21"/>
<rectangle x1="5.58038125" y1="0.5969" x2="5.6769" y2="0.60198125" layer="21"/>
<rectangle x1="6.06298125" y1="0.5969" x2="6.16458125" y2="0.60198125" layer="21"/>
<rectangle x1="6.41858125" y1="0.5969" x2="6.504940625" y2="0.60198125" layer="21"/>
<rectangle x1="6.74878125" y1="0.5969" x2="6.84021875" y2="0.60198125" layer="21"/>
<rectangle x1="7.241540625" y1="0.5969" x2="7.33298125" y2="0.60198125" layer="21"/>
<rectangle x1="7.53618125" y1="0.5969" x2="7.6327" y2="0.60198125" layer="21"/>
<rectangle x1="8.13561875" y1="0.5969" x2="8.232140625" y2="0.60198125" layer="21"/>
<rectangle x1="0.129540625" y1="0.60198125" x2="0.19558125" y2="0.607059375" layer="21"/>
<rectangle x1="0.505459375" y1="0.60198125" x2="0.74421875" y2="0.607059375" layer="21"/>
<rectangle x1="0.891540625" y1="0.60198125" x2="1.10998125" y2="0.607059375" layer="21"/>
<rectangle x1="1.267459375" y1="0.60198125" x2="1.49098125" y2="0.607059375" layer="21"/>
<rectangle x1="1.7145" y1="0.60198125" x2="1.9431" y2="0.607059375" layer="21"/>
<rectangle x1="2.16661875" y1="0.60198125" x2="2.390140625" y2="0.607059375" layer="21"/>
<rectangle x1="2.55778125" y1="0.60198125" x2="2.791459375" y2="0.607059375" layer="21"/>
<rectangle x1="3.08101875" y1="0.60198125" x2="3.31978125" y2="0.607059375" layer="21"/>
<rectangle x1="3.4671" y1="0.60198125" x2="3.685540625" y2="0.607059375" layer="21"/>
<rectangle x1="3.97001875" y1="0.60198125" x2="4.188459375" y2="0.607059375" layer="21"/>
<rectangle x1="4.193540625" y1="0.60198125" x2="4.40181875" y2="0.607059375" layer="21"/>
<rectangle x1="4.66598125" y1="0.60198125" x2="4.7625" y2="0.607059375" layer="21"/>
<rectangle x1="5.280659375" y1="0.60198125" x2="5.37718125" y2="0.607059375" layer="21"/>
<rectangle x1="5.58038125" y1="0.60198125" x2="5.6769" y2="0.607059375" layer="21"/>
<rectangle x1="6.06298125" y1="0.60198125" x2="6.16458125" y2="0.607059375" layer="21"/>
<rectangle x1="6.4135" y1="0.60198125" x2="6.51001875" y2="0.607059375" layer="21"/>
<rectangle x1="6.74878125" y1="0.60198125" x2="6.8453" y2="0.607059375" layer="21"/>
<rectangle x1="7.241540625" y1="0.60198125" x2="7.338059375" y2="0.607059375" layer="21"/>
<rectangle x1="7.53618125" y1="0.60198125" x2="7.6327" y2="0.607059375" layer="21"/>
<rectangle x1="8.13561875" y1="0.60198125" x2="8.232140625" y2="0.607059375" layer="21"/>
<rectangle x1="0.13461875" y1="0.607059375" x2="0.1905" y2="0.612140625" layer="21"/>
<rectangle x1="0.505459375" y1="0.607059375" x2="0.74421875" y2="0.612140625" layer="21"/>
<rectangle x1="0.886459375" y1="0.607059375" x2="1.115059375" y2="0.612140625" layer="21"/>
<rectangle x1="1.267459375" y1="0.607059375" x2="1.49098125" y2="0.612140625" layer="21"/>
<rectangle x1="1.7145" y1="0.607059375" x2="1.93801875" y2="0.612140625" layer="21"/>
<rectangle x1="2.16661875" y1="0.607059375" x2="2.390140625" y2="0.612140625" layer="21"/>
<rectangle x1="2.5527" y1="0.607059375" x2="2.78638125" y2="0.612140625" layer="21"/>
<rectangle x1="3.0861" y1="0.607059375" x2="3.324859375" y2="0.612140625" layer="21"/>
<rectangle x1="3.46201875" y1="0.607059375" x2="3.69061875" y2="0.612140625" layer="21"/>
<rectangle x1="3.964940625" y1="0.607059375" x2="4.188459375" y2="0.612140625" layer="21"/>
<rectangle x1="4.193540625" y1="0.607059375" x2="4.40181875" y2="0.612140625" layer="21"/>
<rectangle x1="4.66598125" y1="0.607059375" x2="4.7625" y2="0.612140625" layer="21"/>
<rectangle x1="5.280659375" y1="0.607059375" x2="5.37718125" y2="0.612140625" layer="21"/>
<rectangle x1="5.58038125" y1="0.607059375" x2="5.67181875" y2="0.612140625" layer="21"/>
<rectangle x1="6.06298125" y1="0.607059375" x2="6.16458125" y2="0.612140625" layer="21"/>
<rectangle x1="6.4135" y1="0.607059375" x2="6.504940625" y2="0.612140625" layer="21"/>
<rectangle x1="6.753859375" y1="0.607059375" x2="6.8453" y2="0.612140625" layer="21"/>
<rectangle x1="7.24661875" y1="0.607059375" x2="7.338059375" y2="0.612140625" layer="21"/>
<rectangle x1="7.53618125" y1="0.607059375" x2="7.6327" y2="0.612140625" layer="21"/>
<rectangle x1="8.1407" y1="0.607059375" x2="8.23721875" y2="0.612140625" layer="21"/>
<rectangle x1="0.1397" y1="0.612140625" x2="0.18541875" y2="0.61721875" layer="21"/>
<rectangle x1="0.510540625" y1="0.612140625" x2="0.7493" y2="0.61721875" layer="21"/>
<rectangle x1="0.891540625" y1="0.612140625" x2="1.10998125" y2="0.61721875" layer="21"/>
<rectangle x1="1.26238125" y1="0.612140625" x2="1.49098125" y2="0.61721875" layer="21"/>
<rectangle x1="1.7145" y1="0.612140625" x2="1.9431" y2="0.61721875" layer="21"/>
<rectangle x1="2.161540625" y1="0.612140625" x2="2.390140625" y2="0.61721875" layer="21"/>
<rectangle x1="2.5527" y1="0.612140625" x2="2.78638125" y2="0.61721875" layer="21"/>
<rectangle x1="3.0861" y1="0.612140625" x2="3.324859375" y2="0.61721875" layer="21"/>
<rectangle x1="3.4671" y1="0.612140625" x2="3.69061875" y2="0.61721875" layer="21"/>
<rectangle x1="3.964940625" y1="0.612140625" x2="4.188459375" y2="0.61721875" layer="21"/>
<rectangle x1="4.19861875" y1="0.612140625" x2="4.40181875" y2="0.61721875" layer="21"/>
<rectangle x1="4.6609" y1="0.612140625" x2="4.75741875" y2="0.61721875" layer="21"/>
<rectangle x1="5.280659375" y1="0.612140625" x2="5.37718125" y2="0.61721875" layer="21"/>
<rectangle x1="5.58038125" y1="0.612140625" x2="5.6769" y2="0.61721875" layer="21"/>
<rectangle x1="6.06298125" y1="0.612140625" x2="6.16458125" y2="0.61721875" layer="21"/>
<rectangle x1="6.4135" y1="0.612140625" x2="6.51001875" y2="0.61721875" layer="21"/>
<rectangle x1="6.74878125" y1="0.612140625" x2="6.8453" y2="0.61721875" layer="21"/>
<rectangle x1="7.24661875" y1="0.612140625" x2="7.33298125" y2="0.61721875" layer="21"/>
<rectangle x1="7.5311" y1="0.612140625" x2="7.62761875" y2="0.61721875" layer="21"/>
<rectangle x1="8.1407" y1="0.612140625" x2="8.23721875" y2="0.61721875" layer="21"/>
<rectangle x1="0.14478125" y1="0.61721875" x2="0.180340625" y2="0.6223" layer="21"/>
<rectangle x1="0.510540625" y1="0.61721875" x2="0.7493" y2="0.6223" layer="21"/>
<rectangle x1="0.886459375" y1="0.61721875" x2="1.115059375" y2="0.6223" layer="21"/>
<rectangle x1="1.267459375" y1="0.61721875" x2="1.49098125" y2="0.6223" layer="21"/>
<rectangle x1="1.7145" y1="0.61721875" x2="1.93801875" y2="0.6223" layer="21"/>
<rectangle x1="2.16661875" y1="0.61721875" x2="2.390140625" y2="0.6223" layer="21"/>
<rectangle x1="2.55778125" y1="0.61721875" x2="2.78638125" y2="0.6223" layer="21"/>
<rectangle x1="3.09118125" y1="0.61721875" x2="3.324859375" y2="0.6223" layer="21"/>
<rectangle x1="3.46201875" y1="0.61721875" x2="3.685540625" y2="0.6223" layer="21"/>
<rectangle x1="3.959859375" y1="0.61721875" x2="4.18338125" y2="0.6223" layer="21"/>
<rectangle x1="4.19861875" y1="0.61721875" x2="4.4069" y2="0.6223" layer="21"/>
<rectangle x1="4.6609" y1="0.61721875" x2="4.75741875" y2="0.6223" layer="21"/>
<rectangle x1="5.285740625" y1="0.61721875" x2="5.37718125" y2="0.6223" layer="21"/>
<rectangle x1="5.58038125" y1="0.61721875" x2="5.67181875" y2="0.6223" layer="21"/>
<rectangle x1="6.068059375" y1="0.61721875" x2="6.16458125" y2="0.6223" layer="21"/>
<rectangle x1="6.41858125" y1="0.61721875" x2="6.51001875" y2="0.6223" layer="21"/>
<rectangle x1="6.753859375" y1="0.61721875" x2="6.8453" y2="0.6223" layer="21"/>
<rectangle x1="7.241540625" y1="0.61721875" x2="7.338059375" y2="0.6223" layer="21"/>
<rectangle x1="7.5311" y1="0.61721875" x2="7.62761875" y2="0.6223" layer="21"/>
<rectangle x1="8.1407" y1="0.61721875" x2="8.23721875" y2="0.6223" layer="21"/>
<rectangle x1="0.149859375" y1="0.6223" x2="0.175259375" y2="0.62738125" layer="21"/>
<rectangle x1="0.51561875" y1="0.6223" x2="0.7493" y2="0.62738125" layer="21"/>
<rectangle x1="0.891540625" y1="0.6223" x2="1.115059375" y2="0.62738125" layer="21"/>
<rectangle x1="1.267459375" y1="0.6223" x2="1.49098125" y2="0.62738125" layer="21"/>
<rectangle x1="1.7145" y1="0.6223" x2="1.93801875" y2="0.62738125" layer="21"/>
<rectangle x1="2.16661875" y1="0.6223" x2="2.390140625" y2="0.62738125" layer="21"/>
<rectangle x1="2.55778125" y1="0.6223" x2="2.7813" y2="0.62738125" layer="21"/>
<rectangle x1="3.09118125" y1="0.6223" x2="3.324859375" y2="0.62738125" layer="21"/>
<rectangle x1="3.4671" y1="0.6223" x2="3.69061875" y2="0.62738125" layer="21"/>
<rectangle x1="3.959859375" y1="0.6223" x2="4.18338125" y2="0.62738125" layer="21"/>
<rectangle x1="4.19861875" y1="0.6223" x2="4.4069" y2="0.62738125" layer="21"/>
<rectangle x1="4.66598125" y1="0.6223" x2="4.75741875" y2="0.62738125" layer="21"/>
<rectangle x1="5.285740625" y1="0.6223" x2="5.37718125" y2="0.62738125" layer="21"/>
<rectangle x1="5.58038125" y1="0.6223" x2="5.6769" y2="0.62738125" layer="21"/>
<rectangle x1="6.068059375" y1="0.6223" x2="6.16458125" y2="0.62738125" layer="21"/>
<rectangle x1="6.4135" y1="0.6223" x2="6.51001875" y2="0.62738125" layer="21"/>
<rectangle x1="6.753859375" y1="0.6223" x2="6.8453" y2="0.62738125" layer="21"/>
<rectangle x1="7.24661875" y1="0.6223" x2="7.338059375" y2="0.62738125" layer="21"/>
<rectangle x1="7.5311" y1="0.6223" x2="7.62761875" y2="0.62738125" layer="21"/>
<rectangle x1="8.1407" y1="0.6223" x2="8.23721875" y2="0.62738125" layer="21"/>
<rectangle x1="0.149859375" y1="0.62738125" x2="0.17018125" y2="0.632459375" layer="21"/>
<rectangle x1="0.51561875" y1="0.62738125" x2="0.7493" y2="0.632459375" layer="21"/>
<rectangle x1="0.886459375" y1="0.62738125" x2="1.10998125" y2="0.632459375" layer="21"/>
<rectangle x1="1.267459375" y1="0.62738125" x2="1.49098125" y2="0.632459375" layer="21"/>
<rectangle x1="1.7145" y1="0.62738125" x2="1.9431" y2="0.632459375" layer="21"/>
<rectangle x1="2.16661875" y1="0.62738125" x2="2.390140625" y2="0.632459375" layer="21"/>
<rectangle x1="2.55778125" y1="0.62738125" x2="2.77621875" y2="0.632459375" layer="21"/>
<rectangle x1="3.096259375" y1="0.62738125" x2="3.324859375" y2="0.632459375" layer="21"/>
<rectangle x1="3.46201875" y1="0.62738125" x2="3.69061875" y2="0.632459375" layer="21"/>
<rectangle x1="3.959859375" y1="0.62738125" x2="4.18338125" y2="0.632459375" layer="21"/>
<rectangle x1="4.2037" y1="0.62738125" x2="4.41198125" y2="0.632459375" layer="21"/>
<rectangle x1="4.6609" y1="0.62738125" x2="4.75741875" y2="0.632459375" layer="21"/>
<rectangle x1="5.285740625" y1="0.62738125" x2="5.37718125" y2="0.632459375" layer="21"/>
<rectangle x1="5.585459375" y1="0.62738125" x2="5.67181875" y2="0.632459375" layer="21"/>
<rectangle x1="6.068059375" y1="0.62738125" x2="6.16458125" y2="0.632459375" layer="21"/>
<rectangle x1="6.41858125" y1="0.62738125" x2="6.504940625" y2="0.632459375" layer="21"/>
<rectangle x1="6.74878125" y1="0.62738125" x2="6.8453" y2="0.632459375" layer="21"/>
<rectangle x1="7.241540625" y1="0.62738125" x2="7.33298125" y2="0.632459375" layer="21"/>
<rectangle x1="7.5311" y1="0.62738125" x2="7.62761875" y2="0.632459375" layer="21"/>
<rectangle x1="8.14578125" y1="0.62738125" x2="8.23721875" y2="0.632459375" layer="21"/>
<rectangle x1="0.154940625" y1="0.632459375" x2="0.17018125" y2="0.637540625" layer="21"/>
<rectangle x1="0.51561875" y1="0.632459375" x2="0.7493" y2="0.637540625" layer="21"/>
<rectangle x1="0.891540625" y1="0.632459375" x2="1.115059375" y2="0.637540625" layer="21"/>
<rectangle x1="1.267459375" y1="0.632459375" x2="1.49098125" y2="0.637540625" layer="21"/>
<rectangle x1="1.71958125" y1="0.632459375" x2="1.9431" y2="0.637540625" layer="21"/>
<rectangle x1="2.16661875" y1="0.632459375" x2="2.390140625" y2="0.637540625" layer="21"/>
<rectangle x1="2.5527" y1="0.632459375" x2="2.77621875" y2="0.637540625" layer="21"/>
<rectangle x1="3.096259375" y1="0.632459375" x2="3.329940625" y2="0.637540625" layer="21"/>
<rectangle x1="3.4671" y1="0.632459375" x2="3.69061875" y2="0.637540625" layer="21"/>
<rectangle x1="3.95478125" y1="0.632459375" x2="4.1783" y2="0.637540625" layer="21"/>
<rectangle x1="4.2037" y1="0.632459375" x2="4.41198125" y2="0.637540625" layer="21"/>
<rectangle x1="4.6609" y1="0.632459375" x2="4.752340625" y2="0.637540625" layer="21"/>
<rectangle x1="5.285740625" y1="0.632459375" x2="5.37718125" y2="0.637540625" layer="21"/>
<rectangle x1="5.58038125" y1="0.632459375" x2="5.67181875" y2="0.637540625" layer="21"/>
<rectangle x1="6.068059375" y1="0.632459375" x2="6.16458125" y2="0.637540625" layer="21"/>
<rectangle x1="6.4135" y1="0.632459375" x2="6.504940625" y2="0.637540625" layer="21"/>
<rectangle x1="6.74878125" y1="0.632459375" x2="6.8453" y2="0.637540625" layer="21"/>
<rectangle x1="7.241540625" y1="0.632459375" x2="7.338059375" y2="0.637540625" layer="21"/>
<rectangle x1="7.5311" y1="0.632459375" x2="7.622540625" y2="0.637540625" layer="21"/>
<rectangle x1="8.14578125" y1="0.632459375" x2="8.2423" y2="0.637540625" layer="21"/>
<rectangle x1="0.51561875" y1="0.637540625" x2="0.7493" y2="0.64261875" layer="21"/>
<rectangle x1="0.886459375" y1="0.637540625" x2="1.10998125" y2="0.64261875" layer="21"/>
<rectangle x1="1.26238125" y1="0.637540625" x2="1.49098125" y2="0.64261875" layer="21"/>
<rectangle x1="1.7145" y1="0.637540625" x2="1.93801875" y2="0.64261875" layer="21"/>
<rectangle x1="2.16661875" y1="0.637540625" x2="2.390140625" y2="0.64261875" layer="21"/>
<rectangle x1="2.55778125" y1="0.637540625" x2="2.77621875" y2="0.64261875" layer="21"/>
<rectangle x1="3.101340625" y1="0.637540625" x2="3.329940625" y2="0.64261875" layer="21"/>
<rectangle x1="3.46201875" y1="0.637540625" x2="3.685540625" y2="0.64261875" layer="21"/>
<rectangle x1="3.95478125" y1="0.637540625" x2="4.1783" y2="0.64261875" layer="21"/>
<rectangle x1="4.2037" y1="0.637540625" x2="4.41198125" y2="0.64261875" layer="21"/>
<rectangle x1="4.6609" y1="0.637540625" x2="4.752340625" y2="0.64261875" layer="21"/>
<rectangle x1="5.29081875" y1="0.637540625" x2="5.37718125" y2="0.64261875" layer="21"/>
<rectangle x1="5.58038125" y1="0.637540625" x2="5.67181875" y2="0.64261875" layer="21"/>
<rectangle x1="6.068059375" y1="0.637540625" x2="6.16458125" y2="0.64261875" layer="21"/>
<rectangle x1="6.4135" y1="0.637540625" x2="6.51001875" y2="0.64261875" layer="21"/>
<rectangle x1="6.753859375" y1="0.637540625" x2="6.84021875" y2="0.64261875" layer="21"/>
<rectangle x1="7.24661875" y1="0.637540625" x2="7.338059375" y2="0.64261875" layer="21"/>
<rectangle x1="7.5311" y1="0.637540625" x2="7.622540625" y2="0.64261875" layer="21"/>
<rectangle x1="8.14578125" y1="0.637540625" x2="8.23721875" y2="0.64261875" layer="21"/>
<rectangle x1="0.51561875" y1="0.64261875" x2="0.7493" y2="0.6477" layer="21"/>
<rectangle x1="0.891540625" y1="0.64261875" x2="1.115059375" y2="0.6477" layer="21"/>
<rectangle x1="1.267459375" y1="0.64261875" x2="1.49098125" y2="0.6477" layer="21"/>
<rectangle x1="1.7145" y1="0.64261875" x2="1.93801875" y2="0.6477" layer="21"/>
<rectangle x1="2.16661875" y1="0.64261875" x2="2.390140625" y2="0.6477" layer="21"/>
<rectangle x1="2.55778125" y1="0.64261875" x2="2.771140625" y2="0.6477" layer="21"/>
<rectangle x1="3.101340625" y1="0.64261875" x2="3.329940625" y2="0.6477" layer="21"/>
<rectangle x1="3.4671" y1="0.64261875" x2="3.69061875" y2="0.6477" layer="21"/>
<rectangle x1="3.9497" y1="0.64261875" x2="4.17321875" y2="0.6477" layer="21"/>
<rectangle x1="4.20878125" y1="0.64261875" x2="4.417059375" y2="0.6477" layer="21"/>
<rectangle x1="4.6609" y1="0.64261875" x2="4.752340625" y2="0.6477" layer="21"/>
<rectangle x1="5.29081875" y1="0.64261875" x2="5.37718125" y2="0.6477" layer="21"/>
<rectangle x1="5.58038125" y1="0.64261875" x2="5.6769" y2="0.6477" layer="21"/>
<rectangle x1="6.073140625" y1="0.64261875" x2="6.16458125" y2="0.6477" layer="21"/>
<rectangle x1="6.4135" y1="0.64261875" x2="6.51001875" y2="0.6477" layer="21"/>
<rectangle x1="6.74878125" y1="0.64261875" x2="6.8453" y2="0.6477" layer="21"/>
<rectangle x1="7.24661875" y1="0.64261875" x2="7.33298125" y2="0.6477" layer="21"/>
<rectangle x1="7.5311" y1="0.64261875" x2="7.622540625" y2="0.6477" layer="21"/>
<rectangle x1="8.14578125" y1="0.64261875" x2="8.23721875" y2="0.6477" layer="21"/>
<rectangle x1="0.51561875" y1="0.6477" x2="0.7493" y2="0.65278125" layer="21"/>
<rectangle x1="0.886459375" y1="0.6477" x2="1.115059375" y2="0.65278125" layer="21"/>
<rectangle x1="1.267459375" y1="0.6477" x2="1.49098125" y2="0.65278125" layer="21"/>
<rectangle x1="1.7145" y1="0.6477" x2="1.93801875" y2="0.65278125" layer="21"/>
<rectangle x1="2.16661875" y1="0.6477" x2="2.390140625" y2="0.65278125" layer="21"/>
<rectangle x1="2.55778125" y1="0.6477" x2="2.771140625" y2="0.65278125" layer="21"/>
<rectangle x1="3.101340625" y1="0.6477" x2="3.329940625" y2="0.65278125" layer="21"/>
<rectangle x1="3.46201875" y1="0.6477" x2="3.69061875" y2="0.65278125" layer="21"/>
<rectangle x1="3.9497" y1="0.6477" x2="4.17321875" y2="0.65278125" layer="21"/>
<rectangle x1="4.20878125" y1="0.6477" x2="4.417059375" y2="0.65278125" layer="21"/>
<rectangle x1="4.65581875" y1="0.6477" x2="4.752340625" y2="0.65278125" layer="21"/>
<rectangle x1="5.29081875" y1="0.6477" x2="5.37718125" y2="0.65278125" layer="21"/>
<rectangle x1="5.58038125" y1="0.6477" x2="5.67181875" y2="0.65278125" layer="21"/>
<rectangle x1="6.068059375" y1="0.6477" x2="6.16458125" y2="0.65278125" layer="21"/>
<rectangle x1="6.41858125" y1="0.6477" x2="6.504940625" y2="0.65278125" layer="21"/>
<rectangle x1="6.753859375" y1="0.6477" x2="6.8453" y2="0.65278125" layer="21"/>
<rectangle x1="7.241540625" y1="0.6477" x2="7.338059375" y2="0.65278125" layer="21"/>
<rectangle x1="7.52601875" y1="0.6477" x2="7.622540625" y2="0.65278125" layer="21"/>
<rectangle x1="8.14578125" y1="0.6477" x2="8.2423" y2="0.65278125" layer="21"/>
<rectangle x1="0.51561875" y1="0.65278125" x2="0.7493" y2="0.657859375" layer="21"/>
<rectangle x1="0.886459375" y1="0.65278125" x2="1.115059375" y2="0.657859375" layer="21"/>
<rectangle x1="1.26238125" y1="0.65278125" x2="1.49098125" y2="0.657859375" layer="21"/>
<rectangle x1="1.71958125" y1="0.65278125" x2="1.9431" y2="0.657859375" layer="21"/>
<rectangle x1="2.16661875" y1="0.65278125" x2="2.390140625" y2="0.657859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.65278125" x2="2.771140625" y2="0.657859375" layer="21"/>
<rectangle x1="3.101340625" y1="0.65278125" x2="3.329940625" y2="0.657859375" layer="21"/>
<rectangle x1="3.4671" y1="0.65278125" x2="3.69061875" y2="0.657859375" layer="21"/>
<rectangle x1="3.94461875" y1="0.65278125" x2="4.17321875" y2="0.657859375" layer="21"/>
<rectangle x1="4.213859375" y1="0.65278125" x2="4.417059375" y2="0.657859375" layer="21"/>
<rectangle x1="4.65581875" y1="0.65278125" x2="4.752340625" y2="0.657859375" layer="21"/>
<rectangle x1="5.29081875" y1="0.65278125" x2="5.37718125" y2="0.657859375" layer="21"/>
<rectangle x1="5.58038125" y1="0.65278125" x2="5.6769" y2="0.657859375" layer="21"/>
<rectangle x1="6.073140625" y1="0.65278125" x2="6.16458125" y2="0.657859375" layer="21"/>
<rectangle x1="6.41858125" y1="0.65278125" x2="6.51001875" y2="0.657859375" layer="21"/>
<rectangle x1="6.74878125" y1="0.65278125" x2="6.8453" y2="0.657859375" layer="21"/>
<rectangle x1="7.24661875" y1="0.65278125" x2="7.33298125" y2="0.657859375" layer="21"/>
<rectangle x1="7.5311" y1="0.65278125" x2="7.622540625" y2="0.657859375" layer="21"/>
<rectangle x1="8.14578125" y1="0.65278125" x2="8.2423" y2="0.657859375" layer="21"/>
<rectangle x1="0.51561875" y1="0.657859375" x2="0.7493" y2="0.662940625" layer="21"/>
<rectangle x1="0.891540625" y1="0.657859375" x2="1.10998125" y2="0.662940625" layer="21"/>
<rectangle x1="1.267459375" y1="0.657859375" x2="1.49098125" y2="0.662940625" layer="21"/>
<rectangle x1="1.7145" y1="0.657859375" x2="1.93801875" y2="0.662940625" layer="21"/>
<rectangle x1="2.16661875" y1="0.657859375" x2="2.390140625" y2="0.662940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.657859375" x2="2.771140625" y2="0.662940625" layer="21"/>
<rectangle x1="3.10641875" y1="0.657859375" x2="3.329940625" y2="0.662940625" layer="21"/>
<rectangle x1="3.46201875" y1="0.657859375" x2="3.685540625" y2="0.662940625" layer="21"/>
<rectangle x1="3.94461875" y1="0.657859375" x2="4.168140625" y2="0.662940625" layer="21"/>
<rectangle x1="4.213859375" y1="0.657859375" x2="4.422140625" y2="0.662940625" layer="21"/>
<rectangle x1="4.65581875" y1="0.657859375" x2="4.752340625" y2="0.662940625" layer="21"/>
<rectangle x1="5.29081875" y1="0.657859375" x2="5.37718125" y2="0.662940625" layer="21"/>
<rectangle x1="5.58038125" y1="0.657859375" x2="5.67181875" y2="0.662940625" layer="21"/>
<rectangle x1="6.073140625" y1="0.657859375" x2="6.16458125" y2="0.662940625" layer="21"/>
<rectangle x1="6.4135" y1="0.657859375" x2="6.504940625" y2="0.662940625" layer="21"/>
<rectangle x1="6.753859375" y1="0.657859375" x2="6.8453" y2="0.662940625" layer="21"/>
<rectangle x1="7.241540625" y1="0.657859375" x2="7.338059375" y2="0.662940625" layer="21"/>
<rectangle x1="7.52601875" y1="0.657859375" x2="7.617459375" y2="0.662940625" layer="21"/>
<rectangle x1="8.150859375" y1="0.657859375" x2="8.2423" y2="0.662940625" layer="21"/>
<rectangle x1="0.51561875" y1="0.662940625" x2="0.7493" y2="0.66801875" layer="21"/>
<rectangle x1="0.886459375" y1="0.662940625" x2="1.115059375" y2="0.66801875" layer="21"/>
<rectangle x1="1.267459375" y1="0.662940625" x2="1.49098125" y2="0.66801875" layer="21"/>
<rectangle x1="1.7145" y1="0.662940625" x2="1.9431" y2="0.66801875" layer="21"/>
<rectangle x1="2.16661875" y1="0.662940625" x2="2.390140625" y2="0.66801875" layer="21"/>
<rectangle x1="2.55778125" y1="0.662940625" x2="2.766059375" y2="0.66801875" layer="21"/>
<rectangle x1="3.10641875" y1="0.662940625" x2="3.329940625" y2="0.66801875" layer="21"/>
<rectangle x1="3.4671" y1="0.662940625" x2="3.69061875" y2="0.66801875" layer="21"/>
<rectangle x1="3.94461875" y1="0.662940625" x2="4.168140625" y2="0.66801875" layer="21"/>
<rectangle x1="4.213859375" y1="0.662940625" x2="4.422140625" y2="0.66801875" layer="21"/>
<rectangle x1="4.65581875" y1="0.662940625" x2="4.747259375" y2="0.66801875" layer="21"/>
<rectangle x1="5.29081875" y1="0.662940625" x2="5.37718125" y2="0.66801875" layer="21"/>
<rectangle x1="5.58038125" y1="0.662940625" x2="5.67181875" y2="0.66801875" layer="21"/>
<rectangle x1="6.068059375" y1="0.662940625" x2="6.16458125" y2="0.66801875" layer="21"/>
<rectangle x1="6.41858125" y1="0.662940625" x2="6.51001875" y2="0.66801875" layer="21"/>
<rectangle x1="6.74878125" y1="0.662940625" x2="6.8453" y2="0.66801875" layer="21"/>
<rectangle x1="7.241540625" y1="0.662940625" x2="7.338059375" y2="0.66801875" layer="21"/>
<rectangle x1="7.52601875" y1="0.662940625" x2="7.622540625" y2="0.66801875" layer="21"/>
<rectangle x1="8.14578125" y1="0.662940625" x2="8.2423" y2="0.66801875" layer="21"/>
<rectangle x1="0.51561875" y1="0.66801875" x2="0.7493" y2="0.6731" layer="21"/>
<rectangle x1="0.891540625" y1="0.66801875" x2="1.115059375" y2="0.6731" layer="21"/>
<rectangle x1="1.267459375" y1="0.66801875" x2="1.49098125" y2="0.6731" layer="21"/>
<rectangle x1="1.7145" y1="0.66801875" x2="1.93801875" y2="0.6731" layer="21"/>
<rectangle x1="2.16661875" y1="0.66801875" x2="2.390140625" y2="0.6731" layer="21"/>
<rectangle x1="2.5527" y1="0.66801875" x2="2.766059375" y2="0.6731" layer="21"/>
<rectangle x1="3.10641875" y1="0.66801875" x2="3.33501875" y2="0.6731" layer="21"/>
<rectangle x1="3.46201875" y1="0.66801875" x2="3.69061875" y2="0.6731" layer="21"/>
<rectangle x1="3.939540625" y1="0.66801875" x2="4.163059375" y2="0.6731" layer="21"/>
<rectangle x1="4.213859375" y1="0.66801875" x2="4.42721875" y2="0.6731" layer="21"/>
<rectangle x1="4.65581875" y1="0.66801875" x2="4.752340625" y2="0.6731" layer="21"/>
<rectangle x1="5.2959" y1="0.66801875" x2="5.37718125" y2="0.6731" layer="21"/>
<rectangle x1="5.58038125" y1="0.66801875" x2="5.67181875" y2="0.6731" layer="21"/>
<rectangle x1="6.073140625" y1="0.66801875" x2="6.16458125" y2="0.6731" layer="21"/>
<rectangle x1="6.4135" y1="0.66801875" x2="6.504940625" y2="0.6731" layer="21"/>
<rectangle x1="6.753859375" y1="0.66801875" x2="6.8453" y2="0.6731" layer="21"/>
<rectangle x1="7.24661875" y1="0.66801875" x2="7.33298125" y2="0.6731" layer="21"/>
<rectangle x1="7.52601875" y1="0.66801875" x2="7.622540625" y2="0.6731" layer="21"/>
<rectangle x1="8.150859375" y1="0.66801875" x2="8.2423" y2="0.6731" layer="21"/>
<rectangle x1="0.510540625" y1="0.6731" x2="0.7493" y2="0.67818125" layer="21"/>
<rectangle x1="0.886459375" y1="0.6731" x2="1.10998125" y2="0.67818125" layer="21"/>
<rectangle x1="1.267459375" y1="0.6731" x2="1.49098125" y2="0.67818125" layer="21"/>
<rectangle x1="1.7145" y1="0.6731" x2="1.9431" y2="0.67818125" layer="21"/>
<rectangle x1="2.161540625" y1="0.6731" x2="2.390140625" y2="0.67818125" layer="21"/>
<rectangle x1="2.55778125" y1="0.6731" x2="2.766059375" y2="0.67818125" layer="21"/>
<rectangle x1="3.10641875" y1="0.6731" x2="3.329940625" y2="0.67818125" layer="21"/>
<rectangle x1="3.4671" y1="0.6731" x2="3.69061875" y2="0.67818125" layer="21"/>
<rectangle x1="3.939540625" y1="0.6731" x2="4.163059375" y2="0.67818125" layer="21"/>
<rectangle x1="4.218940625" y1="0.6731" x2="4.42721875" y2="0.67818125" layer="21"/>
<rectangle x1="4.65581875" y1="0.6731" x2="4.747259375" y2="0.67818125" layer="21"/>
<rectangle x1="5.2959" y1="0.6731" x2="5.37718125" y2="0.67818125" layer="21"/>
<rectangle x1="5.58038125" y1="0.6731" x2="5.67181875" y2="0.67818125" layer="21"/>
<rectangle x1="6.073140625" y1="0.6731" x2="6.16458125" y2="0.67818125" layer="21"/>
<rectangle x1="6.41858125" y1="0.6731" x2="6.51001875" y2="0.67818125" layer="21"/>
<rectangle x1="6.753859375" y1="0.6731" x2="6.8453" y2="0.67818125" layer="21"/>
<rectangle x1="7.24661875" y1="0.6731" x2="7.338059375" y2="0.67818125" layer="21"/>
<rectangle x1="7.52601875" y1="0.6731" x2="7.617459375" y2="0.67818125" layer="21"/>
<rectangle x1="8.150859375" y1="0.6731" x2="8.2423" y2="0.67818125" layer="21"/>
<rectangle x1="0.510540625" y1="0.67818125" x2="0.7493" y2="0.683259375" layer="21"/>
<rectangle x1="0.891540625" y1="0.67818125" x2="1.115059375" y2="0.683259375" layer="21"/>
<rectangle x1="1.26238125" y1="0.67818125" x2="1.49098125" y2="0.683259375" layer="21"/>
<rectangle x1="1.7145" y1="0.67818125" x2="1.93801875" y2="0.683259375" layer="21"/>
<rectangle x1="2.16661875" y1="0.67818125" x2="2.390140625" y2="0.683259375" layer="21"/>
<rectangle x1="2.55778125" y1="0.67818125" x2="2.766059375" y2="0.683259375" layer="21"/>
<rectangle x1="3.1115" y1="0.67818125" x2="3.33501875" y2="0.683259375" layer="21"/>
<rectangle x1="3.46201875" y1="0.67818125" x2="3.685540625" y2="0.683259375" layer="21"/>
<rectangle x1="3.934459375" y1="0.67818125" x2="4.163059375" y2="0.683259375" layer="21"/>
<rectangle x1="4.218940625" y1="0.67818125" x2="4.42721875" y2="0.683259375" layer="21"/>
<rectangle x1="4.65581875" y1="0.67818125" x2="4.752340625" y2="0.683259375" layer="21"/>
<rectangle x1="5.29081875" y1="0.67818125" x2="5.37718125" y2="0.683259375" layer="21"/>
<rectangle x1="5.585459375" y1="0.67818125" x2="5.6769" y2="0.683259375" layer="21"/>
<rectangle x1="6.073140625" y1="0.67818125" x2="6.16458125" y2="0.683259375" layer="21"/>
<rectangle x1="6.4135" y1="0.67818125" x2="6.51001875" y2="0.683259375" layer="21"/>
<rectangle x1="6.74878125" y1="0.67818125" x2="6.84021875" y2="0.683259375" layer="21"/>
<rectangle x1="7.241540625" y1="0.67818125" x2="7.338059375" y2="0.683259375" layer="21"/>
<rectangle x1="7.52601875" y1="0.67818125" x2="7.617459375" y2="0.683259375" layer="21"/>
<rectangle x1="8.150859375" y1="0.67818125" x2="8.2423" y2="0.683259375" layer="21"/>
<rectangle x1="0.510540625" y1="0.683259375" x2="0.7493" y2="0.688340625" layer="21"/>
<rectangle x1="0.886459375" y1="0.683259375" x2="1.10998125" y2="0.688340625" layer="21"/>
<rectangle x1="1.267459375" y1="0.683259375" x2="1.49098125" y2="0.688340625" layer="21"/>
<rectangle x1="1.71958125" y1="0.683259375" x2="1.93801875" y2="0.688340625" layer="21"/>
<rectangle x1="2.16661875" y1="0.683259375" x2="2.390140625" y2="0.688340625" layer="21"/>
<rectangle x1="2.55778125" y1="0.683259375" x2="2.766059375" y2="0.688340625" layer="21"/>
<rectangle x1="3.10641875" y1="0.683259375" x2="3.33501875" y2="0.688340625" layer="21"/>
<rectangle x1="3.4671" y1="0.683259375" x2="3.69061875" y2="0.688340625" layer="21"/>
<rectangle x1="3.934459375" y1="0.683259375" x2="4.15798125" y2="0.688340625" layer="21"/>
<rectangle x1="4.218940625" y1="0.683259375" x2="4.4323" y2="0.688340625" layer="21"/>
<rectangle x1="4.65581875" y1="0.683259375" x2="4.747259375" y2="0.688340625" layer="21"/>
<rectangle x1="5.2959" y1="0.683259375" x2="5.37718125" y2="0.688340625" layer="21"/>
<rectangle x1="5.58038125" y1="0.683259375" x2="5.67181875" y2="0.688340625" layer="21"/>
<rectangle x1="6.073140625" y1="0.683259375" x2="6.16458125" y2="0.688340625" layer="21"/>
<rectangle x1="6.41858125" y1="0.683259375" x2="6.51001875" y2="0.688340625" layer="21"/>
<rectangle x1="6.74878125" y1="0.683259375" x2="6.8453" y2="0.688340625" layer="21"/>
<rectangle x1="7.24661875" y1="0.683259375" x2="7.33298125" y2="0.688340625" layer="21"/>
<rectangle x1="7.5311" y1="0.683259375" x2="7.622540625" y2="0.688340625" layer="21"/>
<rectangle x1="8.150859375" y1="0.683259375" x2="8.2423" y2="0.688340625" layer="21"/>
<rectangle x1="0.505459375" y1="0.688340625" x2="0.7493" y2="0.69341875" layer="21"/>
<rectangle x1="0.886459375" y1="0.688340625" x2="1.115059375" y2="0.69341875" layer="21"/>
<rectangle x1="1.267459375" y1="0.688340625" x2="1.49098125" y2="0.69341875" layer="21"/>
<rectangle x1="1.7145" y1="0.688340625" x2="1.9431" y2="0.69341875" layer="21"/>
<rectangle x1="2.16661875" y1="0.688340625" x2="2.390140625" y2="0.69341875" layer="21"/>
<rectangle x1="2.5527" y1="0.688340625" x2="2.766059375" y2="0.69341875" layer="21"/>
<rectangle x1="3.1115" y1="0.688340625" x2="3.33501875" y2="0.69341875" layer="21"/>
<rectangle x1="3.46201875" y1="0.688340625" x2="3.69061875" y2="0.69341875" layer="21"/>
<rectangle x1="3.92938125" y1="0.688340625" x2="4.15798125" y2="0.69341875" layer="21"/>
<rectangle x1="4.22401875" y1="0.688340625" x2="4.4323" y2="0.69341875" layer="21"/>
<rectangle x1="4.65581875" y1="0.688340625" x2="4.747259375" y2="0.69341875" layer="21"/>
<rectangle x1="5.2959" y1="0.688340625" x2="5.37718125" y2="0.69341875" layer="21"/>
<rectangle x1="5.58038125" y1="0.688340625" x2="5.6769" y2="0.69341875" layer="21"/>
<rectangle x1="6.073140625" y1="0.688340625" x2="6.16458125" y2="0.69341875" layer="21"/>
<rectangle x1="6.4135" y1="0.688340625" x2="6.504940625" y2="0.69341875" layer="21"/>
<rectangle x1="6.753859375" y1="0.688340625" x2="6.8453" y2="0.69341875" layer="21"/>
<rectangle x1="7.241540625" y1="0.688340625" x2="7.338059375" y2="0.69341875" layer="21"/>
<rectangle x1="7.52601875" y1="0.688340625" x2="7.617459375" y2="0.69341875" layer="21"/>
<rectangle x1="8.150859375" y1="0.688340625" x2="8.2423" y2="0.69341875" layer="21"/>
<rectangle x1="0.50038125" y1="0.69341875" x2="0.7493" y2="0.6985" layer="21"/>
<rectangle x1="0.886459375" y1="0.69341875" x2="1.115059375" y2="0.6985" layer="21"/>
<rectangle x1="1.26238125" y1="0.69341875" x2="1.49098125" y2="0.6985" layer="21"/>
<rectangle x1="1.7145" y1="0.69341875" x2="1.9431" y2="0.6985" layer="21"/>
<rectangle x1="2.16661875" y1="0.69341875" x2="2.390140625" y2="0.6985" layer="21"/>
<rectangle x1="2.55778125" y1="0.69341875" x2="2.76098125" y2="0.6985" layer="21"/>
<rectangle x1="3.10641875" y1="0.69341875" x2="3.33501875" y2="0.6985" layer="21"/>
<rectangle x1="3.4671" y1="0.69341875" x2="3.685540625" y2="0.6985" layer="21"/>
<rectangle x1="3.92938125" y1="0.69341875" x2="4.15798125" y2="0.6985" layer="21"/>
<rectangle x1="4.22401875" y1="0.69341875" x2="4.43738125" y2="0.6985" layer="21"/>
<rectangle x1="4.65581875" y1="0.69341875" x2="4.752340625" y2="0.6985" layer="21"/>
<rectangle x1="5.2959" y1="0.69341875" x2="5.37718125" y2="0.6985" layer="21"/>
<rectangle x1="5.58038125" y1="0.69341875" x2="5.6769" y2="0.6985" layer="21"/>
<rectangle x1="6.073140625" y1="0.69341875" x2="6.16458125" y2="0.6985" layer="21"/>
<rectangle x1="6.41858125" y1="0.69341875" x2="6.504940625" y2="0.6985" layer="21"/>
<rectangle x1="6.74878125" y1="0.69341875" x2="6.8453" y2="0.6985" layer="21"/>
<rectangle x1="7.241540625" y1="0.69341875" x2="7.338059375" y2="0.6985" layer="21"/>
<rectangle x1="7.52601875" y1="0.69341875" x2="7.622540625" y2="0.6985" layer="21"/>
<rectangle x1="8.14578125" y1="0.69341875" x2="8.2423" y2="0.6985" layer="21"/>
<rectangle x1="0.50038125" y1="0.6985" x2="0.7493" y2="0.70358125" layer="21"/>
<rectangle x1="0.891540625" y1="0.6985" x2="1.10998125" y2="0.70358125" layer="21"/>
<rectangle x1="1.267459375" y1="0.6985" x2="1.49098125" y2="0.70358125" layer="21"/>
<rectangle x1="1.7145" y1="0.6985" x2="1.93801875" y2="0.70358125" layer="21"/>
<rectangle x1="2.16661875" y1="0.6985" x2="2.390140625" y2="0.70358125" layer="21"/>
<rectangle x1="2.55778125" y1="0.6985" x2="2.766059375" y2="0.70358125" layer="21"/>
<rectangle x1="3.1115" y1="0.6985" x2="3.33501875" y2="0.70358125" layer="21"/>
<rectangle x1="3.46201875" y1="0.6985" x2="3.69061875" y2="0.70358125" layer="21"/>
<rectangle x1="3.9243" y1="0.6985" x2="4.1529" y2="0.70358125" layer="21"/>
<rectangle x1="4.22401875" y1="0.6985" x2="4.43738125" y2="0.70358125" layer="21"/>
<rectangle x1="4.65581875" y1="0.6985" x2="4.747259375" y2="0.70358125" layer="21"/>
<rectangle x1="5.2959" y1="0.6985" x2="5.37718125" y2="0.70358125" layer="21"/>
<rectangle x1="5.58038125" y1="0.6985" x2="5.67181875" y2="0.70358125" layer="21"/>
<rectangle x1="6.073140625" y1="0.6985" x2="6.16458125" y2="0.70358125" layer="21"/>
<rectangle x1="6.4135" y1="0.6985" x2="6.51001875" y2="0.70358125" layer="21"/>
<rectangle x1="6.753859375" y1="0.6985" x2="6.8453" y2="0.70358125" layer="21"/>
<rectangle x1="7.24661875" y1="0.6985" x2="7.33298125" y2="0.70358125" layer="21"/>
<rectangle x1="7.52601875" y1="0.6985" x2="7.617459375" y2="0.70358125" layer="21"/>
<rectangle x1="8.150859375" y1="0.6985" x2="8.2423" y2="0.70358125" layer="21"/>
<rectangle x1="0.4953" y1="0.70358125" x2="0.7493" y2="0.708659375" layer="21"/>
<rectangle x1="0.886459375" y1="0.70358125" x2="1.115059375" y2="0.708659375" layer="21"/>
<rectangle x1="1.267459375" y1="0.70358125" x2="1.49098125" y2="0.708659375" layer="21"/>
<rectangle x1="1.7145" y1="0.70358125" x2="1.93801875" y2="0.708659375" layer="21"/>
<rectangle x1="2.16661875" y1="0.70358125" x2="2.390140625" y2="0.708659375" layer="21"/>
<rectangle x1="2.55778125" y1="0.70358125" x2="2.76098125" y2="0.708659375" layer="21"/>
<rectangle x1="3.10641875" y1="0.70358125" x2="3.33501875" y2="0.708659375" layer="21"/>
<rectangle x1="3.4671" y1="0.70358125" x2="3.685540625" y2="0.708659375" layer="21"/>
<rectangle x1="3.9243" y1="0.70358125" x2="4.1529" y2="0.708659375" layer="21"/>
<rectangle x1="4.2291" y1="0.70358125" x2="4.43738125" y2="0.708659375" layer="21"/>
<rectangle x1="4.65581875" y1="0.70358125" x2="4.747259375" y2="0.708659375" layer="21"/>
<rectangle x1="5.2959" y1="0.70358125" x2="5.37718125" y2="0.708659375" layer="21"/>
<rectangle x1="5.58038125" y1="0.70358125" x2="5.67181875" y2="0.708659375" layer="21"/>
<rectangle x1="6.073140625" y1="0.70358125" x2="6.16458125" y2="0.708659375" layer="21"/>
<rectangle x1="6.41858125" y1="0.70358125" x2="6.51001875" y2="0.708659375" layer="21"/>
<rectangle x1="6.753859375" y1="0.70358125" x2="6.8453" y2="0.708659375" layer="21"/>
<rectangle x1="7.24661875" y1="0.70358125" x2="7.338059375" y2="0.708659375" layer="21"/>
<rectangle x1="7.52601875" y1="0.70358125" x2="7.617459375" y2="0.708659375" layer="21"/>
<rectangle x1="8.150859375" y1="0.70358125" x2="8.2423" y2="0.708659375" layer="21"/>
<rectangle x1="0.49021875" y1="0.708659375" x2="0.7493" y2="0.713740625" layer="21"/>
<rectangle x1="0.891540625" y1="0.708659375" x2="1.10998125" y2="0.713740625" layer="21"/>
<rectangle x1="1.267459375" y1="0.708659375" x2="1.49098125" y2="0.713740625" layer="21"/>
<rectangle x1="1.7145" y1="0.708659375" x2="1.93801875" y2="0.713740625" layer="21"/>
<rectangle x1="2.16661875" y1="0.708659375" x2="2.390140625" y2="0.713740625" layer="21"/>
<rectangle x1="2.5527" y1="0.708659375" x2="2.766059375" y2="0.713740625" layer="21"/>
<rectangle x1="3.1115" y1="0.708659375" x2="3.33501875" y2="0.713740625" layer="21"/>
<rectangle x1="3.46201875" y1="0.708659375" x2="3.69061875" y2="0.713740625" layer="21"/>
<rectangle x1="3.9243" y1="0.708659375" x2="4.14781875" y2="0.713740625" layer="21"/>
<rectangle x1="4.2291" y1="0.708659375" x2="4.442459375" y2="0.713740625" layer="21"/>
<rectangle x1="4.65581875" y1="0.708659375" x2="4.752340625" y2="0.713740625" layer="21"/>
<rectangle x1="5.29081875" y1="0.708659375" x2="5.37718125" y2="0.713740625" layer="21"/>
<rectangle x1="5.58038125" y1="0.708659375" x2="5.67181875" y2="0.713740625" layer="21"/>
<rectangle x1="6.073140625" y1="0.708659375" x2="6.16458125" y2="0.713740625" layer="21"/>
<rectangle x1="6.4135" y1="0.708659375" x2="6.504940625" y2="0.713740625" layer="21"/>
<rectangle x1="6.74878125" y1="0.708659375" x2="6.8453" y2="0.713740625" layer="21"/>
<rectangle x1="7.241540625" y1="0.708659375" x2="7.338059375" y2="0.713740625" layer="21"/>
<rectangle x1="7.5311" y1="0.708659375" x2="7.622540625" y2="0.713740625" layer="21"/>
<rectangle x1="8.150859375" y1="0.708659375" x2="8.2423" y2="0.713740625" layer="21"/>
<rectangle x1="0.485140625" y1="0.713740625" x2="0.7493" y2="0.71881875" layer="21"/>
<rectangle x1="0.886459375" y1="0.713740625" x2="1.115059375" y2="0.71881875" layer="21"/>
<rectangle x1="1.267459375" y1="0.713740625" x2="1.49098125" y2="0.71881875" layer="21"/>
<rectangle x1="1.71958125" y1="0.713740625" x2="1.9431" y2="0.71881875" layer="21"/>
<rectangle x1="2.16661875" y1="0.713740625" x2="2.390140625" y2="0.71881875" layer="21"/>
<rectangle x1="2.55778125" y1="0.713740625" x2="2.766059375" y2="0.71881875" layer="21"/>
<rectangle x1="3.1115" y1="0.713740625" x2="3.33501875" y2="0.71881875" layer="21"/>
<rectangle x1="3.46201875" y1="0.713740625" x2="3.69061875" y2="0.71881875" layer="21"/>
<rectangle x1="3.91921875" y1="0.713740625" x2="4.14781875" y2="0.71881875" layer="21"/>
<rectangle x1="4.2291" y1="0.713740625" x2="4.442459375" y2="0.71881875" layer="21"/>
<rectangle x1="4.65581875" y1="0.713740625" x2="4.752340625" y2="0.71881875" layer="21"/>
<rectangle x1="5.29081875" y1="0.713740625" x2="5.37718125" y2="0.71881875" layer="21"/>
<rectangle x1="5.58038125" y1="0.713740625" x2="5.67181875" y2="0.71881875" layer="21"/>
<rectangle x1="6.073140625" y1="0.713740625" x2="6.16458125" y2="0.71881875" layer="21"/>
<rectangle x1="6.41858125" y1="0.713740625" x2="6.51001875" y2="0.71881875" layer="21"/>
<rectangle x1="6.74878125" y1="0.713740625" x2="6.8453" y2="0.71881875" layer="21"/>
<rectangle x1="7.24661875" y1="0.713740625" x2="7.338059375" y2="0.71881875" layer="21"/>
<rectangle x1="7.52601875" y1="0.713740625" x2="7.622540625" y2="0.71881875" layer="21"/>
<rectangle x1="8.150859375" y1="0.713740625" x2="8.2423" y2="0.71881875" layer="21"/>
<rectangle x1="0.480059375" y1="0.71881875" x2="0.74421875" y2="0.7239" layer="21"/>
<rectangle x1="0.886459375" y1="0.71881875" x2="1.115059375" y2="0.7239" layer="21"/>
<rectangle x1="1.26238125" y1="0.71881875" x2="1.49098125" y2="0.7239" layer="21"/>
<rectangle x1="1.7145" y1="0.71881875" x2="1.93801875" y2="0.7239" layer="21"/>
<rectangle x1="2.16661875" y1="0.71881875" x2="2.390140625" y2="0.7239" layer="21"/>
<rectangle x1="2.55778125" y1="0.71881875" x2="2.76098125" y2="0.7239" layer="21"/>
<rectangle x1="3.1115" y1="0.71881875" x2="3.33501875" y2="0.7239" layer="21"/>
<rectangle x1="3.4671" y1="0.71881875" x2="3.69061875" y2="0.7239" layer="21"/>
<rectangle x1="3.91921875" y1="0.71881875" x2="4.14781875" y2="0.7239" layer="21"/>
<rectangle x1="4.23418125" y1="0.71881875" x2="4.447540625" y2="0.7239" layer="21"/>
<rectangle x1="4.65581875" y1="0.71881875" x2="4.747259375" y2="0.7239" layer="21"/>
<rectangle x1="5.2959" y1="0.71881875" x2="5.37718125" y2="0.7239" layer="21"/>
<rectangle x1="5.58038125" y1="0.71881875" x2="5.6769" y2="0.7239" layer="21"/>
<rectangle x1="6.073140625" y1="0.71881875" x2="6.16458125" y2="0.7239" layer="21"/>
<rectangle x1="6.4135" y1="0.71881875" x2="6.504940625" y2="0.7239" layer="21"/>
<rectangle x1="6.753859375" y1="0.71881875" x2="6.8453" y2="0.7239" layer="21"/>
<rectangle x1="7.241540625" y1="0.71881875" x2="7.338059375" y2="0.7239" layer="21"/>
<rectangle x1="7.5311" y1="0.71881875" x2="7.617459375" y2="0.7239" layer="21"/>
<rectangle x1="8.14578125" y1="0.71881875" x2="8.2423" y2="0.7239" layer="21"/>
<rectangle x1="0.4699" y1="0.7239" x2="0.7493" y2="0.72898125" layer="21"/>
<rectangle x1="0.886459375" y1="0.7239" x2="1.10998125" y2="0.72898125" layer="21"/>
<rectangle x1="1.267459375" y1="0.7239" x2="1.49098125" y2="0.72898125" layer="21"/>
<rectangle x1="1.7145" y1="0.7239" x2="1.9431" y2="0.72898125" layer="21"/>
<rectangle x1="2.16661875" y1="0.7239" x2="2.390140625" y2="0.72898125" layer="21"/>
<rectangle x1="2.55778125" y1="0.7239" x2="2.766059375" y2="0.72898125" layer="21"/>
<rectangle x1="3.1115" y1="0.7239" x2="3.33501875" y2="0.72898125" layer="21"/>
<rectangle x1="3.4671" y1="0.7239" x2="3.685540625" y2="0.72898125" layer="21"/>
<rectangle x1="3.914140625" y1="0.7239" x2="4.142740625" y2="0.72898125" layer="21"/>
<rectangle x1="4.23418125" y1="0.7239" x2="4.447540625" y2="0.72898125" layer="21"/>
<rectangle x1="4.65581875" y1="0.7239" x2="4.752340625" y2="0.72898125" layer="21"/>
<rectangle x1="5.29081875" y1="0.7239" x2="5.37718125" y2="0.72898125" layer="21"/>
<rectangle x1="5.58038125" y1="0.7239" x2="5.67181875" y2="0.72898125" layer="21"/>
<rectangle x1="6.073140625" y1="0.7239" x2="6.16458125" y2="0.72898125" layer="21"/>
<rectangle x1="6.41858125" y1="0.7239" x2="6.51001875" y2="0.72898125" layer="21"/>
<rectangle x1="6.74878125" y1="0.7239" x2="6.8453" y2="0.72898125" layer="21"/>
<rectangle x1="7.241540625" y1="0.7239" x2="7.33298125" y2="0.72898125" layer="21"/>
<rectangle x1="7.52601875" y1="0.7239" x2="7.622540625" y2="0.72898125" layer="21"/>
<rectangle x1="8.150859375" y1="0.7239" x2="8.2423" y2="0.72898125" layer="21"/>
<rectangle x1="0.459740625" y1="0.72898125" x2="0.74421875" y2="0.734059375" layer="21"/>
<rectangle x1="0.891540625" y1="0.72898125" x2="1.115059375" y2="0.734059375" layer="21"/>
<rectangle x1="1.267459375" y1="0.72898125" x2="1.49098125" y2="0.734059375" layer="21"/>
<rectangle x1="1.7145" y1="0.72898125" x2="1.93801875" y2="0.734059375" layer="21"/>
<rectangle x1="2.16661875" y1="0.72898125" x2="2.390140625" y2="0.734059375" layer="21"/>
<rectangle x1="2.5527" y1="0.72898125" x2="2.766059375" y2="0.734059375" layer="21"/>
<rectangle x1="3.10641875" y1="0.72898125" x2="3.33501875" y2="0.734059375" layer="21"/>
<rectangle x1="3.46201875" y1="0.72898125" x2="3.69061875" y2="0.734059375" layer="21"/>
<rectangle x1="3.914140625" y1="0.72898125" x2="4.142740625" y2="0.734059375" layer="21"/>
<rectangle x1="4.23418125" y1="0.72898125" x2="4.447540625" y2="0.734059375" layer="21"/>
<rectangle x1="4.65581875" y1="0.72898125" x2="4.752340625" y2="0.734059375" layer="21"/>
<rectangle x1="5.29081875" y1="0.72898125" x2="5.37718125" y2="0.734059375" layer="21"/>
<rectangle x1="5.58038125" y1="0.72898125" x2="5.6769" y2="0.734059375" layer="21"/>
<rectangle x1="6.073140625" y1="0.72898125" x2="6.16458125" y2="0.734059375" layer="21"/>
<rectangle x1="6.4135" y1="0.72898125" x2="6.504940625" y2="0.734059375" layer="21"/>
<rectangle x1="6.753859375" y1="0.72898125" x2="6.8453" y2="0.734059375" layer="21"/>
<rectangle x1="7.24661875" y1="0.72898125" x2="7.338059375" y2="0.734059375" layer="21"/>
<rectangle x1="7.52601875" y1="0.72898125" x2="7.622540625" y2="0.734059375" layer="21"/>
<rectangle x1="8.14578125" y1="0.72898125" x2="8.23721875" y2="0.734059375" layer="21"/>
<rectangle x1="0.454659375" y1="0.734059375" x2="0.74421875" y2="0.739140625" layer="21"/>
<rectangle x1="0.886459375" y1="0.734059375" x2="1.10998125" y2="0.739140625" layer="21"/>
<rectangle x1="1.26238125" y1="0.734059375" x2="1.49098125" y2="0.739140625" layer="21"/>
<rectangle x1="1.71958125" y1="0.734059375" x2="1.93801875" y2="0.739140625" layer="21"/>
<rectangle x1="2.16661875" y1="0.734059375" x2="2.390140625" y2="0.739140625" layer="21"/>
<rectangle x1="2.5527" y1="0.734059375" x2="2.766059375" y2="0.739140625" layer="21"/>
<rectangle x1="3.10641875" y1="0.734059375" x2="3.33501875" y2="0.739140625" layer="21"/>
<rectangle x1="3.4671" y1="0.734059375" x2="3.685540625" y2="0.739140625" layer="21"/>
<rectangle x1="3.909059375" y1="0.734059375" x2="4.137659375" y2="0.739140625" layer="21"/>
<rectangle x1="4.239259375" y1="0.734059375" x2="4.45261875" y2="0.739140625" layer="21"/>
<rectangle x1="4.65581875" y1="0.734059375" x2="4.752340625" y2="0.739140625" layer="21"/>
<rectangle x1="5.29081875" y1="0.734059375" x2="5.37718125" y2="0.739140625" layer="21"/>
<rectangle x1="5.58038125" y1="0.734059375" x2="5.6769" y2="0.739140625" layer="21"/>
<rectangle x1="6.073140625" y1="0.734059375" x2="6.16458125" y2="0.739140625" layer="21"/>
<rectangle x1="6.4135" y1="0.734059375" x2="6.51001875" y2="0.739140625" layer="21"/>
<rectangle x1="6.74878125" y1="0.734059375" x2="6.85038125" y2="0.739140625" layer="21"/>
<rectangle x1="7.24661875" y1="0.734059375" x2="7.338059375" y2="0.739140625" layer="21"/>
<rectangle x1="7.52601875" y1="0.734059375" x2="7.622540625" y2="0.739140625" layer="21"/>
<rectangle x1="8.14578125" y1="0.734059375" x2="8.2423" y2="0.739140625" layer="21"/>
<rectangle x1="0.4445" y1="0.739140625" x2="0.74421875" y2="0.74421875" layer="21"/>
<rectangle x1="0.891540625" y1="0.739140625" x2="1.115059375" y2="0.74421875" layer="21"/>
<rectangle x1="1.267459375" y1="0.739140625" x2="1.49098125" y2="0.74421875" layer="21"/>
<rectangle x1="1.7145" y1="0.739140625" x2="1.9431" y2="0.74421875" layer="21"/>
<rectangle x1="2.16661875" y1="0.739140625" x2="2.390140625" y2="0.74421875" layer="21"/>
<rectangle x1="2.55778125" y1="0.739140625" x2="2.766059375" y2="0.74421875" layer="21"/>
<rectangle x1="3.10641875" y1="0.739140625" x2="3.33501875" y2="0.74421875" layer="21"/>
<rectangle x1="3.46201875" y1="0.739140625" x2="3.69061875" y2="0.74421875" layer="21"/>
<rectangle x1="3.909059375" y1="0.739140625" x2="4.137659375" y2="0.74421875" layer="21"/>
<rectangle x1="4.239259375" y1="0.739140625" x2="4.45261875" y2="0.74421875" layer="21"/>
<rectangle x1="4.6609" y1="0.739140625" x2="4.752340625" y2="0.74421875" layer="21"/>
<rectangle x1="5.29081875" y1="0.739140625" x2="5.37718125" y2="0.74421875" layer="21"/>
<rectangle x1="5.58038125" y1="0.739140625" x2="5.67181875" y2="0.74421875" layer="21"/>
<rectangle x1="6.073140625" y1="0.739140625" x2="6.16458125" y2="0.74421875" layer="21"/>
<rectangle x1="6.41858125" y1="0.739140625" x2="6.51001875" y2="0.74421875" layer="21"/>
<rectangle x1="6.753859375" y1="0.739140625" x2="6.8453" y2="0.74421875" layer="21"/>
<rectangle x1="7.241540625" y1="0.739140625" x2="7.33298125" y2="0.74421875" layer="21"/>
<rectangle x1="7.5311" y1="0.739140625" x2="7.622540625" y2="0.74421875" layer="21"/>
<rectangle x1="8.14578125" y1="0.739140625" x2="8.23721875" y2="0.74421875" layer="21"/>
<rectangle x1="0.434340625" y1="0.74421875" x2="0.74421875" y2="0.7493" layer="21"/>
<rectangle x1="0.886459375" y1="0.74421875" x2="1.115059375" y2="0.7493" layer="21"/>
<rectangle x1="1.267459375" y1="0.74421875" x2="1.496059375" y2="0.7493" layer="21"/>
<rectangle x1="1.7145" y1="0.74421875" x2="1.9431" y2="0.7493" layer="21"/>
<rectangle x1="2.161540625" y1="0.74421875" x2="2.390140625" y2="0.7493" layer="21"/>
<rectangle x1="2.55778125" y1="0.74421875" x2="2.766059375" y2="0.7493" layer="21"/>
<rectangle x1="3.1115" y1="0.74421875" x2="3.33501875" y2="0.7493" layer="21"/>
<rectangle x1="3.46201875" y1="0.74421875" x2="3.69061875" y2="0.7493" layer="21"/>
<rectangle x1="3.909059375" y1="0.74421875" x2="4.137659375" y2="0.7493" layer="21"/>
<rectangle x1="4.244340625" y1="0.74421875" x2="4.45261875" y2="0.7493" layer="21"/>
<rectangle x1="4.6609" y1="0.74421875" x2="4.752340625" y2="0.7493" layer="21"/>
<rectangle x1="5.29081875" y1="0.74421875" x2="5.37718125" y2="0.7493" layer="21"/>
<rectangle x1="5.58038125" y1="0.74421875" x2="5.67181875" y2="0.7493" layer="21"/>
<rectangle x1="6.073140625" y1="0.74421875" x2="6.16458125" y2="0.7493" layer="21"/>
<rectangle x1="6.4135" y1="0.74421875" x2="6.51001875" y2="0.7493" layer="21"/>
<rectangle x1="6.74878125" y1="0.74421875" x2="6.8453" y2="0.7493" layer="21"/>
<rectangle x1="7.24661875" y1="0.74421875" x2="7.338059375" y2="0.7493" layer="21"/>
<rectangle x1="7.5311" y1="0.74421875" x2="7.62761875" y2="0.7493" layer="21"/>
<rectangle x1="8.14578125" y1="0.74421875" x2="8.2423" y2="0.7493" layer="21"/>
<rectangle x1="0.42418125" y1="0.7493" x2="0.739140625" y2="0.75438125" layer="21"/>
<rectangle x1="0.891540625" y1="0.7493" x2="1.10998125" y2="0.75438125" layer="21"/>
<rectangle x1="1.267459375" y1="0.7493" x2="1.49098125" y2="0.75438125" layer="21"/>
<rectangle x1="1.7145" y1="0.7493" x2="1.93801875" y2="0.75438125" layer="21"/>
<rectangle x1="2.16661875" y1="0.7493" x2="2.390140625" y2="0.75438125" layer="21"/>
<rectangle x1="2.55778125" y1="0.7493" x2="2.766059375" y2="0.75438125" layer="21"/>
<rectangle x1="3.10641875" y1="0.7493" x2="3.329940625" y2="0.75438125" layer="21"/>
<rectangle x1="3.4671" y1="0.7493" x2="3.69061875" y2="0.75438125" layer="21"/>
<rectangle x1="3.90398125" y1="0.7493" x2="4.13258125" y2="0.75438125" layer="21"/>
<rectangle x1="4.244340625" y1="0.7493" x2="4.4577" y2="0.75438125" layer="21"/>
<rectangle x1="4.6609" y1="0.7493" x2="4.752340625" y2="0.75438125" layer="21"/>
<rectangle x1="5.285740625" y1="0.7493" x2="5.37718125" y2="0.75438125" layer="21"/>
<rectangle x1="5.58038125" y1="0.7493" x2="5.67181875" y2="0.75438125" layer="21"/>
<rectangle x1="6.073140625" y1="0.7493" x2="6.16458125" y2="0.75438125" layer="21"/>
<rectangle x1="6.4135" y1="0.7493" x2="6.504940625" y2="0.75438125" layer="21"/>
<rectangle x1="6.753859375" y1="0.7493" x2="6.85038125" y2="0.75438125" layer="21"/>
<rectangle x1="7.241540625" y1="0.7493" x2="7.338059375" y2="0.75438125" layer="21"/>
<rectangle x1="7.5311" y1="0.7493" x2="7.622540625" y2="0.75438125" layer="21"/>
<rectangle x1="8.14578125" y1="0.7493" x2="8.23721875" y2="0.75438125" layer="21"/>
<rectangle x1="0.41401875" y1="0.75438125" x2="0.739140625" y2="0.759459375" layer="21"/>
<rectangle x1="0.886459375" y1="0.75438125" x2="1.115059375" y2="0.759459375" layer="21"/>
<rectangle x1="1.267459375" y1="0.75438125" x2="1.49098125" y2="0.759459375" layer="21"/>
<rectangle x1="1.7145" y1="0.75438125" x2="1.9431" y2="0.759459375" layer="21"/>
<rectangle x1="2.16661875" y1="0.75438125" x2="2.390140625" y2="0.759459375" layer="21"/>
<rectangle x1="2.5527" y1="0.75438125" x2="2.771140625" y2="0.759459375" layer="21"/>
<rectangle x1="3.10641875" y1="0.75438125" x2="3.33501875" y2="0.759459375" layer="21"/>
<rectangle x1="3.4671" y1="0.75438125" x2="3.685540625" y2="0.759459375" layer="21"/>
<rectangle x1="3.90398125" y1="0.75438125" x2="4.13258125" y2="0.759459375" layer="21"/>
<rectangle x1="4.244340625" y1="0.75438125" x2="4.4577" y2="0.759459375" layer="21"/>
<rectangle x1="4.6609" y1="0.75438125" x2="4.75741875" y2="0.759459375" layer="21"/>
<rectangle x1="5.285740625" y1="0.75438125" x2="5.37718125" y2="0.759459375" layer="21"/>
<rectangle x1="5.58038125" y1="0.75438125" x2="5.67181875" y2="0.759459375" layer="21"/>
<rectangle x1="6.073140625" y1="0.75438125" x2="6.16458125" y2="0.759459375" layer="21"/>
<rectangle x1="6.4135" y1="0.75438125" x2="6.504940625" y2="0.759459375" layer="21"/>
<rectangle x1="6.753859375" y1="0.75438125" x2="6.85038125" y2="0.759459375" layer="21"/>
<rectangle x1="7.241540625" y1="0.75438125" x2="7.338059375" y2="0.759459375" layer="21"/>
<rectangle x1="7.5311" y1="0.75438125" x2="7.62761875" y2="0.759459375" layer="21"/>
<rectangle x1="8.1407" y1="0.75438125" x2="8.23721875" y2="0.759459375" layer="21"/>
<rectangle x1="0.39878125" y1="0.759459375" x2="0.739140625" y2="0.764540625" layer="21"/>
<rectangle x1="0.891540625" y1="0.759459375" x2="1.10998125" y2="0.764540625" layer="21"/>
<rectangle x1="1.26238125" y1="0.759459375" x2="1.496059375" y2="0.764540625" layer="21"/>
<rectangle x1="1.7145" y1="0.759459375" x2="1.9431" y2="0.764540625" layer="21"/>
<rectangle x1="2.16661875" y1="0.759459375" x2="2.390140625" y2="0.764540625" layer="21"/>
<rectangle x1="2.55778125" y1="0.759459375" x2="2.766059375" y2="0.764540625" layer="21"/>
<rectangle x1="3.101340625" y1="0.759459375" x2="3.33501875" y2="0.764540625" layer="21"/>
<rectangle x1="3.46201875" y1="0.759459375" x2="3.69061875" y2="0.764540625" layer="21"/>
<rectangle x1="3.8989" y1="0.759459375" x2="4.1275" y2="0.764540625" layer="21"/>
<rectangle x1="4.244340625" y1="0.759459375" x2="4.46278125" y2="0.764540625" layer="21"/>
<rectangle x1="4.6609" y1="0.759459375" x2="4.75741875" y2="0.764540625" layer="21"/>
<rectangle x1="5.285740625" y1="0.759459375" x2="5.37718125" y2="0.764540625" layer="21"/>
<rectangle x1="5.58038125" y1="0.759459375" x2="5.6769" y2="0.764540625" layer="21"/>
<rectangle x1="6.073140625" y1="0.759459375" x2="6.16458125" y2="0.764540625" layer="21"/>
<rectangle x1="6.41858125" y1="0.759459375" x2="6.51001875" y2="0.764540625" layer="21"/>
<rectangle x1="6.74878125" y1="0.759459375" x2="6.85038125" y2="0.764540625" layer="21"/>
<rectangle x1="7.241540625" y1="0.759459375" x2="7.33298125" y2="0.764540625" layer="21"/>
<rectangle x1="7.5311" y1="0.759459375" x2="7.62761875" y2="0.764540625" layer="21"/>
<rectangle x1="8.1407" y1="0.759459375" x2="8.23721875" y2="0.764540625" layer="21"/>
<rectangle x1="0.383540625" y1="0.764540625" x2="0.739140625" y2="0.76961875" layer="21"/>
<rectangle x1="0.886459375" y1="0.764540625" x2="1.115059375" y2="0.76961875" layer="21"/>
<rectangle x1="1.267459375" y1="0.764540625" x2="1.496059375" y2="0.76961875" layer="21"/>
<rectangle x1="1.7145" y1="0.764540625" x2="1.9431" y2="0.76961875" layer="21"/>
<rectangle x1="2.161540625" y1="0.764540625" x2="2.390140625" y2="0.76961875" layer="21"/>
<rectangle x1="2.55778125" y1="0.764540625" x2="2.771140625" y2="0.76961875" layer="21"/>
<rectangle x1="3.101340625" y1="0.764540625" x2="3.329940625" y2="0.76961875" layer="21"/>
<rectangle x1="3.4671" y1="0.764540625" x2="3.685540625" y2="0.76961875" layer="21"/>
<rectangle x1="3.8989" y1="0.764540625" x2="4.1275" y2="0.76961875" layer="21"/>
<rectangle x1="4.24941875" y1="0.764540625" x2="4.46278125" y2="0.76961875" layer="21"/>
<rectangle x1="4.6609" y1="0.764540625" x2="4.75741875" y2="0.76961875" layer="21"/>
<rectangle x1="5.285740625" y1="0.764540625" x2="5.37718125" y2="0.76961875" layer="21"/>
<rectangle x1="5.58038125" y1="0.764540625" x2="5.67181875" y2="0.76961875" layer="21"/>
<rectangle x1="6.073140625" y1="0.764540625" x2="6.16458125" y2="0.76961875" layer="21"/>
<rectangle x1="6.4135" y1="0.764540625" x2="6.51001875" y2="0.76961875" layer="21"/>
<rectangle x1="6.74878125" y1="0.764540625" x2="6.85038125" y2="0.76961875" layer="21"/>
<rectangle x1="7.241540625" y1="0.764540625" x2="7.338059375" y2="0.76961875" layer="21"/>
<rectangle x1="7.5311" y1="0.764540625" x2="7.62761875" y2="0.76961875" layer="21"/>
<rectangle x1="8.1407" y1="0.764540625" x2="8.23721875" y2="0.76961875" layer="21"/>
<rectangle x1="0.3683" y1="0.76961875" x2="0.734059375" y2="0.7747" layer="21"/>
<rectangle x1="0.886459375" y1="0.76961875" x2="1.115059375" y2="0.7747" layer="21"/>
<rectangle x1="1.267459375" y1="0.76961875" x2="1.496059375" y2="0.7747" layer="21"/>
<rectangle x1="1.7145" y1="0.76961875" x2="1.9431" y2="0.7747" layer="21"/>
<rectangle x1="2.16661875" y1="0.76961875" x2="2.390140625" y2="0.7747" layer="21"/>
<rectangle x1="2.55778125" y1="0.76961875" x2="2.771140625" y2="0.7747" layer="21"/>
<rectangle x1="3.101340625" y1="0.76961875" x2="3.329940625" y2="0.7747" layer="21"/>
<rectangle x1="3.46201875" y1="0.76961875" x2="3.69061875" y2="0.7747" layer="21"/>
<rectangle x1="3.89381875" y1="0.76961875" x2="4.1275" y2="0.7747" layer="21"/>
<rectangle x1="4.24941875" y1="0.76961875" x2="4.46278125" y2="0.7747" layer="21"/>
<rectangle x1="4.66598125" y1="0.76961875" x2="4.75741875" y2="0.7747" layer="21"/>
<rectangle x1="5.280659375" y1="0.76961875" x2="5.37718125" y2="0.7747" layer="21"/>
<rectangle x1="5.58038125" y1="0.76961875" x2="5.6769" y2="0.7747" layer="21"/>
<rectangle x1="6.073140625" y1="0.76961875" x2="6.16458125" y2="0.7747" layer="21"/>
<rectangle x1="6.41858125" y1="0.76961875" x2="6.504940625" y2="0.7747" layer="21"/>
<rectangle x1="6.753859375" y1="0.76961875" x2="6.85038125" y2="0.7747" layer="21"/>
<rectangle x1="7.241540625" y1="0.76961875" x2="7.338059375" y2="0.7747" layer="21"/>
<rectangle x1="7.53618125" y1="0.76961875" x2="7.62761875" y2="0.7747" layer="21"/>
<rectangle x1="8.1407" y1="0.76961875" x2="8.23721875" y2="0.7747" layer="21"/>
<rectangle x1="0.353059375" y1="0.7747" x2="0.734059375" y2="0.77978125" layer="21"/>
<rectangle x1="0.886459375" y1="0.7747" x2="1.115059375" y2="0.77978125" layer="21"/>
<rectangle x1="1.26238125" y1="0.7747" x2="1.496059375" y2="0.77978125" layer="21"/>
<rectangle x1="1.7145" y1="0.7747" x2="1.9431" y2="0.77978125" layer="21"/>
<rectangle x1="2.161540625" y1="0.7747" x2="2.390140625" y2="0.77978125" layer="21"/>
<rectangle x1="2.55778125" y1="0.7747" x2="2.77621875" y2="0.77978125" layer="21"/>
<rectangle x1="3.101340625" y1="0.7747" x2="3.329940625" y2="0.77978125" layer="21"/>
<rectangle x1="3.4671" y1="0.7747" x2="3.69061875" y2="0.77978125" layer="21"/>
<rectangle x1="3.89381875" y1="0.7747" x2="4.12241875" y2="0.77978125" layer="21"/>
<rectangle x1="4.24941875" y1="0.7747" x2="4.467859375" y2="0.77978125" layer="21"/>
<rectangle x1="4.66598125" y1="0.7747" x2="4.7625" y2="0.77978125" layer="21"/>
<rectangle x1="5.280659375" y1="0.7747" x2="5.37718125" y2="0.77978125" layer="21"/>
<rectangle x1="5.58038125" y1="0.7747" x2="5.6769" y2="0.77978125" layer="21"/>
<rectangle x1="6.073140625" y1="0.7747" x2="6.16458125" y2="0.77978125" layer="21"/>
<rectangle x1="6.4135" y1="0.7747" x2="6.51001875" y2="0.77978125" layer="21"/>
<rectangle x1="6.74878125" y1="0.7747" x2="6.85038125" y2="0.77978125" layer="21"/>
<rectangle x1="7.241540625" y1="0.7747" x2="7.33298125" y2="0.77978125" layer="21"/>
<rectangle x1="7.53618125" y1="0.7747" x2="7.6327" y2="0.77978125" layer="21"/>
<rectangle x1="8.1407" y1="0.7747" x2="8.232140625" y2="0.77978125" layer="21"/>
<rectangle x1="0.3429" y1="0.77978125" x2="0.734059375" y2="0.784859375" layer="21"/>
<rectangle x1="0.891540625" y1="0.77978125" x2="1.10998125" y2="0.784859375" layer="21"/>
<rectangle x1="1.267459375" y1="0.77978125" x2="1.496059375" y2="0.784859375" layer="21"/>
<rectangle x1="1.7145" y1="0.77978125" x2="1.9431" y2="0.784859375" layer="21"/>
<rectangle x1="2.161540625" y1="0.77978125" x2="2.390140625" y2="0.784859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.77978125" x2="2.77621875" y2="0.784859375" layer="21"/>
<rectangle x1="3.096259375" y1="0.77978125" x2="3.329940625" y2="0.784859375" layer="21"/>
<rectangle x1="3.46201875" y1="0.77978125" x2="3.685540625" y2="0.784859375" layer="21"/>
<rectangle x1="3.888740625" y1="0.77978125" x2="4.12241875" y2="0.784859375" layer="21"/>
<rectangle x1="4.2545" y1="0.77978125" x2="4.467859375" y2="0.784859375" layer="21"/>
<rectangle x1="4.66598125" y1="0.77978125" x2="4.7625" y2="0.784859375" layer="21"/>
<rectangle x1="5.280659375" y1="0.77978125" x2="5.37718125" y2="0.784859375" layer="21"/>
<rectangle x1="5.58038125" y1="0.77978125" x2="5.67181875" y2="0.784859375" layer="21"/>
<rectangle x1="6.073140625" y1="0.77978125" x2="6.16458125" y2="0.784859375" layer="21"/>
<rectangle x1="6.4135" y1="0.77978125" x2="6.504940625" y2="0.784859375" layer="21"/>
<rectangle x1="6.753859375" y1="0.77978125" x2="6.855459375" y2="0.784859375" layer="21"/>
<rectangle x1="7.241540625" y1="0.77978125" x2="7.338059375" y2="0.784859375" layer="21"/>
<rectangle x1="7.53618125" y1="0.77978125" x2="7.6327" y2="0.784859375" layer="21"/>
<rectangle x1="8.13561875" y1="0.77978125" x2="8.232140625" y2="0.784859375" layer="21"/>
<rectangle x1="0.32258125" y1="0.784859375" x2="0.72898125" y2="0.789940625" layer="21"/>
<rectangle x1="0.886459375" y1="0.784859375" x2="1.115059375" y2="0.789940625" layer="21"/>
<rectangle x1="1.267459375" y1="0.784859375" x2="1.496059375" y2="0.789940625" layer="21"/>
<rectangle x1="1.70941875" y1="0.784859375" x2="1.94818125" y2="0.789940625" layer="21"/>
<rectangle x1="2.16661875" y1="0.784859375" x2="2.390140625" y2="0.789940625" layer="21"/>
<rectangle x1="2.55778125" y1="0.784859375" x2="2.77621875" y2="0.789940625" layer="21"/>
<rectangle x1="3.096259375" y1="0.784859375" x2="3.329940625" y2="0.789940625" layer="21"/>
<rectangle x1="3.4671" y1="0.784859375" x2="3.69061875" y2="0.789940625" layer="21"/>
<rectangle x1="3.888740625" y1="0.784859375" x2="4.117340625" y2="0.789940625" layer="21"/>
<rectangle x1="4.2545" y1="0.784859375" x2="4.472940625" y2="0.789940625" layer="21"/>
<rectangle x1="4.66598125" y1="0.784859375" x2="4.7625" y2="0.789940625" layer="21"/>
<rectangle x1="5.27558125" y1="0.784859375" x2="5.37718125" y2="0.789940625" layer="21"/>
<rectangle x1="5.58038125" y1="0.784859375" x2="5.67181875" y2="0.789940625" layer="21"/>
<rectangle x1="6.073140625" y1="0.784859375" x2="6.16458125" y2="0.789940625" layer="21"/>
<rectangle x1="6.4135" y1="0.784859375" x2="6.51001875" y2="0.789940625" layer="21"/>
<rectangle x1="6.753859375" y1="0.784859375" x2="6.855459375" y2="0.789940625" layer="21"/>
<rectangle x1="7.241540625" y1="0.784859375" x2="7.338059375" y2="0.789940625" layer="21"/>
<rectangle x1="7.53618125" y1="0.784859375" x2="7.6327" y2="0.789940625" layer="21"/>
<rectangle x1="8.13561875" y1="0.784859375" x2="8.232140625" y2="0.789940625" layer="21"/>
<rectangle x1="0.307340625" y1="0.789940625" x2="0.72898125" y2="0.79501875" layer="21"/>
<rectangle x1="0.891540625" y1="0.789940625" x2="1.115059375" y2="0.79501875" layer="21"/>
<rectangle x1="1.267459375" y1="0.789940625" x2="1.501140625" y2="0.79501875" layer="21"/>
<rectangle x1="1.7145" y1="0.789940625" x2="1.94818125" y2="0.79501875" layer="21"/>
<rectangle x1="2.161540625" y1="0.789940625" x2="2.390140625" y2="0.79501875" layer="21"/>
<rectangle x1="2.5527" y1="0.789940625" x2="2.7813" y2="0.79501875" layer="21"/>
<rectangle x1="3.096259375" y1="0.789940625" x2="3.329940625" y2="0.79501875" layer="21"/>
<rectangle x1="3.46201875" y1="0.789940625" x2="3.69061875" y2="0.79501875" layer="21"/>
<rectangle x1="3.888740625" y1="0.789940625" x2="4.117340625" y2="0.79501875" layer="21"/>
<rectangle x1="4.2545" y1="0.789940625" x2="4.472940625" y2="0.79501875" layer="21"/>
<rectangle x1="4.66598125" y1="0.789940625" x2="4.76758125" y2="0.79501875" layer="21"/>
<rectangle x1="5.27558125" y1="0.789940625" x2="5.37718125" y2="0.79501875" layer="21"/>
<rectangle x1="5.58038125" y1="0.789940625" x2="5.67181875" y2="0.79501875" layer="21"/>
<rectangle x1="6.073140625" y1="0.789940625" x2="6.16458125" y2="0.79501875" layer="21"/>
<rectangle x1="6.41858125" y1="0.789940625" x2="6.504940625" y2="0.79501875" layer="21"/>
<rectangle x1="6.74878125" y1="0.789940625" x2="6.855459375" y2="0.79501875" layer="21"/>
<rectangle x1="7.241540625" y1="0.789940625" x2="7.33298125" y2="0.79501875" layer="21"/>
<rectangle x1="7.53618125" y1="0.789940625" x2="7.6327" y2="0.79501875" layer="21"/>
<rectangle x1="8.13561875" y1="0.789940625" x2="8.232140625" y2="0.79501875" layer="21"/>
<rectangle x1="0.2921" y1="0.79501875" x2="0.7239" y2="0.8001" layer="21"/>
<rectangle x1="0.886459375" y1="0.79501875" x2="1.10998125" y2="0.8001" layer="21"/>
<rectangle x1="1.267459375" y1="0.79501875" x2="1.501140625" y2="0.8001" layer="21"/>
<rectangle x1="1.70941875" y1="0.79501875" x2="1.94818125" y2="0.8001" layer="21"/>
<rectangle x1="2.161540625" y1="0.79501875" x2="2.390140625" y2="0.8001" layer="21"/>
<rectangle x1="2.55778125" y1="0.79501875" x2="2.7813" y2="0.8001" layer="21"/>
<rectangle x1="3.09118125" y1="0.79501875" x2="3.329940625" y2="0.8001" layer="21"/>
<rectangle x1="3.4671" y1="0.79501875" x2="3.69061875" y2="0.8001" layer="21"/>
<rectangle x1="3.883659375" y1="0.79501875" x2="4.117340625" y2="0.8001" layer="21"/>
<rectangle x1="4.25958125" y1="0.79501875" x2="4.472940625" y2="0.8001" layer="21"/>
<rectangle x1="4.671059375" y1="0.79501875" x2="4.76758125" y2="0.8001" layer="21"/>
<rectangle x1="5.2705" y1="0.79501875" x2="5.37718125" y2="0.8001" layer="21"/>
<rectangle x1="5.58038125" y1="0.79501875" x2="5.67181875" y2="0.8001" layer="21"/>
<rectangle x1="6.073140625" y1="0.79501875" x2="6.16458125" y2="0.8001" layer="21"/>
<rectangle x1="6.4135" y1="0.79501875" x2="6.51001875" y2="0.8001" layer="21"/>
<rectangle x1="6.74878125" y1="0.79501875" x2="6.855459375" y2="0.8001" layer="21"/>
<rectangle x1="7.241540625" y1="0.79501875" x2="7.33298125" y2="0.8001" layer="21"/>
<rectangle x1="7.541259375" y1="0.79501875" x2="7.63778125" y2="0.8001" layer="21"/>
<rectangle x1="8.130540625" y1="0.79501875" x2="8.227059375" y2="0.8001" layer="21"/>
<rectangle x1="0.276859375" y1="0.8001" x2="0.7239" y2="0.80518125" layer="21"/>
<rectangle x1="0.886459375" y1="0.8001" x2="1.115059375" y2="0.80518125" layer="21"/>
<rectangle x1="1.26238125" y1="0.8001" x2="1.501140625" y2="0.80518125" layer="21"/>
<rectangle x1="1.7145" y1="0.8001" x2="1.94818125" y2="0.80518125" layer="21"/>
<rectangle x1="2.161540625" y1="0.8001" x2="2.385059375" y2="0.80518125" layer="21"/>
<rectangle x1="2.55778125" y1="0.8001" x2="2.78638125" y2="0.80518125" layer="21"/>
<rectangle x1="3.09118125" y1="0.8001" x2="3.324859375" y2="0.80518125" layer="21"/>
<rectangle x1="3.46201875" y1="0.8001" x2="3.685540625" y2="0.80518125" layer="21"/>
<rectangle x1="3.883659375" y1="0.8001" x2="4.112259375" y2="0.80518125" layer="21"/>
<rectangle x1="4.25958125" y1="0.8001" x2="4.47801875" y2="0.80518125" layer="21"/>
<rectangle x1="4.671059375" y1="0.8001" x2="4.76758125" y2="0.80518125" layer="21"/>
<rectangle x1="5.2705" y1="0.8001" x2="5.37718125" y2="0.80518125" layer="21"/>
<rectangle x1="5.585459375" y1="0.8001" x2="5.6769" y2="0.80518125" layer="21"/>
<rectangle x1="6.073140625" y1="0.8001" x2="6.16458125" y2="0.80518125" layer="21"/>
<rectangle x1="6.41858125" y1="0.8001" x2="6.51001875" y2="0.80518125" layer="21"/>
<rectangle x1="6.753859375" y1="0.8001" x2="6.860540625" y2="0.80518125" layer="21"/>
<rectangle x1="7.241540625" y1="0.8001" x2="7.338059375" y2="0.80518125" layer="21"/>
<rectangle x1="7.541259375" y1="0.8001" x2="7.63778125" y2="0.80518125" layer="21"/>
<rectangle x1="8.130540625" y1="0.8001" x2="8.227059375" y2="0.80518125" layer="21"/>
<rectangle x1="0.26161875" y1="0.80518125" x2="0.71881875" y2="0.810259375" layer="21"/>
<rectangle x1="0.886459375" y1="0.80518125" x2="1.10998125" y2="0.810259375" layer="21"/>
<rectangle x1="1.267459375" y1="0.80518125" x2="1.50621875" y2="0.810259375" layer="21"/>
<rectangle x1="1.70941875" y1="0.80518125" x2="1.953259375" y2="0.810259375" layer="21"/>
<rectangle x1="2.161540625" y1="0.80518125" x2="2.390140625" y2="0.810259375" layer="21"/>
<rectangle x1="2.55778125" y1="0.80518125" x2="2.78638125" y2="0.810259375" layer="21"/>
<rectangle x1="3.0861" y1="0.80518125" x2="3.324859375" y2="0.810259375" layer="21"/>
<rectangle x1="3.4671" y1="0.80518125" x2="3.69061875" y2="0.810259375" layer="21"/>
<rectangle x1="3.87858125" y1="0.80518125" x2="4.112259375" y2="0.810259375" layer="21"/>
<rectangle x1="4.264659375" y1="0.80518125" x2="4.47801875" y2="0.810259375" layer="21"/>
<rectangle x1="4.671059375" y1="0.80518125" x2="4.772659375" y2="0.810259375" layer="21"/>
<rectangle x1="5.2705" y1="0.80518125" x2="5.37718125" y2="0.810259375" layer="21"/>
<rectangle x1="5.58038125" y1="0.80518125" x2="5.67181875" y2="0.810259375" layer="21"/>
<rectangle x1="6.073140625" y1="0.80518125" x2="6.16458125" y2="0.810259375" layer="21"/>
<rectangle x1="6.4135" y1="0.80518125" x2="6.51001875" y2="0.810259375" layer="21"/>
<rectangle x1="6.74878125" y1="0.80518125" x2="6.860540625" y2="0.810259375" layer="21"/>
<rectangle x1="7.241540625" y1="0.80518125" x2="7.33298125" y2="0.810259375" layer="21"/>
<rectangle x1="7.541259375" y1="0.80518125" x2="7.642859375" y2="0.810259375" layer="21"/>
<rectangle x1="8.130540625" y1="0.80518125" x2="8.227059375" y2="0.810259375" layer="21"/>
<rectangle x1="0.24638125" y1="0.810259375" x2="0.71881875" y2="0.815340625" layer="21"/>
<rectangle x1="0.891540625" y1="0.810259375" x2="1.115059375" y2="0.815340625" layer="21"/>
<rectangle x1="1.267459375" y1="0.810259375" x2="1.501140625" y2="0.815340625" layer="21"/>
<rectangle x1="1.70941875" y1="0.810259375" x2="1.953259375" y2="0.815340625" layer="21"/>
<rectangle x1="2.161540625" y1="0.810259375" x2="2.390140625" y2="0.815340625" layer="21"/>
<rectangle x1="2.5527" y1="0.810259375" x2="2.791459375" y2="0.815340625" layer="21"/>
<rectangle x1="3.08101875" y1="0.810259375" x2="3.324859375" y2="0.815340625" layer="21"/>
<rectangle x1="3.46201875" y1="0.810259375" x2="3.69061875" y2="0.815340625" layer="21"/>
<rectangle x1="3.87858125" y1="0.810259375" x2="4.112259375" y2="0.815340625" layer="21"/>
<rectangle x1="4.264659375" y1="0.810259375" x2="4.4831" y2="0.815340625" layer="21"/>
<rectangle x1="4.676140625" y1="0.810259375" x2="4.772659375" y2="0.815340625" layer="21"/>
<rectangle x1="5.26541875" y1="0.810259375" x2="5.37718125" y2="0.815340625" layer="21"/>
<rectangle x1="5.58038125" y1="0.810259375" x2="5.6769" y2="0.815340625" layer="21"/>
<rectangle x1="6.073140625" y1="0.810259375" x2="6.16458125" y2="0.815340625" layer="21"/>
<rectangle x1="6.4135" y1="0.810259375" x2="6.504940625" y2="0.815340625" layer="21"/>
<rectangle x1="6.753859375" y1="0.810259375" x2="6.860540625" y2="0.815340625" layer="21"/>
<rectangle x1="7.236459375" y1="0.810259375" x2="7.33298125" y2="0.815340625" layer="21"/>
<rectangle x1="7.546340625" y1="0.810259375" x2="7.642859375" y2="0.815340625" layer="21"/>
<rectangle x1="8.125459375" y1="0.810259375" x2="8.227059375" y2="0.815340625" layer="21"/>
<rectangle x1="0.231140625" y1="0.815340625" x2="0.713740625" y2="0.82041875" layer="21"/>
<rectangle x1="0.886459375" y1="0.815340625" x2="1.10998125" y2="0.82041875" layer="21"/>
<rectangle x1="1.26238125" y1="0.815340625" x2="1.50621875" y2="0.82041875" layer="21"/>
<rectangle x1="1.70941875" y1="0.815340625" x2="1.953259375" y2="0.82041875" layer="21"/>
<rectangle x1="2.156459375" y1="0.815340625" x2="2.385059375" y2="0.82041875" layer="21"/>
<rectangle x1="2.55778125" y1="0.815340625" x2="2.791459375" y2="0.82041875" layer="21"/>
<rectangle x1="3.08101875" y1="0.815340625" x2="3.31978125" y2="0.82041875" layer="21"/>
<rectangle x1="3.46201875" y1="0.815340625" x2="3.685540625" y2="0.82041875" layer="21"/>
<rectangle x1="3.87858125" y1="0.815340625" x2="4.10718125" y2="0.82041875" layer="21"/>
<rectangle x1="4.264659375" y1="0.815340625" x2="4.4831" y2="0.82041875" layer="21"/>
<rectangle x1="4.676140625" y1="0.815340625" x2="4.777740625" y2="0.82041875" layer="21"/>
<rectangle x1="5.260340625" y1="0.815340625" x2="5.37718125" y2="0.82041875" layer="21"/>
<rectangle x1="5.58038125" y1="0.815340625" x2="5.6769" y2="0.82041875" layer="21"/>
<rectangle x1="6.073140625" y1="0.815340625" x2="6.16458125" y2="0.82041875" layer="21"/>
<rectangle x1="6.4135" y1="0.815340625" x2="6.504940625" y2="0.82041875" layer="21"/>
<rectangle x1="6.753859375" y1="0.815340625" x2="6.86561875" y2="0.82041875" layer="21"/>
<rectangle x1="7.236459375" y1="0.815340625" x2="7.3279" y2="0.82041875" layer="21"/>
<rectangle x1="7.546340625" y1="0.815340625" x2="7.647940625" y2="0.82041875" layer="21"/>
<rectangle x1="8.125459375" y1="0.815340625" x2="8.22198125" y2="0.82041875" layer="21"/>
<rectangle x1="0.22098125" y1="0.82041875" x2="0.708659375" y2="0.8255" layer="21"/>
<rectangle x1="0.891540625" y1="0.82041875" x2="1.115059375" y2="0.8255" layer="21"/>
<rectangle x1="1.267459375" y1="0.82041875" x2="1.5113" y2="0.8255" layer="21"/>
<rectangle x1="1.704340625" y1="0.82041875" x2="1.958340625" y2="0.8255" layer="21"/>
<rectangle x1="2.156459375" y1="0.82041875" x2="2.390140625" y2="0.8255" layer="21"/>
<rectangle x1="2.55778125" y1="0.82041875" x2="2.796540625" y2="0.8255" layer="21"/>
<rectangle x1="3.075940625" y1="0.82041875" x2="3.324859375" y2="0.8255" layer="21"/>
<rectangle x1="3.4671" y1="0.82041875" x2="3.69061875" y2="0.8255" layer="21"/>
<rectangle x1="3.8735" y1="0.82041875" x2="4.10718125" y2="0.8255" layer="21"/>
<rectangle x1="4.264659375" y1="0.82041875" x2="4.4831" y2="0.8255" layer="21"/>
<rectangle x1="4.676140625" y1="0.82041875" x2="4.777740625" y2="0.8255" layer="21"/>
<rectangle x1="5.260340625" y1="0.82041875" x2="5.37718125" y2="0.8255" layer="21"/>
<rectangle x1="5.58038125" y1="0.82041875" x2="5.67181875" y2="0.8255" layer="21"/>
<rectangle x1="6.073140625" y1="0.82041875" x2="6.16458125" y2="0.8255" layer="21"/>
<rectangle x1="6.41858125" y1="0.82041875" x2="6.51001875" y2="0.8255" layer="21"/>
<rectangle x1="6.74878125" y1="0.82041875" x2="6.86561875" y2="0.8255" layer="21"/>
<rectangle x1="7.236459375" y1="0.82041875" x2="7.33298125" y2="0.8255" layer="21"/>
<rectangle x1="7.546340625" y1="0.82041875" x2="7.647940625" y2="0.8255" layer="21"/>
<rectangle x1="8.12038125" y1="0.82041875" x2="8.22198125" y2="0.8255" layer="21"/>
<rectangle x1="0.205740625" y1="0.8255" x2="0.70358125" y2="0.83058125" layer="21"/>
<rectangle x1="0.886459375" y1="0.8255" x2="1.115059375" y2="0.83058125" layer="21"/>
<rectangle x1="1.267459375" y1="0.8255" x2="1.5113" y2="0.83058125" layer="21"/>
<rectangle x1="1.704340625" y1="0.8255" x2="1.958340625" y2="0.83058125" layer="21"/>
<rectangle x1="2.156459375" y1="0.8255" x2="2.390140625" y2="0.83058125" layer="21"/>
<rectangle x1="2.55778125" y1="0.8255" x2="2.80161875" y2="0.83058125" layer="21"/>
<rectangle x1="3.070859375" y1="0.8255" x2="3.31978125" y2="0.83058125" layer="21"/>
<rectangle x1="3.4671" y1="0.8255" x2="3.685540625" y2="0.83058125" layer="21"/>
<rectangle x1="3.8735" y1="0.8255" x2="4.1021" y2="0.83058125" layer="21"/>
<rectangle x1="4.269740625" y1="0.8255" x2="4.48818125" y2="0.83058125" layer="21"/>
<rectangle x1="4.68121875" y1="0.8255" x2="4.777740625" y2="0.83058125" layer="21"/>
<rectangle x1="5.255259375" y1="0.8255" x2="5.37718125" y2="0.83058125" layer="21"/>
<rectangle x1="5.58038125" y1="0.8255" x2="5.67181875" y2="0.83058125" layer="21"/>
<rectangle x1="6.073140625" y1="0.8255" x2="6.16458125" y2="0.83058125" layer="21"/>
<rectangle x1="6.4135" y1="0.8255" x2="6.51001875" y2="0.83058125" layer="21"/>
<rectangle x1="6.74878125" y1="0.8255" x2="6.8707" y2="0.83058125" layer="21"/>
<rectangle x1="7.236459375" y1="0.8255" x2="7.33298125" y2="0.83058125" layer="21"/>
<rectangle x1="7.55141875" y1="0.8255" x2="7.647940625" y2="0.83058125" layer="21"/>
<rectangle x1="8.12038125" y1="0.8255" x2="8.22198125" y2="0.83058125" layer="21"/>
<rectangle x1="0.19558125" y1="0.83058125" x2="0.70358125" y2="0.835659375" layer="21"/>
<rectangle x1="0.891540625" y1="0.83058125" x2="1.10998125" y2="0.835659375" layer="21"/>
<rectangle x1="1.267459375" y1="0.83058125" x2="1.51638125" y2="0.835659375" layer="21"/>
<rectangle x1="1.699259375" y1="0.83058125" x2="1.96341875" y2="0.835659375" layer="21"/>
<rectangle x1="2.15138125" y1="0.83058125" x2="2.385059375" y2="0.835659375" layer="21"/>
<rectangle x1="2.5527" y1="0.83058125" x2="2.8067" y2="0.835659375" layer="21"/>
<rectangle x1="3.070859375" y1="0.83058125" x2="3.31978125" y2="0.835659375" layer="21"/>
<rectangle x1="3.46201875" y1="0.83058125" x2="3.69061875" y2="0.835659375" layer="21"/>
<rectangle x1="3.86841875" y1="0.83058125" x2="4.1021" y2="0.835659375" layer="21"/>
<rectangle x1="4.269740625" y1="0.83058125" x2="4.48818125" y2="0.835659375" layer="21"/>
<rectangle x1="4.68121875" y1="0.83058125" x2="4.78281875" y2="0.835659375" layer="21"/>
<rectangle x1="5.255259375" y1="0.83058125" x2="5.37718125" y2="0.835659375" layer="21"/>
<rectangle x1="5.58038125" y1="0.83058125" x2="5.67181875" y2="0.835659375" layer="21"/>
<rectangle x1="6.073140625" y1="0.83058125" x2="6.16458125" y2="0.835659375" layer="21"/>
<rectangle x1="6.41858125" y1="0.83058125" x2="6.504940625" y2="0.835659375" layer="21"/>
<rectangle x1="6.753859375" y1="0.83058125" x2="6.8707" y2="0.835659375" layer="21"/>
<rectangle x1="7.236459375" y1="0.83058125" x2="7.33298125" y2="0.835659375" layer="21"/>
<rectangle x1="7.55141875" y1="0.83058125" x2="7.65301875" y2="0.835659375" layer="21"/>
<rectangle x1="8.1153" y1="0.83058125" x2="8.2169" y2="0.835659375" layer="21"/>
<rectangle x1="0.1905" y1="0.835659375" x2="0.6985" y2="0.840740625" layer="21"/>
<rectangle x1="0.886459375" y1="0.835659375" x2="1.115059375" y2="0.840740625" layer="21"/>
<rectangle x1="1.267459375" y1="0.835659375" x2="1.51638125" y2="0.840740625" layer="21"/>
<rectangle x1="1.699259375" y1="0.835659375" x2="1.9685" y2="0.840740625" layer="21"/>
<rectangle x1="2.15138125" y1="0.835659375" x2="2.385059375" y2="0.840740625" layer="21"/>
<rectangle x1="2.55778125" y1="0.835659375" x2="2.8067" y2="0.840740625" layer="21"/>
<rectangle x1="3.06578125" y1="0.835659375" x2="3.31978125" y2="0.840740625" layer="21"/>
<rectangle x1="3.4671" y1="0.835659375" x2="3.69061875" y2="0.840740625" layer="21"/>
<rectangle x1="3.86841875" y1="0.835659375" x2="4.1021" y2="0.840740625" layer="21"/>
<rectangle x1="4.27481875" y1="0.835659375" x2="4.493259375" y2="0.840740625" layer="21"/>
<rectangle x1="4.68121875" y1="0.835659375" x2="4.7879" y2="0.840740625" layer="21"/>
<rectangle x1="5.25018125" y1="0.835659375" x2="5.37718125" y2="0.840740625" layer="21"/>
<rectangle x1="5.58038125" y1="0.835659375" x2="5.67181875" y2="0.840740625" layer="21"/>
<rectangle x1="6.073140625" y1="0.835659375" x2="6.16458125" y2="0.840740625" layer="21"/>
<rectangle x1="6.4135" y1="0.835659375" x2="6.51001875" y2="0.840740625" layer="21"/>
<rectangle x1="6.74878125" y1="0.835659375" x2="6.8707" y2="0.840740625" layer="21"/>
<rectangle x1="7.236459375" y1="0.835659375" x2="7.3279" y2="0.840740625" layer="21"/>
<rectangle x1="7.55141875" y1="0.835659375" x2="7.6581" y2="0.840740625" layer="21"/>
<rectangle x1="8.1153" y1="0.835659375" x2="8.2169" y2="0.840740625" layer="21"/>
<rectangle x1="0.175259375" y1="0.840740625" x2="0.69341875" y2="0.84581875" layer="21"/>
<rectangle x1="0.891540625" y1="0.840740625" x2="1.10998125" y2="0.84581875" layer="21"/>
<rectangle x1="1.26238125" y1="0.840740625" x2="1.521459375" y2="0.84581875" layer="21"/>
<rectangle x1="1.699259375" y1="0.840740625" x2="1.9685" y2="0.84581875" layer="21"/>
<rectangle x1="2.1463" y1="0.840740625" x2="2.385059375" y2="0.84581875" layer="21"/>
<rectangle x1="2.55778125" y1="0.840740625" x2="2.816859375" y2="0.84581875" layer="21"/>
<rectangle x1="3.0607" y1="0.840740625" x2="3.31978125" y2="0.84581875" layer="21"/>
<rectangle x1="3.46201875" y1="0.840740625" x2="3.69061875" y2="0.84581875" layer="21"/>
<rectangle x1="3.863340625" y1="0.840740625" x2="4.09701875" y2="0.84581875" layer="21"/>
<rectangle x1="4.27481875" y1="0.840740625" x2="4.493259375" y2="0.84581875" layer="21"/>
<rectangle x1="4.6863" y1="0.840740625" x2="4.7879" y2="0.84581875" layer="21"/>
<rectangle x1="5.25018125" y1="0.840740625" x2="5.37718125" y2="0.84581875" layer="21"/>
<rectangle x1="5.585459375" y1="0.840740625" x2="5.6769" y2="0.84581875" layer="21"/>
<rectangle x1="6.073140625" y1="0.840740625" x2="6.16458125" y2="0.84581875" layer="21"/>
<rectangle x1="6.4135" y1="0.840740625" x2="6.504940625" y2="0.84581875" layer="21"/>
<rectangle x1="6.753859375" y1="0.840740625" x2="6.87578125" y2="0.84581875" layer="21"/>
<rectangle x1="7.23138125" y1="0.840740625" x2="7.3279" y2="0.84581875" layer="21"/>
<rectangle x1="7.5565" y1="0.840740625" x2="7.6581" y2="0.84581875" layer="21"/>
<rectangle x1="8.11021875" y1="0.840740625" x2="8.21181875" y2="0.84581875" layer="21"/>
<rectangle x1="0.17018125" y1="0.84581875" x2="0.69341875" y2="0.8509" layer="21"/>
<rectangle x1="0.886459375" y1="0.84581875" x2="1.115059375" y2="0.8509" layer="21"/>
<rectangle x1="1.267459375" y1="0.84581875" x2="1.526540625" y2="0.8509" layer="21"/>
<rectangle x1="1.69418125" y1="0.84581875" x2="1.97358125" y2="0.8509" layer="21"/>
<rectangle x1="2.1463" y1="0.84581875" x2="2.385059375" y2="0.8509" layer="21"/>
<rectangle x1="2.55778125" y1="0.84581875" x2="2.821940625" y2="0.8509" layer="21"/>
<rectangle x1="3.05561875" y1="0.84581875" x2="3.3147" y2="0.8509" layer="21"/>
<rectangle x1="3.46201875" y1="0.84581875" x2="3.685540625" y2="0.8509" layer="21"/>
<rectangle x1="3.863340625" y1="0.84581875" x2="4.09701875" y2="0.8509" layer="21"/>
<rectangle x1="4.27481875" y1="0.84581875" x2="4.493259375" y2="0.8509" layer="21"/>
<rectangle x1="4.6863" y1="0.84581875" x2="4.79298125" y2="0.8509" layer="21"/>
<rectangle x1="5.2451" y1="0.84581875" x2="5.37718125" y2="0.8509" layer="21"/>
<rectangle x1="5.58038125" y1="0.84581875" x2="5.67181875" y2="0.8509" layer="21"/>
<rectangle x1="6.073140625" y1="0.84581875" x2="6.16458125" y2="0.8509" layer="21"/>
<rectangle x1="6.4135" y1="0.84581875" x2="6.51001875" y2="0.8509" layer="21"/>
<rectangle x1="6.753859375" y1="0.84581875" x2="6.880859375" y2="0.8509" layer="21"/>
<rectangle x1="7.23138125" y1="0.84581875" x2="7.3279" y2="0.8509" layer="21"/>
<rectangle x1="7.5565" y1="0.84581875" x2="7.66318125" y2="0.8509" layer="21"/>
<rectangle x1="8.105140625" y1="0.84581875" x2="8.21181875" y2="0.8509" layer="21"/>
<rectangle x1="0.16001875" y1="0.8509" x2="0.688340625" y2="0.85598125" layer="21"/>
<rectangle x1="0.886459375" y1="0.8509" x2="1.115059375" y2="0.85598125" layer="21"/>
<rectangle x1="1.267459375" y1="0.8509" x2="1.526540625" y2="0.85598125" layer="21"/>
<rectangle x1="1.6891" y1="0.8509" x2="1.978659375" y2="0.85598125" layer="21"/>
<rectangle x1="2.14121875" y1="0.8509" x2="2.385059375" y2="0.85598125" layer="21"/>
<rectangle x1="2.5527" y1="0.8509" x2="2.82701875" y2="0.85598125" layer="21"/>
<rectangle x1="3.045459375" y1="0.8509" x2="3.3147" y2="0.85598125" layer="21"/>
<rectangle x1="3.4671" y1="0.8509" x2="3.69061875" y2="0.85598125" layer="21"/>
<rectangle x1="3.858259375" y1="0.8509" x2="4.091940625" y2="0.85598125" layer="21"/>
<rectangle x1="4.27481875" y1="0.8509" x2="4.498340625" y2="0.85598125" layer="21"/>
<rectangle x1="4.69138125" y1="0.8509" x2="4.79298125" y2="0.85598125" layer="21"/>
<rectangle x1="5.24001875" y1="0.8509" x2="5.37718125" y2="0.85598125" layer="21"/>
<rectangle x1="5.58038125" y1="0.8509" x2="5.6769" y2="0.85598125" layer="21"/>
<rectangle x1="6.073140625" y1="0.8509" x2="6.16458125" y2="0.85598125" layer="21"/>
<rectangle x1="6.41858125" y1="0.8509" x2="6.504940625" y2="0.85598125" layer="21"/>
<rectangle x1="6.74878125" y1="0.8509" x2="6.880859375" y2="0.85598125" layer="21"/>
<rectangle x1="7.23138125" y1="0.8509" x2="7.3279" y2="0.85598125" layer="21"/>
<rectangle x1="7.56158125" y1="0.8509" x2="7.66318125" y2="0.85598125" layer="21"/>
<rectangle x1="8.105140625" y1="0.8509" x2="8.206740625" y2="0.85598125" layer="21"/>
<rectangle x1="0.149859375" y1="0.85598125" x2="0.683259375" y2="0.861059375" layer="21"/>
<rectangle x1="0.886459375" y1="0.85598125" x2="1.115059375" y2="0.861059375" layer="21"/>
<rectangle x1="1.26238125" y1="0.85598125" x2="1.53161875" y2="0.861059375" layer="21"/>
<rectangle x1="1.6891" y1="0.85598125" x2="1.983740625" y2="0.861059375" layer="21"/>
<rectangle x1="2.14121875" y1="0.85598125" x2="2.385059375" y2="0.861059375" layer="21"/>
<rectangle x1="2.5527" y1="0.85598125" x2="2.8321" y2="0.861059375" layer="21"/>
<rectangle x1="3.04038125" y1="0.85598125" x2="3.3147" y2="0.861059375" layer="21"/>
<rectangle x1="3.46201875" y1="0.85598125" x2="3.685540625" y2="0.861059375" layer="21"/>
<rectangle x1="3.858259375" y1="0.85598125" x2="4.091940625" y2="0.861059375" layer="21"/>
<rectangle x1="4.2799" y1="0.85598125" x2="4.498340625" y2="0.861059375" layer="21"/>
<rectangle x1="4.69138125" y1="0.85598125" x2="4.798059375" y2="0.861059375" layer="21"/>
<rectangle x1="5.234940625" y1="0.85598125" x2="5.37718125" y2="0.861059375" layer="21"/>
<rectangle x1="5.58038125" y1="0.85598125" x2="5.6769" y2="0.861059375" layer="21"/>
<rectangle x1="6.073140625" y1="0.85598125" x2="6.16458125" y2="0.861059375" layer="21"/>
<rectangle x1="6.4135" y1="0.85598125" x2="6.51001875" y2="0.861059375" layer="21"/>
<rectangle x1="6.74878125" y1="0.85598125" x2="6.885940625" y2="0.861059375" layer="21"/>
<rectangle x1="7.23138125" y1="0.85598125" x2="7.3279" y2="0.861059375" layer="21"/>
<rectangle x1="7.56158125" y1="0.85598125" x2="7.668259375" y2="0.861059375" layer="21"/>
<rectangle x1="8.100059375" y1="0.85598125" x2="8.206740625" y2="0.861059375" layer="21"/>
<rectangle x1="0.14478125" y1="0.861059375" x2="0.6731" y2="0.866140625" layer="21"/>
<rectangle x1="0.891540625" y1="0.861059375" x2="1.10998125" y2="0.866140625" layer="21"/>
<rectangle x1="1.267459375" y1="0.861059375" x2="1.54178125" y2="0.866140625" layer="21"/>
<rectangle x1="1.68401875" y1="0.861059375" x2="1.98881875" y2="0.866140625" layer="21"/>
<rectangle x1="2.136140625" y1="0.861059375" x2="2.385059375" y2="0.866140625" layer="21"/>
<rectangle x1="2.55778125" y1="0.861059375" x2="2.842259375" y2="0.866140625" layer="21"/>
<rectangle x1="3.0353" y1="0.861059375" x2="3.30961875" y2="0.866140625" layer="21"/>
<rectangle x1="3.4671" y1="0.861059375" x2="3.69061875" y2="0.866140625" layer="21"/>
<rectangle x1="3.858259375" y1="0.861059375" x2="4.091940625" y2="0.866140625" layer="21"/>
<rectangle x1="4.2799" y1="0.861059375" x2="4.498340625" y2="0.866140625" layer="21"/>
<rectangle x1="4.696459375" y1="0.861059375" x2="4.803140625" y2="0.866140625" layer="21"/>
<rectangle x1="5.234940625" y1="0.861059375" x2="5.37718125" y2="0.866140625" layer="21"/>
<rectangle x1="5.58038125" y1="0.861059375" x2="5.67181875" y2="0.866140625" layer="21"/>
<rectangle x1="6.073140625" y1="0.861059375" x2="6.16458125" y2="0.866140625" layer="21"/>
<rectangle x1="6.41858125" y1="0.861059375" x2="6.51001875" y2="0.866140625" layer="21"/>
<rectangle x1="6.753859375" y1="0.861059375" x2="6.885940625" y2="0.866140625" layer="21"/>
<rectangle x1="7.2263" y1="0.861059375" x2="7.3279" y2="0.866140625" layer="21"/>
<rectangle x1="7.566659375" y1="0.861059375" x2="7.673340625" y2="0.866140625" layer="21"/>
<rectangle x1="8.09498125" y1="0.861059375" x2="8.201659375" y2="0.866140625" layer="21"/>
<rectangle x1="0.13461875" y1="0.866140625" x2="0.6731" y2="0.87121875" layer="21"/>
<rectangle x1="0.886459375" y1="0.866140625" x2="1.115059375" y2="0.87121875" layer="21"/>
<rectangle x1="1.267459375" y1="0.866140625" x2="1.546859375" y2="0.87121875" layer="21"/>
<rectangle x1="1.678940625" y1="0.866140625" x2="1.99898125" y2="0.87121875" layer="21"/>
<rectangle x1="2.131059375" y1="0.866140625" x2="2.37998125" y2="0.87121875" layer="21"/>
<rectangle x1="2.55778125" y1="0.866140625" x2="2.847340625" y2="0.87121875" layer="21"/>
<rectangle x1="3.025140625" y1="0.866140625" x2="3.30961875" y2="0.87121875" layer="21"/>
<rectangle x1="3.4671" y1="0.866140625" x2="3.69061875" y2="0.87121875" layer="21"/>
<rectangle x1="3.85318125" y1="0.866140625" x2="4.086859375" y2="0.87121875" layer="21"/>
<rectangle x1="4.2799" y1="0.866140625" x2="4.50341875" y2="0.87121875" layer="21"/>
<rectangle x1="4.696459375" y1="0.866140625" x2="4.80821875" y2="0.87121875" layer="21"/>
<rectangle x1="5.229859375" y1="0.866140625" x2="5.37718125" y2="0.87121875" layer="21"/>
<rectangle x1="5.58038125" y1="0.866140625" x2="5.67181875" y2="0.87121875" layer="21"/>
<rectangle x1="6.073140625" y1="0.866140625" x2="6.16458125" y2="0.87121875" layer="21"/>
<rectangle x1="6.4135" y1="0.866140625" x2="6.51001875" y2="0.87121875" layer="21"/>
<rectangle x1="6.74878125" y1="0.866140625" x2="6.89101875" y2="0.87121875" layer="21"/>
<rectangle x1="7.2263" y1="0.866140625" x2="7.32281875" y2="0.87121875" layer="21"/>
<rectangle x1="7.566659375" y1="0.866140625" x2="7.673340625" y2="0.87121875" layer="21"/>
<rectangle x1="8.09498125" y1="0.866140625" x2="8.201659375" y2="0.87121875" layer="21"/>
<rectangle x1="0.129540625" y1="0.87121875" x2="0.662940625" y2="0.8763" layer="21"/>
<rectangle x1="0.891540625" y1="0.87121875" x2="1.115059375" y2="0.8763" layer="21"/>
<rectangle x1="1.267459375" y1="0.87121875" x2="1.55701875" y2="0.8763" layer="21"/>
<rectangle x1="1.66878125" y1="0.87121875" x2="2.004059375" y2="0.8763" layer="21"/>
<rectangle x1="2.1209" y1="0.87121875" x2="2.37998125" y2="0.8763" layer="21"/>
<rectangle x1="2.55778125" y1="0.87121875" x2="2.86258125" y2="0.8763" layer="21"/>
<rectangle x1="3.0099" y1="0.87121875" x2="3.304540625" y2="0.8763" layer="21"/>
<rectangle x1="3.46201875" y1="0.87121875" x2="3.69061875" y2="0.8763" layer="21"/>
<rectangle x1="3.85318125" y1="0.87121875" x2="4.086859375" y2="0.8763" layer="21"/>
<rectangle x1="4.28498125" y1="0.87121875" x2="4.50341875" y2="0.8763" layer="21"/>
<rectangle x1="4.701540625" y1="0.87121875" x2="4.80821875" y2="0.8763" layer="21"/>
<rectangle x1="5.22478125" y1="0.87121875" x2="5.37718125" y2="0.8763" layer="21"/>
<rectangle x1="5.58038125" y1="0.87121875" x2="5.67181875" y2="0.8763" layer="21"/>
<rectangle x1="6.073140625" y1="0.87121875" x2="6.16458125" y2="0.8763" layer="21"/>
<rectangle x1="6.4135" y1="0.87121875" x2="6.504940625" y2="0.8763" layer="21"/>
<rectangle x1="6.753859375" y1="0.87121875" x2="6.89101875" y2="0.8763" layer="21"/>
<rectangle x1="7.22121875" y1="0.87121875" x2="7.32281875" y2="0.8763" layer="21"/>
<rectangle x1="7.571740625" y1="0.87121875" x2="7.67841875" y2="0.8763" layer="21"/>
<rectangle x1="8.0899" y1="0.87121875" x2="8.201659375" y2="0.8763" layer="21"/>
<rectangle x1="0.124459375" y1="0.8763" x2="0.657859375" y2="0.88138125" layer="21"/>
<rectangle x1="0.886459375" y1="0.8763" x2="1.10998125" y2="0.88138125" layer="21"/>
<rectangle x1="1.267459375" y1="0.8763" x2="1.56718125" y2="0.88138125" layer="21"/>
<rectangle x1="1.6637" y1="0.8763" x2="2.0193" y2="0.88138125" layer="21"/>
<rectangle x1="2.11581875" y1="0.8763" x2="2.37998125" y2="0.88138125" layer="21"/>
<rectangle x1="2.5527" y1="0.8763" x2="2.87781875" y2="0.88138125" layer="21"/>
<rectangle x1="2.999740625" y1="0.8763" x2="3.304540625" y2="0.88138125" layer="21"/>
<rectangle x1="3.4671" y1="0.8763" x2="3.685540625" y2="0.88138125" layer="21"/>
<rectangle x1="3.8481" y1="0.8763" x2="4.086859375" y2="0.88138125" layer="21"/>
<rectangle x1="4.28498125" y1="0.8763" x2="4.5085" y2="0.88138125" layer="21"/>
<rectangle x1="4.701540625" y1="0.8763" x2="4.8133" y2="0.88138125" layer="21"/>
<rectangle x1="5.2197" y1="0.8763" x2="5.37718125" y2="0.88138125" layer="21"/>
<rectangle x1="5.58038125" y1="0.8763" x2="5.67181875" y2="0.88138125" layer="21"/>
<rectangle x1="6.073140625" y1="0.8763" x2="6.16458125" y2="0.88138125" layer="21"/>
<rectangle x1="6.4135" y1="0.8763" x2="6.504940625" y2="0.88138125" layer="21"/>
<rectangle x1="6.753859375" y1="0.8763" x2="6.8961" y2="0.88138125" layer="21"/>
<rectangle x1="7.22121875" y1="0.8763" x2="7.32281875" y2="0.88138125" layer="21"/>
<rectangle x1="7.571740625" y1="0.8763" x2="7.6835" y2="0.88138125" layer="21"/>
<rectangle x1="8.08481875" y1="0.8763" x2="8.19658125" y2="0.88138125" layer="21"/>
<rectangle x1="0.11938125" y1="0.88138125" x2="0.65278125" y2="0.886459375" layer="21"/>
<rectangle x1="0.886459375" y1="0.88138125" x2="1.115059375" y2="0.886459375" layer="21"/>
<rectangle x1="1.26238125" y1="0.88138125" x2="1.5875" y2="0.886459375" layer="21"/>
<rectangle x1="1.648459375" y1="0.88138125" x2="2.034540625" y2="0.886459375" layer="21"/>
<rectangle x1="2.10058125" y1="0.88138125" x2="2.37998125" y2="0.886459375" layer="21"/>
<rectangle x1="2.55778125" y1="0.88138125" x2="2.898140625" y2="0.886459375" layer="21"/>
<rectangle x1="2.974340625" y1="0.88138125" x2="3.304540625" y2="0.886459375" layer="21"/>
<rectangle x1="3.46201875" y1="0.88138125" x2="3.69061875" y2="0.886459375" layer="21"/>
<rectangle x1="3.8481" y1="0.88138125" x2="4.08178125" y2="0.886459375" layer="21"/>
<rectangle x1="4.28498125" y1="0.88138125" x2="4.5085" y2="0.886459375" layer="21"/>
<rectangle x1="4.70661875" y1="0.88138125" x2="4.81838125" y2="0.886459375" layer="21"/>
<rectangle x1="5.21461875" y1="0.88138125" x2="5.37718125" y2="0.886459375" layer="21"/>
<rectangle x1="5.58038125" y1="0.88138125" x2="5.6769" y2="0.886459375" layer="21"/>
<rectangle x1="6.073140625" y1="0.88138125" x2="6.16458125" y2="0.886459375" layer="21"/>
<rectangle x1="6.41858125" y1="0.88138125" x2="6.51001875" y2="0.886459375" layer="21"/>
<rectangle x1="6.74878125" y1="0.88138125" x2="6.90118125" y2="0.886459375" layer="21"/>
<rectangle x1="7.22121875" y1="0.88138125" x2="7.32281875" y2="0.886459375" layer="21"/>
<rectangle x1="7.57681875" y1="0.88138125" x2="7.68858125" y2="0.886459375" layer="21"/>
<rectangle x1="8.079740625" y1="0.88138125" x2="8.1915" y2="0.886459375" layer="21"/>
<rectangle x1="0.10921875" y1="0.886459375" x2="0.6477" y2="0.891540625" layer="21"/>
<rectangle x1="0.886459375" y1="0.886459375" x2="1.10998125" y2="0.891540625" layer="21"/>
<rectangle x1="1.267459375" y1="0.886459375" x2="1.61798125" y2="0.891540625" layer="21"/>
<rectangle x1="1.623059375" y1="0.886459375" x2="2.37998125" y2="0.891540625" layer="21"/>
<rectangle x1="2.55778125" y1="0.886459375" x2="3.299459375" y2="0.891540625" layer="21"/>
<rectangle x1="3.46201875" y1="0.886459375" x2="3.685540625" y2="0.891540625" layer="21"/>
<rectangle x1="3.84301875" y1="0.886459375" x2="4.08178125" y2="0.891540625" layer="21"/>
<rectangle x1="4.290059375" y1="0.886459375" x2="4.5085" y2="0.891540625" layer="21"/>
<rectangle x1="4.70661875" y1="0.886459375" x2="4.823459375" y2="0.891540625" layer="21"/>
<rectangle x1="5.209540625" y1="0.886459375" x2="5.37718125" y2="0.891540625" layer="21"/>
<rectangle x1="5.58038125" y1="0.886459375" x2="5.67181875" y2="0.891540625" layer="21"/>
<rectangle x1="6.073140625" y1="0.886459375" x2="6.16458125" y2="0.891540625" layer="21"/>
<rectangle x1="6.4135" y1="0.886459375" x2="6.51001875" y2="0.891540625" layer="21"/>
<rectangle x1="6.74878125" y1="0.886459375" x2="6.90118125" y2="0.891540625" layer="21"/>
<rectangle x1="7.216140625" y1="0.886459375" x2="7.317740625" y2="0.891540625" layer="21"/>
<rectangle x1="7.57681875" y1="0.886459375" x2="7.693659375" y2="0.891540625" layer="21"/>
<rectangle x1="8.074659375" y1="0.886459375" x2="8.1915" y2="0.891540625" layer="21"/>
<rectangle x1="0.104140625" y1="0.891540625" x2="0.637540625" y2="0.89661875" layer="21"/>
<rectangle x1="0.891540625" y1="0.891540625" x2="1.115059375" y2="0.89661875" layer="21"/>
<rectangle x1="1.267459375" y1="0.891540625" x2="2.37998125" y2="0.89661875" layer="21"/>
<rectangle x1="2.55778125" y1="0.891540625" x2="3.299459375" y2="0.89661875" layer="21"/>
<rectangle x1="3.4671" y1="0.891540625" x2="3.69061875" y2="0.89661875" layer="21"/>
<rectangle x1="3.84301875" y1="0.891540625" x2="4.0767" y2="0.89661875" layer="21"/>
<rectangle x1="4.290059375" y1="0.891540625" x2="4.51358125" y2="0.89661875" layer="21"/>
<rectangle x1="4.7117" y1="0.891540625" x2="4.828540625" y2="0.89661875" layer="21"/>
<rectangle x1="5.204459375" y1="0.891540625" x2="5.37718125" y2="0.89661875" layer="21"/>
<rectangle x1="5.58038125" y1="0.891540625" x2="5.6769" y2="0.89661875" layer="21"/>
<rectangle x1="6.073140625" y1="0.891540625" x2="6.16458125" y2="0.89661875" layer="21"/>
<rectangle x1="6.41858125" y1="0.891540625" x2="6.504940625" y2="0.89661875" layer="21"/>
<rectangle x1="6.74878125" y1="0.891540625" x2="6.906259375" y2="0.89661875" layer="21"/>
<rectangle x1="7.211059375" y1="0.891540625" x2="7.317740625" y2="0.89661875" layer="21"/>
<rectangle x1="7.5819" y1="0.891540625" x2="7.698740625" y2="0.89661875" layer="21"/>
<rectangle x1="8.06958125" y1="0.891540625" x2="8.18641875" y2="0.89661875" layer="21"/>
<rectangle x1="0.099059375" y1="0.89661875" x2="0.632459375" y2="0.9017" layer="21"/>
<rectangle x1="0.886459375" y1="0.89661875" x2="1.115059375" y2="0.9017" layer="21"/>
<rectangle x1="1.26238125" y1="0.89661875" x2="2.3749" y2="0.9017" layer="21"/>
<rectangle x1="2.55778125" y1="0.89661875" x2="3.29438125" y2="0.9017" layer="21"/>
<rectangle x1="3.4671" y1="0.89661875" x2="3.685540625" y2="0.9017" layer="21"/>
<rectangle x1="3.84301875" y1="0.89661875" x2="4.0767" y2="0.9017" layer="21"/>
<rectangle x1="4.295140625" y1="0.89661875" x2="4.51358125" y2="0.9017" layer="21"/>
<rectangle x1="4.71678125" y1="0.89661875" x2="4.83361875" y2="0.9017" layer="21"/>
<rectangle x1="5.19938125" y1="0.89661875" x2="5.37718125" y2="0.9017" layer="21"/>
<rectangle x1="5.58038125" y1="0.89661875" x2="5.6769" y2="0.9017" layer="21"/>
<rectangle x1="6.073140625" y1="0.89661875" x2="6.16458125" y2="0.9017" layer="21"/>
<rectangle x1="6.41858125" y1="0.89661875" x2="6.51001875" y2="0.9017" layer="21"/>
<rectangle x1="6.74878125" y1="0.89661875" x2="6.911340625" y2="0.9017" layer="21"/>
<rectangle x1="7.211059375" y1="0.89661875" x2="7.317740625" y2="0.9017" layer="21"/>
<rectangle x1="7.58698125" y1="0.89661875" x2="7.70381875" y2="0.9017" layer="21"/>
<rectangle x1="8.0645" y1="0.89661875" x2="8.181340625" y2="0.9017" layer="21"/>
<rectangle x1="0.09398125" y1="0.9017" x2="0.6223" y2="0.90678125" layer="21"/>
<rectangle x1="0.891540625" y1="0.9017" x2="1.10998125" y2="0.90678125" layer="21"/>
<rectangle x1="1.267459375" y1="0.9017" x2="2.3749" y2="0.90678125" layer="21"/>
<rectangle x1="2.55778125" y1="0.9017" x2="3.29438125" y2="0.90678125" layer="21"/>
<rectangle x1="3.46201875" y1="0.9017" x2="3.69061875" y2="0.90678125" layer="21"/>
<rectangle x1="3.837940625" y1="0.9017" x2="4.0767" y2="0.90678125" layer="21"/>
<rectangle x1="4.295140625" y1="0.9017" x2="4.518659375" y2="0.90678125" layer="21"/>
<rectangle x1="4.71678125" y1="0.9017" x2="4.8387" y2="0.90678125" layer="21"/>
<rectangle x1="5.1943" y1="0.9017" x2="5.37718125" y2="0.90678125" layer="21"/>
<rectangle x1="5.58038125" y1="0.9017" x2="5.67181875" y2="0.90678125" layer="21"/>
<rectangle x1="6.073140625" y1="0.9017" x2="6.16458125" y2="0.90678125" layer="21"/>
<rectangle x1="6.4135" y1="0.9017" x2="6.504940625" y2="0.90678125" layer="21"/>
<rectangle x1="6.74878125" y1="0.9017" x2="6.91641875" y2="0.90678125" layer="21"/>
<rectangle x1="7.20598125" y1="0.9017" x2="7.312659375" y2="0.90678125" layer="21"/>
<rectangle x1="7.58698125" y1="0.9017" x2="7.7089" y2="0.90678125" layer="21"/>
<rectangle x1="8.05941875" y1="0.9017" x2="8.181340625" y2="0.90678125" layer="21"/>
<rectangle x1="0.09398125" y1="0.90678125" x2="0.612140625" y2="0.911859375" layer="21"/>
<rectangle x1="0.886459375" y1="0.90678125" x2="1.115059375" y2="0.911859375" layer="21"/>
<rectangle x1="1.267459375" y1="0.90678125" x2="2.3749" y2="0.911859375" layer="21"/>
<rectangle x1="2.55778125" y1="0.90678125" x2="3.2893" y2="0.911859375" layer="21"/>
<rectangle x1="3.4671" y1="0.90678125" x2="3.685540625" y2="0.911859375" layer="21"/>
<rectangle x1="3.837940625" y1="0.90678125" x2="4.07161875" y2="0.911859375" layer="21"/>
<rectangle x1="4.295140625" y1="0.90678125" x2="4.518659375" y2="0.911859375" layer="21"/>
<rectangle x1="4.721859375" y1="0.90678125" x2="4.848859375" y2="0.911859375" layer="21"/>
<rectangle x1="5.184140625" y1="0.90678125" x2="5.37718125" y2="0.911859375" layer="21"/>
<rectangle x1="5.58038125" y1="0.90678125" x2="5.67181875" y2="0.911859375" layer="21"/>
<rectangle x1="6.073140625" y1="0.90678125" x2="6.16458125" y2="0.911859375" layer="21"/>
<rectangle x1="6.41858125" y1="0.90678125" x2="6.51001875" y2="0.911859375" layer="21"/>
<rectangle x1="6.74878125" y1="0.90678125" x2="6.9215" y2="0.911859375" layer="21"/>
<rectangle x1="7.20598125" y1="0.90678125" x2="7.312659375" y2="0.911859375" layer="21"/>
<rectangle x1="7.592059375" y1="0.90678125" x2="7.719059375" y2="0.911859375" layer="21"/>
<rectangle x1="8.054340625" y1="0.90678125" x2="8.176259375" y2="0.911859375" layer="21"/>
<rectangle x1="0.08381875" y1="0.911859375" x2="0.607059375" y2="0.916940625" layer="21"/>
<rectangle x1="0.891540625" y1="0.911859375" x2="1.10998125" y2="0.916940625" layer="21"/>
<rectangle x1="1.267459375" y1="0.911859375" x2="2.36981875" y2="0.916940625" layer="21"/>
<rectangle x1="2.5527" y1="0.911859375" x2="3.2893" y2="0.916940625" layer="21"/>
<rectangle x1="3.46201875" y1="0.911859375" x2="3.69061875" y2="0.916940625" layer="21"/>
<rectangle x1="3.832859375" y1="0.911859375" x2="4.07161875" y2="0.916940625" layer="21"/>
<rectangle x1="4.295140625" y1="0.911859375" x2="4.518659375" y2="0.916940625" layer="21"/>
<rectangle x1="4.726940625" y1="0.911859375" x2="4.853940625" y2="0.916940625" layer="21"/>
<rectangle x1="5.17398125" y1="0.911859375" x2="5.37718125" y2="0.916940625" layer="21"/>
<rectangle x1="5.58038125" y1="0.911859375" x2="5.67181875" y2="0.916940625" layer="21"/>
<rectangle x1="6.073140625" y1="0.911859375" x2="6.16458125" y2="0.916940625" layer="21"/>
<rectangle x1="6.4135" y1="0.911859375" x2="6.504940625" y2="0.916940625" layer="21"/>
<rectangle x1="6.74878125" y1="0.911859375" x2="6.92658125" y2="0.916940625" layer="21"/>
<rectangle x1="7.2009" y1="0.911859375" x2="7.312659375" y2="0.916940625" layer="21"/>
<rectangle x1="7.597140625" y1="0.911859375" x2="7.724140625" y2="0.916940625" layer="21"/>
<rectangle x1="8.04418125" y1="0.911859375" x2="8.17118125" y2="0.916940625" layer="21"/>
<rectangle x1="0.08381875" y1="0.916940625" x2="0.5969" y2="0.92201875" layer="21"/>
<rectangle x1="0.886459375" y1="0.916940625" x2="1.115059375" y2="0.92201875" layer="21"/>
<rectangle x1="1.267459375" y1="0.916940625" x2="2.36981875" y2="0.92201875" layer="21"/>
<rectangle x1="2.55778125" y1="0.916940625" x2="3.28421875" y2="0.92201875" layer="21"/>
<rectangle x1="3.4671" y1="0.916940625" x2="3.69061875" y2="0.92201875" layer="21"/>
<rectangle x1="3.832859375" y1="0.916940625" x2="4.066540625" y2="0.92201875" layer="21"/>
<rectangle x1="4.30021875" y1="0.916940625" x2="4.523740625" y2="0.92201875" layer="21"/>
<rectangle x1="4.73201875" y1="0.916940625" x2="4.85901875" y2="0.92201875" layer="21"/>
<rectangle x1="5.1689" y1="0.916940625" x2="5.37718125" y2="0.92201875" layer="21"/>
<rectangle x1="5.58038125" y1="0.916940625" x2="5.67181875" y2="0.92201875" layer="21"/>
<rectangle x1="6.073140625" y1="0.916940625" x2="6.16458125" y2="0.92201875" layer="21"/>
<rectangle x1="6.41858125" y1="0.916940625" x2="6.51001875" y2="0.92201875" layer="21"/>
<rectangle x1="6.74878125" y1="0.916940625" x2="6.84021875" y2="0.92201875" layer="21"/>
<rectangle x1="6.8453" y1="0.916940625" x2="6.936740625" y2="0.92201875" layer="21"/>
<rectangle x1="7.19581875" y1="0.916940625" x2="7.30758125" y2="0.92201875" layer="21"/>
<rectangle x1="7.60221875" y1="0.916940625" x2="7.72921875" y2="0.92201875" layer="21"/>
<rectangle x1="8.0391" y1="0.916940625" x2="8.17118125" y2="0.92201875" layer="21"/>
<rectangle x1="0.078740625" y1="0.92201875" x2="0.586740625" y2="0.9271" layer="21"/>
<rectangle x1="0.891540625" y1="0.92201875" x2="1.115059375" y2="0.9271" layer="21"/>
<rectangle x1="1.26238125" y1="0.92201875" x2="2.36981875" y2="0.9271" layer="21"/>
<rectangle x1="2.55778125" y1="0.92201875" x2="3.279140625" y2="0.9271" layer="21"/>
<rectangle x1="3.46201875" y1="0.92201875" x2="3.69061875" y2="0.9271" layer="21"/>
<rectangle x1="3.832859375" y1="0.92201875" x2="4.066540625" y2="0.9271" layer="21"/>
<rectangle x1="4.30021875" y1="0.92201875" x2="4.523740625" y2="0.9271" layer="21"/>
<rectangle x1="4.73201875" y1="0.92201875" x2="4.86918125" y2="0.9271" layer="21"/>
<rectangle x1="5.16381875" y1="0.92201875" x2="5.280659375" y2="0.9271" layer="21"/>
<rectangle x1="5.285740625" y1="0.92201875" x2="5.37718125" y2="0.9271" layer="21"/>
<rectangle x1="5.58038125" y1="0.92201875" x2="5.6769" y2="0.9271" layer="21"/>
<rectangle x1="6.073140625" y1="0.92201875" x2="6.16458125" y2="0.9271" layer="21"/>
<rectangle x1="6.4135" y1="0.92201875" x2="6.51001875" y2="0.9271" layer="21"/>
<rectangle x1="6.74878125" y1="0.92201875" x2="6.84021875" y2="0.9271" layer="21"/>
<rectangle x1="6.85038125" y1="0.92201875" x2="6.94181875" y2="0.9271" layer="21"/>
<rectangle x1="7.190740625" y1="0.92201875" x2="7.30758125" y2="0.9271" layer="21"/>
<rectangle x1="7.60221875" y1="0.92201875" x2="7.73938125" y2="0.9271" layer="21"/>
<rectangle x1="8.03401875" y1="0.92201875" x2="8.1661" y2="0.9271" layer="21"/>
<rectangle x1="0.073659375" y1="0.9271" x2="0.5715" y2="0.93218125" layer="21"/>
<rectangle x1="0.886459375" y1="0.9271" x2="1.10998125" y2="0.93218125" layer="21"/>
<rectangle x1="1.267459375" y1="0.9271" x2="2.36981875" y2="0.93218125" layer="21"/>
<rectangle x1="2.55778125" y1="0.9271" x2="3.279140625" y2="0.93218125" layer="21"/>
<rectangle x1="3.4671" y1="0.9271" x2="3.685540625" y2="0.93218125" layer="21"/>
<rectangle x1="3.82778125" y1="0.9271" x2="4.066540625" y2="0.93218125" layer="21"/>
<rectangle x1="4.30021875" y1="0.9271" x2="4.523740625" y2="0.93218125" layer="21"/>
<rectangle x1="4.7371" y1="0.9271" x2="4.874259375" y2="0.93218125" layer="21"/>
<rectangle x1="5.153659375" y1="0.9271" x2="5.27558125" y2="0.93218125" layer="21"/>
<rectangle x1="5.285740625" y1="0.9271" x2="5.37718125" y2="0.93218125" layer="21"/>
<rectangle x1="5.58038125" y1="0.9271" x2="5.67181875" y2="0.93218125" layer="21"/>
<rectangle x1="6.073140625" y1="0.9271" x2="6.16458125" y2="0.93218125" layer="21"/>
<rectangle x1="6.41858125" y1="0.9271" x2="6.51001875" y2="0.93218125" layer="21"/>
<rectangle x1="6.74878125" y1="0.9271" x2="6.84021875" y2="0.93218125" layer="21"/>
<rectangle x1="6.85038125" y1="0.9271" x2="6.9469" y2="0.93218125" layer="21"/>
<rectangle x1="7.185659375" y1="0.9271" x2="7.3025" y2="0.93218125" layer="21"/>
<rectangle x1="7.6073" y1="0.9271" x2="7.744459375" y2="0.93218125" layer="21"/>
<rectangle x1="8.023859375" y1="0.9271" x2="8.16101875" y2="0.93218125" layer="21"/>
<rectangle x1="0.06858125" y1="0.93218125" x2="0.561340625" y2="0.937259375" layer="21"/>
<rectangle x1="0.886459375" y1="0.93218125" x2="1.115059375" y2="0.937259375" layer="21"/>
<rectangle x1="1.267459375" y1="0.93218125" x2="2.364740625" y2="0.937259375" layer="21"/>
<rectangle x1="2.5527" y1="0.93218125" x2="3.274059375" y2="0.937259375" layer="21"/>
<rectangle x1="3.46201875" y1="0.93218125" x2="3.69061875" y2="0.937259375" layer="21"/>
<rectangle x1="3.8227" y1="0.93218125" x2="4.061459375" y2="0.937259375" layer="21"/>
<rectangle x1="4.3053" y1="0.93218125" x2="4.52881875" y2="0.937259375" layer="21"/>
<rectangle x1="4.74218125" y1="0.93218125" x2="4.88441875" y2="0.937259375" layer="21"/>
<rectangle x1="5.1435" y1="0.93218125" x2="5.27558125" y2="0.937259375" layer="21"/>
<rectangle x1="5.285740625" y1="0.93218125" x2="5.37718125" y2="0.937259375" layer="21"/>
<rectangle x1="5.58038125" y1="0.93218125" x2="5.6769" y2="0.937259375" layer="21"/>
<rectangle x1="6.073140625" y1="0.93218125" x2="6.16458125" y2="0.937259375" layer="21"/>
<rectangle x1="6.4135" y1="0.93218125" x2="6.504940625" y2="0.937259375" layer="21"/>
<rectangle x1="6.74878125" y1="0.93218125" x2="6.84021875" y2="0.937259375" layer="21"/>
<rectangle x1="6.855459375" y1="0.93218125" x2="6.957059375" y2="0.937259375" layer="21"/>
<rectangle x1="7.18058125" y1="0.93218125" x2="7.3025" y2="0.937259375" layer="21"/>
<rectangle x1="7.61238125" y1="0.93218125" x2="7.75461875" y2="0.937259375" layer="21"/>
<rectangle x1="8.0137" y1="0.93218125" x2="8.155940625" y2="0.937259375" layer="21"/>
<rectangle x1="0.06858125" y1="0.937259375" x2="0.55118125" y2="0.942340625" layer="21"/>
<rectangle x1="0.886459375" y1="0.937259375" x2="1.10998125" y2="0.942340625" layer="21"/>
<rectangle x1="1.26238125" y1="0.937259375" x2="2.364740625" y2="0.942340625" layer="21"/>
<rectangle x1="2.55778125" y1="0.937259375" x2="3.274059375" y2="0.942340625" layer="21"/>
<rectangle x1="3.4671" y1="0.937259375" x2="3.685540625" y2="0.942340625" layer="21"/>
<rectangle x1="3.8227" y1="0.937259375" x2="4.061459375" y2="0.942340625" layer="21"/>
<rectangle x1="4.3053" y1="0.937259375" x2="4.52881875" y2="0.942340625" layer="21"/>
<rectangle x1="4.747259375" y1="0.937259375" x2="4.89458125" y2="0.942340625" layer="21"/>
<rectangle x1="5.133340625" y1="0.937259375" x2="5.2705" y2="0.942340625" layer="21"/>
<rectangle x1="5.285740625" y1="0.937259375" x2="5.37718125" y2="0.942340625" layer="21"/>
<rectangle x1="5.58038125" y1="0.937259375" x2="5.6769" y2="0.942340625" layer="21"/>
<rectangle x1="6.073140625" y1="0.937259375" x2="6.16458125" y2="0.942340625" layer="21"/>
<rectangle x1="6.41858125" y1="0.937259375" x2="6.504940625" y2="0.942340625" layer="21"/>
<rectangle x1="6.74878125" y1="0.937259375" x2="6.84021875" y2="0.942340625" layer="21"/>
<rectangle x1="6.855459375" y1="0.937259375" x2="6.962140625" y2="0.942340625" layer="21"/>
<rectangle x1="7.1755" y1="0.937259375" x2="7.29741875" y2="0.942340625" layer="21"/>
<rectangle x1="7.617459375" y1="0.937259375" x2="7.76478125" y2="0.942340625" layer="21"/>
<rectangle x1="8.003540625" y1="0.937259375" x2="8.150859375" y2="0.942340625" layer="21"/>
<rectangle x1="0.0635" y1="0.942340625" x2="0.535940625" y2="0.94741875" layer="21"/>
<rectangle x1="0.891540625" y1="0.942340625" x2="1.115059375" y2="0.94741875" layer="21"/>
<rectangle x1="1.267459375" y1="0.942340625" x2="2.359659375" y2="0.94741875" layer="21"/>
<rectangle x1="2.55778125" y1="0.942340625" x2="3.26898125" y2="0.94741875" layer="21"/>
<rectangle x1="3.46201875" y1="0.942340625" x2="3.69061875" y2="0.94741875" layer="21"/>
<rectangle x1="3.8227" y1="0.942340625" x2="4.05638125" y2="0.94741875" layer="21"/>
<rectangle x1="4.3053" y1="0.942340625" x2="4.5339" y2="0.94741875" layer="21"/>
<rectangle x1="4.752340625" y1="0.942340625" x2="4.904740625" y2="0.94741875" layer="21"/>
<rectangle x1="5.12318125" y1="0.942340625" x2="5.26541875" y2="0.94741875" layer="21"/>
<rectangle x1="5.285740625" y1="0.942340625" x2="5.37718125" y2="0.94741875" layer="21"/>
<rectangle x1="5.58038125" y1="0.942340625" x2="5.67181875" y2="0.94741875" layer="21"/>
<rectangle x1="6.073140625" y1="0.942340625" x2="6.16458125" y2="0.94741875" layer="21"/>
<rectangle x1="6.4135" y1="0.942340625" x2="6.51001875" y2="0.94741875" layer="21"/>
<rectangle x1="6.74878125" y1="0.942340625" x2="6.84021875" y2="0.94741875" layer="21"/>
<rectangle x1="6.860540625" y1="0.942340625" x2="6.9723" y2="0.94741875" layer="21"/>
<rectangle x1="7.165340625" y1="0.942340625" x2="7.29741875" y2="0.94741875" layer="21"/>
<rectangle x1="7.622540625" y1="0.942340625" x2="7.774940625" y2="0.94741875" layer="21"/>
<rectangle x1="7.99338125" y1="0.942340625" x2="8.14578125" y2="0.94741875" layer="21"/>
<rectangle x1="0.0635" y1="0.94741875" x2="0.5207" y2="0.9525" layer="21"/>
<rectangle x1="0.886459375" y1="0.94741875" x2="1.115059375" y2="0.9525" layer="21"/>
<rectangle x1="1.267459375" y1="0.94741875" x2="2.359659375" y2="0.9525" layer="21"/>
<rectangle x1="2.55778125" y1="0.94741875" x2="3.2639" y2="0.9525" layer="21"/>
<rectangle x1="3.4671" y1="0.94741875" x2="3.69061875" y2="0.9525" layer="21"/>
<rectangle x1="3.81761875" y1="0.94741875" x2="4.05638125" y2="0.9525" layer="21"/>
<rectangle x1="4.31038125" y1="0.94741875" x2="4.5339" y2="0.9525" layer="21"/>
<rectangle x1="4.75741875" y1="0.94741875" x2="4.9149" y2="0.9525" layer="21"/>
<rectangle x1="5.11301875" y1="0.94741875" x2="5.260340625" y2="0.9525" layer="21"/>
<rectangle x1="5.285740625" y1="0.94741875" x2="5.37718125" y2="0.9525" layer="21"/>
<rectangle x1="5.58038125" y1="0.94741875" x2="5.67181875" y2="0.9525" layer="21"/>
<rectangle x1="6.073140625" y1="0.94741875" x2="6.16458125" y2="0.9525" layer="21"/>
<rectangle x1="6.41858125" y1="0.94741875" x2="6.51001875" y2="0.9525" layer="21"/>
<rectangle x1="6.74878125" y1="0.94741875" x2="6.84021875" y2="0.9525" layer="21"/>
<rectangle x1="6.86561875" y1="0.94741875" x2="6.987540625" y2="0.9525" layer="21"/>
<rectangle x1="7.15518125" y1="0.94741875" x2="7.292340625" y2="0.9525" layer="21"/>
<rectangle x1="7.62761875" y1="0.94741875" x2="7.79018125" y2="0.9525" layer="21"/>
<rectangle x1="7.978140625" y1="0.94741875" x2="8.1407" y2="0.9525" layer="21"/>
<rectangle x1="0.05841875" y1="0.9525" x2="0.50038125" y2="0.95758125" layer="21"/>
<rectangle x1="0.891540625" y1="0.9525" x2="1.10998125" y2="0.95758125" layer="21"/>
<rectangle x1="1.267459375" y1="0.9525" x2="2.359659375" y2="0.95758125" layer="21"/>
<rectangle x1="2.5527" y1="0.9525" x2="3.25881875" y2="0.95758125" layer="21"/>
<rectangle x1="3.46201875" y1="0.9525" x2="3.69061875" y2="0.95758125" layer="21"/>
<rectangle x1="3.81761875" y1="0.9525" x2="4.05638125" y2="0.95758125" layer="21"/>
<rectangle x1="4.31038125" y1="0.9525" x2="4.5339" y2="0.95758125" layer="21"/>
<rectangle x1="4.7625" y1="0.9525" x2="4.93521875" y2="0.95758125" layer="21"/>
<rectangle x1="5.0927" y1="0.9525" x2="5.255259375" y2="0.95758125" layer="21"/>
<rectangle x1="5.285740625" y1="0.9525" x2="5.37718125" y2="0.95758125" layer="21"/>
<rectangle x1="5.58038125" y1="0.9525" x2="5.67181875" y2="0.95758125" layer="21"/>
<rectangle x1="6.073140625" y1="0.9525" x2="6.16458125" y2="0.95758125" layer="21"/>
<rectangle x1="6.4135" y1="0.9525" x2="6.504940625" y2="0.95758125" layer="21"/>
<rectangle x1="6.74878125" y1="0.9525" x2="6.84021875" y2="0.95758125" layer="21"/>
<rectangle x1="6.8707" y1="0.9525" x2="7.00278125" y2="0.95758125" layer="21"/>
<rectangle x1="7.14501875" y1="0.9525" x2="7.287259375" y2="0.95758125" layer="21"/>
<rectangle x1="7.6327" y1="0.9525" x2="7.800340625" y2="0.95758125" layer="21"/>
<rectangle x1="7.9629" y1="0.9525" x2="8.13561875" y2="0.95758125" layer="21"/>
<rectangle x1="0.053340625" y1="0.95758125" x2="0.485140625" y2="0.962659375" layer="21"/>
<rectangle x1="0.886459375" y1="0.95758125" x2="1.115059375" y2="0.962659375" layer="21"/>
<rectangle x1="1.267459375" y1="0.95758125" x2="2.35458125" y2="0.962659375" layer="21"/>
<rectangle x1="2.55778125" y1="0.95758125" x2="3.25881875" y2="0.962659375" layer="21"/>
<rectangle x1="3.46201875" y1="0.95758125" x2="3.685540625" y2="0.962659375" layer="21"/>
<rectangle x1="3.812540625" y1="0.95758125" x2="4.0513" y2="0.962659375" layer="21"/>
<rectangle x1="4.315459375" y1="0.95758125" x2="4.53898125" y2="0.962659375" layer="21"/>
<rectangle x1="4.76758125" y1="0.95758125" x2="4.955540625" y2="0.962659375" layer="21"/>
<rectangle x1="5.077459375" y1="0.95758125" x2="5.255259375" y2="0.962659375" layer="21"/>
<rectangle x1="5.285740625" y1="0.95758125" x2="5.37718125" y2="0.962659375" layer="21"/>
<rectangle x1="5.58038125" y1="0.95758125" x2="5.67181875" y2="0.962659375" layer="21"/>
<rectangle x1="6.073140625" y1="0.95758125" x2="6.16458125" y2="0.962659375" layer="21"/>
<rectangle x1="6.41858125" y1="0.95758125" x2="6.51001875" y2="0.962659375" layer="21"/>
<rectangle x1="6.74878125" y1="0.95758125" x2="6.84021875" y2="0.962659375" layer="21"/>
<rectangle x1="6.8707" y1="0.95758125" x2="7.0231" y2="0.962659375" layer="21"/>
<rectangle x1="7.12978125" y1="0.95758125" x2="7.287259375" y2="0.962659375" layer="21"/>
<rectangle x1="7.63778125" y1="0.95758125" x2="7.825740625" y2="0.962659375" layer="21"/>
<rectangle x1="7.947659375" y1="0.95758125" x2="8.130540625" y2="0.962659375" layer="21"/>
<rectangle x1="0.053340625" y1="0.962659375" x2="0.46481875" y2="0.967740625" layer="21"/>
<rectangle x1="0.886459375" y1="0.962659375" x2="1.10998125" y2="0.967740625" layer="21"/>
<rectangle x1="1.26238125" y1="0.962659375" x2="1.907540625" y2="0.967740625" layer="21"/>
<rectangle x1="1.91261875" y1="0.962659375" x2="2.35458125" y2="0.967740625" layer="21"/>
<rectangle x1="2.55778125" y1="0.962659375" x2="3.253740625" y2="0.967740625" layer="21"/>
<rectangle x1="3.4671" y1="0.962659375" x2="3.69061875" y2="0.967740625" layer="21"/>
<rectangle x1="3.812540625" y1="0.962659375" x2="4.0513" y2="0.967740625" layer="21"/>
<rectangle x1="4.315459375" y1="0.962659375" x2="4.53898125" y2="0.967740625" layer="21"/>
<rectangle x1="4.772659375" y1="0.962659375" x2="4.980940625" y2="0.967740625" layer="21"/>
<rectangle x1="4.98601875" y1="0.962659375" x2="4.9911" y2="0.967740625" layer="21"/>
<rectangle x1="5.01141875" y1="0.962659375" x2="5.0165" y2="0.967740625" layer="21"/>
<rectangle x1="5.0419" y1="0.962659375" x2="5.2451" y2="0.967740625" layer="21"/>
<rectangle x1="5.285740625" y1="0.962659375" x2="5.37718125" y2="0.967740625" layer="21"/>
<rectangle x1="5.585459375" y1="0.962659375" x2="5.6769" y2="0.967740625" layer="21"/>
<rectangle x1="6.073140625" y1="0.962659375" x2="6.16458125" y2="0.967740625" layer="21"/>
<rectangle x1="6.4135" y1="0.962659375" x2="6.504940625" y2="0.967740625" layer="21"/>
<rectangle x1="6.74878125" y1="0.962659375" x2="6.84021875" y2="0.967740625" layer="21"/>
<rectangle x1="6.87578125" y1="0.962659375" x2="7.0485" y2="0.967740625" layer="21"/>
<rectangle x1="7.05358125" y1="0.962659375" x2="7.058659375" y2="0.967740625" layer="21"/>
<rectangle x1="7.09421875" y1="0.962659375" x2="7.28218125" y2="0.967740625" layer="21"/>
<rectangle x1="7.642859375" y1="0.962659375" x2="7.851140625" y2="0.967740625" layer="21"/>
<rectangle x1="7.85621875" y1="0.962659375" x2="7.86638125" y2="0.967740625" layer="21"/>
<rectangle x1="7.8867" y1="0.962659375" x2="7.89178125" y2="0.967740625" layer="21"/>
<rectangle x1="7.90701875" y1="0.962659375" x2="7.9121" y2="0.967740625" layer="21"/>
<rectangle x1="7.91718125" y1="0.962659375" x2="8.125459375" y2="0.967740625" layer="21"/>
<rectangle x1="0.048259375" y1="0.967740625" x2="0.44958125" y2="0.97281875" layer="21"/>
<rectangle x1="0.886459375" y1="0.967740625" x2="1.115059375" y2="0.97281875" layer="21"/>
<rectangle x1="1.267459375" y1="0.967740625" x2="1.907540625" y2="0.97281875" layer="21"/>
<rectangle x1="1.91261875" y1="0.967740625" x2="2.3495" y2="0.97281875" layer="21"/>
<rectangle x1="2.55778125" y1="0.967740625" x2="3.248659375" y2="0.97281875" layer="21"/>
<rectangle x1="3.4671" y1="0.967740625" x2="3.685540625" y2="0.97281875" layer="21"/>
<rectangle x1="3.807459375" y1="0.967740625" x2="4.04621875" y2="0.97281875" layer="21"/>
<rectangle x1="4.315459375" y1="0.967740625" x2="4.544059375" y2="0.97281875" layer="21"/>
<rectangle x1="4.777740625" y1="0.967740625" x2="5.24001875" y2="0.97281875" layer="21"/>
<rectangle x1="5.285740625" y1="0.967740625" x2="5.37718125" y2="0.97281875" layer="21"/>
<rectangle x1="5.58038125" y1="0.967740625" x2="5.67181875" y2="0.97281875" layer="21"/>
<rectangle x1="6.073140625" y1="0.967740625" x2="6.16458125" y2="0.97281875" layer="21"/>
<rectangle x1="6.41858125" y1="0.967740625" x2="6.51001875" y2="0.97281875" layer="21"/>
<rectangle x1="6.74878125" y1="0.967740625" x2="6.84021875" y2="0.97281875" layer="21"/>
<rectangle x1="6.885940625" y1="0.967740625" x2="7.2771" y2="0.97281875" layer="21"/>
<rectangle x1="7.647940625" y1="0.967740625" x2="8.12038125" y2="0.97281875" layer="21"/>
<rectangle x1="0.048259375" y1="0.97281875" x2="0.434340625" y2="0.9779" layer="21"/>
<rectangle x1="0.891540625" y1="0.97281875" x2="1.115059375" y2="0.9779" layer="21"/>
<rectangle x1="1.267459375" y1="0.97281875" x2="1.902459375" y2="0.9779" layer="21"/>
<rectangle x1="1.9177" y1="0.97281875" x2="2.3495" y2="0.9779" layer="21"/>
<rectangle x1="2.5527" y1="0.97281875" x2="3.24358125" y2="0.9779" layer="21"/>
<rectangle x1="3.46201875" y1="0.97281875" x2="3.69061875" y2="0.9779" layer="21"/>
<rectangle x1="3.807459375" y1="0.97281875" x2="4.04621875" y2="0.9779" layer="21"/>
<rectangle x1="4.315459375" y1="0.97281875" x2="4.544059375" y2="0.9779" layer="21"/>
<rectangle x1="4.78281875" y1="0.97281875" x2="5.234940625" y2="0.9779" layer="21"/>
<rectangle x1="5.285740625" y1="0.97281875" x2="5.37718125" y2="0.9779" layer="21"/>
<rectangle x1="5.58038125" y1="0.97281875" x2="5.6769" y2="0.9779" layer="21"/>
<rectangle x1="6.073140625" y1="0.97281875" x2="6.16458125" y2="0.9779" layer="21"/>
<rectangle x1="6.4135" y1="0.97281875" x2="6.504940625" y2="0.9779" layer="21"/>
<rectangle x1="6.74878125" y1="0.97281875" x2="6.84021875" y2="0.9779" layer="21"/>
<rectangle x1="6.885940625" y1="0.97281875" x2="7.27201875" y2="0.9779" layer="21"/>
<rectangle x1="7.65301875" y1="0.97281875" x2="8.1153" y2="0.9779" layer="21"/>
<rectangle x1="0.048259375" y1="0.9779" x2="0.4191" y2="0.98298125" layer="21"/>
<rectangle x1="0.891540625" y1="0.9779" x2="1.115059375" y2="0.98298125" layer="21"/>
<rectangle x1="1.26238125" y1="0.9779" x2="1.48081875" y2="0.98298125" layer="21"/>
<rectangle x1="1.4859" y1="0.9779" x2="1.902459375" y2="0.98298125" layer="21"/>
<rectangle x1="1.92278125" y1="0.9779" x2="2.34441875" y2="0.98298125" layer="21"/>
<rectangle x1="2.5527" y1="0.9779" x2="2.76098125" y2="0.98298125" layer="21"/>
<rectangle x1="2.766059375" y1="0.9779" x2="3.2385" y2="0.98298125" layer="21"/>
<rectangle x1="3.4671" y1="0.9779" x2="3.69061875" y2="0.98298125" layer="21"/>
<rectangle x1="3.807459375" y1="0.9779" x2="4.04621875" y2="0.98298125" layer="21"/>
<rectangle x1="4.320540625" y1="0.9779" x2="4.544059375" y2="0.98298125" layer="21"/>
<rectangle x1="4.79298125" y1="0.9779" x2="5.229859375" y2="0.98298125" layer="21"/>
<rectangle x1="5.285740625" y1="0.9779" x2="5.37718125" y2="0.98298125" layer="21"/>
<rectangle x1="5.58038125" y1="0.9779" x2="5.67181875" y2="0.98298125" layer="21"/>
<rectangle x1="6.073140625" y1="0.9779" x2="6.16458125" y2="0.98298125" layer="21"/>
<rectangle x1="6.4135" y1="0.9779" x2="6.51001875" y2="0.98298125" layer="21"/>
<rectangle x1="6.7437" y1="0.9779" x2="6.84021875" y2="0.98298125" layer="21"/>
<rectangle x1="6.89101875" y1="0.9779" x2="7.266940625" y2="0.98298125" layer="21"/>
<rectangle x1="7.66318125" y1="0.9779" x2="8.105140625" y2="0.98298125" layer="21"/>
<rectangle x1="0.04318125" y1="0.98298125" x2="0.403859375" y2="0.988059375" layer="21"/>
<rectangle x1="0.886459375" y1="0.98298125" x2="1.10998125" y2="0.988059375" layer="21"/>
<rectangle x1="1.267459375" y1="0.98298125" x2="1.48081875" y2="0.988059375" layer="21"/>
<rectangle x1="1.49098125" y1="0.98298125" x2="1.89738125" y2="0.988059375" layer="21"/>
<rectangle x1="1.92278125" y1="0.98298125" x2="2.34441875" y2="0.988059375" layer="21"/>
<rectangle x1="2.55778125" y1="0.98298125" x2="2.766059375" y2="0.988059375" layer="21"/>
<rectangle x1="2.771140625" y1="0.98298125" x2="3.23341875" y2="0.988059375" layer="21"/>
<rectangle x1="3.46201875" y1="0.98298125" x2="3.685540625" y2="0.988059375" layer="21"/>
<rectangle x1="3.80238125" y1="0.98298125" x2="4.041140625" y2="0.988059375" layer="21"/>
<rectangle x1="4.320540625" y1="0.98298125" x2="4.549140625" y2="0.988059375" layer="21"/>
<rectangle x1="4.798059375" y1="0.98298125" x2="5.2197" y2="0.988059375" layer="21"/>
<rectangle x1="5.285740625" y1="0.98298125" x2="5.37718125" y2="0.988059375" layer="21"/>
<rectangle x1="5.58038125" y1="0.98298125" x2="5.67181875" y2="0.988059375" layer="21"/>
<rectangle x1="6.073140625" y1="0.98298125" x2="6.16458125" y2="0.988059375" layer="21"/>
<rectangle x1="6.41858125" y1="0.98298125" x2="6.51001875" y2="0.988059375" layer="21"/>
<rectangle x1="6.74878125" y1="0.98298125" x2="6.84021875" y2="0.988059375" layer="21"/>
<rectangle x1="6.90118125" y1="0.98298125" x2="7.266940625" y2="0.988059375" layer="21"/>
<rectangle x1="7.668259375" y1="0.98298125" x2="8.100059375" y2="0.988059375" layer="21"/>
<rectangle x1="0.04318125" y1="0.988059375" x2="0.38861875" y2="0.993140625" layer="21"/>
<rectangle x1="0.891540625" y1="0.988059375" x2="1.115059375" y2="0.993140625" layer="21"/>
<rectangle x1="1.267459375" y1="0.988059375" x2="1.4859" y2="0.993140625" layer="21"/>
<rectangle x1="1.49098125" y1="0.988059375" x2="1.8923" y2="0.993140625" layer="21"/>
<rectangle x1="1.927859375" y1="0.988059375" x2="2.339340625" y2="0.993140625" layer="21"/>
<rectangle x1="2.55778125" y1="0.988059375" x2="2.766059375" y2="0.993140625" layer="21"/>
<rectangle x1="2.77621875" y1="0.988059375" x2="3.228340625" y2="0.993140625" layer="21"/>
<rectangle x1="3.46201875" y1="0.988059375" x2="3.69061875" y2="0.993140625" layer="21"/>
<rectangle x1="3.80238125" y1="0.988059375" x2="4.041140625" y2="0.993140625" layer="21"/>
<rectangle x1="4.320540625" y1="0.988059375" x2="4.549140625" y2="0.993140625" layer="21"/>
<rectangle x1="4.803140625" y1="0.988059375" x2="5.21461875" y2="0.993140625" layer="21"/>
<rectangle x1="5.285740625" y1="0.988059375" x2="5.37718125" y2="0.993140625" layer="21"/>
<rectangle x1="5.58038125" y1="0.988059375" x2="5.6769" y2="0.993140625" layer="21"/>
<rectangle x1="6.073140625" y1="0.988059375" x2="6.16458125" y2="0.993140625" layer="21"/>
<rectangle x1="6.4135" y1="0.988059375" x2="6.51001875" y2="0.993140625" layer="21"/>
<rectangle x1="6.7437" y1="0.988059375" x2="6.84021875" y2="0.993140625" layer="21"/>
<rectangle x1="6.906259375" y1="0.988059375" x2="7.25678125" y2="0.993140625" layer="21"/>
<rectangle x1="7.673340625" y1="0.988059375" x2="8.09498125" y2="0.993140625" layer="21"/>
<rectangle x1="0.04318125" y1="0.993140625" x2="0.3683" y2="0.99821875" layer="21"/>
<rectangle x1="0.886459375" y1="0.993140625" x2="1.115059375" y2="0.99821875" layer="21"/>
<rectangle x1="1.267459375" y1="0.993140625" x2="1.48081875" y2="0.99821875" layer="21"/>
<rectangle x1="1.496059375" y1="0.993140625" x2="1.8923" y2="0.99821875" layer="21"/>
<rectangle x1="1.932940625" y1="0.993140625" x2="2.334259375" y2="0.99821875" layer="21"/>
<rectangle x1="2.55778125" y1="0.993140625" x2="2.76098125" y2="0.99821875" layer="21"/>
<rectangle x1="2.7813" y1="0.993140625" x2="3.223259375" y2="0.99821875" layer="21"/>
<rectangle x1="3.4671" y1="0.993140625" x2="3.69061875" y2="0.99821875" layer="21"/>
<rectangle x1="3.7973" y1="0.993140625" x2="4.041140625" y2="0.99821875" layer="21"/>
<rectangle x1="4.32561875" y1="0.993140625" x2="4.55421875" y2="0.99821875" layer="21"/>
<rectangle x1="4.8133" y1="0.993140625" x2="5.209540625" y2="0.99821875" layer="21"/>
<rectangle x1="5.285740625" y1="0.993140625" x2="5.37718125" y2="0.99821875" layer="21"/>
<rectangle x1="5.58038125" y1="0.993140625" x2="5.67181875" y2="0.99821875" layer="21"/>
<rectangle x1="6.073140625" y1="0.993140625" x2="6.16458125" y2="0.99821875" layer="21"/>
<rectangle x1="6.41858125" y1="0.993140625" x2="6.504940625" y2="0.99821875" layer="21"/>
<rectangle x1="6.74878125" y1="0.993140625" x2="6.835140625" y2="0.99821875" layer="21"/>
<rectangle x1="6.911340625" y1="0.993140625" x2="7.2517" y2="0.99821875" layer="21"/>
<rectangle x1="7.6835" y1="0.993140625" x2="8.08481875" y2="0.99821875" layer="21"/>
<rectangle x1="0.0381" y1="0.99821875" x2="0.358140625" y2="1.0033" layer="21"/>
<rectangle x1="0.891540625" y1="0.99821875" x2="1.10998125" y2="1.0033" layer="21"/>
<rectangle x1="1.267459375" y1="0.99821875" x2="1.48081875" y2="1.0033" layer="21"/>
<rectangle x1="1.501140625" y1="0.99821875" x2="1.88721875" y2="1.0033" layer="21"/>
<rectangle x1="1.93801875" y1="0.99821875" x2="2.32918125" y2="1.0033" layer="21"/>
<rectangle x1="2.5527" y1="0.99821875" x2="2.766059375" y2="1.0033" layer="21"/>
<rectangle x1="2.7813" y1="0.99821875" x2="3.21818125" y2="1.0033" layer="21"/>
<rectangle x1="3.4671" y1="0.99821875" x2="3.685540625" y2="1.0033" layer="21"/>
<rectangle x1="3.7973" y1="0.99821875" x2="4.036059375" y2="1.0033" layer="21"/>
<rectangle x1="4.32561875" y1="0.99821875" x2="4.55421875" y2="1.0033" layer="21"/>
<rectangle x1="4.823459375" y1="0.99821875" x2="5.19938125" y2="1.0033" layer="21"/>
<rectangle x1="5.285740625" y1="0.99821875" x2="5.37718125" y2="1.0033" layer="21"/>
<rectangle x1="5.58038125" y1="0.99821875" x2="5.67181875" y2="1.0033" layer="21"/>
<rectangle x1="6.073140625" y1="0.99821875" x2="6.16458125" y2="1.0033" layer="21"/>
<rectangle x1="6.4135" y1="0.99821875" x2="6.504940625" y2="1.0033" layer="21"/>
<rectangle x1="6.7437" y1="0.99821875" x2="6.84021875" y2="1.0033" layer="21"/>
<rectangle x1="6.91641875" y1="0.99821875" x2="7.24661875" y2="1.0033" layer="21"/>
<rectangle x1="7.693659375" y1="0.99821875" x2="8.074659375" y2="1.0033" layer="21"/>
<rectangle x1="0.0381" y1="1.0033" x2="0.34798125" y2="1.00838125" layer="21"/>
<rectangle x1="0.886459375" y1="1.0033" x2="1.115059375" y2="1.00838125" layer="21"/>
<rectangle x1="1.26238125" y1="1.0033" x2="1.4859" y2="1.00838125" layer="21"/>
<rectangle x1="1.50621875" y1="1.0033" x2="1.882140625" y2="1.00838125" layer="21"/>
<rectangle x1="1.93801875" y1="1.0033" x2="2.32918125" y2="1.00838125" layer="21"/>
<rectangle x1="2.55778125" y1="1.0033" x2="2.766059375" y2="1.00838125" layer="21"/>
<rectangle x1="2.791459375" y1="1.0033" x2="3.2131" y2="1.00838125" layer="21"/>
<rectangle x1="3.46201875" y1="1.0033" x2="3.69061875" y2="1.00838125" layer="21"/>
<rectangle x1="3.79221875" y1="1.0033" x2="4.036059375" y2="1.00838125" layer="21"/>
<rectangle x1="4.32561875" y1="1.0033" x2="4.55421875" y2="1.00838125" layer="21"/>
<rectangle x1="4.828540625" y1="1.0033" x2="5.18921875" y2="1.00838125" layer="21"/>
<rectangle x1="5.285740625" y1="1.0033" x2="5.37718125" y2="1.00838125" layer="21"/>
<rectangle x1="5.58038125" y1="1.0033" x2="5.67181875" y2="1.00838125" layer="21"/>
<rectangle x1="6.073140625" y1="1.0033" x2="6.16458125" y2="1.00838125" layer="21"/>
<rectangle x1="6.41858125" y1="1.0033" x2="6.51001875" y2="1.00838125" layer="21"/>
<rectangle x1="6.7437" y1="1.0033" x2="6.835140625" y2="1.00838125" layer="21"/>
<rectangle x1="6.9215" y1="1.0033" x2="7.241540625" y2="1.00838125" layer="21"/>
<rectangle x1="7.70381875" y1="1.0033" x2="8.06958125" y2="1.00838125" layer="21"/>
<rectangle x1="0.0381" y1="1.00838125" x2="0.33781875" y2="1.013459375" layer="21"/>
<rectangle x1="0.891540625" y1="1.00838125" x2="1.10998125" y2="1.013459375" layer="21"/>
<rectangle x1="1.267459375" y1="1.00838125" x2="1.48081875" y2="1.013459375" layer="21"/>
<rectangle x1="1.50621875" y1="1.00838125" x2="1.877059375" y2="1.013459375" layer="21"/>
<rectangle x1="1.9431" y1="1.00838125" x2="2.3241" y2="1.013459375" layer="21"/>
<rectangle x1="2.55778125" y1="1.00838125" x2="2.76098125" y2="1.013459375" layer="21"/>
<rectangle x1="2.791459375" y1="1.00838125" x2="3.20801875" y2="1.013459375" layer="21"/>
<rectangle x1="3.4671" y1="1.00838125" x2="3.69061875" y2="1.013459375" layer="21"/>
<rectangle x1="3.79221875" y1="1.00838125" x2="4.03098125" y2="1.013459375" layer="21"/>
<rectangle x1="4.3307" y1="1.00838125" x2="4.5593" y2="1.013459375" layer="21"/>
<rectangle x1="4.8387" y1="1.00838125" x2="5.184140625" y2="1.013459375" layer="21"/>
<rectangle x1="5.285740625" y1="1.00838125" x2="5.37718125" y2="1.013459375" layer="21"/>
<rectangle x1="5.58038125" y1="1.00838125" x2="5.67181875" y2="1.013459375" layer="21"/>
<rectangle x1="6.073140625" y1="1.00838125" x2="6.16458125" y2="1.013459375" layer="21"/>
<rectangle x1="6.4135" y1="1.00838125" x2="6.51001875" y2="1.013459375" layer="21"/>
<rectangle x1="6.7437" y1="1.00838125" x2="6.84021875" y2="1.013459375" layer="21"/>
<rectangle x1="6.931659375" y1="1.00838125" x2="7.23138125" y2="1.013459375" layer="21"/>
<rectangle x1="7.7089" y1="1.00838125" x2="8.05941875" y2="1.013459375" layer="21"/>
<rectangle x1="0.0381" y1="1.013459375" x2="0.332740625" y2="1.018540625" layer="21"/>
<rectangle x1="0.886459375" y1="1.013459375" x2="1.115059375" y2="1.018540625" layer="21"/>
<rectangle x1="1.267459375" y1="1.013459375" x2="1.4859" y2="1.018540625" layer="21"/>
<rectangle x1="1.51638125" y1="1.013459375" x2="1.877059375" y2="1.018540625" layer="21"/>
<rectangle x1="1.94818125" y1="1.013459375" x2="2.31901875" y2="1.018540625" layer="21"/>
<rectangle x1="2.55778125" y1="1.013459375" x2="2.766059375" y2="1.018540625" layer="21"/>
<rectangle x1="2.80161875" y1="1.013459375" x2="3.202940625" y2="1.018540625" layer="21"/>
<rectangle x1="3.46201875" y1="1.013459375" x2="3.69061875" y2="1.018540625" layer="21"/>
<rectangle x1="3.79221875" y1="1.013459375" x2="4.03098125" y2="1.018540625" layer="21"/>
<rectangle x1="4.3307" y1="1.013459375" x2="4.5593" y2="1.018540625" layer="21"/>
<rectangle x1="4.848859375" y1="1.013459375" x2="5.17398125" y2="1.018540625" layer="21"/>
<rectangle x1="5.285740625" y1="1.013459375" x2="5.37718125" y2="1.018540625" layer="21"/>
<rectangle x1="5.58038125" y1="1.013459375" x2="5.6769" y2="1.018540625" layer="21"/>
<rectangle x1="6.073140625" y1="1.013459375" x2="6.16458125" y2="1.018540625" layer="21"/>
<rectangle x1="6.4135" y1="1.013459375" x2="6.504940625" y2="1.018540625" layer="21"/>
<rectangle x1="6.74878125" y1="1.013459375" x2="6.84021875" y2="1.018540625" layer="21"/>
<rectangle x1="6.94181875" y1="1.013459375" x2="7.2263" y2="1.018540625" layer="21"/>
<rectangle x1="7.719059375" y1="1.013459375" x2="8.049259375" y2="1.018540625" layer="21"/>
<rectangle x1="0.0381" y1="1.018540625" x2="0.32258125" y2="1.02361875" layer="21"/>
<rectangle x1="0.891540625" y1="1.018540625" x2="1.115059375" y2="1.02361875" layer="21"/>
<rectangle x1="1.26238125" y1="1.018540625" x2="1.4859" y2="1.02361875" layer="21"/>
<rectangle x1="1.51638125" y1="1.018540625" x2="1.87198125" y2="1.02361875" layer="21"/>
<rectangle x1="1.953259375" y1="1.018540625" x2="2.313940625" y2="1.02361875" layer="21"/>
<rectangle x1="2.55778125" y1="1.018540625" x2="2.766059375" y2="1.02361875" layer="21"/>
<rectangle x1="2.8067" y1="1.018540625" x2="3.19278125" y2="1.02361875" layer="21"/>
<rectangle x1="3.4671" y1="1.018540625" x2="3.69061875" y2="1.02361875" layer="21"/>
<rectangle x1="3.787140625" y1="1.018540625" x2="4.03098125" y2="1.02361875" layer="21"/>
<rectangle x1="4.3307" y1="1.018540625" x2="4.5593" y2="1.02361875" layer="21"/>
<rectangle x1="4.8641" y1="1.018540625" x2="5.158740625" y2="1.02361875" layer="21"/>
<rectangle x1="5.285740625" y1="1.018540625" x2="5.37718125" y2="1.02361875" layer="21"/>
<rectangle x1="5.58038125" y1="1.018540625" x2="5.67181875" y2="1.02361875" layer="21"/>
<rectangle x1="6.073140625" y1="1.018540625" x2="6.16458125" y2="1.02361875" layer="21"/>
<rectangle x1="6.4135" y1="1.018540625" x2="6.51001875" y2="1.02361875" layer="21"/>
<rectangle x1="6.7437" y1="1.018540625" x2="6.84021875" y2="1.02361875" layer="21"/>
<rectangle x1="6.9469" y1="1.018540625" x2="7.216140625" y2="1.02361875" layer="21"/>
<rectangle x1="7.7343" y1="1.018540625" x2="8.0391" y2="1.02361875" layer="21"/>
<rectangle x1="0.03301875" y1="1.02361875" x2="0.31241875" y2="1.0287" layer="21"/>
<rectangle x1="0.886459375" y1="1.02361875" x2="1.10998125" y2="1.0287" layer="21"/>
<rectangle x1="1.267459375" y1="1.02361875" x2="1.48081875" y2="1.0287" layer="21"/>
<rectangle x1="1.526540625" y1="1.02361875" x2="1.86181875" y2="1.0287" layer="21"/>
<rectangle x1="1.958340625" y1="1.02361875" x2="2.308859375" y2="1.0287" layer="21"/>
<rectangle x1="2.55778125" y1="1.02361875" x2="2.76098125" y2="1.0287" layer="21"/>
<rectangle x1="2.81178125" y1="1.02361875" x2="3.1877" y2="1.0287" layer="21"/>
<rectangle x1="3.46201875" y1="1.02361875" x2="3.685540625" y2="1.0287" layer="21"/>
<rectangle x1="3.787140625" y1="1.02361875" x2="4.0259" y2="1.0287" layer="21"/>
<rectangle x1="4.33578125" y1="1.02361875" x2="4.56438125" y2="1.0287" layer="21"/>
<rectangle x1="4.874259375" y1="1.02361875" x2="5.14858125" y2="1.0287" layer="21"/>
<rectangle x1="5.285740625" y1="1.02361875" x2="5.37718125" y2="1.0287" layer="21"/>
<rectangle x1="5.58038125" y1="1.02361875" x2="5.6769" y2="1.0287" layer="21"/>
<rectangle x1="6.073140625" y1="1.02361875" x2="6.16458125" y2="1.0287" layer="21"/>
<rectangle x1="6.41858125" y1="1.02361875" x2="6.504940625" y2="1.0287" layer="21"/>
<rectangle x1="6.7437" y1="1.02361875" x2="6.835140625" y2="1.0287" layer="21"/>
<rectangle x1="6.962140625" y1="1.02361875" x2="7.2009" y2="1.0287" layer="21"/>
<rectangle x1="7.744459375" y1="1.02361875" x2="8.023859375" y2="1.0287" layer="21"/>
<rectangle x1="0.03301875" y1="1.0287" x2="0.307340625" y2="1.03378125" layer="21"/>
<rectangle x1="0.891540625" y1="1.0287" x2="1.115059375" y2="1.03378125" layer="21"/>
<rectangle x1="1.267459375" y1="1.0287" x2="1.4859" y2="1.03378125" layer="21"/>
<rectangle x1="1.53161875" y1="1.0287" x2="1.86181875" y2="1.03378125" layer="21"/>
<rectangle x1="1.96341875" y1="1.0287" x2="2.30378125" y2="1.03378125" layer="21"/>
<rectangle x1="2.55778125" y1="1.0287" x2="2.766059375" y2="1.03378125" layer="21"/>
<rectangle x1="2.821940625" y1="1.0287" x2="3.177540625" y2="1.03378125" layer="21"/>
<rectangle x1="3.4671" y1="1.0287" x2="3.69061875" y2="1.03378125" layer="21"/>
<rectangle x1="3.782059375" y1="1.0287" x2="4.0259" y2="1.03378125" layer="21"/>
<rectangle x1="4.33578125" y1="1.0287" x2="4.56438125" y2="1.03378125" layer="21"/>
<rectangle x1="4.8895" y1="1.0287" x2="5.133340625" y2="1.03378125" layer="21"/>
<rectangle x1="5.285740625" y1="1.0287" x2="5.37718125" y2="1.03378125" layer="21"/>
<rectangle x1="5.585459375" y1="1.0287" x2="5.67181875" y2="1.03378125" layer="21"/>
<rectangle x1="6.073140625" y1="1.0287" x2="6.0833" y2="1.03378125" layer="21"/>
<rectangle x1="6.08838125" y1="1.0287" x2="6.16458125" y2="1.03378125" layer="21"/>
<rectangle x1="6.41858125" y1="1.0287" x2="6.504940625" y2="1.03378125" layer="21"/>
<rectangle x1="6.74878125" y1="1.0287" x2="6.835140625" y2="1.03378125" layer="21"/>
<rectangle x1="6.9723" y1="1.0287" x2="7.19581875" y2="1.03378125" layer="21"/>
<rectangle x1="7.7597" y1="1.0287" x2="8.00861875" y2="1.03378125" layer="21"/>
<rectangle x1="0.03301875" y1="1.03378125" x2="0.29718125" y2="1.038859375" layer="21"/>
<rectangle x1="0.886459375" y1="1.03378125" x2="1.10998125" y2="1.038859375" layer="21"/>
<rectangle x1="1.267459375" y1="1.03378125" x2="1.48081875" y2="1.038859375" layer="21"/>
<rectangle x1="1.5367" y1="1.03378125" x2="1.851659375" y2="1.038859375" layer="21"/>
<rectangle x1="1.9685" y1="1.03378125" x2="2.2987" y2="1.038859375" layer="21"/>
<rectangle x1="2.5527" y1="1.03378125" x2="2.766059375" y2="1.038859375" layer="21"/>
<rectangle x1="2.82701875" y1="1.03378125" x2="3.172459375" y2="1.038859375" layer="21"/>
<rectangle x1="3.46201875" y1="1.03378125" x2="3.69061875" y2="1.038859375" layer="21"/>
<rectangle x1="3.782059375" y1="1.03378125" x2="4.02081875" y2="1.038859375" layer="21"/>
<rectangle x1="4.340859375" y1="1.03378125" x2="4.569459375" y2="1.038859375" layer="21"/>
<rectangle x1="4.904740625" y1="1.03378125" x2="5.1181" y2="1.038859375" layer="21"/>
<rectangle x1="5.285740625" y1="1.03378125" x2="5.37718125" y2="1.038859375" layer="21"/>
<rectangle x1="6.982459375" y1="1.03378125" x2="7.18058125" y2="1.038859375" layer="21"/>
<rectangle x1="7.774940625" y1="1.03378125" x2="7.99338125" y2="1.038859375" layer="21"/>
<rectangle x1="0.03301875" y1="1.038859375" x2="0.2921" y2="1.043940625" layer="21"/>
<rectangle x1="0.891540625" y1="1.038859375" x2="1.115059375" y2="1.043940625" layer="21"/>
<rectangle x1="1.26238125" y1="1.038859375" x2="1.48081875" y2="1.043940625" layer="21"/>
<rectangle x1="1.54178125" y1="1.038859375" x2="1.84658125" y2="1.043940625" layer="21"/>
<rectangle x1="1.978659375" y1="1.038859375" x2="2.288540625" y2="1.043940625" layer="21"/>
<rectangle x1="2.55778125" y1="1.038859375" x2="2.76098125" y2="1.043940625" layer="21"/>
<rectangle x1="2.8321" y1="1.038859375" x2="3.1623" y2="1.043940625" layer="21"/>
<rectangle x1="3.4671" y1="1.038859375" x2="3.685540625" y2="1.043940625" layer="21"/>
<rectangle x1="3.77698125" y1="1.038859375" x2="4.02081875" y2="1.043940625" layer="21"/>
<rectangle x1="4.340859375" y1="1.038859375" x2="4.569459375" y2="1.043940625" layer="21"/>
<rectangle x1="4.925059375" y1="1.038859375" x2="5.09778125" y2="1.043940625" layer="21"/>
<rectangle x1="5.285740625" y1="1.038859375" x2="5.37718125" y2="1.043940625" layer="21"/>
<rectangle x1="7.00278125" y1="1.038859375" x2="7.165340625" y2="1.043940625" layer="21"/>
<rectangle x1="7.800340625" y1="1.038859375" x2="7.973059375" y2="1.043940625" layer="21"/>
<rectangle x1="0.03301875" y1="1.043940625" x2="0.2921" y2="1.04901875" layer="21"/>
<rectangle x1="0.886459375" y1="1.043940625" x2="1.115059375" y2="1.04901875" layer="21"/>
<rectangle x1="1.267459375" y1="1.043940625" x2="1.4859" y2="1.04901875" layer="21"/>
<rectangle x1="1.551940625" y1="1.043940625" x2="1.83641875" y2="1.04901875" layer="21"/>
<rectangle x1="1.983740625" y1="1.043940625" x2="2.283459375" y2="1.04901875" layer="21"/>
<rectangle x1="2.55778125" y1="1.043940625" x2="2.766059375" y2="1.04901875" layer="21"/>
<rectangle x1="2.842259375" y1="1.043940625" x2="3.152140625" y2="1.04901875" layer="21"/>
<rectangle x1="3.46201875" y1="1.043940625" x2="3.69061875" y2="1.04901875" layer="21"/>
<rectangle x1="3.77698125" y1="1.043940625" x2="4.02081875" y2="1.04901875" layer="21"/>
<rectangle x1="4.340859375" y1="1.043940625" x2="4.569459375" y2="1.04901875" layer="21"/>
<rectangle x1="4.950459375" y1="1.043940625" x2="5.07238125" y2="1.04901875" layer="21"/>
<rectangle x1="5.285740625" y1="1.043940625" x2="5.37718125" y2="1.04901875" layer="21"/>
<rectangle x1="7.0231" y1="1.043940625" x2="7.12978125" y2="1.04901875" layer="21"/>
<rectangle x1="7.134859375" y1="1.043940625" x2="7.139940625" y2="1.04901875" layer="21"/>
<rectangle x1="7.820659375" y1="1.043940625" x2="7.825740625" y2="1.04901875" layer="21"/>
<rectangle x1="7.83081875" y1="1.043940625" x2="7.947659375" y2="1.04901875" layer="21"/>
<rectangle x1="0.03301875" y1="1.04901875" x2="0.28701875" y2="1.0541" layer="21"/>
<rectangle x1="0.891540625" y1="1.04901875" x2="1.10998125" y2="1.0541" layer="21"/>
<rectangle x1="1.267459375" y1="1.04901875" x2="1.48081875" y2="1.0541" layer="21"/>
<rectangle x1="1.5621" y1="1.04901875" x2="1.831340625" y2="1.0541" layer="21"/>
<rectangle x1="1.9939" y1="1.04901875" x2="2.27838125" y2="1.0541" layer="21"/>
<rectangle x1="2.55778125" y1="1.04901875" x2="2.766059375" y2="1.0541" layer="21"/>
<rectangle x1="2.85241875" y1="1.04901875" x2="3.14198125" y2="1.0541" layer="21"/>
<rectangle x1="3.4671" y1="1.04901875" x2="3.69061875" y2="1.0541" layer="21"/>
<rectangle x1="3.7719" y1="1.04901875" x2="4.015740625" y2="1.0541" layer="21"/>
<rectangle x1="4.340859375" y1="1.04901875" x2="4.574540625" y2="1.0541" layer="21"/>
<rectangle x1="5.01141875" y1="1.04901875" x2="5.0165" y2="1.0541" layer="21"/>
<rectangle x1="5.285740625" y1="1.04901875" x2="5.37718125" y2="1.0541" layer="21"/>
<rectangle x1="7.084059375" y1="1.04901875" x2="7.089140625" y2="1.0541" layer="21"/>
<rectangle x1="7.8613" y1="1.04901875" x2="7.86638125" y2="1.0541" layer="21"/>
<rectangle x1="7.896859375" y1="1.04901875" x2="7.901940625" y2="1.0541" layer="21"/>
<rectangle x1="0.03301875" y1="1.0541" x2="0.28701875" y2="1.05918125" layer="21"/>
<rectangle x1="0.886459375" y1="1.0541" x2="1.115059375" y2="1.05918125" layer="21"/>
<rectangle x1="1.267459375" y1="1.0541" x2="1.48081875" y2="1.05918125" layer="21"/>
<rectangle x1="1.56718125" y1="1.0541" x2="1.82118125" y2="1.05918125" layer="21"/>
<rectangle x1="1.99898125" y1="1.0541" x2="2.26821875" y2="1.05918125" layer="21"/>
<rectangle x1="2.5527" y1="1.0541" x2="2.766059375" y2="1.05918125" layer="21"/>
<rectangle x1="2.86258125" y1="1.0541" x2="3.13181875" y2="1.05918125" layer="21"/>
<rectangle x1="3.46201875" y1="1.0541" x2="3.69061875" y2="1.05918125" layer="21"/>
<rectangle x1="3.7719" y1="1.0541" x2="4.015740625" y2="1.05918125" layer="21"/>
<rectangle x1="4.345940625" y1="1.0541" x2="4.574540625" y2="1.05918125" layer="21"/>
<rectangle x1="5.285740625" y1="1.0541" x2="5.37718125" y2="1.05918125" layer="21"/>
<rectangle x1="0.03301875" y1="1.05918125" x2="0.281940625" y2="1.064259375" layer="21"/>
<rectangle x1="0.891540625" y1="1.05918125" x2="1.115059375" y2="1.064259375" layer="21"/>
<rectangle x1="1.267459375" y1="1.05918125" x2="1.48081875" y2="1.064259375" layer="21"/>
<rectangle x1="1.577340625" y1="1.05918125" x2="1.81101875" y2="1.064259375" layer="21"/>
<rectangle x1="2.009140625" y1="1.05918125" x2="2.258059375" y2="1.064259375" layer="21"/>
<rectangle x1="2.55778125" y1="1.05918125" x2="2.766059375" y2="1.064259375" layer="21"/>
<rectangle x1="2.872740625" y1="1.05918125" x2="3.121659375" y2="1.064259375" layer="21"/>
<rectangle x1="3.46201875" y1="1.05918125" x2="3.69061875" y2="1.064259375" layer="21"/>
<rectangle x1="3.7719" y1="1.05918125" x2="4.015740625" y2="1.064259375" layer="21"/>
<rectangle x1="4.345940625" y1="1.05918125" x2="4.57961875" y2="1.064259375" layer="21"/>
<rectangle x1="5.285740625" y1="1.05918125" x2="5.37718125" y2="1.064259375" layer="21"/>
<rectangle x1="0.03301875" y1="1.064259375" x2="0.276859375" y2="1.069340625" layer="21"/>
<rectangle x1="0.891540625" y1="1.064259375" x2="0.911859375" y2="1.069340625" layer="21"/>
<rectangle x1="0.916940625" y1="1.064259375" x2="0.942340625" y2="1.069340625" layer="21"/>
<rectangle x1="0.94741875" y1="1.064259375" x2="0.97281875" y2="1.069340625" layer="21"/>
<rectangle x1="0.9779" y1="1.064259375" x2="1.0033" y2="1.069340625" layer="21"/>
<rectangle x1="1.00838125" y1="1.064259375" x2="1.03378125" y2="1.069340625" layer="21"/>
<rectangle x1="1.038859375" y1="1.064259375" x2="1.064259375" y2="1.069340625" layer="21"/>
<rectangle x1="1.069340625" y1="1.064259375" x2="1.089659375" y2="1.069340625" layer="21"/>
<rectangle x1="1.094740625" y1="1.064259375" x2="1.10998125" y2="1.069340625" layer="21"/>
<rectangle x1="1.267459375" y1="1.064259375" x2="1.27761875" y2="1.069340625" layer="21"/>
<rectangle x1="1.2827" y1="1.064259375" x2="1.3081" y2="1.069340625" layer="21"/>
<rectangle x1="1.31318125" y1="1.064259375" x2="1.33858125" y2="1.069340625" layer="21"/>
<rectangle x1="1.343659375" y1="1.064259375" x2="1.36398125" y2="1.069340625" layer="21"/>
<rectangle x1="1.369059375" y1="1.064259375" x2="1.394459375" y2="1.069340625" layer="21"/>
<rectangle x1="1.399540625" y1="1.064259375" x2="1.41478125" y2="1.069340625" layer="21"/>
<rectangle x1="1.419859375" y1="1.064259375" x2="1.45541875" y2="1.069340625" layer="21"/>
<rectangle x1="1.4605" y1="1.064259375" x2="1.48081875" y2="1.069340625" layer="21"/>
<rectangle x1="1.59258125" y1="1.064259375" x2="1.79578125" y2="1.069340625" layer="21"/>
<rectangle x1="2.02438125" y1="1.064259375" x2="2.24281875" y2="1.069340625" layer="21"/>
<rectangle x1="2.55778125" y1="1.064259375" x2="2.593340625" y2="1.069340625" layer="21"/>
<rectangle x1="2.59841875" y1="1.064259375" x2="2.618740625" y2="1.069340625" layer="21"/>
<rectangle x1="2.62381875" y1="1.064259375" x2="2.64921875" y2="1.069340625" layer="21"/>
<rectangle x1="2.6543" y1="1.064259375" x2="2.67461875" y2="1.069340625" layer="21"/>
<rectangle x1="2.6797" y1="1.064259375" x2="2.70001875" y2="1.069340625" layer="21"/>
<rectangle x1="2.7051" y1="1.064259375" x2="2.7305" y2="1.069340625" layer="21"/>
<rectangle x1="2.73558125" y1="1.064259375" x2="2.76098125" y2="1.069340625" layer="21"/>
<rectangle x1="2.8829" y1="1.064259375" x2="3.101340625" y2="1.069340625" layer="21"/>
<rectangle x1="3.4671" y1="1.064259375" x2="3.685540625" y2="1.069340625" layer="21"/>
<rectangle x1="3.7719" y1="1.064259375" x2="3.787140625" y2="1.069340625" layer="21"/>
<rectangle x1="3.79221875" y1="1.064259375" x2="3.812540625" y2="1.069340625" layer="21"/>
<rectangle x1="3.81761875" y1="1.064259375" x2="3.837940625" y2="1.069340625" layer="21"/>
<rectangle x1="3.84301875" y1="1.064259375" x2="3.86841875" y2="1.069340625" layer="21"/>
<rectangle x1="3.8735" y1="1.064259375" x2="3.914140625" y2="1.069340625" layer="21"/>
<rectangle x1="3.91921875" y1="1.064259375" x2="3.934459375" y2="1.069340625" layer="21"/>
<rectangle x1="3.939540625" y1="1.064259375" x2="3.9751" y2="1.069340625" layer="21"/>
<rectangle x1="3.985259375" y1="1.064259375" x2="4.00558125" y2="1.069340625" layer="21"/>
<rectangle x1="4.35101875" y1="1.064259375" x2="4.366259375" y2="1.069340625" layer="21"/>
<rectangle x1="4.371340625" y1="1.064259375" x2="4.396740625" y2="1.069340625" layer="21"/>
<rectangle x1="4.40181875" y1="1.064259375" x2="4.42721875" y2="1.069340625" layer="21"/>
<rectangle x1="4.4323" y1="1.064259375" x2="4.45261875" y2="1.069340625" layer="21"/>
<rectangle x1="4.4577" y1="1.064259375" x2="4.4831" y2="1.069340625" layer="21"/>
<rectangle x1="4.48818125" y1="1.064259375" x2="4.50341875" y2="1.069340625" layer="21"/>
<rectangle x1="4.5085" y1="1.064259375" x2="4.544059375" y2="1.069340625" layer="21"/>
<rectangle x1="4.549140625" y1="1.064259375" x2="4.574540625" y2="1.069340625" layer="21"/>
<rectangle x1="5.285740625" y1="1.064259375" x2="5.37718125" y2="1.069340625" layer="21"/>
<rectangle x1="0.03301875" y1="1.069340625" x2="0.276859375" y2="1.07441875" layer="21"/>
<rectangle x1="1.60781875" y1="1.069340625" x2="1.78561875" y2="1.07441875" layer="21"/>
<rectangle x1="2.034540625" y1="1.069340625" x2="2.232659375" y2="1.07441875" layer="21"/>
<rectangle x1="2.898140625" y1="1.069340625" x2="3.09118125" y2="1.07441875" layer="21"/>
<rectangle x1="3.4671" y1="1.069340625" x2="3.69061875" y2="1.07441875" layer="21"/>
<rectangle x1="5.285740625" y1="1.069340625" x2="5.37718125" y2="1.07441875" layer="21"/>
<rectangle x1="0.03301875" y1="1.07441875" x2="0.276859375" y2="1.0795" layer="21"/>
<rectangle x1="1.61798125" y1="1.07441875" x2="1.7653" y2="1.0795" layer="21"/>
<rectangle x1="2.054859375" y1="1.07441875" x2="2.212340625" y2="1.0795" layer="21"/>
<rectangle x1="2.91338125" y1="1.07441875" x2="3.070859375" y2="1.0795" layer="21"/>
<rectangle x1="3.46201875" y1="1.07441875" x2="3.69061875" y2="1.0795" layer="21"/>
<rectangle x1="5.285740625" y1="1.07441875" x2="5.37718125" y2="1.0795" layer="21"/>
<rectangle x1="0.03301875" y1="1.0795" x2="0.276859375" y2="1.08458125" layer="21"/>
<rectangle x1="1.64338125" y1="1.0795" x2="1.73481875" y2="1.08458125" layer="21"/>
<rectangle x1="1.7399" y1="1.0795" x2="1.74498125" y2="1.08458125" layer="21"/>
<rectangle x1="2.080259375" y1="1.0795" x2="2.186940625" y2="1.08458125" layer="21"/>
<rectangle x1="2.9337" y1="1.0795" x2="2.93878125" y2="1.08458125" layer="21"/>
<rectangle x1="2.943859375" y1="1.0795" x2="3.04038125" y2="1.08458125" layer="21"/>
<rectangle x1="3.4671" y1="1.0795" x2="3.69061875" y2="1.08458125" layer="21"/>
<rectangle x1="5.285740625" y1="1.0795" x2="5.37718125" y2="1.08458125" layer="21"/>
<rectangle x1="0.03301875" y1="1.08458125" x2="0.27178125" y2="1.089659375" layer="21"/>
<rectangle x1="3.46201875" y1="1.08458125" x2="3.685540625" y2="1.089659375" layer="21"/>
<rectangle x1="5.285740625" y1="1.08458125" x2="5.37718125" y2="1.089659375" layer="21"/>
<rectangle x1="0.027940625" y1="1.089659375" x2="0.276859375" y2="1.094740625" layer="21"/>
<rectangle x1="3.46201875" y1="1.089659375" x2="3.69061875" y2="1.094740625" layer="21"/>
<rectangle x1="5.285740625" y1="1.089659375" x2="5.37718125" y2="1.094740625" layer="21"/>
<rectangle x1="0.03301875" y1="1.094740625" x2="0.27178125" y2="1.09981875" layer="21"/>
<rectangle x1="3.4671" y1="1.094740625" x2="3.69061875" y2="1.09981875" layer="21"/>
<rectangle x1="5.285740625" y1="1.094740625" x2="5.37718125" y2="1.09981875" layer="21"/>
<rectangle x1="0.03301875" y1="1.09981875" x2="0.27178125" y2="1.1049" layer="21"/>
<rectangle x1="3.46201875" y1="1.09981875" x2="3.685540625" y2="1.1049" layer="21"/>
<rectangle x1="5.285740625" y1="1.09981875" x2="5.37718125" y2="1.1049" layer="21"/>
<rectangle x1="0.03301875" y1="1.1049" x2="0.276859375" y2="1.10998125" layer="21"/>
<rectangle x1="3.4671" y1="1.1049" x2="3.69061875" y2="1.10998125" layer="21"/>
<rectangle x1="5.285740625" y1="1.1049" x2="5.37718125" y2="1.10998125" layer="21"/>
<rectangle x1="0.03301875" y1="1.10998125" x2="0.27178125" y2="1.115059375" layer="21"/>
<rectangle x1="3.4671" y1="1.10998125" x2="3.685540625" y2="1.115059375" layer="21"/>
<rectangle x1="5.285740625" y1="1.10998125" x2="5.37718125" y2="1.115059375" layer="21"/>
<rectangle x1="0.03301875" y1="1.115059375" x2="0.276859375" y2="1.120140625" layer="21"/>
<rectangle x1="3.46201875" y1="1.115059375" x2="3.69061875" y2="1.120140625" layer="21"/>
<rectangle x1="5.285740625" y1="1.115059375" x2="5.37718125" y2="1.120140625" layer="21"/>
<rectangle x1="0.03301875" y1="1.120140625" x2="0.276859375" y2="1.12521875" layer="21"/>
<rectangle x1="3.4671" y1="1.120140625" x2="3.69061875" y2="1.12521875" layer="21"/>
<rectangle x1="5.285740625" y1="1.120140625" x2="5.37718125" y2="1.12521875" layer="21"/>
<rectangle x1="0.03301875" y1="1.12521875" x2="0.276859375" y2="1.1303" layer="21"/>
<rectangle x1="3.46201875" y1="1.12521875" x2="3.69061875" y2="1.1303" layer="21"/>
<rectangle x1="5.285740625" y1="1.12521875" x2="5.37718125" y2="1.1303" layer="21"/>
<rectangle x1="0.03301875" y1="1.1303" x2="0.281940625" y2="1.13538125" layer="21"/>
<rectangle x1="3.46201875" y1="1.1303" x2="3.685540625" y2="1.13538125" layer="21"/>
<rectangle x1="5.285740625" y1="1.1303" x2="5.37718125" y2="1.13538125" layer="21"/>
<rectangle x1="0.03301875" y1="1.13538125" x2="0.281940625" y2="1.140459375" layer="21"/>
<rectangle x1="0.586740625" y1="1.13538125" x2="0.5969" y2="1.140459375" layer="21"/>
<rectangle x1="3.4671" y1="1.13538125" x2="3.69061875" y2="1.140459375" layer="21"/>
<rectangle x1="5.285740625" y1="1.13538125" x2="5.37718125" y2="1.140459375" layer="21"/>
<rectangle x1="0.0381" y1="1.140459375" x2="0.28701875" y2="1.145540625" layer="21"/>
<rectangle x1="0.581659375" y1="1.140459375" x2="0.5969" y2="1.145540625" layer="21"/>
<rectangle x1="3.46201875" y1="1.140459375" x2="3.685540625" y2="1.145540625" layer="21"/>
<rectangle x1="5.285740625" y1="1.140459375" x2="5.37718125" y2="1.145540625" layer="21"/>
<rectangle x1="0.0381" y1="1.145540625" x2="0.28701875" y2="1.15061875" layer="21"/>
<rectangle x1="0.57658125" y1="1.145540625" x2="0.60198125" y2="1.15061875" layer="21"/>
<rectangle x1="3.4671" y1="1.145540625" x2="3.69061875" y2="1.15061875" layer="21"/>
<rectangle x1="5.285740625" y1="1.145540625" x2="5.37718125" y2="1.15061875" layer="21"/>
<rectangle x1="0.0381" y1="1.15061875" x2="0.2921" y2="1.1557" layer="21"/>
<rectangle x1="0.5715" y1="1.15061875" x2="0.607059375" y2="1.1557" layer="21"/>
<rectangle x1="3.4671" y1="1.15061875" x2="3.69061875" y2="1.1557" layer="21"/>
<rectangle x1="5.285740625" y1="1.15061875" x2="5.37718125" y2="1.1557" layer="21"/>
<rectangle x1="0.0381" y1="1.1557" x2="0.29718125" y2="1.16078125" layer="21"/>
<rectangle x1="0.56641875" y1="1.1557" x2="0.612140625" y2="1.16078125" layer="21"/>
<rectangle x1="3.46201875" y1="1.1557" x2="3.69061875" y2="1.16078125" layer="21"/>
<rectangle x1="5.285740625" y1="1.1557" x2="5.37718125" y2="1.16078125" layer="21"/>
<rectangle x1="0.0381" y1="1.16078125" x2="0.302259375" y2="1.165859375" layer="21"/>
<rectangle x1="0.556259375" y1="1.16078125" x2="0.61721875" y2="1.165859375" layer="21"/>
<rectangle x1="0.967740625" y1="1.16078125" x2="1.03378125" y2="1.165859375" layer="21"/>
<rectangle x1="3.4671" y1="1.16078125" x2="3.685540625" y2="1.165859375" layer="21"/>
<rectangle x1="5.285740625" y1="1.16078125" x2="5.37718125" y2="1.165859375" layer="21"/>
<rectangle x1="0.04318125" y1="1.165859375" x2="0.307340625" y2="1.170940625" layer="21"/>
<rectangle x1="0.55118125" y1="1.165859375" x2="0.6223" y2="1.170940625" layer="21"/>
<rectangle x1="0.9525" y1="1.165859375" x2="1.04901875" y2="1.170940625" layer="21"/>
<rectangle x1="3.46201875" y1="1.165859375" x2="3.69061875" y2="1.170940625" layer="21"/>
<rectangle x1="5.285740625" y1="1.165859375" x2="5.37718125" y2="1.170940625" layer="21"/>
<rectangle x1="0.04318125" y1="1.170940625" x2="0.31241875" y2="1.17601875" layer="21"/>
<rectangle x1="0.54101875" y1="1.170940625" x2="0.62738125" y2="1.17601875" layer="21"/>
<rectangle x1="0.942340625" y1="1.170940625" x2="1.05918125" y2="1.17601875" layer="21"/>
<rectangle x1="3.46201875" y1="1.170940625" x2="3.685540625" y2="1.17601875" layer="21"/>
<rectangle x1="5.285740625" y1="1.170940625" x2="5.37718125" y2="1.17601875" layer="21"/>
<rectangle x1="0.04318125" y1="1.17601875" x2="0.32258125" y2="1.1811" layer="21"/>
<rectangle x1="0.530859375" y1="1.17601875" x2="0.632459375" y2="1.1811" layer="21"/>
<rectangle x1="0.93218125" y1="1.17601875" x2="1.069340625" y2="1.1811" layer="21"/>
<rectangle x1="3.4671" y1="1.17601875" x2="3.69061875" y2="1.1811" layer="21"/>
<rectangle x1="5.285740625" y1="1.17601875" x2="5.37718125" y2="1.1811" layer="21"/>
<rectangle x1="0.04318125" y1="1.1811" x2="0.332740625" y2="1.18618125" layer="21"/>
<rectangle x1="0.5207" y1="1.1811" x2="0.637540625" y2="1.18618125" layer="21"/>
<rectangle x1="0.9271" y1="1.1811" x2="1.07441875" y2="1.18618125" layer="21"/>
<rectangle x1="3.46201875" y1="1.1811" x2="3.69061875" y2="1.18618125" layer="21"/>
<rectangle x1="5.285740625" y1="1.1811" x2="5.37718125" y2="1.18618125" layer="21"/>
<rectangle x1="0.048259375" y1="1.18618125" x2="0.3429" y2="1.191259375" layer="21"/>
<rectangle x1="0.505459375" y1="1.18618125" x2="0.64261875" y2="1.191259375" layer="21"/>
<rectangle x1="0.916940625" y1="1.18618125" x2="1.08458125" y2="1.191259375" layer="21"/>
<rectangle x1="3.4671" y1="1.18618125" x2="3.685540625" y2="1.191259375" layer="21"/>
<rectangle x1="5.285740625" y1="1.18618125" x2="5.37718125" y2="1.191259375" layer="21"/>
<rectangle x1="0.048259375" y1="1.191259375" x2="0.36321875" y2="1.196340625" layer="21"/>
<rectangle x1="0.49021875" y1="1.191259375" x2="0.6477" y2="1.196340625" layer="21"/>
<rectangle x1="0.911859375" y1="1.191259375" x2="1.089659375" y2="1.196340625" layer="21"/>
<rectangle x1="3.4671" y1="1.191259375" x2="3.69061875" y2="1.196340625" layer="21"/>
<rectangle x1="5.285740625" y1="1.191259375" x2="5.37718125" y2="1.196340625" layer="21"/>
<rectangle x1="0.053340625" y1="1.196340625" x2="0.378459375" y2="1.20141875" layer="21"/>
<rectangle x1="0.4699" y1="1.196340625" x2="0.65278125" y2="1.20141875" layer="21"/>
<rectangle x1="0.90678125" y1="1.196340625" x2="1.094740625" y2="1.20141875" layer="21"/>
<rectangle x1="3.46201875" y1="1.196340625" x2="3.69061875" y2="1.20141875" layer="21"/>
<rectangle x1="5.285740625" y1="1.196340625" x2="5.37718125" y2="1.20141875" layer="21"/>
<rectangle x1="0.053340625" y1="1.20141875" x2="0.41401875" y2="1.2065" layer="21"/>
<rectangle x1="0.4191" y1="1.20141875" x2="0.42418125" y2="1.2065" layer="21"/>
<rectangle x1="0.429259375" y1="1.20141875" x2="0.65278125" y2="1.2065" layer="21"/>
<rectangle x1="0.9017" y1="1.20141875" x2="1.09981875" y2="1.2065" layer="21"/>
<rectangle x1="3.4671" y1="1.20141875" x2="3.69061875" y2="1.2065" layer="21"/>
<rectangle x1="5.285740625" y1="1.20141875" x2="5.37718125" y2="1.2065" layer="21"/>
<rectangle x1="0.053340625" y1="1.2065" x2="0.657859375" y2="1.21158125" layer="21"/>
<rectangle x1="0.89661875" y1="1.2065" x2="1.1049" y2="1.21158125" layer="21"/>
<rectangle x1="3.46201875" y1="1.2065" x2="3.685540625" y2="1.21158125" layer="21"/>
<rectangle x1="5.285740625" y1="1.2065" x2="5.37718125" y2="1.21158125" layer="21"/>
<rectangle x1="0.05841875" y1="1.21158125" x2="0.662940625" y2="1.216659375" layer="21"/>
<rectangle x1="0.891540625" y1="1.21158125" x2="1.1049" y2="1.216659375" layer="21"/>
<rectangle x1="3.46201875" y1="1.21158125" x2="3.69061875" y2="1.216659375" layer="21"/>
<rectangle x1="5.285740625" y1="1.21158125" x2="5.37718125" y2="1.216659375" layer="21"/>
<rectangle x1="0.05841875" y1="1.216659375" x2="0.66801875" y2="1.221740625" layer="21"/>
<rectangle x1="0.891540625" y1="1.216659375" x2="1.10998125" y2="1.221740625" layer="21"/>
<rectangle x1="3.4671" y1="1.216659375" x2="3.69061875" y2="1.221740625" layer="21"/>
<rectangle x1="5.285740625" y1="1.216659375" x2="5.37718125" y2="1.221740625" layer="21"/>
<rectangle x1="0.0635" y1="1.221740625" x2="0.6731" y2="1.22681875" layer="21"/>
<rectangle x1="0.886459375" y1="1.221740625" x2="1.115059375" y2="1.22681875" layer="21"/>
<rectangle x1="3.4671" y1="1.221740625" x2="3.69061875" y2="1.22681875" layer="21"/>
<rectangle x1="5.285740625" y1="1.221740625" x2="5.37718125" y2="1.22681875" layer="21"/>
<rectangle x1="0.0635" y1="1.22681875" x2="0.67818125" y2="1.2319" layer="21"/>
<rectangle x1="0.886459375" y1="1.22681875" x2="1.115059375" y2="1.2319" layer="21"/>
<rectangle x1="3.46201875" y1="1.22681875" x2="3.685540625" y2="1.2319" layer="21"/>
<rectangle x1="5.285740625" y1="1.22681875" x2="5.37718125" y2="1.2319" layer="21"/>
<rectangle x1="6.454140625" y1="1.22681875" x2="6.4643" y2="1.2319" layer="21"/>
<rectangle x1="0.06858125" y1="1.2319" x2="0.683259375" y2="1.23698125" layer="21"/>
<rectangle x1="0.88138125" y1="1.2319" x2="1.120140625" y2="1.23698125" layer="21"/>
<rectangle x1="3.4671" y1="1.2319" x2="3.69061875" y2="1.23698125" layer="21"/>
<rectangle x1="5.285740625" y1="1.2319" x2="5.37718125" y2="1.23698125" layer="21"/>
<rectangle x1="6.43381875" y1="1.2319" x2="6.4897" y2="1.23698125" layer="21"/>
<rectangle x1="0.06858125" y1="1.23698125" x2="0.688340625" y2="1.242059375" layer="21"/>
<rectangle x1="0.88138125" y1="1.23698125" x2="1.120140625" y2="1.242059375" layer="21"/>
<rectangle x1="3.46201875" y1="1.23698125" x2="3.69061875" y2="1.242059375" layer="21"/>
<rectangle x1="5.285740625" y1="1.23698125" x2="5.37718125" y2="1.242059375" layer="21"/>
<rectangle x1="6.428740625" y1="1.23698125" x2="6.499859375" y2="1.242059375" layer="21"/>
<rectangle x1="0.073659375" y1="1.242059375" x2="0.69341875" y2="1.247140625" layer="21"/>
<rectangle x1="0.8763" y1="1.242059375" x2="1.12521875" y2="1.247140625" layer="21"/>
<rectangle x1="3.4671" y1="1.242059375" x2="3.685540625" y2="1.247140625" layer="21"/>
<rectangle x1="5.285740625" y1="1.242059375" x2="5.37718125" y2="1.247140625" layer="21"/>
<rectangle x1="6.41858125" y1="1.242059375" x2="6.504940625" y2="1.247140625" layer="21"/>
<rectangle x1="0.078740625" y1="1.247140625" x2="0.6985" y2="1.25221875" layer="21"/>
<rectangle x1="0.8763" y1="1.247140625" x2="1.12521875" y2="1.25221875" layer="21"/>
<rectangle x1="3.46201875" y1="1.247140625" x2="3.69061875" y2="1.25221875" layer="21"/>
<rectangle x1="5.285740625" y1="1.247140625" x2="5.37718125" y2="1.25221875" layer="21"/>
<rectangle x1="6.4135" y1="1.247140625" x2="6.51001875" y2="1.25221875" layer="21"/>
<rectangle x1="0.078740625" y1="1.25221875" x2="0.70358125" y2="1.2573" layer="21"/>
<rectangle x1="0.8763" y1="1.25221875" x2="1.12521875" y2="1.2573" layer="21"/>
<rectangle x1="3.4671" y1="1.25221875" x2="3.69061875" y2="1.2573" layer="21"/>
<rectangle x1="5.285740625" y1="1.25221875" x2="5.37718125" y2="1.2573" layer="21"/>
<rectangle x1="6.40841875" y1="1.25221875" x2="6.5151" y2="1.2573" layer="21"/>
<rectangle x1="0.08381875" y1="1.2573" x2="0.70358125" y2="1.26238125" layer="21"/>
<rectangle x1="0.8763" y1="1.2573" x2="1.1303" y2="1.26238125" layer="21"/>
<rectangle x1="3.46201875" y1="1.2573" x2="3.69061875" y2="1.26238125" layer="21"/>
<rectangle x1="5.285740625" y1="1.2573" x2="5.37718125" y2="1.26238125" layer="21"/>
<rectangle x1="6.403340625" y1="1.2573" x2="6.52018125" y2="1.26238125" layer="21"/>
<rectangle x1="0.0889" y1="1.26238125" x2="0.708659375" y2="1.267459375" layer="21"/>
<rectangle x1="0.87121875" y1="1.26238125" x2="1.1303" y2="1.267459375" layer="21"/>
<rectangle x1="3.4671" y1="1.26238125" x2="3.69061875" y2="1.267459375" layer="21"/>
<rectangle x1="5.285740625" y1="1.26238125" x2="5.37718125" y2="1.267459375" layer="21"/>
<rectangle x1="6.403340625" y1="1.26238125" x2="6.525259375" y2="1.267459375" layer="21"/>
<rectangle x1="0.0889" y1="1.267459375" x2="0.713740625" y2="1.272540625" layer="21"/>
<rectangle x1="0.87121875" y1="1.267459375" x2="1.1303" y2="1.272540625" layer="21"/>
<rectangle x1="3.46201875" y1="1.267459375" x2="3.685540625" y2="1.272540625" layer="21"/>
<rectangle x1="5.285740625" y1="1.267459375" x2="5.37718125" y2="1.272540625" layer="21"/>
<rectangle x1="6.398259375" y1="1.267459375" x2="6.525259375" y2="1.272540625" layer="21"/>
<rectangle x1="0.09398125" y1="1.272540625" x2="0.71881875" y2="1.27761875" layer="21"/>
<rectangle x1="0.87121875" y1="1.272540625" x2="1.1303" y2="1.27761875" layer="21"/>
<rectangle x1="3.4671" y1="1.272540625" x2="3.69061875" y2="1.27761875" layer="21"/>
<rectangle x1="5.285740625" y1="1.272540625" x2="5.37718125" y2="1.27761875" layer="21"/>
<rectangle x1="6.398259375" y1="1.272540625" x2="6.525259375" y2="1.27761875" layer="21"/>
<rectangle x1="0.099059375" y1="1.27761875" x2="0.7239" y2="1.2827" layer="21"/>
<rectangle x1="0.87121875" y1="1.27761875" x2="1.1303" y2="1.2827" layer="21"/>
<rectangle x1="3.46201875" y1="1.27761875" x2="3.69061875" y2="1.2827" layer="21"/>
<rectangle x1="5.285740625" y1="1.27761875" x2="5.37718125" y2="1.2827" layer="21"/>
<rectangle x1="6.39318125" y1="1.27761875" x2="6.530340625" y2="1.2827" layer="21"/>
<rectangle x1="0.104140625" y1="1.2827" x2="0.72898125" y2="1.28778125" layer="21"/>
<rectangle x1="0.87121875" y1="1.2827" x2="1.1303" y2="1.28778125" layer="21"/>
<rectangle x1="3.46201875" y1="1.2827" x2="3.685540625" y2="1.28778125" layer="21"/>
<rectangle x1="5.285740625" y1="1.2827" x2="5.37718125" y2="1.28778125" layer="21"/>
<rectangle x1="6.39318125" y1="1.2827" x2="6.530340625" y2="1.28778125" layer="21"/>
<rectangle x1="0.104140625" y1="1.28778125" x2="0.734059375" y2="1.292859375" layer="21"/>
<rectangle x1="0.87121875" y1="1.28778125" x2="1.1303" y2="1.292859375" layer="21"/>
<rectangle x1="3.4671" y1="1.28778125" x2="3.69061875" y2="1.292859375" layer="21"/>
<rectangle x1="5.285740625" y1="1.28778125" x2="5.37718125" y2="1.292859375" layer="21"/>
<rectangle x1="6.39318125" y1="1.28778125" x2="6.530340625" y2="1.292859375" layer="21"/>
<rectangle x1="0.10921875" y1="1.292859375" x2="0.739140625" y2="1.297940625" layer="21"/>
<rectangle x1="0.87121875" y1="1.292859375" x2="1.1303" y2="1.297940625" layer="21"/>
<rectangle x1="3.4671" y1="1.292859375" x2="3.69061875" y2="1.297940625" layer="21"/>
<rectangle x1="5.285740625" y1="1.292859375" x2="5.37718125" y2="1.297940625" layer="21"/>
<rectangle x1="6.39318125" y1="1.292859375" x2="6.530340625" y2="1.297940625" layer="21"/>
<rectangle x1="0.1143" y1="1.297940625" x2="0.74421875" y2="1.30301875" layer="21"/>
<rectangle x1="0.87121875" y1="1.297940625" x2="1.1303" y2="1.30301875" layer="21"/>
<rectangle x1="3.46201875" y1="1.297940625" x2="3.69061875" y2="1.30301875" layer="21"/>
<rectangle x1="5.285740625" y1="1.297940625" x2="5.37718125" y2="1.30301875" layer="21"/>
<rectangle x1="6.398259375" y1="1.297940625" x2="6.530340625" y2="1.30301875" layer="21"/>
<rectangle x1="0.11938125" y1="1.30301875" x2="0.7493" y2="1.3081" layer="21"/>
<rectangle x1="0.87121875" y1="1.30301875" x2="1.1303" y2="1.3081" layer="21"/>
<rectangle x1="3.4671" y1="1.30301875" x2="3.685540625" y2="1.3081" layer="21"/>
<rectangle x1="5.285740625" y1="1.30301875" x2="5.37718125" y2="1.3081" layer="21"/>
<rectangle x1="6.39318125" y1="1.30301875" x2="6.530340625" y2="1.3081" layer="21"/>
<rectangle x1="0.124459375" y1="1.3081" x2="0.7493" y2="1.31318125" layer="21"/>
<rectangle x1="0.87121875" y1="1.3081" x2="1.1303" y2="1.31318125" layer="21"/>
<rectangle x1="3.46201875" y1="1.3081" x2="3.69061875" y2="1.31318125" layer="21"/>
<rectangle x1="5.285740625" y1="1.3081" x2="5.37718125" y2="1.31318125" layer="21"/>
<rectangle x1="6.39318125" y1="1.3081" x2="6.530340625" y2="1.31318125" layer="21"/>
<rectangle x1="0.129540625" y1="1.31318125" x2="0.74421875" y2="1.318259375" layer="21"/>
<rectangle x1="0.87121875" y1="1.31318125" x2="1.1303" y2="1.318259375" layer="21"/>
<rectangle x1="3.46201875" y1="1.31318125" x2="3.685540625" y2="1.318259375" layer="21"/>
<rectangle x1="5.285740625" y1="1.31318125" x2="5.37718125" y2="1.318259375" layer="21"/>
<rectangle x1="6.398259375" y1="1.31318125" x2="6.530340625" y2="1.318259375" layer="21"/>
<rectangle x1="0.1397" y1="1.318259375" x2="0.734059375" y2="1.323340625" layer="21"/>
<rectangle x1="0.8763" y1="1.318259375" x2="1.12521875" y2="1.323340625" layer="21"/>
<rectangle x1="3.4671" y1="1.318259375" x2="3.69061875" y2="1.323340625" layer="21"/>
<rectangle x1="5.285740625" y1="1.318259375" x2="5.37718125" y2="1.323340625" layer="21"/>
<rectangle x1="6.398259375" y1="1.318259375" x2="6.525259375" y2="1.323340625" layer="21"/>
<rectangle x1="0.1397" y1="1.323340625" x2="0.72898125" y2="1.32841875" layer="21"/>
<rectangle x1="0.8763" y1="1.323340625" x2="1.12521875" y2="1.32841875" layer="21"/>
<rectangle x1="3.4671" y1="1.323340625" x2="3.69061875" y2="1.32841875" layer="21"/>
<rectangle x1="5.285740625" y1="1.323340625" x2="5.37718125" y2="1.32841875" layer="21"/>
<rectangle x1="6.398259375" y1="1.323340625" x2="6.525259375" y2="1.32841875" layer="21"/>
<rectangle x1="0.149859375" y1="1.32841875" x2="0.7239" y2="1.3335" layer="21"/>
<rectangle x1="0.8763" y1="1.32841875" x2="1.12521875" y2="1.3335" layer="21"/>
<rectangle x1="3.46201875" y1="1.32841875" x2="3.69061875" y2="1.3335" layer="21"/>
<rectangle x1="5.285740625" y1="1.32841875" x2="5.37718125" y2="1.3335" layer="21"/>
<rectangle x1="6.403340625" y1="1.32841875" x2="6.525259375" y2="1.3335" layer="21"/>
<rectangle x1="0.154940625" y1="1.3335" x2="0.713740625" y2="1.33858125" layer="21"/>
<rectangle x1="0.8763" y1="1.3335" x2="1.120140625" y2="1.33858125" layer="21"/>
<rectangle x1="3.4671" y1="1.3335" x2="3.685540625" y2="1.33858125" layer="21"/>
<rectangle x1="5.285740625" y1="1.3335" x2="5.37718125" y2="1.33858125" layer="21"/>
<rectangle x1="6.403340625" y1="1.3335" x2="6.52018125" y2="1.33858125" layer="21"/>
<rectangle x1="0.16001875" y1="1.33858125" x2="0.708659375" y2="1.343659375" layer="21"/>
<rectangle x1="0.88138125" y1="1.33858125" x2="1.120140625" y2="1.343659375" layer="21"/>
<rectangle x1="3.46201875" y1="1.33858125" x2="3.69061875" y2="1.343659375" layer="21"/>
<rectangle x1="5.285740625" y1="1.33858125" x2="5.37718125" y2="1.343659375" layer="21"/>
<rectangle x1="6.40841875" y1="1.33858125" x2="6.5151" y2="1.343659375" layer="21"/>
<rectangle x1="0.17018125" y1="1.343659375" x2="0.70358125" y2="1.348740625" layer="21"/>
<rectangle x1="0.88138125" y1="1.343659375" x2="1.120140625" y2="1.348740625" layer="21"/>
<rectangle x1="3.4671" y1="1.343659375" x2="3.685540625" y2="1.348740625" layer="21"/>
<rectangle x1="5.285740625" y1="1.343659375" x2="5.37718125" y2="1.348740625" layer="21"/>
<rectangle x1="6.4135" y1="1.343659375" x2="6.51001875" y2="1.348740625" layer="21"/>
<rectangle x1="0.175259375" y1="1.348740625" x2="0.69341875" y2="1.35381875" layer="21"/>
<rectangle x1="0.886459375" y1="1.348740625" x2="1.115059375" y2="1.35381875" layer="21"/>
<rectangle x1="3.46201875" y1="1.348740625" x2="3.69061875" y2="1.35381875" layer="21"/>
<rectangle x1="5.285740625" y1="1.348740625" x2="5.37718125" y2="1.35381875" layer="21"/>
<rectangle x1="6.41858125" y1="1.348740625" x2="6.504940625" y2="1.35381875" layer="21"/>
<rectangle x1="0.18541875" y1="1.35381875" x2="0.683259375" y2="1.3589" layer="21"/>
<rectangle x1="0.891540625" y1="1.35381875" x2="1.10998125" y2="1.3589" layer="21"/>
<rectangle x1="3.4671" y1="1.35381875" x2="3.69061875" y2="1.3589" layer="21"/>
<rectangle x1="5.285740625" y1="1.35381875" x2="5.37718125" y2="1.3589" layer="21"/>
<rectangle x1="6.423659375" y1="1.35381875" x2="6.499859375" y2="1.3589" layer="21"/>
<rectangle x1="0.19558125" y1="1.3589" x2="0.67818125" y2="1.36398125" layer="21"/>
<rectangle x1="0.891540625" y1="1.3589" x2="1.10998125" y2="1.36398125" layer="21"/>
<rectangle x1="3.46201875" y1="1.3589" x2="3.69061875" y2="1.36398125" layer="21"/>
<rectangle x1="5.285740625" y1="1.3589" x2="5.37718125" y2="1.36398125" layer="21"/>
<rectangle x1="6.43381875" y1="1.3589" x2="6.4897" y2="1.36398125" layer="21"/>
<rectangle x1="0.200659375" y1="1.36398125" x2="0.66801875" y2="1.369059375" layer="21"/>
<rectangle x1="0.89661875" y1="1.36398125" x2="1.1049" y2="1.369059375" layer="21"/>
<rectangle x1="3.4671" y1="1.36398125" x2="3.685540625" y2="1.369059375" layer="21"/>
<rectangle x1="5.285740625" y1="1.36398125" x2="5.37718125" y2="1.369059375" layer="21"/>
<rectangle x1="6.454140625" y1="1.36398125" x2="6.4643" y2="1.369059375" layer="21"/>
<rectangle x1="6.46938125" y1="1.36398125" x2="6.474459375" y2="1.369059375" layer="21"/>
<rectangle x1="0.21081875" y1="1.369059375" x2="0.657859375" y2="1.374140625" layer="21"/>
<rectangle x1="0.9017" y1="1.369059375" x2="1.09981875" y2="1.374140625" layer="21"/>
<rectangle x1="3.46201875" y1="1.369059375" x2="3.69061875" y2="1.374140625" layer="21"/>
<rectangle x1="5.285740625" y1="1.369059375" x2="5.37718125" y2="1.374140625" layer="21"/>
<rectangle x1="0.22098125" y1="1.374140625" x2="0.6477" y2="1.37921875" layer="21"/>
<rectangle x1="0.9017" y1="1.374140625" x2="1.09981875" y2="1.37921875" layer="21"/>
<rectangle x1="3.4671" y1="1.374140625" x2="3.685540625" y2="1.37921875" layer="21"/>
<rectangle x1="5.285740625" y1="1.374140625" x2="5.37718125" y2="1.37921875" layer="21"/>
<rectangle x1="0.23621875" y1="1.37921875" x2="0.637540625" y2="1.3843" layer="21"/>
<rectangle x1="0.90678125" y1="1.37921875" x2="1.094740625" y2="1.3843" layer="21"/>
<rectangle x1="3.46201875" y1="1.37921875" x2="3.69061875" y2="1.3843" layer="21"/>
<rectangle x1="5.285740625" y1="1.37921875" x2="5.37718125" y2="1.3843" layer="21"/>
<rectangle x1="0.24638125" y1="1.3843" x2="0.6223" y2="1.38938125" layer="21"/>
<rectangle x1="0.911859375" y1="1.3843" x2="1.08458125" y2="1.38938125" layer="21"/>
<rectangle x1="3.46201875" y1="1.3843" x2="3.685540625" y2="1.38938125" layer="21"/>
<rectangle x1="5.285740625" y1="1.3843" x2="5.37718125" y2="1.38938125" layer="21"/>
<rectangle x1="0.256540625" y1="1.38938125" x2="0.607059375" y2="1.394459375" layer="21"/>
<rectangle x1="0.92201875" y1="1.38938125" x2="1.0795" y2="1.394459375" layer="21"/>
<rectangle x1="3.4671" y1="1.38938125" x2="3.69061875" y2="1.394459375" layer="21"/>
<rectangle x1="5.285740625" y1="1.38938125" x2="5.37718125" y2="1.394459375" layer="21"/>
<rectangle x1="0.276859375" y1="1.394459375" x2="0.59181875" y2="1.399540625" layer="21"/>
<rectangle x1="0.9271" y1="1.394459375" x2="1.07441875" y2="1.399540625" layer="21"/>
<rectangle x1="3.4671" y1="1.394459375" x2="3.685540625" y2="1.399540625" layer="21"/>
<rectangle x1="5.285740625" y1="1.394459375" x2="5.37718125" y2="1.399540625" layer="21"/>
<rectangle x1="0.29718125" y1="1.399540625" x2="0.5715" y2="1.40461875" layer="21"/>
<rectangle x1="0.937259375" y1="1.399540625" x2="1.064259375" y2="1.40461875" layer="21"/>
<rectangle x1="3.46201875" y1="1.399540625" x2="3.69061875" y2="1.40461875" layer="21"/>
<rectangle x1="5.285740625" y1="1.399540625" x2="5.37718125" y2="1.40461875" layer="21"/>
<rectangle x1="0.3175" y1="1.40461875" x2="0.55118125" y2="1.4097" layer="21"/>
<rectangle x1="0.942340625" y1="1.40461875" x2="1.05918125" y2="1.4097" layer="21"/>
<rectangle x1="3.4671" y1="1.40461875" x2="3.69061875" y2="1.4097" layer="21"/>
<rectangle x1="5.285740625" y1="1.40461875" x2="5.37718125" y2="1.4097" layer="21"/>
<rectangle x1="0.3429" y1="1.4097" x2="0.5207" y2="1.41478125" layer="21"/>
<rectangle x1="0.95758125" y1="1.4097" x2="1.043940625" y2="1.41478125" layer="21"/>
<rectangle x1="3.46201875" y1="1.4097" x2="3.69061875" y2="1.41478125" layer="21"/>
<rectangle x1="5.285740625" y1="1.4097" x2="5.37718125" y2="1.41478125" layer="21"/>
<rectangle x1="0.37338125" y1="1.41478125" x2="0.378459375" y2="1.419859375" layer="21"/>
<rectangle x1="0.383540625" y1="1.41478125" x2="0.38861875" y2="1.419859375" layer="21"/>
<rectangle x1="0.3937" y1="1.41478125" x2="0.480059375" y2="1.419859375" layer="21"/>
<rectangle x1="0.97281875" y1="1.41478125" x2="0.9779" y2="1.419859375" layer="21"/>
<rectangle x1="0.98298125" y1="1.41478125" x2="1.02361875" y2="1.419859375" layer="21"/>
<rectangle x1="3.46201875" y1="1.41478125" x2="3.685540625" y2="1.419859375" layer="21"/>
<rectangle x1="5.285740625" y1="1.41478125" x2="5.37718125" y2="1.419859375" layer="21"/>
<rectangle x1="3.4671" y1="1.419859375" x2="3.69061875" y2="1.424940625" layer="21"/>
<rectangle x1="5.285740625" y1="1.419859375" x2="5.37718125" y2="1.424940625" layer="21"/>
<rectangle x1="3.4671" y1="1.424940625" x2="3.685540625" y2="1.43001875" layer="21"/>
<rectangle x1="5.285740625" y1="1.424940625" x2="5.37718125" y2="1.43001875" layer="21"/>
<rectangle x1="3.46201875" y1="1.43001875" x2="3.69061875" y2="1.4351" layer="21"/>
<rectangle x1="5.285740625" y1="1.43001875" x2="5.37718125" y2="1.4351" layer="21"/>
<rectangle x1="3.4671" y1="1.4351" x2="3.69061875" y2="1.44018125" layer="21"/>
<rectangle x1="5.285740625" y1="1.4351" x2="5.37718125" y2="1.44018125" layer="21"/>
<rectangle x1="3.46201875" y1="1.44018125" x2="3.69061875" y2="1.445259375" layer="21"/>
<rectangle x1="5.285740625" y1="1.44018125" x2="5.37718125" y2="1.445259375" layer="21"/>
<rectangle x1="3.4671" y1="1.445259375" x2="3.685540625" y2="1.450340625" layer="21"/>
<rectangle x1="5.285740625" y1="1.445259375" x2="5.37718125" y2="1.450340625" layer="21"/>
<rectangle x1="3.46201875" y1="1.450340625" x2="3.69061875" y2="1.45541875" layer="21"/>
<rectangle x1="5.285740625" y1="1.450340625" x2="5.37718125" y2="1.45541875" layer="21"/>
<rectangle x1="3.46201875" y1="1.45541875" x2="3.69061875" y2="1.4605" layer="21"/>
<rectangle x1="5.285740625" y1="1.45541875" x2="5.37718125" y2="1.4605" layer="21"/>
<rectangle x1="3.47218125" y1="1.4605" x2="3.477259375" y2="1.46558125" layer="21"/>
<rectangle x1="3.482340625" y1="1.4605" x2="3.49758125" y2="1.46558125" layer="21"/>
<rectangle x1="3.502659375" y1="1.4605" x2="3.507740625" y2="1.46558125" layer="21"/>
<rectangle x1="3.51281875" y1="1.4605" x2="3.5179" y2="1.46558125" layer="21"/>
<rectangle x1="3.52298125" y1="1.4605" x2="3.528059375" y2="1.46558125" layer="21"/>
<rectangle x1="3.533140625" y1="1.4605" x2="3.53821875" y2="1.46558125" layer="21"/>
<rectangle x1="3.5433" y1="1.4605" x2="3.54838125" y2="1.46558125" layer="21"/>
<rectangle x1="3.553459375" y1="1.4605" x2="3.558540625" y2="1.46558125" layer="21"/>
<rectangle x1="3.56361875" y1="1.4605" x2="3.5687" y2="1.46558125" layer="21"/>
<rectangle x1="3.57378125" y1="1.4605" x2="3.578859375" y2="1.46558125" layer="21"/>
<rectangle x1="3.583940625" y1="1.4605" x2="3.58901875" y2="1.46558125" layer="21"/>
<rectangle x1="3.5941" y1="1.4605" x2="3.59918125" y2="1.46558125" layer="21"/>
<rectangle x1="3.604259375" y1="1.4605" x2="3.609340625" y2="1.46558125" layer="21"/>
<rectangle x1="3.61441875" y1="1.4605" x2="3.6195" y2="1.46558125" layer="21"/>
<rectangle x1="3.62458125" y1="1.4605" x2="3.629659375" y2="1.46558125" layer="21"/>
<rectangle x1="3.634740625" y1="1.4605" x2="3.63981875" y2="1.46558125" layer="21"/>
<rectangle x1="3.6449" y1="1.4605" x2="3.64998125" y2="1.46558125" layer="21"/>
<rectangle x1="3.655059375" y1="1.4605" x2="3.660140625" y2="1.46558125" layer="21"/>
<rectangle x1="3.66521875" y1="1.4605" x2="3.6703" y2="1.46558125" layer="21"/>
<rectangle x1="3.67538125" y1="1.4605" x2="3.680459375" y2="1.46558125" layer="21"/>
<rectangle x1="5.29081875" y1="1.4605" x2="5.2959" y2="1.46558125" layer="21"/>
<rectangle x1="5.30098125" y1="1.4605" x2="5.306059375" y2="1.46558125" layer="21"/>
<rectangle x1="5.311140625" y1="1.4605" x2="5.31621875" y2="1.46558125" layer="21"/>
<rectangle x1="5.3213" y1="1.4605" x2="5.32638125" y2="1.46558125" layer="21"/>
<rectangle x1="5.331459375" y1="1.4605" x2="5.336540625" y2="1.46558125" layer="21"/>
<rectangle x1="5.34161875" y1="1.4605" x2="5.3467" y2="1.46558125" layer="21"/>
<rectangle x1="5.35178125" y1="1.4605" x2="5.356859375" y2="1.46558125" layer="21"/>
<rectangle x1="5.361940625" y1="1.4605" x2="5.36701875" y2="1.46558125" layer="21"/>
<rectangle x1="5.3721" y1="1.4605" x2="5.37718125" y2="1.46558125" layer="21"/>
</package>
<package name="LOGO_SIMPLYDUINO">
<rectangle x1="7.86891875" y1="-0.00508125" x2="7.87908125" y2="0.00508125" layer="21"/>
<rectangle x1="7.8994" y1="-0.00508125" x2="7.909559375" y2="0.00508125" layer="21"/>
<rectangle x1="7.92988125" y1="-0.00508125" x2="7.940040625" y2="0.00508125" layer="21"/>
<rectangle x1="7.77748125" y1="0.00508125" x2="8.0518" y2="0.015240625" layer="21"/>
<rectangle x1="7.706359375" y1="0.015240625" x2="8.112759375" y2="0.0254" layer="21"/>
<rectangle x1="7.655559375" y1="0.0254" x2="8.163559375" y2="0.035559375" layer="21"/>
<rectangle x1="5.115559375" y1="0.035559375" x2="5.5626" y2="0.04571875" layer="21"/>
<rectangle x1="7.62508125" y1="0.035559375" x2="8.18388125" y2="0.04571875" layer="21"/>
<rectangle x1="5.1054" y1="0.04571875" x2="5.5626" y2="0.05588125" layer="21"/>
<rectangle x1="7.584440625" y1="0.04571875" x2="8.214359375" y2="0.05588125" layer="21"/>
<rectangle x1="5.115559375" y1="0.05588125" x2="5.5626" y2="0.066040625" layer="21"/>
<rectangle x1="7.57428125" y1="0.05588125" x2="8.23468125" y2="0.066040625" layer="21"/>
<rectangle x1="5.115559375" y1="0.066040625" x2="5.5626" y2="0.0762" layer="21"/>
<rectangle x1="7.57428125" y1="0.066040625" x2="8.255" y2="0.0762" layer="21"/>
<rectangle x1="5.115559375" y1="0.0762" x2="5.5626" y2="0.086359375" layer="21"/>
<rectangle x1="7.57428125" y1="0.0762" x2="8.265159375" y2="0.086359375" layer="21"/>
<rectangle x1="5.115559375" y1="0.086359375" x2="5.5626" y2="0.09651875" layer="21"/>
<rectangle x1="7.57428125" y1="0.086359375" x2="8.28548125" y2="0.09651875" layer="21"/>
<rectangle x1="5.115559375" y1="0.09651875" x2="5.5626" y2="0.10668125" layer="21"/>
<rectangle x1="7.584440625" y1="0.09651875" x2="8.295640625" y2="0.10668125" layer="21"/>
<rectangle x1="5.115559375" y1="0.10668125" x2="5.5626" y2="0.116840625" layer="21"/>
<rectangle x1="7.584440625" y1="0.10668125" x2="8.3058" y2="0.116840625" layer="21"/>
<rectangle x1="5.1054" y1="0.116840625" x2="5.5626" y2="0.127" layer="21"/>
<rectangle x1="7.584440625" y1="0.116840625" x2="8.32611875" y2="0.127" layer="21"/>
<rectangle x1="5.115559375" y1="0.127" x2="5.5626" y2="0.137159375" layer="21"/>
<rectangle x1="7.584440625" y1="0.127" x2="8.33628125" y2="0.137159375" layer="21"/>
<rectangle x1="5.115559375" y1="0.137159375" x2="5.5626" y2="0.14731875" layer="21"/>
<rectangle x1="7.584440625" y1="0.137159375" x2="8.33628125" y2="0.14731875" layer="21"/>
<rectangle x1="5.115559375" y1="0.14731875" x2="5.5626" y2="0.15748125" layer="21"/>
<rectangle x1="7.584440625" y1="0.14731875" x2="8.3566" y2="0.15748125" layer="21"/>
<rectangle x1="5.1054" y1="0.15748125" x2="5.5626" y2="0.167640625" layer="21"/>
<rectangle x1="7.5946" y1="0.15748125" x2="8.3566" y2="0.167640625" layer="21"/>
<rectangle x1="5.115559375" y1="0.167640625" x2="5.5626" y2="0.1778" layer="21"/>
<rectangle x1="7.5946" y1="0.167640625" x2="8.366759375" y2="0.1778" layer="21"/>
<rectangle x1="5.115559375" y1="0.1778" x2="5.5626" y2="0.187959375" layer="21"/>
<rectangle x1="7.5946" y1="0.1778" x2="8.37691875" y2="0.187959375" layer="21"/>
<rectangle x1="5.115559375" y1="0.187959375" x2="5.5626" y2="0.19811875" layer="21"/>
<rectangle x1="7.5946" y1="0.187959375" x2="8.38708125" y2="0.19811875" layer="21"/>
<rectangle x1="5.1054" y1="0.19811875" x2="5.5626" y2="0.20828125" layer="21"/>
<rectangle x1="7.5946" y1="0.19811875" x2="8.38708125" y2="0.20828125" layer="21"/>
<rectangle x1="5.115559375" y1="0.20828125" x2="5.5626" y2="0.218440625" layer="21"/>
<rectangle x1="7.5946" y1="0.20828125" x2="8.397240625" y2="0.218440625" layer="21"/>
<rectangle x1="5.115559375" y1="0.218440625" x2="5.5626" y2="0.2286" layer="21"/>
<rectangle x1="7.5946" y1="0.218440625" x2="8.397240625" y2="0.2286" layer="21"/>
<rectangle x1="5.115559375" y1="0.2286" x2="5.5626" y2="0.238759375" layer="21"/>
<rectangle x1="7.604759375" y1="0.2286" x2="8.4074" y2="0.238759375" layer="21"/>
<rectangle x1="5.1054" y1="0.238759375" x2="5.5626" y2="0.24891875" layer="21"/>
<rectangle x1="7.604759375" y1="0.238759375" x2="8.417559375" y2="0.24891875" layer="21"/>
<rectangle x1="5.1054" y1="0.24891875" x2="5.5626" y2="0.25908125" layer="21"/>
<rectangle x1="7.604759375" y1="0.24891875" x2="8.417559375" y2="0.25908125" layer="21"/>
<rectangle x1="5.115559375" y1="0.25908125" x2="5.5626" y2="0.269240625" layer="21"/>
<rectangle x1="7.604759375" y1="0.25908125" x2="8.42771875" y2="0.269240625" layer="21"/>
<rectangle x1="5.115559375" y1="0.269240625" x2="5.5626" y2="0.2794" layer="21"/>
<rectangle x1="7.604759375" y1="0.269240625" x2="8.42771875" y2="0.2794" layer="21"/>
<rectangle x1="5.115559375" y1="0.2794" x2="5.5626" y2="0.289559375" layer="21"/>
<rectangle x1="7.604759375" y1="0.2794" x2="8.43788125" y2="0.289559375" layer="21"/>
<rectangle x1="5.1054" y1="0.289559375" x2="5.5626" y2="0.29971875" layer="21"/>
<rectangle x1="7.604759375" y1="0.289559375" x2="8.43788125" y2="0.29971875" layer="21"/>
<rectangle x1="5.115559375" y1="0.29971875" x2="5.5626" y2="0.30988125" layer="21"/>
<rectangle x1="7.61491875" y1="0.29971875" x2="8.448040625" y2="0.30988125" layer="21"/>
<rectangle x1="5.115559375" y1="0.30988125" x2="5.5626" y2="0.320040625" layer="21"/>
<rectangle x1="7.61491875" y1="0.30988125" x2="8.448040625" y2="0.320040625" layer="21"/>
<rectangle x1="5.115559375" y1="0.320040625" x2="5.5626" y2="0.3302" layer="21"/>
<rectangle x1="7.61491875" y1="0.320040625" x2="8.448040625" y2="0.3302" layer="21"/>
<rectangle x1="5.115559375" y1="0.3302" x2="5.5626" y2="0.340359375" layer="21"/>
<rectangle x1="7.61491875" y1="0.3302" x2="8.4582" y2="0.340359375" layer="21"/>
<rectangle x1="5.115559375" y1="0.340359375" x2="5.5626" y2="0.35051875" layer="21"/>
<rectangle x1="7.61491875" y1="0.340359375" x2="8.4582" y2="0.35051875" layer="21"/>
<rectangle x1="5.115559375" y1="0.35051875" x2="5.5626" y2="0.36068125" layer="21"/>
<rectangle x1="7.62508125" y1="0.35051875" x2="8.468359375" y2="0.36068125" layer="21"/>
<rectangle x1="5.1054" y1="0.36068125" x2="5.5626" y2="0.370840625" layer="21"/>
<rectangle x1="7.62508125" y1="0.36068125" x2="8.468359375" y2="0.370840625" layer="21"/>
<rectangle x1="5.115559375" y1="0.370840625" x2="5.5626" y2="0.381" layer="21"/>
<rectangle x1="7.62508125" y1="0.370840625" x2="8.47851875" y2="0.381" layer="21"/>
<rectangle x1="5.115559375" y1="0.381" x2="5.5626" y2="0.391159375" layer="21"/>
<rectangle x1="7.62508125" y1="0.381" x2="7.76731875" y2="0.391159375" layer="21"/>
<rectangle x1="7.909559375" y1="0.381" x2="8.47851875" y2="0.391159375" layer="21"/>
<rectangle x1="5.115559375" y1="0.391159375" x2="5.5626" y2="0.40131875" layer="21"/>
<rectangle x1="7.62508125" y1="0.391159375" x2="7.71651875" y2="0.40131875" layer="21"/>
<rectangle x1="7.960359375" y1="0.391159375" x2="8.47851875" y2="0.40131875" layer="21"/>
<rectangle x1="5.1054" y1="0.40131875" x2="5.5626" y2="0.41148125" layer="21"/>
<rectangle x1="7.635240625" y1="0.40131875" x2="7.686040625" y2="0.41148125" layer="21"/>
<rectangle x1="7.98068125" y1="0.40131875" x2="8.48868125" y2="0.41148125" layer="21"/>
<rectangle x1="5.115559375" y1="0.41148125" x2="5.5626" y2="0.421640625" layer="21"/>
<rectangle x1="7.62508125" y1="0.41148125" x2="7.655559375" y2="0.421640625" layer="21"/>
<rectangle x1="8.001" y1="0.41148125" x2="8.48868125" y2="0.421640625" layer="21"/>
<rectangle x1="5.115559375" y1="0.421640625" x2="5.5626" y2="0.4318" layer="21"/>
<rectangle x1="8.011159375" y1="0.421640625" x2="8.498840625" y2="0.4318" layer="21"/>
<rectangle x1="5.115559375" y1="0.4318" x2="5.5626" y2="0.441959375" layer="21"/>
<rectangle x1="8.03148125" y1="0.4318" x2="8.498840625" y2="0.441959375" layer="21"/>
<rectangle x1="5.1054" y1="0.441959375" x2="5.5626" y2="0.45211875" layer="21"/>
<rectangle x1="8.041640625" y1="0.441959375" x2="8.498840625" y2="0.45211875" layer="21"/>
<rectangle x1="5.115559375" y1="0.45211875" x2="5.5626" y2="0.46228125" layer="21"/>
<rectangle x1="8.0518" y1="0.45211875" x2="8.509" y2="0.46228125" layer="21"/>
<rectangle x1="5.115559375" y1="0.46228125" x2="5.5626" y2="0.472440625" layer="21"/>
<rectangle x1="8.0518" y1="0.46228125" x2="8.509" y2="0.472440625" layer="21"/>
<rectangle x1="5.115559375" y1="0.472440625" x2="5.5626" y2="0.4826" layer="21"/>
<rectangle x1="8.061959375" y1="0.472440625" x2="8.519159375" y2="0.4826" layer="21"/>
<rectangle x1="5.1054" y1="0.4826" x2="5.5626" y2="0.492759375" layer="21"/>
<rectangle x1="8.07211875" y1="0.4826" x2="8.519159375" y2="0.492759375" layer="21"/>
<rectangle x1="5.1054" y1="0.492759375" x2="5.5626" y2="0.50291875" layer="21"/>
<rectangle x1="8.07211875" y1="0.492759375" x2="8.519159375" y2="0.50291875" layer="21"/>
<rectangle x1="5.115559375" y1="0.50291875" x2="5.5626" y2="0.51308125" layer="21"/>
<rectangle x1="8.08228125" y1="0.50291875" x2="8.52931875" y2="0.51308125" layer="21"/>
<rectangle x1="5.115559375" y1="0.51308125" x2="5.5626" y2="0.523240625" layer="21"/>
<rectangle x1="8.08228125" y1="0.51308125" x2="8.52931875" y2="0.523240625" layer="21"/>
<rectangle x1="5.115559375" y1="0.523240625" x2="5.5626" y2="0.5334" layer="21"/>
<rectangle x1="8.092440625" y1="0.523240625" x2="8.52931875" y2="0.5334" layer="21"/>
<rectangle x1="5.1054" y1="0.5334" x2="5.5626" y2="0.543559375" layer="21"/>
<rectangle x1="8.092440625" y1="0.5334" x2="8.53948125" y2="0.543559375" layer="21"/>
<rectangle x1="5.115559375" y1="0.543559375" x2="5.5626" y2="0.55371875" layer="21"/>
<rectangle x1="8.1026" y1="0.543559375" x2="8.53948125" y2="0.55371875" layer="21"/>
<rectangle x1="5.115559375" y1="0.55371875" x2="5.5626" y2="0.56388125" layer="21"/>
<rectangle x1="8.1026" y1="0.55371875" x2="8.549640625" y2="0.56388125" layer="21"/>
<rectangle x1="5.115559375" y1="0.56388125" x2="5.5626" y2="0.574040625" layer="21"/>
<rectangle x1="8.112759375" y1="0.56388125" x2="8.549640625" y2="0.574040625" layer="21"/>
<rectangle x1="5.115559375" y1="0.574040625" x2="5.5626" y2="0.5842" layer="21"/>
<rectangle x1="8.112759375" y1="0.574040625" x2="8.549640625" y2="0.5842" layer="21"/>
<rectangle x1="5.115559375" y1="0.5842" x2="5.5626" y2="0.594359375" layer="21"/>
<rectangle x1="8.112759375" y1="0.5842" x2="8.5598" y2="0.594359375" layer="21"/>
<rectangle x1="5.115559375" y1="0.594359375" x2="5.5626" y2="0.60451875" layer="21"/>
<rectangle x1="8.12291875" y1="0.594359375" x2="8.5598" y2="0.60451875" layer="21"/>
<rectangle x1="5.1054" y1="0.60451875" x2="5.5626" y2="0.61468125" layer="21"/>
<rectangle x1="8.12291875" y1="0.60451875" x2="8.569959375" y2="0.61468125" layer="21"/>
<rectangle x1="5.115559375" y1="0.61468125" x2="5.5626" y2="0.624840625" layer="21"/>
<rectangle x1="8.13308125" y1="0.61468125" x2="8.569959375" y2="0.624840625" layer="21"/>
<rectangle x1="5.115559375" y1="0.624840625" x2="5.5626" y2="0.635" layer="21"/>
<rectangle x1="8.13308125" y1="0.624840625" x2="8.569959375" y2="0.635" layer="21"/>
<rectangle x1="5.115559375" y1="0.635" x2="5.5626" y2="0.645159375" layer="21"/>
<rectangle x1="8.13308125" y1="0.635" x2="8.58011875" y2="0.645159375" layer="21"/>
<rectangle x1="0.635" y1="0.645159375" x2="0.645159375" y2="0.65531875" layer="21"/>
<rectangle x1="0.65531875" y1="0.645159375" x2="0.66548125" y2="0.65531875" layer="21"/>
<rectangle x1="0.675640625" y1="0.645159375" x2="0.6858" y2="0.65531875" layer="21"/>
<rectangle x1="0.70611875" y1="0.645159375" x2="0.71628125" y2="0.65531875" layer="21"/>
<rectangle x1="0.7366" y1="0.645159375" x2="0.746759375" y2="0.65531875" layer="21"/>
<rectangle x1="5.1054" y1="0.645159375" x2="5.5626" y2="0.65531875" layer="21"/>
<rectangle x1="8.143240625" y1="0.645159375" x2="8.58011875" y2="0.65531875" layer="21"/>
<rectangle x1="0.5334" y1="0.65531875" x2="0.543559375" y2="0.66548125" layer="21"/>
<rectangle x1="0.55371875" y1="0.65531875" x2="0.848359375" y2="0.66548125" layer="21"/>
<rectangle x1="5.115559375" y1="0.65531875" x2="5.5626" y2="0.66548125" layer="21"/>
<rectangle x1="8.143240625" y1="0.65531875" x2="8.59028125" y2="0.66548125" layer="21"/>
<rectangle x1="0.472440625" y1="0.66548125" x2="0.91948125" y2="0.675640625" layer="21"/>
<rectangle x1="5.115559375" y1="0.66548125" x2="5.5626" y2="0.675640625" layer="21"/>
<rectangle x1="5.93851875" y1="0.66548125" x2="5.94868125" y2="0.675640625" layer="21"/>
<rectangle x1="5.958840625" y1="0.66548125" x2="5.969" y2="0.675640625" layer="21"/>
<rectangle x1="5.979159375" y1="0.66548125" x2="6.009640625" y2="0.675640625" layer="21"/>
<rectangle x1="6.0198" y1="0.66548125" x2="6.060440625" y2="0.675640625" layer="21"/>
<rectangle x1="6.0706" y1="0.66548125" x2="6.080759375" y2="0.675640625" layer="21"/>
<rectangle x1="8.1534" y1="0.66548125" x2="8.59028125" y2="0.675640625" layer="21"/>
<rectangle x1="9.96188125" y1="0.66548125" x2="10.01268125" y2="0.675640625" layer="21"/>
<rectangle x1="10.022840625" y1="0.66548125" x2="10.05331875" y2="0.675640625" layer="21"/>
<rectangle x1="10.06348125" y1="0.66548125" x2="10.0838" y2="0.675640625" layer="21"/>
<rectangle x1="11.597640625" y1="0.66548125" x2="11.6078" y2="0.675640625" layer="21"/>
<rectangle x1="11.62811875" y1="0.66548125" x2="11.648440625" y2="0.675640625" layer="21"/>
<rectangle x1="11.6586" y1="0.66548125" x2="11.668759375" y2="0.675640625" layer="21"/>
<rectangle x1="11.67891875" y1="0.66548125" x2="11.719559375" y2="0.675640625" layer="21"/>
<rectangle x1="11.72971875" y1="0.66548125" x2="11.73988125" y2="0.675640625" layer="21"/>
<rectangle x1="15.69211875" y1="0.66548125" x2="15.70228125" y2="0.675640625" layer="21"/>
<rectangle x1="15.712440625" y1="0.66548125" x2="15.74291875" y2="0.675640625" layer="21"/>
<rectangle x1="15.75308125" y1="0.66548125" x2="15.783559375" y2="0.675640625" layer="21"/>
<rectangle x1="15.79371875" y1="0.66548125" x2="15.8242" y2="0.675640625" layer="21"/>
<rectangle x1="0.4318" y1="0.675640625" x2="0.96011875" y2="0.6858" layer="21"/>
<rectangle x1="5.115559375" y1="0.675640625" x2="5.5626" y2="0.6858" layer="21"/>
<rectangle x1="5.877559375" y1="0.675640625" x2="6.14171875" y2="0.6858" layer="21"/>
<rectangle x1="8.1534" y1="0.675640625" x2="8.59028125" y2="0.6858" layer="21"/>
<rectangle x1="9.8806" y1="0.675640625" x2="10.16508125" y2="0.6858" layer="21"/>
<rectangle x1="11.546840625" y1="0.675640625" x2="11.811" y2="0.6858" layer="21"/>
<rectangle x1="15.621" y1="0.675640625" x2="15.915640625" y2="0.6858" layer="21"/>
<rectangle x1="0.391159375" y1="0.6858" x2="1.000759375" y2="0.695959375" layer="21"/>
<rectangle x1="5.1054" y1="0.6858" x2="5.5626" y2="0.695959375" layer="21"/>
<rectangle x1="5.826759375" y1="0.6858" x2="6.182359375" y2="0.695959375" layer="21"/>
<rectangle x1="8.1534" y1="0.6858" x2="8.59028125" y2="0.695959375" layer="21"/>
<rectangle x1="9.839959375" y1="0.6858" x2="10.21588125" y2="0.695959375" layer="21"/>
<rectangle x1="11.496040625" y1="0.6858" x2="11.84148125" y2="0.695959375" layer="21"/>
<rectangle x1="15.580359375" y1="0.6858" x2="15.95628125" y2="0.695959375" layer="21"/>
<rectangle x1="0.36068125" y1="0.695959375" x2="1.0414" y2="0.70611875" layer="21"/>
<rectangle x1="5.115559375" y1="0.695959375" x2="5.5626" y2="0.70611875" layer="21"/>
<rectangle x1="5.78611875" y1="0.695959375" x2="6.212840625" y2="0.70611875" layer="21"/>
<rectangle x1="8.1534" y1="0.695959375" x2="8.600440625" y2="0.70611875" layer="21"/>
<rectangle x1="9.79931875" y1="0.695959375" x2="10.246359375" y2="0.70611875" layer="21"/>
<rectangle x1="11.465559375" y1="0.695959375" x2="11.871959375" y2="0.70611875" layer="21"/>
<rectangle x1="15.53971875" y1="0.695959375" x2="15.99691875" y2="0.70611875" layer="21"/>
<rectangle x1="0.320040625" y1="0.70611875" x2="1.07188125" y2="0.71628125" layer="21"/>
<rectangle x1="1.77291875" y1="0.70611875" x2="2.23011875" y2="0.71628125" layer="21"/>
<rectangle x1="2.53491875" y1="0.70611875" x2="2.981959375" y2="0.71628125" layer="21"/>
<rectangle x1="3.429" y1="0.70611875" x2="3.876040625" y2="0.71628125" layer="21"/>
<rectangle x1="4.333240625" y1="0.70611875" x2="4.78028125" y2="0.71628125" layer="21"/>
<rectangle x1="5.115559375" y1="0.70611875" x2="5.5626" y2="0.71628125" layer="21"/>
<rectangle x1="5.755640625" y1="0.70611875" x2="6.24331875" y2="0.71628125" layer="21"/>
<rectangle x1="6.924040625" y1="0.70611875" x2="7.381240625" y2="0.71628125" layer="21"/>
<rectangle x1="8.1534" y1="0.70611875" x2="8.6106" y2="0.71628125" layer="21"/>
<rectangle x1="9.768840625" y1="0.70611875" x2="10.276840625" y2="0.71628125" layer="21"/>
<rectangle x1="10.57148125" y1="0.70611875" x2="10.754359375" y2="0.71628125" layer="21"/>
<rectangle x1="11.43508125" y1="0.70611875" x2="11.902440625" y2="0.71628125" layer="21"/>
<rectangle x1="12.156440625" y1="0.70611875" x2="12.34948125" y2="0.71628125" layer="21"/>
<rectangle x1="12.837159375" y1="0.70611875" x2="13.020040625" y2="0.71628125" layer="21"/>
<rectangle x1="13.497559375" y1="0.70611875" x2="13.6906" y2="0.71628125" layer="21"/>
<rectangle x1="14.48308125" y1="0.70611875" x2="14.665959375" y2="0.71628125" layer="21"/>
<rectangle x1="15.509240625" y1="0.70611875" x2="16.0274" y2="0.71628125" layer="21"/>
<rectangle x1="0.29971875" y1="0.71628125" x2="1.082040625" y2="0.726440625" layer="21"/>
<rectangle x1="1.78308125" y1="0.71628125" x2="2.219959375" y2="0.726440625" layer="21"/>
<rectangle x1="2.53491875" y1="0.71628125" x2="2.981959375" y2="0.726440625" layer="21"/>
<rectangle x1="3.429" y1="0.71628125" x2="3.876040625" y2="0.726440625" layer="21"/>
<rectangle x1="4.333240625" y1="0.71628125" x2="4.78028125" y2="0.726440625" layer="21"/>
<rectangle x1="5.115559375" y1="0.71628125" x2="5.5626" y2="0.726440625" layer="21"/>
<rectangle x1="5.73531875" y1="0.71628125" x2="6.263640625" y2="0.726440625" layer="21"/>
<rectangle x1="6.924040625" y1="0.71628125" x2="7.37108125" y2="0.726440625" layer="21"/>
<rectangle x1="8.1534" y1="0.71628125" x2="8.6106" y2="0.726440625" layer="21"/>
<rectangle x1="9.738359375" y1="0.71628125" x2="10.30731875" y2="0.726440625" layer="21"/>
<rectangle x1="10.57148125" y1="0.71628125" x2="10.754359375" y2="0.726440625" layer="21"/>
<rectangle x1="11.414759375" y1="0.71628125" x2="11.922759375" y2="0.726440625" layer="21"/>
<rectangle x1="12.156440625" y1="0.71628125" x2="12.33931875" y2="0.726440625" layer="21"/>
<rectangle x1="12.827" y1="0.71628125" x2="13.020040625" y2="0.726440625" layer="21"/>
<rectangle x1="13.50771875" y1="0.71628125" x2="13.6906" y2="0.726440625" layer="21"/>
<rectangle x1="14.48308125" y1="0.71628125" x2="14.67611875" y2="0.726440625" layer="21"/>
<rectangle x1="15.478759375" y1="0.71628125" x2="16.05788125" y2="0.726440625" layer="21"/>
<rectangle x1="0.269240625" y1="0.726440625" x2="1.11251875" y2="0.7366" layer="21"/>
<rectangle x1="1.77291875" y1="0.726440625" x2="2.23011875" y2="0.7366" layer="21"/>
<rectangle x1="2.53491875" y1="0.726440625" x2="2.981959375" y2="0.7366" layer="21"/>
<rectangle x1="3.429" y1="0.726440625" x2="3.8862" y2="0.7366" layer="21"/>
<rectangle x1="4.333240625" y1="0.726440625" x2="4.78028125" y2="0.7366" layer="21"/>
<rectangle x1="5.1054" y1="0.726440625" x2="5.5626" y2="0.7366" layer="21"/>
<rectangle x1="5.704840625" y1="0.726440625" x2="6.29411875" y2="0.7366" layer="21"/>
<rectangle x1="6.9342" y1="0.726440625" x2="7.381240625" y2="0.7366" layer="21"/>
<rectangle x1="8.143240625" y1="0.726440625" x2="8.6106" y2="0.7366" layer="21"/>
<rectangle x1="9.718040625" y1="0.726440625" x2="10.327640625" y2="0.7366" layer="21"/>
<rectangle x1="10.57148125" y1="0.726440625" x2="10.754359375" y2="0.7366" layer="21"/>
<rectangle x1="11.394440625" y1="0.726440625" x2="11.94308125" y2="0.7366" layer="21"/>
<rectangle x1="12.156440625" y1="0.726440625" x2="12.34948125" y2="0.7366" layer="21"/>
<rectangle x1="12.827" y1="0.726440625" x2="13.00988125" y2="0.7366" layer="21"/>
<rectangle x1="13.497559375" y1="0.726440625" x2="13.6906" y2="0.7366" layer="21"/>
<rectangle x1="14.493240625" y1="0.726440625" x2="14.67611875" y2="0.7366" layer="21"/>
<rectangle x1="15.458440625" y1="0.726440625" x2="16.0782" y2="0.7366" layer="21"/>
<rectangle x1="0.24891875" y1="0.7366" x2="1.132840625" y2="0.746759375" layer="21"/>
<rectangle x1="1.78308125" y1="0.7366" x2="2.23011875" y2="0.746759375" layer="21"/>
<rectangle x1="2.524759375" y1="0.7366" x2="2.981959375" y2="0.746759375" layer="21"/>
<rectangle x1="3.429" y1="0.7366" x2="3.8862" y2="0.746759375" layer="21"/>
<rectangle x1="4.333240625" y1="0.7366" x2="4.78028125" y2="0.746759375" layer="21"/>
<rectangle x1="5.1054" y1="0.7366" x2="5.5626" y2="0.746759375" layer="21"/>
<rectangle x1="5.68451875" y1="0.7366" x2="6.30428125" y2="0.746759375" layer="21"/>
<rectangle x1="6.9342" y1="0.7366" x2="7.37108125" y2="0.746759375" layer="21"/>
<rectangle x1="8.143240625" y1="0.7366" x2="8.620759375" y2="0.746759375" layer="21"/>
<rectangle x1="9.687559375" y1="0.7366" x2="10.35811875" y2="0.746759375" layer="21"/>
<rectangle x1="10.57148125" y1="0.7366" x2="10.754359375" y2="0.746759375" layer="21"/>
<rectangle x1="11.37411875" y1="0.7366" x2="11.9634" y2="0.746759375" layer="21"/>
<rectangle x1="12.156440625" y1="0.7366" x2="12.34948125" y2="0.746759375" layer="21"/>
<rectangle x1="12.837159375" y1="0.7366" x2="13.020040625" y2="0.746759375" layer="21"/>
<rectangle x1="13.497559375" y1="0.7366" x2="13.6906" y2="0.746759375" layer="21"/>
<rectangle x1="14.48308125" y1="0.7366" x2="14.67611875" y2="0.746759375" layer="21"/>
<rectangle x1="15.427959375" y1="0.7366" x2="16.10868125" y2="0.746759375" layer="21"/>
<rectangle x1="0.2286" y1="0.746759375" x2="1.153159375" y2="0.75691875" layer="21"/>
<rectangle x1="1.77291875" y1="0.746759375" x2="2.219959375" y2="0.75691875" layer="21"/>
<rectangle x1="2.53491875" y1="0.746759375" x2="2.981959375" y2="0.75691875" layer="21"/>
<rectangle x1="3.429" y1="0.746759375" x2="3.876040625" y2="0.75691875" layer="21"/>
<rectangle x1="4.333240625" y1="0.746759375" x2="4.78028125" y2="0.75691875" layer="21"/>
<rectangle x1="5.115559375" y1="0.746759375" x2="5.5626" y2="0.75691875" layer="21"/>
<rectangle x1="5.674359375" y1="0.746759375" x2="6.3246" y2="0.75691875" layer="21"/>
<rectangle x1="6.924040625" y1="0.746759375" x2="7.381240625" y2="0.75691875" layer="21"/>
<rectangle x1="8.13308125" y1="0.746759375" x2="8.620759375" y2="0.75691875" layer="21"/>
<rectangle x1="9.6774" y1="0.746759375" x2="10.36828125" y2="0.75691875" layer="21"/>
<rectangle x1="10.57148125" y1="0.746759375" x2="10.754359375" y2="0.75691875" layer="21"/>
<rectangle x1="11.363959375" y1="0.746759375" x2="11.973559375" y2="0.75691875" layer="21"/>
<rectangle x1="12.156440625" y1="0.746759375" x2="12.33931875" y2="0.75691875" layer="21"/>
<rectangle x1="12.827" y1="0.746759375" x2="13.020040625" y2="0.75691875" layer="21"/>
<rectangle x1="13.50771875" y1="0.746759375" x2="13.680440625" y2="0.75691875" layer="21"/>
<rectangle x1="14.48308125" y1="0.746759375" x2="14.665959375" y2="0.75691875" layer="21"/>
<rectangle x1="15.4178" y1="0.746759375" x2="16.118840625" y2="0.75691875" layer="21"/>
<rectangle x1="0.20828125" y1="0.75691875" x2="1.17348125" y2="0.76708125" layer="21"/>
<rectangle x1="1.78308125" y1="0.75691875" x2="2.23011875" y2="0.76708125" layer="21"/>
<rectangle x1="2.53491875" y1="0.75691875" x2="2.981959375" y2="0.76708125" layer="21"/>
<rectangle x1="3.429" y1="0.75691875" x2="3.876040625" y2="0.76708125" layer="21"/>
<rectangle x1="4.333240625" y1="0.75691875" x2="4.78028125" y2="0.76708125" layer="21"/>
<rectangle x1="5.115559375" y1="0.75691875" x2="5.5626" y2="0.76708125" layer="21"/>
<rectangle x1="5.654040625" y1="0.75691875" x2="6.334759375" y2="0.76708125" layer="21"/>
<rectangle x1="6.924040625" y1="0.75691875" x2="7.381240625" y2="0.76708125" layer="21"/>
<rectangle x1="8.13308125" y1="0.75691875" x2="8.620759375" y2="0.76708125" layer="21"/>
<rectangle x1="9.65708125" y1="0.75691875" x2="10.3886" y2="0.76708125" layer="21"/>
<rectangle x1="10.57148125" y1="0.75691875" x2="10.754359375" y2="0.76708125" layer="21"/>
<rectangle x1="11.3538" y1="0.75691875" x2="11.99388125" y2="0.76708125" layer="21"/>
<rectangle x1="12.156440625" y1="0.75691875" x2="12.34948125" y2="0.76708125" layer="21"/>
<rectangle x1="12.837159375" y1="0.75691875" x2="13.020040625" y2="0.76708125" layer="21"/>
<rectangle x1="13.497559375" y1="0.75691875" x2="13.6906" y2="0.76708125" layer="21"/>
<rectangle x1="14.493240625" y1="0.75691875" x2="14.67611875" y2="0.76708125" layer="21"/>
<rectangle x1="15.39748125" y1="0.75691875" x2="16.139159375" y2="0.76708125" layer="21"/>
<rectangle x1="0.187959375" y1="0.76708125" x2="1.1938" y2="0.777240625" layer="21"/>
<rectangle x1="1.77291875" y1="0.76708125" x2="2.219959375" y2="0.777240625" layer="21"/>
<rectangle x1="2.53491875" y1="0.76708125" x2="2.981959375" y2="0.777240625" layer="21"/>
<rectangle x1="3.429" y1="0.76708125" x2="3.8862" y2="0.777240625" layer="21"/>
<rectangle x1="4.333240625" y1="0.76708125" x2="4.78028125" y2="0.777240625" layer="21"/>
<rectangle x1="5.115559375" y1="0.76708125" x2="5.5626" y2="0.777240625" layer="21"/>
<rectangle x1="5.64388125" y1="0.76708125" x2="6.35508125" y2="0.777240625" layer="21"/>
<rectangle x1="6.9342" y1="0.76708125" x2="7.381240625" y2="0.777240625" layer="21"/>
<rectangle x1="8.12291875" y1="0.76708125" x2="8.63091875" y2="0.777240625" layer="21"/>
<rectangle x1="9.636759375" y1="0.76708125" x2="10.40891875" y2="0.777240625" layer="21"/>
<rectangle x1="10.57148125" y1="0.76708125" x2="10.754359375" y2="0.777240625" layer="21"/>
<rectangle x1="11.33348125" y1="0.76708125" x2="12.004040625" y2="0.777240625" layer="21"/>
<rectangle x1="12.156440625" y1="0.76708125" x2="12.33931875" y2="0.777240625" layer="21"/>
<rectangle x1="12.827" y1="0.76708125" x2="13.00988125" y2="0.777240625" layer="21"/>
<rectangle x1="13.50771875" y1="0.76708125" x2="13.6906" y2="0.777240625" layer="21"/>
<rectangle x1="14.48308125" y1="0.76708125" x2="14.67611875" y2="0.777240625" layer="21"/>
<rectangle x1="15.377159375" y1="0.76708125" x2="16.15948125" y2="0.777240625" layer="21"/>
<rectangle x1="0.167640625" y1="0.777240625" x2="1.203959375" y2="0.7874" layer="21"/>
<rectangle x1="1.78308125" y1="0.777240625" x2="2.23011875" y2="0.7874" layer="21"/>
<rectangle x1="2.53491875" y1="0.777240625" x2="2.981959375" y2="0.7874" layer="21"/>
<rectangle x1="3.439159375" y1="0.777240625" x2="3.8862" y2="0.7874" layer="21"/>
<rectangle x1="4.333240625" y1="0.777240625" x2="4.78028125" y2="0.7874" layer="21"/>
<rectangle x1="5.1054" y1="0.777240625" x2="5.5626" y2="0.7874" layer="21"/>
<rectangle x1="5.63371875" y1="0.777240625" x2="6.3754" y2="0.7874" layer="21"/>
<rectangle x1="6.9342" y1="0.777240625" x2="7.37108125" y2="0.7874" layer="21"/>
<rectangle x1="8.12291875" y1="0.777240625" x2="8.63091875" y2="0.7874" layer="21"/>
<rectangle x1="9.616440625" y1="0.777240625" x2="10.41908125" y2="0.7874" layer="21"/>
<rectangle x1="10.57148125" y1="0.777240625" x2="10.754359375" y2="0.7874" layer="21"/>
<rectangle x1="11.32331875" y1="0.777240625" x2="12.0142" y2="0.7874" layer="21"/>
<rectangle x1="12.156440625" y1="0.777240625" x2="12.34948125" y2="0.7874" layer="21"/>
<rectangle x1="12.827" y1="0.777240625" x2="13.00988125" y2="0.7874" layer="21"/>
<rectangle x1="13.50771875" y1="0.777240625" x2="13.6906" y2="0.7874" layer="21"/>
<rectangle x1="14.493240625" y1="0.777240625" x2="14.67611875" y2="0.7874" layer="21"/>
<rectangle x1="15.356840625" y1="0.777240625" x2="16.1798" y2="0.7874" layer="21"/>
<rectangle x1="0.14731875" y1="0.7874" x2="1.22428125" y2="0.797559375" layer="21"/>
<rectangle x1="1.77291875" y1="0.7874" x2="2.23011875" y2="0.797559375" layer="21"/>
<rectangle x1="2.524759375" y1="0.7874" x2="2.981959375" y2="0.797559375" layer="21"/>
<rectangle x1="3.429" y1="0.7874" x2="3.876040625" y2="0.797559375" layer="21"/>
<rectangle x1="4.333240625" y1="0.7874" x2="4.78028125" y2="0.797559375" layer="21"/>
<rectangle x1="5.115559375" y1="0.7874" x2="5.5626" y2="0.797559375" layer="21"/>
<rectangle x1="5.6134" y1="0.7874" x2="6.385559375" y2="0.797559375" layer="21"/>
<rectangle x1="6.924040625" y1="0.7874" x2="7.381240625" y2="0.797559375" layer="21"/>
<rectangle x1="8.12291875" y1="0.7874" x2="8.64108125" y2="0.797559375" layer="21"/>
<rectangle x1="9.60628125" y1="0.7874" x2="10.4394" y2="0.797559375" layer="21"/>
<rectangle x1="10.57148125" y1="0.7874" x2="10.754359375" y2="0.797559375" layer="21"/>
<rectangle x1="11.313159375" y1="0.7874" x2="12.024359375" y2="0.797559375" layer="21"/>
<rectangle x1="12.156440625" y1="0.7874" x2="12.33931875" y2="0.797559375" layer="21"/>
<rectangle x1="12.837159375" y1="0.7874" x2="13.020040625" y2="0.797559375" layer="21"/>
<rectangle x1="13.497559375" y1="0.7874" x2="13.6906" y2="0.797559375" layer="21"/>
<rectangle x1="14.48308125" y1="0.7874" x2="14.665959375" y2="0.797559375" layer="21"/>
<rectangle x1="15.34668125" y1="0.7874" x2="16.189959375" y2="0.797559375" layer="21"/>
<rectangle x1="0.137159375" y1="0.797559375" x2="1.234440625" y2="0.80771875" layer="21"/>
<rectangle x1="1.78308125" y1="0.797559375" x2="2.219959375" y2="0.80771875" layer="21"/>
<rectangle x1="2.53491875" y1="0.797559375" x2="2.981959375" y2="0.80771875" layer="21"/>
<rectangle x1="3.429" y1="0.797559375" x2="3.876040625" y2="0.80771875" layer="21"/>
<rectangle x1="4.333240625" y1="0.797559375" x2="4.78028125" y2="0.80771875" layer="21"/>
<rectangle x1="5.115559375" y1="0.797559375" x2="5.5626" y2="0.80771875" layer="21"/>
<rectangle x1="5.603240625" y1="0.797559375" x2="6.39571875" y2="0.80771875" layer="21"/>
<rectangle x1="6.9342" y1="0.797559375" x2="7.37108125" y2="0.80771875" layer="21"/>
<rectangle x1="8.112759375" y1="0.797559375" x2="8.64108125" y2="0.80771875" layer="21"/>
<rectangle x1="9.585959375" y1="0.797559375" x2="10.449559375" y2="0.80771875" layer="21"/>
<rectangle x1="10.57148125" y1="0.797559375" x2="10.754359375" y2="0.80771875" layer="21"/>
<rectangle x1="11.303" y1="0.797559375" x2="12.04468125" y2="0.80771875" layer="21"/>
<rectangle x1="12.156440625" y1="0.797559375" x2="12.33931875" y2="0.80771875" layer="21"/>
<rectangle x1="12.827" y1="0.797559375" x2="13.020040625" y2="0.80771875" layer="21"/>
<rectangle x1="13.497559375" y1="0.797559375" x2="13.6906" y2="0.80771875" layer="21"/>
<rectangle x1="14.493240625" y1="0.797559375" x2="14.67611875" y2="0.80771875" layer="21"/>
<rectangle x1="15.33651875" y1="0.797559375" x2="16.20011875" y2="0.80771875" layer="21"/>
<rectangle x1="0.127" y1="0.80771875" x2="1.254759375" y2="0.81788125" layer="21"/>
<rectangle x1="1.77291875" y1="0.80771875" x2="2.23011875" y2="0.81788125" layer="21"/>
<rectangle x1="2.53491875" y1="0.80771875" x2="2.981959375" y2="0.81788125" layer="21"/>
<rectangle x1="3.429" y1="0.80771875" x2="3.876040625" y2="0.81788125" layer="21"/>
<rectangle x1="4.333240625" y1="0.80771875" x2="4.78028125" y2="0.81788125" layer="21"/>
<rectangle x1="5.115559375" y1="0.80771875" x2="5.5626" y2="0.81788125" layer="21"/>
<rectangle x1="5.59308125" y1="0.80771875" x2="6.40588125" y2="0.81788125" layer="21"/>
<rectangle x1="6.924040625" y1="0.80771875" x2="7.381240625" y2="0.81788125" layer="21"/>
<rectangle x1="8.112759375" y1="0.80771875" x2="8.64108125" y2="0.81788125" layer="21"/>
<rectangle x1="9.5758" y1="0.80771875" x2="10.45971875" y2="0.81788125" layer="21"/>
<rectangle x1="10.57148125" y1="0.80771875" x2="10.754359375" y2="0.81788125" layer="21"/>
<rectangle x1="11.292840625" y1="0.80771875" x2="12.054840625" y2="0.81788125" layer="21"/>
<rectangle x1="12.156440625" y1="0.80771875" x2="12.33931875" y2="0.81788125" layer="21"/>
<rectangle x1="12.837159375" y1="0.80771875" x2="13.00988125" y2="0.81788125" layer="21"/>
<rectangle x1="13.50771875" y1="0.80771875" x2="13.6906" y2="0.81788125" layer="21"/>
<rectangle x1="14.48308125" y1="0.80771875" x2="14.67611875" y2="0.81788125" layer="21"/>
<rectangle x1="15.3162" y1="0.80771875" x2="16.220440625" y2="0.81788125" layer="21"/>
<rectangle x1="0.10668125" y1="0.81788125" x2="1.26491875" y2="0.828040625" layer="21"/>
<rectangle x1="1.77291875" y1="0.81788125" x2="2.219959375" y2="0.828040625" layer="21"/>
<rectangle x1="2.524759375" y1="0.81788125" x2="2.981959375" y2="0.828040625" layer="21"/>
<rectangle x1="3.429" y1="0.81788125" x2="3.8862" y2="0.828040625" layer="21"/>
<rectangle x1="4.32308125" y1="0.81788125" x2="4.78028125" y2="0.828040625" layer="21"/>
<rectangle x1="5.115559375" y1="0.81788125" x2="5.5626" y2="0.828040625" layer="21"/>
<rectangle x1="5.58291875" y1="0.81788125" x2="6.4262" y2="0.828040625" layer="21"/>
<rectangle x1="6.9342" y1="0.81788125" x2="7.37108125" y2="0.828040625" layer="21"/>
<rectangle x1="8.1026" y1="0.81788125" x2="8.651240625" y2="0.828040625" layer="21"/>
<rectangle x1="9.565640625" y1="0.81788125" x2="10.480040625" y2="0.828040625" layer="21"/>
<rectangle x1="10.57148125" y1="0.81788125" x2="10.754359375" y2="0.828040625" layer="21"/>
<rectangle x1="11.28268125" y1="0.81788125" x2="12.065" y2="0.828040625" layer="21"/>
<rectangle x1="12.156440625" y1="0.81788125" x2="12.33931875" y2="0.828040625" layer="21"/>
<rectangle x1="12.837159375" y1="0.81788125" x2="13.020040625" y2="0.828040625" layer="21"/>
<rectangle x1="13.497559375" y1="0.81788125" x2="13.6906" y2="0.828040625" layer="21"/>
<rectangle x1="14.493240625" y1="0.81788125" x2="14.67611875" y2="0.828040625" layer="21"/>
<rectangle x1="15.306040625" y1="0.81788125" x2="16.2306" y2="0.828040625" layer="21"/>
<rectangle x1="0.09651875" y1="0.828040625" x2="1.27508125" y2="0.8382" layer="21"/>
<rectangle x1="1.78308125" y1="0.828040625" x2="2.23011875" y2="0.8382" layer="21"/>
<rectangle x1="2.53491875" y1="0.828040625" x2="2.981959375" y2="0.8382" layer="21"/>
<rectangle x1="3.429" y1="0.828040625" x2="3.876040625" y2="0.8382" layer="21"/>
<rectangle x1="4.333240625" y1="0.828040625" x2="4.78028125" y2="0.8382" layer="21"/>
<rectangle x1="5.115559375" y1="0.828040625" x2="5.5626" y2="0.8382" layer="21"/>
<rectangle x1="5.58291875" y1="0.828040625" x2="6.4262" y2="0.8382" layer="21"/>
<rectangle x1="6.924040625" y1="0.828040625" x2="7.381240625" y2="0.8382" layer="21"/>
<rectangle x1="8.1026" y1="0.828040625" x2="8.651240625" y2="0.8382" layer="21"/>
<rectangle x1="9.55548125" y1="0.828040625" x2="10.4902" y2="0.8382" layer="21"/>
<rectangle x1="10.57148125" y1="0.828040625" x2="10.754359375" y2="0.8382" layer="21"/>
<rectangle x1="11.27251875" y1="0.828040625" x2="12.075159375" y2="0.8382" layer="21"/>
<rectangle x1="12.156440625" y1="0.828040625" x2="12.33931875" y2="0.8382" layer="21"/>
<rectangle x1="12.827" y1="0.828040625" x2="13.00988125" y2="0.8382" layer="21"/>
<rectangle x1="13.50771875" y1="0.828040625" x2="13.6906" y2="0.8382" layer="21"/>
<rectangle x1="14.48308125" y1="0.828040625" x2="14.67611875" y2="0.8382" layer="21"/>
<rectangle x1="15.29588125" y1="0.828040625" x2="16.240759375" y2="0.8382" layer="21"/>
<rectangle x1="0.0762" y1="0.8382" x2="1.2954" y2="0.848359375" layer="21"/>
<rectangle x1="1.77291875" y1="0.8382" x2="2.23011875" y2="0.848359375" layer="21"/>
<rectangle x1="2.53491875" y1="0.8382" x2="2.981959375" y2="0.848359375" layer="21"/>
<rectangle x1="3.429" y1="0.8382" x2="3.8862" y2="0.848359375" layer="21"/>
<rectangle x1="4.333240625" y1="0.8382" x2="4.78028125" y2="0.848359375" layer="21"/>
<rectangle x1="5.115559375" y1="0.8382" x2="5.5626" y2="0.848359375" layer="21"/>
<rectangle x1="5.572759375" y1="0.8382" x2="6.44651875" y2="0.848359375" layer="21"/>
<rectangle x1="6.9342" y1="0.8382" x2="7.37108125" y2="0.848359375" layer="21"/>
<rectangle x1="8.092440625" y1="0.8382" x2="8.6614" y2="0.848359375" layer="21"/>
<rectangle x1="9.54531875" y1="0.8382" x2="9.941559375" y2="0.848359375" layer="21"/>
<rectangle x1="10.11428125" y1="0.8382" x2="10.500359375" y2="0.848359375" layer="21"/>
<rectangle x1="10.57148125" y1="0.8382" x2="10.754359375" y2="0.848359375" layer="21"/>
<rectangle x1="11.262359375" y1="0.8382" x2="11.6078" y2="0.848359375" layer="21"/>
<rectangle x1="11.7602" y1="0.8382" x2="12.08531875" y2="0.848359375" layer="21"/>
<rectangle x1="12.156440625" y1="0.8382" x2="12.33931875" y2="0.848359375" layer="21"/>
<rectangle x1="12.837159375" y1="0.8382" x2="13.020040625" y2="0.848359375" layer="21"/>
<rectangle x1="13.50771875" y1="0.8382" x2="13.6906" y2="0.848359375" layer="21"/>
<rectangle x1="14.493240625" y1="0.8382" x2="14.665959375" y2="0.848359375" layer="21"/>
<rectangle x1="15.275559375" y1="0.8382" x2="15.6718" y2="0.848359375" layer="21"/>
<rectangle x1="15.681959375" y1="0.8382" x2="15.69211875" y2="0.848359375" layer="21"/>
<rectangle x1="15.85468125" y1="0.8382" x2="16.25091875" y2="0.848359375" layer="21"/>
<rectangle x1="0.066040625" y1="0.848359375" x2="1.305559375" y2="0.85851875" layer="21"/>
<rectangle x1="1.77291875" y1="0.848359375" x2="2.219959375" y2="0.85851875" layer="21"/>
<rectangle x1="2.53491875" y1="0.848359375" x2="2.981959375" y2="0.85851875" layer="21"/>
<rectangle x1="3.439159375" y1="0.848359375" x2="3.876040625" y2="0.85851875" layer="21"/>
<rectangle x1="4.333240625" y1="0.848359375" x2="4.78028125" y2="0.85851875" layer="21"/>
<rectangle x1="5.1054" y1="0.848359375" x2="6.45668125" y2="0.85851875" layer="21"/>
<rectangle x1="6.924040625" y1="0.848359375" x2="7.381240625" y2="0.85851875" layer="21"/>
<rectangle x1="8.092440625" y1="0.848359375" x2="8.6614" y2="0.85851875" layer="21"/>
<rectangle x1="9.525" y1="0.848359375" x2="9.890759375" y2="0.85851875" layer="21"/>
<rectangle x1="10.16508125" y1="0.848359375" x2="10.500359375" y2="0.85851875" layer="21"/>
<rectangle x1="10.57148125" y1="0.848359375" x2="10.754359375" y2="0.85851875" layer="21"/>
<rectangle x1="11.262359375" y1="0.848359375" x2="11.567159375" y2="0.85851875" layer="21"/>
<rectangle x1="11.811" y1="0.848359375" x2="12.09548125" y2="0.85851875" layer="21"/>
<rectangle x1="12.156440625" y1="0.848359375" x2="12.33931875" y2="0.85851875" layer="21"/>
<rectangle x1="12.827" y1="0.848359375" x2="13.00988125" y2="0.85851875" layer="21"/>
<rectangle x1="13.497559375" y1="0.848359375" x2="13.6906" y2="0.85851875" layer="21"/>
<rectangle x1="14.48308125" y1="0.848359375" x2="14.67611875" y2="0.85851875" layer="21"/>
<rectangle x1="15.2654" y1="0.848359375" x2="15.631159375" y2="0.85851875" layer="21"/>
<rectangle x1="15.90548125" y1="0.848359375" x2="16.271240625" y2="0.85851875" layer="21"/>
<rectangle x1="0.05588125" y1="0.85851875" x2="1.31571875" y2="0.86868125" layer="21"/>
<rectangle x1="1.77291875" y1="0.85851875" x2="2.23011875" y2="0.86868125" layer="21"/>
<rectangle x1="2.53491875" y1="0.85851875" x2="2.981959375" y2="0.86868125" layer="21"/>
<rectangle x1="3.429" y1="0.85851875" x2="3.8862" y2="0.86868125" layer="21"/>
<rectangle x1="4.333240625" y1="0.85851875" x2="4.78028125" y2="0.86868125" layer="21"/>
<rectangle x1="5.115559375" y1="0.85851875" x2="6.466840625" y2="0.86868125" layer="21"/>
<rectangle x1="6.924040625" y1="0.85851875" x2="7.381240625" y2="0.86868125" layer="21"/>
<rectangle x1="8.092440625" y1="0.85851875" x2="8.6614" y2="0.86868125" layer="21"/>
<rectangle x1="9.514840625" y1="0.85851875" x2="9.86028125" y2="0.86868125" layer="21"/>
<rectangle x1="10.20571875" y1="0.85851875" x2="10.52068125" y2="0.86868125" layer="21"/>
<rectangle x1="10.57148125" y1="0.85851875" x2="10.754359375" y2="0.86868125" layer="21"/>
<rectangle x1="11.2522" y1="0.85851875" x2="11.53668125" y2="0.86868125" layer="21"/>
<rectangle x1="11.84148125" y1="0.85851875" x2="12.09548125" y2="0.86868125" layer="21"/>
<rectangle x1="12.156440625" y1="0.85851875" x2="12.33931875" y2="0.86868125" layer="21"/>
<rectangle x1="12.837159375" y1="0.85851875" x2="13.020040625" y2="0.86868125" layer="21"/>
<rectangle x1="13.497559375" y1="0.85851875" x2="13.6906" y2="0.86868125" layer="21"/>
<rectangle x1="14.48308125" y1="0.85851875" x2="14.67611875" y2="0.86868125" layer="21"/>
<rectangle x1="15.255240625" y1="0.85851875" x2="15.60068125" y2="0.86868125" layer="21"/>
<rectangle x1="15.94611875" y1="0.85851875" x2="16.271240625" y2="0.86868125" layer="21"/>
<rectangle x1="0.04571875" y1="0.86868125" x2="1.32588125" y2="0.878840625" layer="21"/>
<rectangle x1="1.78308125" y1="0.86868125" x2="2.219959375" y2="0.878840625" layer="21"/>
<rectangle x1="2.524759375" y1="0.86868125" x2="2.981959375" y2="0.878840625" layer="21"/>
<rectangle x1="3.429" y1="0.86868125" x2="3.876040625" y2="0.878840625" layer="21"/>
<rectangle x1="4.333240625" y1="0.86868125" x2="4.78028125" y2="0.878840625" layer="21"/>
<rectangle x1="5.115559375" y1="0.86868125" x2="6.466840625" y2="0.878840625" layer="21"/>
<rectangle x1="6.9342" y1="0.86868125" x2="7.381240625" y2="0.878840625" layer="21"/>
<rectangle x1="8.08228125" y1="0.86868125" x2="8.671559375" y2="0.878840625" layer="21"/>
<rectangle x1="9.514840625" y1="0.86868125" x2="9.8298" y2="0.878840625" layer="21"/>
<rectangle x1="10.226040625" y1="0.86868125" x2="10.530840625" y2="0.878840625" layer="21"/>
<rectangle x1="10.57148125" y1="0.86868125" x2="10.754359375" y2="0.878840625" layer="21"/>
<rectangle x1="11.242040625" y1="0.86868125" x2="11.516359375" y2="0.878840625" layer="21"/>
<rectangle x1="11.871959375" y1="0.86868125" x2="12.105640625" y2="0.878840625" layer="21"/>
<rectangle x1="12.156440625" y1="0.86868125" x2="12.33931875" y2="0.878840625" layer="21"/>
<rectangle x1="12.827" y1="0.86868125" x2="13.020040625" y2="0.878840625" layer="21"/>
<rectangle x1="13.50771875" y1="0.86868125" x2="13.680440625" y2="0.878840625" layer="21"/>
<rectangle x1="14.493240625" y1="0.86868125" x2="14.665959375" y2="0.878840625" layer="21"/>
<rectangle x1="15.255240625" y1="0.86868125" x2="15.560040625" y2="0.878840625" layer="21"/>
<rectangle x1="15.966440625" y1="0.86868125" x2="16.291559375" y2="0.878840625" layer="21"/>
<rectangle x1="0.0254" y1="0.878840625" x2="1.336040625" y2="0.889" layer="21"/>
<rectangle x1="1.77291875" y1="0.878840625" x2="2.23011875" y2="0.889" layer="21"/>
<rectangle x1="2.53491875" y1="0.878840625" x2="2.981959375" y2="0.889" layer="21"/>
<rectangle x1="3.429" y1="0.878840625" x2="3.876040625" y2="0.889" layer="21"/>
<rectangle x1="4.333240625" y1="0.878840625" x2="4.78028125" y2="0.889" layer="21"/>
<rectangle x1="5.115559375" y1="0.878840625" x2="6.477" y2="0.889" layer="21"/>
<rectangle x1="6.9342" y1="0.878840625" x2="7.37108125" y2="0.889" layer="21"/>
<rectangle x1="8.07211875" y1="0.878840625" x2="8.671559375" y2="0.889" layer="21"/>
<rectangle x1="9.50468125" y1="0.878840625" x2="9.79931875" y2="0.889" layer="21"/>
<rectangle x1="10.25651875" y1="0.878840625" x2="10.530840625" y2="0.889" layer="21"/>
<rectangle x1="10.57148125" y1="0.878840625" x2="10.754359375" y2="0.889" layer="21"/>
<rectangle x1="11.242040625" y1="0.878840625" x2="11.496040625" y2="0.889" layer="21"/>
<rectangle x1="11.89228125" y1="0.878840625" x2="12.1158" y2="0.889" layer="21"/>
<rectangle x1="12.156440625" y1="0.878840625" x2="12.33931875" y2="0.889" layer="21"/>
<rectangle x1="12.837159375" y1="0.878840625" x2="13.020040625" y2="0.889" layer="21"/>
<rectangle x1="13.497559375" y1="0.878840625" x2="13.6906" y2="0.889" layer="21"/>
<rectangle x1="14.493240625" y1="0.878840625" x2="14.67611875" y2="0.889" layer="21"/>
<rectangle x1="15.23491875" y1="0.878840625" x2="15.53971875" y2="0.889" layer="21"/>
<rectangle x1="15.99691875" y1="0.878840625" x2="16.291559375" y2="0.889" layer="21"/>
<rectangle x1="0.015240625" y1="0.889" x2="1.336040625" y2="0.899159375" layer="21"/>
<rectangle x1="1.78308125" y1="0.889" x2="2.23011875" y2="0.899159375" layer="21"/>
<rectangle x1="2.53491875" y1="0.889" x2="2.981959375" y2="0.899159375" layer="21"/>
<rectangle x1="3.429" y1="0.889" x2="3.8862" y2="0.899159375" layer="21"/>
<rectangle x1="4.333240625" y1="0.889" x2="4.78028125" y2="0.899159375" layer="21"/>
<rectangle x1="5.1054" y1="0.889" x2="6.487159375" y2="0.899159375" layer="21"/>
<rectangle x1="6.924040625" y1="0.889" x2="7.381240625" y2="0.899159375" layer="21"/>
<rectangle x1="8.07211875" y1="0.889" x2="8.671559375" y2="0.899159375" layer="21"/>
<rectangle x1="9.484359375" y1="0.889" x2="9.779" y2="0.899159375" layer="21"/>
<rectangle x1="10.276840625" y1="0.889" x2="10.541" y2="0.899159375" layer="21"/>
<rectangle x1="10.57148125" y1="0.889" x2="10.754359375" y2="0.899159375" layer="21"/>
<rectangle x1="11.23188125" y1="0.889" x2="11.48588125" y2="0.899159375" layer="21"/>
<rectangle x1="11.9126" y1="0.889" x2="12.125959375" y2="0.899159375" layer="21"/>
<rectangle x1="12.156440625" y1="0.889" x2="12.33931875" y2="0.899159375" layer="21"/>
<rectangle x1="12.827" y1="0.889" x2="13.00988125" y2="0.899159375" layer="21"/>
<rectangle x1="13.50771875" y1="0.889" x2="13.6906" y2="0.899159375" layer="21"/>
<rectangle x1="14.48308125" y1="0.889" x2="14.67611875" y2="0.899159375" layer="21"/>
<rectangle x1="15.23491875" y1="0.889" x2="15.5194" y2="0.899159375" layer="21"/>
<rectangle x1="16.017240625" y1="0.889" x2="16.31188125" y2="0.899159375" layer="21"/>
<rectangle x1="0.00508125" y1="0.899159375" x2="1.356359375" y2="0.90931875" layer="21"/>
<rectangle x1="1.77291875" y1="0.899159375" x2="2.23011875" y2="0.90931875" layer="21"/>
<rectangle x1="2.524759375" y1="0.899159375" x2="2.981959375" y2="0.90931875" layer="21"/>
<rectangle x1="3.439159375" y1="0.899159375" x2="3.8862" y2="0.90931875" layer="21"/>
<rectangle x1="4.333240625" y1="0.899159375" x2="4.78028125" y2="0.90931875" layer="21"/>
<rectangle x1="5.115559375" y1="0.899159375" x2="6.49731875" y2="0.90931875" layer="21"/>
<rectangle x1="6.9342" y1="0.899159375" x2="7.37108125" y2="0.90931875" layer="21"/>
<rectangle x1="8.07211875" y1="0.899159375" x2="8.68171875" y2="0.90931875" layer="21"/>
<rectangle x1="9.484359375" y1="0.899159375" x2="9.75868125" y2="0.90931875" layer="21"/>
<rectangle x1="10.297159375" y1="0.899159375" x2="10.551159375" y2="0.90931875" layer="21"/>
<rectangle x1="10.57148125" y1="0.899159375" x2="10.754359375" y2="0.90931875" layer="21"/>
<rectangle x1="11.23188125" y1="0.899159375" x2="11.465559375" y2="0.90931875" layer="21"/>
<rectangle x1="11.93291875" y1="0.899159375" x2="12.125959375" y2="0.90931875" layer="21"/>
<rectangle x1="12.156440625" y1="0.899159375" x2="12.33931875" y2="0.90931875" layer="21"/>
<rectangle x1="12.837159375" y1="0.899159375" x2="13.00988125" y2="0.90931875" layer="21"/>
<rectangle x1="13.50771875" y1="0.899159375" x2="13.6906" y2="0.90931875" layer="21"/>
<rectangle x1="14.493240625" y1="0.899159375" x2="14.67611875" y2="0.90931875" layer="21"/>
<rectangle x1="15.224759375" y1="0.899159375" x2="15.49908125" y2="0.90931875" layer="21"/>
<rectangle x1="16.037559375" y1="0.899159375" x2="16.322040625" y2="0.90931875" layer="21"/>
<rectangle x1="-0.00508125" y1="0.90931875" x2="1.356359375" y2="0.91948125" layer="21"/>
<rectangle x1="1.77291875" y1="0.90931875" x2="2.219959375" y2="0.91948125" layer="21"/>
<rectangle x1="2.53491875" y1="0.90931875" x2="2.981959375" y2="0.91948125" layer="21"/>
<rectangle x1="3.429" y1="0.90931875" x2="3.876040625" y2="0.91948125" layer="21"/>
<rectangle x1="4.333240625" y1="0.90931875" x2="4.78028125" y2="0.91948125" layer="21"/>
<rectangle x1="5.115559375" y1="0.90931875" x2="6.50748125" y2="0.91948125" layer="21"/>
<rectangle x1="6.924040625" y1="0.90931875" x2="7.381240625" y2="0.91948125" layer="21"/>
<rectangle x1="8.061959375" y1="0.90931875" x2="8.68171875" y2="0.91948125" layer="21"/>
<rectangle x1="9.4742" y1="0.90931875" x2="9.74851875" y2="0.91948125" layer="21"/>
<rectangle x1="10.31748125" y1="0.90931875" x2="10.551159375" y2="0.91948125" layer="21"/>
<rectangle x1="10.57148125" y1="0.90931875" x2="10.754359375" y2="0.91948125" layer="21"/>
<rectangle x1="11.22171875" y1="0.90931875" x2="11.465559375" y2="0.91948125" layer="21"/>
<rectangle x1="11.94308125" y1="0.90931875" x2="12.13611875" y2="0.91948125" layer="21"/>
<rectangle x1="12.156440625" y1="0.90931875" x2="12.33931875" y2="0.91948125" layer="21"/>
<rectangle x1="12.827" y1="0.90931875" x2="13.020040625" y2="0.91948125" layer="21"/>
<rectangle x1="13.497559375" y1="0.90931875" x2="13.6906" y2="0.91948125" layer="21"/>
<rectangle x1="14.48308125" y1="0.90931875" x2="14.67611875" y2="0.91948125" layer="21"/>
<rectangle x1="15.2146" y1="0.90931875" x2="15.48891875" y2="0.91948125" layer="21"/>
<rectangle x1="16.05788125" y1="0.90931875" x2="16.322040625" y2="0.91948125" layer="21"/>
<rectangle x1="0.00508125" y1="0.91948125" x2="1.36651875" y2="0.929640625" layer="21"/>
<rectangle x1="1.77291875" y1="0.91948125" x2="2.23011875" y2="0.929640625" layer="21"/>
<rectangle x1="2.53491875" y1="0.91948125" x2="2.981959375" y2="0.929640625" layer="21"/>
<rectangle x1="3.429" y1="0.91948125" x2="3.876040625" y2="0.929640625" layer="21"/>
<rectangle x1="4.32308125" y1="0.91948125" x2="4.78028125" y2="0.929640625" layer="21"/>
<rectangle x1="5.115559375" y1="0.91948125" x2="6.517640625" y2="0.929640625" layer="21"/>
<rectangle x1="6.924040625" y1="0.91948125" x2="7.381240625" y2="0.929640625" layer="21"/>
<rectangle x1="8.061959375" y1="0.91948125" x2="8.69188125" y2="0.929640625" layer="21"/>
<rectangle x1="9.464040625" y1="0.91948125" x2="9.7282" y2="0.929640625" layer="21"/>
<rectangle x1="10.327640625" y1="0.91948125" x2="10.56131875" y2="0.929640625" layer="21"/>
<rectangle x1="10.57148125" y1="0.91948125" x2="10.754359375" y2="0.929640625" layer="21"/>
<rectangle x1="11.22171875" y1="0.91948125" x2="11.445240625" y2="0.929640625" layer="21"/>
<rectangle x1="11.953240625" y1="0.91948125" x2="12.13611875" y2="0.929640625" layer="21"/>
<rectangle x1="12.14628125" y1="0.91948125" x2="12.33931875" y2="0.929640625" layer="21"/>
<rectangle x1="12.837159375" y1="0.91948125" x2="13.020040625" y2="0.929640625" layer="21"/>
<rectangle x1="13.497559375" y1="0.91948125" x2="13.6906" y2="0.929640625" layer="21"/>
<rectangle x1="14.48308125" y1="0.91948125" x2="14.665959375" y2="0.929640625" layer="21"/>
<rectangle x1="15.204440625" y1="0.91948125" x2="15.4686" y2="0.929640625" layer="21"/>
<rectangle x1="16.068040625" y1="0.91948125" x2="16.3322" y2="0.929640625" layer="21"/>
<rectangle x1="0.00508125" y1="0.929640625" x2="1.37668125" y2="0.9398" layer="21"/>
<rectangle x1="1.78308125" y1="0.929640625" x2="2.23011875" y2="0.9398" layer="21"/>
<rectangle x1="2.53491875" y1="0.929640625" x2="2.981959375" y2="0.9398" layer="21"/>
<rectangle x1="3.429" y1="0.929640625" x2="3.876040625" y2="0.9398" layer="21"/>
<rectangle x1="4.333240625" y1="0.929640625" x2="4.78028125" y2="0.9398" layer="21"/>
<rectangle x1="5.1054" y1="0.929640625" x2="6.517640625" y2="0.9398" layer="21"/>
<rectangle x1="6.9342" y1="0.929640625" x2="7.381240625" y2="0.9398" layer="21"/>
<rectangle x1="8.0518" y1="0.929640625" x2="8.69188125" y2="0.9398" layer="21"/>
<rectangle x1="9.45388125" y1="0.929640625" x2="9.70788125" y2="0.9398" layer="21"/>
<rectangle x1="10.347959375" y1="0.929640625" x2="10.754359375" y2="0.9398" layer="21"/>
<rectangle x1="11.211559375" y1="0.929640625" x2="11.43508125" y2="0.9398" layer="21"/>
<rectangle x1="11.973559375" y1="0.929640625" x2="12.14628125" y2="0.9398" layer="21"/>
<rectangle x1="12.156440625" y1="0.929640625" x2="12.33931875" y2="0.9398" layer="21"/>
<rectangle x1="12.827" y1="0.929640625" x2="13.00988125" y2="0.9398" layer="21"/>
<rectangle x1="13.50771875" y1="0.929640625" x2="13.6906" y2="0.9398" layer="21"/>
<rectangle x1="14.493240625" y1="0.929640625" x2="14.67611875" y2="0.9398" layer="21"/>
<rectangle x1="15.19428125" y1="0.929640625" x2="15.458440625" y2="0.9398" layer="21"/>
<rectangle x1="16.0782" y1="0.929640625" x2="16.342359375" y2="0.9398" layer="21"/>
<rectangle x1="0.015240625" y1="0.9398" x2="1.386840625" y2="0.949959375" layer="21"/>
<rectangle x1="1.77291875" y1="0.9398" x2="2.219959375" y2="0.949959375" layer="21"/>
<rectangle x1="2.53491875" y1="0.9398" x2="2.981959375" y2="0.949959375" layer="21"/>
<rectangle x1="3.429" y1="0.9398" x2="3.8862" y2="0.949959375" layer="21"/>
<rectangle x1="4.333240625" y1="0.9398" x2="4.78028125" y2="0.949959375" layer="21"/>
<rectangle x1="5.115559375" y1="0.9398" x2="6.5278" y2="0.949959375" layer="21"/>
<rectangle x1="6.9342" y1="0.9398" x2="7.37108125" y2="0.949959375" layer="21"/>
<rectangle x1="8.0518" y1="0.9398" x2="8.702040625" y2="0.949959375" layer="21"/>
<rectangle x1="9.45388125" y1="0.9398" x2="9.69771875" y2="0.949959375" layer="21"/>
<rectangle x1="10.35811875" y1="0.9398" x2="10.754359375" y2="0.949959375" layer="21"/>
<rectangle x1="11.211559375" y1="0.9398" x2="11.43508125" y2="0.949959375" layer="21"/>
<rectangle x1="11.98371875" y1="0.9398" x2="12.33931875" y2="0.949959375" layer="21"/>
<rectangle x1="12.837159375" y1="0.9398" x2="13.020040625" y2="0.949959375" layer="21"/>
<rectangle x1="13.497559375" y1="0.9398" x2="13.6906" y2="0.949959375" layer="21"/>
<rectangle x1="14.493240625" y1="0.9398" x2="14.67611875" y2="0.949959375" layer="21"/>
<rectangle x1="15.19428125" y1="0.9398" x2="15.43811875" y2="0.949959375" layer="21"/>
<rectangle x1="16.09851875" y1="0.9398" x2="16.35251875" y2="0.949959375" layer="21"/>
<rectangle x1="0.0254" y1="0.949959375" x2="1.386840625" y2="0.96011875" layer="21"/>
<rectangle x1="1.78308125" y1="0.949959375" x2="2.23011875" y2="0.96011875" layer="21"/>
<rectangle x1="2.524759375" y1="0.949959375" x2="2.981959375" y2="0.96011875" layer="21"/>
<rectangle x1="3.429" y1="0.949959375" x2="3.876040625" y2="0.96011875" layer="21"/>
<rectangle x1="4.333240625" y1="0.949959375" x2="4.78028125" y2="0.96011875" layer="21"/>
<rectangle x1="5.115559375" y1="0.949959375" x2="6.537959375" y2="0.96011875" layer="21"/>
<rectangle x1="6.924040625" y1="0.949959375" x2="7.381240625" y2="0.96011875" layer="21"/>
<rectangle x1="8.0518" y1="0.949959375" x2="8.702040625" y2="0.96011875" layer="21"/>
<rectangle x1="9.44371875" y1="0.949959375" x2="9.687559375" y2="0.96011875" layer="21"/>
<rectangle x1="10.378440625" y1="0.949959375" x2="10.754359375" y2="0.96011875" layer="21"/>
<rectangle x1="11.2014" y1="0.949959375" x2="11.42491875" y2="0.96011875" layer="21"/>
<rectangle x1="11.99388125" y1="0.949959375" x2="12.329159375" y2="0.96011875" layer="21"/>
<rectangle x1="12.827" y1="0.949959375" x2="13.00988125" y2="0.96011875" layer="21"/>
<rectangle x1="13.50771875" y1="0.949959375" x2="13.6906" y2="0.96011875" layer="21"/>
<rectangle x1="14.48308125" y1="0.949959375" x2="14.665959375" y2="0.96011875" layer="21"/>
<rectangle x1="15.18411875" y1="0.949959375" x2="15.427959375" y2="0.96011875" layer="21"/>
<rectangle x1="16.10868125" y1="0.949959375" x2="16.35251875" y2="0.96011875" layer="21"/>
<rectangle x1="0.035559375" y1="0.96011875" x2="1.397" y2="0.97028125" layer="21"/>
<rectangle x1="1.77291875" y1="0.96011875" x2="2.219959375" y2="0.97028125" layer="21"/>
<rectangle x1="2.53491875" y1="0.96011875" x2="2.981959375" y2="0.97028125" layer="21"/>
<rectangle x1="3.439159375" y1="0.96011875" x2="3.8862" y2="0.97028125" layer="21"/>
<rectangle x1="4.32308125" y1="0.96011875" x2="4.78028125" y2="0.97028125" layer="21"/>
<rectangle x1="5.115559375" y1="0.96011875" x2="6.537959375" y2="0.97028125" layer="21"/>
<rectangle x1="6.9342" y1="0.96011875" x2="7.37108125" y2="0.97028125" layer="21"/>
<rectangle x1="8.041640625" y1="0.96011875" x2="8.702040625" y2="0.97028125" layer="21"/>
<rectangle x1="9.433559375" y1="0.96011875" x2="9.6774" y2="0.97028125" layer="21"/>
<rectangle x1="10.3886" y1="0.96011875" x2="10.754359375" y2="0.97028125" layer="21"/>
<rectangle x1="11.2014" y1="0.96011875" x2="11.414759375" y2="0.97028125" layer="21"/>
<rectangle x1="12.004040625" y1="0.96011875" x2="12.33931875" y2="0.97028125" layer="21"/>
<rectangle x1="12.837159375" y1="0.96011875" x2="13.020040625" y2="0.97028125" layer="21"/>
<rectangle x1="13.50771875" y1="0.96011875" x2="13.6906" y2="0.97028125" layer="21"/>
<rectangle x1="14.493240625" y1="0.96011875" x2="14.67611875" y2="0.97028125" layer="21"/>
<rectangle x1="15.173959375" y1="0.96011875" x2="15.4178" y2="0.97028125" layer="21"/>
<rectangle x1="16.118840625" y1="0.96011875" x2="16.36268125" y2="0.97028125" layer="21"/>
<rectangle x1="0.04571875" y1="0.97028125" x2="1.407159375" y2="0.980440625" layer="21"/>
<rectangle x1="1.77291875" y1="0.97028125" x2="2.23011875" y2="0.980440625" layer="21"/>
<rectangle x1="2.53491875" y1="0.97028125" x2="2.981959375" y2="0.980440625" layer="21"/>
<rectangle x1="3.429" y1="0.97028125" x2="3.876040625" y2="0.980440625" layer="21"/>
<rectangle x1="4.333240625" y1="0.97028125" x2="4.78028125" y2="0.980440625" layer="21"/>
<rectangle x1="5.1054" y1="0.97028125" x2="6.537959375" y2="0.980440625" layer="21"/>
<rectangle x1="6.924040625" y1="0.97028125" x2="7.381240625" y2="0.980440625" layer="21"/>
<rectangle x1="8.041640625" y1="0.97028125" x2="8.7122" y2="0.980440625" layer="21"/>
<rectangle x1="9.433559375" y1="0.97028125" x2="9.667240625" y2="0.980440625" layer="21"/>
<rectangle x1="10.398759375" y1="0.97028125" x2="10.754359375" y2="0.980440625" layer="21"/>
<rectangle x1="11.2014" y1="0.97028125" x2="11.4046" y2="0.980440625" layer="21"/>
<rectangle x1="12.0142" y1="0.97028125" x2="12.33931875" y2="0.980440625" layer="21"/>
<rectangle x1="12.827" y1="0.97028125" x2="13.00988125" y2="0.980440625" layer="21"/>
<rectangle x1="13.497559375" y1="0.97028125" x2="13.6906" y2="0.980440625" layer="21"/>
<rectangle x1="14.48308125" y1="0.97028125" x2="14.67611875" y2="0.980440625" layer="21"/>
<rectangle x1="15.1638" y1="0.97028125" x2="15.407640625" y2="0.980440625" layer="21"/>
<rectangle x1="16.129" y1="0.97028125" x2="16.372840625" y2="0.980440625" layer="21"/>
<rectangle x1="0.05588125" y1="0.980440625" x2="1.407159375" y2="0.9906" layer="21"/>
<rectangle x1="1.77291875" y1="0.980440625" x2="2.219959375" y2="0.9906" layer="21"/>
<rectangle x1="2.524759375" y1="0.980440625" x2="2.981959375" y2="0.9906" layer="21"/>
<rectangle x1="3.429" y1="0.980440625" x2="3.8862" y2="0.9906" layer="21"/>
<rectangle x1="4.333240625" y1="0.980440625" x2="4.78028125" y2="0.9906" layer="21"/>
<rectangle x1="5.1054" y1="0.980440625" x2="6.54811875" y2="0.9906" layer="21"/>
<rectangle x1="6.9342" y1="0.980440625" x2="7.381240625" y2="0.9906" layer="21"/>
<rectangle x1="8.03148125" y1="0.980440625" x2="8.7122" y2="0.9906" layer="21"/>
<rectangle x1="9.4234" y1="0.980440625" x2="9.65708125" y2="0.9906" layer="21"/>
<rectangle x1="10.41908125" y1="0.980440625" x2="10.754359375" y2="0.9906" layer="21"/>
<rectangle x1="11.191240625" y1="0.980440625" x2="11.4046" y2="0.9906" layer="21"/>
<rectangle x1="12.024359375" y1="0.980440625" x2="12.33931875" y2="0.9906" layer="21"/>
<rectangle x1="12.827" y1="0.980440625" x2="13.020040625" y2="0.9906" layer="21"/>
<rectangle x1="13.497559375" y1="0.980440625" x2="13.6906" y2="0.9906" layer="21"/>
<rectangle x1="14.493240625" y1="0.980440625" x2="14.665959375" y2="0.9906" layer="21"/>
<rectangle x1="15.1638" y1="0.980440625" x2="15.39748125" y2="0.9906" layer="21"/>
<rectangle x1="16.139159375" y1="0.980440625" x2="16.372840625" y2="0.9906" layer="21"/>
<rectangle x1="0.066040625" y1="0.9906" x2="1.41731875" y2="1.000759375" layer="21"/>
<rectangle x1="1.78308125" y1="0.9906" x2="2.23011875" y2="1.000759375" layer="21"/>
<rectangle x1="2.53491875" y1="0.9906" x2="2.981959375" y2="1.000759375" layer="21"/>
<rectangle x1="3.429" y1="0.9906" x2="3.876040625" y2="1.000759375" layer="21"/>
<rectangle x1="4.333240625" y1="0.9906" x2="4.78028125" y2="1.000759375" layer="21"/>
<rectangle x1="5.115559375" y1="0.9906" x2="6.55828125" y2="1.000759375" layer="21"/>
<rectangle x1="6.924040625" y1="0.9906" x2="7.37108125" y2="1.000759375" layer="21"/>
<rectangle x1="8.03148125" y1="0.9906" x2="8.722359375" y2="1.000759375" layer="21"/>
<rectangle x1="9.413240625" y1="0.9906" x2="9.64691875" y2="1.000759375" layer="21"/>
<rectangle x1="10.41908125" y1="0.9906" x2="10.754359375" y2="1.000759375" layer="21"/>
<rectangle x1="11.191240625" y1="0.9906" x2="11.394440625" y2="1.000759375" layer="21"/>
<rectangle x1="12.03451875" y1="0.9906" x2="12.329159375" y2="1.000759375" layer="21"/>
<rectangle x1="12.837159375" y1="0.9906" x2="13.020040625" y2="1.000759375" layer="21"/>
<rectangle x1="13.50771875" y1="0.9906" x2="13.6906" y2="1.000759375" layer="21"/>
<rectangle x1="14.48308125" y1="0.9906" x2="14.67611875" y2="1.000759375" layer="21"/>
<rectangle x1="15.153640625" y1="0.9906" x2="15.38731875" y2="1.000759375" layer="21"/>
<rectangle x1="16.15948125" y1="0.9906" x2="16.383" y2="1.000759375" layer="21"/>
<rectangle x1="0.0762" y1="1.000759375" x2="1.41731875" y2="1.01091875" layer="21"/>
<rectangle x1="1.77291875" y1="1.000759375" x2="2.23011875" y2="1.01091875" layer="21"/>
<rectangle x1="2.53491875" y1="1.000759375" x2="2.981959375" y2="1.01091875" layer="21"/>
<rectangle x1="3.429" y1="1.000759375" x2="3.876040625" y2="1.01091875" layer="21"/>
<rectangle x1="4.333240625" y1="1.000759375" x2="4.78028125" y2="1.01091875" layer="21"/>
<rectangle x1="5.115559375" y1="1.000759375" x2="6.568440625" y2="1.01091875" layer="21"/>
<rectangle x1="6.9342" y1="1.000759375" x2="7.381240625" y2="1.01091875" layer="21"/>
<rectangle x1="8.02131875" y1="1.000759375" x2="8.722359375" y2="1.01091875" layer="21"/>
<rectangle x1="9.413240625" y1="1.000759375" x2="9.636759375" y2="1.01091875" layer="21"/>
<rectangle x1="10.429240625" y1="1.000759375" x2="10.754359375" y2="1.01091875" layer="21"/>
<rectangle x1="11.191240625" y1="1.000759375" x2="11.394440625" y2="1.01091875" layer="21"/>
<rectangle x1="12.03451875" y1="1.000759375" x2="12.33931875" y2="1.01091875" layer="21"/>
<rectangle x1="12.827" y1="1.000759375" x2="13.020040625" y2="1.01091875" layer="21"/>
<rectangle x1="13.497559375" y1="1.000759375" x2="13.6906" y2="1.01091875" layer="21"/>
<rectangle x1="14.493240625" y1="1.000759375" x2="14.67611875" y2="1.01091875" layer="21"/>
<rectangle x1="15.153640625" y1="1.000759375" x2="15.377159375" y2="1.01091875" layer="21"/>
<rectangle x1="16.15948125" y1="1.000759375" x2="16.393159375" y2="1.01091875" layer="21"/>
<rectangle x1="0.086359375" y1="1.01091875" x2="1.42748125" y2="1.02108125" layer="21"/>
<rectangle x1="1.78308125" y1="1.01091875" x2="2.219959375" y2="1.02108125" layer="21"/>
<rectangle x1="2.53491875" y1="1.01091875" x2="2.981959375" y2="1.02108125" layer="21"/>
<rectangle x1="3.429" y1="1.01091875" x2="3.8862" y2="1.02108125" layer="21"/>
<rectangle x1="4.333240625" y1="1.01091875" x2="4.78028125" y2="1.02108125" layer="21"/>
<rectangle x1="5.115559375" y1="1.01091875" x2="6.568440625" y2="1.02108125" layer="21"/>
<rectangle x1="6.924040625" y1="1.01091875" x2="7.381240625" y2="1.02108125" layer="21"/>
<rectangle x1="8.02131875" y1="1.01091875" x2="8.722359375" y2="1.02108125" layer="21"/>
<rectangle x1="9.40308125" y1="1.01091875" x2="9.6266" y2="1.02108125" layer="21"/>
<rectangle x1="10.4394" y1="1.01091875" x2="10.754359375" y2="1.02108125" layer="21"/>
<rectangle x1="11.191240625" y1="1.01091875" x2="11.394440625" y2="1.02108125" layer="21"/>
<rectangle x1="12.04468125" y1="1.01091875" x2="12.329159375" y2="1.02108125" layer="21"/>
<rectangle x1="12.837159375" y1="1.01091875" x2="13.00988125" y2="1.02108125" layer="21"/>
<rectangle x1="13.50771875" y1="1.01091875" x2="13.6906" y2="1.02108125" layer="21"/>
<rectangle x1="14.48308125" y1="1.01091875" x2="14.665959375" y2="1.02108125" layer="21"/>
<rectangle x1="15.14348125" y1="1.01091875" x2="15.367" y2="1.02108125" layer="21"/>
<rectangle x1="16.169640625" y1="1.01091875" x2="16.393159375" y2="1.02108125" layer="21"/>
<rectangle x1="0.09651875" y1="1.02108125" x2="1.437640625" y2="1.031240625" layer="21"/>
<rectangle x1="1.77291875" y1="1.02108125" x2="2.23011875" y2="1.031240625" layer="21"/>
<rectangle x1="2.53491875" y1="1.02108125" x2="2.981959375" y2="1.031240625" layer="21"/>
<rectangle x1="3.439159375" y1="1.02108125" x2="3.8862" y2="1.031240625" layer="21"/>
<rectangle x1="4.333240625" y1="1.02108125" x2="4.78028125" y2="1.031240625" layer="21"/>
<rectangle x1="5.1054" y1="1.02108125" x2="6.5786" y2="1.031240625" layer="21"/>
<rectangle x1="6.9342" y1="1.02108125" x2="7.381240625" y2="1.031240625" layer="21"/>
<rectangle x1="8.02131875" y1="1.02108125" x2="8.73251875" y2="1.031240625" layer="21"/>
<rectangle x1="9.39291875" y1="1.02108125" x2="9.616440625" y2="1.031240625" layer="21"/>
<rectangle x1="10.449559375" y1="1.02108125" x2="10.754359375" y2="1.031240625" layer="21"/>
<rectangle x1="11.18108125" y1="1.02108125" x2="11.38428125" y2="1.031240625" layer="21"/>
<rectangle x1="12.054840625" y1="1.02108125" x2="12.33931875" y2="1.031240625" layer="21"/>
<rectangle x1="12.827" y1="1.02108125" x2="13.00988125" y2="1.031240625" layer="21"/>
<rectangle x1="13.50771875" y1="1.02108125" x2="13.6906" y2="1.031240625" layer="21"/>
<rectangle x1="14.493240625" y1="1.02108125" x2="14.67611875" y2="1.031240625" layer="21"/>
<rectangle x1="15.14348125" y1="1.02108125" x2="15.356840625" y2="1.031240625" layer="21"/>
<rectangle x1="16.1798" y1="1.02108125" x2="16.40331875" y2="1.031240625" layer="21"/>
<rectangle x1="0.09651875" y1="1.031240625" x2="1.437640625" y2="1.0414" layer="21"/>
<rectangle x1="1.77291875" y1="1.031240625" x2="2.219959375" y2="1.0414" layer="21"/>
<rectangle x1="2.524759375" y1="1.031240625" x2="2.981959375" y2="1.0414" layer="21"/>
<rectangle x1="3.429" y1="1.031240625" x2="3.876040625" y2="1.0414" layer="21"/>
<rectangle x1="4.333240625" y1="1.031240625" x2="4.78028125" y2="1.0414" layer="21"/>
<rectangle x1="5.115559375" y1="1.031240625" x2="6.5786" y2="1.0414" layer="21"/>
<rectangle x1="6.924040625" y1="1.031240625" x2="7.37108125" y2="1.0414" layer="21"/>
<rectangle x1="8.011159375" y1="1.031240625" x2="8.73251875" y2="1.0414" layer="21"/>
<rectangle x1="9.39291875" y1="1.031240625" x2="9.60628125" y2="1.0414" layer="21"/>
<rectangle x1="10.45971875" y1="1.031240625" x2="10.754359375" y2="1.0414" layer="21"/>
<rectangle x1="11.18108125" y1="1.031240625" x2="11.38428125" y2="1.0414" layer="21"/>
<rectangle x1="12.054840625" y1="1.031240625" x2="12.329159375" y2="1.0414" layer="21"/>
<rectangle x1="12.837159375" y1="1.031240625" x2="13.020040625" y2="1.0414" layer="21"/>
<rectangle x1="13.497559375" y1="1.031240625" x2="13.680440625" y2="1.0414" layer="21"/>
<rectangle x1="14.48308125" y1="1.031240625" x2="14.67611875" y2="1.0414" layer="21"/>
<rectangle x1="15.13331875" y1="1.031240625" x2="15.34668125" y2="1.0414" layer="21"/>
<rectangle x1="16.189959375" y1="1.031240625" x2="16.40331875" y2="1.0414" layer="21"/>
<rectangle x1="0.10668125" y1="1.0414" x2="1.437640625" y2="1.051559375" layer="21"/>
<rectangle x1="1.77291875" y1="1.0414" x2="2.23011875" y2="1.051559375" layer="21"/>
<rectangle x1="2.53491875" y1="1.0414" x2="2.981959375" y2="1.051559375" layer="21"/>
<rectangle x1="3.429" y1="1.0414" x2="3.876040625" y2="1.051559375" layer="21"/>
<rectangle x1="4.333240625" y1="1.0414" x2="4.78028125" y2="1.051559375" layer="21"/>
<rectangle x1="5.115559375" y1="1.0414" x2="6.588759375" y2="1.051559375" layer="21"/>
<rectangle x1="6.9342" y1="1.0414" x2="7.381240625" y2="1.051559375" layer="21"/>
<rectangle x1="8.011159375" y1="1.0414" x2="8.73251875" y2="1.051559375" layer="21"/>
<rectangle x1="9.39291875" y1="1.0414" x2="9.59611875" y2="1.051559375" layer="21"/>
<rectangle x1="10.46988125" y1="1.0414" x2="10.754359375" y2="1.051559375" layer="21"/>
<rectangle x1="11.18108125" y1="1.0414" x2="11.37411875" y2="1.051559375" layer="21"/>
<rectangle x1="12.065" y1="1.0414" x2="12.329159375" y2="1.051559375" layer="21"/>
<rectangle x1="12.827" y1="1.0414" x2="13.020040625" y2="1.051559375" layer="21"/>
<rectangle x1="13.497559375" y1="1.0414" x2="13.6906" y2="1.051559375" layer="21"/>
<rectangle x1="14.493240625" y1="1.0414" x2="14.665959375" y2="1.051559375" layer="21"/>
<rectangle x1="15.123159375" y1="1.0414" x2="15.34668125" y2="1.051559375" layer="21"/>
<rectangle x1="16.20011875" y1="1.0414" x2="16.41348125" y2="1.051559375" layer="21"/>
<rectangle x1="0.116840625" y1="1.051559375" x2="1.4478" y2="1.06171875" layer="21"/>
<rectangle x1="1.78308125" y1="1.051559375" x2="2.23011875" y2="1.06171875" layer="21"/>
<rectangle x1="2.53491875" y1="1.051559375" x2="2.981959375" y2="1.06171875" layer="21"/>
<rectangle x1="3.429" y1="1.051559375" x2="3.876040625" y2="1.06171875" layer="21"/>
<rectangle x1="4.333240625" y1="1.051559375" x2="4.78028125" y2="1.06171875" layer="21"/>
<rectangle x1="5.115559375" y1="1.051559375" x2="6.588759375" y2="1.06171875" layer="21"/>
<rectangle x1="6.924040625" y1="1.051559375" x2="7.381240625" y2="1.06171875" layer="21"/>
<rectangle x1="8.001" y1="1.051559375" x2="8.73251875" y2="1.06171875" layer="21"/>
<rectangle x1="9.382759375" y1="1.051559375" x2="9.59611875" y2="1.06171875" layer="21"/>
<rectangle x1="10.480040625" y1="1.051559375" x2="10.754359375" y2="1.06171875" layer="21"/>
<rectangle x1="11.18108125" y1="1.051559375" x2="11.37411875" y2="1.06171875" layer="21"/>
<rectangle x1="12.065" y1="1.051559375" x2="12.33931875" y2="1.06171875" layer="21"/>
<rectangle x1="12.827" y1="1.051559375" x2="13.00988125" y2="1.06171875" layer="21"/>
<rectangle x1="13.50771875" y1="1.051559375" x2="13.6906" y2="1.06171875" layer="21"/>
<rectangle x1="14.48308125" y1="1.051559375" x2="14.67611875" y2="1.06171875" layer="21"/>
<rectangle x1="15.123159375" y1="1.051559375" x2="15.33651875" y2="1.06171875" layer="21"/>
<rectangle x1="16.20011875" y1="1.051559375" x2="16.41348125" y2="1.06171875" layer="21"/>
<rectangle x1="0.127" y1="1.06171875" x2="1.4478" y2="1.07188125" layer="21"/>
<rectangle x1="1.77291875" y1="1.06171875" x2="2.23011875" y2="1.07188125" layer="21"/>
<rectangle x1="2.524759375" y1="1.06171875" x2="2.981959375" y2="1.07188125" layer="21"/>
<rectangle x1="3.429" y1="1.06171875" x2="3.8862" y2="1.07188125" layer="21"/>
<rectangle x1="4.32308125" y1="1.06171875" x2="4.78028125" y2="1.07188125" layer="21"/>
<rectangle x1="5.115559375" y1="1.06171875" x2="5.83691875" y2="1.07188125" layer="21"/>
<rectangle x1="5.857240625" y1="1.06171875" x2="5.8674" y2="1.07188125" layer="21"/>
<rectangle x1="5.88771875" y1="1.06171875" x2="5.89788125" y2="1.07188125" layer="21"/>
<rectangle x1="5.9182" y1="1.06171875" x2="6.588759375" y2="1.07188125" layer="21"/>
<rectangle x1="6.9342" y1="1.06171875" x2="7.37108125" y2="1.07188125" layer="21"/>
<rectangle x1="8.001" y1="1.06171875" x2="8.74268125" y2="1.07188125" layer="21"/>
<rectangle x1="9.382759375" y1="1.06171875" x2="9.585959375" y2="1.07188125" layer="21"/>
<rectangle x1="10.480040625" y1="1.06171875" x2="10.754359375" y2="1.07188125" layer="21"/>
<rectangle x1="11.18108125" y1="1.06171875" x2="11.37411875" y2="1.07188125" layer="21"/>
<rectangle x1="12.075159375" y1="1.06171875" x2="12.329159375" y2="1.07188125" layer="21"/>
<rectangle x1="12.827" y1="1.06171875" x2="13.020040625" y2="1.07188125" layer="21"/>
<rectangle x1="13.497559375" y1="1.06171875" x2="13.6906" y2="1.07188125" layer="21"/>
<rectangle x1="14.493240625" y1="1.06171875" x2="14.67611875" y2="1.07188125" layer="21"/>
<rectangle x1="15.123159375" y1="1.06171875" x2="15.326359375" y2="1.07188125" layer="21"/>
<rectangle x1="16.21028125" y1="1.06171875" x2="16.423640625" y2="1.07188125" layer="21"/>
<rectangle x1="0.137159375" y1="1.07188125" x2="1.457959375" y2="1.082040625" layer="21"/>
<rectangle x1="1.78308125" y1="1.07188125" x2="2.219959375" y2="1.082040625" layer="21"/>
<rectangle x1="2.53491875" y1="1.07188125" x2="2.981959375" y2="1.082040625" layer="21"/>
<rectangle x1="3.429" y1="1.07188125" x2="3.876040625" y2="1.082040625" layer="21"/>
<rectangle x1="4.333240625" y1="1.07188125" x2="4.78028125" y2="1.082040625" layer="21"/>
<rectangle x1="5.115559375" y1="1.07188125" x2="5.775959375" y2="1.082040625" layer="21"/>
<rectangle x1="5.979159375" y1="1.07188125" x2="6.59891875" y2="1.082040625" layer="21"/>
<rectangle x1="6.924040625" y1="1.07188125" x2="7.381240625" y2="1.082040625" layer="21"/>
<rectangle x1="7.990840625" y1="1.07188125" x2="8.752840625" y2="1.082040625" layer="21"/>
<rectangle x1="9.3726" y1="1.07188125" x2="9.585959375" y2="1.082040625" layer="21"/>
<rectangle x1="10.4902" y1="1.07188125" x2="10.754359375" y2="1.082040625" layer="21"/>
<rectangle x1="11.18108125" y1="1.07188125" x2="11.363959375" y2="1.082040625" layer="21"/>
<rectangle x1="12.075159375" y1="1.07188125" x2="12.329159375" y2="1.082040625" layer="21"/>
<rectangle x1="12.837159375" y1="1.07188125" x2="13.00988125" y2="1.082040625" layer="21"/>
<rectangle x1="13.50771875" y1="1.07188125" x2="13.6906" y2="1.082040625" layer="21"/>
<rectangle x1="14.48308125" y1="1.07188125" x2="14.665959375" y2="1.082040625" layer="21"/>
<rectangle x1="15.113" y1="1.07188125" x2="15.326359375" y2="1.082040625" layer="21"/>
<rectangle x1="16.220440625" y1="1.07188125" x2="16.423640625" y2="1.082040625" layer="21"/>
<rectangle x1="0.14731875" y1="1.082040625" x2="0.65531875" y2="1.0922" layer="21"/>
<rectangle x1="0.76708125" y1="1.082040625" x2="0.777240625" y2="1.0922" layer="21"/>
<rectangle x1="0.7874" y1="1.082040625" x2="1.457959375" y2="1.0922" layer="21"/>
<rectangle x1="1.77291875" y1="1.082040625" x2="2.23011875" y2="1.0922" layer="21"/>
<rectangle x1="2.53491875" y1="1.082040625" x2="2.981959375" y2="1.0922" layer="21"/>
<rectangle x1="3.439159375" y1="1.082040625" x2="3.8862" y2="1.0922" layer="21"/>
<rectangle x1="4.333240625" y1="1.082040625" x2="4.78028125" y2="1.0922" layer="21"/>
<rectangle x1="5.115559375" y1="1.082040625" x2="5.73531875" y2="1.0922" layer="21"/>
<rectangle x1="6.009640625" y1="1.082040625" x2="6.59891875" y2="1.0922" layer="21"/>
<rectangle x1="6.9342" y1="1.082040625" x2="7.37108125" y2="1.0922" layer="21"/>
<rectangle x1="7.990840625" y1="1.082040625" x2="8.752840625" y2="1.0922" layer="21"/>
<rectangle x1="9.3726" y1="1.082040625" x2="9.5758" y2="1.0922" layer="21"/>
<rectangle x1="10.500359375" y1="1.082040625" x2="10.754359375" y2="1.0922" layer="21"/>
<rectangle x1="11.17091875" y1="1.082040625" x2="11.37411875" y2="1.0922" layer="21"/>
<rectangle x1="12.08531875" y1="1.082040625" x2="12.329159375" y2="1.0922" layer="21"/>
<rectangle x1="12.827" y1="1.082040625" x2="13.020040625" y2="1.0922" layer="21"/>
<rectangle x1="13.50771875" y1="1.082040625" x2="13.6906" y2="1.0922" layer="21"/>
<rectangle x1="14.493240625" y1="1.082040625" x2="14.67611875" y2="1.0922" layer="21"/>
<rectangle x1="15.113" y1="1.082040625" x2="15.3162" y2="1.0922" layer="21"/>
<rectangle x1="16.220440625" y1="1.082040625" x2="16.423640625" y2="1.0922" layer="21"/>
<rectangle x1="0.15748125" y1="1.0922" x2="0.60451875" y2="1.102359375" layer="21"/>
<rectangle x1="0.8382" y1="1.0922" x2="1.46811875" y2="1.102359375" layer="21"/>
<rectangle x1="1.77291875" y1="1.0922" x2="2.23011875" y2="1.102359375" layer="21"/>
<rectangle x1="2.53491875" y1="1.0922" x2="2.981959375" y2="1.102359375" layer="21"/>
<rectangle x1="3.429" y1="1.0922" x2="3.876040625" y2="1.102359375" layer="21"/>
<rectangle x1="4.333240625" y1="1.0922" x2="4.78028125" y2="1.102359375" layer="21"/>
<rectangle x1="5.1054" y1="1.0922" x2="5.704840625" y2="1.102359375" layer="21"/>
<rectangle x1="6.04011875" y1="1.0922" x2="6.60908125" y2="1.102359375" layer="21"/>
<rectangle x1="6.924040625" y1="1.0922" x2="7.381240625" y2="1.102359375" layer="21"/>
<rectangle x1="7.990840625" y1="1.0922" x2="8.752840625" y2="1.102359375" layer="21"/>
<rectangle x1="9.362440625" y1="1.0922" x2="9.565640625" y2="1.102359375" layer="21"/>
<rectangle x1="10.500359375" y1="1.0922" x2="10.754359375" y2="1.102359375" layer="21"/>
<rectangle x1="11.17091875" y1="1.0922" x2="11.363959375" y2="1.102359375" layer="21"/>
<rectangle x1="12.09548125" y1="1.0922" x2="12.329159375" y2="1.102359375" layer="21"/>
<rectangle x1="12.837159375" y1="1.0922" x2="13.00988125" y2="1.102359375" layer="21"/>
<rectangle x1="13.497559375" y1="1.0922" x2="13.6906" y2="1.102359375" layer="21"/>
<rectangle x1="14.48308125" y1="1.0922" x2="14.67611875" y2="1.102359375" layer="21"/>
<rectangle x1="15.102840625" y1="1.0922" x2="15.306040625" y2="1.102359375" layer="21"/>
<rectangle x1="16.2306" y1="1.0922" x2="16.4338" y2="1.102359375" layer="21"/>
<rectangle x1="0.167640625" y1="1.102359375" x2="0.574040625" y2="1.11251875" layer="21"/>
<rectangle x1="0.86868125" y1="1.102359375" x2="1.46811875" y2="1.11251875" layer="21"/>
<rectangle x1="1.77291875" y1="1.102359375" x2="2.219959375" y2="1.11251875" layer="21"/>
<rectangle x1="2.53491875" y1="1.102359375" x2="2.981959375" y2="1.11251875" layer="21"/>
<rectangle x1="3.429" y1="1.102359375" x2="3.8862" y2="1.11251875" layer="21"/>
<rectangle x1="4.333240625" y1="1.102359375" x2="4.78028125" y2="1.11251875" layer="21"/>
<rectangle x1="5.115559375" y1="1.102359375" x2="5.69468125" y2="1.11251875" layer="21"/>
<rectangle x1="6.060440625" y1="1.102359375" x2="6.60908125" y2="1.11251875" layer="21"/>
<rectangle x1="6.924040625" y1="1.102359375" x2="7.381240625" y2="1.11251875" layer="21"/>
<rectangle x1="7.98068125" y1="1.102359375" x2="8.763" y2="1.11251875" layer="21"/>
<rectangle x1="9.362440625" y1="1.102359375" x2="9.565640625" y2="1.11251875" layer="21"/>
<rectangle x1="10.51051875" y1="1.102359375" x2="10.754359375" y2="1.11251875" layer="21"/>
<rectangle x1="11.17091875" y1="1.102359375" x2="11.363959375" y2="1.11251875" layer="21"/>
<rectangle x1="12.09548125" y1="1.102359375" x2="12.329159375" y2="1.11251875" layer="21"/>
<rectangle x1="12.827" y1="1.102359375" x2="13.020040625" y2="1.11251875" layer="21"/>
<rectangle x1="13.497559375" y1="1.102359375" x2="13.6906" y2="1.11251875" layer="21"/>
<rectangle x1="14.493240625" y1="1.102359375" x2="14.67611875" y2="1.11251875" layer="21"/>
<rectangle x1="15.102840625" y1="1.102359375" x2="15.306040625" y2="1.11251875" layer="21"/>
<rectangle x1="16.2306" y1="1.102359375" x2="16.4338" y2="1.11251875" layer="21"/>
<rectangle x1="0.1778" y1="1.11251875" x2="0.543559375" y2="1.12268125" layer="21"/>
<rectangle x1="0.899159375" y1="1.11251875" x2="1.46811875" y2="1.12268125" layer="21"/>
<rectangle x1="1.78308125" y1="1.11251875" x2="2.23011875" y2="1.12268125" layer="21"/>
<rectangle x1="2.524759375" y1="1.11251875" x2="2.981959375" y2="1.12268125" layer="21"/>
<rectangle x1="3.429" y1="1.11251875" x2="3.876040625" y2="1.12268125" layer="21"/>
<rectangle x1="4.333240625" y1="1.11251875" x2="4.78028125" y2="1.12268125" layer="21"/>
<rectangle x1="5.115559375" y1="1.11251875" x2="5.674359375" y2="1.12268125" layer="21"/>
<rectangle x1="6.0706" y1="1.11251875" x2="6.619240625" y2="1.12268125" layer="21"/>
<rectangle x1="6.9342" y1="1.11251875" x2="7.381240625" y2="1.12268125" layer="21"/>
<rectangle x1="7.98068125" y1="1.11251875" x2="8.763" y2="1.12268125" layer="21"/>
<rectangle x1="9.35228125" y1="1.11251875" x2="9.55548125" y2="1.12268125" layer="21"/>
<rectangle x1="10.52068125" y1="1.11251875" x2="10.754359375" y2="1.12268125" layer="21"/>
<rectangle x1="11.17091875" y1="1.11251875" x2="11.363959375" y2="1.12268125" layer="21"/>
<rectangle x1="12.09548125" y1="1.11251875" x2="12.329159375" y2="1.12268125" layer="21"/>
<rectangle x1="12.827" y1="1.11251875" x2="13.020040625" y2="1.12268125" layer="21"/>
<rectangle x1="13.50771875" y1="1.11251875" x2="13.680440625" y2="1.12268125" layer="21"/>
<rectangle x1="14.48308125" y1="1.11251875" x2="14.67611875" y2="1.12268125" layer="21"/>
<rectangle x1="15.09268125" y1="1.11251875" x2="15.29588125" y2="1.12268125" layer="21"/>
<rectangle x1="16.240759375" y1="1.11251875" x2="16.443959375" y2="1.12268125" layer="21"/>
<rectangle x1="0.187959375" y1="1.12268125" x2="0.51308125" y2="1.132840625" layer="21"/>
<rectangle x1="0.91948125" y1="1.12268125" x2="1.46811875" y2="1.132840625" layer="21"/>
<rectangle x1="1.77291875" y1="1.12268125" x2="2.219959375" y2="1.132840625" layer="21"/>
<rectangle x1="2.53491875" y1="1.12268125" x2="2.981959375" y2="1.132840625" layer="21"/>
<rectangle x1="3.429" y1="1.12268125" x2="3.876040625" y2="1.132840625" layer="21"/>
<rectangle x1="4.333240625" y1="1.12268125" x2="4.78028125" y2="1.132840625" layer="21"/>
<rectangle x1="5.115559375" y1="1.12268125" x2="5.6642" y2="1.132840625" layer="21"/>
<rectangle x1="6.09091875" y1="1.12268125" x2="6.619240625" y2="1.132840625" layer="21"/>
<rectangle x1="6.9342" y1="1.12268125" x2="7.37108125" y2="1.132840625" layer="21"/>
<rectangle x1="7.97051875" y1="1.12268125" x2="8.763" y2="1.132840625" layer="21"/>
<rectangle x1="9.35228125" y1="1.12268125" x2="9.55548125" y2="1.132840625" layer="21"/>
<rectangle x1="10.52068125" y1="1.12268125" x2="10.754359375" y2="1.132840625" layer="21"/>
<rectangle x1="11.17091875" y1="1.12268125" x2="11.363959375" y2="1.132840625" layer="21"/>
<rectangle x1="12.105640625" y1="1.12268125" x2="12.329159375" y2="1.132840625" layer="21"/>
<rectangle x1="12.827" y1="1.12268125" x2="13.020040625" y2="1.132840625" layer="21"/>
<rectangle x1="13.497559375" y1="1.12268125" x2="13.6906" y2="1.132840625" layer="21"/>
<rectangle x1="14.493240625" y1="1.12268125" x2="14.665959375" y2="1.132840625" layer="21"/>
<rectangle x1="15.09268125" y1="1.12268125" x2="15.29588125" y2="1.132840625" layer="21"/>
<rectangle x1="16.240759375" y1="1.12268125" x2="16.443959375" y2="1.132840625" layer="21"/>
<rectangle x1="0.19811875" y1="1.132840625" x2="0.50291875" y2="1.143" layer="21"/>
<rectangle x1="0.9398" y1="1.132840625" x2="1.47828125" y2="1.143" layer="21"/>
<rectangle x1="1.78308125" y1="1.132840625" x2="2.23011875" y2="1.143" layer="21"/>
<rectangle x1="2.53491875" y1="1.132840625" x2="2.981959375" y2="1.143" layer="21"/>
<rectangle x1="3.439159375" y1="1.132840625" x2="3.8862" y2="1.143" layer="21"/>
<rectangle x1="4.333240625" y1="1.132840625" x2="4.78028125" y2="1.143" layer="21"/>
<rectangle x1="5.1054" y1="1.132840625" x2="5.64388125" y2="1.143" layer="21"/>
<rectangle x1="6.10108125" y1="1.132840625" x2="6.619240625" y2="1.143" layer="21"/>
<rectangle x1="6.924040625" y1="1.132840625" x2="7.381240625" y2="1.143" layer="21"/>
<rectangle x1="7.97051875" y1="1.132840625" x2="8.773159375" y2="1.143" layer="21"/>
<rectangle x1="9.35228125" y1="1.132840625" x2="9.54531875" y2="1.143" layer="21"/>
<rectangle x1="10.530840625" y1="1.132840625" x2="10.754359375" y2="1.143" layer="21"/>
<rectangle x1="11.17091875" y1="1.132840625" x2="11.3538" y2="1.143" layer="21"/>
<rectangle x1="12.105640625" y1="1.132840625" x2="12.329159375" y2="1.143" layer="21"/>
<rectangle x1="12.837159375" y1="1.132840625" x2="13.00988125" y2="1.143" layer="21"/>
<rectangle x1="13.50771875" y1="1.132840625" x2="13.6906" y2="1.143" layer="21"/>
<rectangle x1="14.48308125" y1="1.132840625" x2="14.67611875" y2="1.143" layer="21"/>
<rectangle x1="15.09268125" y1="1.132840625" x2="15.28571875" y2="1.143" layer="21"/>
<rectangle x1="16.25091875" y1="1.132840625" x2="16.443959375" y2="1.143" layer="21"/>
<rectangle x1="0.19811875" y1="1.143" x2="0.4826" y2="1.153159375" layer="21"/>
<rectangle x1="0.949959375" y1="1.143" x2="1.47828125" y2="1.153159375" layer="21"/>
<rectangle x1="1.78308125" y1="1.143" x2="2.23011875" y2="1.153159375" layer="21"/>
<rectangle x1="2.524759375" y1="1.143" x2="2.981959375" y2="1.153159375" layer="21"/>
<rectangle x1="3.429" y1="1.143" x2="3.8862" y2="1.153159375" layer="21"/>
<rectangle x1="4.333240625" y1="1.143" x2="4.78028125" y2="1.153159375" layer="21"/>
<rectangle x1="5.115559375" y1="1.143" x2="5.63371875" y2="1.153159375" layer="21"/>
<rectangle x1="6.111240625" y1="1.143" x2="6.6294" y2="1.153159375" layer="21"/>
<rectangle x1="6.9342" y1="1.143" x2="7.37108125" y2="1.153159375" layer="21"/>
<rectangle x1="7.960359375" y1="1.143" x2="8.773159375" y2="1.153159375" layer="21"/>
<rectangle x1="9.35228125" y1="1.143" x2="9.54531875" y2="1.153159375" layer="21"/>
<rectangle x1="10.530840625" y1="1.143" x2="10.754359375" y2="1.153159375" layer="21"/>
<rectangle x1="11.160759375" y1="1.143" x2="11.3538" y2="1.153159375" layer="21"/>
<rectangle x1="12.1158" y1="1.143" x2="12.329159375" y2="1.153159375" layer="21"/>
<rectangle x1="12.827" y1="1.143" x2="13.00988125" y2="1.153159375" layer="21"/>
<rectangle x1="13.497559375" y1="1.143" x2="13.6906" y2="1.153159375" layer="21"/>
<rectangle x1="14.48308125" y1="1.143" x2="14.67611875" y2="1.153159375" layer="21"/>
<rectangle x1="15.08251875" y1="1.143" x2="15.28571875" y2="1.153159375" layer="21"/>
<rectangle x1="16.25091875" y1="1.143" x2="16.45411875" y2="1.153159375" layer="21"/>
<rectangle x1="0.20828125" y1="1.153159375" x2="0.46228125" y2="1.16331875" layer="21"/>
<rectangle x1="0.97028125" y1="1.153159375" x2="1.47828125" y2="1.16331875" layer="21"/>
<rectangle x1="1.77291875" y1="1.153159375" x2="2.219959375" y2="1.16331875" layer="21"/>
<rectangle x1="2.53491875" y1="1.153159375" x2="2.981959375" y2="1.16331875" layer="21"/>
<rectangle x1="3.429" y1="1.153159375" x2="3.876040625" y2="1.16331875" layer="21"/>
<rectangle x1="4.333240625" y1="1.153159375" x2="4.78028125" y2="1.16331875" layer="21"/>
<rectangle x1="5.115559375" y1="1.153159375" x2="5.623559375" y2="1.16331875" layer="21"/>
<rectangle x1="6.1214" y1="1.153159375" x2="6.6294" y2="1.16331875" layer="21"/>
<rectangle x1="6.924040625" y1="1.153159375" x2="7.381240625" y2="1.16331875" layer="21"/>
<rectangle x1="7.960359375" y1="1.153159375" x2="8.78331875" y2="1.16331875" layer="21"/>
<rectangle x1="9.34211875" y1="1.153159375" x2="9.54531875" y2="1.16331875" layer="21"/>
<rectangle x1="10.541" y1="1.153159375" x2="10.754359375" y2="1.16331875" layer="21"/>
<rectangle x1="11.17091875" y1="1.153159375" x2="11.3538" y2="1.16331875" layer="21"/>
<rectangle x1="12.1158" y1="1.153159375" x2="12.329159375" y2="1.16331875" layer="21"/>
<rectangle x1="12.837159375" y1="1.153159375" x2="13.020040625" y2="1.16331875" layer="21"/>
<rectangle x1="13.50771875" y1="1.153159375" x2="13.6906" y2="1.16331875" layer="21"/>
<rectangle x1="14.493240625" y1="1.153159375" x2="14.665959375" y2="1.16331875" layer="21"/>
<rectangle x1="15.08251875" y1="1.153159375" x2="15.28571875" y2="1.16331875" layer="21"/>
<rectangle x1="16.26108125" y1="1.153159375" x2="16.45411875" y2="1.16331875" layer="21"/>
<rectangle x1="0.218440625" y1="1.16331875" x2="0.441959375" y2="1.17348125" layer="21"/>
<rectangle x1="0.980440625" y1="1.16331875" x2="1.47828125" y2="1.17348125" layer="21"/>
<rectangle x1="1.78308125" y1="1.16331875" x2="2.23011875" y2="1.17348125" layer="21"/>
<rectangle x1="2.53491875" y1="1.16331875" x2="2.981959375" y2="1.17348125" layer="21"/>
<rectangle x1="3.429" y1="1.16331875" x2="3.876040625" y2="1.17348125" layer="21"/>
<rectangle x1="4.333240625" y1="1.16331875" x2="4.78028125" y2="1.17348125" layer="21"/>
<rectangle x1="5.115559375" y1="1.16331875" x2="5.6134" y2="1.17348125" layer="21"/>
<rectangle x1="6.131559375" y1="1.16331875" x2="6.6294" y2="1.17348125" layer="21"/>
<rectangle x1="6.924040625" y1="1.16331875" x2="7.381240625" y2="1.17348125" layer="21"/>
<rectangle x1="7.9502" y1="1.16331875" x2="8.78331875" y2="1.17348125" layer="21"/>
<rectangle x1="9.34211875" y1="1.16331875" x2="9.535159375" y2="1.17348125" layer="21"/>
<rectangle x1="10.541" y1="1.16331875" x2="10.754359375" y2="1.17348125" layer="21"/>
<rectangle x1="11.160759375" y1="1.16331875" x2="11.3538" y2="1.17348125" layer="21"/>
<rectangle x1="12.1158" y1="1.16331875" x2="12.329159375" y2="1.17348125" layer="21"/>
<rectangle x1="12.827" y1="1.16331875" x2="13.020040625" y2="1.17348125" layer="21"/>
<rectangle x1="13.497559375" y1="1.16331875" x2="13.6906" y2="1.17348125" layer="21"/>
<rectangle x1="14.493240625" y1="1.16331875" x2="14.67611875" y2="1.17348125" layer="21"/>
<rectangle x1="15.08251875" y1="1.16331875" x2="15.275559375" y2="1.17348125" layer="21"/>
<rectangle x1="16.26108125" y1="1.16331875" x2="16.45411875" y2="1.17348125" layer="21"/>
<rectangle x1="0.2286" y1="1.17348125" x2="0.4318" y2="1.183640625" layer="21"/>
<rectangle x1="0.9906" y1="1.17348125" x2="1.488440625" y2="1.183640625" layer="21"/>
<rectangle x1="1.77291875" y1="1.17348125" x2="2.219959375" y2="1.183640625" layer="21"/>
<rectangle x1="2.53491875" y1="1.17348125" x2="2.981959375" y2="1.183640625" layer="21"/>
<rectangle x1="3.429" y1="1.17348125" x2="3.876040625" y2="1.183640625" layer="21"/>
<rectangle x1="4.333240625" y1="1.17348125" x2="4.78028125" y2="1.183640625" layer="21"/>
<rectangle x1="5.1054" y1="1.17348125" x2="5.603240625" y2="1.183640625" layer="21"/>
<rectangle x1="6.14171875" y1="1.17348125" x2="6.639559375" y2="1.183640625" layer="21"/>
<rectangle x1="6.9342" y1="1.17348125" x2="7.381240625" y2="1.183640625" layer="21"/>
<rectangle x1="7.9502" y1="1.17348125" x2="8.78331875" y2="1.183640625" layer="21"/>
<rectangle x1="9.331959375" y1="1.17348125" x2="9.535159375" y2="1.183640625" layer="21"/>
<rectangle x1="10.551159375" y1="1.17348125" x2="10.754359375" y2="1.183640625" layer="21"/>
<rectangle x1="11.160759375" y1="1.17348125" x2="11.3538" y2="1.183640625" layer="21"/>
<rectangle x1="12.1158" y1="1.17348125" x2="12.329159375" y2="1.183640625" layer="21"/>
<rectangle x1="12.837159375" y1="1.17348125" x2="13.00988125" y2="1.183640625" layer="21"/>
<rectangle x1="13.50771875" y1="1.17348125" x2="13.6906" y2="1.183640625" layer="21"/>
<rectangle x1="14.48308125" y1="1.17348125" x2="14.67611875" y2="1.183640625" layer="21"/>
<rectangle x1="15.08251875" y1="1.17348125" x2="15.275559375" y2="1.183640625" layer="21"/>
<rectangle x1="16.26108125" y1="1.17348125" x2="16.46428125" y2="1.183640625" layer="21"/>
<rectangle x1="0.238759375" y1="1.183640625" x2="0.421640625" y2="1.1938" layer="21"/>
<rectangle x1="1.000759375" y1="1.183640625" x2="1.488440625" y2="1.1938" layer="21"/>
<rectangle x1="1.78308125" y1="1.183640625" x2="2.23011875" y2="1.1938" layer="21"/>
<rectangle x1="2.53491875" y1="1.183640625" x2="2.981959375" y2="1.1938" layer="21"/>
<rectangle x1="3.439159375" y1="1.183640625" x2="3.8862" y2="1.1938" layer="21"/>
<rectangle x1="4.333240625" y1="1.183640625" x2="4.78028125" y2="1.1938" layer="21"/>
<rectangle x1="5.115559375" y1="1.183640625" x2="5.603240625" y2="1.1938" layer="21"/>
<rectangle x1="6.15188125" y1="1.183640625" x2="6.639559375" y2="1.1938" layer="21"/>
<rectangle x1="6.9342" y1="1.183640625" x2="7.37108125" y2="1.1938" layer="21"/>
<rectangle x1="7.9502" y1="1.183640625" x2="8.79348125" y2="1.1938" layer="21"/>
<rectangle x1="9.331959375" y1="1.183640625" x2="9.525" y2="1.1938" layer="21"/>
<rectangle x1="10.551159375" y1="1.183640625" x2="10.754359375" y2="1.1938" layer="21"/>
<rectangle x1="11.160759375" y1="1.183640625" x2="11.3538" y2="1.1938" layer="21"/>
<rectangle x1="12.125959375" y1="1.183640625" x2="12.329159375" y2="1.1938" layer="21"/>
<rectangle x1="12.827" y1="1.183640625" x2="13.020040625" y2="1.1938" layer="21"/>
<rectangle x1="13.50771875" y1="1.183640625" x2="13.6906" y2="1.1938" layer="21"/>
<rectangle x1="14.493240625" y1="1.183640625" x2="14.67611875" y2="1.1938" layer="21"/>
<rectangle x1="15.072359375" y1="1.183640625" x2="15.2654" y2="1.1938" layer="21"/>
<rectangle x1="16.271240625" y1="1.183640625" x2="16.46428125" y2="1.1938" layer="21"/>
<rectangle x1="0.24891875" y1="1.1938" x2="0.40131875" y2="1.203959375" layer="21"/>
<rectangle x1="1.000759375" y1="1.1938" x2="1.488440625" y2="1.203959375" layer="21"/>
<rectangle x1="1.77291875" y1="1.1938" x2="2.23011875" y2="1.203959375" layer="21"/>
<rectangle x1="2.524759375" y1="1.1938" x2="2.981959375" y2="1.203959375" layer="21"/>
<rectangle x1="3.429" y1="1.1938" x2="3.876040625" y2="1.203959375" layer="21"/>
<rectangle x1="4.333240625" y1="1.1938" x2="4.78028125" y2="1.203959375" layer="21"/>
<rectangle x1="5.115559375" y1="1.1938" x2="5.59308125" y2="1.203959375" layer="21"/>
<rectangle x1="6.162040625" y1="1.1938" x2="6.639559375" y2="1.203959375" layer="21"/>
<rectangle x1="6.924040625" y1="1.1938" x2="7.381240625" y2="1.203959375" layer="21"/>
<rectangle x1="7.940040625" y1="1.1938" x2="8.79348125" y2="1.203959375" layer="21"/>
<rectangle x1="9.331959375" y1="1.1938" x2="9.525" y2="1.203959375" layer="21"/>
<rectangle x1="10.551159375" y1="1.1938" x2="10.754359375" y2="1.203959375" layer="21"/>
<rectangle x1="11.160759375" y1="1.1938" x2="11.3538" y2="1.203959375" layer="21"/>
<rectangle x1="12.125959375" y1="1.1938" x2="12.329159375" y2="1.203959375" layer="21"/>
<rectangle x1="12.837159375" y1="1.1938" x2="13.00988125" y2="1.203959375" layer="21"/>
<rectangle x1="13.497559375" y1="1.1938" x2="13.680440625" y2="1.203959375" layer="21"/>
<rectangle x1="14.48308125" y1="1.1938" x2="14.665959375" y2="1.203959375" layer="21"/>
<rectangle x1="15.072359375" y1="1.1938" x2="15.2654" y2="1.203959375" layer="21"/>
<rectangle x1="16.271240625" y1="1.1938" x2="16.46428125" y2="1.203959375" layer="21"/>
<rectangle x1="0.25908125" y1="1.203959375" x2="0.391159375" y2="1.21411875" layer="21"/>
<rectangle x1="1.01091875" y1="1.203959375" x2="1.488440625" y2="1.21411875" layer="21"/>
<rectangle x1="1.78308125" y1="1.203959375" x2="2.219959375" y2="1.21411875" layer="21"/>
<rectangle x1="2.53491875" y1="1.203959375" x2="2.981959375" y2="1.21411875" layer="21"/>
<rectangle x1="3.429" y1="1.203959375" x2="3.8862" y2="1.21411875" layer="21"/>
<rectangle x1="4.333240625" y1="1.203959375" x2="4.78028125" y2="1.21411875" layer="21"/>
<rectangle x1="5.115559375" y1="1.203959375" x2="5.58291875" y2="1.21411875" layer="21"/>
<rectangle x1="6.162040625" y1="1.203959375" x2="6.639559375" y2="1.21411875" layer="21"/>
<rectangle x1="6.9342" y1="1.203959375" x2="7.37108125" y2="1.21411875" layer="21"/>
<rectangle x1="7.940040625" y1="1.203959375" x2="8.37691875" y2="1.21411875" layer="21"/>
<rectangle x1="8.38708125" y1="1.203959375" x2="8.803640625" y2="1.21411875" layer="21"/>
<rectangle x1="9.331959375" y1="1.203959375" x2="9.525" y2="1.21411875" layer="21"/>
<rectangle x1="10.56131875" y1="1.203959375" x2="10.754359375" y2="1.21411875" layer="21"/>
<rectangle x1="11.160759375" y1="1.203959375" x2="11.3538" y2="1.21411875" layer="21"/>
<rectangle x1="12.125959375" y1="1.203959375" x2="12.329159375" y2="1.21411875" layer="21"/>
<rectangle x1="12.827" y1="1.203959375" x2="13.020040625" y2="1.21411875" layer="21"/>
<rectangle x1="13.497559375" y1="1.203959375" x2="13.6906" y2="1.21411875" layer="21"/>
<rectangle x1="14.48308125" y1="1.203959375" x2="14.67611875" y2="1.21411875" layer="21"/>
<rectangle x1="15.072359375" y1="1.203959375" x2="15.2654" y2="1.21411875" layer="21"/>
<rectangle x1="16.271240625" y1="1.203959375" x2="16.46428125" y2="1.21411875" layer="21"/>
<rectangle x1="0.269240625" y1="1.21411875" x2="0.381" y2="1.22428125" layer="21"/>
<rectangle x1="1.01091875" y1="1.21411875" x2="1.488440625" y2="1.22428125" layer="21"/>
<rectangle x1="1.77291875" y1="1.21411875" x2="2.23011875" y2="1.22428125" layer="21"/>
<rectangle x1="2.53491875" y1="1.21411875" x2="2.981959375" y2="1.22428125" layer="21"/>
<rectangle x1="3.429" y1="1.21411875" x2="3.876040625" y2="1.22428125" layer="21"/>
<rectangle x1="4.333240625" y1="1.21411875" x2="4.78028125" y2="1.22428125" layer="21"/>
<rectangle x1="5.1054" y1="1.21411875" x2="5.572759375" y2="1.22428125" layer="21"/>
<rectangle x1="6.1722" y1="1.21411875" x2="6.64971875" y2="1.22428125" layer="21"/>
<rectangle x1="6.924040625" y1="1.21411875" x2="7.381240625" y2="1.22428125" layer="21"/>
<rectangle x1="7.92988125" y1="1.21411875" x2="8.37691875" y2="1.22428125" layer="21"/>
<rectangle x1="8.38708125" y1="1.21411875" x2="8.803640625" y2="1.22428125" layer="21"/>
<rectangle x1="9.331959375" y1="1.21411875" x2="9.525" y2="1.22428125" layer="21"/>
<rectangle x1="10.56131875" y1="1.21411875" x2="10.754359375" y2="1.22428125" layer="21"/>
<rectangle x1="11.160759375" y1="1.21411875" x2="11.343640625" y2="1.22428125" layer="21"/>
<rectangle x1="12.125959375" y1="1.21411875" x2="12.329159375" y2="1.22428125" layer="21"/>
<rectangle x1="12.827" y1="1.21411875" x2="13.00988125" y2="1.22428125" layer="21"/>
<rectangle x1="13.50771875" y1="1.21411875" x2="13.6906" y2="1.22428125" layer="21"/>
<rectangle x1="14.493240625" y1="1.21411875" x2="14.67611875" y2="1.22428125" layer="21"/>
<rectangle x1="15.072359375" y1="1.21411875" x2="15.2654" y2="1.22428125" layer="21"/>
<rectangle x1="16.2814" y1="1.21411875" x2="16.474440625" y2="1.22428125" layer="21"/>
<rectangle x1="0.2794" y1="1.22428125" x2="0.370840625" y2="1.234440625" layer="21"/>
<rectangle x1="1.02108125" y1="1.22428125" x2="1.4986" y2="1.234440625" layer="21"/>
<rectangle x1="1.78308125" y1="1.22428125" x2="2.219959375" y2="1.234440625" layer="21"/>
<rectangle x1="2.524759375" y1="1.22428125" x2="2.981959375" y2="1.234440625" layer="21"/>
<rectangle x1="3.429" y1="1.22428125" x2="3.8862" y2="1.234440625" layer="21"/>
<rectangle x1="4.32308125" y1="1.22428125" x2="4.78028125" y2="1.234440625" layer="21"/>
<rectangle x1="5.1054" y1="1.22428125" x2="5.572759375" y2="1.234440625" layer="21"/>
<rectangle x1="6.1722" y1="1.22428125" x2="6.64971875" y2="1.234440625" layer="21"/>
<rectangle x1="6.9342" y1="1.22428125" x2="7.381240625" y2="1.234440625" layer="21"/>
<rectangle x1="7.92988125" y1="1.22428125" x2="8.37691875" y2="1.234440625" layer="21"/>
<rectangle x1="8.397240625" y1="1.22428125" x2="8.803640625" y2="1.234440625" layer="21"/>
<rectangle x1="9.3218" y1="1.22428125" x2="9.514840625" y2="1.234440625" layer="21"/>
<rectangle x1="10.56131875" y1="1.22428125" x2="10.754359375" y2="1.234440625" layer="21"/>
<rectangle x1="11.160759375" y1="1.22428125" x2="11.3538" y2="1.234440625" layer="21"/>
<rectangle x1="12.125959375" y1="1.22428125" x2="12.329159375" y2="1.234440625" layer="21"/>
<rectangle x1="12.827" y1="1.22428125" x2="13.020040625" y2="1.234440625" layer="21"/>
<rectangle x1="13.497559375" y1="1.22428125" x2="13.6906" y2="1.234440625" layer="21"/>
<rectangle x1="14.493240625" y1="1.22428125" x2="14.665959375" y2="1.234440625" layer="21"/>
<rectangle x1="15.0622" y1="1.22428125" x2="15.255240625" y2="1.234440625" layer="21"/>
<rectangle x1="16.2814" y1="1.22428125" x2="16.474440625" y2="1.234440625" layer="21"/>
<rectangle x1="0.289559375" y1="1.234440625" x2="0.36068125" y2="1.2446" layer="21"/>
<rectangle x1="1.02108125" y1="1.234440625" x2="1.4986" y2="1.2446" layer="21"/>
<rectangle x1="1.77291875" y1="1.234440625" x2="2.23011875" y2="1.2446" layer="21"/>
<rectangle x1="2.53491875" y1="1.234440625" x2="2.981959375" y2="1.2446" layer="21"/>
<rectangle x1="3.429" y1="1.234440625" x2="3.876040625" y2="1.2446" layer="21"/>
<rectangle x1="4.333240625" y1="1.234440625" x2="4.78028125" y2="1.2446" layer="21"/>
<rectangle x1="5.115559375" y1="1.234440625" x2="5.572759375" y2="1.2446" layer="21"/>
<rectangle x1="6.182359375" y1="1.234440625" x2="6.64971875" y2="1.2446" layer="21"/>
<rectangle x1="6.924040625" y1="1.234440625" x2="7.37108125" y2="1.2446" layer="21"/>
<rectangle x1="7.91971875" y1="1.234440625" x2="8.366759375" y2="1.2446" layer="21"/>
<rectangle x1="8.397240625" y1="1.234440625" x2="8.8138" y2="1.2446" layer="21"/>
<rectangle x1="9.3218" y1="1.234440625" x2="9.514840625" y2="1.2446" layer="21"/>
<rectangle x1="10.57148125" y1="1.234440625" x2="10.754359375" y2="1.2446" layer="21"/>
<rectangle x1="11.160759375" y1="1.234440625" x2="11.343640625" y2="1.2446" layer="21"/>
<rectangle x1="12.13611875" y1="1.234440625" x2="12.329159375" y2="1.2446" layer="21"/>
<rectangle x1="12.837159375" y1="1.234440625" x2="13.020040625" y2="1.2446" layer="21"/>
<rectangle x1="13.50771875" y1="1.234440625" x2="13.6906" y2="1.2446" layer="21"/>
<rectangle x1="14.48308125" y1="1.234440625" x2="14.67611875" y2="1.2446" layer="21"/>
<rectangle x1="15.0622" y1="1.234440625" x2="15.255240625" y2="1.2446" layer="21"/>
<rectangle x1="16.2814" y1="1.234440625" x2="16.474440625" y2="1.2446" layer="21"/>
<rectangle x1="0.29971875" y1="1.2446" x2="0.35051875" y2="1.254759375" layer="21"/>
<rectangle x1="1.031240625" y1="1.2446" x2="1.4986" y2="1.254759375" layer="21"/>
<rectangle x1="1.78308125" y1="1.2446" x2="2.23011875" y2="1.254759375" layer="21"/>
<rectangle x1="2.53491875" y1="1.2446" x2="2.981959375" y2="1.254759375" layer="21"/>
<rectangle x1="3.429" y1="1.2446" x2="3.876040625" y2="1.254759375" layer="21"/>
<rectangle x1="4.333240625" y1="1.2446" x2="4.78028125" y2="1.254759375" layer="21"/>
<rectangle x1="5.115559375" y1="1.2446" x2="5.5626" y2="1.254759375" layer="21"/>
<rectangle x1="6.182359375" y1="1.2446" x2="6.64971875" y2="1.254759375" layer="21"/>
<rectangle x1="6.9342" y1="1.2446" x2="7.381240625" y2="1.254759375" layer="21"/>
<rectangle x1="7.91971875" y1="1.2446" x2="8.366759375" y2="1.254759375" layer="21"/>
<rectangle x1="8.397240625" y1="1.2446" x2="8.8138" y2="1.254759375" layer="21"/>
<rectangle x1="9.331959375" y1="1.2446" x2="9.514840625" y2="1.254759375" layer="21"/>
<rectangle x1="10.57148125" y1="1.2446" x2="10.754359375" y2="1.254759375" layer="21"/>
<rectangle x1="11.160759375" y1="1.2446" x2="11.3538" y2="1.254759375" layer="21"/>
<rectangle x1="12.13611875" y1="1.2446" x2="12.329159375" y2="1.254759375" layer="21"/>
<rectangle x1="12.827" y1="1.2446" x2="13.020040625" y2="1.254759375" layer="21"/>
<rectangle x1="13.50771875" y1="1.2446" x2="13.6906" y2="1.254759375" layer="21"/>
<rectangle x1="14.493240625" y1="1.2446" x2="14.67611875" y2="1.254759375" layer="21"/>
<rectangle x1="15.0622" y1="1.2446" x2="15.255240625" y2="1.254759375" layer="21"/>
<rectangle x1="16.2814" y1="1.2446" x2="16.474440625" y2="1.254759375" layer="21"/>
<rectangle x1="0.29971875" y1="1.254759375" x2="0.340359375" y2="1.26491875" layer="21"/>
<rectangle x1="1.031240625" y1="1.254759375" x2="1.4986" y2="1.26491875" layer="21"/>
<rectangle x1="1.77291875" y1="1.254759375" x2="2.219959375" y2="1.26491875" layer="21"/>
<rectangle x1="2.53491875" y1="1.254759375" x2="2.981959375" y2="1.26491875" layer="21"/>
<rectangle x1="3.429" y1="1.254759375" x2="3.8862" y2="1.26491875" layer="21"/>
<rectangle x1="4.333240625" y1="1.254759375" x2="4.78028125" y2="1.26491875" layer="21"/>
<rectangle x1="5.115559375" y1="1.254759375" x2="5.552440625" y2="1.26491875" layer="21"/>
<rectangle x1="6.19251875" y1="1.254759375" x2="6.64971875" y2="1.26491875" layer="21"/>
<rectangle x1="6.924040625" y1="1.254759375" x2="7.381240625" y2="1.26491875" layer="21"/>
<rectangle x1="7.91971875" y1="1.254759375" x2="8.366759375" y2="1.26491875" layer="21"/>
<rectangle x1="8.4074" y1="1.254759375" x2="8.823959375" y2="1.26491875" layer="21"/>
<rectangle x1="9.3218" y1="1.254759375" x2="9.514840625" y2="1.26491875" layer="21"/>
<rectangle x1="10.57148125" y1="1.254759375" x2="10.754359375" y2="1.26491875" layer="21"/>
<rectangle x1="11.17091875" y1="1.254759375" x2="11.343640625" y2="1.26491875" layer="21"/>
<rectangle x1="12.13611875" y1="1.254759375" x2="12.329159375" y2="1.26491875" layer="21"/>
<rectangle x1="12.837159375" y1="1.254759375" x2="13.00988125" y2="1.26491875" layer="21"/>
<rectangle x1="13.497559375" y1="1.254759375" x2="13.6906" y2="1.26491875" layer="21"/>
<rectangle x1="14.48308125" y1="1.254759375" x2="14.665959375" y2="1.26491875" layer="21"/>
<rectangle x1="15.0622" y1="1.254759375" x2="15.255240625" y2="1.26491875" layer="21"/>
<rectangle x1="16.291559375" y1="1.254759375" x2="16.474440625" y2="1.26491875" layer="21"/>
<rectangle x1="0.30988125" y1="1.26491875" x2="0.340359375" y2="1.27508125" layer="21"/>
<rectangle x1="1.031240625" y1="1.26491875" x2="1.4986" y2="1.27508125" layer="21"/>
<rectangle x1="1.78308125" y1="1.26491875" x2="2.23011875" y2="1.27508125" layer="21"/>
<rectangle x1="2.53491875" y1="1.26491875" x2="2.981959375" y2="1.27508125" layer="21"/>
<rectangle x1="3.439159375" y1="1.26491875" x2="3.8862" y2="1.27508125" layer="21"/>
<rectangle x1="4.333240625" y1="1.26491875" x2="4.78028125" y2="1.27508125" layer="21"/>
<rectangle x1="5.1054" y1="1.26491875" x2="5.552440625" y2="1.27508125" layer="21"/>
<rectangle x1="6.19251875" y1="1.26491875" x2="6.65988125" y2="1.27508125" layer="21"/>
<rectangle x1="6.9342" y1="1.26491875" x2="7.381240625" y2="1.27508125" layer="21"/>
<rectangle x1="7.909559375" y1="1.26491875" x2="8.3566" y2="1.27508125" layer="21"/>
<rectangle x1="8.4074" y1="1.26491875" x2="8.823959375" y2="1.27508125" layer="21"/>
<rectangle x1="9.3218" y1="1.26491875" x2="9.50468125" y2="1.27508125" layer="21"/>
<rectangle x1="10.57148125" y1="1.26491875" x2="10.754359375" y2="1.27508125" layer="21"/>
<rectangle x1="11.160759375" y1="1.26491875" x2="11.343640625" y2="1.27508125" layer="21"/>
<rectangle x1="12.13611875" y1="1.26491875" x2="12.329159375" y2="1.27508125" layer="21"/>
<rectangle x1="12.827" y1="1.26491875" x2="13.00988125" y2="1.27508125" layer="21"/>
<rectangle x1="13.497559375" y1="1.26491875" x2="13.6906" y2="1.27508125" layer="21"/>
<rectangle x1="14.48308125" y1="1.26491875" x2="14.67611875" y2="1.27508125" layer="21"/>
<rectangle x1="15.0622" y1="1.26491875" x2="15.24508125" y2="1.27508125" layer="21"/>
<rectangle x1="16.291559375" y1="1.26491875" x2="16.4846" y2="1.27508125" layer="21"/>
<rectangle x1="1.031240625" y1="1.27508125" x2="1.4986" y2="1.285240625" layer="21"/>
<rectangle x1="1.77291875" y1="1.27508125" x2="2.219959375" y2="1.285240625" layer="21"/>
<rectangle x1="2.524759375" y1="1.27508125" x2="2.981959375" y2="1.285240625" layer="21"/>
<rectangle x1="3.429" y1="1.27508125" x2="3.876040625" y2="1.285240625" layer="21"/>
<rectangle x1="4.333240625" y1="1.27508125" x2="4.78028125" y2="1.285240625" layer="21"/>
<rectangle x1="5.115559375" y1="1.27508125" x2="5.552440625" y2="1.285240625" layer="21"/>
<rectangle x1="6.20268125" y1="1.27508125" x2="6.65988125" y2="1.285240625" layer="21"/>
<rectangle x1="6.924040625" y1="1.27508125" x2="7.37108125" y2="1.285240625" layer="21"/>
<rectangle x1="7.909559375" y1="1.27508125" x2="8.3566" y2="1.285240625" layer="21"/>
<rectangle x1="8.4074" y1="1.27508125" x2="8.823959375" y2="1.285240625" layer="21"/>
<rectangle x1="9.3218" y1="1.27508125" x2="9.50468125" y2="1.285240625" layer="21"/>
<rectangle x1="10.581640625" y1="1.27508125" x2="10.754359375" y2="1.285240625" layer="21"/>
<rectangle x1="11.160759375" y1="1.27508125" x2="11.343640625" y2="1.285240625" layer="21"/>
<rectangle x1="12.13611875" y1="1.27508125" x2="12.329159375" y2="1.285240625" layer="21"/>
<rectangle x1="12.827" y1="1.27508125" x2="13.020040625" y2="1.285240625" layer="21"/>
<rectangle x1="13.50771875" y1="1.27508125" x2="13.680440625" y2="1.285240625" layer="21"/>
<rectangle x1="14.493240625" y1="1.27508125" x2="14.67611875" y2="1.285240625" layer="21"/>
<rectangle x1="15.0622" y1="1.27508125" x2="15.24508125" y2="1.285240625" layer="21"/>
<rectangle x1="16.291559375" y1="1.27508125" x2="16.474440625" y2="1.285240625" layer="21"/>
<rectangle x1="1.031240625" y1="1.285240625" x2="1.4986" y2="1.2954" layer="21"/>
<rectangle x1="1.78308125" y1="1.285240625" x2="2.23011875" y2="1.2954" layer="21"/>
<rectangle x1="2.53491875" y1="1.285240625" x2="2.981959375" y2="1.2954" layer="21"/>
<rectangle x1="3.429" y1="1.285240625" x2="3.876040625" y2="1.2954" layer="21"/>
<rectangle x1="4.333240625" y1="1.285240625" x2="4.78028125" y2="1.2954" layer="21"/>
<rectangle x1="5.115559375" y1="1.285240625" x2="5.54228125" y2="1.2954" layer="21"/>
<rectangle x1="6.20268125" y1="1.285240625" x2="6.65988125" y2="1.2954" layer="21"/>
<rectangle x1="6.9342" y1="1.285240625" x2="7.381240625" y2="1.2954" layer="21"/>
<rectangle x1="7.8994" y1="1.285240625" x2="8.346440625" y2="1.2954" layer="21"/>
<rectangle x1="8.417559375" y1="1.285240625" x2="8.83411875" y2="1.2954" layer="21"/>
<rectangle x1="9.3218" y1="1.285240625" x2="9.50468125" y2="1.2954" layer="21"/>
<rectangle x1="10.581640625" y1="1.285240625" x2="10.754359375" y2="1.2954" layer="21"/>
<rectangle x1="11.160759375" y1="1.285240625" x2="11.3538" y2="1.2954" layer="21"/>
<rectangle x1="12.14628125" y1="1.285240625" x2="12.329159375" y2="1.2954" layer="21"/>
<rectangle x1="12.827" y1="1.285240625" x2="13.020040625" y2="1.2954" layer="21"/>
<rectangle x1="13.497559375" y1="1.285240625" x2="13.6906" y2="1.2954" layer="21"/>
<rectangle x1="14.493240625" y1="1.285240625" x2="14.665959375" y2="1.2954" layer="21"/>
<rectangle x1="15.0622" y1="1.285240625" x2="15.24508125" y2="1.2954" layer="21"/>
<rectangle x1="16.291559375" y1="1.285240625" x2="16.474440625" y2="1.2954" layer="21"/>
<rectangle x1="1.031240625" y1="1.2954" x2="1.4986" y2="1.305559375" layer="21"/>
<rectangle x1="1.77291875" y1="1.2954" x2="2.23011875" y2="1.305559375" layer="21"/>
<rectangle x1="2.53491875" y1="1.2954" x2="2.981959375" y2="1.305559375" layer="21"/>
<rectangle x1="3.429" y1="1.2954" x2="3.876040625" y2="1.305559375" layer="21"/>
<rectangle x1="4.333240625" y1="1.2954" x2="4.78028125" y2="1.305559375" layer="21"/>
<rectangle x1="5.115559375" y1="1.2954" x2="5.54228125" y2="1.305559375" layer="21"/>
<rectangle x1="6.20268125" y1="1.2954" x2="6.65988125" y2="1.305559375" layer="21"/>
<rectangle x1="6.924040625" y1="1.2954" x2="7.381240625" y2="1.305559375" layer="21"/>
<rectangle x1="7.8994" y1="1.2954" x2="8.346440625" y2="1.305559375" layer="21"/>
<rectangle x1="8.417559375" y1="1.2954" x2="8.83411875" y2="1.305559375" layer="21"/>
<rectangle x1="9.311640625" y1="1.2954" x2="9.50468125" y2="1.305559375" layer="21"/>
<rectangle x1="10.581640625" y1="1.2954" x2="10.754359375" y2="1.305559375" layer="21"/>
<rectangle x1="11.160759375" y1="1.2954" x2="11.343640625" y2="1.305559375" layer="21"/>
<rectangle x1="12.13611875" y1="1.2954" x2="12.329159375" y2="1.305559375" layer="21"/>
<rectangle x1="12.837159375" y1="1.2954" x2="13.00988125" y2="1.305559375" layer="21"/>
<rectangle x1="13.50771875" y1="1.2954" x2="13.6906" y2="1.305559375" layer="21"/>
<rectangle x1="14.48308125" y1="1.2954" x2="14.67611875" y2="1.305559375" layer="21"/>
<rectangle x1="15.052040625" y1="1.2954" x2="15.24508125" y2="1.305559375" layer="21"/>
<rectangle x1="16.291559375" y1="1.2954" x2="16.4846" y2="1.305559375" layer="21"/>
<rectangle x1="1.031240625" y1="1.305559375" x2="1.4986" y2="1.31571875" layer="21"/>
<rectangle x1="1.77291875" y1="1.305559375" x2="2.23011875" y2="1.31571875" layer="21"/>
<rectangle x1="2.524759375" y1="1.305559375" x2="2.981959375" y2="1.31571875" layer="21"/>
<rectangle x1="3.439159375" y1="1.305559375" x2="3.8862" y2="1.31571875" layer="21"/>
<rectangle x1="4.333240625" y1="1.305559375" x2="4.78028125" y2="1.31571875" layer="21"/>
<rectangle x1="5.115559375" y1="1.305559375" x2="5.54228125" y2="1.31571875" layer="21"/>
<rectangle x1="6.20268125" y1="1.305559375" x2="6.65988125" y2="1.31571875" layer="21"/>
<rectangle x1="6.9342" y1="1.305559375" x2="7.381240625" y2="1.31571875" layer="21"/>
<rectangle x1="7.889240625" y1="1.305559375" x2="8.346440625" y2="1.31571875" layer="21"/>
<rectangle x1="8.42771875" y1="1.305559375" x2="8.83411875" y2="1.31571875" layer="21"/>
<rectangle x1="9.311640625" y1="1.305559375" x2="9.50468125" y2="1.31571875" layer="21"/>
<rectangle x1="10.581640625" y1="1.305559375" x2="10.754359375" y2="1.31571875" layer="21"/>
<rectangle x1="11.160759375" y1="1.305559375" x2="11.3538" y2="1.31571875" layer="21"/>
<rectangle x1="12.14628125" y1="1.305559375" x2="12.329159375" y2="1.31571875" layer="21"/>
<rectangle x1="12.837159375" y1="1.305559375" x2="13.020040625" y2="1.31571875" layer="21"/>
<rectangle x1="13.497559375" y1="1.305559375" x2="13.6906" y2="1.31571875" layer="21"/>
<rectangle x1="14.493240625" y1="1.305559375" x2="14.665959375" y2="1.31571875" layer="21"/>
<rectangle x1="15.0622" y1="1.305559375" x2="15.24508125" y2="1.31571875" layer="21"/>
<rectangle x1="16.291559375" y1="1.305559375" x2="16.4846" y2="1.31571875" layer="21"/>
<rectangle x1="1.031240625" y1="1.31571875" x2="1.4986" y2="1.32588125" layer="21"/>
<rectangle x1="1.78308125" y1="1.31571875" x2="2.219959375" y2="1.32588125" layer="21"/>
<rectangle x1="2.53491875" y1="1.31571875" x2="2.981959375" y2="1.32588125" layer="21"/>
<rectangle x1="3.429" y1="1.31571875" x2="3.876040625" y2="1.32588125" layer="21"/>
<rectangle x1="4.333240625" y1="1.31571875" x2="4.78028125" y2="1.32588125" layer="21"/>
<rectangle x1="5.115559375" y1="1.31571875" x2="5.54228125" y2="1.32588125" layer="21"/>
<rectangle x1="6.212840625" y1="1.31571875" x2="6.65988125" y2="1.32588125" layer="21"/>
<rectangle x1="6.924040625" y1="1.31571875" x2="7.37108125" y2="1.32588125" layer="21"/>
<rectangle x1="7.889240625" y1="1.31571875" x2="8.33628125" y2="1.32588125" layer="21"/>
<rectangle x1="8.42771875" y1="1.31571875" x2="8.84428125" y2="1.32588125" layer="21"/>
<rectangle x1="9.311640625" y1="1.31571875" x2="9.50468125" y2="1.32588125" layer="21"/>
<rectangle x1="10.581640625" y1="1.31571875" x2="10.754359375" y2="1.32588125" layer="21"/>
<rectangle x1="11.160759375" y1="1.31571875" x2="11.343640625" y2="1.32588125" layer="21"/>
<rectangle x1="12.14628125" y1="1.31571875" x2="12.329159375" y2="1.32588125" layer="21"/>
<rectangle x1="12.827" y1="1.31571875" x2="13.00988125" y2="1.32588125" layer="21"/>
<rectangle x1="13.50771875" y1="1.31571875" x2="13.6906" y2="1.32588125" layer="21"/>
<rectangle x1="14.48308125" y1="1.31571875" x2="14.67611875" y2="1.32588125" layer="21"/>
<rectangle x1="15.052040625" y1="1.31571875" x2="15.23491875" y2="1.32588125" layer="21"/>
<rectangle x1="16.30171875" y1="1.31571875" x2="16.4846" y2="1.32588125" layer="21"/>
<rectangle x1="1.031240625" y1="1.32588125" x2="1.4986" y2="1.336040625" layer="21"/>
<rectangle x1="1.77291875" y1="1.32588125" x2="2.23011875" y2="1.336040625" layer="21"/>
<rectangle x1="2.53491875" y1="1.32588125" x2="2.981959375" y2="1.336040625" layer="21"/>
<rectangle x1="3.429" y1="1.32588125" x2="3.8862" y2="1.336040625" layer="21"/>
<rectangle x1="4.333240625" y1="1.32588125" x2="4.78028125" y2="1.336040625" layer="21"/>
<rectangle x1="5.115559375" y1="1.32588125" x2="5.53211875" y2="1.336040625" layer="21"/>
<rectangle x1="6.212840625" y1="1.32588125" x2="6.65988125" y2="1.336040625" layer="21"/>
<rectangle x1="6.9342" y1="1.32588125" x2="7.381240625" y2="1.336040625" layer="21"/>
<rectangle x1="7.889240625" y1="1.32588125" x2="8.33628125" y2="1.336040625" layer="21"/>
<rectangle x1="8.42771875" y1="1.32588125" x2="8.84428125" y2="1.336040625" layer="21"/>
<rectangle x1="9.311640625" y1="1.32588125" x2="9.49451875" y2="1.336040625" layer="21"/>
<rectangle x1="10.581640625" y1="1.32588125" x2="10.754359375" y2="1.336040625" layer="21"/>
<rectangle x1="11.160759375" y1="1.32588125" x2="11.343640625" y2="1.336040625" layer="21"/>
<rectangle x1="12.13611875" y1="1.32588125" x2="12.329159375" y2="1.336040625" layer="21"/>
<rectangle x1="12.837159375" y1="1.32588125" x2="13.020040625" y2="1.336040625" layer="21"/>
<rectangle x1="13.497559375" y1="1.32588125" x2="13.6906" y2="1.336040625" layer="21"/>
<rectangle x1="14.48308125" y1="1.32588125" x2="14.67611875" y2="1.336040625" layer="21"/>
<rectangle x1="15.052040625" y1="1.32588125" x2="15.24508125" y2="1.336040625" layer="21"/>
<rectangle x1="16.291559375" y1="1.32588125" x2="16.4846" y2="1.336040625" layer="21"/>
<rectangle x1="1.031240625" y1="1.336040625" x2="1.4986" y2="1.3462" layer="21"/>
<rectangle x1="1.78308125" y1="1.336040625" x2="2.23011875" y2="1.3462" layer="21"/>
<rectangle x1="2.53491875" y1="1.336040625" x2="2.981959375" y2="1.3462" layer="21"/>
<rectangle x1="3.429" y1="1.336040625" x2="3.876040625" y2="1.3462" layer="21"/>
<rectangle x1="4.333240625" y1="1.336040625" x2="4.78028125" y2="1.3462" layer="21"/>
<rectangle x1="5.1054" y1="1.336040625" x2="5.53211875" y2="1.3462" layer="21"/>
<rectangle x1="6.212840625" y1="1.336040625" x2="6.670040625" y2="1.3462" layer="21"/>
<rectangle x1="6.924040625" y1="1.336040625" x2="7.381240625" y2="1.3462" layer="21"/>
<rectangle x1="7.87908125" y1="1.336040625" x2="8.32611875" y2="1.3462" layer="21"/>
<rectangle x1="8.42771875" y1="1.336040625" x2="8.854440625" y2="1.3462" layer="21"/>
<rectangle x1="9.311640625" y1="1.336040625" x2="9.50468125" y2="1.3462" layer="21"/>
<rectangle x1="10.5918" y1="1.336040625" x2="10.754359375" y2="1.3462" layer="21"/>
<rectangle x1="11.160759375" y1="1.336040625" x2="11.343640625" y2="1.3462" layer="21"/>
<rectangle x1="12.14628125" y1="1.336040625" x2="12.329159375" y2="1.3462" layer="21"/>
<rectangle x1="12.827" y1="1.336040625" x2="13.00988125" y2="1.3462" layer="21"/>
<rectangle x1="13.50771875" y1="1.336040625" x2="13.6906" y2="1.3462" layer="21"/>
<rectangle x1="14.493240625" y1="1.336040625" x2="14.665959375" y2="1.3462" layer="21"/>
<rectangle x1="15.052040625" y1="1.336040625" x2="15.24508125" y2="1.3462" layer="21"/>
<rectangle x1="16.30171875" y1="1.336040625" x2="16.4846" y2="1.3462" layer="21"/>
<rectangle x1="1.02108125" y1="1.3462" x2="1.4986" y2="1.356359375" layer="21"/>
<rectangle x1="1.77291875" y1="1.3462" x2="2.219959375" y2="1.356359375" layer="21"/>
<rectangle x1="2.53491875" y1="1.3462" x2="2.981959375" y2="1.356359375" layer="21"/>
<rectangle x1="3.429" y1="1.3462" x2="3.8862" y2="1.356359375" layer="21"/>
<rectangle x1="4.32308125" y1="1.3462" x2="4.78028125" y2="1.356359375" layer="21"/>
<rectangle x1="5.115559375" y1="1.3462" x2="5.53211875" y2="1.356359375" layer="21"/>
<rectangle x1="6.212840625" y1="1.3462" x2="6.65988125" y2="1.356359375" layer="21"/>
<rectangle x1="6.9342" y1="1.3462" x2="7.381240625" y2="1.356359375" layer="21"/>
<rectangle x1="7.87908125" y1="1.3462" x2="8.32611875" y2="1.356359375" layer="21"/>
<rectangle x1="8.43788125" y1="1.3462" x2="8.854440625" y2="1.356359375" layer="21"/>
<rectangle x1="9.311640625" y1="1.3462" x2="9.49451875" y2="1.356359375" layer="21"/>
<rectangle x1="10.5918" y1="1.3462" x2="10.754359375" y2="1.356359375" layer="21"/>
<rectangle x1="11.160759375" y1="1.3462" x2="11.343640625" y2="1.356359375" layer="21"/>
<rectangle x1="12.14628125" y1="1.3462" x2="12.329159375" y2="1.356359375" layer="21"/>
<rectangle x1="12.837159375" y1="1.3462" x2="13.020040625" y2="1.356359375" layer="21"/>
<rectangle x1="13.50771875" y1="1.3462" x2="13.6906" y2="1.356359375" layer="21"/>
<rectangle x1="14.493240625" y1="1.3462" x2="14.67611875" y2="1.356359375" layer="21"/>
<rectangle x1="15.052040625" y1="1.3462" x2="15.23491875" y2="1.356359375" layer="21"/>
<rectangle x1="16.30171875" y1="1.3462" x2="16.4846" y2="1.356359375" layer="21"/>
<rectangle x1="1.02108125" y1="1.356359375" x2="1.4986" y2="1.36651875" layer="21"/>
<rectangle x1="1.78308125" y1="1.356359375" x2="2.23011875" y2="1.36651875" layer="21"/>
<rectangle x1="2.524759375" y1="1.356359375" x2="2.981959375" y2="1.36651875" layer="21"/>
<rectangle x1="3.429" y1="1.356359375" x2="3.876040625" y2="1.36651875" layer="21"/>
<rectangle x1="4.333240625" y1="1.356359375" x2="4.78028125" y2="1.36651875" layer="21"/>
<rectangle x1="5.115559375" y1="1.356359375" x2="5.53211875" y2="1.36651875" layer="21"/>
<rectangle x1="6.223" y1="1.356359375" x2="6.670040625" y2="1.36651875" layer="21"/>
<rectangle x1="6.924040625" y1="1.356359375" x2="7.37108125" y2="1.36651875" layer="21"/>
<rectangle x1="7.86891875" y1="1.356359375" x2="8.32611875" y2="1.36651875" layer="21"/>
<rectangle x1="8.43788125" y1="1.356359375" x2="8.854440625" y2="1.36651875" layer="21"/>
<rectangle x1="9.311640625" y1="1.356359375" x2="9.50468125" y2="1.36651875" layer="21"/>
<rectangle x1="10.581640625" y1="1.356359375" x2="10.754359375" y2="1.36651875" layer="21"/>
<rectangle x1="11.17091875" y1="1.356359375" x2="11.3538" y2="1.36651875" layer="21"/>
<rectangle x1="12.14628125" y1="1.356359375" x2="12.329159375" y2="1.36651875" layer="21"/>
<rectangle x1="12.827" y1="1.356359375" x2="13.020040625" y2="1.36651875" layer="21"/>
<rectangle x1="13.497559375" y1="1.356359375" x2="13.680440625" y2="1.36651875" layer="21"/>
<rectangle x1="14.48308125" y1="1.356359375" x2="14.67611875" y2="1.36651875" layer="21"/>
<rectangle x1="15.052040625" y1="1.356359375" x2="15.23491875" y2="1.36651875" layer="21"/>
<rectangle x1="16.30171875" y1="1.356359375" x2="16.4846" y2="1.36651875" layer="21"/>
<rectangle x1="1.02108125" y1="1.36651875" x2="1.4986" y2="1.37668125" layer="21"/>
<rectangle x1="1.77291875" y1="1.36651875" x2="2.219959375" y2="1.37668125" layer="21"/>
<rectangle x1="2.53491875" y1="1.36651875" x2="2.981959375" y2="1.37668125" layer="21"/>
<rectangle x1="3.439159375" y1="1.36651875" x2="3.876040625" y2="1.37668125" layer="21"/>
<rectangle x1="4.333240625" y1="1.36651875" x2="4.78028125" y2="1.37668125" layer="21"/>
<rectangle x1="5.115559375" y1="1.36651875" x2="5.53211875" y2="1.37668125" layer="21"/>
<rectangle x1="6.212840625" y1="1.36651875" x2="6.670040625" y2="1.37668125" layer="21"/>
<rectangle x1="6.9342" y1="1.36651875" x2="7.381240625" y2="1.37668125" layer="21"/>
<rectangle x1="7.86891875" y1="1.36651875" x2="8.315959375" y2="1.37668125" layer="21"/>
<rectangle x1="8.43788125" y1="1.36651875" x2="8.8646" y2="1.37668125" layer="21"/>
<rectangle x1="9.311640625" y1="1.36651875" x2="9.49451875" y2="1.37668125" layer="21"/>
<rectangle x1="10.5918" y1="1.36651875" x2="10.754359375" y2="1.37668125" layer="21"/>
<rectangle x1="11.160759375" y1="1.36651875" x2="11.343640625" y2="1.37668125" layer="21"/>
<rectangle x1="12.14628125" y1="1.36651875" x2="12.329159375" y2="1.37668125" layer="21"/>
<rectangle x1="12.837159375" y1="1.36651875" x2="13.020040625" y2="1.37668125" layer="21"/>
<rectangle x1="13.497559375" y1="1.36651875" x2="13.6906" y2="1.37668125" layer="21"/>
<rectangle x1="14.493240625" y1="1.36651875" x2="14.665959375" y2="1.37668125" layer="21"/>
<rectangle x1="15.0622" y1="1.36651875" x2="15.24508125" y2="1.37668125" layer="21"/>
<rectangle x1="16.30171875" y1="1.36651875" x2="16.4846" y2="1.37668125" layer="21"/>
<rectangle x1="1.01091875" y1="1.37668125" x2="1.4986" y2="1.386840625" layer="21"/>
<rectangle x1="1.77291875" y1="1.37668125" x2="2.23011875" y2="1.386840625" layer="21"/>
<rectangle x1="2.53491875" y1="1.37668125" x2="2.981959375" y2="1.386840625" layer="21"/>
<rectangle x1="3.429" y1="1.37668125" x2="3.8862" y2="1.386840625" layer="21"/>
<rectangle x1="4.333240625" y1="1.37668125" x2="4.78028125" y2="1.386840625" layer="21"/>
<rectangle x1="5.1054" y1="1.37668125" x2="5.53211875" y2="1.386840625" layer="21"/>
<rectangle x1="6.223" y1="1.37668125" x2="6.670040625" y2="1.386840625" layer="21"/>
<rectangle x1="6.924040625" y1="1.37668125" x2="7.381240625" y2="1.386840625" layer="21"/>
<rectangle x1="7.858759375" y1="1.37668125" x2="8.315959375" y2="1.386840625" layer="21"/>
<rectangle x1="8.448040625" y1="1.37668125" x2="8.8646" y2="1.386840625" layer="21"/>
<rectangle x1="9.311640625" y1="1.37668125" x2="9.49451875" y2="1.386840625" layer="21"/>
<rectangle x1="10.5918" y1="1.37668125" x2="10.754359375" y2="1.386840625" layer="21"/>
<rectangle x1="11.160759375" y1="1.37668125" x2="11.3538" y2="1.386840625" layer="21"/>
<rectangle x1="12.14628125" y1="1.37668125" x2="12.329159375" y2="1.386840625" layer="21"/>
<rectangle x1="12.827" y1="1.37668125" x2="13.00988125" y2="1.386840625" layer="21"/>
<rectangle x1="13.50771875" y1="1.37668125" x2="13.6906" y2="1.386840625" layer="21"/>
<rectangle x1="14.48308125" y1="1.37668125" x2="14.67611875" y2="1.386840625" layer="21"/>
<rectangle x1="15.052040625" y1="1.37668125" x2="15.23491875" y2="1.386840625" layer="21"/>
<rectangle x1="16.30171875" y1="1.37668125" x2="16.4846" y2="1.386840625" layer="21"/>
<rectangle x1="1.000759375" y1="1.386840625" x2="1.4986" y2="1.397" layer="21"/>
<rectangle x1="1.77291875" y1="1.386840625" x2="2.23011875" y2="1.397" layer="21"/>
<rectangle x1="2.524759375" y1="1.386840625" x2="2.981959375" y2="1.397" layer="21"/>
<rectangle x1="3.429" y1="1.386840625" x2="3.8862" y2="1.397" layer="21"/>
<rectangle x1="4.333240625" y1="1.386840625" x2="4.78028125" y2="1.397" layer="21"/>
<rectangle x1="5.115559375" y1="1.386840625" x2="5.521959375" y2="1.397" layer="21"/>
<rectangle x1="6.212840625" y1="1.386840625" x2="6.670040625" y2="1.397" layer="21"/>
<rectangle x1="6.9342" y1="1.386840625" x2="7.37108125" y2="1.397" layer="21"/>
<rectangle x1="7.858759375" y1="1.386840625" x2="8.315959375" y2="1.397" layer="21"/>
<rectangle x1="8.448040625" y1="1.386840625" x2="8.874759375" y2="1.397" layer="21"/>
<rectangle x1="9.311640625" y1="1.386840625" x2="9.50468125" y2="1.397" layer="21"/>
<rectangle x1="10.5918" y1="1.386840625" x2="10.754359375" y2="1.397" layer="21"/>
<rectangle x1="11.160759375" y1="1.386840625" x2="11.3538" y2="1.397" layer="21"/>
<rectangle x1="12.14628125" y1="1.386840625" x2="12.329159375" y2="1.397" layer="21"/>
<rectangle x1="12.837159375" y1="1.386840625" x2="13.00988125" y2="1.397" layer="21"/>
<rectangle x1="13.497559375" y1="1.386840625" x2="13.6906" y2="1.397" layer="21"/>
<rectangle x1="14.48308125" y1="1.386840625" x2="14.67611875" y2="1.397" layer="21"/>
<rectangle x1="15.052040625" y1="1.386840625" x2="15.24508125" y2="1.397" layer="21"/>
<rectangle x1="16.291559375" y1="1.386840625" x2="16.4846" y2="1.397" layer="21"/>
<rectangle x1="1.000759375" y1="1.397" x2="1.4986" y2="1.407159375" layer="21"/>
<rectangle x1="1.78308125" y1="1.397" x2="2.219959375" y2="1.407159375" layer="21"/>
<rectangle x1="2.53491875" y1="1.397" x2="2.981959375" y2="1.407159375" layer="21"/>
<rectangle x1="3.429" y1="1.397" x2="3.876040625" y2="1.407159375" layer="21"/>
<rectangle x1="4.333240625" y1="1.397" x2="4.78028125" y2="1.407159375" layer="21"/>
<rectangle x1="5.115559375" y1="1.397" x2="5.53211875" y2="1.407159375" layer="21"/>
<rectangle x1="6.223" y1="1.397" x2="6.670040625" y2="1.407159375" layer="21"/>
<rectangle x1="6.924040625" y1="1.397" x2="7.381240625" y2="1.407159375" layer="21"/>
<rectangle x1="7.8486" y1="1.397" x2="8.3058" y2="1.407159375" layer="21"/>
<rectangle x1="8.448040625" y1="1.397" x2="8.874759375" y2="1.407159375" layer="21"/>
<rectangle x1="9.311640625" y1="1.397" x2="9.49451875" y2="1.407159375" layer="21"/>
<rectangle x1="10.5918" y1="1.397" x2="10.754359375" y2="1.407159375" layer="21"/>
<rectangle x1="11.160759375" y1="1.397" x2="11.343640625" y2="1.407159375" layer="21"/>
<rectangle x1="12.14628125" y1="1.397" x2="12.329159375" y2="1.407159375" layer="21"/>
<rectangle x1="12.827" y1="1.397" x2="13.020040625" y2="1.407159375" layer="21"/>
<rectangle x1="13.50771875" y1="1.397" x2="13.6906" y2="1.407159375" layer="21"/>
<rectangle x1="14.493240625" y1="1.397" x2="14.665959375" y2="1.407159375" layer="21"/>
<rectangle x1="15.052040625" y1="1.397" x2="15.23491875" y2="1.407159375" layer="21"/>
<rectangle x1="16.30171875" y1="1.397" x2="16.4846" y2="1.407159375" layer="21"/>
<rectangle x1="0.9906" y1="1.407159375" x2="1.4986" y2="1.41731875" layer="21"/>
<rectangle x1="1.77291875" y1="1.407159375" x2="2.23011875" y2="1.41731875" layer="21"/>
<rectangle x1="2.53491875" y1="1.407159375" x2="2.981959375" y2="1.41731875" layer="21"/>
<rectangle x1="3.429" y1="1.407159375" x2="3.876040625" y2="1.41731875" layer="21"/>
<rectangle x1="4.333240625" y1="1.407159375" x2="4.78028125" y2="1.41731875" layer="21"/>
<rectangle x1="5.115559375" y1="1.407159375" x2="5.521959375" y2="1.41731875" layer="21"/>
<rectangle x1="6.212840625" y1="1.407159375" x2="6.670040625" y2="1.41731875" layer="21"/>
<rectangle x1="6.9342" y1="1.407159375" x2="7.37108125" y2="1.41731875" layer="21"/>
<rectangle x1="7.8486" y1="1.407159375" x2="8.3058" y2="1.41731875" layer="21"/>
<rectangle x1="8.4582" y1="1.407159375" x2="8.874759375" y2="1.41731875" layer="21"/>
<rectangle x1="9.311640625" y1="1.407159375" x2="9.49451875" y2="1.41731875" layer="21"/>
<rectangle x1="10.5918" y1="1.407159375" x2="10.754359375" y2="1.41731875" layer="21"/>
<rectangle x1="11.160759375" y1="1.407159375" x2="11.343640625" y2="1.41731875" layer="21"/>
<rectangle x1="12.14628125" y1="1.407159375" x2="12.329159375" y2="1.41731875" layer="21"/>
<rectangle x1="12.837159375" y1="1.407159375" x2="13.020040625" y2="1.41731875" layer="21"/>
<rectangle x1="13.50771875" y1="1.407159375" x2="13.6906" y2="1.41731875" layer="21"/>
<rectangle x1="14.493240625" y1="1.407159375" x2="14.67611875" y2="1.41731875" layer="21"/>
<rectangle x1="15.052040625" y1="1.407159375" x2="15.23491875" y2="1.41731875" layer="21"/>
<rectangle x1="16.30171875" y1="1.407159375" x2="16.4846" y2="1.41731875" layer="21"/>
<rectangle x1="0.980440625" y1="1.41731875" x2="1.4986" y2="1.42748125" layer="21"/>
<rectangle x1="1.78308125" y1="1.41731875" x2="2.219959375" y2="1.42748125" layer="21"/>
<rectangle x1="2.53491875" y1="1.41731875" x2="2.981959375" y2="1.42748125" layer="21"/>
<rectangle x1="3.429" y1="1.41731875" x2="3.876040625" y2="1.42748125" layer="21"/>
<rectangle x1="4.333240625" y1="1.41731875" x2="4.78028125" y2="1.42748125" layer="21"/>
<rectangle x1="5.1054" y1="1.41731875" x2="5.53211875" y2="1.42748125" layer="21"/>
<rectangle x1="6.223" y1="1.41731875" x2="6.670040625" y2="1.42748125" layer="21"/>
<rectangle x1="6.924040625" y1="1.41731875" x2="7.381240625" y2="1.42748125" layer="21"/>
<rectangle x1="7.8486" y1="1.41731875" x2="8.295640625" y2="1.42748125" layer="21"/>
<rectangle x1="8.4582" y1="1.41731875" x2="8.88491875" y2="1.42748125" layer="21"/>
<rectangle x1="9.311640625" y1="1.41731875" x2="9.50468125" y2="1.42748125" layer="21"/>
<rectangle x1="10.581640625" y1="1.41731875" x2="10.754359375" y2="1.42748125" layer="21"/>
<rectangle x1="11.160759375" y1="1.41731875" x2="11.343640625" y2="1.42748125" layer="21"/>
<rectangle x1="12.14628125" y1="1.41731875" x2="12.329159375" y2="1.42748125" layer="21"/>
<rectangle x1="12.827" y1="1.41731875" x2="13.00988125" y2="1.42748125" layer="21"/>
<rectangle x1="13.497559375" y1="1.41731875" x2="13.6906" y2="1.42748125" layer="21"/>
<rectangle x1="14.48308125" y1="1.41731875" x2="14.67611875" y2="1.42748125" layer="21"/>
<rectangle x1="15.0622" y1="1.41731875" x2="15.24508125" y2="1.42748125" layer="21"/>
<rectangle x1="16.30171875" y1="1.41731875" x2="16.4846" y2="1.42748125" layer="21"/>
<rectangle x1="0.97028125" y1="1.42748125" x2="1.4986" y2="1.437640625" layer="21"/>
<rectangle x1="1.77291875" y1="1.42748125" x2="2.23011875" y2="1.437640625" layer="21"/>
<rectangle x1="2.53491875" y1="1.42748125" x2="2.981959375" y2="1.437640625" layer="21"/>
<rectangle x1="3.439159375" y1="1.42748125" x2="3.8862" y2="1.437640625" layer="21"/>
<rectangle x1="4.333240625" y1="1.42748125" x2="4.78028125" y2="1.437640625" layer="21"/>
<rectangle x1="5.115559375" y1="1.42748125" x2="5.53211875" y2="1.437640625" layer="21"/>
<rectangle x1="6.223" y1="1.42748125" x2="6.670040625" y2="1.437640625" layer="21"/>
<rectangle x1="6.924040625" y1="1.42748125" x2="7.381240625" y2="1.437640625" layer="21"/>
<rectangle x1="7.838440625" y1="1.42748125" x2="8.295640625" y2="1.437640625" layer="21"/>
<rectangle x1="8.4582" y1="1.42748125" x2="8.88491875" y2="1.437640625" layer="21"/>
<rectangle x1="9.311640625" y1="1.42748125" x2="9.50468125" y2="1.437640625" layer="21"/>
<rectangle x1="10.581640625" y1="1.42748125" x2="10.754359375" y2="1.437640625" layer="21"/>
<rectangle x1="11.160759375" y1="1.42748125" x2="11.343640625" y2="1.437640625" layer="21"/>
<rectangle x1="12.14628125" y1="1.42748125" x2="12.329159375" y2="1.437640625" layer="21"/>
<rectangle x1="12.837159375" y1="1.42748125" x2="13.020040625" y2="1.437640625" layer="21"/>
<rectangle x1="13.497559375" y1="1.42748125" x2="13.6906" y2="1.437640625" layer="21"/>
<rectangle x1="14.493240625" y1="1.42748125" x2="14.67611875" y2="1.437640625" layer="21"/>
<rectangle x1="15.052040625" y1="1.42748125" x2="15.24508125" y2="1.437640625" layer="21"/>
<rectangle x1="16.30171875" y1="1.42748125" x2="16.4846" y2="1.437640625" layer="21"/>
<rectangle x1="0.96011875" y1="1.437640625" x2="1.488440625" y2="1.4478" layer="21"/>
<rectangle x1="1.77291875" y1="1.437640625" x2="2.23011875" y2="1.4478" layer="21"/>
<rectangle x1="2.524759375" y1="1.437640625" x2="2.981959375" y2="1.4478" layer="21"/>
<rectangle x1="3.429" y1="1.437640625" x2="3.876040625" y2="1.4478" layer="21"/>
<rectangle x1="4.333240625" y1="1.437640625" x2="4.78028125" y2="1.4478" layer="21"/>
<rectangle x1="5.115559375" y1="1.437640625" x2="5.521959375" y2="1.4478" layer="21"/>
<rectangle x1="6.223" y1="1.437640625" x2="6.670040625" y2="1.4478" layer="21"/>
<rectangle x1="6.9342" y1="1.437640625" x2="7.381240625" y2="1.4478" layer="21"/>
<rectangle x1="7.838440625" y1="1.437640625" x2="8.295640625" y2="1.4478" layer="21"/>
<rectangle x1="8.468359375" y1="1.437640625" x2="8.89508125" y2="1.4478" layer="21"/>
<rectangle x1="9.311640625" y1="1.437640625" x2="9.49451875" y2="1.4478" layer="21"/>
<rectangle x1="10.5918" y1="1.437640625" x2="10.754359375" y2="1.4478" layer="21"/>
<rectangle x1="11.160759375" y1="1.437640625" x2="11.3538" y2="1.4478" layer="21"/>
<rectangle x1="12.14628125" y1="1.437640625" x2="12.329159375" y2="1.4478" layer="21"/>
<rectangle x1="12.827" y1="1.437640625" x2="13.00988125" y2="1.4478" layer="21"/>
<rectangle x1="13.50771875" y1="1.437640625" x2="13.6906" y2="1.4478" layer="21"/>
<rectangle x1="14.48308125" y1="1.437640625" x2="14.67611875" y2="1.4478" layer="21"/>
<rectangle x1="15.0622" y1="1.437640625" x2="15.23491875" y2="1.4478" layer="21"/>
<rectangle x1="16.291559375" y1="1.437640625" x2="16.4846" y2="1.4478" layer="21"/>
<rectangle x1="0.9398" y1="1.4478" x2="1.4986" y2="1.457959375" layer="21"/>
<rectangle x1="1.77291875" y1="1.4478" x2="2.219959375" y2="1.457959375" layer="21"/>
<rectangle x1="2.53491875" y1="1.4478" x2="2.981959375" y2="1.457959375" layer="21"/>
<rectangle x1="3.429" y1="1.4478" x2="3.8862" y2="1.457959375" layer="21"/>
<rectangle x1="4.333240625" y1="1.4478" x2="4.78028125" y2="1.457959375" layer="21"/>
<rectangle x1="5.115559375" y1="1.4478" x2="5.53211875" y2="1.457959375" layer="21"/>
<rectangle x1="6.223" y1="1.4478" x2="6.670040625" y2="1.457959375" layer="21"/>
<rectangle x1="6.9342" y1="1.4478" x2="7.37108125" y2="1.457959375" layer="21"/>
<rectangle x1="7.82828125" y1="1.4478" x2="8.28548125" y2="1.457959375" layer="21"/>
<rectangle x1="8.468359375" y1="1.4478" x2="8.89508125" y2="1.457959375" layer="21"/>
<rectangle x1="9.311640625" y1="1.4478" x2="9.50468125" y2="1.457959375" layer="21"/>
<rectangle x1="10.581640625" y1="1.4478" x2="10.754359375" y2="1.457959375" layer="21"/>
<rectangle x1="11.160759375" y1="1.4478" x2="11.343640625" y2="1.457959375" layer="21"/>
<rectangle x1="12.14628125" y1="1.4478" x2="12.329159375" y2="1.457959375" layer="21"/>
<rectangle x1="12.837159375" y1="1.4478" x2="13.020040625" y2="1.457959375" layer="21"/>
<rectangle x1="13.497559375" y1="1.4478" x2="13.6906" y2="1.457959375" layer="21"/>
<rectangle x1="14.48308125" y1="1.4478" x2="14.665959375" y2="1.457959375" layer="21"/>
<rectangle x1="15.052040625" y1="1.4478" x2="15.24508125" y2="1.457959375" layer="21"/>
<rectangle x1="16.30171875" y1="1.4478" x2="16.4846" y2="1.457959375" layer="21"/>
<rectangle x1="0.91948125" y1="1.457959375" x2="1.488440625" y2="1.46811875" layer="21"/>
<rectangle x1="1.78308125" y1="1.457959375" x2="2.23011875" y2="1.46811875" layer="21"/>
<rectangle x1="2.53491875" y1="1.457959375" x2="2.981959375" y2="1.46811875" layer="21"/>
<rectangle x1="3.429" y1="1.457959375" x2="3.876040625" y2="1.46811875" layer="21"/>
<rectangle x1="4.333240625" y1="1.457959375" x2="4.78028125" y2="1.46811875" layer="21"/>
<rectangle x1="5.1054" y1="1.457959375" x2="5.53211875" y2="1.46811875" layer="21"/>
<rectangle x1="6.212840625" y1="1.457959375" x2="6.670040625" y2="1.46811875" layer="21"/>
<rectangle x1="6.924040625" y1="1.457959375" x2="7.381240625" y2="1.46811875" layer="21"/>
<rectangle x1="7.82828125" y1="1.457959375" x2="8.28548125" y2="1.46811875" layer="21"/>
<rectangle x1="8.468359375" y1="1.457959375" x2="8.89508125" y2="1.46811875" layer="21"/>
<rectangle x1="9.311640625" y1="1.457959375" x2="9.50468125" y2="1.46811875" layer="21"/>
<rectangle x1="10.581640625" y1="1.457959375" x2="10.754359375" y2="1.46811875" layer="21"/>
<rectangle x1="11.160759375" y1="1.457959375" x2="11.3538" y2="1.46811875" layer="21"/>
<rectangle x1="12.14628125" y1="1.457959375" x2="12.329159375" y2="1.46811875" layer="21"/>
<rectangle x1="12.827" y1="1.457959375" x2="13.00988125" y2="1.46811875" layer="21"/>
<rectangle x1="13.50771875" y1="1.457959375" x2="13.6906" y2="1.46811875" layer="21"/>
<rectangle x1="14.493240625" y1="1.457959375" x2="14.67611875" y2="1.46811875" layer="21"/>
<rectangle x1="15.052040625" y1="1.457959375" x2="15.24508125" y2="1.46811875" layer="21"/>
<rectangle x1="16.291559375" y1="1.457959375" x2="16.474440625" y2="1.46811875" layer="21"/>
<rectangle x1="0.90931875" y1="1.46811875" x2="1.488440625" y2="1.47828125" layer="21"/>
<rectangle x1="1.77291875" y1="1.46811875" x2="2.219959375" y2="1.47828125" layer="21"/>
<rectangle x1="2.524759375" y1="1.46811875" x2="2.981959375" y2="1.47828125" layer="21"/>
<rectangle x1="3.439159375" y1="1.46811875" x2="3.876040625" y2="1.47828125" layer="21"/>
<rectangle x1="4.333240625" y1="1.46811875" x2="4.78028125" y2="1.47828125" layer="21"/>
<rectangle x1="5.1054" y1="1.46811875" x2="5.53211875" y2="1.47828125" layer="21"/>
<rectangle x1="6.212840625" y1="1.46811875" x2="6.670040625" y2="1.47828125" layer="21"/>
<rectangle x1="6.9342" y1="1.46811875" x2="7.37108125" y2="1.47828125" layer="21"/>
<rectangle x1="7.81811875" y1="1.46811875" x2="8.27531875" y2="1.47828125" layer="21"/>
<rectangle x1="8.47851875" y1="1.46811875" x2="8.905240625" y2="1.47828125" layer="21"/>
<rectangle x1="9.311640625" y1="1.46811875" x2="9.50468125" y2="1.47828125" layer="21"/>
<rectangle x1="10.581640625" y1="1.46811875" x2="10.754359375" y2="1.47828125" layer="21"/>
<rectangle x1="11.160759375" y1="1.46811875" x2="11.3538" y2="1.47828125" layer="21"/>
<rectangle x1="12.14628125" y1="1.46811875" x2="12.329159375" y2="1.47828125" layer="21"/>
<rectangle x1="12.827" y1="1.46811875" x2="13.020040625" y2="1.47828125" layer="21"/>
<rectangle x1="13.497559375" y1="1.46811875" x2="13.700759375" y2="1.47828125" layer="21"/>
<rectangle x1="14.493240625" y1="1.46811875" x2="14.67611875" y2="1.47828125" layer="21"/>
<rectangle x1="15.052040625" y1="1.46811875" x2="15.24508125" y2="1.47828125" layer="21"/>
<rectangle x1="16.291559375" y1="1.46811875" x2="16.4846" y2="1.47828125" layer="21"/>
<rectangle x1="0.889" y1="1.47828125" x2="1.488440625" y2="1.488440625" layer="21"/>
<rectangle x1="1.78308125" y1="1.47828125" x2="2.23011875" y2="1.488440625" layer="21"/>
<rectangle x1="2.53491875" y1="1.47828125" x2="2.981959375" y2="1.488440625" layer="21"/>
<rectangle x1="3.429" y1="1.47828125" x2="3.8862" y2="1.488440625" layer="21"/>
<rectangle x1="4.333240625" y1="1.47828125" x2="4.78028125" y2="1.488440625" layer="21"/>
<rectangle x1="5.115559375" y1="1.47828125" x2="5.53211875" y2="1.488440625" layer="21"/>
<rectangle x1="6.212840625" y1="1.47828125" x2="6.670040625" y2="1.488440625" layer="21"/>
<rectangle x1="6.924040625" y1="1.47828125" x2="7.381240625" y2="1.488440625" layer="21"/>
<rectangle x1="7.81811875" y1="1.47828125" x2="8.27531875" y2="1.488440625" layer="21"/>
<rectangle x1="8.47851875" y1="1.47828125" x2="8.905240625" y2="1.488440625" layer="21"/>
<rectangle x1="9.3218" y1="1.47828125" x2="9.50468125" y2="1.488440625" layer="21"/>
<rectangle x1="10.581640625" y1="1.47828125" x2="10.754359375" y2="1.488440625" layer="21"/>
<rectangle x1="11.160759375" y1="1.47828125" x2="11.343640625" y2="1.488440625" layer="21"/>
<rectangle x1="12.14628125" y1="1.47828125" x2="12.329159375" y2="1.488440625" layer="21"/>
<rectangle x1="12.837159375" y1="1.47828125" x2="13.020040625" y2="1.488440625" layer="21"/>
<rectangle x1="13.50771875" y1="1.47828125" x2="13.6906" y2="1.488440625" layer="21"/>
<rectangle x1="14.48308125" y1="1.47828125" x2="14.665959375" y2="1.488440625" layer="21"/>
<rectangle x1="15.0622" y1="1.47828125" x2="15.24508125" y2="1.488440625" layer="21"/>
<rectangle x1="16.291559375" y1="1.47828125" x2="16.474440625" y2="1.488440625" layer="21"/>
<rectangle x1="0.86868125" y1="1.488440625" x2="1.488440625" y2="1.4986" layer="21"/>
<rectangle x1="1.77291875" y1="1.488440625" x2="2.23011875" y2="1.4986" layer="21"/>
<rectangle x1="2.53491875" y1="1.488440625" x2="2.99211875" y2="1.4986" layer="21"/>
<rectangle x1="3.429" y1="1.488440625" x2="3.8862" y2="1.4986" layer="21"/>
<rectangle x1="4.32308125" y1="1.488440625" x2="4.78028125" y2="1.4986" layer="21"/>
<rectangle x1="5.115559375" y1="1.488440625" x2="5.53211875" y2="1.4986" layer="21"/>
<rectangle x1="6.223" y1="1.488440625" x2="6.670040625" y2="1.4986" layer="21"/>
<rectangle x1="6.924040625" y1="1.488440625" x2="7.381240625" y2="1.4986" layer="21"/>
<rectangle x1="7.81811875" y1="1.488440625" x2="8.27531875" y2="1.4986" layer="21"/>
<rectangle x1="8.48868125" y1="1.488440625" x2="8.905240625" y2="1.4986" layer="21"/>
<rectangle x1="9.3218" y1="1.488440625" x2="9.50468125" y2="1.4986" layer="21"/>
<rectangle x1="10.581640625" y1="1.488440625" x2="10.754359375" y2="1.4986" layer="21"/>
<rectangle x1="11.160759375" y1="1.488440625" x2="11.343640625" y2="1.4986" layer="21"/>
<rectangle x1="12.14628125" y1="1.488440625" x2="12.329159375" y2="1.4986" layer="21"/>
<rectangle x1="12.827" y1="1.488440625" x2="13.020040625" y2="1.4986" layer="21"/>
<rectangle x1="13.497559375" y1="1.488440625" x2="13.6906" y2="1.4986" layer="21"/>
<rectangle x1="14.493240625" y1="1.488440625" x2="14.67611875" y2="1.4986" layer="21"/>
<rectangle x1="15.0622" y1="1.488440625" x2="15.255240625" y2="1.4986" layer="21"/>
<rectangle x1="16.291559375" y1="1.488440625" x2="16.4846" y2="1.4986" layer="21"/>
<rectangle x1="0.848359375" y1="1.4986" x2="1.47828125" y2="1.508759375" layer="21"/>
<rectangle x1="1.78308125" y1="1.4986" x2="2.219959375" y2="1.508759375" layer="21"/>
<rectangle x1="2.53491875" y1="1.4986" x2="2.981959375" y2="1.508759375" layer="21"/>
<rectangle x1="3.429" y1="1.4986" x2="3.876040625" y2="1.508759375" layer="21"/>
<rectangle x1="4.333240625" y1="1.4986" x2="4.78028125" y2="1.508759375" layer="21"/>
<rectangle x1="5.115559375" y1="1.4986" x2="5.53211875" y2="1.508759375" layer="21"/>
<rectangle x1="6.212840625" y1="1.4986" x2="6.65988125" y2="1.508759375" layer="21"/>
<rectangle x1="6.9342" y1="1.4986" x2="7.381240625" y2="1.508759375" layer="21"/>
<rectangle x1="7.807959375" y1="1.4986" x2="8.265159375" y2="1.508759375" layer="21"/>
<rectangle x1="8.48868125" y1="1.4986" x2="8.9154" y2="1.508759375" layer="21"/>
<rectangle x1="9.3218" y1="1.4986" x2="9.50468125" y2="1.508759375" layer="21"/>
<rectangle x1="10.57148125" y1="1.4986" x2="10.754359375" y2="1.508759375" layer="21"/>
<rectangle x1="11.160759375" y1="1.4986" x2="11.343640625" y2="1.508759375" layer="21"/>
<rectangle x1="12.14628125" y1="1.4986" x2="12.329159375" y2="1.508759375" layer="21"/>
<rectangle x1="12.827" y1="1.4986" x2="13.00988125" y2="1.508759375" layer="21"/>
<rectangle x1="13.50771875" y1="1.4986" x2="13.700759375" y2="1.508759375" layer="21"/>
<rectangle x1="14.48308125" y1="1.4986" x2="14.67611875" y2="1.508759375" layer="21"/>
<rectangle x1="15.0622" y1="1.4986" x2="15.24508125" y2="1.508759375" layer="21"/>
<rectangle x1="16.291559375" y1="1.4986" x2="16.474440625" y2="1.508759375" layer="21"/>
<rectangle x1="0.828040625" y1="1.508759375" x2="1.47828125" y2="1.51891875" layer="21"/>
<rectangle x1="1.77291875" y1="1.508759375" x2="2.23011875" y2="1.51891875" layer="21"/>
<rectangle x1="2.53491875" y1="1.508759375" x2="2.981959375" y2="1.51891875" layer="21"/>
<rectangle x1="3.429" y1="1.508759375" x2="3.8862" y2="1.51891875" layer="21"/>
<rectangle x1="4.333240625" y1="1.508759375" x2="4.78028125" y2="1.51891875" layer="21"/>
<rectangle x1="5.1054" y1="1.508759375" x2="5.54228125" y2="1.51891875" layer="21"/>
<rectangle x1="6.212840625" y1="1.508759375" x2="6.670040625" y2="1.51891875" layer="21"/>
<rectangle x1="6.9342" y1="1.508759375" x2="7.37108125" y2="1.51891875" layer="21"/>
<rectangle x1="7.807959375" y1="1.508759375" x2="8.265159375" y2="1.51891875" layer="21"/>
<rectangle x1="8.48868125" y1="1.508759375" x2="8.9154" y2="1.51891875" layer="21"/>
<rectangle x1="9.3218" y1="1.508759375" x2="9.514840625" y2="1.51891875" layer="21"/>
<rectangle x1="10.57148125" y1="1.508759375" x2="10.754359375" y2="1.51891875" layer="21"/>
<rectangle x1="11.160759375" y1="1.508759375" x2="11.343640625" y2="1.51891875" layer="21"/>
<rectangle x1="12.14628125" y1="1.508759375" x2="12.329159375" y2="1.51891875" layer="21"/>
<rectangle x1="12.827" y1="1.508759375" x2="13.00988125" y2="1.51891875" layer="21"/>
<rectangle x1="13.50771875" y1="1.508759375" x2="13.700759375" y2="1.51891875" layer="21"/>
<rectangle x1="14.48308125" y1="1.508759375" x2="14.67611875" y2="1.51891875" layer="21"/>
<rectangle x1="15.0622" y1="1.508759375" x2="15.255240625" y2="1.51891875" layer="21"/>
<rectangle x1="16.2814" y1="1.508759375" x2="16.474440625" y2="1.51891875" layer="21"/>
<rectangle x1="0.797559375" y1="1.51891875" x2="1.47828125" y2="1.52908125" layer="21"/>
<rectangle x1="1.78308125" y1="1.51891875" x2="2.219959375" y2="1.52908125" layer="21"/>
<rectangle x1="2.524759375" y1="1.51891875" x2="2.99211875" y2="1.52908125" layer="21"/>
<rectangle x1="3.429" y1="1.51891875" x2="3.8862" y2="1.52908125" layer="21"/>
<rectangle x1="4.333240625" y1="1.51891875" x2="4.78028125" y2="1.52908125" layer="21"/>
<rectangle x1="5.115559375" y1="1.51891875" x2="5.53211875" y2="1.52908125" layer="21"/>
<rectangle x1="6.20268125" y1="1.51891875" x2="6.670040625" y2="1.52908125" layer="21"/>
<rectangle x1="6.924040625" y1="1.51891875" x2="7.381240625" y2="1.52908125" layer="21"/>
<rectangle x1="7.7978" y1="1.51891875" x2="8.255" y2="1.52908125" layer="21"/>
<rectangle x1="8.48868125" y1="1.51891875" x2="8.925559375" y2="1.52908125" layer="21"/>
<rectangle x1="9.3218" y1="1.51891875" x2="9.514840625" y2="1.52908125" layer="21"/>
<rectangle x1="10.57148125" y1="1.51891875" x2="10.754359375" y2="1.52908125" layer="21"/>
<rectangle x1="11.160759375" y1="1.51891875" x2="11.3538" y2="1.52908125" layer="21"/>
<rectangle x1="12.14628125" y1="1.51891875" x2="12.329159375" y2="1.52908125" layer="21"/>
<rectangle x1="12.837159375" y1="1.51891875" x2="13.020040625" y2="1.52908125" layer="21"/>
<rectangle x1="13.497559375" y1="1.51891875" x2="13.700759375" y2="1.52908125" layer="21"/>
<rectangle x1="14.48308125" y1="1.51891875" x2="14.665959375" y2="1.52908125" layer="21"/>
<rectangle x1="15.0622" y1="1.51891875" x2="15.255240625" y2="1.52908125" layer="21"/>
<rectangle x1="16.2814" y1="1.51891875" x2="16.474440625" y2="1.52908125" layer="21"/>
<rectangle x1="0.76708125" y1="1.52908125" x2="1.47828125" y2="1.539240625" layer="21"/>
<rectangle x1="1.77291875" y1="1.52908125" x2="2.23011875" y2="1.539240625" layer="21"/>
<rectangle x1="2.53491875" y1="1.52908125" x2="2.99211875" y2="1.539240625" layer="21"/>
<rectangle x1="3.429" y1="1.52908125" x2="3.8862" y2="1.539240625" layer="21"/>
<rectangle x1="4.32308125" y1="1.52908125" x2="4.78028125" y2="1.539240625" layer="21"/>
<rectangle x1="5.115559375" y1="1.52908125" x2="5.54228125" y2="1.539240625" layer="21"/>
<rectangle x1="6.20268125" y1="1.52908125" x2="6.65988125" y2="1.539240625" layer="21"/>
<rectangle x1="6.9342" y1="1.52908125" x2="7.37108125" y2="1.539240625" layer="21"/>
<rectangle x1="7.7978" y1="1.52908125" x2="8.255" y2="1.539240625" layer="21"/>
<rectangle x1="8.498840625" y1="1.52908125" x2="8.925559375" y2="1.539240625" layer="21"/>
<rectangle x1="9.3218" y1="1.52908125" x2="9.514840625" y2="1.539240625" layer="21"/>
<rectangle x1="10.57148125" y1="1.52908125" x2="10.754359375" y2="1.539240625" layer="21"/>
<rectangle x1="11.160759375" y1="1.52908125" x2="11.343640625" y2="1.539240625" layer="21"/>
<rectangle x1="12.14628125" y1="1.52908125" x2="12.329159375" y2="1.539240625" layer="21"/>
<rectangle x1="12.827" y1="1.52908125" x2="13.020040625" y2="1.539240625" layer="21"/>
<rectangle x1="13.497559375" y1="1.52908125" x2="13.700759375" y2="1.539240625" layer="21"/>
<rectangle x1="14.48308125" y1="1.52908125" x2="14.67611875" y2="1.539240625" layer="21"/>
<rectangle x1="15.0622" y1="1.52908125" x2="15.255240625" y2="1.539240625" layer="21"/>
<rectangle x1="16.2814" y1="1.52908125" x2="16.474440625" y2="1.539240625" layer="21"/>
<rectangle x1="0.7366" y1="1.539240625" x2="1.46811875" y2="1.5494" layer="21"/>
<rectangle x1="1.77291875" y1="1.539240625" x2="2.23011875" y2="1.5494" layer="21"/>
<rectangle x1="2.53491875" y1="1.539240625" x2="2.99211875" y2="1.5494" layer="21"/>
<rectangle x1="3.429" y1="1.539240625" x2="3.8862" y2="1.5494" layer="21"/>
<rectangle x1="4.333240625" y1="1.539240625" x2="4.78028125" y2="1.5494" layer="21"/>
<rectangle x1="5.115559375" y1="1.539240625" x2="5.54228125" y2="1.5494" layer="21"/>
<rectangle x1="6.20268125" y1="1.539240625" x2="6.65988125" y2="1.5494" layer="21"/>
<rectangle x1="6.924040625" y1="1.539240625" x2="7.381240625" y2="1.5494" layer="21"/>
<rectangle x1="7.787640625" y1="1.539240625" x2="8.255" y2="1.5494" layer="21"/>
<rectangle x1="8.498840625" y1="1.539240625" x2="8.925559375" y2="1.5494" layer="21"/>
<rectangle x1="9.331959375" y1="1.539240625" x2="9.514840625" y2="1.5494" layer="21"/>
<rectangle x1="10.56131875" y1="1.539240625" x2="10.754359375" y2="1.5494" layer="21"/>
<rectangle x1="11.160759375" y1="1.539240625" x2="11.3538" y2="1.5494" layer="21"/>
<rectangle x1="12.14628125" y1="1.539240625" x2="12.329159375" y2="1.5494" layer="21"/>
<rectangle x1="12.837159375" y1="1.539240625" x2="13.00988125" y2="1.5494" layer="21"/>
<rectangle x1="13.50771875" y1="1.539240625" x2="13.700759375" y2="1.5494" layer="21"/>
<rectangle x1="14.48308125" y1="1.539240625" x2="14.67611875" y2="1.5494" layer="21"/>
<rectangle x1="15.072359375" y1="1.539240625" x2="15.255240625" y2="1.5494" layer="21"/>
<rectangle x1="16.2814" y1="1.539240625" x2="16.474440625" y2="1.5494" layer="21"/>
<rectangle x1="0.70611875" y1="1.5494" x2="1.46811875" y2="1.559559375" layer="21"/>
<rectangle x1="1.77291875" y1="1.5494" x2="2.23011875" y2="1.559559375" layer="21"/>
<rectangle x1="2.524759375" y1="1.5494" x2="2.99211875" y2="1.559559375" layer="21"/>
<rectangle x1="3.429" y1="1.5494" x2="3.8862" y2="1.559559375" layer="21"/>
<rectangle x1="4.32308125" y1="1.5494" x2="4.78028125" y2="1.559559375" layer="21"/>
<rectangle x1="5.115559375" y1="1.5494" x2="5.552440625" y2="1.559559375" layer="21"/>
<rectangle x1="6.20268125" y1="1.5494" x2="6.65988125" y2="1.559559375" layer="21"/>
<rectangle x1="6.9342" y1="1.5494" x2="7.381240625" y2="1.559559375" layer="21"/>
<rectangle x1="7.787640625" y1="1.5494" x2="8.244840625" y2="1.559559375" layer="21"/>
<rectangle x1="8.498840625" y1="1.5494" x2="8.93571875" y2="1.559559375" layer="21"/>
<rectangle x1="9.331959375" y1="1.5494" x2="9.525" y2="1.559559375" layer="21"/>
<rectangle x1="10.56131875" y1="1.5494" x2="10.754359375" y2="1.559559375" layer="21"/>
<rectangle x1="11.160759375" y1="1.5494" x2="11.3538" y2="1.559559375" layer="21"/>
<rectangle x1="12.14628125" y1="1.5494" x2="12.329159375" y2="1.559559375" layer="21"/>
<rectangle x1="12.827" y1="1.5494" x2="13.020040625" y2="1.559559375" layer="21"/>
<rectangle x1="13.497559375" y1="1.5494" x2="13.700759375" y2="1.559559375" layer="21"/>
<rectangle x1="14.48308125" y1="1.5494" x2="14.665959375" y2="1.559559375" layer="21"/>
<rectangle x1="15.072359375" y1="1.5494" x2="15.2654" y2="1.559559375" layer="21"/>
<rectangle x1="16.2814" y1="1.5494" x2="16.46428125" y2="1.559559375" layer="21"/>
<rectangle x1="0.6858" y1="1.559559375" x2="1.46811875" y2="1.56971875" layer="21"/>
<rectangle x1="1.78308125" y1="1.559559375" x2="2.219959375" y2="1.56971875" layer="21"/>
<rectangle x1="2.53491875" y1="1.559559375" x2="2.99211875" y2="1.56971875" layer="21"/>
<rectangle x1="3.429" y1="1.559559375" x2="3.8862" y2="1.56971875" layer="21"/>
<rectangle x1="4.32308125" y1="1.559559375" x2="4.78028125" y2="1.56971875" layer="21"/>
<rectangle x1="5.115559375" y1="1.559559375" x2="5.552440625" y2="1.56971875" layer="21"/>
<rectangle x1="6.19251875" y1="1.559559375" x2="6.65988125" y2="1.56971875" layer="21"/>
<rectangle x1="6.924040625" y1="1.559559375" x2="7.37108125" y2="1.56971875" layer="21"/>
<rectangle x1="7.77748125" y1="1.559559375" x2="8.244840625" y2="1.56971875" layer="21"/>
<rectangle x1="8.509" y1="1.559559375" x2="8.93571875" y2="1.56971875" layer="21"/>
<rectangle x1="9.331959375" y1="1.559559375" x2="9.525" y2="1.56971875" layer="21"/>
<rectangle x1="10.56131875" y1="1.559559375" x2="10.754359375" y2="1.56971875" layer="21"/>
<rectangle x1="11.160759375" y1="1.559559375" x2="11.343640625" y2="1.56971875" layer="21"/>
<rectangle x1="12.14628125" y1="1.559559375" x2="12.329159375" y2="1.56971875" layer="21"/>
<rectangle x1="12.827" y1="1.559559375" x2="13.00988125" y2="1.56971875" layer="21"/>
<rectangle x1="13.50771875" y1="1.559559375" x2="13.71091875" y2="1.56971875" layer="21"/>
<rectangle x1="14.48308125" y1="1.559559375" x2="14.67611875" y2="1.56971875" layer="21"/>
<rectangle x1="15.072359375" y1="1.559559375" x2="15.2654" y2="1.56971875" layer="21"/>
<rectangle x1="16.271240625" y1="1.559559375" x2="16.46428125" y2="1.56971875" layer="21"/>
<rectangle x1="0.645159375" y1="1.56971875" x2="1.457959375" y2="1.57988125" layer="21"/>
<rectangle x1="1.77291875" y1="1.56971875" x2="2.23011875" y2="1.57988125" layer="21"/>
<rectangle x1="2.53491875" y1="1.56971875" x2="2.99211875" y2="1.57988125" layer="21"/>
<rectangle x1="3.418840625" y1="1.56971875" x2="3.896359375" y2="1.57988125" layer="21"/>
<rectangle x1="4.333240625" y1="1.56971875" x2="4.78028125" y2="1.57988125" layer="21"/>
<rectangle x1="5.115559375" y1="1.56971875" x2="5.552440625" y2="1.57988125" layer="21"/>
<rectangle x1="6.19251875" y1="1.56971875" x2="6.65988125" y2="1.57988125" layer="21"/>
<rectangle x1="6.9342" y1="1.56971875" x2="7.381240625" y2="1.57988125" layer="21"/>
<rectangle x1="7.77748125" y1="1.56971875" x2="8.23468125" y2="1.57988125" layer="21"/>
<rectangle x1="8.509" y1="1.56971875" x2="8.94588125" y2="1.57988125" layer="21"/>
<rectangle x1="9.331959375" y1="1.56971875" x2="9.525" y2="1.57988125" layer="21"/>
<rectangle x1="10.551159375" y1="1.56971875" x2="10.754359375" y2="1.57988125" layer="21"/>
<rectangle x1="11.160759375" y1="1.56971875" x2="11.343640625" y2="1.57988125" layer="21"/>
<rectangle x1="12.14628125" y1="1.56971875" x2="12.329159375" y2="1.57988125" layer="21"/>
<rectangle x1="12.827" y1="1.56971875" x2="13.020040625" y2="1.57988125" layer="21"/>
<rectangle x1="13.50771875" y1="1.56971875" x2="13.71091875" y2="1.57988125" layer="21"/>
<rectangle x1="14.48308125" y1="1.56971875" x2="14.67611875" y2="1.57988125" layer="21"/>
<rectangle x1="15.072359375" y1="1.56971875" x2="15.2654" y2="1.57988125" layer="21"/>
<rectangle x1="16.271240625" y1="1.56971875" x2="16.46428125" y2="1.57988125" layer="21"/>
<rectangle x1="0.61468125" y1="1.57988125" x2="1.457959375" y2="1.590040625" layer="21"/>
<rectangle x1="1.78308125" y1="1.57988125" x2="2.23011875" y2="1.590040625" layer="21"/>
<rectangle x1="2.53491875" y1="1.57988125" x2="3.00228125" y2="1.590040625" layer="21"/>
<rectangle x1="3.429" y1="1.57988125" x2="3.896359375" y2="1.590040625" layer="21"/>
<rectangle x1="4.32308125" y1="1.57988125" x2="4.78028125" y2="1.590040625" layer="21"/>
<rectangle x1="5.1054" y1="1.57988125" x2="5.5626" y2="1.590040625" layer="21"/>
<rectangle x1="6.19251875" y1="1.57988125" x2="6.65988125" y2="1.590040625" layer="21"/>
<rectangle x1="6.924040625" y1="1.57988125" x2="7.381240625" y2="1.590040625" layer="21"/>
<rectangle x1="7.77748125" y1="1.57988125" x2="8.23468125" y2="1.590040625" layer="21"/>
<rectangle x1="8.509" y1="1.57988125" x2="8.94588125" y2="1.590040625" layer="21"/>
<rectangle x1="9.331959375" y1="1.57988125" x2="9.535159375" y2="1.590040625" layer="21"/>
<rectangle x1="10.551159375" y1="1.57988125" x2="10.754359375" y2="1.590040625" layer="21"/>
<rectangle x1="11.160759375" y1="1.57988125" x2="11.343640625" y2="1.590040625" layer="21"/>
<rectangle x1="12.14628125" y1="1.57988125" x2="12.329159375" y2="1.590040625" layer="21"/>
<rectangle x1="12.837159375" y1="1.57988125" x2="13.00988125" y2="1.590040625" layer="21"/>
<rectangle x1="13.497559375" y1="1.57988125" x2="13.71091875" y2="1.590040625" layer="21"/>
<rectangle x1="14.48308125" y1="1.57988125" x2="14.665959375" y2="1.590040625" layer="21"/>
<rectangle x1="15.072359375" y1="1.57988125" x2="15.2654" y2="1.590040625" layer="21"/>
<rectangle x1="16.271240625" y1="1.57988125" x2="16.46428125" y2="1.590040625" layer="21"/>
<rectangle x1="0.5842" y1="1.590040625" x2="1.4478" y2="1.6002" layer="21"/>
<rectangle x1="1.77291875" y1="1.590040625" x2="2.219959375" y2="1.6002" layer="21"/>
<rectangle x1="2.53491875" y1="1.590040625" x2="3.00228125" y2="1.6002" layer="21"/>
<rectangle x1="3.418840625" y1="1.590040625" x2="3.896359375" y2="1.6002" layer="21"/>
<rectangle x1="4.32308125" y1="1.590040625" x2="4.78028125" y2="1.6002" layer="21"/>
<rectangle x1="5.115559375" y1="1.590040625" x2="5.5626" y2="1.6002" layer="21"/>
<rectangle x1="6.182359375" y1="1.590040625" x2="6.65988125" y2="1.6002" layer="21"/>
<rectangle x1="6.9342" y1="1.590040625" x2="7.381240625" y2="1.6002" layer="21"/>
<rectangle x1="7.76731875" y1="1.590040625" x2="8.23468125" y2="1.6002" layer="21"/>
<rectangle x1="8.519159375" y1="1.590040625" x2="8.94588125" y2="1.6002" layer="21"/>
<rectangle x1="9.34211875" y1="1.590040625" x2="9.535159375" y2="1.6002" layer="21"/>
<rectangle x1="10.541" y1="1.590040625" x2="10.754359375" y2="1.6002" layer="21"/>
<rectangle x1="11.160759375" y1="1.590040625" x2="11.343640625" y2="1.6002" layer="21"/>
<rectangle x1="12.14628125" y1="1.590040625" x2="12.329159375" y2="1.6002" layer="21"/>
<rectangle x1="12.827" y1="1.590040625" x2="13.020040625" y2="1.6002" layer="21"/>
<rectangle x1="13.497559375" y1="1.590040625" x2="13.71091875" y2="1.6002" layer="21"/>
<rectangle x1="14.48308125" y1="1.590040625" x2="14.665959375" y2="1.6002" layer="21"/>
<rectangle x1="15.08251875" y1="1.590040625" x2="15.275559375" y2="1.6002" layer="21"/>
<rectangle x1="16.26108125" y1="1.590040625" x2="16.45411875" y2="1.6002" layer="21"/>
<rectangle x1="0.55371875" y1="1.6002" x2="1.4478" y2="1.610359375" layer="21"/>
<rectangle x1="1.77291875" y1="1.6002" x2="2.23011875" y2="1.610359375" layer="21"/>
<rectangle x1="2.524759375" y1="1.6002" x2="3.00228125" y2="1.610359375" layer="21"/>
<rectangle x1="3.429" y1="1.6002" x2="3.896359375" y2="1.610359375" layer="21"/>
<rectangle x1="4.32308125" y1="1.6002" x2="4.77011875" y2="1.610359375" layer="21"/>
<rectangle x1="5.115559375" y1="1.6002" x2="5.572759375" y2="1.610359375" layer="21"/>
<rectangle x1="6.182359375" y1="1.6002" x2="6.64971875" y2="1.610359375" layer="21"/>
<rectangle x1="6.924040625" y1="1.6002" x2="7.37108125" y2="1.610359375" layer="21"/>
<rectangle x1="7.76731875" y1="1.6002" x2="8.22451875" y2="1.610359375" layer="21"/>
<rectangle x1="8.519159375" y1="1.6002" x2="8.956040625" y2="1.610359375" layer="21"/>
<rectangle x1="9.34211875" y1="1.6002" x2="9.535159375" y2="1.610359375" layer="21"/>
<rectangle x1="10.541" y1="1.6002" x2="10.754359375" y2="1.610359375" layer="21"/>
<rectangle x1="11.17091875" y1="1.6002" x2="11.3538" y2="1.610359375" layer="21"/>
<rectangle x1="12.14628125" y1="1.6002" x2="12.329159375" y2="1.610359375" layer="21"/>
<rectangle x1="12.837159375" y1="1.6002" x2="13.020040625" y2="1.610359375" layer="21"/>
<rectangle x1="13.50771875" y1="1.6002" x2="13.72108125" y2="1.610359375" layer="21"/>
<rectangle x1="14.48308125" y1="1.6002" x2="14.67611875" y2="1.610359375" layer="21"/>
<rectangle x1="15.08251875" y1="1.6002" x2="15.275559375" y2="1.610359375" layer="21"/>
<rectangle x1="16.26108125" y1="1.6002" x2="16.45411875" y2="1.610359375" layer="21"/>
<rectangle x1="0.523240625" y1="1.610359375" x2="1.437640625" y2="1.62051875" layer="21"/>
<rectangle x1="1.77291875" y1="1.610359375" x2="2.219959375" y2="1.62051875" layer="21"/>
<rectangle x1="2.53491875" y1="1.610359375" x2="3.012440625" y2="1.62051875" layer="21"/>
<rectangle x1="3.418840625" y1="1.610359375" x2="3.90651875" y2="1.62051875" layer="21"/>
<rectangle x1="4.32308125" y1="1.610359375" x2="4.78028125" y2="1.62051875" layer="21"/>
<rectangle x1="5.115559375" y1="1.610359375" x2="5.572759375" y2="1.62051875" layer="21"/>
<rectangle x1="6.1722" y1="1.610359375" x2="6.64971875" y2="1.62051875" layer="21"/>
<rectangle x1="6.9342" y1="1.610359375" x2="7.381240625" y2="1.62051875" layer="21"/>
<rectangle x1="7.757159375" y1="1.610359375" x2="8.22451875" y2="1.62051875" layer="21"/>
<rectangle x1="8.52931875" y1="1.610359375" x2="8.956040625" y2="1.62051875" layer="21"/>
<rectangle x1="9.34211875" y1="1.610359375" x2="9.54531875" y2="1.62051875" layer="21"/>
<rectangle x1="10.541" y1="1.610359375" x2="10.754359375" y2="1.62051875" layer="21"/>
<rectangle x1="11.160759375" y1="1.610359375" x2="11.343640625" y2="1.62051875" layer="21"/>
<rectangle x1="12.14628125" y1="1.610359375" x2="12.329159375" y2="1.62051875" layer="21"/>
<rectangle x1="12.827" y1="1.610359375" x2="13.020040625" y2="1.62051875" layer="21"/>
<rectangle x1="13.497559375" y1="1.610359375" x2="13.72108125" y2="1.62051875" layer="21"/>
<rectangle x1="14.48308125" y1="1.610359375" x2="14.665959375" y2="1.62051875" layer="21"/>
<rectangle x1="15.08251875" y1="1.610359375" x2="15.28571875" y2="1.62051875" layer="21"/>
<rectangle x1="16.26108125" y1="1.610359375" x2="16.45411875" y2="1.62051875" layer="21"/>
<rectangle x1="0.492759375" y1="1.62051875" x2="1.437640625" y2="1.63068125" layer="21"/>
<rectangle x1="1.78308125" y1="1.62051875" x2="2.23011875" y2="1.63068125" layer="21"/>
<rectangle x1="2.53491875" y1="1.62051875" x2="3.00228125" y2="1.63068125" layer="21"/>
<rectangle x1="3.418840625" y1="1.62051875" x2="3.90651875" y2="1.63068125" layer="21"/>
<rectangle x1="4.32308125" y1="1.62051875" x2="4.78028125" y2="1.63068125" layer="21"/>
<rectangle x1="5.1054" y1="1.62051875" x2="5.58291875" y2="1.63068125" layer="21"/>
<rectangle x1="6.162040625" y1="1.62051875" x2="6.64971875" y2="1.63068125" layer="21"/>
<rectangle x1="6.924040625" y1="1.62051875" x2="7.381240625" y2="1.63068125" layer="21"/>
<rectangle x1="7.757159375" y1="1.62051875" x2="8.22451875" y2="1.63068125" layer="21"/>
<rectangle x1="8.52931875" y1="1.62051875" x2="8.9662" y2="1.63068125" layer="21"/>
<rectangle x1="9.35228125" y1="1.62051875" x2="9.54531875" y2="1.63068125" layer="21"/>
<rectangle x1="10.530840625" y1="1.62051875" x2="10.754359375" y2="1.63068125" layer="21"/>
<rectangle x1="11.160759375" y1="1.62051875" x2="11.3538" y2="1.63068125" layer="21"/>
<rectangle x1="12.14628125" y1="1.62051875" x2="12.329159375" y2="1.63068125" layer="21"/>
<rectangle x1="12.827" y1="1.62051875" x2="13.00988125" y2="1.63068125" layer="21"/>
<rectangle x1="13.50771875" y1="1.62051875" x2="13.72108125" y2="1.63068125" layer="21"/>
<rectangle x1="14.47291875" y1="1.62051875" x2="14.665959375" y2="1.63068125" layer="21"/>
<rectangle x1="15.09268125" y1="1.62051875" x2="15.28571875" y2="1.63068125" layer="21"/>
<rectangle x1="16.25091875" y1="1.62051875" x2="16.45411875" y2="1.63068125" layer="21"/>
<rectangle x1="0.46228125" y1="1.63068125" x2="1.42748125" y2="1.640840625" layer="21"/>
<rectangle x1="1.77291875" y1="1.63068125" x2="2.219959375" y2="1.640840625" layer="21"/>
<rectangle x1="2.524759375" y1="1.63068125" x2="3.012440625" y2="1.640840625" layer="21"/>
<rectangle x1="3.418840625" y1="1.63068125" x2="3.90651875" y2="1.640840625" layer="21"/>
<rectangle x1="4.31291875" y1="1.63068125" x2="4.77011875" y2="1.640840625" layer="21"/>
<rectangle x1="5.115559375" y1="1.63068125" x2="5.58291875" y2="1.640840625" layer="21"/>
<rectangle x1="6.162040625" y1="1.63068125" x2="6.639559375" y2="1.640840625" layer="21"/>
<rectangle x1="6.924040625" y1="1.63068125" x2="7.37108125" y2="1.640840625" layer="21"/>
<rectangle x1="7.757159375" y1="1.63068125" x2="8.214359375" y2="1.640840625" layer="21"/>
<rectangle x1="8.52931875" y1="1.63068125" x2="8.9662" y2="1.640840625" layer="21"/>
<rectangle x1="9.35228125" y1="1.63068125" x2="9.55548125" y2="1.640840625" layer="21"/>
<rectangle x1="10.52068125" y1="1.63068125" x2="10.754359375" y2="1.640840625" layer="21"/>
<rectangle x1="11.160759375" y1="1.63068125" x2="11.3538" y2="1.640840625" layer="21"/>
<rectangle x1="12.14628125" y1="1.63068125" x2="12.329159375" y2="1.640840625" layer="21"/>
<rectangle x1="12.827" y1="1.63068125" x2="13.00988125" y2="1.640840625" layer="21"/>
<rectangle x1="13.50771875" y1="1.63068125" x2="13.731240625" y2="1.640840625" layer="21"/>
<rectangle x1="14.47291875" y1="1.63068125" x2="14.6558" y2="1.640840625" layer="21"/>
<rectangle x1="15.09268125" y1="1.63068125" x2="15.29588125" y2="1.640840625" layer="21"/>
<rectangle x1="16.25091875" y1="1.63068125" x2="16.443959375" y2="1.640840625" layer="21"/>
<rectangle x1="0.441959375" y1="1.640840625" x2="1.41731875" y2="1.651" layer="21"/>
<rectangle x1="1.78308125" y1="1.640840625" x2="2.23011875" y2="1.651" layer="21"/>
<rectangle x1="2.53491875" y1="1.640840625" x2="3.0226" y2="1.651" layer="21"/>
<rectangle x1="3.40868125" y1="1.640840625" x2="3.91668125" y2="1.651" layer="21"/>
<rectangle x1="4.31291875" y1="1.640840625" x2="4.78028125" y2="1.651" layer="21"/>
<rectangle x1="5.115559375" y1="1.640840625" x2="5.59308125" y2="1.651" layer="21"/>
<rectangle x1="6.15188125" y1="1.640840625" x2="6.64971875" y2="1.651" layer="21"/>
<rectangle x1="6.9342" y1="1.640840625" x2="7.381240625" y2="1.651" layer="21"/>
<rectangle x1="7.747" y1="1.640840625" x2="8.214359375" y2="1.651" layer="21"/>
<rectangle x1="8.52931875" y1="1.640840625" x2="8.9662" y2="1.651" layer="21"/>
<rectangle x1="9.35228125" y1="1.640840625" x2="9.55548125" y2="1.651" layer="21"/>
<rectangle x1="10.52068125" y1="1.640840625" x2="10.754359375" y2="1.651" layer="21"/>
<rectangle x1="11.160759375" y1="1.640840625" x2="11.343640625" y2="1.651" layer="21"/>
<rectangle x1="12.14628125" y1="1.640840625" x2="12.329159375" y2="1.651" layer="21"/>
<rectangle x1="12.837159375" y1="1.640840625" x2="13.020040625" y2="1.651" layer="21"/>
<rectangle x1="13.497559375" y1="1.640840625" x2="13.731240625" y2="1.651" layer="21"/>
<rectangle x1="14.47291875" y1="1.640840625" x2="14.665959375" y2="1.651" layer="21"/>
<rectangle x1="15.09268125" y1="1.640840625" x2="15.29588125" y2="1.651" layer="21"/>
<rectangle x1="16.240759375" y1="1.640840625" x2="16.443959375" y2="1.651" layer="21"/>
<rectangle x1="0.41148125" y1="1.651" x2="1.407159375" y2="1.661159375" layer="21"/>
<rectangle x1="1.77291875" y1="1.651" x2="2.23011875" y2="1.661159375" layer="21"/>
<rectangle x1="2.53491875" y1="1.651" x2="3.0226" y2="1.661159375" layer="21"/>
<rectangle x1="3.40868125" y1="1.651" x2="3.91668125" y2="1.661159375" layer="21"/>
<rectangle x1="4.31291875" y1="1.651" x2="4.78028125" y2="1.661159375" layer="21"/>
<rectangle x1="5.115559375" y1="1.651" x2="5.603240625" y2="1.661159375" layer="21"/>
<rectangle x1="6.14171875" y1="1.651" x2="6.639559375" y2="1.661159375" layer="21"/>
<rectangle x1="6.9342" y1="1.651" x2="7.37108125" y2="1.661159375" layer="21"/>
<rectangle x1="7.747" y1="1.651" x2="8.2042" y2="1.661159375" layer="21"/>
<rectangle x1="8.53948125" y1="1.651" x2="8.976359375" y2="1.661159375" layer="21"/>
<rectangle x1="9.362440625" y1="1.651" x2="9.55548125" y2="1.661159375" layer="21"/>
<rectangle x1="10.51051875" y1="1.651" x2="10.754359375" y2="1.661159375" layer="21"/>
<rectangle x1="11.160759375" y1="1.651" x2="11.343640625" y2="1.661159375" layer="21"/>
<rectangle x1="12.14628125" y1="1.651" x2="12.329159375" y2="1.661159375" layer="21"/>
<rectangle x1="12.827" y1="1.651" x2="13.020040625" y2="1.661159375" layer="21"/>
<rectangle x1="13.497559375" y1="1.651" x2="13.7414" y2="1.661159375" layer="21"/>
<rectangle x1="14.47291875" y1="1.651" x2="14.665959375" y2="1.661159375" layer="21"/>
<rectangle x1="15.102840625" y1="1.651" x2="15.29588125" y2="1.661159375" layer="21"/>
<rectangle x1="16.240759375" y1="1.651" x2="16.443959375" y2="1.661159375" layer="21"/>
<rectangle x1="0.391159375" y1="1.661159375" x2="1.407159375" y2="1.67131875" layer="21"/>
<rectangle x1="1.78308125" y1="1.661159375" x2="2.219959375" y2="1.67131875" layer="21"/>
<rectangle x1="2.53491875" y1="1.661159375" x2="3.032759375" y2="1.67131875" layer="21"/>
<rectangle x1="3.39851875" y1="1.661159375" x2="3.926840625" y2="1.67131875" layer="21"/>
<rectangle x1="4.302759375" y1="1.661159375" x2="4.77011875" y2="1.67131875" layer="21"/>
<rectangle x1="5.1054" y1="1.661159375" x2="5.6134" y2="1.67131875" layer="21"/>
<rectangle x1="6.14171875" y1="1.661159375" x2="6.639559375" y2="1.67131875" layer="21"/>
<rectangle x1="6.924040625" y1="1.661159375" x2="7.381240625" y2="1.67131875" layer="21"/>
<rectangle x1="7.736840625" y1="1.661159375" x2="8.2042" y2="1.67131875" layer="21"/>
<rectangle x1="8.53948125" y1="1.661159375" x2="8.976359375" y2="1.67131875" layer="21"/>
<rectangle x1="9.362440625" y1="1.661159375" x2="9.565640625" y2="1.67131875" layer="21"/>
<rectangle x1="10.51051875" y1="1.661159375" x2="10.754359375" y2="1.67131875" layer="21"/>
<rectangle x1="11.160759375" y1="1.661159375" x2="11.343640625" y2="1.67131875" layer="21"/>
<rectangle x1="12.14628125" y1="1.661159375" x2="12.329159375" y2="1.67131875" layer="21"/>
<rectangle x1="12.837159375" y1="1.661159375" x2="13.00988125" y2="1.67131875" layer="21"/>
<rectangle x1="13.50771875" y1="1.661159375" x2="13.7414" y2="1.67131875" layer="21"/>
<rectangle x1="14.47291875" y1="1.661159375" x2="14.665959375" y2="1.67131875" layer="21"/>
<rectangle x1="15.102840625" y1="1.661159375" x2="15.306040625" y2="1.67131875" layer="21"/>
<rectangle x1="16.2306" y1="1.661159375" x2="16.4338" y2="1.67131875" layer="21"/>
<rectangle x1="0.381" y1="1.67131875" x2="1.397" y2="1.68148125" layer="21"/>
<rectangle x1="1.77291875" y1="1.67131875" x2="2.23011875" y2="1.68148125" layer="21"/>
<rectangle x1="2.53491875" y1="1.67131875" x2="3.032759375" y2="1.68148125" layer="21"/>
<rectangle x1="3.39851875" y1="1.67131875" x2="3.937" y2="1.68148125" layer="21"/>
<rectangle x1="4.302759375" y1="1.67131875" x2="4.77011875" y2="1.68148125" layer="21"/>
<rectangle x1="5.115559375" y1="1.67131875" x2="5.6134" y2="1.68148125" layer="21"/>
<rectangle x1="6.131559375" y1="1.67131875" x2="6.639559375" y2="1.68148125" layer="21"/>
<rectangle x1="6.9342" y1="1.67131875" x2="7.381240625" y2="1.68148125" layer="21"/>
<rectangle x1="7.736840625" y1="1.67131875" x2="8.2042" y2="1.68148125" layer="21"/>
<rectangle x1="8.549640625" y1="1.67131875" x2="8.98651875" y2="1.68148125" layer="21"/>
<rectangle x1="9.362440625" y1="1.67131875" x2="9.5758" y2="1.68148125" layer="21"/>
<rectangle x1="10.500359375" y1="1.67131875" x2="10.754359375" y2="1.68148125" layer="21"/>
<rectangle x1="11.160759375" y1="1.67131875" x2="11.343640625" y2="1.68148125" layer="21"/>
<rectangle x1="12.14628125" y1="1.67131875" x2="12.329159375" y2="1.68148125" layer="21"/>
<rectangle x1="12.827" y1="1.67131875" x2="13.020040625" y2="1.68148125" layer="21"/>
<rectangle x1="13.497559375" y1="1.67131875" x2="13.7414" y2="1.68148125" layer="21"/>
<rectangle x1="14.47291875" y1="1.67131875" x2="14.6558" y2="1.68148125" layer="21"/>
<rectangle x1="15.102840625" y1="1.67131875" x2="15.3162" y2="1.68148125" layer="21"/>
<rectangle x1="16.2306" y1="1.67131875" x2="16.4338" y2="1.68148125" layer="21"/>
<rectangle x1="0.35051875" y1="1.68148125" x2="1.386840625" y2="1.691640625" layer="21"/>
<rectangle x1="1.78308125" y1="1.68148125" x2="2.219959375" y2="1.691640625" layer="21"/>
<rectangle x1="2.524759375" y1="1.68148125" x2="3.04291875" y2="1.691640625" layer="21"/>
<rectangle x1="3.39851875" y1="1.68148125" x2="3.937" y2="1.691640625" layer="21"/>
<rectangle x1="4.2926" y1="1.68148125" x2="4.77011875" y2="1.691640625" layer="21"/>
<rectangle x1="5.115559375" y1="1.68148125" x2="5.63371875" y2="1.691640625" layer="21"/>
<rectangle x1="6.1214" y1="1.68148125" x2="6.639559375" y2="1.691640625" layer="21"/>
<rectangle x1="6.924040625" y1="1.68148125" x2="7.381240625" y2="1.691640625" layer="21"/>
<rectangle x1="7.72668125" y1="1.68148125" x2="8.194040625" y2="1.691640625" layer="21"/>
<rectangle x1="8.549640625" y1="1.68148125" x2="8.98651875" y2="1.691640625" layer="21"/>
<rectangle x1="9.3726" y1="1.68148125" x2="9.5758" y2="1.691640625" layer="21"/>
<rectangle x1="10.500359375" y1="1.68148125" x2="10.754359375" y2="1.691640625" layer="21"/>
<rectangle x1="11.17091875" y1="1.68148125" x2="11.3538" y2="1.691640625" layer="21"/>
<rectangle x1="12.14628125" y1="1.68148125" x2="12.329159375" y2="1.691640625" layer="21"/>
<rectangle x1="12.827" y1="1.68148125" x2="13.00988125" y2="1.691640625" layer="21"/>
<rectangle x1="13.50771875" y1="1.68148125" x2="13.751559375" y2="1.691640625" layer="21"/>
<rectangle x1="14.462759375" y1="1.68148125" x2="14.6558" y2="1.691640625" layer="21"/>
<rectangle x1="15.113" y1="1.68148125" x2="15.3162" y2="1.691640625" layer="21"/>
<rectangle x1="16.220440625" y1="1.68148125" x2="16.423640625" y2="1.691640625" layer="21"/>
<rectangle x1="0.340359375" y1="1.691640625" x2="1.386840625" y2="1.7018" layer="21"/>
<rectangle x1="1.77291875" y1="1.691640625" x2="2.23011875" y2="1.7018" layer="21"/>
<rectangle x1="2.53491875" y1="1.691640625" x2="3.05308125" y2="1.7018" layer="21"/>
<rectangle x1="3.388359375" y1="1.691640625" x2="3.947159375" y2="1.7018" layer="21"/>
<rectangle x1="4.2926" y1="1.691640625" x2="4.77011875" y2="1.7018" layer="21"/>
<rectangle x1="5.115559375" y1="1.691640625" x2="5.64388125" y2="1.7018" layer="21"/>
<rectangle x1="6.111240625" y1="1.691640625" x2="6.6294" y2="1.7018" layer="21"/>
<rectangle x1="6.924040625" y1="1.691640625" x2="7.37108125" y2="1.7018" layer="21"/>
<rectangle x1="7.72668125" y1="1.691640625" x2="8.194040625" y2="1.7018" layer="21"/>
<rectangle x1="8.549640625" y1="1.691640625" x2="8.98651875" y2="1.7018" layer="21"/>
<rectangle x1="9.3726" y1="1.691640625" x2="9.585959375" y2="1.7018" layer="21"/>
<rectangle x1="10.4902" y1="1.691640625" x2="10.754359375" y2="1.7018" layer="21"/>
<rectangle x1="11.160759375" y1="1.691640625" x2="11.343640625" y2="1.7018" layer="21"/>
<rectangle x1="12.14628125" y1="1.691640625" x2="12.329159375" y2="1.7018" layer="21"/>
<rectangle x1="12.827" y1="1.691640625" x2="13.020040625" y2="1.7018" layer="21"/>
<rectangle x1="13.50771875" y1="1.691640625" x2="13.76171875" y2="1.7018" layer="21"/>
<rectangle x1="14.462759375" y1="1.691640625" x2="14.6558" y2="1.7018" layer="21"/>
<rectangle x1="15.113" y1="1.691640625" x2="15.326359375" y2="1.7018" layer="21"/>
<rectangle x1="16.21028125" y1="1.691640625" x2="16.423640625" y2="1.7018" layer="21"/>
<rectangle x1="0.320040625" y1="1.7018" x2="1.37668125" y2="1.711959375" layer="21"/>
<rectangle x1="1.77291875" y1="1.7018" x2="2.23011875" y2="1.711959375" layer="21"/>
<rectangle x1="2.53491875" y1="1.7018" x2="3.05308125" y2="1.711959375" layer="21"/>
<rectangle x1="3.3782" y1="1.7018" x2="3.95731875" y2="1.711959375" layer="21"/>
<rectangle x1="4.282440625" y1="1.7018" x2="4.77011875" y2="1.711959375" layer="21"/>
<rectangle x1="5.1054" y1="1.7018" x2="5.654040625" y2="1.711959375" layer="21"/>
<rectangle x1="6.09091875" y1="1.7018" x2="6.6294" y2="1.711959375" layer="21"/>
<rectangle x1="6.9342" y1="1.7018" x2="7.381240625" y2="1.711959375" layer="21"/>
<rectangle x1="7.71651875" y1="1.7018" x2="8.18388125" y2="1.711959375" layer="21"/>
<rectangle x1="8.549640625" y1="1.7018" x2="8.99668125" y2="1.711959375" layer="21"/>
<rectangle x1="9.382759375" y1="1.7018" x2="9.585959375" y2="1.711959375" layer="21"/>
<rectangle x1="10.480040625" y1="1.7018" x2="10.754359375" y2="1.711959375" layer="21"/>
<rectangle x1="11.160759375" y1="1.7018" x2="11.3538" y2="1.711959375" layer="21"/>
<rectangle x1="12.14628125" y1="1.7018" x2="12.329159375" y2="1.711959375" layer="21"/>
<rectangle x1="12.837159375" y1="1.7018" x2="13.00988125" y2="1.711959375" layer="21"/>
<rectangle x1="13.497559375" y1="1.7018" x2="13.76171875" y2="1.711959375" layer="21"/>
<rectangle x1="14.462759375" y1="1.7018" x2="14.6558" y2="1.711959375" layer="21"/>
<rectangle x1="15.123159375" y1="1.7018" x2="15.326359375" y2="1.711959375" layer="21"/>
<rectangle x1="16.21028125" y1="1.7018" x2="16.41348125" y2="1.711959375" layer="21"/>
<rectangle x1="0.29971875" y1="1.711959375" x2="1.36651875" y2="1.72211875" layer="21"/>
<rectangle x1="1.77291875" y1="1.711959375" x2="2.23011875" y2="1.72211875" layer="21"/>
<rectangle x1="2.524759375" y1="1.711959375" x2="3.063240625" y2="1.72211875" layer="21"/>
<rectangle x1="3.3782" y1="1.711959375" x2="3.96748125" y2="1.72211875" layer="21"/>
<rectangle x1="4.282440625" y1="1.711959375" x2="4.77011875" y2="1.72211875" layer="21"/>
<rectangle x1="5.1054" y1="1.711959375" x2="5.6642" y2="1.72211875" layer="21"/>
<rectangle x1="6.080759375" y1="1.711959375" x2="6.6294" y2="1.72211875" layer="21"/>
<rectangle x1="6.924040625" y1="1.711959375" x2="7.37108125" y2="1.72211875" layer="21"/>
<rectangle x1="7.71651875" y1="1.711959375" x2="8.18388125" y2="1.72211875" layer="21"/>
<rectangle x1="8.5598" y1="1.711959375" x2="8.99668125" y2="1.72211875" layer="21"/>
<rectangle x1="9.382759375" y1="1.711959375" x2="9.59611875" y2="1.72211875" layer="21"/>
<rectangle x1="10.46988125" y1="1.711959375" x2="10.754359375" y2="1.72211875" layer="21"/>
<rectangle x1="11.160759375" y1="1.711959375" x2="11.3538" y2="1.72211875" layer="21"/>
<rectangle x1="12.14628125" y1="1.711959375" x2="12.329159375" y2="1.72211875" layer="21"/>
<rectangle x1="12.827" y1="1.711959375" x2="13.020040625" y2="1.72211875" layer="21"/>
<rectangle x1="13.497559375" y1="1.711959375" x2="13.77188125" y2="1.72211875" layer="21"/>
<rectangle x1="14.462759375" y1="1.711959375" x2="14.6558" y2="1.72211875" layer="21"/>
<rectangle x1="15.123159375" y1="1.711959375" x2="15.33651875" y2="1.72211875" layer="21"/>
<rectangle x1="16.20011875" y1="1.711959375" x2="16.41348125" y2="1.72211875" layer="21"/>
<rectangle x1="0.289559375" y1="1.72211875" x2="1.3462" y2="1.73228125" layer="21"/>
<rectangle x1="1.78308125" y1="1.72211875" x2="2.219959375" y2="1.73228125" layer="21"/>
<rectangle x1="2.53491875" y1="1.72211875" x2="3.083559375" y2="1.73228125" layer="21"/>
<rectangle x1="3.368040625" y1="1.72211875" x2="3.977640625" y2="1.73228125" layer="21"/>
<rectangle x1="4.27228125" y1="1.72211875" x2="4.77011875" y2="1.73228125" layer="21"/>
<rectangle x1="5.115559375" y1="1.72211875" x2="5.68451875" y2="1.73228125" layer="21"/>
<rectangle x1="6.0706" y1="1.72211875" x2="6.619240625" y2="1.73228125" layer="21"/>
<rectangle x1="6.9342" y1="1.72211875" x2="7.381240625" y2="1.73228125" layer="21"/>
<rectangle x1="7.71651875" y1="1.72211875" x2="8.18388125" y2="1.73228125" layer="21"/>
<rectangle x1="8.5598" y1="1.72211875" x2="8.99668125" y2="1.73228125" layer="21"/>
<rectangle x1="9.39291875" y1="1.72211875" x2="9.60628125" y2="1.73228125" layer="21"/>
<rectangle x1="10.46988125" y1="1.72211875" x2="10.754359375" y2="1.73228125" layer="21"/>
<rectangle x1="11.160759375" y1="1.72211875" x2="11.343640625" y2="1.73228125" layer="21"/>
<rectangle x1="12.14628125" y1="1.72211875" x2="12.329159375" y2="1.73228125" layer="21"/>
<rectangle x1="12.837159375" y1="1.72211875" x2="13.020040625" y2="1.73228125" layer="21"/>
<rectangle x1="13.50771875" y1="1.72211875" x2="13.77188125" y2="1.73228125" layer="21"/>
<rectangle x1="14.4526" y1="1.72211875" x2="14.6558" y2="1.73228125" layer="21"/>
<rectangle x1="15.13331875" y1="1.72211875" x2="15.34668125" y2="1.73228125" layer="21"/>
<rectangle x1="16.189959375" y1="1.72211875" x2="16.40331875" y2="1.73228125" layer="21"/>
<rectangle x1="0.269240625" y1="1.73228125" x2="1.3462" y2="1.742440625" layer="21"/>
<rectangle x1="1.77291875" y1="1.73228125" x2="2.23011875" y2="1.742440625" layer="21"/>
<rectangle x1="2.53491875" y1="1.73228125" x2="3.09371875" y2="1.742440625" layer="21"/>
<rectangle x1="3.35788125" y1="1.73228125" x2="3.997959375" y2="1.742440625" layer="21"/>
<rectangle x1="4.26211875" y1="1.73228125" x2="4.759959375" y2="1.742440625" layer="21"/>
<rectangle x1="5.115559375" y1="1.73228125" x2="5.69468125" y2="1.742440625" layer="21"/>
<rectangle x1="6.05028125" y1="1.73228125" x2="6.619240625" y2="1.742440625" layer="21"/>
<rectangle x1="6.9342" y1="1.73228125" x2="7.381240625" y2="1.742440625" layer="21"/>
<rectangle x1="7.706359375" y1="1.73228125" x2="8.17371875" y2="1.742440625" layer="21"/>
<rectangle x1="8.5598" y1="1.73228125" x2="9.006840625" y2="1.742440625" layer="21"/>
<rectangle x1="9.39291875" y1="1.73228125" x2="9.616440625" y2="1.742440625" layer="21"/>
<rectangle x1="10.45971875" y1="1.73228125" x2="10.754359375" y2="1.742440625" layer="21"/>
<rectangle x1="11.160759375" y1="1.73228125" x2="11.343640625" y2="1.742440625" layer="21"/>
<rectangle x1="12.14628125" y1="1.73228125" x2="12.329159375" y2="1.742440625" layer="21"/>
<rectangle x1="12.827" y1="1.73228125" x2="13.020040625" y2="1.742440625" layer="21"/>
<rectangle x1="13.497559375" y1="1.73228125" x2="13.782040625" y2="1.742440625" layer="21"/>
<rectangle x1="14.4526" y1="1.73228125" x2="14.645640625" y2="1.742440625" layer="21"/>
<rectangle x1="15.13331875" y1="1.73228125" x2="15.34668125" y2="1.742440625" layer="21"/>
<rectangle x1="16.189959375" y1="1.73228125" x2="16.40331875" y2="1.742440625" layer="21"/>
<rectangle x1="0.25908125" y1="1.742440625" x2="1.32588125" y2="1.7526" layer="21"/>
<rectangle x1="1.78308125" y1="1.742440625" x2="2.23011875" y2="1.7526" layer="21"/>
<rectangle x1="2.53491875" y1="1.742440625" x2="3.114040625" y2="1.7526" layer="21"/>
<rectangle x1="3.337559375" y1="1.742440625" x2="4.00811875" y2="1.7526" layer="21"/>
<rectangle x1="4.2418" y1="1.742440625" x2="4.759959375" y2="1.7526" layer="21"/>
<rectangle x1="5.115559375" y1="1.742440625" x2="5.725159375" y2="1.7526" layer="21"/>
<rectangle x1="6.0198" y1="1.742440625" x2="6.60908125" y2="1.7526" layer="21"/>
<rectangle x1="6.924040625" y1="1.742440625" x2="7.381240625" y2="1.7526" layer="21"/>
<rectangle x1="7.706359375" y1="1.742440625" x2="8.17371875" y2="1.7526" layer="21"/>
<rectangle x1="8.569959375" y1="1.742440625" x2="9.006840625" y2="1.7526" layer="21"/>
<rectangle x1="9.40308125" y1="1.742440625" x2="9.616440625" y2="1.7526" layer="21"/>
<rectangle x1="10.449559375" y1="1.742440625" x2="10.754359375" y2="1.7526" layer="21"/>
<rectangle x1="11.160759375" y1="1.742440625" x2="11.343640625" y2="1.7526" layer="21"/>
<rectangle x1="12.14628125" y1="1.742440625" x2="12.329159375" y2="1.7526" layer="21"/>
<rectangle x1="12.827" y1="1.742440625" x2="13.00988125" y2="1.7526" layer="21"/>
<rectangle x1="13.50771875" y1="1.742440625" x2="13.782040625" y2="1.7526" layer="21"/>
<rectangle x1="14.442440625" y1="1.742440625" x2="14.645640625" y2="1.7526" layer="21"/>
<rectangle x1="15.14348125" y1="1.742440625" x2="15.356840625" y2="1.7526" layer="21"/>
<rectangle x1="16.1798" y1="1.742440625" x2="16.40331875" y2="1.7526" layer="21"/>
<rectangle x1="0.24891875" y1="1.7526" x2="1.31571875" y2="1.762759375" layer="21"/>
<rectangle x1="1.77291875" y1="1.7526" x2="2.219959375" y2="1.762759375" layer="21"/>
<rectangle x1="2.53491875" y1="1.7526" x2="3.134359375" y2="1.762759375" layer="21"/>
<rectangle x1="3.3274" y1="1.7526" x2="4.0386" y2="1.762759375" layer="21"/>
<rectangle x1="4.231640625" y1="1.7526" x2="4.759959375" y2="1.762759375" layer="21"/>
<rectangle x1="5.1054" y1="1.7526" x2="5.755640625" y2="1.762759375" layer="21"/>
<rectangle x1="5.99948125" y1="1.7526" x2="6.60908125" y2="1.762759375" layer="21"/>
<rectangle x1="6.9342" y1="1.7526" x2="7.37108125" y2="1.762759375" layer="21"/>
<rectangle x1="7.6962" y1="1.7526" x2="8.17371875" y2="1.762759375" layer="21"/>
<rectangle x1="8.569959375" y1="1.7526" x2="9.017" y2="1.762759375" layer="21"/>
<rectangle x1="9.40308125" y1="1.7526" x2="9.6266" y2="1.762759375" layer="21"/>
<rectangle x1="10.4394" y1="1.7526" x2="10.754359375" y2="1.762759375" layer="21"/>
<rectangle x1="11.160759375" y1="1.7526" x2="11.343640625" y2="1.762759375" layer="21"/>
<rectangle x1="12.14628125" y1="1.7526" x2="12.329159375" y2="1.762759375" layer="21"/>
<rectangle x1="12.827" y1="1.7526" x2="13.00988125" y2="1.762759375" layer="21"/>
<rectangle x1="13.50771875" y1="1.7526" x2="13.7922" y2="1.762759375" layer="21"/>
<rectangle x1="14.442440625" y1="1.7526" x2="14.645640625" y2="1.762759375" layer="21"/>
<rectangle x1="15.14348125" y1="1.7526" x2="15.367" y2="1.762759375" layer="21"/>
<rectangle x1="16.169640625" y1="1.7526" x2="16.393159375" y2="1.762759375" layer="21"/>
<rectangle x1="0.238759375" y1="1.762759375" x2="1.305559375" y2="1.77291875" layer="21"/>
<rectangle x1="1.77291875" y1="1.762759375" x2="2.23011875" y2="1.77291875" layer="21"/>
<rectangle x1="2.524759375" y1="1.762759375" x2="3.175" y2="1.77291875" layer="21"/>
<rectangle x1="3.29691875" y1="1.762759375" x2="4.06908125" y2="1.77291875" layer="21"/>
<rectangle x1="4.201159375" y1="1.762759375" x2="4.759959375" y2="1.77291875" layer="21"/>
<rectangle x1="5.115559375" y1="1.762759375" x2="5.79628125" y2="1.77291875" layer="21"/>
<rectangle x1="5.94868125" y1="1.762759375" x2="6.60908125" y2="1.77291875" layer="21"/>
<rectangle x1="6.924040625" y1="1.762759375" x2="7.381240625" y2="1.77291875" layer="21"/>
<rectangle x1="7.6962" y1="1.762759375" x2="8.163559375" y2="1.77291875" layer="21"/>
<rectangle x1="8.569959375" y1="1.762759375" x2="9.017" y2="1.77291875" layer="21"/>
<rectangle x1="9.413240625" y1="1.762759375" x2="9.636759375" y2="1.77291875" layer="21"/>
<rectangle x1="10.429240625" y1="1.762759375" x2="10.754359375" y2="1.77291875" layer="21"/>
<rectangle x1="11.160759375" y1="1.762759375" x2="11.3538" y2="1.77291875" layer="21"/>
<rectangle x1="12.14628125" y1="1.762759375" x2="12.329159375" y2="1.77291875" layer="21"/>
<rectangle x1="12.837159375" y1="1.762759375" x2="13.020040625" y2="1.77291875" layer="21"/>
<rectangle x1="13.497559375" y1="1.762759375" x2="13.802359375" y2="1.77291875" layer="21"/>
<rectangle x1="14.442440625" y1="1.762759375" x2="14.645640625" y2="1.77291875" layer="21"/>
<rectangle x1="15.153640625" y1="1.762759375" x2="15.377159375" y2="1.77291875" layer="21"/>
<rectangle x1="16.15948125" y1="1.762759375" x2="16.383" y2="1.77291875" layer="21"/>
<rectangle x1="0.218440625" y1="1.77291875" x2="1.2954" y2="1.78308125" layer="21"/>
<rectangle x1="1.77291875" y1="1.77291875" x2="2.219959375" y2="1.78308125" layer="21"/>
<rectangle x1="2.53491875" y1="1.77291875" x2="3.235959375" y2="1.78308125" layer="21"/>
<rectangle x1="3.24611875" y1="1.77291875" x2="4.759959375" y2="1.78308125" layer="21"/>
<rectangle x1="5.115559375" y1="1.77291875" x2="6.59891875" y2="1.78308125" layer="21"/>
<rectangle x1="6.924040625" y1="1.77291875" x2="7.37108125" y2="1.78308125" layer="21"/>
<rectangle x1="7.686040625" y1="1.77291875" x2="8.163559375" y2="1.78308125" layer="21"/>
<rectangle x1="8.58011875" y1="1.77291875" x2="9.017" y2="1.78308125" layer="21"/>
<rectangle x1="9.413240625" y1="1.77291875" x2="9.64691875" y2="1.78308125" layer="21"/>
<rectangle x1="10.41908125" y1="1.77291875" x2="10.754359375" y2="1.78308125" layer="21"/>
<rectangle x1="11.160759375" y1="1.77291875" x2="11.343640625" y2="1.78308125" layer="21"/>
<rectangle x1="12.14628125" y1="1.77291875" x2="12.329159375" y2="1.78308125" layer="21"/>
<rectangle x1="12.827" y1="1.77291875" x2="13.020040625" y2="1.78308125" layer="21"/>
<rectangle x1="13.497559375" y1="1.77291875" x2="13.802359375" y2="1.78308125" layer="21"/>
<rectangle x1="14.43228125" y1="1.77291875" x2="14.63548125" y2="1.78308125" layer="21"/>
<rectangle x1="15.153640625" y1="1.77291875" x2="15.38731875" y2="1.78308125" layer="21"/>
<rectangle x1="16.14931875" y1="1.77291875" x2="16.383" y2="1.78308125" layer="21"/>
<rectangle x1="0.20828125" y1="1.78308125" x2="1.27508125" y2="1.793240625" layer="21"/>
<rectangle x1="1.78308125" y1="1.78308125" x2="2.23011875" y2="1.793240625" layer="21"/>
<rectangle x1="2.53491875" y1="1.78308125" x2="4.759959375" y2="1.793240625" layer="21"/>
<rectangle x1="5.115559375" y1="1.78308125" x2="6.59891875" y2="1.793240625" layer="21"/>
<rectangle x1="6.9342" y1="1.78308125" x2="7.381240625" y2="1.793240625" layer="21"/>
<rectangle x1="7.686040625" y1="1.78308125" x2="8.1534" y2="1.793240625" layer="21"/>
<rectangle x1="8.58011875" y1="1.78308125" x2="9.027159375" y2="1.793240625" layer="21"/>
<rectangle x1="9.4234" y1="1.78308125" x2="9.65708125" y2="1.793240625" layer="21"/>
<rectangle x1="10.40891875" y1="1.78308125" x2="10.754359375" y2="1.793240625" layer="21"/>
<rectangle x1="11.160759375" y1="1.78308125" x2="11.3538" y2="1.793240625" layer="21"/>
<rectangle x1="12.14628125" y1="1.78308125" x2="12.329159375" y2="1.793240625" layer="21"/>
<rectangle x1="12.837159375" y1="1.78308125" x2="13.00988125" y2="1.793240625" layer="21"/>
<rectangle x1="13.497559375" y1="1.78308125" x2="13.81251875" y2="1.793240625" layer="21"/>
<rectangle x1="14.42211875" y1="1.78308125" x2="14.63548125" y2="1.793240625" layer="21"/>
<rectangle x1="15.1638" y1="1.78308125" x2="15.39748125" y2="1.793240625" layer="21"/>
<rectangle x1="16.139159375" y1="1.78308125" x2="16.372840625" y2="1.793240625" layer="21"/>
<rectangle x1="0.19811875" y1="1.793240625" x2="1.26491875" y2="1.8034" layer="21"/>
<rectangle x1="1.77291875" y1="1.793240625" x2="2.23011875" y2="1.8034" layer="21"/>
<rectangle x1="2.524759375" y1="1.793240625" x2="4.7498" y2="1.8034" layer="21"/>
<rectangle x1="5.115559375" y1="1.793240625" x2="6.588759375" y2="1.8034" layer="21"/>
<rectangle x1="6.9342" y1="1.793240625" x2="7.37108125" y2="1.8034" layer="21"/>
<rectangle x1="7.686040625" y1="1.793240625" x2="8.1534" y2="1.8034" layer="21"/>
<rectangle x1="8.59028125" y1="1.793240625" x2="9.027159375" y2="1.8034" layer="21"/>
<rectangle x1="9.433559375" y1="1.793240625" x2="9.667240625" y2="1.8034" layer="21"/>
<rectangle x1="10.398759375" y1="1.793240625" x2="10.754359375" y2="1.8034" layer="21"/>
<rectangle x1="11.160759375" y1="1.793240625" x2="11.3538" y2="1.8034" layer="21"/>
<rectangle x1="12.14628125" y1="1.793240625" x2="12.329159375" y2="1.8034" layer="21"/>
<rectangle x1="12.837159375" y1="1.793240625" x2="13.020040625" y2="1.8034" layer="21"/>
<rectangle x1="13.497559375" y1="1.793240625" x2="13.82268125" y2="1.8034" layer="21"/>
<rectangle x1="14.42211875" y1="1.793240625" x2="14.63548125" y2="1.8034" layer="21"/>
<rectangle x1="15.173959375" y1="1.793240625" x2="15.407640625" y2="1.8034" layer="21"/>
<rectangle x1="16.129" y1="1.793240625" x2="16.36268125" y2="1.8034" layer="21"/>
<rectangle x1="0.187959375" y1="1.8034" x2="1.2446" y2="1.813559375" layer="21"/>
<rectangle x1="1.78308125" y1="1.8034" x2="2.219959375" y2="1.813559375" layer="21"/>
<rectangle x1="2.53491875" y1="1.8034" x2="4.7498" y2="1.813559375" layer="21"/>
<rectangle x1="5.115559375" y1="1.8034" x2="6.588759375" y2="1.813559375" layer="21"/>
<rectangle x1="6.924040625" y1="1.8034" x2="7.381240625" y2="1.813559375" layer="21"/>
<rectangle x1="7.67588125" y1="1.8034" x2="8.1534" y2="1.813559375" layer="21"/>
<rectangle x1="8.59028125" y1="1.8034" x2="9.03731875" y2="1.813559375" layer="21"/>
<rectangle x1="9.433559375" y1="1.8034" x2="9.6774" y2="1.813559375" layer="21"/>
<rectangle x1="10.3886" y1="1.8034" x2="10.754359375" y2="1.813559375" layer="21"/>
<rectangle x1="11.160759375" y1="1.8034" x2="11.343640625" y2="1.813559375" layer="21"/>
<rectangle x1="12.14628125" y1="1.8034" x2="12.329159375" y2="1.813559375" layer="21"/>
<rectangle x1="12.827" y1="1.8034" x2="13.00988125" y2="1.813559375" layer="21"/>
<rectangle x1="13.497559375" y1="1.8034" x2="13.832840625" y2="1.813559375" layer="21"/>
<rectangle x1="14.411959375" y1="1.8034" x2="14.62531875" y2="1.813559375" layer="21"/>
<rectangle x1="15.173959375" y1="1.8034" x2="15.4178" y2="1.813559375" layer="21"/>
<rectangle x1="16.118840625" y1="1.8034" x2="16.36268125" y2="1.813559375" layer="21"/>
<rectangle x1="0.187959375" y1="1.813559375" x2="1.22428125" y2="1.82371875" layer="21"/>
<rectangle x1="1.77291875" y1="1.813559375" x2="2.23011875" y2="1.82371875" layer="21"/>
<rectangle x1="2.53491875" y1="1.813559375" x2="4.7498" y2="1.82371875" layer="21"/>
<rectangle x1="5.115559375" y1="1.813559375" x2="6.5786" y2="1.82371875" layer="21"/>
<rectangle x1="6.9342" y1="1.813559375" x2="7.37108125" y2="1.82371875" layer="21"/>
<rectangle x1="7.67588125" y1="1.813559375" x2="8.143240625" y2="1.82371875" layer="21"/>
<rectangle x1="8.59028125" y1="1.813559375" x2="9.03731875" y2="1.82371875" layer="21"/>
<rectangle x1="9.44371875" y1="1.813559375" x2="9.69771875" y2="1.82371875" layer="21"/>
<rectangle x1="10.36828125" y1="1.813559375" x2="10.754359375" y2="1.82371875" layer="21"/>
<rectangle x1="11.160759375" y1="1.813559375" x2="11.343640625" y2="1.82371875" layer="21"/>
<rectangle x1="12.14628125" y1="1.813559375" x2="12.329159375" y2="1.82371875" layer="21"/>
<rectangle x1="12.837159375" y1="1.813559375" x2="13.020040625" y2="1.82371875" layer="21"/>
<rectangle x1="13.497559375" y1="1.813559375" x2="13.843" y2="1.82371875" layer="21"/>
<rectangle x1="14.411959375" y1="1.813559375" x2="14.62531875" y2="1.82371875" layer="21"/>
<rectangle x1="15.18411875" y1="1.813559375" x2="15.43811875" y2="1.82371875" layer="21"/>
<rectangle x1="16.10868125" y1="1.813559375" x2="16.35251875" y2="1.82371875" layer="21"/>
<rectangle x1="0.167640625" y1="1.82371875" x2="1.21411875" y2="1.83388125" layer="21"/>
<rectangle x1="1.78308125" y1="1.82371875" x2="2.219959375" y2="1.83388125" layer="21"/>
<rectangle x1="2.53491875" y1="1.82371875" x2="4.739640625" y2="1.83388125" layer="21"/>
<rectangle x1="5.1054" y1="1.82371875" x2="6.5786" y2="1.83388125" layer="21"/>
<rectangle x1="6.924040625" y1="1.82371875" x2="7.381240625" y2="1.83388125" layer="21"/>
<rectangle x1="7.66571875" y1="1.82371875" x2="8.143240625" y2="1.83388125" layer="21"/>
<rectangle x1="8.59028125" y1="1.82371875" x2="9.03731875" y2="1.83388125" layer="21"/>
<rectangle x1="9.45388125" y1="1.82371875" x2="9.70788125" y2="1.83388125" layer="21"/>
<rectangle x1="10.347959375" y1="1.82371875" x2="10.754359375" y2="1.83388125" layer="21"/>
<rectangle x1="11.160759375" y1="1.82371875" x2="11.343640625" y2="1.83388125" layer="21"/>
<rectangle x1="12.14628125" y1="1.82371875" x2="12.329159375" y2="1.83388125" layer="21"/>
<rectangle x1="12.827" y1="1.82371875" x2="13.00988125" y2="1.83388125" layer="21"/>
<rectangle x1="13.497559375" y1="1.82371875" x2="13.853159375" y2="1.83388125" layer="21"/>
<rectangle x1="14.4018" y1="1.82371875" x2="14.62531875" y2="1.83388125" layer="21"/>
<rectangle x1="15.19428125" y1="1.82371875" x2="15.44828125" y2="1.83388125" layer="21"/>
<rectangle x1="16.088359375" y1="1.82371875" x2="16.342359375" y2="1.83388125" layer="21"/>
<rectangle x1="0.167640625" y1="1.83388125" x2="1.1938" y2="1.844040625" layer="21"/>
<rectangle x1="1.77291875" y1="1.83388125" x2="2.23011875" y2="1.844040625" layer="21"/>
<rectangle x1="2.53491875" y1="1.83388125" x2="4.739640625" y2="1.844040625" layer="21"/>
<rectangle x1="5.115559375" y1="1.83388125" x2="6.568440625" y2="1.844040625" layer="21"/>
<rectangle x1="6.9342" y1="1.83388125" x2="7.381240625" y2="1.844040625" layer="21"/>
<rectangle x1="7.66571875" y1="1.83388125" x2="8.13308125" y2="1.844040625" layer="21"/>
<rectangle x1="8.600440625" y1="1.83388125" x2="9.04748125" y2="1.844040625" layer="21"/>
<rectangle x1="9.464040625" y1="1.83388125" x2="9.718040625" y2="1.844040625" layer="21"/>
<rectangle x1="10.3378" y1="1.83388125" x2="10.754359375" y2="1.844040625" layer="21"/>
<rectangle x1="11.160759375" y1="1.83388125" x2="11.343640625" y2="1.844040625" layer="21"/>
<rectangle x1="12.14628125" y1="1.83388125" x2="12.329159375" y2="1.844040625" layer="21"/>
<rectangle x1="12.837159375" y1="1.83388125" x2="13.020040625" y2="1.844040625" layer="21"/>
<rectangle x1="13.497559375" y1="1.83388125" x2="13.680440625" y2="1.844040625" layer="21"/>
<rectangle x1="13.6906" y1="1.83388125" x2="13.87348125" y2="1.844040625" layer="21"/>
<rectangle x1="14.391640625" y1="1.83388125" x2="14.615159375" y2="1.844040625" layer="21"/>
<rectangle x1="15.204440625" y1="1.83388125" x2="15.458440625" y2="1.844040625" layer="21"/>
<rectangle x1="16.0782" y1="1.83388125" x2="16.342359375" y2="1.844040625" layer="21"/>
<rectangle x1="0.15748125" y1="1.844040625" x2="1.17348125" y2="1.8542" layer="21"/>
<rectangle x1="1.78308125" y1="1.844040625" x2="2.23011875" y2="1.8542" layer="21"/>
<rectangle x1="2.524759375" y1="1.844040625" x2="4.739640625" y2="1.8542" layer="21"/>
<rectangle x1="5.115559375" y1="1.844040625" x2="6.55828125" y2="1.8542" layer="21"/>
<rectangle x1="6.924040625" y1="1.844040625" x2="7.381240625" y2="1.8542" layer="21"/>
<rectangle x1="7.66571875" y1="1.844040625" x2="8.13308125" y2="1.8542" layer="21"/>
<rectangle x1="8.600440625" y1="1.844040625" x2="9.04748125" y2="1.8542" layer="21"/>
<rectangle x1="9.464040625" y1="1.844040625" x2="9.738359375" y2="1.8542" layer="21"/>
<rectangle x1="10.327640625" y1="1.844040625" x2="10.56131875" y2="1.8542" layer="21"/>
<rectangle x1="10.57148125" y1="1.844040625" x2="10.754359375" y2="1.8542" layer="21"/>
<rectangle x1="11.160759375" y1="1.844040625" x2="11.3538" y2="1.8542" layer="21"/>
<rectangle x1="12.14628125" y1="1.844040625" x2="12.329159375" y2="1.8542" layer="21"/>
<rectangle x1="12.827" y1="1.844040625" x2="13.020040625" y2="1.8542" layer="21"/>
<rectangle x1="13.497559375" y1="1.844040625" x2="13.680440625" y2="1.8542" layer="21"/>
<rectangle x1="13.700759375" y1="1.844040625" x2="13.883640625" y2="1.8542" layer="21"/>
<rectangle x1="14.38148125" y1="1.844040625" x2="14.615159375" y2="1.8542" layer="21"/>
<rectangle x1="15.204440625" y1="1.844040625" x2="15.478759375" y2="1.8542" layer="21"/>
<rectangle x1="16.068040625" y1="1.844040625" x2="16.3322" y2="1.8542" layer="21"/>
<rectangle x1="0.14731875" y1="1.8542" x2="1.143" y2="1.864359375" layer="21"/>
<rectangle x1="1.77291875" y1="1.8542" x2="2.219959375" y2="1.864359375" layer="21"/>
<rectangle x1="2.53491875" y1="1.8542" x2="4.739640625" y2="1.864359375" layer="21"/>
<rectangle x1="5.115559375" y1="1.8542" x2="6.55828125" y2="1.864359375" layer="21"/>
<rectangle x1="6.9342" y1="1.8542" x2="7.37108125" y2="1.864359375" layer="21"/>
<rectangle x1="7.655559375" y1="1.8542" x2="8.13308125" y2="1.864359375" layer="21"/>
<rectangle x1="8.600440625" y1="1.8542" x2="9.04748125" y2="1.864359375" layer="21"/>
<rectangle x1="9.4742" y1="1.8542" x2="9.74851875" y2="1.864359375" layer="21"/>
<rectangle x1="10.30731875" y1="1.8542" x2="10.551159375" y2="1.864359375" layer="21"/>
<rectangle x1="10.57148125" y1="1.8542" x2="10.754359375" y2="1.864359375" layer="21"/>
<rectangle x1="11.160759375" y1="1.8542" x2="11.343640625" y2="1.864359375" layer="21"/>
<rectangle x1="12.14628125" y1="1.8542" x2="12.329159375" y2="1.864359375" layer="21"/>
<rectangle x1="12.837159375" y1="1.8542" x2="13.020040625" y2="1.864359375" layer="21"/>
<rectangle x1="13.497559375" y1="1.8542" x2="13.680440625" y2="1.864359375" layer="21"/>
<rectangle x1="13.700759375" y1="1.8542" x2="13.8938" y2="1.864359375" layer="21"/>
<rectangle x1="14.37131875" y1="1.8542" x2="14.605" y2="1.864359375" layer="21"/>
<rectangle x1="15.2146" y1="1.8542" x2="15.48891875" y2="1.864359375" layer="21"/>
<rectangle x1="16.04771875" y1="1.8542" x2="16.322040625" y2="1.864359375" layer="21"/>
<rectangle x1="0.137159375" y1="1.864359375" x2="1.12268125" y2="1.87451875" layer="21"/>
<rectangle x1="1.77291875" y1="1.864359375" x2="2.23011875" y2="1.87451875" layer="21"/>
<rectangle x1="2.53491875" y1="1.864359375" x2="4.72948125" y2="1.87451875" layer="21"/>
<rectangle x1="5.1054" y1="1.864359375" x2="6.54811875" y2="1.87451875" layer="21"/>
<rectangle x1="6.924040625" y1="1.864359375" x2="7.381240625" y2="1.87451875" layer="21"/>
<rectangle x1="7.6454" y1="1.864359375" x2="8.12291875" y2="1.87451875" layer="21"/>
<rectangle x1="8.6106" y1="1.864359375" x2="9.057640625" y2="1.87451875" layer="21"/>
<rectangle x1="9.484359375" y1="1.864359375" x2="9.768840625" y2="1.87451875" layer="21"/>
<rectangle x1="10.287" y1="1.864359375" x2="10.551159375" y2="1.87451875" layer="21"/>
<rectangle x1="10.57148125" y1="1.864359375" x2="10.754359375" y2="1.87451875" layer="21"/>
<rectangle x1="11.160759375" y1="1.864359375" x2="11.3538" y2="1.87451875" layer="21"/>
<rectangle x1="12.14628125" y1="1.864359375" x2="12.329159375" y2="1.87451875" layer="21"/>
<rectangle x1="12.827" y1="1.864359375" x2="13.00988125" y2="1.87451875" layer="21"/>
<rectangle x1="13.497559375" y1="1.864359375" x2="13.680440625" y2="1.87451875" layer="21"/>
<rectangle x1="13.71091875" y1="1.864359375" x2="13.91411875" y2="1.87451875" layer="21"/>
<rectangle x1="14.361159375" y1="1.864359375" x2="14.605" y2="1.87451875" layer="21"/>
<rectangle x1="15.224759375" y1="1.864359375" x2="15.509240625" y2="1.87451875" layer="21"/>
<rectangle x1="16.0274" y1="1.864359375" x2="16.31188125" y2="1.87451875" layer="21"/>
<rectangle x1="0.137159375" y1="1.87451875" x2="1.102359375" y2="1.88468125" layer="21"/>
<rectangle x1="1.77291875" y1="1.87451875" x2="2.219959375" y2="1.88468125" layer="21"/>
<rectangle x1="2.524759375" y1="1.87451875" x2="4.72948125" y2="1.88468125" layer="21"/>
<rectangle x1="5.115559375" y1="1.87451875" x2="6.54811875" y2="1.88468125" layer="21"/>
<rectangle x1="6.9342" y1="1.87451875" x2="7.37108125" y2="1.88468125" layer="21"/>
<rectangle x1="7.6454" y1="1.87451875" x2="8.12291875" y2="1.88468125" layer="21"/>
<rectangle x1="8.6106" y1="1.87451875" x2="9.057640625" y2="1.88468125" layer="21"/>
<rectangle x1="9.49451875" y1="1.87451875" x2="9.789159375" y2="1.88468125" layer="21"/>
<rectangle x1="10.26668125" y1="1.87451875" x2="10.541" y2="1.88468125" layer="21"/>
<rectangle x1="10.57148125" y1="1.87451875" x2="10.754359375" y2="1.88468125" layer="21"/>
<rectangle x1="11.160759375" y1="1.87451875" x2="11.3538" y2="1.88468125" layer="21"/>
<rectangle x1="12.14628125" y1="1.87451875" x2="12.329159375" y2="1.88468125" layer="21"/>
<rectangle x1="12.837159375" y1="1.87451875" x2="13.00988125" y2="1.88468125" layer="21"/>
<rectangle x1="13.497559375" y1="1.87451875" x2="13.680440625" y2="1.88468125" layer="21"/>
<rectangle x1="13.71091875" y1="1.87451875" x2="13.92428125" y2="1.88468125" layer="21"/>
<rectangle x1="14.351" y1="1.87451875" x2="14.594840625" y2="1.88468125" layer="21"/>
<rectangle x1="15.23491875" y1="1.87451875" x2="15.529559375" y2="1.88468125" layer="21"/>
<rectangle x1="16.00708125" y1="1.87451875" x2="16.30171875" y2="1.88468125" layer="21"/>
<rectangle x1="0.127" y1="1.88468125" x2="1.07188125" y2="1.894840625" layer="21"/>
<rectangle x1="1.78308125" y1="1.88468125" x2="2.23011875" y2="1.894840625" layer="21"/>
<rectangle x1="2.53491875" y1="1.88468125" x2="4.71931875" y2="1.894840625" layer="21"/>
<rectangle x1="5.115559375" y1="1.88468125" x2="6.537959375" y2="1.894840625" layer="21"/>
<rectangle x1="6.924040625" y1="1.88468125" x2="7.381240625" y2="1.894840625" layer="21"/>
<rectangle x1="7.6454" y1="1.88468125" x2="8.112759375" y2="1.894840625" layer="21"/>
<rectangle x1="8.6106" y1="1.88468125" x2="9.0678" y2="1.894840625" layer="21"/>
<rectangle x1="9.50468125" y1="1.88468125" x2="9.80948125" y2="1.894840625" layer="21"/>
<rectangle x1="10.246359375" y1="1.88468125" x2="10.530840625" y2="1.894840625" layer="21"/>
<rectangle x1="10.57148125" y1="1.88468125" x2="10.754359375" y2="1.894840625" layer="21"/>
<rectangle x1="11.160759375" y1="1.88468125" x2="11.343640625" y2="1.894840625" layer="21"/>
<rectangle x1="12.14628125" y1="1.88468125" x2="12.329159375" y2="1.894840625" layer="21"/>
<rectangle x1="12.827" y1="1.88468125" x2="13.020040625" y2="1.894840625" layer="21"/>
<rectangle x1="13.497559375" y1="1.88468125" x2="13.680440625" y2="1.894840625" layer="21"/>
<rectangle x1="13.72108125" y1="1.88468125" x2="13.9446" y2="1.894840625" layer="21"/>
<rectangle x1="14.33068125" y1="1.88468125" x2="14.594840625" y2="1.894840625" layer="21"/>
<rectangle x1="15.24508125" y1="1.88468125" x2="15.54988125" y2="1.894840625" layer="21"/>
<rectangle x1="15.986759375" y1="1.88468125" x2="16.291559375" y2="1.894840625" layer="21"/>
<rectangle x1="0.127" y1="1.894840625" x2="1.0414" y2="1.905" layer="21"/>
<rectangle x1="1.77291875" y1="1.894840625" x2="2.23011875" y2="1.905" layer="21"/>
<rectangle x1="2.53491875" y1="1.894840625" x2="4.71931875" y2="1.905" layer="21"/>
<rectangle x1="5.115559375" y1="1.894840625" x2="6.5278" y2="1.905" layer="21"/>
<rectangle x1="6.9342" y1="1.894840625" x2="7.381240625" y2="1.905" layer="21"/>
<rectangle x1="7.635240625" y1="1.894840625" x2="8.112759375" y2="1.905" layer="21"/>
<rectangle x1="8.620759375" y1="1.894840625" x2="9.0678" y2="1.905" layer="21"/>
<rectangle x1="9.514840625" y1="1.894840625" x2="9.8298" y2="1.905" layer="21"/>
<rectangle x1="10.226040625" y1="1.894840625" x2="10.52068125" y2="1.905" layer="21"/>
<rectangle x1="10.57148125" y1="1.894840625" x2="10.754359375" y2="1.905" layer="21"/>
<rectangle x1="11.160759375" y1="1.894840625" x2="11.343640625" y2="1.905" layer="21"/>
<rectangle x1="12.14628125" y1="1.894840625" x2="12.329159375" y2="1.905" layer="21"/>
<rectangle x1="12.837159375" y1="1.894840625" x2="13.020040625" y2="1.905" layer="21"/>
<rectangle x1="13.497559375" y1="1.894840625" x2="13.680440625" y2="1.905" layer="21"/>
<rectangle x1="13.731240625" y1="1.894840625" x2="13.97508125" y2="1.905" layer="21"/>
<rectangle x1="14.310359375" y1="1.894840625" x2="14.58468125" y2="1.905" layer="21"/>
<rectangle x1="15.255240625" y1="1.894840625" x2="15.580359375" y2="1.905" layer="21"/>
<rectangle x1="15.95628125" y1="1.894840625" x2="16.2814" y2="1.905" layer="21"/>
<rectangle x1="0.116840625" y1="1.905" x2="1.000759375" y2="1.915159375" layer="21"/>
<rectangle x1="1.78308125" y1="1.905" x2="2.219959375" y2="1.915159375" layer="21"/>
<rectangle x1="2.53491875" y1="1.905" x2="4.71931875" y2="1.915159375" layer="21"/>
<rectangle x1="5.1054" y1="1.905" x2="6.517640625" y2="1.915159375" layer="21"/>
<rectangle x1="6.924040625" y1="1.905" x2="7.381240625" y2="1.915159375" layer="21"/>
<rectangle x1="7.635240625" y1="1.905" x2="8.112759375" y2="1.915159375" layer="21"/>
<rectangle x1="8.620759375" y1="1.905" x2="9.0678" y2="1.915159375" layer="21"/>
<rectangle x1="9.525" y1="1.905" x2="9.870440625" y2="1.915159375" layer="21"/>
<rectangle x1="10.1854" y1="1.905" x2="10.51051875" y2="1.915159375" layer="21"/>
<rectangle x1="10.57148125" y1="1.905" x2="10.754359375" y2="1.915159375" layer="21"/>
<rectangle x1="11.160759375" y1="1.905" x2="11.343640625" y2="1.915159375" layer="21"/>
<rectangle x1="12.14628125" y1="1.905" x2="12.329159375" y2="1.915159375" layer="21"/>
<rectangle x1="12.827" y1="1.905" x2="13.00988125" y2="1.915159375" layer="21"/>
<rectangle x1="13.497559375" y1="1.905" x2="13.680440625" y2="1.915159375" layer="21"/>
<rectangle x1="13.7414" y1="1.905" x2="14.005559375" y2="1.915159375" layer="21"/>
<rectangle x1="14.290040625" y1="1.905" x2="14.57451875" y2="1.915159375" layer="21"/>
<rectangle x1="15.2654" y1="1.905" x2="15.60068125" y2="1.915159375" layer="21"/>
<rectangle x1="15.9258" y1="1.905" x2="16.271240625" y2="1.915159375" layer="21"/>
<rectangle x1="0.10668125" y1="1.915159375" x2="0.97028125" y2="1.92531875" layer="21"/>
<rectangle x1="1.77291875" y1="1.915159375" x2="2.23011875" y2="1.92531875" layer="21"/>
<rectangle x1="2.53491875" y1="1.915159375" x2="4.709159375" y2="1.92531875" layer="21"/>
<rectangle x1="5.115559375" y1="1.915159375" x2="6.517640625" y2="1.92531875" layer="21"/>
<rectangle x1="6.924040625" y1="1.915159375" x2="7.37108125" y2="1.92531875" layer="21"/>
<rectangle x1="7.62508125" y1="1.915159375" x2="8.1026" y2="1.92531875" layer="21"/>
<rectangle x1="8.63091875" y1="1.915159375" x2="9.077959375" y2="1.92531875" layer="21"/>
<rectangle x1="9.535159375" y1="1.915159375" x2="9.91108125" y2="1.92531875" layer="21"/>
<rectangle x1="10.15491875" y1="1.915159375" x2="10.51051875" y2="1.92531875" layer="21"/>
<rectangle x1="10.57148125" y1="1.915159375" x2="10.754359375" y2="1.92531875" layer="21"/>
<rectangle x1="11.160759375" y1="1.915159375" x2="11.343640625" y2="1.92531875" layer="21"/>
<rectangle x1="12.14628125" y1="1.915159375" x2="12.329159375" y2="1.92531875" layer="21"/>
<rectangle x1="12.837159375" y1="1.915159375" x2="13.020040625" y2="1.92531875" layer="21"/>
<rectangle x1="13.497559375" y1="1.915159375" x2="13.680440625" y2="1.92531875" layer="21"/>
<rectangle x1="13.7414" y1="1.915159375" x2="14.0462" y2="1.92531875" layer="21"/>
<rectangle x1="14.259559375" y1="1.915159375" x2="14.57451875" y2="1.92531875" layer="21"/>
<rectangle x1="15.275559375" y1="1.915159375" x2="15.65148125" y2="1.92531875" layer="21"/>
<rectangle x1="15.89531875" y1="1.915159375" x2="16.26108125" y2="1.92531875" layer="21"/>
<rectangle x1="0.10668125" y1="1.92531875" x2="0.929640625" y2="1.93548125" layer="21"/>
<rectangle x1="1.77291875" y1="1.92531875" x2="2.219959375" y2="1.93548125" layer="21"/>
<rectangle x1="2.524759375" y1="1.92531875" x2="3.81508125" y2="1.93548125" layer="21"/>
<rectangle x1="3.825240625" y1="1.92531875" x2="4.709159375" y2="1.93548125" layer="21"/>
<rectangle x1="5.115559375" y1="1.92531875" x2="6.50748125" y2="1.93548125" layer="21"/>
<rectangle x1="6.9342" y1="1.92531875" x2="7.381240625" y2="1.93548125" layer="21"/>
<rectangle x1="7.62508125" y1="1.92531875" x2="8.1026" y2="1.93548125" layer="21"/>
<rectangle x1="8.63091875" y1="1.92531875" x2="9.077959375" y2="1.93548125" layer="21"/>
<rectangle x1="9.54531875" y1="1.92531875" x2="9.96188125" y2="1.93548125" layer="21"/>
<rectangle x1="9.972040625" y1="1.92531875" x2="9.9822" y2="1.93548125" layer="21"/>
<rectangle x1="10.022840625" y1="1.92531875" x2="10.033" y2="1.93548125" layer="21"/>
<rectangle x1="10.0838" y1="1.92531875" x2="10.4902" y2="1.93548125" layer="21"/>
<rectangle x1="10.57148125" y1="1.92531875" x2="10.754359375" y2="1.93548125" layer="21"/>
<rectangle x1="11.17091875" y1="1.92531875" x2="11.3538" y2="1.93548125" layer="21"/>
<rectangle x1="12.14628125" y1="1.92531875" x2="12.329159375" y2="1.93548125" layer="21"/>
<rectangle x1="12.827" y1="1.92531875" x2="13.00988125" y2="1.93548125" layer="21"/>
<rectangle x1="13.497559375" y1="1.92531875" x2="13.680440625" y2="1.93548125" layer="21"/>
<rectangle x1="13.751559375" y1="1.92531875" x2="14.097" y2="1.93548125" layer="21"/>
<rectangle x1="14.107159375" y1="1.92531875" x2="14.11731875" y2="1.93548125" layer="21"/>
<rectangle x1="14.188440625" y1="1.92531875" x2="14.564359375" y2="1.93548125" layer="21"/>
<rectangle x1="15.28571875" y1="1.92531875" x2="15.70228125" y2="1.93548125" layer="21"/>
<rectangle x1="15.712440625" y1="1.92531875" x2="15.732759375" y2="1.93548125" layer="21"/>
<rectangle x1="15.7734" y1="1.92531875" x2="15.783559375" y2="1.93548125" layer="21"/>
<rectangle x1="15.814040625" y1="1.92531875" x2="15.8242" y2="1.93548125" layer="21"/>
<rectangle x1="15.834359375" y1="1.92531875" x2="16.25091875" y2="1.93548125" layer="21"/>
<rectangle x1="0.09651875" y1="1.93548125" x2="0.899159375" y2="1.945640625" layer="21"/>
<rectangle x1="1.77291875" y1="1.93548125" x2="2.23011875" y2="1.945640625" layer="21"/>
<rectangle x1="2.53491875" y1="1.93548125" x2="3.81508125" y2="1.945640625" layer="21"/>
<rectangle x1="3.825240625" y1="1.93548125" x2="4.699" y2="1.945640625" layer="21"/>
<rectangle x1="5.115559375" y1="1.93548125" x2="6.49731875" y2="1.945640625" layer="21"/>
<rectangle x1="6.9342" y1="1.93548125" x2="7.37108125" y2="1.945640625" layer="21"/>
<rectangle x1="7.61491875" y1="1.93548125" x2="8.092440625" y2="1.945640625" layer="21"/>
<rectangle x1="8.63091875" y1="1.93548125" x2="9.08811875" y2="1.945640625" layer="21"/>
<rectangle x1="9.55548125" y1="1.93548125" x2="10.480040625" y2="1.945640625" layer="21"/>
<rectangle x1="10.57148125" y1="1.93548125" x2="10.754359375" y2="1.945640625" layer="21"/>
<rectangle x1="11.160759375" y1="1.93548125" x2="11.343640625" y2="1.945640625" layer="21"/>
<rectangle x1="12.14628125" y1="1.93548125" x2="12.329159375" y2="1.945640625" layer="21"/>
<rectangle x1="12.837159375" y1="1.93548125" x2="13.020040625" y2="1.945640625" layer="21"/>
<rectangle x1="13.497559375" y1="1.93548125" x2="13.680440625" y2="1.945640625" layer="21"/>
<rectangle x1="13.77188125" y1="1.93548125" x2="14.5542" y2="1.945640625" layer="21"/>
<rectangle x1="15.29588125" y1="1.93548125" x2="16.240759375" y2="1.945640625" layer="21"/>
<rectangle x1="0.09651875" y1="1.945640625" x2="0.86868125" y2="1.9558" layer="21"/>
<rectangle x1="1.78308125" y1="1.945640625" x2="2.23011875" y2="1.9558" layer="21"/>
<rectangle x1="2.53491875" y1="1.945640625" x2="3.80491875" y2="1.9558" layer="21"/>
<rectangle x1="3.8354" y1="1.945640625" x2="4.699" y2="1.9558" layer="21"/>
<rectangle x1="5.1054" y1="1.945640625" x2="6.487159375" y2="1.9558" layer="21"/>
<rectangle x1="6.924040625" y1="1.945640625" x2="7.381240625" y2="1.9558" layer="21"/>
<rectangle x1="7.61491875" y1="1.945640625" x2="8.092440625" y2="1.9558" layer="21"/>
<rectangle x1="8.63091875" y1="1.945640625" x2="9.08811875" y2="1.9558" layer="21"/>
<rectangle x1="9.565640625" y1="1.945640625" x2="10.46988125" y2="1.9558" layer="21"/>
<rectangle x1="10.57148125" y1="1.945640625" x2="10.754359375" y2="1.9558" layer="21"/>
<rectangle x1="11.160759375" y1="1.945640625" x2="11.3538" y2="1.9558" layer="21"/>
<rectangle x1="12.14628125" y1="1.945640625" x2="12.329159375" y2="1.9558" layer="21"/>
<rectangle x1="12.827" y1="1.945640625" x2="13.00988125" y2="1.9558" layer="21"/>
<rectangle x1="13.497559375" y1="1.945640625" x2="13.680440625" y2="1.9558" layer="21"/>
<rectangle x1="13.77188125" y1="1.945640625" x2="14.544040625" y2="1.9558" layer="21"/>
<rectangle x1="15.306040625" y1="1.945640625" x2="16.2306" y2="1.9558" layer="21"/>
<rectangle x1="0.09651875" y1="1.9558" x2="0.8382" y2="1.965959375" layer="21"/>
<rectangle x1="1.78308125" y1="1.9558" x2="2.23011875" y2="1.965959375" layer="21"/>
<rectangle x1="2.524759375" y1="1.9558" x2="2.961640625" y2="1.965959375" layer="21"/>
<rectangle x1="2.9718" y1="1.9558" x2="3.80491875" y2="1.965959375" layer="21"/>
<rectangle x1="3.845559375" y1="1.9558" x2="4.688840625" y2="1.965959375" layer="21"/>
<rectangle x1="5.1054" y1="1.9558" x2="5.521959375" y2="1.965959375" layer="21"/>
<rectangle x1="5.53211875" y1="1.9558" x2="6.477" y2="1.965959375" layer="21"/>
<rectangle x1="6.9342" y1="1.9558" x2="7.381240625" y2="1.965959375" layer="21"/>
<rectangle x1="7.61491875" y1="1.9558" x2="8.092440625" y2="1.965959375" layer="21"/>
<rectangle x1="8.64108125" y1="1.9558" x2="9.08811875" y2="1.965959375" layer="21"/>
<rectangle x1="9.585959375" y1="1.9558" x2="10.45971875" y2="1.965959375" layer="21"/>
<rectangle x1="10.57148125" y1="1.9558" x2="10.754359375" y2="1.965959375" layer="21"/>
<rectangle x1="11.160759375" y1="1.9558" x2="11.343640625" y2="1.965959375" layer="21"/>
<rectangle x1="12.14628125" y1="1.9558" x2="12.329159375" y2="1.965959375" layer="21"/>
<rectangle x1="12.827" y1="1.9558" x2="13.020040625" y2="1.965959375" layer="21"/>
<rectangle x1="13.4874" y1="1.9558" x2="13.680440625" y2="1.965959375" layer="21"/>
<rectangle x1="13.782040625" y1="1.9558" x2="14.53388125" y2="1.965959375" layer="21"/>
<rectangle x1="15.326359375" y1="1.9558" x2="16.21028125" y2="1.965959375" layer="21"/>
<rectangle x1="0.086359375" y1="1.965959375" x2="0.80771875" y2="1.97611875" layer="21"/>
<rectangle x1="1.77291875" y1="1.965959375" x2="2.219959375" y2="1.97611875" layer="21"/>
<rectangle x1="2.53491875" y1="1.965959375" x2="2.961640625" y2="1.97611875" layer="21"/>
<rectangle x1="2.981959375" y1="1.965959375" x2="3.794759375" y2="1.97611875" layer="21"/>
<rectangle x1="3.845559375" y1="1.965959375" x2="4.688840625" y2="1.97611875" layer="21"/>
<rectangle x1="5.115559375" y1="1.965959375" x2="5.53211875" y2="1.97611875" layer="21"/>
<rectangle x1="5.54228125" y1="1.965959375" x2="6.466840625" y2="1.97611875" layer="21"/>
<rectangle x1="6.924040625" y1="1.965959375" x2="7.37108125" y2="1.97611875" layer="21"/>
<rectangle x1="7.604759375" y1="1.965959375" x2="8.08228125" y2="1.97611875" layer="21"/>
<rectangle x1="8.64108125" y1="1.965959375" x2="9.09828125" y2="1.97611875" layer="21"/>
<rectangle x1="9.59611875" y1="1.965959375" x2="10.4394" y2="1.97611875" layer="21"/>
<rectangle x1="10.57148125" y1="1.965959375" x2="10.754359375" y2="1.97611875" layer="21"/>
<rectangle x1="11.160759375" y1="1.965959375" x2="11.343640625" y2="1.97611875" layer="21"/>
<rectangle x1="12.14628125" y1="1.965959375" x2="12.329159375" y2="1.97611875" layer="21"/>
<rectangle x1="12.837159375" y1="1.965959375" x2="13.020040625" y2="1.97611875" layer="21"/>
<rectangle x1="13.497559375" y1="1.965959375" x2="13.680440625" y2="1.97611875" layer="21"/>
<rectangle x1="13.802359375" y1="1.965959375" x2="14.53388125" y2="1.97611875" layer="21"/>
<rectangle x1="15.33651875" y1="1.965959375" x2="16.20011875" y2="1.97611875" layer="21"/>
<rectangle x1="0.086359375" y1="1.97611875" x2="0.777240625" y2="1.98628125" layer="21"/>
<rectangle x1="1.78308125" y1="1.97611875" x2="2.23011875" y2="1.98628125" layer="21"/>
<rectangle x1="2.53491875" y1="1.97611875" x2="2.9718" y2="1.98628125" layer="21"/>
<rectangle x1="2.981959375" y1="1.97611875" x2="3.7846" y2="1.98628125" layer="21"/>
<rectangle x1="3.85571875" y1="1.97611875" x2="4.67868125" y2="1.98628125" layer="21"/>
<rectangle x1="5.115559375" y1="1.97611875" x2="5.53211875" y2="1.98628125" layer="21"/>
<rectangle x1="5.552440625" y1="1.97611875" x2="6.45668125" y2="1.98628125" layer="21"/>
<rectangle x1="6.924040625" y1="1.97611875" x2="7.381240625" y2="1.98628125" layer="21"/>
<rectangle x1="7.604759375" y1="1.97611875" x2="8.08228125" y2="1.98628125" layer="21"/>
<rectangle x1="8.64108125" y1="1.97611875" x2="9.09828125" y2="1.98628125" layer="21"/>
<rectangle x1="9.60628125" y1="1.97611875" x2="10.429240625" y2="1.98628125" layer="21"/>
<rectangle x1="10.57148125" y1="1.97611875" x2="10.754359375" y2="1.98628125" layer="21"/>
<rectangle x1="11.160759375" y1="1.97611875" x2="11.3538" y2="1.98628125" layer="21"/>
<rectangle x1="12.14628125" y1="1.97611875" x2="12.329159375" y2="1.98628125" layer="21"/>
<rectangle x1="12.827" y1="1.97611875" x2="13.020040625" y2="1.98628125" layer="21"/>
<rectangle x1="13.4874" y1="1.97611875" x2="13.680440625" y2="1.98628125" layer="21"/>
<rectangle x1="13.81251875" y1="1.97611875" x2="14.513559375" y2="1.98628125" layer="21"/>
<rectangle x1="15.34668125" y1="1.97611875" x2="16.189959375" y2="1.98628125" layer="21"/>
<rectangle x1="0.086359375" y1="1.98628125" x2="0.7366" y2="1.996440625" layer="21"/>
<rectangle x1="1.77291875" y1="1.98628125" x2="2.23011875" y2="1.996440625" layer="21"/>
<rectangle x1="2.53491875" y1="1.98628125" x2="2.961640625" y2="1.996440625" layer="21"/>
<rectangle x1="2.99211875" y1="1.98628125" x2="3.7846" y2="1.996440625" layer="21"/>
<rectangle x1="3.86588125" y1="1.98628125" x2="4.66851875" y2="1.996440625" layer="21"/>
<rectangle x1="5.115559375" y1="1.98628125" x2="5.521959375" y2="1.996440625" layer="21"/>
<rectangle x1="5.5626" y1="1.98628125" x2="6.44651875" y2="1.996440625" layer="21"/>
<rectangle x1="6.9342" y1="1.98628125" x2="7.381240625" y2="1.996440625" layer="21"/>
<rectangle x1="7.5946" y1="1.98628125" x2="8.08228125" y2="1.996440625" layer="21"/>
<rectangle x1="8.651240625" y1="1.98628125" x2="9.108440625" y2="1.996440625" layer="21"/>
<rectangle x1="9.6266" y1="1.98628125" x2="10.41908125" y2="1.996440625" layer="21"/>
<rectangle x1="10.57148125" y1="1.98628125" x2="10.754359375" y2="1.996440625" layer="21"/>
<rectangle x1="11.160759375" y1="1.98628125" x2="11.343640625" y2="1.996440625" layer="21"/>
<rectangle x1="12.14628125" y1="1.98628125" x2="12.329159375" y2="1.996440625" layer="21"/>
<rectangle x1="12.837159375" y1="1.98628125" x2="13.00988125" y2="1.996440625" layer="21"/>
<rectangle x1="13.497559375" y1="1.98628125" x2="13.67028125" y2="1.996440625" layer="21"/>
<rectangle x1="13.82268125" y1="1.98628125" x2="14.5034" y2="1.996440625" layer="21"/>
<rectangle x1="15.367" y1="1.98628125" x2="16.169640625" y2="1.996440625" layer="21"/>
<rectangle x1="0.0762" y1="1.996440625" x2="0.71628125" y2="2.0066" layer="21"/>
<rectangle x1="1.78308125" y1="1.996440625" x2="2.219959375" y2="2.0066" layer="21"/>
<rectangle x1="2.53491875" y1="1.996440625" x2="2.961640625" y2="2.0066" layer="21"/>
<rectangle x1="3.00228125" y1="1.996440625" x2="3.774440625" y2="2.0066" layer="21"/>
<rectangle x1="3.876040625" y1="1.996440625" x2="4.658359375" y2="2.0066" layer="21"/>
<rectangle x1="5.1054" y1="1.996440625" x2="5.53211875" y2="2.0066" layer="21"/>
<rectangle x1="5.5626" y1="1.996440625" x2="6.436359375" y2="2.0066" layer="21"/>
<rectangle x1="6.9342" y1="1.996440625" x2="7.37108125" y2="2.0066" layer="21"/>
<rectangle x1="7.5946" y1="1.996440625" x2="8.07211875" y2="2.0066" layer="21"/>
<rectangle x1="8.651240625" y1="1.996440625" x2="9.108440625" y2="2.0066" layer="21"/>
<rectangle x1="9.64691875" y1="1.996440625" x2="10.398759375" y2="2.0066" layer="21"/>
<rectangle x1="10.57148125" y1="1.996440625" x2="10.754359375" y2="2.0066" layer="21"/>
<rectangle x1="11.160759375" y1="1.996440625" x2="11.343640625" y2="2.0066" layer="21"/>
<rectangle x1="12.14628125" y1="1.996440625" x2="12.329159375" y2="2.0066" layer="21"/>
<rectangle x1="12.827" y1="1.996440625" x2="13.00988125" y2="2.0066" layer="21"/>
<rectangle x1="13.4874" y1="1.996440625" x2="13.680440625" y2="2.0066" layer="21"/>
<rectangle x1="13.832840625" y1="1.996440625" x2="14.493240625" y2="2.0066" layer="21"/>
<rectangle x1="15.38731875" y1="1.996440625" x2="16.14931875" y2="2.0066" layer="21"/>
<rectangle x1="0.0762" y1="2.0066" x2="0.695959375" y2="2.016759375" layer="21"/>
<rectangle x1="1.77291875" y1="2.0066" x2="2.23011875" y2="2.016759375" layer="21"/>
<rectangle x1="2.524759375" y1="2.0066" x2="2.9718" y2="2.016759375" layer="21"/>
<rectangle x1="3.012440625" y1="2.0066" x2="3.76428125" y2="2.016759375" layer="21"/>
<rectangle x1="3.876040625" y1="2.0066" x2="4.658359375" y2="2.016759375" layer="21"/>
<rectangle x1="5.115559375" y1="2.0066" x2="5.53211875" y2="2.016759375" layer="21"/>
<rectangle x1="5.58291875" y1="2.0066" x2="6.4262" y2="2.016759375" layer="21"/>
<rectangle x1="6.924040625" y1="2.0066" x2="7.381240625" y2="2.016759375" layer="21"/>
<rectangle x1="7.584440625" y1="2.0066" x2="8.07211875" y2="2.016759375" layer="21"/>
<rectangle x1="8.651240625" y1="2.0066" x2="9.108440625" y2="2.016759375" layer="21"/>
<rectangle x1="9.65708125" y1="2.0066" x2="10.378440625" y2="2.016759375" layer="21"/>
<rectangle x1="10.57148125" y1="2.0066" x2="10.754359375" y2="2.016759375" layer="21"/>
<rectangle x1="11.160759375" y1="2.0066" x2="11.343640625" y2="2.016759375" layer="21"/>
<rectangle x1="12.14628125" y1="2.0066" x2="12.329159375" y2="2.016759375" layer="21"/>
<rectangle x1="12.837159375" y1="2.0066" x2="13.020040625" y2="2.016759375" layer="21"/>
<rectangle x1="13.4874" y1="2.0066" x2="13.67028125" y2="2.016759375" layer="21"/>
<rectangle x1="13.843" y1="2.0066" x2="14.48308125" y2="2.016759375" layer="21"/>
<rectangle x1="15.407640625" y1="2.0066" x2="16.139159375" y2="2.016759375" layer="21"/>
<rectangle x1="0.0762" y1="2.016759375" x2="0.675640625" y2="2.02691875" layer="21"/>
<rectangle x1="1.78308125" y1="2.016759375" x2="2.219959375" y2="2.02691875" layer="21"/>
<rectangle x1="2.53491875" y1="2.016759375" x2="2.961640625" y2="2.02691875" layer="21"/>
<rectangle x1="3.012440625" y1="2.016759375" x2="3.75411875" y2="2.02691875" layer="21"/>
<rectangle x1="3.8862" y1="2.016759375" x2="4.6482" y2="2.02691875" layer="21"/>
<rectangle x1="5.115559375" y1="2.016759375" x2="5.521959375" y2="2.02691875" layer="21"/>
<rectangle x1="5.58291875" y1="2.016759375" x2="6.416040625" y2="2.02691875" layer="21"/>
<rectangle x1="6.9342" y1="2.016759375" x2="7.381240625" y2="2.02691875" layer="21"/>
<rectangle x1="7.584440625" y1="2.016759375" x2="8.061959375" y2="2.02691875" layer="21"/>
<rectangle x1="8.6614" y1="2.016759375" x2="9.1186" y2="2.02691875" layer="21"/>
<rectangle x1="9.6774" y1="2.016759375" x2="10.36828125" y2="2.02691875" layer="21"/>
<rectangle x1="10.57148125" y1="2.016759375" x2="10.754359375" y2="2.02691875" layer="21"/>
<rectangle x1="11.160759375" y1="2.016759375" x2="11.343640625" y2="2.02691875" layer="21"/>
<rectangle x1="12.14628125" y1="2.016759375" x2="12.329159375" y2="2.02691875" layer="21"/>
<rectangle x1="12.827" y1="2.016759375" x2="13.020040625" y2="2.02691875" layer="21"/>
<rectangle x1="13.4874" y1="2.016759375" x2="13.680440625" y2="2.02691875" layer="21"/>
<rectangle x1="13.86331875" y1="2.016759375" x2="14.462759375" y2="2.02691875" layer="21"/>
<rectangle x1="15.4178" y1="2.016759375" x2="16.118840625" y2="2.02691875" layer="21"/>
<rectangle x1="0.0762" y1="2.02691875" x2="0.66548125" y2="2.03708125" layer="21"/>
<rectangle x1="1.77291875" y1="2.02691875" x2="2.23011875" y2="2.03708125" layer="21"/>
<rectangle x1="2.53491875" y1="2.02691875" x2="2.9718" y2="2.03708125" layer="21"/>
<rectangle x1="3.032759375" y1="2.02691875" x2="3.75411875" y2="2.03708125" layer="21"/>
<rectangle x1="3.896359375" y1="2.02691875" x2="4.638040625" y2="2.03708125" layer="21"/>
<rectangle x1="5.115559375" y1="2.02691875" x2="5.53211875" y2="2.03708125" layer="21"/>
<rectangle x1="5.603240625" y1="2.02691875" x2="6.40588125" y2="2.03708125" layer="21"/>
<rectangle x1="6.924040625" y1="2.02691875" x2="7.381240625" y2="2.03708125" layer="21"/>
<rectangle x1="7.584440625" y1="2.02691875" x2="8.061959375" y2="2.03708125" layer="21"/>
<rectangle x1="8.6614" y1="2.02691875" x2="9.1186" y2="2.03708125" layer="21"/>
<rectangle x1="9.69771875" y1="2.02691875" x2="10.347959375" y2="2.03708125" layer="21"/>
<rectangle x1="10.57148125" y1="2.02691875" x2="10.754359375" y2="2.03708125" layer="21"/>
<rectangle x1="11.160759375" y1="2.02691875" x2="11.3538" y2="2.03708125" layer="21"/>
<rectangle x1="12.14628125" y1="2.02691875" x2="12.329159375" y2="2.03708125" layer="21"/>
<rectangle x1="12.827" y1="2.02691875" x2="13.00988125" y2="2.03708125" layer="21"/>
<rectangle x1="13.497559375" y1="2.02691875" x2="13.680440625" y2="2.03708125" layer="21"/>
<rectangle x1="13.883640625" y1="2.02691875" x2="14.4526" y2="2.03708125" layer="21"/>
<rectangle x1="15.43811875" y1="2.02691875" x2="16.09851875" y2="2.03708125" layer="21"/>
<rectangle x1="0.0762" y1="2.03708125" x2="0.645159375" y2="2.047240625" layer="21"/>
<rectangle x1="1.78308125" y1="2.03708125" x2="2.23011875" y2="2.047240625" layer="21"/>
<rectangle x1="2.524759375" y1="2.03708125" x2="2.9718" y2="2.047240625" layer="21"/>
<rectangle x1="3.032759375" y1="2.03708125" x2="3.743959375" y2="2.047240625" layer="21"/>
<rectangle x1="3.90651875" y1="2.03708125" x2="4.62788125" y2="2.047240625" layer="21"/>
<rectangle x1="5.115559375" y1="2.03708125" x2="5.53211875" y2="2.047240625" layer="21"/>
<rectangle x1="5.6134" y1="2.03708125" x2="6.385559375" y2="2.047240625" layer="21"/>
<rectangle x1="6.9342" y1="2.03708125" x2="7.381240625" y2="2.047240625" layer="21"/>
<rectangle x1="7.57428125" y1="2.03708125" x2="8.061959375" y2="2.047240625" layer="21"/>
<rectangle x1="8.6614" y1="2.03708125" x2="9.1186" y2="2.047240625" layer="21"/>
<rectangle x1="9.7282" y1="2.03708125" x2="10.31748125" y2="2.047240625" layer="21"/>
<rectangle x1="10.57148125" y1="2.03708125" x2="10.754359375" y2="2.047240625" layer="21"/>
<rectangle x1="11.160759375" y1="2.03708125" x2="11.343640625" y2="2.047240625" layer="21"/>
<rectangle x1="12.14628125" y1="2.03708125" x2="12.329159375" y2="2.047240625" layer="21"/>
<rectangle x1="12.827" y1="2.03708125" x2="13.020040625" y2="2.047240625" layer="21"/>
<rectangle x1="13.4874" y1="2.03708125" x2="13.680440625" y2="2.047240625" layer="21"/>
<rectangle x1="13.8938" y1="2.03708125" x2="14.43228125" y2="2.047240625" layer="21"/>
<rectangle x1="15.4686" y1="2.03708125" x2="16.0782" y2="2.047240625" layer="21"/>
<rectangle x1="0.066040625" y1="2.047240625" x2="0.624840625" y2="2.0574" layer="21"/>
<rectangle x1="1.77291875" y1="2.047240625" x2="2.219959375" y2="2.0574" layer="21"/>
<rectangle x1="2.53491875" y1="2.047240625" x2="2.961640625" y2="2.0574" layer="21"/>
<rectangle x1="3.05308125" y1="2.047240625" x2="3.723640625" y2="2.0574" layer="21"/>
<rectangle x1="3.91668125" y1="2.047240625" x2="4.61771875" y2="2.0574" layer="21"/>
<rectangle x1="5.115559375" y1="2.047240625" x2="5.521959375" y2="2.0574" layer="21"/>
<rectangle x1="5.623559375" y1="2.047240625" x2="6.3754" y2="2.0574" layer="21"/>
<rectangle x1="6.924040625" y1="2.047240625" x2="7.37108125" y2="2.0574" layer="21"/>
<rectangle x1="7.57428125" y1="2.047240625" x2="8.0518" y2="2.0574" layer="21"/>
<rectangle x1="8.671559375" y1="2.047240625" x2="9.128759375" y2="2.0574" layer="21"/>
<rectangle x1="9.74851875" y1="2.047240625" x2="10.297159375" y2="2.0574" layer="21"/>
<rectangle x1="10.57148125" y1="2.047240625" x2="10.754359375" y2="2.0574" layer="21"/>
<rectangle x1="11.160759375" y1="2.047240625" x2="11.3538" y2="2.0574" layer="21"/>
<rectangle x1="12.14628125" y1="2.047240625" x2="12.329159375" y2="2.0574" layer="21"/>
<rectangle x1="12.837159375" y1="2.047240625" x2="13.00988125" y2="2.0574" layer="21"/>
<rectangle x1="13.4874" y1="2.047240625" x2="13.67028125" y2="2.0574" layer="21"/>
<rectangle x1="13.92428125" y1="2.047240625" x2="14.4018" y2="2.0574" layer="21"/>
<rectangle x1="15.48891875" y1="2.047240625" x2="16.04771875" y2="2.0574" layer="21"/>
<rectangle x1="0.066040625" y1="2.0574" x2="0.61468125" y2="2.067559375" layer="21"/>
<rectangle x1="1.78308125" y1="2.0574" x2="2.23011875" y2="2.067559375" layer="21"/>
<rectangle x1="2.53491875" y1="2.0574" x2="2.9718" y2="2.067559375" layer="21"/>
<rectangle x1="3.063240625" y1="2.0574" x2="3.723640625" y2="2.067559375" layer="21"/>
<rectangle x1="3.926840625" y1="2.0574" x2="4.607559375" y2="2.067559375" layer="21"/>
<rectangle x1="5.115559375" y1="2.0574" x2="5.53211875" y2="2.067559375" layer="21"/>
<rectangle x1="5.64388125" y1="2.0574" x2="6.35508125" y2="2.067559375" layer="21"/>
<rectangle x1="6.9342" y1="2.0574" x2="7.381240625" y2="2.067559375" layer="21"/>
<rectangle x1="7.56411875" y1="2.0574" x2="8.0518" y2="2.067559375" layer="21"/>
<rectangle x1="8.671559375" y1="2.0574" x2="9.128759375" y2="2.067559375" layer="21"/>
<rectangle x1="9.779" y1="2.0574" x2="10.26668125" y2="2.067559375" layer="21"/>
<rectangle x1="10.57148125" y1="2.0574" x2="10.754359375" y2="2.067559375" layer="21"/>
<rectangle x1="11.17091875" y1="2.0574" x2="11.343640625" y2="2.067559375" layer="21"/>
<rectangle x1="12.14628125" y1="2.0574" x2="12.1666" y2="2.067559375" layer="21"/>
<rectangle x1="12.176759375" y1="2.0574" x2="12.329159375" y2="2.067559375" layer="21"/>
<rectangle x1="12.837159375" y1="2.0574" x2="13.00988125" y2="2.067559375" layer="21"/>
<rectangle x1="13.497559375" y1="2.0574" x2="13.67028125" y2="2.067559375" layer="21"/>
<rectangle x1="13.9446" y1="2.0574" x2="14.391640625" y2="2.067559375" layer="21"/>
<rectangle x1="15.5194" y1="2.0574" x2="16.017240625" y2="2.067559375" layer="21"/>
<rectangle x1="0.066040625" y1="2.067559375" x2="0.594359375" y2="2.07771875" layer="21"/>
<rectangle x1="1.77291875" y1="2.067559375" x2="2.219959375" y2="2.07771875" layer="21"/>
<rectangle x1="2.53491875" y1="2.067559375" x2="2.961640625" y2="2.07771875" layer="21"/>
<rectangle x1="3.0734" y1="2.067559375" x2="3.70331875" y2="2.07771875" layer="21"/>
<rectangle x1="3.937" y1="2.067559375" x2="4.5974" y2="2.07771875" layer="21"/>
<rectangle x1="5.1054" y1="2.067559375" x2="5.53211875" y2="2.07771875" layer="21"/>
<rectangle x1="5.654040625" y1="2.067559375" x2="6.34491875" y2="2.07771875" layer="21"/>
<rectangle x1="6.924040625" y1="2.067559375" x2="7.381240625" y2="2.07771875" layer="21"/>
<rectangle x1="7.56411875" y1="2.067559375" x2="8.041640625" y2="2.07771875" layer="21"/>
<rectangle x1="8.68171875" y1="2.067559375" x2="9.13891875" y2="2.07771875" layer="21"/>
<rectangle x1="9.80948125" y1="2.067559375" x2="10.2362" y2="2.07771875" layer="21"/>
<rectangle x1="10.57148125" y1="2.067559375" x2="10.754359375" y2="2.07771875" layer="21"/>
<rectangle x1="13.96491875" y1="2.067559375" x2="14.361159375" y2="2.07771875" layer="21"/>
<rectangle x1="15.54988125" y1="2.067559375" x2="15.986759375" y2="2.07771875" layer="21"/>
<rectangle x1="0.066040625" y1="2.07771875" x2="0.5842" y2="2.08788125" layer="21"/>
<rectangle x1="1.78308125" y1="2.07771875" x2="2.23011875" y2="2.08788125" layer="21"/>
<rectangle x1="2.524759375" y1="2.07771875" x2="2.961640625" y2="2.08788125" layer="21"/>
<rectangle x1="3.083559375" y1="2.07771875" x2="3.693159375" y2="2.08788125" layer="21"/>
<rectangle x1="3.95731875" y1="2.07771875" x2="4.57708125" y2="2.08788125" layer="21"/>
<rectangle x1="5.115559375" y1="2.07771875" x2="5.521959375" y2="2.08788125" layer="21"/>
<rectangle x1="5.6642" y1="2.07771875" x2="6.3246" y2="2.08788125" layer="21"/>
<rectangle x1="6.9342" y1="2.07771875" x2="7.37108125" y2="2.08788125" layer="21"/>
<rectangle x1="7.553959375" y1="2.07771875" x2="8.041640625" y2="2.08788125" layer="21"/>
<rectangle x1="8.68171875" y1="2.07771875" x2="9.13891875" y2="2.08788125" layer="21"/>
<rectangle x1="9.85011875" y1="2.07771875" x2="10.195559375" y2="2.08788125" layer="21"/>
<rectangle x1="10.57148125" y1="2.07771875" x2="10.754359375" y2="2.08788125" layer="21"/>
<rectangle x1="14.005559375" y1="2.07771875" x2="14.33068125" y2="2.08788125" layer="21"/>
<rectangle x1="15.60068125" y1="2.07771875" x2="15.94611875" y2="2.08788125" layer="21"/>
<rectangle x1="0.066040625" y1="2.08788125" x2="0.5842" y2="2.098040625" layer="21"/>
<rectangle x1="1.77291875" y1="2.08788125" x2="2.23011875" y2="2.098040625" layer="21"/>
<rectangle x1="2.53491875" y1="2.08788125" x2="2.9718" y2="2.098040625" layer="21"/>
<rectangle x1="3.10388125" y1="2.08788125" x2="3.672840625" y2="2.098040625" layer="21"/>
<rectangle x1="3.96748125" y1="2.08788125" x2="4.56691875" y2="2.098040625" layer="21"/>
<rectangle x1="5.115559375" y1="2.08788125" x2="5.53211875" y2="2.098040625" layer="21"/>
<rectangle x1="5.68451875" y1="2.08788125" x2="6.30428125" y2="2.098040625" layer="21"/>
<rectangle x1="6.924040625" y1="2.08788125" x2="7.381240625" y2="2.098040625" layer="21"/>
<rectangle x1="7.553959375" y1="2.08788125" x2="8.041640625" y2="2.098040625" layer="21"/>
<rectangle x1="8.68171875" y1="2.08788125" x2="9.13891875" y2="2.098040625" layer="21"/>
<rectangle x1="9.90091875" y1="2.08788125" x2="10.144759375" y2="2.098040625" layer="21"/>
<rectangle x1="10.57148125" y1="2.08788125" x2="10.754359375" y2="2.098040625" layer="21"/>
<rectangle x1="14.0462" y1="2.08788125" x2="14.259559375" y2="2.098040625" layer="21"/>
<rectangle x1="14.26971875" y1="2.08788125" x2="14.27988125" y2="2.098040625" layer="21"/>
<rectangle x1="15.64131875" y1="2.08788125" x2="15.65148125" y2="2.098040625" layer="21"/>
<rectangle x1="15.661640625" y1="2.08788125" x2="15.89531875" y2="2.098040625" layer="21"/>
<rectangle x1="0.066040625" y1="2.098040625" x2="0.574040625" y2="2.1082" layer="21"/>
<rectangle x1="1.78308125" y1="2.098040625" x2="2.219959375" y2="2.1082" layer="21"/>
<rectangle x1="2.53491875" y1="2.098040625" x2="2.961640625" y2="2.1082" layer="21"/>
<rectangle x1="3.1242" y1="2.098040625" x2="3.66268125" y2="2.1082" layer="21"/>
<rectangle x1="3.9878" y1="2.098040625" x2="4.556759375" y2="2.1082" layer="21"/>
<rectangle x1="5.115559375" y1="2.098040625" x2="5.53211875" y2="2.1082" layer="21"/>
<rectangle x1="5.704840625" y1="2.098040625" x2="6.283959375" y2="2.1082" layer="21"/>
<rectangle x1="6.9342" y1="2.098040625" x2="7.381240625" y2="2.1082" layer="21"/>
<rectangle x1="7.5438" y1="2.098040625" x2="8.03148125" y2="2.1082" layer="21"/>
<rectangle x1="8.68171875" y1="2.098040625" x2="9.14908125" y2="2.1082" layer="21"/>
<rectangle x1="10.022840625" y1="2.098040625" x2="10.033" y2="2.1082" layer="21"/>
<rectangle x1="10.57148125" y1="2.098040625" x2="10.754359375" y2="2.1082" layer="21"/>
<rectangle x1="14.16811875" y1="2.098040625" x2="14.17828125" y2="2.1082" layer="21"/>
<rectangle x1="15.7226" y1="2.098040625" x2="15.732759375" y2="2.1082" layer="21"/>
<rectangle x1="15.79371875" y1="2.098040625" x2="15.80388125" y2="2.1082" layer="21"/>
<rectangle x1="0.066040625" y1="2.1082" x2="0.574040625" y2="2.118359375" layer="21"/>
<rectangle x1="1.77291875" y1="2.1082" x2="2.23011875" y2="2.118359375" layer="21"/>
<rectangle x1="2.53491875" y1="2.1082" x2="2.961640625" y2="2.118359375" layer="21"/>
<rectangle x1="3.134359375" y1="2.1082" x2="3.642359375" y2="2.118359375" layer="21"/>
<rectangle x1="3.997959375" y1="2.1082" x2="4.536440625" y2="2.118359375" layer="21"/>
<rectangle x1="5.1054" y1="2.1082" x2="5.53211875" y2="2.118359375" layer="21"/>
<rectangle x1="5.725159375" y1="2.1082" x2="6.263640625" y2="2.118359375" layer="21"/>
<rectangle x1="6.924040625" y1="2.1082" x2="7.381240625" y2="2.118359375" layer="21"/>
<rectangle x1="7.5438" y1="2.1082" x2="8.03148125" y2="2.118359375" layer="21"/>
<rectangle x1="8.69188125" y1="2.1082" x2="9.14908125" y2="2.118359375" layer="21"/>
<rectangle x1="10.57148125" y1="2.1082" x2="10.754359375" y2="2.118359375" layer="21"/>
<rectangle x1="0.066040625" y1="2.118359375" x2="0.56388125" y2="2.12851875" layer="21"/>
<rectangle x1="1.78308125" y1="2.118359375" x2="2.23011875" y2="2.12851875" layer="21"/>
<rectangle x1="2.53491875" y1="2.118359375" x2="2.961640625" y2="2.12851875" layer="21"/>
<rectangle x1="3.15468125" y1="2.118359375" x2="3.622040625" y2="2.12851875" layer="21"/>
<rectangle x1="4.01828125" y1="2.118359375" x2="4.51611875" y2="2.12851875" layer="21"/>
<rectangle x1="5.115559375" y1="2.118359375" x2="5.53211875" y2="2.12851875" layer="21"/>
<rectangle x1="5.74548125" y1="2.118359375" x2="6.24331875" y2="2.12851875" layer="21"/>
<rectangle x1="6.924040625" y1="2.118359375" x2="7.381240625" y2="2.12851875" layer="21"/>
<rectangle x1="7.5438" y1="2.118359375" x2="8.03148125" y2="2.12851875" layer="21"/>
<rectangle x1="8.69188125" y1="2.118359375" x2="9.159240625" y2="2.12851875" layer="21"/>
<rectangle x1="10.57148125" y1="2.118359375" x2="10.754359375" y2="2.12851875" layer="21"/>
<rectangle x1="0.066040625" y1="2.12851875" x2="0.55371875" y2="2.13868125" layer="21"/>
<rectangle x1="1.78308125" y1="2.12851875" x2="1.82371875" y2="2.13868125" layer="21"/>
<rectangle x1="1.83388125" y1="2.12851875" x2="1.88468125" y2="2.13868125" layer="21"/>
<rectangle x1="1.894840625" y1="2.12851875" x2="1.945640625" y2="2.13868125" layer="21"/>
<rectangle x1="1.9558" y1="2.12851875" x2="2.0066" y2="2.13868125" layer="21"/>
<rectangle x1="2.016759375" y1="2.12851875" x2="2.067559375" y2="2.13868125" layer="21"/>
<rectangle x1="2.07771875" y1="2.12851875" x2="2.12851875" y2="2.13868125" layer="21"/>
<rectangle x1="2.13868125" y1="2.12851875" x2="2.17931875" y2="2.13868125" layer="21"/>
<rectangle x1="2.18948125" y1="2.12851875" x2="2.219959375" y2="2.13868125" layer="21"/>
<rectangle x1="2.53491875" y1="2.12851875" x2="2.555240625" y2="2.13868125" layer="21"/>
<rectangle x1="2.5654" y1="2.12851875" x2="2.6162" y2="2.13868125" layer="21"/>
<rectangle x1="2.626359375" y1="2.12851875" x2="2.677159375" y2="2.13868125" layer="21"/>
<rectangle x1="2.68731875" y1="2.12851875" x2="2.727959375" y2="2.13868125" layer="21"/>
<rectangle x1="2.73811875" y1="2.12851875" x2="2.78891875" y2="2.13868125" layer="21"/>
<rectangle x1="2.79908125" y1="2.12851875" x2="2.829559375" y2="2.13868125" layer="21"/>
<rectangle x1="2.83971875" y1="2.12851875" x2="2.910840625" y2="2.13868125" layer="21"/>
<rectangle x1="2.921" y1="2.12851875" x2="2.961640625" y2="2.13868125" layer="21"/>
<rectangle x1="3.185159375" y1="2.12851875" x2="3.591559375" y2="2.13868125" layer="21"/>
<rectangle x1="4.048759375" y1="2.12851875" x2="4.485640625" y2="2.13868125" layer="21"/>
<rectangle x1="5.115559375" y1="2.12851875" x2="5.18668125" y2="2.13868125" layer="21"/>
<rectangle x1="5.196840625" y1="2.12851875" x2="5.23748125" y2="2.13868125" layer="21"/>
<rectangle x1="5.247640625" y1="2.12851875" x2="5.298440625" y2="2.13868125" layer="21"/>
<rectangle x1="5.3086" y1="2.12851875" x2="5.349240625" y2="2.13868125" layer="21"/>
<rectangle x1="5.3594" y1="2.12851875" x2="5.400040625" y2="2.13868125" layer="21"/>
<rectangle x1="5.4102" y1="2.12851875" x2="5.461" y2="2.13868125" layer="21"/>
<rectangle x1="5.471159375" y1="2.12851875" x2="5.521959375" y2="2.13868125" layer="21"/>
<rectangle x1="5.7658" y1="2.12851875" x2="6.20268125" y2="2.13868125" layer="21"/>
<rectangle x1="6.9342" y1="2.12851875" x2="7.37108125" y2="2.13868125" layer="21"/>
<rectangle x1="7.5438" y1="2.12851875" x2="7.57428125" y2="2.13868125" layer="21"/>
<rectangle x1="7.584440625" y1="2.12851875" x2="7.62508125" y2="2.13868125" layer="21"/>
<rectangle x1="7.635240625" y1="2.12851875" x2="7.67588125" y2="2.13868125" layer="21"/>
<rectangle x1="7.686040625" y1="2.12851875" x2="7.736840625" y2="2.13868125" layer="21"/>
<rectangle x1="7.747" y1="2.12851875" x2="7.82828125" y2="2.13868125" layer="21"/>
<rectangle x1="7.838440625" y1="2.12851875" x2="7.86891875" y2="2.13868125" layer="21"/>
<rectangle x1="7.87908125" y1="2.12851875" x2="7.9502" y2="2.13868125" layer="21"/>
<rectangle x1="7.97051875" y1="2.12851875" x2="8.011159375" y2="2.13868125" layer="21"/>
<rectangle x1="8.702040625" y1="2.12851875" x2="8.73251875" y2="2.13868125" layer="21"/>
<rectangle x1="8.74268125" y1="2.12851875" x2="8.79348125" y2="2.13868125" layer="21"/>
<rectangle x1="8.803640625" y1="2.12851875" x2="8.854440625" y2="2.13868125" layer="21"/>
<rectangle x1="8.8646" y1="2.12851875" x2="8.905240625" y2="2.13868125" layer="21"/>
<rectangle x1="8.9154" y1="2.12851875" x2="8.9662" y2="2.13868125" layer="21"/>
<rectangle x1="8.976359375" y1="2.12851875" x2="9.006840625" y2="2.13868125" layer="21"/>
<rectangle x1="9.017" y1="2.12851875" x2="9.08811875" y2="2.13868125" layer="21"/>
<rectangle x1="9.09828125" y1="2.12851875" x2="9.14908125" y2="2.13868125" layer="21"/>
<rectangle x1="10.57148125" y1="2.12851875" x2="10.754359375" y2="2.13868125" layer="21"/>
<rectangle x1="0.066040625" y1="2.13868125" x2="0.55371875" y2="2.148840625" layer="21"/>
<rectangle x1="3.215640625" y1="2.13868125" x2="3.571240625" y2="2.148840625" layer="21"/>
<rectangle x1="4.06908125" y1="2.13868125" x2="4.46531875" y2="2.148840625" layer="21"/>
<rectangle x1="5.79628125" y1="2.13868125" x2="6.182359375" y2="2.148840625" layer="21"/>
<rectangle x1="6.9342" y1="2.13868125" x2="7.381240625" y2="2.148840625" layer="21"/>
<rectangle x1="10.57148125" y1="2.13868125" x2="10.754359375" y2="2.148840625" layer="21"/>
<rectangle x1="0.066040625" y1="2.148840625" x2="0.55371875" y2="2.159" layer="21"/>
<rectangle x1="3.235959375" y1="2.148840625" x2="3.5306" y2="2.159" layer="21"/>
<rectangle x1="4.10971875" y1="2.148840625" x2="4.42468125" y2="2.159" layer="21"/>
<rectangle x1="5.826759375" y1="2.148840625" x2="6.14171875" y2="2.159" layer="21"/>
<rectangle x1="6.924040625" y1="2.148840625" x2="7.381240625" y2="2.159" layer="21"/>
<rectangle x1="10.57148125" y1="2.148840625" x2="10.754359375" y2="2.159" layer="21"/>
<rectangle x1="0.066040625" y1="2.159" x2="0.55371875" y2="2.169159375" layer="21"/>
<rectangle x1="3.286759375" y1="2.159" x2="3.469640625" y2="2.169159375" layer="21"/>
<rectangle x1="3.4798" y1="2.159" x2="3.489959375" y2="2.169159375" layer="21"/>
<rectangle x1="4.16051875" y1="2.159" x2="4.37388125" y2="2.169159375" layer="21"/>
<rectangle x1="5.8674" y1="2.159" x2="5.877559375" y2="2.169159375" layer="21"/>
<rectangle x1="5.88771875" y1="2.159" x2="6.080759375" y2="2.169159375" layer="21"/>
<rectangle x1="6.9342" y1="2.159" x2="7.381240625" y2="2.169159375" layer="21"/>
<rectangle x1="10.57148125" y1="2.159" x2="10.754359375" y2="2.169159375" layer="21"/>
<rectangle x1="0.066040625" y1="2.169159375" x2="0.543559375" y2="2.17931875" layer="21"/>
<rectangle x1="6.924040625" y1="2.169159375" x2="7.37108125" y2="2.17931875" layer="21"/>
<rectangle x1="10.57148125" y1="2.169159375" x2="10.754359375" y2="2.17931875" layer="21"/>
<rectangle x1="0.05588125" y1="2.17931875" x2="0.55371875" y2="2.18948125" layer="21"/>
<rectangle x1="6.924040625" y1="2.17931875" x2="7.381240625" y2="2.18948125" layer="21"/>
<rectangle x1="10.57148125" y1="2.17931875" x2="10.754359375" y2="2.18948125" layer="21"/>
<rectangle x1="0.066040625" y1="2.18948125" x2="0.543559375" y2="2.199640625" layer="21"/>
<rectangle x1="6.9342" y1="2.18948125" x2="7.381240625" y2="2.199640625" layer="21"/>
<rectangle x1="10.57148125" y1="2.18948125" x2="10.754359375" y2="2.199640625" layer="21"/>
<rectangle x1="0.066040625" y1="2.199640625" x2="0.543559375" y2="2.2098" layer="21"/>
<rectangle x1="6.924040625" y1="2.199640625" x2="7.37108125" y2="2.2098" layer="21"/>
<rectangle x1="10.57148125" y1="2.199640625" x2="10.754359375" y2="2.2098" layer="21"/>
<rectangle x1="0.066040625" y1="2.2098" x2="0.55371875" y2="2.219959375" layer="21"/>
<rectangle x1="6.9342" y1="2.2098" x2="7.381240625" y2="2.219959375" layer="21"/>
<rectangle x1="10.57148125" y1="2.2098" x2="10.754359375" y2="2.219959375" layer="21"/>
<rectangle x1="0.066040625" y1="2.219959375" x2="0.543559375" y2="2.23011875" layer="21"/>
<rectangle x1="6.9342" y1="2.219959375" x2="7.37108125" y2="2.23011875" layer="21"/>
<rectangle x1="10.57148125" y1="2.219959375" x2="10.754359375" y2="2.23011875" layer="21"/>
<rectangle x1="0.066040625" y1="2.23011875" x2="0.55371875" y2="2.24028125" layer="21"/>
<rectangle x1="6.924040625" y1="2.23011875" x2="7.381240625" y2="2.24028125" layer="21"/>
<rectangle x1="10.57148125" y1="2.23011875" x2="10.754359375" y2="2.24028125" layer="21"/>
<rectangle x1="0.066040625" y1="2.24028125" x2="0.55371875" y2="2.250440625" layer="21"/>
<rectangle x1="6.9342" y1="2.24028125" x2="7.381240625" y2="2.250440625" layer="21"/>
<rectangle x1="10.57148125" y1="2.24028125" x2="10.754359375" y2="2.250440625" layer="21"/>
<rectangle x1="0.066040625" y1="2.250440625" x2="0.55371875" y2="2.2606" layer="21"/>
<rectangle x1="6.924040625" y1="2.250440625" x2="7.381240625" y2="2.2606" layer="21"/>
<rectangle x1="10.57148125" y1="2.250440625" x2="10.754359375" y2="2.2606" layer="21"/>
<rectangle x1="0.066040625" y1="2.2606" x2="0.56388125" y2="2.270759375" layer="21"/>
<rectangle x1="6.924040625" y1="2.2606" x2="7.37108125" y2="2.270759375" layer="21"/>
<rectangle x1="10.57148125" y1="2.2606" x2="10.754359375" y2="2.270759375" layer="21"/>
<rectangle x1="0.066040625" y1="2.270759375" x2="0.56388125" y2="2.28091875" layer="21"/>
<rectangle x1="1.17348125" y1="2.270759375" x2="1.1938" y2="2.28091875" layer="21"/>
<rectangle x1="6.9342" y1="2.270759375" x2="7.381240625" y2="2.28091875" layer="21"/>
<rectangle x1="10.57148125" y1="2.270759375" x2="10.754359375" y2="2.28091875" layer="21"/>
<rectangle x1="0.0762" y1="2.28091875" x2="0.574040625" y2="2.29108125" layer="21"/>
<rectangle x1="1.16331875" y1="2.28091875" x2="1.1938" y2="2.29108125" layer="21"/>
<rectangle x1="6.924040625" y1="2.28091875" x2="7.37108125" y2="2.29108125" layer="21"/>
<rectangle x1="10.57148125" y1="2.28091875" x2="10.754359375" y2="2.29108125" layer="21"/>
<rectangle x1="0.0762" y1="2.29108125" x2="0.574040625" y2="2.301240625" layer="21"/>
<rectangle x1="1.153159375" y1="2.29108125" x2="1.203959375" y2="2.301240625" layer="21"/>
<rectangle x1="6.9342" y1="2.29108125" x2="7.381240625" y2="2.301240625" layer="21"/>
<rectangle x1="10.57148125" y1="2.29108125" x2="10.754359375" y2="2.301240625" layer="21"/>
<rectangle x1="0.0762" y1="2.301240625" x2="0.5842" y2="2.3114" layer="21"/>
<rectangle x1="1.143" y1="2.301240625" x2="1.21411875" y2="2.3114" layer="21"/>
<rectangle x1="6.9342" y1="2.301240625" x2="7.381240625" y2="2.3114" layer="21"/>
<rectangle x1="10.57148125" y1="2.301240625" x2="10.754359375" y2="2.3114" layer="21"/>
<rectangle x1="0.0762" y1="2.3114" x2="0.594359375" y2="2.321559375" layer="21"/>
<rectangle x1="1.132840625" y1="2.3114" x2="1.22428125" y2="2.321559375" layer="21"/>
<rectangle x1="6.924040625" y1="2.3114" x2="7.381240625" y2="2.321559375" layer="21"/>
<rectangle x1="10.57148125" y1="2.3114" x2="10.754359375" y2="2.321559375" layer="21"/>
<rectangle x1="0.0762" y1="2.321559375" x2="0.60451875" y2="2.33171875" layer="21"/>
<rectangle x1="1.11251875" y1="2.321559375" x2="1.234440625" y2="2.33171875" layer="21"/>
<rectangle x1="1.93548125" y1="2.321559375" x2="2.067559375" y2="2.33171875" layer="21"/>
<rectangle x1="6.9342" y1="2.321559375" x2="7.37108125" y2="2.33171875" layer="21"/>
<rectangle x1="10.57148125" y1="2.321559375" x2="10.754359375" y2="2.33171875" layer="21"/>
<rectangle x1="0.086359375" y1="2.33171875" x2="0.61468125" y2="2.34188125" layer="21"/>
<rectangle x1="1.102359375" y1="2.33171875" x2="1.2446" y2="2.34188125" layer="21"/>
<rectangle x1="1.905" y1="2.33171875" x2="2.098040625" y2="2.34188125" layer="21"/>
<rectangle x1="6.924040625" y1="2.33171875" x2="7.381240625" y2="2.34188125" layer="21"/>
<rectangle x1="10.57148125" y1="2.33171875" x2="10.754359375" y2="2.34188125" layer="21"/>
<rectangle x1="0.086359375" y1="2.34188125" x2="0.624840625" y2="2.352040625" layer="21"/>
<rectangle x1="1.082040625" y1="2.34188125" x2="1.254759375" y2="2.352040625" layer="21"/>
<rectangle x1="1.88468125" y1="2.34188125" x2="2.118359375" y2="2.352040625" layer="21"/>
<rectangle x1="6.924040625" y1="2.34188125" x2="7.37108125" y2="2.352040625" layer="21"/>
<rectangle x1="10.57148125" y1="2.34188125" x2="10.754359375" y2="2.352040625" layer="21"/>
<rectangle x1="0.086359375" y1="2.352040625" x2="0.645159375" y2="2.3622" layer="21"/>
<rectangle x1="1.06171875" y1="2.352040625" x2="1.26491875" y2="2.3622" layer="21"/>
<rectangle x1="1.864359375" y1="2.352040625" x2="2.13868125" y2="2.3622" layer="21"/>
<rectangle x1="6.9342" y1="2.352040625" x2="7.381240625" y2="2.3622" layer="21"/>
<rectangle x1="10.57148125" y1="2.352040625" x2="10.754359375" y2="2.3622" layer="21"/>
<rectangle x1="0.086359375" y1="2.3622" x2="0.66548125" y2="2.372359375" layer="21"/>
<rectangle x1="1.0414" y1="2.3622" x2="1.27508125" y2="2.372359375" layer="21"/>
<rectangle x1="1.8542" y1="2.3622" x2="2.148840625" y2="2.372359375" layer="21"/>
<rectangle x1="6.924040625" y1="2.3622" x2="7.381240625" y2="2.372359375" layer="21"/>
<rectangle x1="10.57148125" y1="2.3622" x2="10.754359375" y2="2.372359375" layer="21"/>
<rectangle x1="0.09651875" y1="2.372359375" x2="0.6858" y2="2.38251875" layer="21"/>
<rectangle x1="1.01091875" y1="2.372359375" x2="1.285240625" y2="2.38251875" layer="21"/>
<rectangle x1="1.83388125" y1="2.372359375" x2="2.169159375" y2="2.38251875" layer="21"/>
<rectangle x1="6.9342" y1="2.372359375" x2="7.37108125" y2="2.38251875" layer="21"/>
<rectangle x1="10.57148125" y1="2.372359375" x2="10.754359375" y2="2.38251875" layer="21"/>
<rectangle x1="0.09651875" y1="2.38251875" x2="0.726440625" y2="2.39268125" layer="21"/>
<rectangle x1="0.980440625" y1="2.38251875" x2="1.2954" y2="2.39268125" layer="21"/>
<rectangle x1="1.82371875" y1="2.38251875" x2="2.17931875" y2="2.39268125" layer="21"/>
<rectangle x1="6.9342" y1="2.38251875" x2="7.381240625" y2="2.39268125" layer="21"/>
<rectangle x1="10.57148125" y1="2.38251875" x2="10.754359375" y2="2.39268125" layer="21"/>
<rectangle x1="0.10668125" y1="2.39268125" x2="0.75691875" y2="2.402840625" layer="21"/>
<rectangle x1="0.9398" y1="2.39268125" x2="1.305559375" y2="2.402840625" layer="21"/>
<rectangle x1="1.813559375" y1="2.39268125" x2="2.18948125" y2="2.402840625" layer="21"/>
<rectangle x1="6.924040625" y1="2.39268125" x2="7.381240625" y2="2.402840625" layer="21"/>
<rectangle x1="10.57148125" y1="2.39268125" x2="10.754359375" y2="2.402840625" layer="21"/>
<rectangle x1="0.10668125" y1="2.402840625" x2="0.828040625" y2="2.413" layer="21"/>
<rectangle x1="0.8382" y1="2.402840625" x2="0.848359375" y2="2.413" layer="21"/>
<rectangle x1="0.85851875" y1="2.402840625" x2="1.305559375" y2="2.413" layer="21"/>
<rectangle x1="1.8034" y1="2.402840625" x2="2.199640625" y2="2.413" layer="21"/>
<rectangle x1="6.9342" y1="2.402840625" x2="7.381240625" y2="2.413" layer="21"/>
<rectangle x1="10.57148125" y1="2.402840625" x2="10.754359375" y2="2.413" layer="21"/>
<rectangle x1="0.10668125" y1="2.413" x2="1.31571875" y2="2.423159375" layer="21"/>
<rectangle x1="1.793240625" y1="2.413" x2="2.2098" y2="2.423159375" layer="21"/>
<rectangle x1="6.924040625" y1="2.413" x2="7.37108125" y2="2.423159375" layer="21"/>
<rectangle x1="10.57148125" y1="2.413" x2="10.754359375" y2="2.423159375" layer="21"/>
<rectangle x1="0.116840625" y1="2.423159375" x2="1.32588125" y2="2.43331875" layer="21"/>
<rectangle x1="1.78308125" y1="2.423159375" x2="2.2098" y2="2.43331875" layer="21"/>
<rectangle x1="6.924040625" y1="2.423159375" x2="7.381240625" y2="2.43331875" layer="21"/>
<rectangle x1="10.57148125" y1="2.423159375" x2="10.754359375" y2="2.43331875" layer="21"/>
<rectangle x1="0.116840625" y1="2.43331875" x2="1.336040625" y2="2.44348125" layer="21"/>
<rectangle x1="1.78308125" y1="2.43331875" x2="2.219959375" y2="2.44348125" layer="21"/>
<rectangle x1="6.9342" y1="2.43331875" x2="7.381240625" y2="2.44348125" layer="21"/>
<rectangle x1="10.57148125" y1="2.43331875" x2="10.754359375" y2="2.44348125" layer="21"/>
<rectangle x1="0.127" y1="2.44348125" x2="1.3462" y2="2.453640625" layer="21"/>
<rectangle x1="1.77291875" y1="2.44348125" x2="2.23011875" y2="2.453640625" layer="21"/>
<rectangle x1="6.9342" y1="2.44348125" x2="7.381240625" y2="2.453640625" layer="21"/>
<rectangle x1="10.57148125" y1="2.44348125" x2="10.754359375" y2="2.453640625" layer="21"/>
<rectangle x1="0.127" y1="2.453640625" x2="1.356359375" y2="2.4638" layer="21"/>
<rectangle x1="1.77291875" y1="2.453640625" x2="2.23011875" y2="2.4638" layer="21"/>
<rectangle x1="6.924040625" y1="2.453640625" x2="7.37108125" y2="2.4638" layer="21"/>
<rectangle x1="10.57148125" y1="2.453640625" x2="10.754359375" y2="2.4638" layer="21"/>
<rectangle x1="12.90828125" y1="2.453640625" x2="12.9286" y2="2.4638" layer="21"/>
<rectangle x1="0.137159375" y1="2.4638" x2="1.36651875" y2="2.473959375" layer="21"/>
<rectangle x1="1.762759375" y1="2.4638" x2="2.24028125" y2="2.473959375" layer="21"/>
<rectangle x1="6.9342" y1="2.4638" x2="7.381240625" y2="2.473959375" layer="21"/>
<rectangle x1="10.57148125" y1="2.4638" x2="10.754359375" y2="2.473959375" layer="21"/>
<rectangle x1="12.867640625" y1="2.4638" x2="12.9794" y2="2.473959375" layer="21"/>
<rectangle x1="0.137159375" y1="2.473959375" x2="1.37668125" y2="2.48411875" layer="21"/>
<rectangle x1="1.762759375" y1="2.473959375" x2="2.24028125" y2="2.48411875" layer="21"/>
<rectangle x1="6.924040625" y1="2.473959375" x2="7.381240625" y2="2.48411875" layer="21"/>
<rectangle x1="10.57148125" y1="2.473959375" x2="10.754359375" y2="2.48411875" layer="21"/>
<rectangle x1="12.85748125" y1="2.473959375" x2="12.99971875" y2="2.48411875" layer="21"/>
<rectangle x1="0.14731875" y1="2.48411875" x2="1.386840625" y2="2.49428125" layer="21"/>
<rectangle x1="1.7526" y1="2.48411875" x2="2.250440625" y2="2.49428125" layer="21"/>
<rectangle x1="6.9342" y1="2.48411875" x2="7.37108125" y2="2.49428125" layer="21"/>
<rectangle x1="10.57148125" y1="2.48411875" x2="10.754359375" y2="2.49428125" layer="21"/>
<rectangle x1="12.837159375" y1="2.48411875" x2="13.00988125" y2="2.49428125" layer="21"/>
<rectangle x1="0.15748125" y1="2.49428125" x2="1.397" y2="2.504440625" layer="21"/>
<rectangle x1="1.7526" y1="2.49428125" x2="2.250440625" y2="2.504440625" layer="21"/>
<rectangle x1="6.924040625" y1="2.49428125" x2="7.381240625" y2="2.504440625" layer="21"/>
<rectangle x1="10.57148125" y1="2.49428125" x2="10.754359375" y2="2.504440625" layer="21"/>
<rectangle x1="12.827" y1="2.49428125" x2="13.020040625" y2="2.504440625" layer="21"/>
<rectangle x1="0.15748125" y1="2.504440625" x2="1.407159375" y2="2.5146" layer="21"/>
<rectangle x1="1.7526" y1="2.504440625" x2="2.250440625" y2="2.5146" layer="21"/>
<rectangle x1="6.9342" y1="2.504440625" x2="7.381240625" y2="2.5146" layer="21"/>
<rectangle x1="10.57148125" y1="2.504440625" x2="10.754359375" y2="2.5146" layer="21"/>
<rectangle x1="12.816840625" y1="2.504440625" x2="13.0302" y2="2.5146" layer="21"/>
<rectangle x1="0.167640625" y1="2.5146" x2="1.407159375" y2="2.524759375" layer="21"/>
<rectangle x1="1.7526" y1="2.5146" x2="2.2606" y2="2.524759375" layer="21"/>
<rectangle x1="6.924040625" y1="2.5146" x2="7.381240625" y2="2.524759375" layer="21"/>
<rectangle x1="10.57148125" y1="2.5146" x2="10.754359375" y2="2.524759375" layer="21"/>
<rectangle x1="12.80668125" y1="2.5146" x2="13.040359375" y2="2.524759375" layer="21"/>
<rectangle x1="0.1778" y1="2.524759375" x2="1.41731875" y2="2.53491875" layer="21"/>
<rectangle x1="1.742440625" y1="2.524759375" x2="2.2606" y2="2.53491875" layer="21"/>
<rectangle x1="6.9342" y1="2.524759375" x2="7.381240625" y2="2.53491875" layer="21"/>
<rectangle x1="10.57148125" y1="2.524759375" x2="10.754359375" y2="2.53491875" layer="21"/>
<rectangle x1="12.80668125" y1="2.524759375" x2="13.05051875" y2="2.53491875" layer="21"/>
<rectangle x1="0.1778" y1="2.53491875" x2="1.42748125" y2="2.54508125" layer="21"/>
<rectangle x1="1.742440625" y1="2.53491875" x2="2.2606" y2="2.54508125" layer="21"/>
<rectangle x1="6.924040625" y1="2.53491875" x2="7.37108125" y2="2.54508125" layer="21"/>
<rectangle x1="10.57148125" y1="2.53491875" x2="10.754359375" y2="2.54508125" layer="21"/>
<rectangle x1="12.79651875" y1="2.53491875" x2="13.05051875" y2="2.54508125" layer="21"/>
<rectangle x1="0.187959375" y1="2.54508125" x2="1.437640625" y2="2.555240625" layer="21"/>
<rectangle x1="1.742440625" y1="2.54508125" x2="2.2606" y2="2.555240625" layer="21"/>
<rectangle x1="6.9342" y1="2.54508125" x2="7.381240625" y2="2.555240625" layer="21"/>
<rectangle x1="10.57148125" y1="2.54508125" x2="10.754359375" y2="2.555240625" layer="21"/>
<rectangle x1="12.79651875" y1="2.54508125" x2="13.05051875" y2="2.555240625" layer="21"/>
<rectangle x1="0.19811875" y1="2.555240625" x2="1.4478" y2="2.5654" layer="21"/>
<rectangle x1="1.742440625" y1="2.555240625" x2="2.2606" y2="2.5654" layer="21"/>
<rectangle x1="6.924040625" y1="2.555240625" x2="7.381240625" y2="2.5654" layer="21"/>
<rectangle x1="10.57148125" y1="2.555240625" x2="10.754359375" y2="2.5654" layer="21"/>
<rectangle x1="12.786359375" y1="2.555240625" x2="13.06068125" y2="2.5654" layer="21"/>
<rectangle x1="0.20828125" y1="2.5654" x2="1.457959375" y2="2.575559375" layer="21"/>
<rectangle x1="1.742440625" y1="2.5654" x2="2.2606" y2="2.575559375" layer="21"/>
<rectangle x1="6.924040625" y1="2.5654" x2="7.37108125" y2="2.575559375" layer="21"/>
<rectangle x1="10.57148125" y1="2.5654" x2="10.754359375" y2="2.575559375" layer="21"/>
<rectangle x1="12.786359375" y1="2.5654" x2="13.06068125" y2="2.575559375" layer="21"/>
<rectangle x1="0.20828125" y1="2.575559375" x2="1.46811875" y2="2.58571875" layer="21"/>
<rectangle x1="1.742440625" y1="2.575559375" x2="2.2606" y2="2.58571875" layer="21"/>
<rectangle x1="6.9342" y1="2.575559375" x2="7.381240625" y2="2.58571875" layer="21"/>
<rectangle x1="10.57148125" y1="2.575559375" x2="10.754359375" y2="2.58571875" layer="21"/>
<rectangle x1="12.786359375" y1="2.575559375" x2="13.06068125" y2="2.58571875" layer="21"/>
<rectangle x1="0.218440625" y1="2.58571875" x2="1.47828125" y2="2.59588125" layer="21"/>
<rectangle x1="1.742440625" y1="2.58571875" x2="2.2606" y2="2.59588125" layer="21"/>
<rectangle x1="6.9342" y1="2.58571875" x2="7.381240625" y2="2.59588125" layer="21"/>
<rectangle x1="10.57148125" y1="2.58571875" x2="10.754359375" y2="2.59588125" layer="21"/>
<rectangle x1="12.786359375" y1="2.58571875" x2="13.06068125" y2="2.59588125" layer="21"/>
<rectangle x1="0.2286" y1="2.59588125" x2="1.488440625" y2="2.606040625" layer="21"/>
<rectangle x1="1.742440625" y1="2.59588125" x2="2.2606" y2="2.606040625" layer="21"/>
<rectangle x1="6.924040625" y1="2.59588125" x2="7.381240625" y2="2.606040625" layer="21"/>
<rectangle x1="10.57148125" y1="2.59588125" x2="10.754359375" y2="2.606040625" layer="21"/>
<rectangle x1="12.79651875" y1="2.59588125" x2="13.06068125" y2="2.606040625" layer="21"/>
<rectangle x1="0.238759375" y1="2.606040625" x2="1.4986" y2="2.6162" layer="21"/>
<rectangle x1="1.742440625" y1="2.606040625" x2="2.2606" y2="2.6162" layer="21"/>
<rectangle x1="6.9342" y1="2.606040625" x2="7.37108125" y2="2.6162" layer="21"/>
<rectangle x1="10.57148125" y1="2.606040625" x2="10.754359375" y2="2.6162" layer="21"/>
<rectangle x1="12.786359375" y1="2.606040625" x2="13.06068125" y2="2.6162" layer="21"/>
<rectangle x1="0.24891875" y1="2.6162" x2="1.4986" y2="2.626359375" layer="21"/>
<rectangle x1="1.742440625" y1="2.6162" x2="2.2606" y2="2.626359375" layer="21"/>
<rectangle x1="6.924040625" y1="2.6162" x2="7.381240625" y2="2.626359375" layer="21"/>
<rectangle x1="10.57148125" y1="2.6162" x2="10.754359375" y2="2.626359375" layer="21"/>
<rectangle x1="12.786359375" y1="2.6162" x2="13.06068125" y2="2.626359375" layer="21"/>
<rectangle x1="0.25908125" y1="2.626359375" x2="1.488440625" y2="2.63651875" layer="21"/>
<rectangle x1="1.742440625" y1="2.626359375" x2="2.2606" y2="2.63651875" layer="21"/>
<rectangle x1="6.924040625" y1="2.626359375" x2="7.37108125" y2="2.63651875" layer="21"/>
<rectangle x1="10.57148125" y1="2.626359375" x2="10.754359375" y2="2.63651875" layer="21"/>
<rectangle x1="12.79651875" y1="2.626359375" x2="13.06068125" y2="2.63651875" layer="21"/>
<rectangle x1="0.2794" y1="2.63651875" x2="1.46811875" y2="2.64668125" layer="21"/>
<rectangle x1="1.7526" y1="2.63651875" x2="2.250440625" y2="2.64668125" layer="21"/>
<rectangle x1="6.9342" y1="2.63651875" x2="7.381240625" y2="2.64668125" layer="21"/>
<rectangle x1="10.57148125" y1="2.63651875" x2="10.754359375" y2="2.64668125" layer="21"/>
<rectangle x1="12.79651875" y1="2.63651875" x2="13.05051875" y2="2.64668125" layer="21"/>
<rectangle x1="0.2794" y1="2.64668125" x2="1.457959375" y2="2.656840625" layer="21"/>
<rectangle x1="1.7526" y1="2.64668125" x2="2.250440625" y2="2.656840625" layer="21"/>
<rectangle x1="6.9342" y1="2.64668125" x2="7.381240625" y2="2.656840625" layer="21"/>
<rectangle x1="10.57148125" y1="2.64668125" x2="10.754359375" y2="2.656840625" layer="21"/>
<rectangle x1="12.79651875" y1="2.64668125" x2="13.05051875" y2="2.656840625" layer="21"/>
<rectangle x1="0.29971875" y1="2.656840625" x2="1.4478" y2="2.667" layer="21"/>
<rectangle x1="1.7526" y1="2.656840625" x2="2.250440625" y2="2.667" layer="21"/>
<rectangle x1="6.924040625" y1="2.656840625" x2="7.381240625" y2="2.667" layer="21"/>
<rectangle x1="10.57148125" y1="2.656840625" x2="10.754359375" y2="2.667" layer="21"/>
<rectangle x1="12.80668125" y1="2.656840625" x2="13.05051875" y2="2.667" layer="21"/>
<rectangle x1="0.30988125" y1="2.667" x2="1.42748125" y2="2.677159375" layer="21"/>
<rectangle x1="1.7526" y1="2.667" x2="2.24028125" y2="2.677159375" layer="21"/>
<rectangle x1="6.9342" y1="2.667" x2="7.37108125" y2="2.677159375" layer="21"/>
<rectangle x1="10.57148125" y1="2.667" x2="10.754359375" y2="2.677159375" layer="21"/>
<rectangle x1="12.80668125" y1="2.667" x2="13.040359375" y2="2.677159375" layer="21"/>
<rectangle x1="0.320040625" y1="2.677159375" x2="1.41731875" y2="2.68731875" layer="21"/>
<rectangle x1="1.762759375" y1="2.677159375" x2="2.24028125" y2="2.68731875" layer="21"/>
<rectangle x1="6.924040625" y1="2.677159375" x2="7.381240625" y2="2.68731875" layer="21"/>
<rectangle x1="10.57148125" y1="2.677159375" x2="10.754359375" y2="2.68731875" layer="21"/>
<rectangle x1="12.816840625" y1="2.677159375" x2="13.0302" y2="2.68731875" layer="21"/>
<rectangle x1="0.340359375" y1="2.68731875" x2="1.407159375" y2="2.69748125" layer="21"/>
<rectangle x1="1.762759375" y1="2.68731875" x2="2.24028125" y2="2.69748125" layer="21"/>
<rectangle x1="6.9342" y1="2.68731875" x2="7.37108125" y2="2.69748125" layer="21"/>
<rectangle x1="10.57148125" y1="2.68731875" x2="10.754359375" y2="2.69748125" layer="21"/>
<rectangle x1="12.827" y1="2.68731875" x2="13.020040625" y2="2.69748125" layer="21"/>
<rectangle x1="0.35051875" y1="2.69748125" x2="1.386840625" y2="2.707640625" layer="21"/>
<rectangle x1="1.77291875" y1="2.69748125" x2="2.23011875" y2="2.707640625" layer="21"/>
<rectangle x1="6.924040625" y1="2.69748125" x2="7.381240625" y2="2.707640625" layer="21"/>
<rectangle x1="10.57148125" y1="2.69748125" x2="10.754359375" y2="2.707640625" layer="21"/>
<rectangle x1="12.837159375" y1="2.69748125" x2="13.00988125" y2="2.707640625" layer="21"/>
<rectangle x1="0.370840625" y1="2.707640625" x2="1.36651875" y2="2.7178" layer="21"/>
<rectangle x1="1.78308125" y1="2.707640625" x2="2.219959375" y2="2.7178" layer="21"/>
<rectangle x1="6.9342" y1="2.707640625" x2="7.381240625" y2="2.7178" layer="21"/>
<rectangle x1="10.57148125" y1="2.707640625" x2="10.754359375" y2="2.7178" layer="21"/>
<rectangle x1="12.84731875" y1="2.707640625" x2="12.99971875" y2="2.7178" layer="21"/>
<rectangle x1="0.391159375" y1="2.7178" x2="1.356359375" y2="2.727959375" layer="21"/>
<rectangle x1="1.78308125" y1="2.7178" x2="2.219959375" y2="2.727959375" layer="21"/>
<rectangle x1="6.924040625" y1="2.7178" x2="7.381240625" y2="2.727959375" layer="21"/>
<rectangle x1="10.57148125" y1="2.7178" x2="10.754359375" y2="2.727959375" layer="21"/>
<rectangle x1="12.867640625" y1="2.7178" x2="12.9794" y2="2.727959375" layer="21"/>
<rectangle x1="0.40131875" y1="2.727959375" x2="1.336040625" y2="2.73811875" layer="21"/>
<rectangle x1="1.793240625" y1="2.727959375" x2="2.2098" y2="2.73811875" layer="21"/>
<rectangle x1="6.9342" y1="2.727959375" x2="7.37108125" y2="2.73811875" layer="21"/>
<rectangle x1="10.57148125" y1="2.727959375" x2="10.754359375" y2="2.73811875" layer="21"/>
<rectangle x1="12.90828125" y1="2.727959375" x2="12.9286" y2="2.73811875" layer="21"/>
<rectangle x1="12.938759375" y1="2.727959375" x2="12.94891875" y2="2.73811875" layer="21"/>
<rectangle x1="0.421640625" y1="2.73811875" x2="1.31571875" y2="2.74828125" layer="21"/>
<rectangle x1="1.8034" y1="2.73811875" x2="2.199640625" y2="2.74828125" layer="21"/>
<rectangle x1="6.924040625" y1="2.73811875" x2="7.381240625" y2="2.74828125" layer="21"/>
<rectangle x1="10.57148125" y1="2.73811875" x2="10.754359375" y2="2.74828125" layer="21"/>
<rectangle x1="0.441959375" y1="2.74828125" x2="1.2954" y2="2.758440625" layer="21"/>
<rectangle x1="1.8034" y1="2.74828125" x2="2.199640625" y2="2.758440625" layer="21"/>
<rectangle x1="6.9342" y1="2.74828125" x2="7.37108125" y2="2.758440625" layer="21"/>
<rectangle x1="10.57148125" y1="2.74828125" x2="10.754359375" y2="2.758440625" layer="21"/>
<rectangle x1="0.472440625" y1="2.758440625" x2="1.27508125" y2="2.7686" layer="21"/>
<rectangle x1="1.813559375" y1="2.758440625" x2="2.18948125" y2="2.7686" layer="21"/>
<rectangle x1="6.924040625" y1="2.758440625" x2="7.381240625" y2="2.7686" layer="21"/>
<rectangle x1="10.57148125" y1="2.758440625" x2="10.754359375" y2="2.7686" layer="21"/>
<rectangle x1="0.492759375" y1="2.7686" x2="1.2446" y2="2.778759375" layer="21"/>
<rectangle x1="1.82371875" y1="2.7686" x2="2.169159375" y2="2.778759375" layer="21"/>
<rectangle x1="6.924040625" y1="2.7686" x2="7.37108125" y2="2.778759375" layer="21"/>
<rectangle x1="10.57148125" y1="2.7686" x2="10.754359375" y2="2.778759375" layer="21"/>
<rectangle x1="0.51308125" y1="2.778759375" x2="1.21411875" y2="2.78891875" layer="21"/>
<rectangle x1="1.844040625" y1="2.778759375" x2="2.159" y2="2.78891875" layer="21"/>
<rectangle x1="6.9342" y1="2.778759375" x2="7.381240625" y2="2.78891875" layer="21"/>
<rectangle x1="10.57148125" y1="2.778759375" x2="10.754359375" y2="2.78891875" layer="21"/>
<rectangle x1="0.55371875" y1="2.78891875" x2="1.183640625" y2="2.79908125" layer="21"/>
<rectangle x1="1.8542" y1="2.78891875" x2="2.148840625" y2="2.79908125" layer="21"/>
<rectangle x1="6.9342" y1="2.78891875" x2="7.37108125" y2="2.79908125" layer="21"/>
<rectangle x1="10.57148125" y1="2.78891875" x2="10.754359375" y2="2.79908125" layer="21"/>
<rectangle x1="0.594359375" y1="2.79908125" x2="1.143" y2="2.809240625" layer="21"/>
<rectangle x1="1.87451875" y1="2.79908125" x2="2.12851875" y2="2.809240625" layer="21"/>
<rectangle x1="6.924040625" y1="2.79908125" x2="7.381240625" y2="2.809240625" layer="21"/>
<rectangle x1="10.57148125" y1="2.79908125" x2="10.754359375" y2="2.809240625" layer="21"/>
<rectangle x1="0.635" y1="2.809240625" x2="1.102359375" y2="2.8194" layer="21"/>
<rectangle x1="1.88468125" y1="2.809240625" x2="2.118359375" y2="2.8194" layer="21"/>
<rectangle x1="6.9342" y1="2.809240625" x2="7.381240625" y2="2.8194" layer="21"/>
<rectangle x1="10.57148125" y1="2.809240625" x2="10.754359375" y2="2.8194" layer="21"/>
<rectangle x1="0.6858" y1="2.8194" x2="1.0414" y2="2.829559375" layer="21"/>
<rectangle x1="1.915159375" y1="2.8194" x2="2.08788125" y2="2.829559375" layer="21"/>
<rectangle x1="6.924040625" y1="2.8194" x2="7.381240625" y2="2.829559375" layer="21"/>
<rectangle x1="10.57148125" y1="2.8194" x2="10.754359375" y2="2.829559375" layer="21"/>
<rectangle x1="0.746759375" y1="2.829559375" x2="0.75691875" y2="2.83971875" layer="21"/>
<rectangle x1="0.76708125" y1="2.829559375" x2="0.777240625" y2="2.83971875" layer="21"/>
<rectangle x1="0.7874" y1="2.829559375" x2="0.96011875" y2="2.83971875" layer="21"/>
<rectangle x1="1.945640625" y1="2.829559375" x2="1.9558" y2="2.83971875" layer="21"/>
<rectangle x1="1.965959375" y1="2.829559375" x2="2.047240625" y2="2.83971875" layer="21"/>
<rectangle x1="6.924040625" y1="2.829559375" x2="7.37108125" y2="2.83971875" layer="21"/>
<rectangle x1="10.57148125" y1="2.829559375" x2="10.754359375" y2="2.83971875" layer="21"/>
<rectangle x1="6.9342" y1="2.83971875" x2="7.381240625" y2="2.84988125" layer="21"/>
<rectangle x1="10.57148125" y1="2.83971875" x2="10.754359375" y2="2.84988125" layer="21"/>
<rectangle x1="6.9342" y1="2.84988125" x2="7.37108125" y2="2.860040625" layer="21"/>
<rectangle x1="10.57148125" y1="2.84988125" x2="10.754359375" y2="2.860040625" layer="21"/>
<rectangle x1="6.924040625" y1="2.860040625" x2="7.381240625" y2="2.8702" layer="21"/>
<rectangle x1="10.57148125" y1="2.860040625" x2="10.754359375" y2="2.8702" layer="21"/>
<rectangle x1="6.9342" y1="2.8702" x2="7.381240625" y2="2.880359375" layer="21"/>
<rectangle x1="10.57148125" y1="2.8702" x2="10.754359375" y2="2.880359375" layer="21"/>
<rectangle x1="6.924040625" y1="2.880359375" x2="7.381240625" y2="2.89051875" layer="21"/>
<rectangle x1="10.57148125" y1="2.880359375" x2="10.754359375" y2="2.89051875" layer="21"/>
<rectangle x1="6.9342" y1="2.89051875" x2="7.37108125" y2="2.90068125" layer="21"/>
<rectangle x1="10.57148125" y1="2.89051875" x2="10.754359375" y2="2.90068125" layer="21"/>
<rectangle x1="6.924040625" y1="2.90068125" x2="7.381240625" y2="2.910840625" layer="21"/>
<rectangle x1="10.57148125" y1="2.90068125" x2="10.754359375" y2="2.910840625" layer="21"/>
<rectangle x1="6.924040625" y1="2.910840625" x2="7.381240625" y2="2.921" layer="21"/>
<rectangle x1="10.57148125" y1="2.910840625" x2="10.754359375" y2="2.921" layer="21"/>
<rectangle x1="6.944359375" y1="2.921" x2="6.95451875" y2="2.931159375" layer="21"/>
<rectangle x1="6.96468125" y1="2.921" x2="6.995159375" y2="2.931159375" layer="21"/>
<rectangle x1="7.00531875" y1="2.921" x2="7.01548125" y2="2.931159375" layer="21"/>
<rectangle x1="7.025640625" y1="2.921" x2="7.0358" y2="2.931159375" layer="21"/>
<rectangle x1="7.045959375" y1="2.921" x2="7.05611875" y2="2.931159375" layer="21"/>
<rectangle x1="7.06628125" y1="2.921" x2="7.076440625" y2="2.931159375" layer="21"/>
<rectangle x1="7.0866" y1="2.921" x2="7.096759375" y2="2.931159375" layer="21"/>
<rectangle x1="7.10691875" y1="2.921" x2="7.11708125" y2="2.931159375" layer="21"/>
<rectangle x1="7.127240625" y1="2.921" x2="7.1374" y2="2.931159375" layer="21"/>
<rectangle x1="7.147559375" y1="2.921" x2="7.15771875" y2="2.931159375" layer="21"/>
<rectangle x1="7.16788125" y1="2.921" x2="7.178040625" y2="2.931159375" layer="21"/>
<rectangle x1="7.1882" y1="2.921" x2="7.198359375" y2="2.931159375" layer="21"/>
<rectangle x1="7.20851875" y1="2.921" x2="7.21868125" y2="2.931159375" layer="21"/>
<rectangle x1="7.228840625" y1="2.921" x2="7.239" y2="2.931159375" layer="21"/>
<rectangle x1="7.249159375" y1="2.921" x2="7.25931875" y2="2.931159375" layer="21"/>
<rectangle x1="7.26948125" y1="2.921" x2="7.279640625" y2="2.931159375" layer="21"/>
<rectangle x1="7.2898" y1="2.921" x2="7.299959375" y2="2.931159375" layer="21"/>
<rectangle x1="7.31011875" y1="2.921" x2="7.32028125" y2="2.931159375" layer="21"/>
<rectangle x1="7.330440625" y1="2.921" x2="7.3406" y2="2.931159375" layer="21"/>
<rectangle x1="7.350759375" y1="2.921" x2="7.36091875" y2="2.931159375" layer="21"/>
<rectangle x1="10.581640625" y1="2.921" x2="10.5918" y2="2.931159375" layer="21"/>
<rectangle x1="10.601959375" y1="2.921" x2="10.61211875" y2="2.931159375" layer="21"/>
<rectangle x1="10.62228125" y1="2.921" x2="10.632440625" y2="2.931159375" layer="21"/>
<rectangle x1="10.6426" y1="2.921" x2="10.652759375" y2="2.931159375" layer="21"/>
<rectangle x1="10.66291875" y1="2.921" x2="10.67308125" y2="2.931159375" layer="21"/>
<rectangle x1="10.683240625" y1="2.921" x2="10.6934" y2="2.931159375" layer="21"/>
<rectangle x1="10.703559375" y1="2.921" x2="10.71371875" y2="2.931159375" layer="21"/>
<rectangle x1="10.72388125" y1="2.921" x2="10.734040625" y2="2.931159375" layer="21"/>
<rectangle x1="10.7442" y1="2.921" x2="10.754359375" y2="2.931159375" layer="21"/>
</package>
<package name="USB-MINIB">
<description>&lt;b&gt;USB Series Mini-B Surface Mounted&lt;/b&gt;

MODIFIED FOOTPRINT</description>
<wire x1="-1.3" y1="3.8" x2="0.8" y2="3.8" width="0.2032" layer="21"/>
<wire x1="3.3" y1="3.1" x2="3.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.2" x2="3.3" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="-3.8" x2="-1.3" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-5.9" y1="3.8" x2="-5.9" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="-3.8" x2="-4.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="3.8" x2="-4.5" y2="3.8" width="0.2032" layer="51"/>
<smd name="D+" x="2.5" y="0" dx="2.5" dy="0.5" layer="1"/>
<smd name="D-" x="2.5" y="0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="GND" x="2.5" y="-1.6" dx="2.5" dy="0.5" layer="1"/>
<smd name="ID" x="2.5" y="-0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="MTN3" x="-3" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="MTN1" x="-3" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="MTN4" x="2.5" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="MTN2" x="2.5" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="VBUS" x="2.5" y="1.6" dx="2.5" dy="0.5" layer="1"/>
<text x="-3.81" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="0" size="0.4064" layer="27">&gt;VALUE</text>
<hole x="0" y="1.75" drill="0.9"/>
<hole x="0" y="-1.75" drill="0.9"/>
</package>
<package name="USB-A-H">
<description>&lt;b&gt;USB Series A Hole Mounted&lt;/b&gt;</description>
<wire x1="-17.8" y1="6" x2="-17.8" y2="-6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="-3" y2="-6" width="0.2032" layer="21"/>
<wire x1="-3" y1="6" x2="-17.8" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="-6" x2="-17.8" y2="-6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="-2" y2="6" width="0.2032" layer="21"/>
<wire x1="-3" y1="-6" x2="-2" y2="-6" width="0.2032" layer="21"/>
<wire x1="1" y1="-4" x2="1" y2="4" width="0.2032" layer="21"/>
<wire x1="-13.5" y1="4.3" x2="-13.5" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-13.5" y1="1.9" x2="-11.2" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-11.2" y1="1.9" x2="-11.2" y2="4.3" width="0.2032" layer="51"/>
<wire x1="-11.2" y1="4.3" x2="-13.5" y2="4.3" width="0.2032" layer="51"/>
<wire x1="-13.5" y1="-1.9" x2="-13.5" y2="-4.3" width="0.2032" layer="51"/>
<wire x1="-13.5" y1="-4.3" x2="-11.2" y2="-4.3" width="0.2032" layer="51"/>
<wire x1="-11.2" y1="-4.3" x2="-11.2" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-11.2" y1="-1.9" x2="-13.5" y2="-1.9" width="0.2032" layer="51"/>
<pad name="GND" x="2.4" y="3.5" drill="0.9144" diameter="1.8796" rot="R270"/>
<pad name="D+" x="2.4" y="1.127" drill="0.9144" diameter="1.8796" rot="R270"/>
<pad name="D-" x="2.4" y="-1.127" drill="0.9144" diameter="1.8796" rot="R270"/>
<pad name="VBUS" x="2.4" y="-3.5" drill="0.9144" diameter="1.8796" rot="R270"/>
<pad name="GND2" x="0" y="-5.8" drill="2.2" rot="R270"/>
<pad name="GND3" x="0" y="5.8" drill="2.2" rot="R270"/>
<text x="5.85" y="-2.7" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.9" y="-4.4" size="1.27" layer="51" rot="R90">PCB Edge</text>
<hole x="-0.1" y="2.25" drill="1.1"/>
<hole x="-0.1" y="-2.25" drill="1.1"/>
</package>
<package name="USB-A-S">
<description>&lt;b&gt;USB Series A Surface Mounted&lt;/b&gt;</description>
<wire x1="3.6957" y1="6.5659" x2="-10.287" y2="6.5659" width="0.127" layer="21"/>
<wire x1="3.6957" y1="-6.5659" x2="-10.287" y2="-6.5659" width="0.127" layer="21"/>
<wire x1="-10.287" y1="6.477" x2="-10.287" y2="-6.477" width="0.127" layer="21"/>
<wire x1="3.7084" y1="6.5024" x2="3.7084" y2="-6.5024" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="-8.89" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="1.27" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.27" x2="-2.54" y2="0.635" width="0.127" layer="21"/>
<pad name="P$5" x="0" y="-6.5659" drill="2.3114" rot="R270"/>
<pad name="P$6" x="0" y="6.5659" drill="2.3114" rot="R270"/>
<smd name="D-" x="3.45" y="1" dx="3" dy="0.9" layer="1"/>
<smd name="VBUS" x="3.45" y="3" dx="3" dy="0.9" layer="1"/>
<smd name="D+" x="3.45" y="-1" dx="3" dy="0.9" layer="1"/>
<smd name="GND" x="3.45" y="-3" dx="3" dy="0.9" layer="1"/>
<text x="5.715" y="3.81" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="USB-MB-H">
<description>&lt;b&gt;USB Series Mini-B Hole Mounted&lt;/b&gt;</description>
<wire x1="-3.75" y1="3.9" x2="-3.75" y2="-3.9" width="0.127" layer="22"/>
<wire x1="5.25" y1="3.9" x2="5.25" y2="-3.9" width="0.127" layer="22"/>
<wire x1="-3.75" y1="3.9" x2="5.25" y2="3.9" width="0.127" layer="22"/>
<wire x1="-3.75" y1="-3.9" x2="5.25" y2="-3.9" width="0.127" layer="22"/>
<wire x1="0.75" y1="3.5" x2="-3.25" y2="3" width="0.127" layer="22"/>
<wire x1="-3.25" y1="3" x2="-3.25" y2="2" width="0.127" layer="22"/>
<wire x1="-3.25" y1="2" x2="0.75" y2="1.5" width="0.127" layer="22"/>
<wire x1="1.25" y1="-3.5" x2="-3.25" y2="-3" width="0.127" layer="22"/>
<wire x1="-3.25" y1="-3" x2="-3.25" y2="-2" width="0.127" layer="22"/>
<wire x1="-3.25" y1="-2" x2="1.25" y2="-1.5" width="0.127" layer="22"/>
<wire x1="-3.25" y1="1.25" x2="1.75" y2="0.75" width="0.127" layer="22"/>
<wire x1="1.75" y1="0.75" x2="1.75" y2="-0.75" width="0.127" layer="22"/>
<wire x1="1.75" y1="-0.75" x2="-3.25" y2="-1.25" width="0.127" layer="22"/>
<pad name="VBUS" x="5.1" y="1.6" drill="0.8"/>
<pad name="D+" x="5.1" y="0" drill="0.8"/>
<pad name="GND" x="5.1" y="-1.6" drill="0.8"/>
<pad name="D-" x="3.9" y="0.8" drill="0.8"/>
<pad name="ID" x="3.9" y="-0.8" drill="0.8"/>
<pad name="P$6" x="0" y="-3.65" drill="1.9"/>
<pad name="P$7" x="0" y="3.65" drill="1.9"/>
<text x="7.25" y="1.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="3.25" y1="3" x2="5.75" y2="4.4" layer="43"/>
<rectangle x1="3.25" y1="-4.4" x2="5.75" y2="-3" layer="43"/>
<rectangle x1="-3.75" y1="-3.1" x2="-1.425" y2="3.1" layer="43"/>
<rectangle x1="-1.425" y1="-2.325" x2="-0.65" y2="2.325" layer="43"/>
</package>
<package name="USB-B-SMT">
<description>USB Series B Surface Mounted</description>
<wire x1="-1" y1="-6" x2="2.4" y2="-6" width="0.2032" layer="51"/>
<wire x1="2.4" y1="6" x2="-1" y2="6" width="0.2032" layer="51"/>
<wire x1="2.4" y1="6" x2="2.4" y2="7.3" width="0.2032" layer="51"/>
<wire x1="2.4" y1="7.3" x2="2.2" y2="7.5" width="0.2032" layer="51"/>
<wire x1="2.2" y1="7.5" x2="1.9" y2="7.5" width="0.2032" layer="51"/>
<wire x1="1.9" y1="7.5" x2="1.4" y2="7" width="0.2032" layer="51"/>
<wire x1="-1" y1="6" x2="-1" y2="7.3" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="7.5" x2="-0.5" y2="7.5" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="7.5" x2="0" y2="7" width="0.2032" layer="51"/>
<wire x1="0" y1="7" x2="1.4" y2="7" width="0.2032" layer="51"/>
<wire x1="-1" y1="-6" x2="-1" y2="-7.3" width="0.2032" layer="51"/>
<wire x1="-1" y1="-7.3" x2="-0.8" y2="-7.5" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-7.5" x2="-0.5" y2="-7.5" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="-7.5" x2="0" y2="-7" width="0.2032" layer="51"/>
<wire x1="1.9" y1="-7.5" x2="1.4" y2="-7" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-7" x2="0" y2="-7" width="0.2032" layer="51"/>
<wire x1="-1" y1="7.3" x2="-0.8" y2="7.5" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-7.5" x2="1.9" y2="-7.5" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-7.5" x2="2.4" y2="-7.3" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-6" x2="2.4" y2="-7.3" width="0.2032" layer="51"/>
<wire x1="-5" y1="6" x2="-5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5" y1="6" x2="-9" y2="6" width="0.2032" layer="51"/>
<wire x1="-9" y1="6" x2="-9" y2="-6" width="0.2032" layer="51"/>
<wire x1="-9" y1="-6" x2="-5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5" y1="6" x2="-3" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="-6" x2="-3" y2="-6" width="0.2032" layer="21"/>
<wire x1="4" y1="-6" x2="7" y2="-6" width="0.2032" layer="21"/>
<wire x1="7" y1="-6" x2="7" y2="-3" width="0.2032" layer="21"/>
<wire x1="7" y1="3" x2="7" y2="6" width="0.2032" layer="21"/>
<wire x1="7" y1="6" x2="4" y2="6" width="0.2032" layer="21"/>
<smd name="5" x="0.58" y="6.8" dx="6.04" dy="3.4" layer="1"/>
<smd name="6" x="0.58" y="-6.8" dx="6.04" dy="3.4" layer="1"/>
<smd name="D+" x="7" y="1.875" dx="3" dy="0.7" layer="1"/>
<smd name="D-" x="7" y="0.625" dx="3" dy="0.7" layer="1"/>
<smd name="GND" x="7" y="-0.625" dx="3" dy="0.7" layer="1"/>
<smd name="VUSB" x="7" y="-1.875" dx="3" dy="0.7" layer="1"/>
<text x="4.3" y="-7.795" size="1.27" layer="25">&gt;NAME</text>
<hole x="0" y="2.25" drill="1.4"/>
<hole x="0" y="-2.25" drill="1.4"/>
</package>
<package name="USB-MINIB-OLD">
<description>&lt;b&gt;USB Series Mini-B Surface Mounted&lt;/b&gt;</description>
<wire x1="-1.5" y1="3.8" x2="0.9" y2="3.8" width="0.127" layer="21"/>
<wire x1="3.3" y1="3.1" x2="3.3" y2="2.1" width="0.127" layer="21"/>
<wire x1="3.3" y1="-2.1" x2="3.3" y2="-3.1" width="0.127" layer="21"/>
<wire x1="1" y1="-3.8" x2="-1.5" y2="-3.8" width="0.127" layer="21"/>
<wire x1="-5.9" y1="3.8" x2="-5.9" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-5.9" y1="-3.8" x2="-4.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-5.9" y1="3.8" x2="-4.5" y2="3.8" width="0.127" layer="51"/>
<smd name="1" x="-3" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="2" x="-3" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="3" x="3" y="-4.5" dx="3.5" dy="2" layer="1"/>
<smd name="4" x="3" y="4.5" dx="3.5" dy="2" layer="1"/>
<smd name="D+" x="3" y="0" dx="3.5" dy="0.5" layer="1"/>
<smd name="D-" x="3" y="0.8" dx="3.5" dy="0.5" layer="1"/>
<smd name="VBUS" x="3.01" y="1.61" dx="3.5" dy="0.5" layer="1"/>
<smd name="ID" x="3" y="-0.8" dx="3.5" dy="0.5" layer="1"/>
<smd name="GND" x="3" y="-1.6" dx="3.5" dy="0.5" layer="1"/>
<text x="-3.81" y="-1.27" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-3.81" y="0" size="0.4064" layer="25">&gt;NAME</text>
<hole x="0" y="2.2" drill="0.9"/>
<hole x="0" y="-2.2" drill="0.9"/>
</package>
<package name="USB-B-PTH">
<description>&lt;b&gt;USB Series B Hole Mounted&lt;/b&gt;</description>
<wire x1="-12.5" y1="6" x2="-8.6" y2="6" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="6" x2="-8.6" y2="-6" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="-6" x2="-12.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-12.5" y1="-6" x2="-12.5" y2="6" width="0.2032" layer="51"/>
<wire x1="-8.6" y1="6" x2="-4.8" y2="6" width="0.2032" layer="21"/>
<wire x1="-8.6" y1="-6" x2="-4.8" y2="-6" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-6" x2="3.3" y2="-6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-6" x2="3.3" y2="6" width="0.2032" layer="21"/>
<wire x1="3.3" y1="6" x2="-0.6" y2="6" width="0.2032" layer="21"/>
<pad name="VBUS" x="1.9812" y="-1.25" drill="0.9144" diameter="1.6764" rot="R90"/>
<pad name="D-" x="1.9812" y="1.25" drill="0.9144" diameter="1.6764" rot="R90"/>
<pad name="D+" x="0" y="1.25" drill="0.9144" diameter="1.6764" rot="R270"/>
<pad name="GND" x="0" y="-1.25" drill="0.9144" diameter="1.6764" rot="R270"/>
<pad name="S1" x="-2.7178" y="-6.0198" drill="2.286"/>
<pad name="S2" x="-2.7178" y="6.0198" drill="2.286"/>
<text x="-1.27" y="3.81" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="2.54" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="S4B-PH">
<wire x1="-6" y1="2" x2="-6" y2="-7" width="0.3048" layer="51"/>
<wire x1="-6" y1="-7" x2="6" y2="-7" width="0.3048" layer="51"/>
<wire x1="6" y1="-7" x2="6" y2="2" width="0.3048" layer="51"/>
<wire x1="6" y1="2" x2="-6" y2="2" width="0.3048" layer="51"/>
<smd name="1" x="-3" y="-4.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="-1" y="-4.7" dx="1" dy="4.6" layer="1"/>
<smd name="3" x="1" y="-4.7" dx="1" dy="4.6" layer="1"/>
<smd name="4" x="3" y="-4.7" dx="1" dy="4.6" layer="1"/>
<smd name="P$1" x="-5.4" y="0.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="5.4" y="0.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
</package>
<package name="USB-A-PCB">
<description>Card-edge USB A connector.

For boards designed to be plugged directly into a USB slot. If possible, ensure that your PCB is about 2.4mm thick to fit snugly.</description>
<wire x1="-5" y1="6" x2="3.7" y2="6" width="0.127" layer="51"/>
<wire x1="3.7" y1="6" x2="3.7" y2="-6" width="0.127" layer="51" style="shortdash"/>
<wire x1="3.7" y1="-6" x2="-5" y2="-6" width="0.127" layer="51"/>
<wire x1="-5" y1="-6" x2="-5" y2="6" width="0.127" layer="51"/>
<smd name="5V" x="-0.2" y="-3.5" dx="7.5" dy="1.5" layer="1"/>
<smd name="USB_M" x="0.3" y="-1" dx="6.5" dy="1" layer="1"/>
<smd name="USB_P" x="0.3" y="1" dx="6.5" dy="1" layer="1"/>
<smd name="GND" x="-0.2" y="3.5" dx="7.5" dy="1.5" layer="1"/>
<text x="-1.27" y="5.08" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-5.08" size="0.4064" layer="27">&gt;Value</text>
<text x="-6.35" y="-3.81" size="1.016" layer="48" rot="R90">Card edge</text>
</package>
<package name="USB-B-PTH-VERTICAL">
<description>&lt;b&gt;USB Series B Hole Mounted&lt;/b&gt;</description>
<wire x1="0" y1="0" x2="11.938" y2="0" width="0.254" layer="21"/>
<wire x1="11.938" y1="0" x2="11.938" y2="11.303" width="0.254" layer="21"/>
<wire x1="11.938" y1="11.303" x2="0" y2="11.303" width="0.254" layer="21"/>
<wire x1="0" y1="11.303" x2="0" y2="0" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.27" x2="10.795" y2="1.27" width="0.254" layer="51"/>
<wire x1="10.795" y1="1.27" x2="10.795" y2="8.255" width="0.254" layer="51"/>
<wire x1="10.795" y1="8.255" x2="8.89" y2="10.16" width="0.254" layer="51"/>
<wire x1="8.89" y1="10.16" x2="3.175" y2="10.16" width="0.254" layer="51"/>
<wire x1="3.175" y1="10.16" x2="1.27" y2="8.255" width="0.254" layer="51"/>
<wire x1="1.27" y1="8.255" x2="1.27" y2="1.27" width="0.254" layer="51"/>
<pad name="GND" x="7.3152" y="4.3942" drill="0.9144" diameter="1.6764" rot="R90"/>
<pad name="VBUS" x="7.3152" y="7.5946" drill="0.9144" diameter="1.6764" rot="R90"/>
<pad name="D-" x="4.826" y="7.5946" drill="0.9144" diameter="1.6764" rot="R270"/>
<pad name="D+" x="4.826" y="4.3942" drill="0.9144" diameter="1.6764" rot="R270"/>
<pad name="P$1" x="0" y="4.9022" drill="2.286"/>
<pad name="P$2" x="12.0396" y="4.9022" drill="2.286"/>
<text x="8.89" y="-1.27" size="0.8128" layer="25">&gt;NAME</text>
<text x="0" y="-1.27" size="0.8128" layer="27">&gt;VALUE</text>
</package>
<package name="USB-A-S-NOSILK">
<wire x1="3.6957" y1="6.5659" x2="-10.287" y2="6.5659" width="0.127" layer="51"/>
<wire x1="3.6957" y1="-6.5659" x2="-10.287" y2="-6.5659" width="0.127" layer="51"/>
<wire x1="-10.287" y1="6.477" x2="-10.287" y2="-6.477" width="0.127" layer="51"/>
<wire x1="3.7084" y1="6.5024" x2="3.7084" y2="-6.5024" width="0.127" layer="51"/>
<wire x1="-2.54" y1="-5.08" x2="-8.89" y2="-4.445" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-2.54" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="51"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="1.27" width="0.127" layer="51"/>
<wire x1="-8.89" y1="1.27" x2="-2.54" y2="0.635" width="0.127" layer="51"/>
<pad name="P$5" x="0" y="-6.5659" drill="2.3114" rot="R270"/>
<pad name="P$6" x="0" y="6.5659" drill="2.3114" rot="R270"/>
<smd name="D-" x="3.45" y="1" dx="3" dy="0.9" layer="1"/>
<smd name="VBUS" x="3.45" y="3" dx="3" dy="0.9" layer="1"/>
<smd name="D+" x="3.45" y="-1" dx="3" dy="0.9" layer="1"/>
<smd name="GND" x="3.45" y="-3" dx="3" dy="0.9" layer="1"/>
<text x="5.715" y="3.81" size="1.27" layer="51" rot="R90">&gt;NAME</text>
</package>
<package name="USB-A-S-NOSILK-FEMALE">
<wire x1="3.6957" y1="6.5659" x2="-10.287" y2="6.5659" width="0.127" layer="51"/>
<wire x1="3.6957" y1="-6.5659" x2="-10.287" y2="-6.5659" width="0.127" layer="51"/>
<wire x1="-10.287" y1="6.477" x2="-10.287" y2="-6.477" width="0.127" layer="51"/>
<wire x1="3.7084" y1="6.5024" x2="3.7084" y2="-6.5024" width="0.127" layer="51"/>
<wire x1="-2.54" y1="-5.08" x2="-8.89" y2="-4.445" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-4.445" x2="-8.89" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-8.89" y1="-1.27" x2="-2.54" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-2.54" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="51"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="1.27" width="0.127" layer="51"/>
<wire x1="-8.89" y1="1.27" x2="-2.54" y2="0.635" width="0.127" layer="51"/>
<pad name="S1" x="0" y="-6.5659" drill="2.3114" rot="R270"/>
<pad name="S2" x="0" y="6.5659" drill="2.3114" rot="R270"/>
<smd name="D-" x="4.212" y="1" dx="3" dy="0.9" layer="1"/>
<smd name="VBUS" x="4.212" y="3.5" dx="3" dy="0.9" layer="1"/>
<smd name="D+" x="4.212" y="-1" dx="3" dy="0.9" layer="1"/>
<smd name="GND" x="4.212" y="-3.5" dx="3" dy="0.9" layer="1"/>
<text x="8.46" y="-7.205" size="1.27" layer="51" rot="R180">&gt;NAME</text>
</package>
<package name="USB-MINIB-NOSTOP">
<wire x1="-1.3" y1="3.8" x2="0.8" y2="3.8" width="0.2032" layer="21"/>
<wire x1="3.3" y1="3.1" x2="3.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.2" x2="3.3" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="-3.8" x2="-1.3" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-5.9" y1="3.8" x2="-5.9" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="-3.8" x2="-4.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="3.8" x2="-4.5" y2="3.8" width="0.2032" layer="51"/>
<circle x="0" y="2.2" radius="0.35" width="0.41" layer="29"/>
<circle x="0" y="-2.2" radius="0.35" width="0.41" layer="29"/>
<pad name="H1" x="0" y="2.2" drill="0.9" diameter="0.8" stop="no"/>
<pad name="H2" x="0" y="-2.2" drill="0.9" diameter="0.7874" stop="no"/>
<smd name="D+" x="2.5" y="0" dx="2.5" dy="0.5" layer="1"/>
<smd name="D-" x="2.5" y="0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="GND" x="2.5" y="-1.6" dx="2.5" dy="0.5" layer="1"/>
<smd name="ID" x="2.5" y="-0.8" dx="2.5" dy="0.5" layer="1"/>
<smd name="G1" x="-3" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="G2" x="-3" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="G4" x="2.5" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="G3" x="2.5" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="VBUS" x="2.5" y="1.6" dx="2.5" dy="0.5" layer="1"/>
<text x="-3.81" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="0" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="USB-A-S-SILK-FEMALE">
<wire x1="6.6957" y1="6.5659" x2="-7.287" y2="6.5659" width="0.127" layer="51"/>
<wire x1="6.6957" y1="-6.5659" x2="-7.287" y2="-6.5659" width="0.127" layer="51"/>
<wire x1="-7.287" y1="6.477" x2="-7.287" y2="-6.477" width="0.127" layer="51"/>
<wire x1="6.7084" y1="6.5024" x2="6.7084" y2="-6.5024" width="0.127" layer="51"/>
<wire x1="0.46" y1="-5.08" x2="-5.89" y2="-4.445" width="0.127" layer="51"/>
<wire x1="-5.89" y1="-4.445" x2="-5.89" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-5.89" y1="-1.27" x2="0.46" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.46" y1="5.08" x2="-5.89" y2="4.445" width="0.127" layer="51"/>
<wire x1="-5.89" y1="4.445" x2="-5.89" y2="1.27" width="0.127" layer="51"/>
<wire x1="-5.89" y1="1.27" x2="0.46" y2="0.635" width="0.127" layer="51"/>
<wire x1="-7.366" y1="6.604" x2="0.508" y2="6.604" width="0.2032" layer="21"/>
<wire x1="-7.366" y1="6.604" x2="-7.366" y2="-6.604" width="0.2032" layer="21"/>
<wire x1="-7.366" y1="-6.604" x2="0.508" y2="-6.604" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-6.604" x2="6.858" y2="-6.604" width="0.2032" layer="21"/>
<wire x1="6.858" y1="-6.604" x2="6.858" y2="-4.318" width="0.2032" layer="21"/>
<wire x1="6.858" y1="4.318" x2="6.858" y2="6.604" width="0.2032" layer="21"/>
<wire x1="6.858" y1="6.604" x2="5.08" y2="6.604" width="0.2032" layer="21"/>
<pad name="P$5" x="3" y="-6.5659" drill="2.3114" rot="R270"/>
<pad name="P$6" x="3" y="6.5659" drill="2.3114" rot="R270"/>
<smd name="D-" x="7.212" y="1" dx="3" dy="0.9" layer="1"/>
<smd name="VBUS" x="7.212" y="3.5" dx="3" dy="0.9" layer="1"/>
<smd name="D+" x="7.212" y="-1" dx="3" dy="0.9" layer="1"/>
<smd name="GND" x="7.212" y="-3.5" dx="3" dy="0.9" layer="1"/>
<text x="-3.81" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="USB-MICROB">
<description>Micro USB Package</description>
<wire x1="-3.4" y1="-2.15" x2="-3" y2="-2.15" width="0.127" layer="51"/>
<wire x1="3" y1="-2.15" x2="3.4" y2="-2.15" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-2.15" x2="-3.4" y2="-1.45" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-1.45" x2="-3.4" y2="2.85" width="0.127" layer="51"/>
<wire x1="3.4" y1="2.85" x2="2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="3.4" y1="2.85" x2="3.4" y2="-1.45" width="0.127" layer="51"/>
<wire x1="3.4" y1="-1.45" x2="3.4" y2="-2.15" width="0.127" layer="51"/>
<wire x1="-3.4" y1="-1.45" x2="3.4" y2="-1.45" width="0.127" layer="51"/>
<wire x1="-3.4" y1="1.25" x2="-3.4" y2="2.85" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="2.85" x2="-2.2" y2="2.85" width="0.2032" layer="21"/>
<wire x1="3.4" y1="2.85" x2="2.2" y2="2.85" width="0.2032" layer="21"/>
<wire x1="3.4" y1="1.25" x2="3.4" y2="2.85" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-1.45" x2="3.4" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.45" x2="2.2" y2="1.45" width="0.127" layer="51"/>
<wire x1="2.2" y1="1.45" x2="2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.45" x2="-2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="-3.4" y1="2.85" x2="-2.2" y2="2.85" width="0.127" layer="51"/>
<wire x1="-2.2" y1="2.85" x2="-2.2" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="1.45" x2="2.2" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.2" y1="1.45" x2="2.2" y2="2.85" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-2.15" x2="-4" y2="-2.75" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-2.15" x2="4" y2="-2.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.15" x2="-3" y2="-2.55" width="0.127" layer="51"/>
<wire x1="-2.8" y1="-2.8" x2="2.75" y2="-2.8" width="0.127" layer="51"/>
<wire x1="3" y1="-2.6" x2="3" y2="-2.15" width="0.127" layer="51"/>
<wire x1="-3" y1="-2.55" x2="-2.8" y2="-2.8" width="0.127" layer="51" curve="84.547378"/>
<wire x1="2.75" y1="-2.8" x2="3" y2="-2.6" width="0.127" layer="51" curve="84.547378"/>
<smd name="VBUS" x="-1.3" y="2.65" dx="1.4" dy="0.35" layer="1" rot="R90"/>
<smd name="GND" x="1.3" y="2.65" dx="1.4" dy="0.35" layer="1" rot="R90"/>
<smd name="D-" x="-0.65" y="2.65" dx="1.4" dy="0.35" layer="1" rot="R90"/>
<smd name="D+" x="0" y="2.65" dx="1.4" dy="0.35" layer="1" rot="R90"/>
<smd name="ID" x="0.65" y="2.65" dx="1.4" dy="0.35" layer="1" rot="R90"/>
<smd name="MT1" x="-4" y="0" dx="1.8" dy="1.9" layer="1"/>
<smd name="MT2" x="4" y="0" dx="1.8" dy="1.9" layer="1"/>
<text x="-1.6" y="-0.35" size="0.762" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="0.762" layer="27">&gt;VALUE</text>
<smd name="P$1" x="-1.27" y="0" dx="1.9" dy="1.9" layer="1"/>
<smd name="P$2" x="1.27" y="0" dx="1.9" dy="1.9" layer="1"/>
</package>
<package name="USB-A-SMT-MALE">
<wire x1="6" y1="14.58" x2="-6" y2="14.58" width="0.2032" layer="51"/>
<wire x1="6" y1="0" x2="-6" y2="0" width="0.2032" layer="21"/>
<wire x1="6" y1="0" x2="6" y2="14.58" width="0.2032" layer="51"/>
<wire x1="-6" y1="0" x2="-6" y2="14.58" width="0.2032" layer="51"/>
<wire x1="6" y1="0" x2="6" y2="-1.22" width="0.2032" layer="21"/>
<wire x1="-6" y1="0" x2="-6" y2="-1.22" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.22" x2="4" y2="-4.22" width="0.2032" layer="21"/>
<wire x1="4.3" y1="10.28" x2="1.9" y2="10.28" width="0.2032" layer="51"/>
<wire x1="1.9" y1="10.28" x2="1.9" y2="7.98" width="0.2032" layer="51"/>
<wire x1="1.9" y1="7.98" x2="4.3" y2="7.98" width="0.2032" layer="51"/>
<wire x1="4.3" y1="7.98" x2="4.3" y2="10.28" width="0.2032" layer="51"/>
<wire x1="-1.9" y1="10.28" x2="-4.3" y2="10.28" width="0.2032" layer="51"/>
<wire x1="-4.3" y1="10.28" x2="-4.3" y2="7.98" width="0.2032" layer="51"/>
<wire x1="-4.3" y1="7.98" x2="-1.9" y2="7.98" width="0.2032" layer="51"/>
<wire x1="-1.9" y1="7.98" x2="-1.9" y2="10.28" width="0.2032" layer="51"/>
<smd name="D+1" x="1.027" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="D-1" x="-1.027" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="GND1" x="3.5508" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<pad name="P$1" x="5.85" y="-3.05" drill="0.8" diameter="1.778" shape="long" rot="R90"/>
<pad name="P$3" x="-5.85" y="-3.05" drill="0.8" diameter="1.778" shape="long" rot="R90"/>
<smd name="VBUS1" x="-3.5" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<text x="-2.7" y="-9.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.4" y="0.68" size="1.27" layer="51">PCB Edge</text>
<hole x="2.25" y="-3.12" drill="1.1"/>
<hole x="-2.25" y="-3.12" drill="1.1"/>
<hole x="-5.85" y="-3.45" drill="0.8"/>
<hole x="-5.85" y="-3.85" drill="0.8"/>
<hole x="-5.85" y="-2.65" drill="0.8"/>
<hole x="-5.85" y="-2.25" drill="0.8"/>
<hole x="5.85" y="-3.45" drill="0.8"/>
<hole x="5.85" y="-3.85" drill="0.8"/>
<hole x="5.85" y="-2.65" drill="0.8"/>
<hole x="5.85" y="-2.25" drill="0.8"/>
</package>
<package name="USB-A-SMT-MALE-LOCKING">
<wire x1="6" y1="14.58" x2="-6" y2="14.58" width="0.2032" layer="51"/>
<wire x1="6" y1="0" x2="-6" y2="0" width="0.2032" layer="21"/>
<wire x1="6" y1="0" x2="6" y2="14.58" width="0.2032" layer="51"/>
<wire x1="-6" y1="0" x2="-6" y2="14.58" width="0.2032" layer="51"/>
<wire x1="6" y1="0" x2="6" y2="-1.22" width="0.2032" layer="21"/>
<wire x1="-6" y1="0" x2="-6" y2="-1.22" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.22" x2="4" y2="-4.22" width="0.2032" layer="21"/>
<wire x1="4.3" y1="10.28" x2="1.9" y2="10.28" width="0.2032" layer="51"/>
<wire x1="1.9" y1="10.28" x2="1.9" y2="7.98" width="0.2032" layer="51"/>
<wire x1="1.9" y1="7.98" x2="4.3" y2="7.98" width="0.2032" layer="51"/>
<wire x1="4.3" y1="7.98" x2="4.3" y2="10.28" width="0.2032" layer="51"/>
<wire x1="-1.9" y1="10.28" x2="-4.3" y2="10.28" width="0.2032" layer="51"/>
<wire x1="-4.3" y1="10.28" x2="-4.3" y2="7.98" width="0.2032" layer="51"/>
<wire x1="-4.3" y1="7.98" x2="-1.9" y2="7.98" width="0.2032" layer="51"/>
<wire x1="-1.9" y1="7.98" x2="-1.9" y2="10.28" width="0.2032" layer="51"/>
<smd name="D+1" x="1.027" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="D-1" x="-1.027" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<smd name="GND1" x="3.5508" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<pad name="P$1" x="5.6468" y="-3.05" drill="0.8" diameter="1.778" shape="long" rot="R90"/>
<pad name="P$3" x="-5.6468" y="-3.05" drill="0.8" diameter="1.778" shape="long" rot="R90"/>
<smd name="VBUS1" x="-3.5" y="-5.87" dx="2.5" dy="0.8" layer="1" rot="R270"/>
<text x="-2.7" y="-9.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.4" y="0.68" size="1.27" layer="51">PCB Edge</text>
<hole x="2.25" y="-3.12" drill="1.1"/>
<hole x="-2.25" y="-3.12" drill="1.1"/>
<hole x="-5.6468" y="-3.45" drill="0.8"/>
<hole x="-5.6468" y="-3.85" drill="0.8"/>
<hole x="-5.6468" y="-2.65" drill="0.8"/>
<hole x="-5.6468" y="-2.25" drill="0.8"/>
<hole x="5.6468" y="-3.45" drill="0.8"/>
<hole x="5.6468" y="-3.85" drill="0.8"/>
<hole x="5.6468" y="-2.65" drill="0.8"/>
<hole x="5.6468" y="-2.25" drill="0.8"/>
<wire x1="-5.9944" y1="-4.064" x2="-5.9944" y2="-2.0828" width="0" layer="51"/>
<wire x1="-6.1849" y1="-4.064" x2="-6.1849" y2="-2.032" width="0" layer="51"/>
<wire x1="-5.6642" y1="-4.064" x2="-5.6642" y2="-2.0828" width="0" layer="51"/>
<rectangle x1="-5.9944" y1="-4.064" x2="-5.6642" y2="-2.032" layer="51"/>
<wire x1="6.1849" y1="-2.032" x2="6.1849" y2="-4.064" width="0" layer="51"/>
<rectangle x1="5.6642" y1="-4.064" x2="5.9944" y2="-2.032" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="MCP73831">
<pin name="STAT" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="VSS" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="VBAT" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="VDD" x="-15.24" y="5.08" length="middle"/>
<pin name="PROG" x="-15.24" y="-5.08" length="middle"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="8.89" size="1.27" layer="94">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.27" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="LOGO_SIMPLYDUINO">
<text x="0" y="0" size="2.1844" layer="94" font="vector">SimplyDuino</text>
</symbol>
<symbol name="USB-1">
<wire x1="5.08" y1="8.89" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="0" y1="8.89" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="3.81" y="0" size="2.54" layer="94" rot="R90">USB</text>
<pin name="D+" x="-2.54" y="7.62" visible="pad" length="short"/>
<pin name="D-" x="-2.54" y="5.08" visible="pad" length="short"/>
<pin name="VBUS" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="GND" x="-2.54" y="0" visible="pad" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP73831">
<gates>
<gate name="G$1" symbol="MCP73831" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="PROG" pad="5"/>
<connect gate="G$1" pin="STAT" pad="1"/>
<connect gate="G$1" pin="VBAT" pad="3"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOGO_SIMPLYDUINO">
<gates>
<gate name="G$1" symbol="LOGO_SIMPLYDUINO" x="0" y="0"/>
</gates>
<devices>
<device name="NORMAL" package="LOGO_SIMPLYDUINO">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL" package="LOGO_SIMPLYDUINO_SMALL">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB" prefix="JP">
<description>&lt;b&gt;USB Connectors&lt;/b&gt;
&lt;p&gt;USB-B-PTH is fully proven SKU : PRT-00139/CONN-08278
&lt;p&gt;USB-miniB MODIFIED RVEER
&lt;p&gt;USB-A-PCB is untested.
&lt;p&gt;USB-A-H is throughly reviewed, but untested. Spark Fun Electronics SKU : PRT-00437
&lt;p&gt;USB-B-SMT is throughly reviewed, but untested. Needs silkscreen touching up.
&lt;p&gt;USB-A-S has not been used/tested
&lt;p&gt;USB-MB-H has not been used/tested
&lt;P&gt;USB-MICROB has been used. CONN-09505</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="-A-H" package="USB-A-H">
<connects>
<connect gate="G$1" pin="D+" pad="D-"/>
<connect gate="G$1" pin="D-" pad="D+"/>
<connect gate="G$1" pin="GND" pad="VBUS"/>
<connect gate="G$1" pin="VBUS" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-A-S" package="USB-A-S">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MB-H" package="USB-MB-H">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-B-S" package="USB-B-SMT">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VUSB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="OLD" package="USB-MINIB-OLD">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="USB-B-PTH">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08278" constant="no"/>
<attribute name="VALUE" value="USB-B" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="S4B-PH">
<connects>
<connect gate="G$1" pin="D+" pad="2"/>
<connect gate="G$1" pin="D-" pad="3"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VBUS" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="USB-MINIB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08193" constant="no"/>
</technology>
</technologies>
</device>
<device name="PCB" package="USB-A-PCB">
<connects>
<connect gate="G$1" pin="D+" pad="USB_P"/>
<connect gate="G$1" pin="D-" pad="USB_M"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="5V"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-VERTICAL" package="USB-B-PTH-VERTICAL">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="USB-A-S-NOSILK" package="USB-A-S-NOSILK">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-A-SMD" package="USB-A-S-NOSILK-FEMALE">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09520"/>
</technology>
</technologies>
</device>
<device name="-SMD-NS" package="USB-MINIB-NOSTOP">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="USB-A-S-SILK-FEMALE">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MICROB" package="USB-MICROB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND MT1 MT2"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09505"/>
</technology>
</technologies>
</device>
<device name="A-SMD-MALE" package="USB-A-SMT-MALE">
<connects>
<connect gate="G$1" pin="D+" pad="D+1"/>
<connect gate="G$1" pin="D-" pad="D-1"/>
<connect gate="G$1" pin="GND" pad="GND1"/>
<connect gate="G$1" pin="VBUS" pad="VBUS1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A-SMD-MALE-LOCKING" package="USB-A-SMT-MALE-LOCKING">
<connects>
<connect gate="G$1" pin="D+" pad="D+1"/>
<connect gate="G$1" pin="D-" pad="D-1"/>
<connect gate="G$1" pin="GND" pad="GND1"/>
<connect gate="G$1" pin="VBUS" pad="VBUS1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="eagle-ltspice">
<description>Default symbols for import LTspice schematics&lt;p&gt;
2012-10-29 alf@cadsoft.de&lt;br&gt;</description>
<packages>
<package name="LED_1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="LED_LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED_SML0603">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LED_LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
<text x="0" y="-5.08" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
<text x="0" y="2.54" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_E" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED_LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="D" constant="no"/>
<attribute name="SPICETYPE" value="diode" constant="no"/>
</technology>
</technologies>
</device>
<device name="LD260" package="LED_LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="D" constant="no"/>
<attribute name="SPICETYPE" value="diode" constant="no"/>
</technology>
</technologies>
</device>
<device name="SML0603" package="LED_SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="SPICEMODEL" value="NONE" constant="no"/>
<attribute name="SPICEPREFIX" value="D" constant="no"/>
<attribute name="SPICETYPE" value="diode" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DiscreteSemi">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete semiconductors- transistors, diodes, TRIACs, optoisolators, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-0.889" y="2.032" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT23-3">
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-0.8255" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="TO-92">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-0.7863" y1="2.5485" x2="-2.0946" y2="-1.651" width="0.2032" layer="21" curve="111.098962"/>
<wire x1="2.0945" y1="-1.651" x2="0.7863" y2="2.548396875" width="0.2032" layer="21" curve="111.099507"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.2537" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="-0.2863" y1="-0.254" x2="0.2863" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="2.2537" y1="-0.254" x2="2.6549" y2="-0.254" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="2" x="0" y="1.905" drill="0.8128" diameter="1.8796"/>
<pad name="3" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.635" size="0.4064" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SOT223">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<wire x1="3.2766" y1="1.651" x2="3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.2766" y1="-1.651" x2="-3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="-1.651" x2="-3.2766" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="1.651" x2="3.2766" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1"/>
<text x="-0.8255" y="4.5085" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
</package>
<package name="SOT89">
<wire x1="2.3" y1="1.3" x2="2.3" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.3" x2="-2.3" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.3" x2="-2.3" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.3" x2="2.3" y2="1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="-1.8" dx="0.58" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.8" dx="0.58" dy="1.2" layer="1"/>
<smd name="3" x="1.5" y="-1.8" dx="0.58" dy="1.2" layer="1"/>
<smd name="4" x="0" y="1.3" dx="2" dy="2" layer="1"/>
<text x="-2.5185" y="-0.9555" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="2.9905" y="-1.1295" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.915" y1="0.3034" x2="0.915" y2="2.1576" layer="51"/>
<rectangle x1="-1.74" y1="-2.23" x2="-1.26" y2="-1.27" layer="51"/>
<rectangle x1="-0.24" y1="-2.23" x2="0.24" y2="-1.27" layer="51"/>
<rectangle x1="1.26" y1="-2.23" x2="1.74" y2="-1.27" layer="51"/>
</package>
<package name="TO-92-AMMO">
<wire x1="-2.0946" y1="-1.651" x2="-0.7863" y2="2.5485" width="0.2032" layer="21" curve="-111.098957" cap="flat"/>
<wire x1="0.7863" y1="2.5484" x2="2.0945" y2="-1.651" width="0.2032" layer="21" curve="-111.09954" cap="flat"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.2032" layer="21" curve="-25.057615"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="1.905" width="0.2032" layer="21" curve="-28.072487"/>
<wire x1="-1.905" y1="1.905" x2="0.635" y2="2.54" width="0.2032" layer="21" curve="-53.130102"/>
<pad name="3" x="2.54" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="2" x="0" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="1.905" size="0.4064" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.905" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;&lt;p&gt;
Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DIODE-1N4001">
<wire x1="3.175" y1="1.27" x2="1.905" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.175" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<pad name="A" x="-5.08" y="0" drill="1" diameter="1.9812"/>
<pad name="C" x="5.08" y="0" drill="1" diameter="1.9812"/>
<text x="-2.921" y="1.651" size="0.6096" layer="25">&gt;Name</text>
<text x="-2.921" y="-0.508" size="1.016" layer="21" ratio="12">&gt;Value</text>
</package>
<package name="SOD-323">
<wire x1="-0.9" y1="0.65" x2="-0.5" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="0.9" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.65" x2="0.9" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<smd name="C" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="A" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="-0.889" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="DIODE-1N4148">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="C" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.032" y="-0.508" size="0.8128" layer="21">&gt;Value</text>
</package>
<package name="SMB-DIODE">
<description>&lt;b&gt;Diode&lt;/b&gt;&lt;p&gt;
Basic small signal diode good up to 200mA. SMB footprint. Common part #: BAS16</description>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<smd name="A" x="2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="DIODE-HV">
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.454" y="0" dx="2.2" dy="2.4" layer="1"/>
<smd name="A" x="2.454" y="0" dx="2.2" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="SMA-DIODE_ALT">
<wire x1="-2.3" y1="1.3" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1.3" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.3" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="0.6" y1="1" x2="0.6" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="C" x="2" y="0" dx="2" dy="2" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SMA-DIODE-KIT">
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.4" y="0" dx="1.77" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.4" y="0" dx="1.77" dy="1.47" layer="1"/>
<text x="-2.286" y="1.651" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.254" y="1.651" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOD523">
<wire x1="-0.59" y1="0.4" x2="0.59" y2="0.4" width="0.1016" layer="21"/>
<wire x1="0.59" y1="0.4" x2="0.59" y2="-0.4" width="0.1016" layer="51"/>
<wire x1="0.59" y1="-0.4" x2="-0.59" y2="-0.4" width="0.1016" layer="21"/>
<wire x1="-0.59" y1="-0.4" x2="-0.59" y2="0.4" width="0.1016" layer="51"/>
<rectangle x1="-0.75" y1="-0.17" x2="-0.54" y2="0.17" layer="51"/>
<rectangle x1="0.54" y1="-0.17" x2="0.75" y2="0.17" layer="51"/>
<rectangle x1="-0.59" y1="-0.4" x2="-0.3" y2="0.4" layer="51"/>
<smd name="A" x="0.7" y="0" dx="0.7" dy="0.5" layer="1"/>
<smd name="C" x="-0.6" y="0" dx="0.7" dy="0.5" layer="1"/>
<text x="-0.7366" y="0.5588" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.6858" y="-0.9906" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1397" y1="-0.3937" x2="-0.0127" y2="0.381" layer="21"/>
</package>
<package name="SMC">
<description>&lt;b&gt;DIODE&lt;/b&gt;</description>
<wire x1="-3.5606" y1="3.105" x2="3.5606" y2="3.105" width="0.1016" layer="21"/>
<wire x1="-3.5606" y1="-3.105" x2="3.5606" y2="-3.105" width="0.1016" layer="21"/>
<wire x1="-3.5606" y1="-3.105" x2="-3.5606" y2="3.105" width="0.1016" layer="51"/>
<wire x1="3.5606" y1="-3.105" x2="3.5606" y2="3.105" width="0.1016" layer="51"/>
<wire x1="0.543" y1="1" x2="-0.83" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.83" y1="0" x2="0.543" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.543" y1="-1" x2="0.543" y2="1" width="0.2032" layer="21"/>
<smd name="C" x="-3.7" y="0" dx="2.8" dy="3.8" layer="1"/>
<smd name="A" x="3.7" y="0" dx="2.8" dy="3.8" layer="1"/>
<text x="-3.459" y="3.359" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.459" y="-4.629" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.094" y1="-1.0922" x2="-3.5606" y2="1.0922" layer="51"/>
<rectangle x1="3.5606" y1="-1.0922" x2="4.094" y2="1.0922" layer="51"/>
<rectangle x1="-2.1" y1="-3.1" x2="-0.85" y2="3.1" layer="21"/>
</package>
<package name="DIODE-1N4148-KIT">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="C" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-2.032" y="-0.508" size="0.8128" layer="21">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="0.9398" width="0" layer="30"/>
<circle x="-3.81" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.9398" width="0" layer="30"/>
</package>
<package name="DIODE-1N4001-KIT">
<wire x1="3.175" y1="1.27" x2="1.905" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.254" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.175" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.27" width="0.254" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.81" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="0" x2="3.81" y2="0" width="0.254" layer="21"/>
<pad name="A" x="-5.08" y="0" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="C" x="5.08" y="0" drill="1.016" diameter="1.8796" stop="no"/>
<text x="-2.921" y="1.651" size="0.6096" layer="25">&gt;Name</text>
<text x="-2.921" y="-0.508" size="1.016" layer="21" ratio="12">&gt;Value</text>
<circle x="-5.08" y="0" radius="0.508" width="0" layer="29"/>
<circle x="5.08" y="0" radius="0.508" width="0" layer="29"/>
<circle x="-5.08" y="0" radius="0.9906" width="0" layer="30"/>
<circle x="5.08" y="0" radius="0.9906" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="5.08" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRANSISTOR_NPN" prefix="Q" uservalue="yes">
<description>&lt;b&gt;Generic NPN BJT&lt;/b&gt; 
&lt;p&gt;
MMBT2222A - TRANS-08049 (SOT-23, 1A 40V)&lt;br&gt;
MPSA42 - TRANS-09116 (SOT-23, 500mA 300V)&lt;br&gt;
MMBT5088LT1G - TRANS-11160 (SOT-23, 50 mA 30V)&lt;br&gt;
&lt;/p&gt;
&lt;ul&gt;
  &lt;li&gt;
    BC547 - 
    &lt;a href="http://www.sparkfun.com/products/8928"&gt;COM-08928&lt;/a&gt;
    (TO-92 45V 100mA) (1.Collector 2.Base 3.Emitter)
  &lt;/li&gt;
  &lt;li&gt;
    2N3904 - 
    &lt;a href="http://www.sparkfun.com/products/521"&gt;COM-00521&lt;/a&gt;
    (TO-92 40V 200mA) (1.Emitter 2.Base 3.Collector)
  &lt;/li&gt;
  &lt;li&gt;
    P2N2222A - 
    &lt;a href="http://www.sparkfun.com/products/12852"&gt;COM-12852&lt;/a&gt;
    (TO-92 40V 600mA) (1.Collector 2.Base 3.Emitter)
  &lt;/li&gt;

&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23-3" package="SOT23-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO92" package="TO-92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="SOT223">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT89" package="SOT89">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO-92-AMMO" package="TO-92-AMMO">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MPSA42" package="SOT23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-09116"/>
<attribute name="VALUE" value="MPSA42" constant="no"/>
</technology>
</technologies>
</device>
<device name="MMBT5088" package="SOT23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-11160"/>
<attribute name="VALUE" value="MMBT5088" constant="no"/>
</technology>
</technologies>
</device>
<device name="MMBT2222A" package="SOT23-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08049" constant="no"/>
<attribute name="VALUE" value="MMBT2222A" constant="no"/>
</technology>
</technologies>
</device>
<device name="2N3904" package="TO-92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-08447" constant="no"/>
</technology>
</technologies>
</device>
<device name="BC547" package="TO-92-AMMO">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P2N2222A" package="TO-92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="TRANS-09536" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;b&gt;Diode&lt;/b&gt;
These are standard reverse protection diodes and small signal diodes. SMA package can handle up to about 1A. SOD-323 can handle about 200mA. What the SOD-323 package when ordering, there are some mfgs out there that are 5-pin packages.</description>
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="SMA" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH" package="DIODE-1N4001">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09646" constant="no"/>
</technology>
</technologies>
</device>
<device name="SOT23" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1N4148" package="DIODE-1N4148">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="SMB-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09646" constant="no"/>
</technology>
</technologies>
</device>
<device name="HV" package="DIODE-HV">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA-ALT" package="SMA-DIODE_ALT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA-KIT" package="SMA-DIODE-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD523" package="SOD523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMC/DO-214AB" package="SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="DIODE-1N4148-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="DIODE-1N4001-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-FreqCtrl">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find crystals and oscillators and other things that go "tick".&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="RESONATOR-PTH">
<wire x1="-2.286" y1="1.778" x2="-2.286" y2="-1.778" width="0.2032" layer="21" curve="162.403081"/>
<wire x1="2.286" y1="-1.778" x2="2.286" y2="1.778" width="0.2032" layer="21" curve="162.403081"/>
<wire x1="-2.286" y1="-1.778" x2="2.286" y2="-1.778" width="0.2032" layer="21" curve="12.71932"/>
<wire x1="2.286" y1="1.778" x2="-2.286" y2="1.778" width="0.2032" layer="21" curve="12.758496"/>
<pad name="2" x="0" y="0" drill="0.8" diameter="1.8796"/>
<pad name="1" x="-2.54" y="0" drill="0.8" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="0.8" diameter="1.8796"/>
</package>
<package name="RESONATOR-SMD">
<wire x1="1.87111875" y1="0.660159375" x2="1.87111875" y2="-0.639840625" width="0.2032" layer="21"/>
<wire x1="-1.878740625" y1="-0.639840625" x2="-1.878740625" y2="0.660159375" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.127" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="0.45" dy="1.905" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="0.45" dy="1.905" layer="1" roundness="100"/>
<smd name="3" x="1.2" y="0" dx="0.45" dy="1.905" layer="1" roundness="100"/>
<text x="-1.016" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-0.9525" y="-1.5875" size="0.4064" layer="27">&gt;Value</text>
<rectangle x1="-1.6332625" y1="-0.6406375" x2="1.6433375" y2="-0.61879375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.61879375" x2="-1.61141875" y2="-0.59695" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.61879375" x2="-0.999790625" y2="-0.59695" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.61879375" x2="0.201634375" y2="-0.59695" layer="200"/>
<rectangle x1="0.98801875" y1="-0.61879375" x2="1.40305" y2="-0.59695" layer="200"/>
<rectangle x1="1.62149375" y1="-0.61879375" x2="1.6433375" y2="-0.59695" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.59695" x2="-1.61141875" y2="-0.57510625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.59695" x2="-0.999790625" y2="-0.57510625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.59695" x2="0.201634375" y2="-0.57510625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.59695" x2="1.40305" y2="-0.57510625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.59695" x2="1.6433375" y2="-0.57510625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.57510625" x2="-1.61141875" y2="-0.5532625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.57510625" x2="-0.999790625" y2="-0.5532625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.57510625" x2="0.201634375" y2="-0.5532625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.57510625" x2="1.40305" y2="-0.5532625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.57510625" x2="1.6433375" y2="-0.5532625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.5532625" x2="-1.61141875" y2="-0.53141875" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.5532625" x2="-0.999790625" y2="-0.53141875" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.5532625" x2="0.201634375" y2="-0.53141875" layer="200"/>
<rectangle x1="0.98801875" y1="-0.5532625" x2="1.40305" y2="-0.53141875" layer="200"/>
<rectangle x1="1.62149375" y1="-0.5532625" x2="1.6433375" y2="-0.53141875" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.53141875" x2="-1.61141875" y2="-0.509575" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.53141875" x2="-0.999790625" y2="-0.509575" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.53141875" x2="0.201634375" y2="-0.509575" layer="200"/>
<rectangle x1="0.98801875" y1="-0.53141875" x2="1.40305" y2="-0.509575" layer="200"/>
<rectangle x1="1.62149375" y1="-0.53141875" x2="1.6433375" y2="-0.509575" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.509575" x2="-1.61141875" y2="-0.48773125" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.509575" x2="-0.999790625" y2="-0.48773125" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.509575" x2="0.201634375" y2="-0.48773125" layer="200"/>
<rectangle x1="0.98801875" y1="-0.509575" x2="1.40305" y2="-0.48773125" layer="200"/>
<rectangle x1="1.62149375" y1="-0.509575" x2="1.6433375" y2="-0.48773125" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.48773125" x2="-1.61141875" y2="-0.4658875" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.48773125" x2="-0.999790625" y2="-0.4658875" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.48773125" x2="0.201634375" y2="-0.4658875" layer="200"/>
<rectangle x1="0.98801875" y1="-0.48773125" x2="1.40305" y2="-0.4658875" layer="200"/>
<rectangle x1="1.62149375" y1="-0.48773125" x2="1.6433375" y2="-0.4658875" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.4658875" x2="-1.61141875" y2="-0.44404375" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.4658875" x2="-0.999790625" y2="-0.44404375" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.4658875" x2="0.201634375" y2="-0.44404375" layer="200"/>
<rectangle x1="0.98801875" y1="-0.4658875" x2="1.40305" y2="-0.44404375" layer="200"/>
<rectangle x1="1.62149375" y1="-0.4658875" x2="1.6433375" y2="-0.44404375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.444040625" x2="-1.61141875" y2="-0.422196875" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.444040625" x2="-0.999790625" y2="-0.422196875" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.444040625" x2="0.201634375" y2="-0.422196875" layer="200"/>
<rectangle x1="0.98801875" y1="-0.444040625" x2="1.40305" y2="-0.422196875" layer="200"/>
<rectangle x1="1.62149375" y1="-0.444040625" x2="1.6433375" y2="-0.422196875" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.422196875" x2="-1.61141875" y2="-0.400353125" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.422196875" x2="-0.999790625" y2="-0.400353125" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.422196875" x2="0.201634375" y2="-0.400353125" layer="200"/>
<rectangle x1="0.98801875" y1="-0.422196875" x2="1.40305" y2="-0.400353125" layer="200"/>
<rectangle x1="1.62149375" y1="-0.422196875" x2="1.6433375" y2="-0.400353125" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.400353125" x2="-1.61141875" y2="-0.378509375" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.400353125" x2="-0.999790625" y2="-0.378509375" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.400353125" x2="0.201634375" y2="-0.378509375" layer="200"/>
<rectangle x1="0.98801875" y1="-0.400353125" x2="1.40305" y2="-0.378509375" layer="200"/>
<rectangle x1="1.62149375" y1="-0.400353125" x2="1.6433375" y2="-0.378509375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.378509375" x2="-1.61141875" y2="-0.356665625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.378509375" x2="-0.999790625" y2="-0.356665625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.378509375" x2="0.201634375" y2="-0.356665625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.378509375" x2="1.40305" y2="-0.356665625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.378509375" x2="1.6433375" y2="-0.356665625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.356665625" x2="-1.61141875" y2="-0.334821875" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.356665625" x2="-0.999790625" y2="-0.334821875" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.356665625" x2="0.201634375" y2="-0.334821875" layer="200"/>
<rectangle x1="0.98801875" y1="-0.356665625" x2="1.40305" y2="-0.334821875" layer="200"/>
<rectangle x1="1.62149375" y1="-0.356665625" x2="1.6433375" y2="-0.334821875" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.334821875" x2="-1.61141875" y2="-0.312978125" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.334821875" x2="-0.999790625" y2="-0.312978125" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.334821875" x2="0.201634375" y2="-0.312978125" layer="200"/>
<rectangle x1="0.98801875" y1="-0.334821875" x2="1.40305" y2="-0.312978125" layer="200"/>
<rectangle x1="1.62149375" y1="-0.334821875" x2="1.6433375" y2="-0.312978125" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.312978125" x2="-1.61141875" y2="-0.291134375" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.312978125" x2="-0.999790625" y2="-0.291134375" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.312978125" x2="0.201634375" y2="-0.291134375" layer="200"/>
<rectangle x1="0.98801875" y1="-0.312978125" x2="1.40305" y2="-0.291134375" layer="200"/>
<rectangle x1="1.62149375" y1="-0.312978125" x2="1.6433375" y2="-0.291134375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.291134375" x2="-1.61141875" y2="-0.269290625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.291134375" x2="-0.999790625" y2="-0.269290625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.291134375" x2="0.201634375" y2="-0.269290625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.291134375" x2="1.40305" y2="-0.269290625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.291134375" x2="1.6433375" y2="-0.269290625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.269290625" x2="-1.61141875" y2="-0.247446875" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.269290625" x2="-0.999790625" y2="-0.247446875" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.269290625" x2="0.201634375" y2="-0.247446875" layer="200"/>
<rectangle x1="0.98801875" y1="-0.269290625" x2="1.40305" y2="-0.247446875" layer="200"/>
<rectangle x1="1.62149375" y1="-0.269290625" x2="1.6433375" y2="-0.247446875" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.247446875" x2="-1.61141875" y2="-0.225603125" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.247446875" x2="-0.999790625" y2="-0.225603125" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.247446875" x2="0.201634375" y2="-0.225603125" layer="200"/>
<rectangle x1="0.98801875" y1="-0.247446875" x2="1.40305" y2="-0.225603125" layer="200"/>
<rectangle x1="1.62149375" y1="-0.247446875" x2="1.6433375" y2="-0.225603125" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.225603125" x2="-1.61141875" y2="-0.203759375" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.225603125" x2="-0.999790625" y2="-0.203759375" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.225603125" x2="0.201634375" y2="-0.203759375" layer="200"/>
<rectangle x1="0.98801875" y1="-0.225603125" x2="1.40305" y2="-0.203759375" layer="200"/>
<rectangle x1="1.62149375" y1="-0.225603125" x2="1.6433375" y2="-0.203759375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.203759375" x2="-1.61141875" y2="-0.181915625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.203759375" x2="-0.999790625" y2="-0.181915625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.203759375" x2="0.201634375" y2="-0.181915625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.203759375" x2="1.40305" y2="-0.181915625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.203759375" x2="1.6433375" y2="-0.181915625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.1819125" x2="-1.61141875" y2="-0.16006875" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.1819125" x2="-0.999790625" y2="-0.16006875" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.1819125" x2="0.201634375" y2="-0.16006875" layer="200"/>
<rectangle x1="0.98801875" y1="-0.1819125" x2="1.40305" y2="-0.16006875" layer="200"/>
<rectangle x1="1.62149375" y1="-0.1819125" x2="1.6433375" y2="-0.16006875" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.16006875" x2="-1.61141875" y2="-0.138225" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.16006875" x2="-0.999790625" y2="-0.138225" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.16006875" x2="0.201634375" y2="-0.138225" layer="200"/>
<rectangle x1="0.98801875" y1="-0.16006875" x2="1.40305" y2="-0.138225" layer="200"/>
<rectangle x1="1.62149375" y1="-0.16006875" x2="1.6433375" y2="-0.138225" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.138225" x2="-1.61141875" y2="-0.11638125" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.138225" x2="-0.999790625" y2="-0.11638125" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.138225" x2="0.201634375" y2="-0.11638125" layer="200"/>
<rectangle x1="0.98801875" y1="-0.138225" x2="1.40305" y2="-0.11638125" layer="200"/>
<rectangle x1="1.62149375" y1="-0.138225" x2="1.6433375" y2="-0.11638125" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.11638125" x2="-1.61141875" y2="-0.0945375" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.11638125" x2="-0.999790625" y2="-0.0945375" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.11638125" x2="0.201634375" y2="-0.0945375" layer="200"/>
<rectangle x1="0.98801875" y1="-0.11638125" x2="1.40305" y2="-0.0945375" layer="200"/>
<rectangle x1="1.62149375" y1="-0.11638125" x2="1.6433375" y2="-0.0945375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.0945375" x2="-1.61141875" y2="-0.07269375" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.0945375" x2="-0.999790625" y2="-0.07269375" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.0945375" x2="0.201634375" y2="-0.07269375" layer="200"/>
<rectangle x1="0.98801875" y1="-0.0945375" x2="1.40305" y2="-0.07269375" layer="200"/>
<rectangle x1="1.62149375" y1="-0.0945375" x2="1.6433375" y2="-0.07269375" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.07269375" x2="-1.61141875" y2="-0.05085" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.07269375" x2="-0.999790625" y2="-0.05085" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.07269375" x2="0.201634375" y2="-0.05085" layer="200"/>
<rectangle x1="0.98801875" y1="-0.07269375" x2="1.40305" y2="-0.05085" layer="200"/>
<rectangle x1="1.62149375" y1="-0.07269375" x2="1.6433375" y2="-0.05085" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.05085" x2="-1.61141875" y2="-0.02900625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.05085" x2="-0.999790625" y2="-0.02900625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.05085" x2="0.201634375" y2="-0.02900625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.05085" x2="1.40305" y2="-0.02900625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.05085" x2="1.6433375" y2="-0.02900625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.02900625" x2="-1.61141875" y2="-0.0071625" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.02900625" x2="-0.999790625" y2="-0.0071625" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.02900625" x2="0.201634375" y2="-0.0071625" layer="200"/>
<rectangle x1="0.98801875" y1="-0.02900625" x2="1.40305" y2="-0.0071625" layer="200"/>
<rectangle x1="1.62149375" y1="-0.02900625" x2="1.6433375" y2="-0.0071625" layer="200"/>
<rectangle x1="-1.6332625" y1="-0.0071625" x2="-1.61141875" y2="0.01468125" layer="200"/>
<rectangle x1="-1.414821875" y1="-0.0071625" x2="-0.999790625" y2="0.01468125" layer="200"/>
<rectangle x1="-0.213403125" y1="-0.0071625" x2="0.201634375" y2="0.01468125" layer="200"/>
<rectangle x1="0.98801875" y1="-0.0071625" x2="1.40305" y2="0.01468125" layer="200"/>
<rectangle x1="1.62149375" y1="-0.0071625" x2="1.6433375" y2="0.01468125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.01468125" x2="-1.61141875" y2="0.036525" layer="200"/>
<rectangle x1="-1.414821875" y1="0.01468125" x2="-0.999790625" y2="0.036525" layer="200"/>
<rectangle x1="-0.213403125" y1="0.01468125" x2="0.201634375" y2="0.036525" layer="200"/>
<rectangle x1="0.98801875" y1="0.01468125" x2="1.40305" y2="0.036525" layer="200"/>
<rectangle x1="1.62149375" y1="0.01468125" x2="1.6433375" y2="0.036525" layer="200"/>
<rectangle x1="-1.6332625" y1="0.036525" x2="-1.61141875" y2="0.05836875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.036525" x2="-0.999790625" y2="0.05836875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.036525" x2="0.201634375" y2="0.05836875" layer="200"/>
<rectangle x1="0.98801875" y1="0.036525" x2="1.40305" y2="0.05836875" layer="200"/>
<rectangle x1="1.62149375" y1="0.036525" x2="1.6433375" y2="0.05836875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.05836875" x2="-1.61141875" y2="0.0802125" layer="200"/>
<rectangle x1="-1.414821875" y1="0.05836875" x2="-0.999790625" y2="0.0802125" layer="200"/>
<rectangle x1="-0.213403125" y1="0.05836875" x2="0.201634375" y2="0.0802125" layer="200"/>
<rectangle x1="0.98801875" y1="0.05836875" x2="1.40305" y2="0.0802125" layer="200"/>
<rectangle x1="1.62149375" y1="0.05836875" x2="1.6433375" y2="0.0802125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.0802125" x2="-1.61141875" y2="0.10205625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.0802125" x2="-0.999790625" y2="0.10205625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.0802125" x2="0.201634375" y2="0.10205625" layer="200"/>
<rectangle x1="0.98801875" y1="0.0802125" x2="1.40305" y2="0.10205625" layer="200"/>
<rectangle x1="1.62149375" y1="0.0802125" x2="1.6433375" y2="0.10205625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.102059375" x2="-1.61141875" y2="0.123903125" layer="200"/>
<rectangle x1="-1.414821875" y1="0.102059375" x2="-0.999790625" y2="0.123903125" layer="200"/>
<rectangle x1="-0.213403125" y1="0.102059375" x2="0.201634375" y2="0.123903125" layer="200"/>
<rectangle x1="0.98801875" y1="0.102059375" x2="1.40305" y2="0.123903125" layer="200"/>
<rectangle x1="1.62149375" y1="0.102059375" x2="1.6433375" y2="0.123903125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.123903125" x2="-1.61141875" y2="0.145746875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.123903125" x2="-0.999790625" y2="0.145746875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.123903125" x2="0.201634375" y2="0.145746875" layer="200"/>
<rectangle x1="0.98801875" y1="0.123903125" x2="1.40305" y2="0.145746875" layer="200"/>
<rectangle x1="1.62149375" y1="0.123903125" x2="1.6433375" y2="0.145746875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.145746875" x2="-1.61141875" y2="0.167590625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.145746875" x2="-0.999790625" y2="0.167590625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.145746875" x2="0.201634375" y2="0.167590625" layer="200"/>
<rectangle x1="0.98801875" y1="0.145746875" x2="1.40305" y2="0.167590625" layer="200"/>
<rectangle x1="1.62149375" y1="0.145746875" x2="1.6433375" y2="0.167590625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.167590625" x2="-1.61141875" y2="0.189434375" layer="200"/>
<rectangle x1="-1.414821875" y1="0.167590625" x2="-0.999790625" y2="0.189434375" layer="200"/>
<rectangle x1="-0.213403125" y1="0.167590625" x2="0.201634375" y2="0.189434375" layer="200"/>
<rectangle x1="0.98801875" y1="0.167590625" x2="1.40305" y2="0.189434375" layer="200"/>
<rectangle x1="1.62149375" y1="0.167590625" x2="1.6433375" y2="0.189434375" layer="200"/>
<rectangle x1="-1.6332625" y1="0.189434375" x2="-1.61141875" y2="0.211278125" layer="200"/>
<rectangle x1="-1.414821875" y1="0.189434375" x2="-0.999790625" y2="0.211278125" layer="200"/>
<rectangle x1="-0.213403125" y1="0.189434375" x2="0.201634375" y2="0.211278125" layer="200"/>
<rectangle x1="0.98801875" y1="0.189434375" x2="1.40305" y2="0.211278125" layer="200"/>
<rectangle x1="1.62149375" y1="0.189434375" x2="1.6433375" y2="0.211278125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.211278125" x2="-1.61141875" y2="0.233121875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.211278125" x2="-0.999790625" y2="0.233121875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.211278125" x2="0.201634375" y2="0.233121875" layer="200"/>
<rectangle x1="0.98801875" y1="0.211278125" x2="1.40305" y2="0.233121875" layer="200"/>
<rectangle x1="1.62149375" y1="0.211278125" x2="1.6433375" y2="0.233121875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.233121875" x2="-1.61141875" y2="0.254965625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.233121875" x2="-0.999790625" y2="0.254965625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.233121875" x2="0.201634375" y2="0.254965625" layer="200"/>
<rectangle x1="0.98801875" y1="0.233121875" x2="1.40305" y2="0.254965625" layer="200"/>
<rectangle x1="1.62149375" y1="0.233121875" x2="1.6433375" y2="0.254965625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.254965625" x2="-1.61141875" y2="0.276809375" layer="200"/>
<rectangle x1="-1.414821875" y1="0.254965625" x2="-0.999790625" y2="0.276809375" layer="200"/>
<rectangle x1="-0.213403125" y1="0.254965625" x2="0.201634375" y2="0.276809375" layer="200"/>
<rectangle x1="0.98801875" y1="0.254965625" x2="1.40305" y2="0.276809375" layer="200"/>
<rectangle x1="1.62149375" y1="0.254965625" x2="1.6433375" y2="0.276809375" layer="200"/>
<rectangle x1="-1.6332625" y1="0.276809375" x2="-1.61141875" y2="0.298653125" layer="200"/>
<rectangle x1="-1.414821875" y1="0.276809375" x2="-0.999790625" y2="0.298653125" layer="200"/>
<rectangle x1="-0.213403125" y1="0.276809375" x2="0.201634375" y2="0.298653125" layer="200"/>
<rectangle x1="0.98801875" y1="0.276809375" x2="1.40305" y2="0.298653125" layer="200"/>
<rectangle x1="1.62149375" y1="0.276809375" x2="1.6433375" y2="0.298653125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.298653125" x2="-1.61141875" y2="0.320496875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.298653125" x2="-0.999790625" y2="0.320496875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.298653125" x2="0.201634375" y2="0.320496875" layer="200"/>
<rectangle x1="0.98801875" y1="0.298653125" x2="1.40305" y2="0.320496875" layer="200"/>
<rectangle x1="1.62149375" y1="0.298653125" x2="1.6433375" y2="0.320496875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.320496875" x2="-1.61141875" y2="0.342340625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.320496875" x2="-0.999790625" y2="0.342340625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.320496875" x2="0.201634375" y2="0.342340625" layer="200"/>
<rectangle x1="0.98801875" y1="0.320496875" x2="1.40305" y2="0.342340625" layer="200"/>
<rectangle x1="1.62149375" y1="0.320496875" x2="1.6433375" y2="0.342340625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.342340625" x2="-1.61141875" y2="0.364184375" layer="200"/>
<rectangle x1="-1.414821875" y1="0.342340625" x2="-0.999790625" y2="0.364184375" layer="200"/>
<rectangle x1="-0.213403125" y1="0.342340625" x2="0.201634375" y2="0.364184375" layer="200"/>
<rectangle x1="0.98801875" y1="0.342340625" x2="1.40305" y2="0.364184375" layer="200"/>
<rectangle x1="1.62149375" y1="0.342340625" x2="1.6433375" y2="0.364184375" layer="200"/>
<rectangle x1="-1.6332625" y1="0.3641875" x2="-1.61141875" y2="0.38603125" layer="200"/>
<rectangle x1="-1.414821875" y1="0.3641875" x2="-0.999790625" y2="0.38603125" layer="200"/>
<rectangle x1="-0.213403125" y1="0.3641875" x2="0.201634375" y2="0.38603125" layer="200"/>
<rectangle x1="0.98801875" y1="0.3641875" x2="1.40305" y2="0.38603125" layer="200"/>
<rectangle x1="1.62149375" y1="0.3641875" x2="1.6433375" y2="0.38603125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.38603125" x2="-1.61141875" y2="0.407875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.38603125" x2="-0.999790625" y2="0.407875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.38603125" x2="0.201634375" y2="0.407875" layer="200"/>
<rectangle x1="0.98801875" y1="0.38603125" x2="1.40305" y2="0.407875" layer="200"/>
<rectangle x1="1.62149375" y1="0.38603125" x2="1.6433375" y2="0.407875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.407875" x2="-1.61141875" y2="0.42971875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.407875" x2="-0.999790625" y2="0.42971875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.407875" x2="0.201634375" y2="0.42971875" layer="200"/>
<rectangle x1="0.98801875" y1="0.407875" x2="1.40305" y2="0.42971875" layer="200"/>
<rectangle x1="1.62149375" y1="0.407875" x2="1.6433375" y2="0.42971875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.42971875" x2="-1.61141875" y2="0.4515625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.42971875" x2="-0.999790625" y2="0.4515625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.42971875" x2="0.201634375" y2="0.4515625" layer="200"/>
<rectangle x1="0.98801875" y1="0.42971875" x2="1.40305" y2="0.4515625" layer="200"/>
<rectangle x1="1.62149375" y1="0.42971875" x2="1.6433375" y2="0.4515625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.4515625" x2="-1.61141875" y2="0.47340625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.4515625" x2="-0.999790625" y2="0.47340625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.4515625" x2="0.201634375" y2="0.47340625" layer="200"/>
<rectangle x1="0.98801875" y1="0.4515625" x2="1.40305" y2="0.47340625" layer="200"/>
<rectangle x1="1.62149375" y1="0.4515625" x2="1.6433375" y2="0.47340625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.47340625" x2="-1.61141875" y2="0.49525" layer="200"/>
<rectangle x1="-1.414821875" y1="0.47340625" x2="-0.999790625" y2="0.49525" layer="200"/>
<rectangle x1="-0.213403125" y1="0.47340625" x2="0.201634375" y2="0.49525" layer="200"/>
<rectangle x1="0.98801875" y1="0.47340625" x2="1.40305" y2="0.49525" layer="200"/>
<rectangle x1="1.62149375" y1="0.47340625" x2="1.6433375" y2="0.49525" layer="200"/>
<rectangle x1="-1.6332625" y1="0.49525" x2="-1.61141875" y2="0.51709375" layer="200"/>
<rectangle x1="-1.414821875" y1="0.49525" x2="-0.999790625" y2="0.51709375" layer="200"/>
<rectangle x1="-0.213403125" y1="0.49525" x2="0.201634375" y2="0.51709375" layer="200"/>
<rectangle x1="0.98801875" y1="0.49525" x2="1.40305" y2="0.51709375" layer="200"/>
<rectangle x1="1.62149375" y1="0.49525" x2="1.6433375" y2="0.51709375" layer="200"/>
<rectangle x1="-1.6332625" y1="0.51709375" x2="-1.61141875" y2="0.5389375" layer="200"/>
<rectangle x1="-1.414821875" y1="0.51709375" x2="-0.999790625" y2="0.5389375" layer="200"/>
<rectangle x1="-0.213403125" y1="0.51709375" x2="0.201634375" y2="0.5389375" layer="200"/>
<rectangle x1="0.98801875" y1="0.51709375" x2="1.40305" y2="0.5389375" layer="200"/>
<rectangle x1="1.62149375" y1="0.51709375" x2="1.6433375" y2="0.5389375" layer="200"/>
<rectangle x1="-1.6332625" y1="0.5389375" x2="-1.61141875" y2="0.56078125" layer="200"/>
<rectangle x1="-1.414821875" y1="0.5389375" x2="-0.999790625" y2="0.56078125" layer="200"/>
<rectangle x1="-0.213403125" y1="0.5389375" x2="0.201634375" y2="0.56078125" layer="200"/>
<rectangle x1="0.98801875" y1="0.5389375" x2="1.40305" y2="0.56078125" layer="200"/>
<rectangle x1="1.62149375" y1="0.5389375" x2="1.6433375" y2="0.56078125" layer="200"/>
<rectangle x1="-1.6332625" y1="0.56078125" x2="-1.61141875" y2="0.582625" layer="200"/>
<rectangle x1="-1.414821875" y1="0.56078125" x2="-0.999790625" y2="0.582625" layer="200"/>
<rectangle x1="-0.213403125" y1="0.56078125" x2="0.201634375" y2="0.582625" layer="200"/>
<rectangle x1="0.98801875" y1="0.56078125" x2="1.40305" y2="0.582625" layer="200"/>
<rectangle x1="1.62149375" y1="0.56078125" x2="1.6433375" y2="0.582625" layer="200"/>
<rectangle x1="-1.6332625" y1="0.582625" x2="-1.61141875" y2="0.60446875" layer="200"/>
<rectangle x1="-1.414821875" y1="0.582625" x2="-0.999790625" y2="0.60446875" layer="200"/>
<rectangle x1="-0.213403125" y1="0.582625" x2="0.201634375" y2="0.60446875" layer="200"/>
<rectangle x1="0.98801875" y1="0.582625" x2="1.40305" y2="0.60446875" layer="200"/>
<rectangle x1="1.62149375" y1="0.582625" x2="1.6433375" y2="0.60446875" layer="200"/>
<rectangle x1="-1.6332625" y1="0.60446875" x2="1.6433375" y2="0.6263125" layer="200"/>
<rectangle x1="-1.75005625" y1="-0.769615625" x2="1.74498125" y2="0.764540625" layer="29"/>
<wire x1="-1.2" y1="0" x2="-1.2" y2="1.016" width="0.3048" layer="1"/>
<wire x1="-1.2" y1="-1.016" x2="-1.2" y2="0" width="0.3048" layer="1"/>
<wire x1="1.2" y1="0" x2="1.2" y2="1.016" width="0.3048" layer="1"/>
<wire x1="1.2" y1="0" x2="1.2" y2="-1.016" width="0.3048" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="1.016" width="0.4064" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="-1.016" width="0.4064" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="RESONATOR">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-2.032" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="1.778" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="3.302" y1="-3.048" x2="2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="1.778" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.048" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-3.048" x2="-2.54" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-3.302" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.048" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.032" x2="-2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-3.302" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.032" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="0" y="-7.62" visible="off" length="point" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESONATOR" prefix="Y" uservalue="yes">
<description>&lt;b&gt;Resonator&lt;/b&gt;&lt;br&gt;
Small SMD resonator.&lt;br&gt;
Used, eg, on the Arduino Pro/ Pro Mini boards.&lt;br&gt;
8MHz- XTAL-08895&lt;br&gt;
16MHz- XTAL-08900</description>
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="RESONATOR-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8MHZ" package="RESONATOR-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-08895"/>
<attribute name="VALUE" value="8MHZ"/>
</technology>
</technologies>
</device>
<device name="16MHZ" package="RESONATOR-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XTAL-08900"/>
<attribute name="VALUE" value="16MHZ"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerIC">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find drivers, regulators, and amplifiers.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.4294" x2="1.4224" y2="-0.4294" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.4294" x2="-1.4224" y2="0.4294" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.2684" y1="0.8104" x2="0.2684" y2="0.8104" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-1.4" y1="0.8" x2="-1.4" y2="-0.8" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="-0.889" y="2.159" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9525" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="PTC">
<wire x1="-3.81" y1="1.524" x2="3.81" y2="1.524" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.524" x2="3.81" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.524" x2="-3.81" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.524" x2="-3.81" y2="1.524" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" diameter="1.8796"/>
<text x="-3.81" y="1.705" size="0.4318" layer="25">&gt;Name</text>
<text x="-3.81" y="-2.14" size="0.4318" layer="27">&gt;Value</text>
</package>
<package name="PTC-1206">
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.762" x2="-0.635" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="0.635" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.143" y1="-1.016" x2="0.254" y2="1.016" width="0.127" layer="51"/>
<wire x1="0.254" y1="1.016" x2="1.143" y2="1.016" width="0.127" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1" dy="1.8" layer="1"/>
<text x="-1.524" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="0603">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="PTC-1206-WIDE">
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="0.635" y1="-0.762" x2="-0.635" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="0.635" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.143" y1="-1.016" x2="0.254" y2="1.016" width="0.127" layer="51"/>
<wire x1="0.254" y1="1.016" x2="1.143" y2="1.016" width="0.127" layer="51"/>
<smd name="1" x="-1.654" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="2" x="1.654" y="0" dx="1" dy="1.8" layer="1"/>
<text x="-1.524" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.524" y="-1.651" size="0.4064" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="V-REG-LDO">
<wire x1="-7.62" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-7.62" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.4064" layer="94"/>
<text x="-7.62" y="9.144" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-11.43" size="1.778" layer="96">&gt;VALUE</text>
<pin name="IN" x="-10.16" y="5.08" visible="pin" length="short" direction="in"/>
<pin name="GND" x="-10.16" y="0" visible="pin" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="5.08" visible="pin" length="short" direction="pas" rot="R180"/>
<pin name="EN" x="-10.16" y="-5.08" visible="pin" length="short" direction="in"/>
<pin name="BP" x="7.62" y="-5.08" visible="pin" length="short" direction="in" rot="R180"/>
</symbol>
<symbol name="PTC">
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.302" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MIC5205" prefix="U" uservalue="yes">
<description>MIC5205 150mA vreg&lt;br&gt;
3.3V - VREG-00822&lt;br&gt;
5V - VREG-00823</description>
<gates>
<gate name="G$1" symbol="V-REG-LDO" x="0" y="0"/>
</gates>
<devices>
<device name="3.3V" package="SOT23-5">
<connects>
<connect gate="G$1" pin="BP" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="VREG-00822"/>
</technology>
</technologies>
</device>
<device name="5V" package="SOT23-5">
<connects>
<connect gate="G$1" pin="BP" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="VREG-00823"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PTC" prefix="F">
<description>&lt;b&gt;Resettable Fuse PTC&lt;/b&gt;
Resettable Fuse. Spark Fun Electronics SKU : COM-08357</description>
<gates>
<gate name="G$1" symbol="PTC" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="PTC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="PTC-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-11150"/>
</technology>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-W" package="PTC-1206-WIDE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jump-0r-smd">
<description>Soldering jumper and bridges</description>
<packages>
<package name="A0R-JMP">
<description>&lt;b&gt;0R Jumper Variant A&lt;/b&gt;&lt;p&gt;
chip 0805</description>
<wire x1="0.635" y1="1.26" x2="0.635" y2="0.44" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="1.26" x2="-0.635" y2="0.44" width="0.1524" layer="51"/>
<smd name="1" x="0" y="1.7" dx="1.3" dy="1.5" layer="1" rot="R270"/>
<smd name="2" x="0" y="0" dx="1.3" dy="1.5" layer="1" rot="R270"/>
<smd name="3" x="0" y="-1.7" dx="1.3" dy="1.5" layer="1" rot="R270"/>
<text x="-0.762" y="2.716" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-3.986" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3235" y1="-0.5814" x2="0.3265" y2="0.8186" layer="51" rot="R270"/>
<rectangle x1="-0.3235" y1="0.8918" x2="0.3265" y2="2.2918" layer="51" rot="R270"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="B0R-JMP">
<description>&lt;b&gt;0R Jumper Variant B&lt;/b&gt;&lt;p&gt;
chip 0805</description>
<wire x1="-0.635" y1="-0.44" x2="-0.635" y2="-1.26" width="0.1524" layer="51"/>
<wire x1="0.635" y1="-1.26" x2="0.635" y2="-0.44" width="0.1524" layer="51"/>
<smd name="1" x="0" y="1.7" dx="1.3" dy="1.5" layer="1" rot="R270"/>
<smd name="2" x="0" y="0" dx="1.3" dy="1.5" layer="1" rot="R270"/>
<smd name="3" x="0" y="-1.7" dx="1.3" dy="1.5" layer="1" rot="R270"/>
<text x="-0.762" y="2.716" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-3.986" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3235" y1="-2.2814" x2="0.3265" y2="-0.8814" layer="51" rot="R270"/>
<rectangle x1="-0.3235" y1="-0.8082" x2="0.3265" y2="0.5918" layer="51" rot="R270"/>
</package>
<package name="C0R-JMP">
<description>&lt;b&gt;0R Jumper Variant C&lt;/b&gt;&lt;p&gt;
Solder type</description>
<wire x1="0.61" y1="0.66" x2="0.61" y2="0.34" width="0.1524" layer="51"/>
<wire x1="-0.61" y1="0.66" x2="-0.61" y2="0.34" width="0.1524" layer="51"/>
<smd name="1" x="0" y="0.938" dx="0.8" dy="1" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1" dy="0.8" layer="1" roundness="50"/>
<smd name="3" x="0" y="-0.938" dx="0.8" dy="1" layer="1" roundness="100"/>
<text x="-0.762" y="2.716" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-3.986" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3235" y1="-0.6814" x2="0.3265" y2="0.7186" layer="51" rot="R270"/>
<rectangle x1="-0.3235" y1="0.2338" x2="0.3265" y2="1.6338" layer="51" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="0RJM">
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="1.524" x2="0.254" y2="1.524" width="0.508" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.508" layer="94" curve="-180" cap="flat"/>
<wire x1="0.254" y1="1.016" x2="-0.254" y2="1.016" width="0.508" layer="94" curve="-180" cap="flat"/>
<wire x1="0.254" y1="-1.524" x2="-0.254" y2="-1.524" width="0.508" layer="94" curve="-180" cap="flat"/>
<text x="2.54" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
</symbol>
<symbol name="JMP">
<wire x1="0.762" y1="-0.254" x2="0.508" y2="-0.254" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.254" y1="-0.254" x2="-0.254" y2="-0.254" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.508" y1="-0.254" x2="-0.762" y2="-0.254" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.762" y1="-2.286" x2="0.508" y2="-2.286" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.254" y1="-2.286" x2="-0.254" y2="-2.286" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.508" y1="-2.286" x2="-0.762" y2="-2.286" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.762" y1="-0.254" x2="0.762" y2="-0.508" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.762" y1="-0.762" x2="0.762" y2="-1.016" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.762" y1="-1.524" x2="0.762" y2="-1.778" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0.762" y1="-2.032" x2="0.762" y2="-2.286" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.762" y1="-2.286" x2="-0.762" y2="-2.032" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.762" y1="-1.778" x2="-0.762" y2="-1.524" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.762" y1="-1.016" x2="-0.762" y2="-0.762" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="-0.254" width="0.1524" layer="94" style="shortdash"/>
<rectangle x1="-0.508" y1="-2.032" x2="0.508" y2="-0.508" layer="94" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0R-JUMP" prefix="JMP">
<description>&lt;b&gt;SMD 0R 0805 Jumper&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-0R" symbol="0RJM" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="JMP" x="-2.54" y="2.54" addlevel="always"/>
</gates>
<devices>
<device name="A" package="A0R-JMP">
<connects>
<connect gate="-0R" pin="1" pad="1"/>
<connect gate="-0R" pin="2" pad="2"/>
<connect gate="-0R" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="B0R-JMP">
<connects>
<connect gate="-0R" pin="1" pad="1"/>
<connect gate="-0R" pin="2" pad="2"/>
<connect gate="-0R" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C" package="C0R-JMP">
<connects>
<connect gate="-0R" pin="1" pad="1"/>
<connect gate="-0R" pin="2" pad="2"/>
<connect gate="-0R" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PAD.02X.02">
<smd name="P$1" x="0" y="0" dx="0.508" dy="0.508" layer="1"/>
</package>
<package name="PAD.03X.03">
<smd name="P$1" x="0" y="0" dx="0.762" dy="0.762" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.05">
<smd name="P$1" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
</package>
<package name="PAD.03X.04">
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100" cream="no"/>
</package>
<package name="TP_15TH">
<pad name="P$1" x="0" y="0" drill="0.381" diameter="0.6096" stop="no"/>
<circle x="0" y="0" radius="0.381" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="TEST-POINT">
<wire x1="2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.762" x2="3.302" y2="-0.762" width="0.1524" layer="94" curve="180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;Name</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;Value</text>
<pin name="1" x="0" y="0" visible="off" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEST-POINT" prefix="TP">
<description>Bare copper test points for troubleshooting or ICT</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="0" y="0"/>
</gates>
<devices>
<device name="2" package="PAD.02X.02">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3" package="PAD.03X.03">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X5" package="PAD.03X.05">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3X4" package="PAD.03X.04">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TP_15TH_THRU" package="TP_15TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0">
</class>
<class number="1" name="GND" width="0.4064" drill="0">
</class>
<class number="2" name="POWER" width="0.4064" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="SparkFun-DigitalIC" deviceset="ATMEGA328_SMT" device="" value="ATMEGA328P"/>
<part name="SUPPLY" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="3V3"/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C1" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="0.1uF"/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="10K"/>
<part name="S1" library="SparkFun-Electromechanical" deviceset="TAC_SWITCH" device="SMD" value="RESET"/>
<part name="SV1" library="SparkFun-Connectors" deviceset="M04X2" device="" value="rf24"/>
<part name="JP1" library="rveer" deviceset="USB" device="SMD"/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C4" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="4.7uF"/>
<part name="R2" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="330"/>
<part name="LED1" library="eagle-ltspice" deviceset="LED_E" device="SML0603" value="full"/>
<part name="R3" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="10K"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="4.7uF"/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="VREG" library="SparkFun-Aesthetics" deviceset="VIN" device="" value="3V3"/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="JP2" library="SparkFun-Connectors" deviceset="FTDI_BASIC" device="PTH"/>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C6" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="0.1uF"/>
<part name="C3" library="SparkFun-Capacitors" deviceset="47UF-10V-10%(TANT)" device="" value="47uF"/>
<part name="JP3" library="SparkFun-Connectors" deviceset="M02" device="POLAR" value="LION"/>
<part name="CHG1" library="rveer" deviceset="MCP73831" device=""/>
<part name="LED2" library="eagle-ltspice" deviceset="LED_E" device="SML0603" value="charging"/>
<part name="R4" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="330"/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C2" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="0.1uF"/>
<part name="SENS1" library="SparkFun-Connectors" deviceset="M03" device="POLAR"/>
<part name="SENS2" library="SparkFun-Connectors" deviceset="M03" device="POLAR"/>
<part name="SENS3" library="SparkFun-Connectors" deviceset="M03" device="POLAR"/>
<part name="GND10" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="MOT1" library="SparkFun-Connectors" deviceset="M02" device="POLAR"/>
<part name="MOT2" library="SparkFun-Connectors" deviceset="M02" device="POLAR"/>
<part name="Q2" library="SparkFun-DiscreteSemi" deviceset="TRANSISTOR_NPN" device="SOT23" value="BC817"/>
<part name="Q3" library="SparkFun-DiscreteSemi" deviceset="TRANSISTOR_NPN" device="SOT23" value="BC817"/>
<part name="GND11" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="R5" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="1K"/>
<part name="R6" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="1K"/>
<part name="D1" library="SparkFun-DiscreteSemi" deviceset="DIODE" device="SOD"/>
<part name="D2" library="SparkFun-DiscreteSemi" deviceset="DIODE" device="SOD"/>
<part name="Y1" library="SparkFun-FreqCtrl" deviceset="RESONATOR" device="8MHZ" value="8MHZ"/>
<part name="R7" library="SparkFun-Resistors" deviceset="RESISTOR" device="0603" value="330"/>
<part name="GND13" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="L3" library="eagle-ltspice" deviceset="LED_E" device="SML0603" value="yellow"/>
<part name="JP4" library="SparkFun-Connectors" deviceset="M08" device="1X08"/>
<part name="BAT1" library="SparkFun-Electromechanical" deviceset="BATTERY" device="FOB"/>
<part name="FRAME" library="frames" deviceset="DINA4_L" device=""/>
<part name="U2" library="SparkFun-PowerIC" deviceset="MIC5205" device="3.3V" value="TPS73233"/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="C7" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="0.1uF"/>
<part name="GND14" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="F1" library="SparkFun-PowerIC" deviceset="PTC" device="0603"/>
<part name="U$1" library="rveer" deviceset="LOGO_SIMPLYDUINO" device="NORMAL" value="LOGO_SIMPLYDUINONORMAL"/>
<part name="SJ1" library="jump-0r-smd" deviceset="0R-JUMP" device="A"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="4.7uF"/>
<part name="GND15" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="TP1" library="SparkFun-Passives" deviceset="TEST-POINT" device="TP_15TH_THRU" value="TP_RST"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="208.28" y="-33.02" size="2.54" layer="94">1.03</text>
</plain>
<instances>
<instance part="U1" gate="G$1" x="111.76" y="71.12"/>
<instance part="SUPPLY" gate="G$1" x="76.2" y="119.38"/>
<instance part="GND2" gate="1" x="66.04" y="33.02"/>
<instance part="C1" gate="G$1" x="58.42" y="91.44" rot="R270"/>
<instance part="GND3" gate="1" x="53.34" y="76.2"/>
<instance part="R1" gate="G$1" x="83.82" y="109.22" rot="R90"/>
<instance part="S1" gate="S" x="63.5" y="99.06"/>
<instance part="SV1" gate="G$1" x="167.64" y="15.24" rot="R180"/>
<instance part="JP1" gate="G$1" x="-30.48" y="10.16" rot="R180"/>
<instance part="GND4" gate="1" x="-15.24" y="20.32"/>
<instance part="GND5" gate="1" x="-15.24" y="-12.7"/>
<instance part="C4" gate="G$1" x="-15.24" y="0"/>
<instance part="R2" gate="G$1" x="73.66" y="-27.94"/>
<instance part="LED1" gate="G$1" x="53.34" y="-27.94" rot="R90"/>
<instance part="R3" gate="G$1" x="0" y="-2.54" rot="R180"/>
<instance part="C5" gate="G$1" x="55.88" y="2.54"/>
<instance part="GND6" gate="1" x="55.88" y="-10.16"/>
<instance part="VREG" gate="G$1" x="187.96" y="134.62"/>
<instance part="GND7" gate="1" x="170.18" y="111.76"/>
<instance part="JP2" gate="G$1" x="-30.48" y="119.38"/>
<instance part="GND8" gate="1" x="-2.54" y="129.54"/>
<instance part="C6" gate="G$1" x="71.12" y="116.84" rot="R180"/>
<instance part="C3" gate="G$1" x="187.96" y="124.46"/>
<instance part="JP3" gate="G$1" x="55.88" y="20.32" rot="R270"/>
<instance part="CHG1" gate="G$1" x="25.4" y="2.54"/>
<instance part="LED2" gate="G$1" x="35.56" y="-27.94" rot="R90"/>
<instance part="R4" gate="G$1" x="22.86" y="-27.94"/>
<instance part="GND9" gate="1" x="86.36" y="-30.48"/>
<instance part="C2" gate="G$1" x="58.42" y="81.28" rot="R270"/>
<instance part="SENS1" gate="G$1" x="203.2" y="93.98" rot="R180"/>
<instance part="SENS2" gate="G$1" x="203.2" y="76.2" rot="R180"/>
<instance part="SENS3" gate="G$1" x="203.2" y="58.42" rot="R180"/>
<instance part="GND10" gate="1" x="170.18" y="48.26"/>
<instance part="MOT1" gate="G$1" x="25.4" y="88.9" rot="R180"/>
<instance part="MOT2" gate="G$1" x="25.4" y="60.96" rot="R180"/>
<instance part="Q2" gate="G$1" x="7.62" y="78.74"/>
<instance part="Q3" gate="G$1" x="7.62" y="48.26"/>
<instance part="GND11" gate="1" x="10.16" y="38.1"/>
<instance part="GND12" gate="1" x="10.16" y="68.58"/>
<instance part="R5" gate="G$1" x="-5.08" y="48.26" rot="R180"/>
<instance part="R6" gate="G$1" x="-2.54" y="78.74" rot="R180"/>
<instance part="D1" gate="G$1" x="10.16" y="86.36" rot="R90"/>
<instance part="D2" gate="G$1" x="10.16" y="58.42" rot="R90"/>
<instance part="Y1" gate="G$1" x="81.28" y="68.58" rot="R270"/>
<instance part="R7" gate="G$1" x="205.74" y="40.64"/>
<instance part="GND13" gate="1" x="213.36" y="38.1"/>
<instance part="L3" gate="G$1" x="193.04" y="40.64" rot="R90"/>
<instance part="JP4" gate="G$1" x="20.32" y="119.38"/>
<instance part="BAT1" gate="G$1" x="170.18" y="121.92" rot="R270"/>
<instance part="FRAME" gate="G$1" x="-43.18" y="-40.64"/>
<instance part="FRAME" gate="G$2" x="119.38" y="-40.64"/>
<instance part="U2" gate="G$1" x="99.06" y="2.54"/>
<instance part="GND1" gate="1" x="83.82" y="-10.16"/>
<instance part="C7" gate="G$1" x="111.76" y="2.54"/>
<instance part="GND14" gate="1" x="111.76" y="-5.08"/>
<instance part="F1" gate="G$1" x="71.12" y="7.62"/>
<instance part="U$1" gate="G$1" x="127" y="-10.16"/>
<instance part="SJ1" gate="-0R" x="170.18" y="134.62" rot="R270"/>
<instance part="C8" gate="G$1" x="154.94" y="5.08"/>
<instance part="GND15" gate="1" x="154.94" y="-2.54"/>
<instance part="TP1" gate="G$1" x="55.88" y="109.22" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="3V3" class="2">
<segment>
<pinref part="U1" gate="G$1" pin="VCC@2"/>
<wire x1="88.9" y1="86.36" x2="76.2" y2="86.36" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCC@1"/>
<wire x1="76.2" y1="88.9" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="AVCC"/>
<wire x1="88.9" y1="91.44" x2="76.2" y2="91.44" width="0.1524" layer="91"/>
<wire x1="76.2" y1="91.44" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<junction x="76.2" y="88.9"/>
<wire x1="76.2" y1="91.44" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<junction x="76.2" y="91.44"/>
<pinref part="SUPPLY" gate="G$1" pin="VIN"/>
<wire x1="76.2" y1="116.84" x2="76.2" y2="119.38" width="0.1524" layer="91"/>
<wire x1="76.2" y1="91.44" x2="63.5" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="83.82" y1="114.3" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
<wire x1="83.82" y1="116.84" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<junction x="76.2" y="116.84"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="2"/>
<wire x1="160.02" y1="10.16" x2="154.94" y2="10.16" width="0.1524" layer="91"/>
<label x="149.86" y="10.16" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="154.94" y1="10.16" x2="149.86" y2="10.16" width="0.1524" layer="91"/>
<junction x="154.94" y="10.16"/>
</segment>
<segment>
<pinref part="VREG" gate="G$1" pin="VIN"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="187.96" y1="134.62" x2="187.96" y2="127" width="0.1524" layer="91"/>
<pinref part="SJ1" gate="-0R" pin="1"/>
<wire x1="172.72" y1="134.62" x2="187.96" y2="134.62" width="0.1524" layer="91"/>
<junction x="187.96" y="134.62"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="4"/>
<wire x1="-25.4" y1="121.92" x2="-10.16" y2="121.92" width="0.1524" layer="91"/>
<label x="-10.16" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="MOT1" gate="G$1" pin="1"/>
<wire x1="17.78" y1="88.9" x2="10.16" y2="88.9" width="0.1524" layer="91"/>
<label x="-12.7" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="10.16" y1="88.9" x2="-12.7" y2="88.9" width="0.1524" layer="91"/>
<junction x="10.16" y="88.9"/>
</segment>
<segment>
<pinref part="MOT2" gate="G$1" pin="1"/>
<wire x1="17.78" y1="60.96" x2="10.16" y2="60.96" width="0.1524" layer="91"/>
<label x="-12.7" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="D2" gate="G$1" pin="C"/>
<junction x="10.16" y="60.96"/>
<wire x1="10.16" y1="60.96" x2="-12.7" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="OUT"/>
<wire x1="106.68" y1="7.62" x2="111.76" y2="7.62" width="0.1524" layer="91"/>
<label x="119.38" y="7.62" size="1.27" layer="95" xref="yes"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="111.76" y1="7.62" x2="119.38" y2="7.62" width="0.1524" layer="91"/>
<junction x="111.76" y="7.62"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="66.04" y1="68.58" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<wire x1="66.04" y1="45.72" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<wire x1="66.04" y1="43.18" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@2"/>
<wire x1="88.9" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<junction x="66.04" y="40.64"/>
<pinref part="U1" gate="G$1" pin="GND@1"/>
<wire x1="88.9" y1="43.18" x2="66.04" y2="43.18" width="0.1524" layer="91"/>
<junction x="66.04" y="43.18"/>
<pinref part="U1" gate="G$1" pin="AGND"/>
<wire x1="88.9" y1="45.72" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<junction x="66.04" y="45.72"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="73.66" y1="68.58" x2="66.04" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="S1" gate="S" pin="1"/>
<wire x1="58.42" y1="99.06" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="53.34" y2="96.52" width="0.1524" layer="91"/>
<junction x="53.34" y="91.44"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="53.34" y1="96.52" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="55.88" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<junction x="53.34" y="81.28"/>
<pinref part="S1" gate="S" pin="2"/>
<wire x1="58.42" y1="96.52" x2="53.34" y2="96.52" width="0.1524" layer="91"/>
<junction x="53.34" y="96.52"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="1"/>
<wire x1="175.26" y1="10.16" x2="185.42" y2="10.16" width="0.1524" layer="91"/>
<label x="185.42" y="10.16" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="GND"/>
<wire x1="-27.94" y1="10.16" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="10.16" x2="-22.86" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="25.4" x2="-15.24" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="-15.24" y1="25.4" x2="-15.24" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-15.24" y="-2.54"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="48.26" y1="-2.54" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="55.88" y1="-2.54" x2="55.88" y2="0" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-2.54" x2="58.42" y2="-2.54" width="0.1524" layer="91"/>
<junction x="55.88" y="-2.54"/>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="58.42" y1="-2.54" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CHG1" gate="G$1" pin="VSS"/>
<wire x1="38.1" y1="2.54" x2="48.26" y2="2.54" width="0.1524" layer="91"/>
<wire x1="48.26" y1="2.54" x2="48.26" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-7.62" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="170.18" y1="116.84" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="170.18" y1="116.84" x2="187.96" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="-"/>
<wire x1="187.96" y1="116.84" x2="187.96" y2="119.38" width="0.1524" layer="91"/>
<pinref part="BAT1" gate="G$1" pin="-"/>
<junction x="170.18" y="116.84"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="6"/>
<wire x1="-25.4" y1="127" x2="-17.78" y2="127" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="127" x2="-10.16" y2="127" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="127" x2="-10.16" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="134.62" x2="-2.54" y2="134.62" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-2.54" y1="134.62" x2="-2.54" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="5"/>
<wire x1="-25.4" y1="124.46" x2="-17.78" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="124.46" x2="-17.78" y2="127" width="0.1524" layer="91"/>
<junction x="-17.78" y="127"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="78.74" y1="-27.94" x2="86.36" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SENS3" gate="G$1" pin="3"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="195.58" y1="55.88" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="170.18" y1="55.88" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SENS2" gate="G$1" pin="3"/>
<wire x1="195.58" y1="73.66" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<wire x1="170.18" y1="73.66" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
<junction x="170.18" y="55.88"/>
<pinref part="SENS1" gate="G$1" pin="3"/>
<wire x1="195.58" y1="91.44" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="170.18" y1="91.44" x2="170.18" y2="73.66" width="0.1524" layer="91"/>
<junction x="170.18" y="73.66"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="E"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="10.16" y1="40.64" x2="10.16" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="Q2" gate="G$1" pin="E"/>
<wire x1="10.16" y1="71.12" x2="10.16" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="213.36" y1="40.64" x2="210.82" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="88.9" y1="2.54" x2="83.82" y2="2.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="2.54" x2="83.82" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="111.76" y1="0" x2="111.76" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="154.94" y1="0" x2="154.94" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC6(/RESET)"/>
<wire x1="88.9" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="83.82" y1="96.52" x2="83.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="83.82" y1="96.52" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<junction x="83.82" y="96.52"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="71.12" y1="96.52" x2="71.12" y2="99.06" width="0.1524" layer="91"/>
<junction x="71.12" y="96.52"/>
<pinref part="S1" gate="S" pin="4"/>
<wire x1="71.12" y1="99.06" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<wire x1="71.12" y1="109.22" x2="71.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="68.58" y1="96.52" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<pinref part="S1" gate="S" pin="3"/>
<wire x1="68.58" y1="99.06" x2="71.12" y2="99.06" width="0.1524" layer="91"/>
<junction x="71.12" y="99.06"/>
<wire x1="55.88" y1="109.22" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<junction x="71.12" y="109.22"/>
<pinref part="TP1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="D13/SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB5(SCK)"/>
<wire x1="137.16" y1="38.1" x2="147.32" y2="38.1" width="0.1524" layer="91"/>
<label x="147.32" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="5"/>
<wire x1="175.26" y1="15.24" x2="185.42" y2="15.24" width="0.1524" layer="91"/>
<label x="185.42" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D12/MISO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB4(MISO)"/>
<wire x1="137.16" y1="40.64" x2="147.32" y2="40.64" width="0.1524" layer="91"/>
<label x="147.32" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="7"/>
<wire x1="175.26" y1="17.78" x2="185.42" y2="17.78" width="0.1524" layer="91"/>
<label x="185.42" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB6(XTAL1/TOSC1)"/>
<wire x1="88.9" y1="71.12" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB7(XTAL2/TOSC2)"/>
<wire x1="88.9" y1="66.04" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="D9/RF_ENABLE" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="3"/>
<wire x1="175.26" y1="12.7" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
<label x="185.42" y="12.7" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB1(OC1A)"/>
<wire x1="137.16" y1="48.26" x2="147.32" y2="48.26" width="0.1524" layer="91"/>
<label x="147.32" y="48.26" size="1.4224" layer="95" xref="yes"/>
</segment>
</net>
<net name="D8/RF_IRQ" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="8"/>
<wire x1="160.02" y1="17.78" x2="149.86" y2="17.78" width="0.1524" layer="91"/>
<label x="149.86" y="17.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB0(ICP)"/>
<wire x1="137.16" y1="50.8" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<label x="147.32" y="50.8" size="1.4224" layer="95" xref="yes"/>
</segment>
</net>
<net name="VUSB" class="2">
<segment>
<pinref part="JP1" gate="G$1" pin="VBUS"/>
<wire x1="-27.94" y1="7.62" x2="-22.86" y2="7.62" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="7.62" x2="-15.24" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="7.62" width="0.1524" layer="91"/>
<junction x="-22.86" y="7.62"/>
<pinref part="CHG1" gate="G$1" pin="VDD"/>
<wire x1="-22.86" y1="-27.94" x2="-22.86" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="91"/>
<junction x="-15.24" y="7.62"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="17.78" y1="-27.94" x2="-22.86" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EXT_RESET" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="114.3" x2="-10.16" y2="114.3" width="0.1524" layer="91"/>
<label x="-10.16" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="71.12" y1="119.38" x2="71.12" y2="124.46" width="0.1524" layer="91"/>
<label x="71.12" y="124.46" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="D1/TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD1(TXD)"/>
<wire x1="137.16" y1="71.12" x2="147.32" y2="71.12" width="0.1524" layer="91"/>
<label x="147.32" y="71.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="116.84" x2="-10.16" y2="116.84" width="0.1524" layer="91"/>
<label x="-10.16" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A0/SENS1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC0(ADC0)"/>
<wire x1="137.16" y1="96.52" x2="147.32" y2="96.52" width="0.1524" layer="91"/>
<label x="147.32" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SENS1" gate="G$1" pin="1"/>
<wire x1="195.58" y1="96.52" x2="193.04" y2="96.52" width="0.1524" layer="91"/>
<label x="193.04" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A1/SENS2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC1(ADC1)"/>
<wire x1="137.16" y1="93.98" x2="147.32" y2="93.98" width="0.1524" layer="91"/>
<label x="147.32" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SENS2" gate="G$1" pin="1"/>
<wire x1="195.58" y1="78.74" x2="193.04" y2="78.74" width="0.1524" layer="91"/>
<label x="193.04" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A2/SENS3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC2(ADC2)"/>
<wire x1="137.16" y1="91.44" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<label x="147.32" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SENS3" gate="G$1" pin="1"/>
<wire x1="195.58" y1="60.96" x2="193.04" y2="60.96" width="0.1524" layer="91"/>
<label x="193.04" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC3(ADC3)"/>
<wire x1="137.16" y1="88.9" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<label x="147.32" y="88.9" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="8"/>
<wire x1="25.4" y1="129.54" x2="33.02" y2="129.54" width="0.1524" layer="91"/>
<label x="33.02" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC4(ADC4/SDA)"/>
<wire x1="137.16" y1="86.36" x2="147.32" y2="86.36" width="0.1524" layer="91"/>
<label x="147.32" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="7"/>
<wire x1="25.4" y1="127" x2="33.02" y2="127" width="0.1524" layer="91"/>
<label x="33.02" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC5(ADC5/SCL)"/>
<wire x1="137.16" y1="83.82" x2="147.32" y2="83.82" width="0.1524" layer="91"/>
<label x="147.32" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="6"/>
<wire x1="25.4" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<label x="33.02" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ADC6"/>
<wire x1="137.16" y1="81.28" x2="147.32" y2="81.28" width="0.1524" layer="91"/>
<label x="147.32" y="81.28" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="5"/>
<wire x1="25.4" y1="121.92" x2="33.02" y2="121.92" width="0.1524" layer="91"/>
<label x="33.02" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="ADC7"/>
<wire x1="137.16" y1="78.74" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<label x="147.32" y="78.74" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="4"/>
<wire x1="25.4" y1="119.38" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<label x="33.02" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D0/RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD0(RXD)"/>
<wire x1="137.16" y1="73.66" x2="147.32" y2="73.66" width="0.1524" layer="91"/>
<label x="147.32" y="73.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="3"/>
<wire x1="-25.4" y1="119.38" x2="-10.16" y2="119.38" width="0.1524" layer="91"/>
<label x="-10.16" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D4/SENSOR_ENABLE" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD4(XCK/T0)"/>
<wire x1="137.16" y1="63.5" x2="147.32" y2="63.5" width="0.1524" layer="91"/>
<label x="147.32" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SENS3" gate="G$1" pin="2"/>
<wire x1="195.58" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<label x="193.04" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SENS2" gate="G$1" pin="2"/>
<wire x1="195.58" y1="76.2" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
<label x="193.04" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SENS1" gate="G$1" pin="2"/>
<wire x1="195.58" y1="93.98" x2="193.04" y2="93.98" width="0.1524" layer="91"/>
<label x="193.04" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD5(T1)"/>
<wire x1="137.16" y1="60.96" x2="147.32" y2="60.96" width="0.1524" layer="91"/>
<label x="147.32" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="3"/>
<wire x1="25.4" y1="116.84" x2="33.02" y2="116.84" width="0.1524" layer="91"/>
<label x="33.02" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD6(AIN0)"/>
<wire x1="137.16" y1="58.42" x2="147.32" y2="58.42" width="0.1524" layer="91"/>
<label x="147.32" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="2"/>
<wire x1="25.4" y1="114.3" x2="33.02" y2="114.3" width="0.1524" layer="91"/>
<label x="33.02" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD7(AIN1)"/>
<wire x1="137.16" y1="55.88" x2="147.32" y2="55.88" width="0.1524" layer="91"/>
<label x="147.32" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="L3" gate="G$1" pin="A"/>
<wire x1="190.5" y1="40.64" x2="185.42" y2="40.64" width="0.1524" layer="91"/>
<label x="185.42" y="40.64" size="1.4224" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="1"/>
<wire x1="25.4" y1="111.76" x2="33.02" y2="111.76" width="0.1524" layer="91"/>
<label x="33.02" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="AREF"/>
<wire x1="88.9" y1="81.28" x2="63.5" y2="81.28" width="0.1524" layer="91"/>
<label x="71.12" y="81.28" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="CHG1" gate="G$1" pin="PROG"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="5.08" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STAT" class="0">
<segment>
<pinref part="CHG1" gate="G$1" pin="STAT"/>
<wire x1="38.1" y1="-2.54" x2="45.72" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-2.54" x2="45.72" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="45.72" y1="-27.94" x2="50.8" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="45.72" y1="-27.94" x2="40.64" y2="-27.94" width="0.1524" layer="91"/>
<junction x="45.72" y="-27.94"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-27.94" x2="27.94" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="58.42" y1="-27.94" x2="68.58" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="B"/>
<wire x1="5.08" y1="78.74" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="B"/>
<wire x1="5.08" y1="48.26" x2="0" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="Q3" gate="G$1" pin="C"/>
<pinref part="MOT2" gate="G$1" pin="2"/>
<wire x1="10.16" y1="53.34" x2="10.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="10.16" y1="55.88" x2="17.78" y2="58.42" width="0.1524" layer="91"/>
<junction x="10.16" y="55.88"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="MOT1" gate="G$1" pin="2"/>
<pinref part="Q2" gate="G$1" pin="C"/>
<wire x1="17.78" y1="86.36" x2="10.16" y2="83.82" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<junction x="10.16" y="83.82"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="C"/>
<wire x1="200.66" y1="40.64" x2="198.12" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D10/RF_SELECT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB2(SS/OC1B)"/>
<wire x1="137.16" y1="45.72" x2="147.32" y2="45.72" width="0.1524" layer="91"/>
<label x="147.32" y="45.72" size="1.4224" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="4"/>
<wire x1="160.02" y1="12.7" x2="149.86" y2="12.7" width="0.1524" layer="91"/>
<label x="149.86" y="12.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D11/MOSI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB3(MOSI/OC2)"/>
<wire x1="137.16" y1="43.18" x2="147.32" y2="43.18" width="0.1524" layer="91"/>
<label x="147.32" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="6"/>
<wire x1="160.02" y1="15.24" x2="149.86" y2="15.24" width="0.1524" layer="91"/>
<label x="149.86" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD3(INT1)"/>
<wire x1="137.16" y1="66.04" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
<label x="147.32" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="78.74" x2="-10.16" y2="78.74" width="0.1524" layer="91"/>
<label x="-10.16" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD2(INT0)"/>
<wire x1="137.16" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<label x="147.32" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="48.26" x2="-12.7" y2="48.26" width="0.1524" layer="91"/>
<label x="-12.7" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VIN" class="2">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="38.1" y1="7.62" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<junction x="55.88" y="7.62"/>
<label x="45.72" y="20.32" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="JP3" gate="G$1" pin="1"/>
<wire x1="55.88" y1="7.62" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CHG1" gate="G$1" pin="VBAT"/>
<wire x1="45.72" y1="7.62" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<wire x1="55.88" y1="7.62" x2="66.04" y2="7.62" width="0.1524" layer="91"/>
<wire x1="45.72" y1="7.62" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<junction x="45.72" y="7.62"/>
<pinref part="F1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="SJ1" gate="-0R" pin="3"/>
<wire x1="167.64" y1="134.62" x2="157.48" y2="134.62" width="0.1524" layer="91"/>
<label x="157.48" y="134.62" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VIN_PROTECTED" class="2">
<segment>
<pinref part="U2" gate="G$1" pin="EN"/>
<pinref part="U2" gate="G$1" pin="IN"/>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="78.74" y1="7.62" x2="81.28" y2="7.62" width="0.1524" layer="91"/>
<wire x1="81.28" y1="7.62" x2="88.9" y2="7.62" width="0.1524" layer="91"/>
<junction x="81.28" y="7.62"/>
<wire x1="81.28" y1="7.62" x2="81.28" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-2.54" x2="88.9" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<pinref part="SJ1" gate="-0R" pin="2"/>
<wire x1="170.18" y1="127" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="D-"/>
<wire x1="-27.94" y1="5.08" x2="-25.4" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="5.08" x2="-25.4" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="D+"/>
<wire x1="-27.94" y1="2.54" x2="-27.94" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="202,1,106.68,-2.54,U2,BP,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
