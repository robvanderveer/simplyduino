Simplyduino
===========

- Notable changes
- Documentation
	- hardware
		- power
		- nrf
		- programming
	- software
		- introduction
		- installation
		- data
- References

##Notable changes that affect your configuration
May 5, 2015

- [Site] Database changed (added LogLevel and NotificationSubject)
- [Site] .htaccess changed (added .css, .woff and .ttf to the exclusion list)
- [Firmware] Both client and hub now use the flash storage to keep configuration. This was necessary to avoid overwrite of the client ID's and to remove the passwords from configuration files in the repo. To reset to configuration, connect pin A6 to ground on the client. The hub has a special configuration console, ground A0 to get access.

Apr 24,2015

- [Site] /index has been moved to /dashboard as a workaround for a conflict with the /index.php page mapping. Both '/' and '/dashboard' now point to the same page and /index is no longer available.
- [Site] setup.php has been moved to /configure (run from the web).
- [Firmware] now uses readVcc to send out a #35 sensor reading for voltage level.
- [Firmware] changed the update frequency to 16 seconds (was 8)

For detailed (undocumented) changes check the repository history.

#Documentation

##Hardware

Say hello to SimplyDuino. I needed a low power board that would be able transmit sensor readings to a central hub, using a low cost NRF24L01 RF transmitter. After a few prototypes and iterations this is the result. The Sensoduino was developed with a simple goal in mind: set up a local sensor network to collect metrics about stuff. Plant water, temperature, mailbox, doors open, etc.

- ATMega328p with 32kb flash and 8kb ram
- Compatible with Arduino 1.6.2 IDE (select Pro Micro 8Mhz)
- On board single cell LiPo charger
- 3v3 LDO regulator 150mA
- 250mA surge protector and reverse voltage protection
- onboard socket for low cost NRF24L01 wireless connection
- Choose between 3v coin cell or 3.7v LiPo battery via VIN
- Onboard FDTI socket for sketch uploading, boot loader can be uploaded through the NRF24L01 socket
- USB charging with status leds (charging, full)
- 3 INPUT sockets (selective power, analog in, gnd)
- 2 OUTPUT sockets (VIN switched by transistor, protected by diode)
- 8 GPIO pins (A3-A7, D5-D7)
- 1x signaling led (D7)

Although the board is designed for a special purpose, some of the features make this board usable for other situations. Because of its small form factor, built in switching transistors and onboard power supply it has a few advantages over a bare Arduino pro micro for example.

If you want to make full use of the D7 pin (as input pin), you need to desolder the signalling led because it influences the load on the pin. The led L1 is located on the top-right of the board, just below the S1 connector.

The battery holder is included, but you would have to solder it yourself as it is optional to use the CR2032 as a main power supply on the reverse side of the board.

For articles and information, check the website: [http://www.simplicate.info/simplyduino]()

##Power
Simplyduino can be powered directly through a single LiPo cell. The USB connector is connected to a charging circuit that feeds the VIN segment of the board. This segment is fed into a low dropout 3.3v regulator to power the rest of the board.

>Note: the usb connector is only used for charging. It cannot be used to upload a sketch.

It is possible to use the onboard coin cell holder. Using a 0Ω resistor. Connecting the two left-most pads will configre the cell holder for a rechargable LR2032 (4.2v) while connecting the right-most pads will configure for an unregulated CR2032 (3.3v) battery.

##NRF24L01+
The 2x4 header is designed to accept a low-cost NRF24L01+ 2.4Gghz module. This module will be destroyed if you put over 3.6v on the VCC line, which is why you should be careful when powering the board or programming it with an FTDI programmer. Play on the safe side and use a 3.3v FTDI programmer.

The Simplyduino board has room for an optional 0805 capacitor next to the VCC pin of the NRF24L01+ module. A 10µF capacitor seems to improve signal quality.

##Programming
The /arduino folder contains a folder structure with a boards.txt folder to be used with the Arduino environment. Just add the library to your IDE and select 'Simplyduino' from the `Tools`>`Board` menu.

The board should have a bootloader (optiboot) but flashing the bootloader can be done through the ISP pins which are available on the NRF24L01+ header: 3V3, GND, MOSI, MISO, SCK. The RESET line is not directly available as a pin, so for loading the boatloader you will have to manually press RESET or connect a pin to the RESET test point (just left of the CPU).

After the bootloader has been programmed, new sketches can be uploaded with a 3.3V/5V USBtoSerial FDTI module, for example [this one from SparkFun](https://www.sparkfun.com/products/9873). Just make sure you have it set to 3.3V or things will smoke. The pinout should be compatible with any FDTI uploader.

The boards.txt has 2 modes: normal programming through the FTDI pins and the `Upload` button, or flashing with the ISP programmer using `Upload with Programmer`. For flashing a bootloader, the ISP is always required.

Pin | Function | Comment
----|----------|-------
1   | GND
2   | GND
3   | 3.3V | Output from the regulator
4   | TXD | connects to RX on the ATMega
5   | RXD | connects to TX on the ATMega
6   | DTR | inline 0.1µF

The DTR pin is connected to the ATmega328 reset line through a 0.1µF capacitor, so autoreset should for just fine. Otherwise, press the reset button on the SimplyDuino while uploading a sketch.

##Sensoduino firmware
The get data from the sensoduino to a master server i chose to use the low cost NRF24L01+ module. One of the limitations of this module is that a single module can only listen to 5 unique devices at the same time. So inspired by others (citation needed) i implemented a protocol on top of the lowlevel IO library to allow up to 255 devices sending data. All the details are described in the [protocol.md documentation](protocol.md).

###SensoriumHub
Todo
###SensoriumClient
Todo

##Sensorium website
To demonstrate the use of Simplyduino a sensor node web application was developed.

The site provides a api GET request endpoint to send in sensor data. New nodes are automatically added to the database. By configuring triggers, the user can receive email notifications when the sensor data reaches a particular value (too high, too low, equal, etc).

I'm not used to writing in PHP so it may look a little spartan to people with PHP experience. The code consists of a simple one-script-one-page code with a few include files and a set of helper classes. MVC reimplementation is planned in a future iteration.

###Installation

> **important**: The code requires mySQL 5.6.3 or higher because it makes use of fractional seconds on a DateTime field. This was introduced in 2012 so if you haven't updated your mySQL in 3 years, now is a good time.

- modify .htaccess to set the `RewriteBase`. If you are running in a subdirectory on the webserver this will be required. For example, if you connect to `http://www.website.com/sensorium/index` you need to set this value to `/sensorium`. Otherwise, just specify `RewriteBase /`
- copy `config-template.php` to `config.php` and adjust the parameters. If you mySQL runs on the webserver box you probably will keep the defaults.
- navigate to `/configure`. This verifies the connection and allows you to create the database and the user that you specified in config.php. Specify root credentials (or another administrative account) to create the tables when asked.
- After the setup is complete, navigate to `settings` and create a configuration entry.

> **note**: The setup script is not yet capable of updating an existing database. The best solution for now is drop the database entirely and run `/configure` again.

###Crontab:
Sensorium needs to be called regularly to update triggers and notifications. Add a row to your crontab to call the `schedule.php` script of the site directly from the commandline. To edit your crontab on OS X for example, enter the following command in `Terminal`.

	> crontab -e

Add the following line:

	*/1 * * * * php /[FULL PATH]/Sensorium/schedule.php >>/tmp/schedule.log

`/tmp/schedule.log` is where you can monitor the output. This may not be necessary once the system is properly running, because all script errors will be saved to the Sensorium error log `log.php`.

###Getting the data in

To insert data from any client, simply send a http request with the following pattern:

	/api/v1/addReading/[NodeID]/[MetricType]/[Value]?apiKey=[APIKey]

where
`NodeID` is a 8-digit hexadecimal ID
`MetricType` is one of the predefines metric type (see below)
`Value` is any positive integer (0..1023 tested only)
`APIKey` is the key that was specified in `settings.php` when you first configured the site.

###MetricTypes
Metric types are used to give a little contexts to a reading. Because a node can provide multiple readings, a way was needed to specify what a single value means. So the metricType was introduced and it is simply a mapping of a number to a name. For example metricType `11` is a battery Level and `34` is a Soil moisture level. See below for a list of predefined metric types.

>note: metric types are not populated by the setup script yet.

The setup script should insert the default metric types automatically.

>note 2: It would be handy to allow automatic registration of metricTypes, "Unknown metric". Currently, unknown metricType readings are rejected.

#References
- none so far.
