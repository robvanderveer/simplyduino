Sensoduino network protocol
===========================

The nRF24L01+ module allows a single receiver to listen (and confirm) to up to 5 transmitting units. As this seriously limits the possibilities of an extensive sensor network i had to investigate other possibilities.

###Braindump
As it turn out (reference needed) this limit only applies because of the AUTO ACK feature of the receiving end. If you do without the built in ACK you can have as many nodes as you want (obviously introducing a little congestion issues, but more about that later).

In short, all client NRFs will have the network ID and firmware. There is only on NRF acting as a hub to receive (and confirm) all the data.

This protocol introduces a new ID named a `sensorID` which goes from 00-FF allowing 255 nodes which should be sufficient. You could always add more hubs if 255 is not enough.

So all the clients share the same networkID but have a different SensorID. The hub has a unique (configured) networkID but obviously no requirement for a sensorID.

> Using a handshake protocol (to be determined) we could even factor out the networkIDs and sensorIDs in a later phase of the project. Not a priority

Clients write to port `[00000000]` and listen on port `[01010101]` for example. To save power, they only will listen for packet confirmations because all other transmissions are irrelevant for the client. After the confirmation has been received, the client can return to power save mode.

To allow a certain level of reliability we need to confirm the proper transmission and reception of data. This will be done by basic ACK pattern where the hub confirms a packet by broadcasting the packet number with the `sensorID` and a `ACK` bit set. This will inform the client that the packet has been received. If the client does not receive an `ACK` packet it will simply resend the packet until a timeout occurs. This introduces an edge case where the `ACK` was sent but not received by the client. The client would then resend the same packet which was already processed by the hub. To handle this situation, the hub will keep the last packet number received per client so that a previously acknowlegded packet can be discarded. Note that these discarded packets will still be confirmed with an `ACK` response.

Clients receiving a packet will check the clientID for a match and disregard everything else.

As of this version of the protocol, clients can only receive ACK packets. Because of this, the packet does not need an ACK indication (which saves space).

> This protocol uses different pipes for send and receive.

###Packet size
NRF packets can be up to 32bytes (needs reference), but the payload of the sensor network does not need to be this big.

This is the payload header:

receiverID | senderID | packetNumber | retries | flag bits|total
----|----|----|----|---|----
0x00-0xFF|0x00-0xFF|0-255|(0-15) 4 bits|4 bits|4 bytes

As you can see the packet header can be as small as 3 bytes.  For example, the distinguishing of senderIDs and receiverID is not necessery because one of the two is alway 0x00 (the hub). Obviously less is more here but i'm saving some data for redundancy and future.

The packet number is a roll-over number to distinguish unique packets per client. The number is used in the ACK protocol as described earlier. We only need the last confirmed packet number to verify if we alread confirm the packet number in case we receive a retry package.

The 4 flag bits are interesting. I have no purpose for them yet. I did reserve them because I don't think more than 15 retries is doing any good.

> With the 4 bytes occupied by the header this leaves us with 32-4=28 maximum payload. It depends on the implementation to determine the exact payload size.


###Handing out clientIDs
The clientID are stored in the EEProm memory of the client. Initially they are 00 (no clientID) and we need to come up with a concept to allow the generation of a new client ID. This obviously cannot be done without the help from the hub.

Step 1, the client will report to the hub (with clientID 00)
Step 2, the hub will somehow determine a new clientID (perhaps by using the ACK table)
Step 3, the hub will ACK the 00-packet with a new clientID to use.

The above protocol will introduce a race condition when two or more clients request a new clientID at the same time.

###Pseudo code

Client

	radio.openWrite(0x00000000);
	radio.openRead(0x10101010);

	packet.senderId = selfId;
	packet.targetId = 0x00;		//hub
	packet.packetNumber = packetNumber++;
	packet.retries = 0;
	packet.flags = 0x00;
	packet.data = ....;

	radio.write(packet);
	radio.startListening();

	start Timer;
	do
	{
		if(packetWaiting)
		{
			read incoming;
			if(incoming packet for me)
			{
				if(incoming packet is ack for packetNumber)
				{
					radio.stopListening();
					//packet confirmed, exit loop.
					return true;
				}
			}
		}

		if(timeOut)
		{
			if(packet.retries < 15)
			{
				packet.retries++;
				radio.write(packet);	//send again.
			}
			else
			{
				//ERROR: packet send retry overflow.
				radio.stopListening();
				return false;
			}
		}

	}
	until /*packet confirmed or error */

Server pseudocode is different, for the lack of retry code and the ACK table maintenance. This code assumes all clientIds have been determined (non-0).

	radio.openRead(0x00000000);
	radio.openWrite(0x10101010);

	unsigned packets[256];

	for(ever)
	{
		if(packetWaiting)
		{
			read incoming;
			if(/* packet is for me */)
			{
				//send ack always.
				ackPacket = new Packet;
				ackPacket.senderId = 0x00;
				ackPacket.targetId = incoming.senderId;
				ackPacket.retries = 0;	//always 0;
				ackPacket.flags = ACK_PACKET;

				//no payload.
				radio.write(ackPacket);

				latestPacketId = packets[incoming.clientId];
				if(incoming.packetNumber != lastPacketId)
				{
					//we did not receive this packet yet.
					packets[incoming.clientId] = incoming.packetNumber;

					processPacket(incoming);
				}
			}
		}
		else
		{
			//no packet waiting
		}
	}


###Congestion issues
As long as 'retry' mechanism is developed, congestion is not a problem. The intended frequency of the transmisions is so low it will not cause any frequent conflicts. If conflicts become an issue, a randomized delay can be used to compensatie for race conditions.
