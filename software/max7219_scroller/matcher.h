/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

 /* maintains a state to indicate received matches a particular string without otherwise touching the input stream. */
 class Matcher
 {
     private:
         const char *_currentMatch;
         uint8_t _currentMatchPosition;
     public:
         Matcher()
         {
         }

         void beginMatch(const char *pattern)
         {
             _currentMatch = pattern;
             _currentMatchPosition = 0;
         }

         bool isMatch(char c)
         {
             if(_currentMatch == NULL)
             {
                 //no active pattern
                 return false;
             }

             if(c == _currentMatch[_currentMatchPosition])
             {
                 //this character matches.

                 _currentMatchPosition++;
                 //did we reach the end of the pattern?
                 if(_currentMatch[_currentMatchPosition] == '\0')
                 {
                     _currentMatch = NULL;
                     return true;
                 }
             }
             else
             {
                 //reset the pattern.
                 _currentMatchPosition = 0;
             }

             return false;
         }
 };
