
#include <SPI.h>
#include <SoftwareSerial.h>
#include <avr/eeprom.h>

#include "font5x7.h"
#include "timedEvent.h"
#include "simplyESP.h"

const uint8_t NUM_DISPLAYS  = 4;

const uint8_t PIN_SS = 10;
const uint8_t PIN_ESP_RX = 5;
const uint8_t PIN_ESP_TX = 6;
const uint8_t PIN_CONFIGURE = A3;

int offset = 0;
int msgPos = 0;

timedEvent scrollEvent;

typedef struct
{
  char ssid[32];
  char pass[32];
  char host[32];
  char message[256];
  bool debug;
} Configuration;

Configuration settings;

SoftwareSerial espSerial(PIN_ESP_RX, PIN_ESP_TX);
SimplyESP esp(&espSerial, &Serial);

void setup() 
{
  Serial.begin(9600);
  Serial.println("Simplicate.info Info Ticker v0.1 (C)2015");
  SPI.begin();
  
  delay(200);
  
  espSerial.begin(9600);
  displayInit();

  printChar(0, '0');
  updateBitmap();
    
  
  
  //load the configuration from EEPROM
  eeprom_read_block((void*)&settings, (void *)0, sizeof(settings));

  //jump A0 to GND to program the Wifi Details through the serial port
  pinMode(PIN_CONFIGURE, INPUT_PULLUP);
  if(digitalRead(PIN_CONFIGURE) == LOW)
  {
     //opens up the serial configuration screen.
    startConsole(&Serial);
  }
  else
  {
    Serial.println(F("Enter configuration mode by connecting A3 to ground and reset."));
  }

  clearBitmap();
  printChar(0, '1');
  updateBitmap();
  
  //cleared the screen, turn on the displays
  displayCommand(12, true);
  
  if(esp.reset())
  {
    printChar(0, 'C');
    updateBitmap();
    esp.connect(settings.ssid, settings.pass);
     
    printChar(0, 'R');
    updateBitmap();
 
    //get the message.
    char cmd[128];
    sprintf_P(cmd, PSTR("GET /motd.php HTTP/1.0\r\nHost: %s\r\nUser-Agent: Sensoduino-Ticket\r\nConnection: close\r\n\r\n"), settings.host);
    esp.beginRequest(settings.host, 80, cmd);
  }
  else
  {
    printChar(0, 'E');
    updateBitmap();
    Serial.println("ESP not responding to reset.");
  }
}

void loop()
{
  updateScroller();
  
  if(esp.updateRequest())
  {
    //get the buffer.
    int count = esp.getResponse(settings.message, 255);
    settings.message[count] = 0;
  }
}

void updateScroller()
{
  //draw and scroll.
  int x = -offset;
  for(int pos = 0; settings.message[pos+msgPos] != '\0' && x < NUM_DISPLAYS*8; pos++)
  {
    char c = settings.message[pos + msgPos]; 
    x += printChar(x, c);
  }
  updateBitmap();
  
  if(scrollEvent.hasElapsed(24))
  {
     if(++offset > 5)
      {
        offset = 0;
        msgPos++;
        
        if(settings.message[msgPos] == 0)
        {
          msgPos = 0;
        }
      }
  }  
}
