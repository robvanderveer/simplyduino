class httpResponse
{
  private:
    char *_buffer;
    int _bufferPos;
    int _statusCode;
    char _reason[32];

    enum {
      Status = 0,
      Headers,
      Body
    } _state;

    enum
    {
      Default,
      Cr,
      Lf
    } _eolState;

  public:
    httpResponse(char *buffer)
    {
      _buffer = buffer;
      reset();
    }

    void reset()
    {
      _state = Status;
      _bufferPos = 0;
      _eolState = Default;
    }

    int getBufferSize()
    {
      return _bufferPos;
    }

    bool add(char c)
    {
      _buffer[_bufferPos++] = c;
      _buffer[_bufferPos] = 0;

      switch (_eolState)
      {
        case Default:
          if (c == '\r')
          {
            _eolState = Cr;
          }
          break;
        case Cr:
          if (c == '\n')
          {
            //_eolState = Lf;

            processBuffer();

            _bufferPos = 0;
            _eolState = Default;
          }
          else
          {
            _eolState = Default;
          }
          break;
      }
    }

    void processBuffer()
    {
      switch (_state)
      {
        case Status:
          if (sscanf_P(_buffer, PSTR("HTTP/1.1 %d %s"), &_statusCode, _reason) == 2)
          {
            char sbuf[32];
            sprintf(sbuf, "> Status = %d, Reason = %s", _statusCode, _reason);
            Serial.println(sbuf);
          }
          else
          {
            Serial.println("> Response Header error");
          }
          _state = Headers;
          break;
        case Headers:
          if (_bufferPos == 2)
          {
            //empty line, switch to body parsing
            Serial.print("> Header done");
            _state = Body;
          }
          else
          {
            //ignore headers.
            Serial.print("> Ignore header :");
            Serial.print(_buffer);
          }
          break;
        case Body:
          //might never get here if the body does not contain CRLF
          break;
      }
    }
};
