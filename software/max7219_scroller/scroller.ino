

const uint8_t font_height = 8;
const uint8_t character_width = 5;

// raw bitmap
// 0000000088888888
// 1111111199999999
// 22222222aaaaaaaa
// .....
// 77777777ffffffff
// it is displayed upside down.
uint8_t bitmap[4 * 8]; //4 screen of 8 bytes bitmap.

/** sends an SPI command to all 4 devices by abusing the Shifter register */
void displayCommand(uint8_t code, uint8_t value)
{
  digitalWrite(PIN_SS, LOW);
  for (int i = 0; i < NUM_DISPLAYS; i++)
  {
    SPI.transfer(code);
    SPI.transfer(value);
  }
  digitalWrite(PIN_SS, HIGH);  //latch all 4 commands at once.
}

void displayInit()
{
  displayCommand(15, 0);    //OP_DISPLAYTEST
  displayCommand(11, 7);    //OP_SCANLIMIT
  displayCommand(9, 0);     //OP_DECODEMODE
  displayCommand(12, false);    //OP_SHUTDOWN, turn display off (note the opposite value)
  displayCommand(10, 1);    //OP_INTENSITY
}

void clearBitmap()
{
  for (int i = 0; i < NUM_DISPLAYS * 8; i++)
  {
    bitmap[i] = 0;
  }
}

//refresh the screen.
void updateBitmap()
{
  //banging the bus.
  for (int row = 0; row < 8; row++)
  {
    digitalWrite(PIN_SS, LOW);
    for (int i = 0; i < NUM_DISPLAYS; i++)
    {
      SPI.transfer(1 + row);
      SPI.transfer(bitmap[(3 - i) * 8 + row]);
    }
    digitalWrite(PIN_SS, HIGH);
  }

}

uint8_t printChar(int pos, char c)
{
  for (int col = 0; col < 6; col++)
  {
    if (pos + col >= 0 && pos + col < NUM_DISPLAYS*8)
    {
      uint8_t line = 0;
      if (col < 5)
      {
        line = pgm_read_byte(font5x7 + ((c - 32) * character_width) + col);
      }

      //line is vertical character.
      for (int row = 0; row < 8; row++)
      {
        uint8_t x = col + pos;

        //map col+x, row -> bitmap offset.
        uint8_t colByte = x & 0b11111000;
        uint8_t offset = colByte + (7 -row);

        uint8_t bitValue = 1 << (x & 0x7);
        if (line & 0x01)
        {
          bitmap[offset] |= bitValue;
        }
        else
        {
          bitmap[offset] &= ~bitValue;
        }
        line >>= 1;
      }
    }

  }
  return 6;
}

