/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

#include <SPI.h>
#include <RF24.h>
#include "printf.h"

#include "myRF.h"

//
// Hardware configuration: first MSP430, then ATMega
//
#define CE	9
#define CS	10
#define BAUD	57600

RF24 radio(CE, CS);
myRFServer server(&radio);


typedef struct 
{
  myRFPacketHeader header;
  uint16_t value;
  uint8_t metricId;
} SensoduinoPacket;


void setup(void)
{

  //
  // Print preamble
  //

  Serial.begin(BAUD);
  printf_begin();
  printf("HUB test\n\r");

  //
  // Setup and configure rf radio
  //

  radio.begin();
  //radio.setRetries(15, 15);
  // radio.setPayloadSize(8);
  radio.setDataRate( RF24_250KBPS ) ;
  radio.setPALevel( RF24_PA_MAX ) ;
  radio.enableDynamicPayloads() ;
  radio.setAutoAck( false ) ;
  //radio.setChannel(13);
  
  const unsigned long long readingPipe = 0x6666666618LL;
  const unsigned long long writingPipe = 0x66666666C0LL;

  radio.openWritingPipe(writingPipe);
  radio.openReadingPipe(1, readingPipe);

  radio.powerUp() ;
  radio.startListening();
}


void dumpPacket(void *data, size_t dataSize)
{
  uint8_t *packet = (uint8_t *)data;
  for(uint8_t i = 0; i < dataSize; i++)
  {
    char sbuf[4];
    sprintf(sbuf, "%02x ", packet[i]);
    Serial.print(sbuf);
  }
  Serial.println();
}

SensoduinoPacket buffer;

void onPacketReceived()
{
//   dumpPacket(&buffer, sizeof(SensoduinoPacket));
   printf("[S%d,#%d/%d] %d = %d \n", buffer.header.senderId, buffer.header.packetNumber, buffer.header.retryFlags, buffer.metricId, buffer.value);
}

void loop(void)
{

  server.execute(&buffer, sizeof(SensoduinoPacket), onPacketReceived);
}


