/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

class myRFClient
{
  private:
    RF24 *_radio;
    uint8_t _maxRetries;
    unsigned long _timeout;
    Stream *_debug;
    unsigned int _errors = 0;
    unsigned int _packetsTimedOut = 0;
    unsigned int _packets = 0;
    unsigned int _packetsConfirmed = 0;

  public:
    myRFClient(RF24 *radio, uint8_t maxRetries, unsigned long timeout, Stream *debug)
    {
      _radio = radio;
      _maxRetries = maxRetries;
      _timeout = timeout;
      _debug = debug;
    }

    void showStatistics()
    {
        if(_debug)
        {
            char buf[64];
            int rate = ((float)_packetsConfirmed * 100) / _packets;
            sprintf_P(buf, PSTR("P: %d, T: %d, E: %d, D: %d, Rate %d%%"), _packets, _packetsTimedOut, _errors, _packetsConfirmed, rate);
            _debug->println(buf);
            _debug->flush();
        }
    }

    bool sendPacket(void *packet, size_t packetSize)
    {
        myRFPacketHeader *header = (myRFPacketHeader *)packet;

        uint8_t attempt = 0;
        bool confirmed = false;

        _packets++;

        do
        {
          header->retryFlags = attempt;
          _radio->write( packet, packetSize);
          _radio->startListening();

          //start listening for ACK packet.
          unsigned long start = millis();
          while((millis() - start) < _timeout)
          {
            //use the reading pipe, which is #1 as the writing pipe is #0
            if(_radio->available() > 0)
            {
              //this is an ACK packet, so no payload.
              myRFPacketHeader ackPacket;

              //because the packet size is asynchronous, we need dynamic payloads.
              uint8_t bytes = _radio->getDynamicPayloadSize();
              //todo: assert that the packet is no more than the payload.

              if(bytes != sizeof(myRFPacketHeader))
              {
                  _errors++;

                  if(_debug)
                  {
                      _debug->print(F("WARNING: incorrect payload size: "));
                      _debug->println(bytes);
                      _debug->flush();
                  }
              }

              _radio->read(&ackPacket, sizeof(myRFPacketHeader));
              bool isACK = ((ackPacket.retryFlags & MYRF_ACK) == MYRF_ACK);
              if(isACK &&  ackPacket.targetId == header->senderId)
              {
                // if(_debug)
                // {
                //     _debug->print(header->packetNumber);
                //     _debug->print(".");
                //     _debug->print(ackPacket.retryFlags, HEX);
                //     _debug->println(F(" Confirmed."));
                //     _debug->flush();
                // }

                _packetsConfirmed++;

                _radio->stopListening();
                return true;
              }
              else
              {
                  if(_debug)
                  {
                      _debug->println(F("ACK, but not for me"));
                      _debug->flush();
                  }
              }
            }
          }

          _radio->stopListening();

          attempt++;
          _packetsTimedOut++;

        //   if(_debug)
        //   {
        //       _debug->print(header->packetNumber);
        //       _debug->print(".");
        //       _debug->print(header->retryFlags, HEX);
        //       _debug->println(F(" timed out."));
        //       _debug->flush();
        //   }
        }
        while(attempt < _maxRetries);

        _errors++;

        if(_debug)
        {
            _debug->print(header->packetNumber);
            _debug->print(".");
            _debug->print(header->retryFlags, HEX);
            _debug->println(F(" not confirmed."));
            _debug->flush();
        }
        return false;
    }

};
