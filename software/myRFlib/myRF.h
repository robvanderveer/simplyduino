/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

#ifndef MYRF_H
#define MYRF_H

struct myRFPacketHeader
{
  uint8_t targetId;
  uint8_t senderId;
  uint8_t packetNumber;
  uint8_t retryFlags;
};

typedef void (*myRFCallback)();

typedef enum
{
        ERR_DROPPED_DUPLICATE = 1,
        ERR_DROPPED_NOT_MY_PACKET = 2,
} RFERROR;

const uint8_t MYRF_ACK = 0x80;

#include "myRFServer.h"
#include "myRFClient.h"

#endif
