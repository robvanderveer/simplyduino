/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

class myRFServer
{
  private:
    RF24 *_radio;
    Stream *_debug;
    uint8_t _lastPacket[256];

  public:
    myRFServer(RF24 *radio, Stream *debug)
    {
      _radio = radio;
      _debug = debug;
    }

    //call `execute` in a tight main loop to process incoming packets.
    void execute(void *bufferToUse, size_t bufferSize, myRFCallback onPacketReceived)
    {
      //get a pointer to the header buffer.
      myRFPacketHeader *header = (myRFPacketHeader *)bufferToUse;

      while ( _radio->available() > 0 )
      {
          uint8_t packetSize = _radio->getDynamicPayloadSize();
          if(packetSize != bufferSize)
          {
                //throw an error.
                _debug->print("ERR packet size (");
                _debug->print(packetSize);
                _debug->println(" bytes)");
                return;
          }

          // Fetch the payload, and see if this was the last one.
          uint8_t status = _radio->read( bufferToUse, packetSize );
        //   _debug->print("STATUS: 0x");
        //   _debug->print(status, HEX);
        //   _debug->println();

          if(header->senderId == 0x00)
          {
              _debug->println("ERR unexpected package sender 0x00");
              return;
          }

         if(header->targetId == 0x00)
         {

            //send ack for the packet.
            myRFPacketHeader ackPacket;
            ackPacket.targetId = header->senderId;
            ackPacket.senderId = 0x00; //hub
            ackPacket.retryFlags = header->retryFlags | 0x80;  //ACK this packet.
            ackPacket.packetNumber = header->packetNumber;

            uint8_t packetNumber = header->packetNumber;
            if(packetNumber == _lastPacket[header->senderId])
            {
                //already processed.
                ackPacket.retryFlags |= 0x40;
            }

        //     //add a little delay so the client has a chance to switch to listening.
        //     delayMicroseconds(250);

            //always ACK immediately.
            _radio->stopListening();
            _radio->write(&ackPacket, sizeof(myRFPacketHeader));
            _radio->startListening();

            //did we receive this packet already?
            if(packetNumber != _lastPacket[header->senderId])
            {
              _lastPacket[header->senderId] = packetNumber;

              (*onPacketReceived)();
            }
            else
            {
              _debug->print(F("ERR already processed senderId "));
              _debug->print(header->senderId);
              _debug->print(F(" packet "));
              _debug->print(packetNumber);
              _debug->print(F(" ("));
              _debug->print(header->retryFlags);
              _debug->println(")");
            }
          }
          else
          {
              _debug->print(F("ERR packet not intended for me (targetId "));
              _debug->print(header->targetId, HEX);
              _debug->println(")");
          }
      }
    }
};
