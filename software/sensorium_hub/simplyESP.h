/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

#ifndef SIMPLYESP_H
#define SIMPLYESP_H

#include <Arduino.h>
#include "matcher.h"
#include "timedEvent.h"

/* The SimplyESP class is designed specifically to send http requests to a server */
/* to allow non-blocking calls, the sendRequest method uses a state machine to monitor the progress */

#define REQUEST_BUFFER_SIZE 256

typedef enum
{
  Idle,            //can start a command.
  CIPStartSent,    //expecting for CIPSTART ACK
  CIPSendSent,     //expecting for CIPSEND ready
  CIPSendWaitAck,  //expecting for SENDOK
  ReadingIPD,
  DecodingIPD,
  ReadingPacket,
  Disconnecting,   //after OK/Unlink it will return to Idle.
  Error      //if the flow gets aborted.
} SimplyESPState;

typedef void (*SimplyESPDelegate)();

class SimplyESP
{
  private:
    Stream *_esp;
    Stream *_debugStream;

    SimplyESPState _state;
    uint16_t _packetSize;
    Matcher _match;
    timedEvent _timer;

    char _requestBuffer[REQUEST_BUFFER_SIZE];
    void dbg(char *msg);
    
    bool handleCIPStartResponse(char c);
    bool handleCIPSendResponse(char c);
    bool handleCIPConfirmedResponse(char c);
    bool handlePacketSent(char c);
    bool handleDecodeIPD(char c);
    bool handlePacketRead(char c);
    bool handleDisconnect(char c);
    bool waitForString(const char *string);
  public:
    SimplyESP(Stream *esp, Stream *debug);
    
    SimplyESPDelegate onTimeout;
    bool isBusy();
    bool reset();
    bool connect(char *ssid, char *pass);

    /* return false when busy or error */
    bool beginRequest(char *host, uint8_t port, char *request);

    /* returns true when the request was completed
     * the response is available in the buffer
     * and a new request can be started.
     */
    bool updateRequest();
};

#endif //SIMPLYESP
