/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

void unitTestRingBuffer()
{
  RingBuffer<int> b;

  //push one item, and print the pop.
  if (!b.push(18))
  {
    Serial.println(F("fail1"));
    return;
  }
  
  if(b.count() != 1)
  {
     Serial.println(F("fail1a"));
  }

  int val;
  if (!b.pop(val))
  {
    Serial.println(F("fail2"));
    return;
  }
  
  if (val != 18)
  {
    Serial.println(val);
    Serial.println(F("fail3"));
    return;
  }

  if(b.count() != 0)
  {
     Serial.println(F("fail2a"));
  }

  
  //test queue empty.
  if(b.pop(val))
  {
    Serial.println(F("fail4"));
    return;
  }

  //push 7, the queue should be full.
  for(int i = 0; i < 7; i++)
  {
    if (!b.push(i))
    {
      Serial.println(i);
      Serial.println(F("fail5"));
      return;
    }
  }
  
  if(b.count() != 7)
  {
     Serial.println(F("fail5a"));
  }

  if(b.push(30))
  {
    //should be full.
      Serial.println(F("fail6"));
      return;
  }

  //should pop 7.
  for(int i = 0; i < 7; i++)
  {
    int num;
    if (!b.pop(num))
    {
      Serial.println(i);
      Serial.println(F("fail7"));
      return;
    }
  }

  //should be empty.
  if(b.pop(val))
  {
    //should be empty.
      Serial.println(F("fail8"));
      return;
  }

  Serial.println(F("Unit tests completed."));
}
