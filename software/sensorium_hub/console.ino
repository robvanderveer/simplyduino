/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

void startConsole(Stream *console)
{
    console->println(F("Entering configuration mode. Enter help for commands.\r\n"));

    char lineBuf[64];

    for(;;)
    {
      console->print("> ");
      if(readLine(console, lineBuf, 64))
      {
        console->println();

        if(strcmp_P(lineBuf, PSTR("help")) == 0)
        {
          console->println(F("command are: help, exit, status, save, configure, terminal, debug"));
        }
        else if(strcmp_P(lineBuf, PSTR("exit")) == 0)
        {
          console->println(F("have a nice day."));
          return;
        }
        else if(strcmp_P(lineBuf, PSTR("terminal")) == 0)
        {
          terminal(console, &espSerial);
        }
        else if(strcmp_P(lineBuf, PSTR("debug")) == 0)
        {
          if(settings.debug == 0)
          {
            settings.debug = true;
            console->println(F("debug: ON"));
          }
          else
          {
            settings.debug = false;
            console->println(F("debug: OFF"));
          }
        }
        else if(strcmp_P(lineBuf, PSTR("status")) == 0)
        {
          console->print(F("Network:  "));
          console->println(settings.ssid);
          console->print(F("Password: (hidden)\n"));
          console->print(F("Server:   "));
          console->println(settings.host);
          console->print(F("Debug:    "));
          if(settings.debug)
          {
            console->println(F("ON"));
          }
          else
          {
            console->println(F("OFF"));
          }
        }
        else if(strcmp_P(lineBuf, PSTR("save")) == 0)
        {
          eeprom_write_block((const void*)&settings, (void*)0, sizeof(settings));
          console->println(F("settings saved."));
          return;
        }
        else if(strcmp_P(lineBuf, PSTR("configure")) == 0)
        {

          console->println(F("SSID and PASS MAX 32 characters"));
          console->print(F("enter SSID > "));
          readLine(console, settings.ssid, 32);
          console->print(F("\nenter PASS > "));
          readLine(console, settings.pass, 32);
          console->print(F("\nenter HOST > "));
          readLine(console, settings.host, 32);
          console->println(F("\nExcellent."));
        }

        else
        {
          console->println(F("Unknown command"));
        }
      }
    }
}

/* echo everything from/to the ESP, until ^Z */
void terminal(Stream *console, Stream *esp)
{
  console->println(F("Starting terminal mode, enter ^Z to close."));

  for(;;)
  {
    while(console->available() > 0)
    {
      char c = console->read();
      if(c == 0x1A) //ctrl-z
      {
        console->println(F("Terminal closed."));
        return;
      }

      esp->write(c);
    }

    while(esp->available() > 0)
    {
      char c = esp->read();
      console->write(c);
    }
  }
}

bool readLine(Stream *stream, char *buf, size_t bufLen)
{
   int bufPos = 0;
   buf[bufPos] = '\0';

   for(;;)
   {
     if(stream->available() > 0)
     {
       char c = stream->read();
       //stream->print(c, HEX);
       stream->write(c);

       if(c == '\r')
       {
         continue;
       }
       
       if(c == '\b')
       {
          if(bufPos > 0)
          {
            //erase the character on the console and erase that one too.
            stream->write(' ');
            stream->write('\b');
            
            buf[--bufPos] = '\0'; 
            continue;
          }
       }

       if(c == '\n')
       {
           return true;
       }

       buf[bufPos++] = c;
       buf[bufPos] = '\0';

       //echo
       if(bufPos == bufLen)
       {
         return false;
       }
     }
   }
}
