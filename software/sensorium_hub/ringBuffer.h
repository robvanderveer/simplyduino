/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

/*

(0,0) ........
(1,0) X.......
(2,0) XX......
(2,1) .X......
(2,2) ........
(3,2) ..X.....
(4,2) ..XX....
(7,2) ..XXXXX.
(8,2) ..XXXXXX
(9,2) X.XXXXXX
(10,2)XXXXXXXX

//
(0,0) ........
(1,0) X.......
(7,0) XXXXXXX.
(8,0) XXXXXXXX

Because the queue cannot differentiate FULL from EMPTY you always have one space empty.
So a buffer of 8 can hold 7 items.

*/

template<typename T> class RingBuffer
{
  private:
    T _items[8];
    uint8_t _ringHead = 0;
    uint8_t _ringTail = 0;

    uint8_t next(uint8_t value)
    {
      return (value + 1) % 8;
    }

  public:
    uint8_t count()
    {
      return (_ringHead + 8 - _ringTail)%8;
    }
  
    bool push(const T &object)
    {
      uint8_t nextHead = next(_ringHead);
      if(nextHead == _ringTail)
      {
        //full.
        return false;
      }

      _items[_ringHead] = object;
      _ringHead = nextHead;

      return true;
    }

    bool pop(T &object)
    {
      if(_ringHead == _ringTail)
      {
        //empty;
        return false;
      }

      object = _items[_ringTail];
      _ringTail = next(_ringTail);
      return true;
    }
    
    bool peek(T &object)
    {
      if(_ringHead == _ringTail)
      {
        //empty;
        return false;
      }

      object = _items[_ringTail];
      return true;
    }
};

#endif //RINGBUFFER_H
