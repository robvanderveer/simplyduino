/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

/* Basic ESP driver library to allow me to make a basic HTTP request
 *
 * Reason for this class: regular libraries are blocking.
 *
 * TODO: parse response, read only right amount of bytes and not until the timeout occurs.
 */

#include "simplyESP.h"

const char *SEND_OK = "SEND OK\r\n";
const char *OK_UNLINK = "OK\r\nUnlink\r\n";
const char *BEGIN = "> ";
const char *OK = "OK\r\n";
const char *OK_LINKED = "OK\r\nLinked\r\n";
const char *IPD = "+IPD,2,";
const char *READY = "ready\r\n";

//This sequence works:
/*
AT+RST
OK
AT+CWJAP="XXXXXX","XXXXXX"
OK
AT+CIPMUX=1
OK
AT+CIPSTART=2,"TCP","www.google.com",80
OK
Linked
AT+CIPSEND=2,(cmdlen)
> [enter message here, must be cmdlen]
SEND OK
[response starts streaming here]
+IPD,2,218:HTTP/1.1 200 OK... etc...

[newline]
OK
AT+CIPCLOSE=2
OK
Unlink
*/


/* create a new instance of the client */
SimplyESP::SimplyESP(Stream *esp, Stream *debug)
{
  _esp = esp;
  _debugStream = debug;
}

void SimplyESP::dbg(char *debug)
{
  if (_debugStream)
  {
    _debugStream->print(debug);
  }
}

bool SimplyESP::reset()
{
   //do a double match on 'ready' or OK.
  for (int attempt = 0; attempt < 5; attempt++)
  {
    _esp->print(F("AT+RST\r\n"));
    if (waitForString(READY))
    {
      _state = Idle;
      return true;
    }
    else
    {
      dbg("Timeout while resetting. Retrying.\r\n");
      //try again.
    }
  }
  return false;
}

bool SimplyESP::connect(char *ssid, char *pass)
{
  _state = Idle;

  //set mux
  _esp->print(F("AT+CIPMUX=1\r\n"));
  if (waitForString(OK) == false)
  {
    return false;
  }

  char sbuf[64];
  sprintf_P(sbuf, PSTR("AT+CWJAP=\"%s\",\"%s\"\r\n"), ssid, pass);

  //start to connect.
  _esp->print(sbuf);
  if (waitForString(OK) == false)
  {
    return false;
  }
  
  return true;
}

/* send a simple write-only command */
bool SimplyESP::beginRequest(char *host, uint8_t port, char *request)
{
  if (isBusy())
  {
    return false;
  }

  if (strlen(request) >= REQUEST_BUFFER_SIZE)
  {
    dbg("COMMAND TOO LONG\r\n");
    return false;
  }

  snprintf_P(_requestBuffer, REQUEST_BUFFER_SIZE, PSTR("AT+CIPSTART=2,\"TCP\",\"%s\",%d\r\n"), host, port);
  _esp->print(_requestBuffer);

  //cache the request. strlcpy always 0-terminates withing the buffer length;
  strlcpy(_requestBuffer, request, REQUEST_BUFFER_SIZE);

  _state = CIPStartSent;
  _timer.reset(5000);
  _match.beginMatch(OK_LINKED);
}

bool SimplyESP::updateRequest()
{
  int bytes;
    
  if (_state == Idle)
  {
    return false;
  }
  
  if(_timer.hasElapsed())
  {
    char sbuf[32];
    
    sprintf(sbuf, "##TIMEOUT in state %d\r\n", _state);
    //timeout has occurred.
    dbg(sbuf);
    
    //the sync was broken, we need to reset the module (which means reset, re-authorize etc.)
    if(onTimeout)
    {
      (*onTimeout)();
    }
    
  }

  while (_esp->available() > 0)
  {
    char c = _esp->read();
    //_debugStream->write(c);
    
    switch(_state)
    {
       case CIPStartSent:
        handleCIPStartResponse(c);
        break; 
       case CIPSendSent:
        handleCIPSendResponse(c);
        break;
       case CIPSendWaitAck:
        handleCIPConfirmedResponse(c);
        break;
       case ReadingIPD:
        handlePacketSent(c);
        break;
       case DecodingIPD:
        handleDecodeIPD(c);
        break;
       case ReadingPacket:
        if(handlePacketRead(c))
        {
          return true;
        }
        break;
       case Disconnecting:
        handleDisconnect(c);
        break;
       default:
          dbg("> INVALID STATE");
          return false;
    }
  }
  
  return false;
}

/* handles the response coming from +CIPSTART */
bool SimplyESP::handleCIPStartResponse(char c)
{
  bool match = _match.isMatch(c);
  if (match)
  {
    //cool, we can now send the packet length.
    char cmd2[64];
    sprintf_P(cmd2, PSTR("AT+CIPSEND=2,%d\r\n"), strlen(_requestBuffer));
    _esp->print(cmd2);

    _match.beginMatch(BEGIN);       //if we get >
    _state = CIPSendSent;           //execute starting.
  }
  return false;
}

bool SimplyESP::handleCIPSendResponse(char c)
{
  bool match = _match.isMatch(c);
  if (match)
  {
    //send the command.
    _esp->print(_requestBuffer);

    _match.beginMatch(SEND_OK);      //if we get "SEND OK"
    _state = CIPSendWaitAck;   //keep reading until unlink
  }
  return false;
}

bool SimplyESP::handleCIPConfirmedResponse(char c)
{
  //waiting for the SendOK ack.
  bool match = _match.isMatch(c);
  if (match)
  {
    //start awaiting the +IPD packet.
    
    _match.beginMatch(IPD);     
    _state = ReadingIPD;  
  }
  
  return false;
}

bool SimplyESP::handlePacketSent(char c)
{
  bool match = _match.isMatch(c);
  if(match)
  {
     _packetSize = 0;
     _state = DecodingIPD;
  }
  
  return false;
}

bool SimplyESP::handleDecodeIPD(char c)
{
  if(c >= '0' && c <= '9')
  {
    _packetSize = (_packetSize) * 10 + (c - '0');
    //this digit adds up to the packet size.
  }
  else if(c == ':')
  {
//    _debugStream->print("## packet size ");
//    _debugStream->print(_packetSize);
//    _debugStream->println();
    _state = ReadingPacket;
  }
  else
  {
    //error.
    _state = Error;
  }
  
  return false;
}

bool SimplyESP::handlePacketRead(char c)
{
  //read until the number of bytes has been reached.
  if(_packetSize > 0)
  {
//     _debugStream->write(c);
    
     //eat away. Might push this to a client
     _packetSize--;
  }
  else
  {
    //should match 'OK'
    _match.beginMatch(OK_UNLINK);
    _state = Disconnecting;
    
    return true;
  }
  
  return false;
}

bool SimplyESP::handleDisconnect(char c)
{
  if(_match.isMatch(c))
  {
    _state = Idle;
  }
  return false;
}

bool SimplyESP::isBusy()
{
  return _state != Idle;
}

bool SimplyESP::waitForString(const char *string)
{
  Matcher m;

  m.beginMatch(string);
  unsigned long timeout = millis() + 5000;

  while (millis() < timeout)
  {
    if (_esp->available() > 0)
    {
      char c = _esp->read();
      if(_debugStream)
      {
        _debugStream->write(c);
      }

      if (m.isMatch(c))
      {
        dbg("> Match\r\n");
        return true;
      }

    }
  }

  dbg("> Timeout\r\n");
  return false;
}
