/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

/* Pinout 

Arduino UNO     ESP8266
=======================
GND              GND
2                TX
3                RX
3v3 (REGULATED)  VIN
3v3 (REGULATED)  CH_PD


Arduino UNO     NRF24L01+
=========================
-                IRQ
9                CE
10               CS
11               MOSI
12               MISO
13               SCK
3v3 regulated    VIN
GND              GND

Connect A0 to GND to enter the configuration console.

*/


//sanity check
#ifndef ARDUINO_AVR_UNO
#error "Please select 'Arduino UNO' from the boards menu
#endif


#include <SoftwareSerial.h>
#include <avr/eeprom.h>
#include <SPI.h>
#include <RF24.h>
#include <myRF.h>
#include "simplyESP.h"
#include "ringBuffer.h"
#include "timedEvent.h"

const uint8_t PIN_ESP_RX = 2;
const uint8_t PIN_ESP_TX = 3;
const uint8_t PIN_NRF_CE = 9;
const uint8_t PIN_NRF_CS = 10;
const uint8_t PIN_CONFIGURE = A0;
const uint8_t PIN_FREE_GND = A1;

const uint8_t PIN_LED = 13;

const unsigned long long readingPipe = 0x6666666618LL;
const unsigned long long writingPipe = 0x66666666C0LL;

typedef struct
{
  myRFPacketHeader header;
  uint16_t value;
  uint8_t metricId;
} SensoduinoPacket;

typedef struct
{
  char ssid[32];
  char pass[32];
  char host[32];
  bool debug;
} Configuration;

RingBuffer<SensoduinoPacket> queue;

RF24 radio(PIN_NRF_CE, PIN_NRF_CS);
myRFServer server(&radio, &Serial);
SoftwareSerial espSerial(PIN_ESP_RX, PIN_ESP_TX);
SimplyESP esp(&espSerial, &Serial);
Configuration settings;
SensoduinoPacket packetBuffer;
timedEvent heartbeatEvent;

void setup()
{
  Serial.begin(9600); //can't be faster than 19200 for softserial
  Serial.println(F("\r\n\r\n***\r\nSensorium HUB v0.96, Simplicate.info"));
  Serial.print(F("Compiled: "));
  Serial.print(F(__DATE__));
  Serial.print(F(", "));
  Serial.println(F(__TIME__));
  delay(200);

  unitTestRingBuffer();

  // Open serial communications and wait for port to open:
  espSerial.begin(9600);

  //load the configuration from EEPROM
  eeprom_read_block((void*)&settings, (void *)0, sizeof(settings));

  //jump A0 to GND to program the Wifi Details through the serial port
  pinMode(PIN_CONFIGURE, INPUT_PULLUP);
  pinMode(PIN_FREE_GND, OUTPUT);
  digitalWrite(PIN_FREE_GND, LOW);

  if(digitalRead(PIN_CONFIGURE) == LOW)
  {
     //opens up the serial configuration screen.
    startConsole(&Serial);
  }
  else
  {
    Serial.println(F("Enter configuration mode by connecting A0 to ground and reset."));
  }

  pinMode(PIN_FREE_GND, INPUT);
  digitalWrite(PIN_FREE_GND, LOW);

  /* setup radio */
  radio.begin();
  radio.setDataRate( RF24_250KBPS ) ;
  radio.setPALevel( RF24_PA_MAX ) ;
  radio.enableDynamicPayloads() ;
  radio.setAutoAck( false ) ;
  radio.openWritingPipe(writingPipe);
  radio.openReadingPipe(1, readingPipe);
  radio.powerUp() ;
  radio.startListening();

  if(!esp.reset())
  {
    bool on = false;
     //fatal error. Boot failure.
    for(;;)
     {
          on = !on;
          digitalWrite(13, on);
          delay(200);
     } 
  }
  esp.connect(settings.ssid, settings.pass);
  esp.onTimeout = &espTimeout;

  //every minute, and fire immediately.
  heartbeatEvent.reset(60*1000U);
  heartbeatEvent.fire();
}

void espTimeout()
{
  Serial.print(F("## timeout occured. Restarting the connection\r\n"));
  
  esp.reset();
  esp.connect(settings.ssid, settings.pass);
  
  heartbeatEvent.fire();
}

void onPacketReceived()
{
   char buf[64];
   sprintf(buf, "IN: [S0x%08x,#%d/%d] %d = %d \n", packetBuffer.header.senderId, packetBuffer.header.packetNumber, packetBuffer.header.retryFlags, packetBuffer.metricId, packetBuffer.value);
   Serial.print(buf);

   if(!queue.push(packetBuffer))
   {
       Serial.println(F("## FAILURE, message buffer overflow"));
   }
}

void loop()
{
  if(heartbeatEvent.hasElapsed())
  {
     Serial.println(F("## Pushing heartbeat"));

     SensoduinoPacket packet;
     packet.header.senderId = 0xFF;
     packet.header.packetNumber = 0x00;
     packet.header.retryFlags = 0x00;
     packet.metricId = 35;
     packet.value = 1;

     if(!queue.push(packet))
     {
         Serial.println(F("## FAILURE, message buffer overflow"));
     }
  }


  server.execute(&packetBuffer, sizeof(SensoduinoPacket), onPacketReceived);

  if (!esp.isBusy())
  {
    SensoduinoPacket packet;
    if(queue.peek(packet))
    {
      char buf[64];
      sprintf(buf, "OUT: [S0x%08x,#%d/%d] %d = %d \n", packet.header.senderId, packet.header.packetNumber, packet.header.retryFlags, packet.metricId, packet.value);
      Serial.print(buf);

      char cmd[128];
      sprintf_P(cmd, PSTR("GET /sensorium/api/v1/addReading/%08x/%d/%d HTTP/1.0\r\nHost: %s\r\nUser-Agent: Sensoduino-Hub\r\nConnection: close\r\n\r\n"), packet.header.senderId, packet.metricId, packet.value, settings.host);
//      if(settings.debug)
//      {
//        Serial.println(cmd);
//      }

      esp.beginRequest(settings.host, 80, cmd);
    }
  }

  if (esp.updateRequest())
  {
    Serial.println(F("OUT: REQUEST COMPLETED"));
    
    //pop the transmitted packet to mark the packet done.
    SensoduinoPacket packet;
    queue.pop(packet);
    
    Serial.print("Queued items: ");
    Serial.println(queue.count());
  }
}

