<?php

abstract class FormField
{
        public $fieldname;
        public $options;

        public function __construct($fieldname, $options = [])
        {
                $this->fieldname = $fieldname;
                $this->options = $options;
        }

        public function renderRow($form, $editMode, $values)
        {
                $value = '';
                if(isset($values[$this->fieldname]))
                {
                        $value = $values[$this->fieldname];
                }
                echo "<div>";
                if(isset($this->options['label']))
                {
                        echo "<label>".$this->options['label']."</label><span>";
                }
                if($editMode && !isset($this->options['readonly']))
                {
                        $validators = $form->validatorsForField($this->fieldname);
                        $isValid = true;
                        foreach($validators as $validator)
                        {
                                if($validator->isValid() == false)
                                {
                                        $isValid = false;
                                }
                        }

                        // if(!$isValid)
                        // {
                        //     echo "<span style='border: 1px solid red;'>";
                        // }
                        $this->renderForm($value);

                        if($isValid == false)
                        {
                                echo "<span class='errorindicator'>*</span>";
                        //        echo "</span>";
                        }
                }
                else
                {
                        $this->renderReadOnly($value);

                }



                echo "</span></div>";
        }

        public abstract function renderForm($value);

        public function renderReadOnly($value)
        {
                echo "<input type='hidden' name='$this->fieldname' id='$this->fieldname' value='".htmlspecialchars($value, ENT_QUOTES)."'/>";
                echo htmlentities($value);
        }
}

class StringField extends FormField
{
        public function renderForm($value)
        {
                $placeholder = isset($this->options["placeholder"])?"placeholder='".htmlspecialchars($this->options["placeholder"], ENT_QUOTES)."'":"";
                $maxLength =  isset($this->options["maxlength"])?"maxlength='".$this->options["maxlength"]."'":"";

                echo "<input id='$this->fieldname' name='$this->fieldname' type='text' $placeholder $maxLength value='".htmlspecialchars($value, ENT_QUOTES)."'/>";
        }
}

class LiteralField extends FormField
{
        public function renderForm($value)
        {
                $this->renderReadOnly($this->fieldname);
        }
}

class OptionField extends FormField
{
        public function renderForm($value)
        {
                echo "<div class='optiongroup'>";
                foreach($this->options["options"] as $optionValue => $optionLabel)
                {
                        $checked = ($value == $optionValue)?"checked":"";

                        echo "<input type='radio' name='$this->fieldname' id='$this->fieldname$optionValue' $checked value='".htmlspecialchars($optionValue, ENT_QUOTES)."'><label for='$this->fieldname$optionValue'>".htmlspecialchars($optionLabel, ENT_QUOTES)."</label></input><br/>";
                }
                echo "</div>";
        }

        public function renderReadOnly($value)
        {
                echo "<input type='hidden' name='$this->fieldname' id='$this->fieldname'  value='".htmlspecialchars($value, ENT_QUOTES)."'/>";

                if(!isset($value) || !is_array($value))
                {
                        //throw new Exception("No initial value for $this->fieldname");
                        echo "Not set";
                }
                else
                {
                        $choices = $this->options["options"];
                        echo htmlentities($choices[$value]);
                }
        }
}

class SelectionField extends FormField
{
        public function renderForm($value)
        {
                //echo "<div class='optiongroup'>";
                echo "<select name='$this->fieldname' id='$this->fieldname'  >";

                foreach($this->options["options"] as $optionValue => $optionLabel)
                {
                        $selected = ($value == $optionValue)?"selected":"";

                        echo "<option class='item' $selected value='".htmlspecialchars($optionValue, ENT_QUOTES)."'><label for='$this->fieldname$optionValue'>".htmlspecialchars($optionLabel, ENT_QUOTES)."</label></option>";
                }
                echo "</select>";
        }

        public function renderReadOnly($value)
        {
                echo "<input type='hidden' name='$this->fieldname' id='$this->fieldname'  value='.htmlspecialchars($value, ENT_QUOTES).'/>";

                if(!isset($value) || !is_array($value))
                {
                        //throw new Exception("No initial value for $this->fieldname");
                        echo "Not set";
                }
                else
                {
                        $choices = $this->options["options"];
                        echo htmlentities($choices[$value]);
                }
        }
}

class CheckboxField extends FormField
{
        public function renderForm($value)
        {
                $checked = ($value)?"checked":"";
                $placeholder = isset($this->options["placeholder"])?htmlspecialchars($this->options["placeholder"], ENT_QUOTES):"&nbsp;";

                echo "<input type='checkbox' name='$this->fieldname' id='$this->fieldname' $checked value='1'><label for='$this->fieldname'><span>$placeholder</span></label></input>";
        }

        public function renderReadOnly($value)
        {
                echo "<input type='hidden' name='$this->fieldname' id='$this->fieldname'  value='".htmlspecialchars($value, ENT_QUOTES)."'/>";

                if($value)
                {
                        echo "Yes";
                }
                else
                {
                        echo "No";
                }
        }
}

class PasswordField extends FormField
{
        public function renderForm($value)
        {
                $placeholder = isset($this->options["placeholder"])?"placeholder='".htmlspecialchars($this->options["placeholder"], ENT_QUOTES)."'":"";

                //do not render the password.
                echo "<input id='$this->fieldname' name='$this->fieldname' $placeholder type='password' value=''/>";
        }

        public function renderReadOnly($value)
        {
                //never save the original password.
                //echo "<input type='hidden' name='$this->fieldname' id='$this->fieldname$optionValue'  value='.htmlspecialchars($value, ENT_QUOTES).'/>";

                echo "********";
        }
}

abstract class FormValidator
{
        protected $isValid = true;
        protected $message;
        public $fieldName;

        public function __construct($name, $message)
        {
                $this->fieldName = $name;
                $this->message = $message;
        }


        public abstract function validate($values);

        public function message()
        {
                return $this->message;
        }
        public function isValid()
        {
                return $this->isValid;
        }
}

class RequiredFieldValidator extends FormValidator
{
        public function validate($values)
        {
                $this->isValid = isset($values[$this->fieldName]) && strlen($values[$this->fieldName]) > 0;
        }
}

class RegularExpressionFieldValidator extends FormValidator
{
        protected $expression;

        public function __construct($name, $message, $expression)
        {
                $this->fieldName = $name;
                $this->message = $message;
                $this->expression = $expression;
        }

        public function validate($values)
        {
                if(isset($values[$this->fieldName]) && strlen($values[$this->fieldName]) > 0)
                {
                        $this->isValid = preg_match($this->expression, $values[$this->fieldName]);
                }
                else
                {
                        $this->isValid = true;
                }
        }
}

class FieldGroup extends FormField
{
        protected $subFields = [];
        protected $label;

        public function __construct($label, $subFields)
        {
                $this->fieldname = $label;
                $this->label = $label;
                $this->subFields = $subFields;
        }

        public function renderRow($form, $editMode, $values)
        {
                echo "<fieldset>";
                echo "<legend>$this->label</legend>";

                foreach($this->subFields as $field)
                {
                        $field->renderRow($form, $editMode, $values);
                }
                echo "</fieldset>";
        }

        public function renderForm($values)
        {
                //empty.
        }

        public function renderReadOnly($values)
        {
                //empty.
        }
}

class EditForm
{
        protected $fields = [];
        protected $buttons = [];
        protected $formID = "";
        protected $validators = [];
        protected $messages = [];
        protected $action;
        public $submitTitle = "Save";

        public function __construct($formID, $action)
        {
                $this->formID = $formID;
                $this->action = $action;
        }

        public function setAction($action)
        {
                $this->action = $action;
        }

        public function addField($newField)
        {
                $this->fields[$newField->fieldname] = $newField;
        }

        public function addMessage($message)
        {
            $this->messages[] = $message;
        }

        public function addButton($name, $link, $confirm = null)
        {
            $this->buttons[] = [ "name" => $name, "link" => $link, "confirm" => $confirm];
        }

        public function addValidator($newValidator)
        {
                $this->validators[] = $newValidator;
        }

        public function validate($values)
        {
                foreach($this->validators as $validator)
                {
                        $validator->validate($values);
                }
        }

        public function isValid()
        {
                foreach($this->validators as $validator)
                {
                        if($validator->isValid() == false)
                        {
                                return false;
                        }
                }

                return true;
        }

        public function isPostback()
        {
                if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST')
                {
                        $thisPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                        if($_SERVER['QUERY_STRING'])
                        {
                                $thisPage .= "?".$_SERVER['QUERY_STRING'];
                        }
                        // var_dump($_SERVER);
                        //
                        // echo $thisPage."<br/>";
                        // echo $_SERVER['HTTP_REFERER']."<br/>";
                        // echo $_SERVER['SCRIPT_NAME']."\n";

                        //verify referrer for sanity.
                        // if ($_SERVER['HTTP_REFERER'] == $thisPage)
                        // {
                        //         // sanity check #2
                        //
                        // }
                        // else
                        // {
                        //         echo $thisPage."<br/>";
                        //         echo $_SERVER['HTTP_REFERER']."<br/>";
                        //         throw new Exception("Access forged");
                        // }

                        if(isset($_POST[$this->formID]))
                        {
                                return true;
                        }
                        else
                        {
                                throw new Exception("Postback consistency error");
                        }
                }
                return false;
        }

        public function render($editMode, $values)
        {
                echo "<form method='POST' action='{$this->action}' id='form$this->formID'>";
                echo "<input type='hidden' name='$this->formID'/>";

                foreach($this->fields as $fieldname => $field)
                {
                        $field->renderRow($this, $editMode, $values);
                }

                if($editMode)
                {
                        echo "<br/>";

                        $this->renderValidationSummary();
                        echo "<div><label>&nbsp;</label>";
                        echo "<a href='javascript:;' class='button big' onclick='document.getElementById(\"form$this->formID\").submit();'>$this->submitTitle</a>";

                        foreach($this->buttons as $button)
                        {
                            echo "&nbsp;";
                            $confirm = "";
                            if(isset($button["confirm"]))
                            {
                                $confirm = " confirmLink' title='".htmlentities($button["confirm"]);
                            }
                            echo "<a href='".$button["link"]."' class='button big{$confirm}'>".$button["name"]."</a>";
                        }

                    echo "</div>";


                }
                echo "</form>";
        }

        public function renderValidationSummary()
        {
                //render the error messages.
                if(($this->isPostback() && !$this->isValid() )|| $this->messages)
                {
                    echo "<div><label></label>";
                        echo "<ul class='errormessages formgroup'>";
                        foreach($this->validators as $validator)
                        {
                                if($validator->isValid() == false)
                                {
                                        echo "<li class='error'>".htmlentities($validator->message())."</li>";
                                }
                        }
                        foreach($this->messages as $message)
                        {
                            echo "<li class='error'>".htmlentities($message)."</li>";
                        }
                        echo "</ul></div>";
                }
        }

        public function validatorsForField($fieldname)
        {
                $result = [];
                foreach($this->validators as $validator)
                {
                        if($validator->fieldName == $fieldname)
                        {
                                $result[] = $validator;
                        }
                }

                return $result;
        }
}

?>
