<?php

require_once(__DIR__.'/db.class.php');
require_once(__DIR__.'/Configuration.class.php');

abstract class LogLevel
{
    const Trace = 0;
    const Info = 1;
    const Warning = 2;
    const Error = 3;
}

class LogEntry
{
        public function MessageTypeName()
        {
                switch( $this->MessageType )
                {
                        case LogLevel::Trace:
                                return "Trace";
                        case LogLevel::Info:
                                return "Info";
                        case LogLevel::Warning:
                                return "Warning";
                        case LogLevel::Error:
                                return "Error";

                }
                return "";
        }
};

function error_handler($code, $message, $file, $line)
{
    if (0 == error_reporting())
    {
        return;
    }
    throw new ErrorException($message, 0, $code, $file, $line);
}

function customExceptionHandler($e)
{
        try
        {
                //we can add more details here.
                addlog(2, $e->getMessage());
        }
        catch(Exception $ee)
        {
                echo "DB Log failed<br/>";
        }

        //end any open script tag to display the error.
        echo "</script><hr>---------";
        echo $e->getMessage();
        echo "<pre>";
        var_dump($e);
        echo "</pre>";

}

set_exception_handler("customExceptionHandler");
set_error_handler("error_handler");

function addlog($type, $message)
{
        $DB = new DB();

        $configuration = loadConfiguration();
        if($type >= $configuration->LogLevel)
        {
            $statement = $DB->prepare("INSERT INTO Log(TimeStamp, MessageType, Message)
                   VALUES(NOW(2), :type, :message)");
               $statement->execute(array(
                   "type" => $type,
                   "message" => $message
               ));
        }
}
?>
