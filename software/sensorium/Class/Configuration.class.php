<?php

require_once(__DIR__.'/db.class.php');

class Configuration
{
    use SolidObject;

    public $DatabaseVersion;
    public $HomeURL;
    public $APIKey;
    public $TimeZone;
    public $SMTPServer;
    public $SMTPUsername;
    public $SMTPPassword;
    public $LogLevel;
    public $NotificationRecipient;
    public $NotificationSubject;

    function __construct()
    {
        //set defaults
        $this->APIKey = "00000000";
        $this->LogLevel = 0;
        $this->DatabaseVersion = 0;
        $this->HomeURL = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER["PHP_SELF"]).'/';
        $this->TimeZone=  "Europe/Amsterdam";
        $this->SMTPServer = "127.0.0.1";
        $this->NotificationSubject = "[Sensorium]";
        $this->SMTPPassword = "";
    }

    /** returns true when all required parameters are filled */
    public function isValid()
    {
        //because the concept of 'optional' is not defined yet.
        return true;
    }

    public function load()
    {
        $db = new DB();
        $sql = "SELECT Name, Value FROM Config";
        $statement = $db->prepare($sql);
        $statement->execute();
        $items = $statement->fetchAll(PDO::FETCH_OBJ);

        foreach ($items as $item)
        {
            if(!property_exists($this, $item->Name))
            {
                throw new Exception("Loading a configuration '$item->Name' that was not defined.");
            }

            $name = $item->Name;
            $this->$name = $item->Value;
        }
    }

    public function save()
    {
        $db = new DB();
        $db->beginTransaction();
        $sql = "INSERT INTO Config (Name, Value) VALUES (:name, :value) ON DUPLICATE KEY UPDATE Value=VALUES(Value)";
        $statement = $db->prepare($sql);

        foreach(get_object_vars($this) as $key => $value)
        {
            // try{
            //
                $statement->execute(["name" => $key, "value" => $value]);
            // }
            // catch(Exception $e)
            // {
            //     echo $e;
            //     var_dump($key);
            // }
        }
        $db->commit();
    }
}

function loadConfiguration()
{
    //load up the configuration object with default values, then attempt to load from the database.
    $configuration = new Configuration();
    $configuration->load();
    return $configuration;
}
?>
