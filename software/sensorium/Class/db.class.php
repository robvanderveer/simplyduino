<?php

if(file_exists(__DIR__.'/../config.php'))
{
        require_once(__DIR__.'/../config.php');
}
else
{
    //    http_response_code(500);
    //    exit("<h1>500 Fatal</h1>Sensorium cannot be started without a valid config.php.");
}

class DbException extends Exception
{
        public $sql;
        public $msg;

        public function __construct($message, $sql)
        {
                $this->sql = $sql;
                parent::__construct($message, 0);
        }

        public function __toString()
        {
                return __CLASS__ . ": {$this->message}\n";
        }
}

class DB
{
        protected $db = '';

        public function __construct($allowFail = false)
        {
                //mysql:host=depot.simplicate.local;dbname=sensorium;charset=utf8
                $connectionString = "mysql:host=".DATABASE_HOST.";dbname=".DATABASE_NAME.";charset=utf8";

                try
                {
                        $db = new PDO($connectionString, DATABASE_USERNAME, DATABASE_PASSWORD);
                        $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );//Error Handli
                        $this->db = $db;
                }
                catch(Exception $e)
                {
                        if($allowFail)
                        {
                                return null;
                        }
                        throw new DbException("Database connection failed", $connectionString, $e);
                }
        }

        public function isValid()
        {
                return $this->db != null;
        }

        public function updateSingle($sql, $params)
        {
                $statement = $this->db->prepare($sql);
                $statement->execute($params);
                if ($statement->rowCount() > 1)
                {
                        echo "RowCount=".$statement->rowCount();
                        throw new DbException("expected to update a single record.", $sql);
                }
        }

        public function execute($sql)
        {
                return $this->db->exec($sql);
        }

        public function executeWith($sql, $args)
        {
                $statement = $this->db->prepare($sql);
                $statement->execute($args);

                return $statement;
        }

        public function prepare($sql)
        {
                return $this->db->prepare($sql);
        }

        public function fetchWith($sql, $args)
        {
            $statement = $this->prepare($sql);
            $statement->execute($args);
            $data = $statement->fetch();
            return $data;
        }

        public function query($sql)
        {
                return $this->db->query($sql);
        }

        public function lastInsertId()
        {
                return $this->db->lastInsertId();
        }

        public function beginTransaction()
        {
                return $this->db->beginTransaction();
        }

        public function rollback()
        {
                return $this->db->rollBack();
        }

        public function commit()
        {
                return $this->db->commit();
        }
}
?>
