<?php

require_once __DIR__.'/Framework/Register.php';

session_start();

// assuming that this php file is in the root of the application, prime the worker with the virtual root folder.
$worker = new HttpWorker();

Routing::init();
Routing::addRoute("TriggerEdit", new Route("trigger/edit/{triggerId}", "TriggerController", ["action" => "edit"]));
Routing::addRoute("TriggerCreate", new Route("trigger/create", "TriggerController", ["action" => "create" ] ));
Routing::addRoute("TriggerSave", new Route("trigger/save", "TriggerController", ["action" => "save"] ));
Routing::addRoute("TriggerDelete", new Route("trigger/delete/{triggerId}", "TriggerController", ["action" => "delete"] ));
Routing::addRoute("Triggers", new Route("triggers", "TriggerController"));

Routing::addRoute("SetupStep", new Route("configure/{action}", "SetupController", [ "action" => "index" ]));

Routing::addRoute("Sensors", new Route("sensors", "SensorController"));
Routing::addRoute("SensorDelete", new Route("sensor/delete/{sensorId}", "SensorController", [ "action" => "delete"] ));
Routing::addRoute("SensorSave", new Route("sensor/save", "SensorController", [ "action" => "save" ]));
Routing::addRoute("SensorEdit", new Route("sensor/edit/{sensorId}", "SensorController", [ "action" => "edit" ]));
Routing::addRoute("Chart", new Route("chart/{sensorId}", "ChartController"));

Routing::addRoute("Scheduler", new Route("schedule", "ScheduleController"));

Routing::addRoute("Login", new Route("login", "LoginController"));
Routing::addRoute("Logout", new Route("logout", "LoginController", ["action" => "logout"]));

Routing::addRoute("Settings", new Route("settings/{action}", "SettingsController", ["action" => "index"]));
Routing::addRoute("Log", new Route("log", "LogController"));
Routing::addRoute("Api", new Route("api/v1/addReading/{nodeID}/{metricID}/{value}", "APIController", ["action" => "addReading"]));

//add the default route
Routing::addRoute("Default", new Route("dashboard", "DashboardController"));
Routing::addRoute("Gauges", new Route("dashboard/gauges", "DashboardController", ["action" => "gauges"]));
Routing::addRoute("Home", new Route("", "DashboardController"));

$worker->invoke($_SERVER["REQUEST_URI"]);

?>
