<?php

if(!isset($triggerInfo))
{
        class dummyTrigger
        {
                public $TriggerID = "X";
                public $SensorDescription = "[SENSOR NAME]";
                public $Threshold = "[THRESHOLD]";
                public $MetricDescription = "[METRIC NAME]";
        }

        $triggerInfo = new dummyTrigger();
        $active = true;
        $triggerType = "[IS ABOVE]";
        $logoImageSrc="../images/Sensorium@2x.png";
        $poweredByImageSrc="../images/PoweredBySimplyduino@2x.png";
}

$url = $homeUrl . '/triggers';

include 'MailHeader.php';
?>

<p>
Hi,
</p>
<p>
The <b><?php echo $triggerInfo->MetricDescription;?></b> for <b><?php echo $triggerInfo->SensorDescription; ?></b> is <?php echo $triggerState ?>.
</p>
<p>
Sensorium.
</p>
<?php

include 'MailFooter.php';

?>
