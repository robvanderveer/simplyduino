<?php

if(!isset($triggerInfo))
{
        class dummyTrigger
        {
                public $TriggerID = "X";
                public $SensorDescription = "[SENSOR NAME]";
                public $Threshold = "[THRESHOLD]";
                public $MetricDescription = "[METRIC NAME]";
                public $SensorID = "[SensorID]";
        }

        $triggerInfo = new dummyTrigger();
}

$url = $homeUrl . '/triggers';

include 'MailHeader.php';
?>

<p>
Hi,
</p>
<p>
The sensor <b><a href="<?php echo $homeUrl."/sensor/edit/".$triggerInfo->SensorID; ?>"><?php echo $triggerInfo->SensorDescription; ?></a></b> has reported that the <b><?php echo $triggerInfo->MetricDescription;?></b> <?php echo $triggerType;?>.
</p>
<p>
You will not receive further emails for this event until the trigger is reset.
</p>
<p>
Sensorium.
</p>
<?php

include 'MailFooter.php';

?>
