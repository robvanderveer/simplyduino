<html><head><style>
BODY {
        font-family: Avenir-light;
        font-size: normal;
        padding: 0px;
        margin: 0px;
}
.header
{
        font-size: x-small;
        padding: 10px;
}

.main
{
        padding: 10px;
}
.footer
{
        padding: 10px;
        font-size: smaller;
        background-color: #2B144E;
        color: #C0C0C0;
        font-size: 9pt;
        min-height: 51px;
}

.footer a
{
        color: white;
}

.footer p
{
        padding: 0px;
        margin: 0px;
}

.footer div[id=poweredBy]
{
        float: right;
}

@media only screen and (min-device-pixel-ratio : 2),
       only screen and (-webkit-min-device-pixel-ratio : 2) {
*[id=logo] { background:url('<?php echo $logoImageSrc2;?>') no-repeat 0 top; -webkit-background-size: 160px 27px; }
*[id=poweredBy] { background:url('<?php echo $poweredByImageSrc2;?>') no-repeat 0 top; -webkit-background-size: 159px 51px; }

div[id=logo] { width: 160px; height: 27px; }
div[id=logo] img { display: none; }

div[id=poweredBy] { width: 159px; height: 51px; }
div[id=poweredBy] img { display: none; }

}


</style>
</head><body>
<div class="header">
        <div id="logo"><img src="<?php echo $logoImageSrc;?>" id="logo" width="160" height="27"/></div>
        Trigger notification
</div>
<div class="main">
