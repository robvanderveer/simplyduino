<?php

require_once(__DIR__.'/../Class/Configuration.class.php');

class APIKey
{
        public function verifyKey($keyToVerify)
        {
                $configuration = loadConfiguration();

                return $keyToVerify == $configuration->APIKey;
        }
}

?>
