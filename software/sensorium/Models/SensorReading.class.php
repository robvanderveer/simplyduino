<?php

class SensorReading
{
}

function getMetricsForSensor($sensorId)
{
        $db = new DB();

        //get an array of all available metrictypes reported for this sensor.
        $sql = "select distinct(MetricType),MetricType.Description from SensorReading left join MetricType on SensorReading.MetricType = MetricType.MetricID where SensorID = :sensorId order by metricID;";
        $statement = $db->prepare($sql);
        $statement->bindValue(":sensorId", $sensorId);
        $statement->execute();
        $metrics = $statement->fetchAll(PDO::FETCH_CLASS, 'MetricType');
        return $metrics;
}

function getDataForMetric($sensorId, $type, $fromDate, $toDate, $round)
{
    $tableName = "SensorReading";
    $sql = "SELECT LEAST(:toDate, FROM_UNIXTIME(CEIL(UNIX_TIMESTAMP(TimeStamp)/:round)*:round)) as TimeStampRounded, AVG(Value) AS Average FROM SensorReading WHERE SensorID = :sensorId AND MetricType = :type AND TimeStamp between :fromDate AND :toDate GROUP BY TimeStampRounded ORDER BY TimeStampRounded";

    //optimized buffer.
    if($round >= 86400)
    {
        $tableName = "SensorReading_Daily";
        $sql = "SELECT TimeStampRounded, Average FROM $tableName WHERE SensorID = :sensorId AND MetricType = :type AND TimeStampRounded between :fromDate AND :toDate ORDER BY TimeStampRounded";
        $round = 0;
    }
    elseif($round >= 3600)
    {
        $tableName = "SensorReading_Hourly";
        $sql = "SELECT TimeStampRounded, Average FROM $tableName WHERE SensorID = :sensorId AND MetricType = :type AND TimeStampRounded between :fromDate AND :toDate ORDER BY TimeStampRounded";
        $round = 0;
    }

        $db = new DB();

        //$sql = "SELECT * FROM SensorReading WHERE SensorID = :sensorId AND MetricType = :type AND TimeStamp between :fromDate AND :toDate ORDER BY TimeStamp";
        $statement = $db->prepare($sql);
        $statement->bindValue(":sensorId", $sensorId);
        $statement->bindValue(":type", $type);
        $statement->bindValue(":fromDate", $fromDate->format('Y-m-d H:i:s'));
        $statement->bindValue(":toDate", $toDate->format('Y-m-d H:i:s'));
        if($round > 0)
        {
            $statement->bindValue(":round", $round);
        }
        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_CLASS, 'SensorReading');

        return $data;

}

function aggregateSensorMetrics()
{
    //TODO: Only run this every day or so.

    $db = new DB();
    $db->beginTransaction();
    try
    {
        $sql = "INSERT INTO SensorReading_Hourly SELECT SensorID, MetricType, FROM_UNIXTIME(CEIL(UNIX_TIMESTAMP(TimeStamp)/:round)*:round) as TimeStampRounded, AVG(Value) AS Average FROM SensorReading GROUP BY SensorID, MetricType, TimeStampRounded ORDER BY SensorID, MetricType, TimeStampRounded ON DUPLICATE KEY UPDATE Average=VALUES(Average)";
        $db->executeWith($sql, [ "round" => 3600 ] );

        $sql = "INSERT INTO SensorReading_Daily SELECT SensorID, MetricType, FROM_UNIXTIME(CEIL(UNIX_TIMESTAMP(TimeStamp)/:round)*:round) as TimeStampRounded, AVG(Value) AS Average FROM SensorReading GROUP BY SensorID, MetricType, TimeStampRounded ORDER BY SensorID, MetricType, TimeStampRounded ON DUPLICATE KEY UPDATE Average=VALUES(Average)";
        $db->executeWith($sql, [ "round" => 86400 ] );

        //delete processed data older than 7 days.
        $sql = "DELETE FROM SensorReading WHERE TimeStamp < DATE_SUB(NOW(2), INTERVAL 7 DAY)";
        $db->execute($sql);
        $db->commit();
    }
    catch(Exception $e)
    {
        $db->rollback();
        throw $e;
    }
}

?>
