<?php

class Sensor
{
        use SolidObject;

        public $SensorID;
        public $Description;
        public $NodeID;
        public $Added;
        public $LastResponse;
        public $IsEnabled;
}

function getSensors()
{
        $db = new DB();
        $sql = "SELECT `SensorID`, `Description`, `NodeID`, `LastResponse`, `Added`, `IsEnabled` FROM Sensor ORDER BY Added";
        $statement = $db->prepare($sql);
        $statement->execute();
        $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'Sensor');

        return $entries;
}

function getSensorsKeyValue()
{
        $db = new DB();
        $sql = "SELECT SensorID, Description, NodeID FROM Sensor ORDER BY SensorID";
        $statement = $db->prepare($sql);
        $statement->execute();
        $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'Sensor');

        $results = [];
        foreach($entries as $row)
        {
                $results[$row->SensorID ] = "$row->Description ($row->NodeID)";
        }

        return $results;
}


function getSensor($sensorID)
{
        $db = new DB();
        $sql = "SELECT `SensorID`, `Description`, `NodeID`, `LastResponse`, `Added`, `IsEnabled` FROM Sensor WHERE SensorID = :sensorID";
        $statement = $db->prepare($sql);
        $statement->bindValue(":sensorID", $sensorID);
        $statement->setFetchMode(PDO::FETCH_CLASS, 'Sensor');
        $statement->execute();
        $sensor_data = $statement->fetch();


        return $sensor_data;
}

function getSensorByNodeID($nodeID)
{
        $db = new DB();
        $sql = "SELECT `SensorID`, `Description`, `NodeID`, `LastResponse`, `Added`, `IsEnabled` FROM Sensor WHERE NodeID = :nodeID";
        $statement = $db->prepare($sql);
        $statement->bindValue(":nodeID", $nodeID);
        $statement->setFetchMode(PDO::FETCH_CLASS, 'Sensor');
        $statement->execute();
        $sensor_data = $statement->fetch();
        return $sensor_data;
}

function updateSensor($sensorID, $newDescription, $newNodeID)
{
        $db = new DB();
        $sql = "UPDATE Sensor SET Description = :description, NodeID = :nodeID WHERE SensorID = :sensorID";
        $statement = $db->prepare($sql);
        $statement->execute(array(
                "sensorID" => $sensorID,
                "description" => $newDescription,
                "nodeID" => $newNodeID
        ));
}

function insertSensor($nodeID)
{
        $db = new DB();
        $statement = $db->prepare(
        "INSERT INTO Sensor(NodeID, Description, Added, IsEnabled )
               VALUES(:nodeId, :description, NOW(2), 1)");
           $statement->execute(array(
               "nodeId" => $nodeID,
               "description" => "New Node",
           ));

        $sensorID = $db->lastInsertId();
        return $sensorID;
}

function deleteSensor($sensorId, $dropData)
{
        $db = new DB();
        $db->beginTransaction();
        try
        {
            $args = [ "sensorID" => $sensorId ];

            $sql = "DELETE FROM SensorReading WHERE SensorID = :sensorID";
            $db->executeWith($sql, $args);

            $sql = "DELETE FROM SensorTrigger WHERE SensorID = :sensorID";
            $db->executeWith($sql, $args);

            $sql = "DELETE FROM Sensor WHERE SensorID = :sensorID";
            $db->executeWith($sql, $args);

            $db->commit();
        }
        catch(Exception $e)
        {
            $db->rollback();
            throw $e;
        }
}
?>
