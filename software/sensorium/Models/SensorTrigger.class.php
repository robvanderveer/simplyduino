<?php

require_once(__DIR__.'/../Class/db.class.php');

class Trigger
{
        const IsAbove = 0;
        const IsBelow = 1;
        const IsEqual = 2;
        const IsNotEqual = 3;
        const Heartbeat = 10;

        public $TriggerID;
        public $SensorID;
        public $MetricType;
        public $TriggerType;
        public $SecondsBeforeTrigger;
        public $Enabled;
        public $Threshold;
        public $Triggered;
        public $LastTriggered;
        public $LastEvaluated;

        public function activeDescription()
        {
                //when...
                //is...
                switch($this->TriggerType)
                {
                        case Trigger::IsAbove:
                        $msg = "is above $this->Threshold";
                        break;
                        case Trigger::IsBelow:
                        $msg = "is below $this->Threshold";
                        break;
                        case Trigger::IsEqual:
                        $msg = "is equal to $this->Threshold";
                        break;
                        case Trigger::IsNotEqual:
                        $msg = "is not equal to $this->Threshold";
                        break;
                        case Trigger::Heartbeat:
                        $msg = "is not updating";
                        break;
                }

                if($this->SecondsBeforeTrigger > 0)
                {
                    $msg .= " for $this->SecondsBeforeTrigger seconds";
                }

                return $msg;
        }

        public function inactiveDescription()
        {
                //is...
                switch($this->TriggerType)
                {
                        case Trigger::IsAbove:
                                $msg = "is no longer above $this->Threshold";
                                break;
                        case Trigger::IsBelow:
                                $msg = "is no longer below $this->Threshold";
                                break;
                        case Trigger::IsEqual:
                                $msg = "is no longer equal to $this->Threshold";
                                break;
                        case Trigger::IsNotEqual:
                                $msg = "is returned to $this->Threshold";
                                break;
                        case Trigger::Heartbeat:
                                $msg = "is reporting activity";
                        break;
                }
                return $msg;
        }
}

function getTriggers()
{
        $db = new DB();
        $sql = "SELECT * FROM SensorTrigger ORDER BY TriggerID";
        $statement = $db->prepare($sql);
        $statement->execute();
        $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'Trigger');

        return $entries;
}

//left-joins details.
function listTriggers()
{
        $db = new DB();
        $sql = "SELECT st.*, s.Description as SensorDescription, mt.Description as MetricDescription
                FROM SensorTrigger st
                LEFT JOIN Sensor s ON st.SensorID = s.SensorID
                LEFT JOIN MetricType mt ON st.MetricType = mt.MetricID
         ORDER BY SensorID, MetricID, TriggerType";
        $statement = $db->prepare($sql);
        $statement->execute();
        $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'Trigger');

        return $entries;
}

function dashboardReport()
{
        $db = new DB();
        $sql = "SELECT st.*,
        s.Description as SensorDescription,
        mt.Description as MetricDescription,
        (SELECT Value FROM SensorReading r WHERE r.SensorID = st.SensorID and r.MetricType = st.MetricType ORDER BY r.ReadingID DESC LIMIT 1) as LastValue
                FROM SensorTrigger st
                LEFT JOIN Sensor s ON st.SensorID = s.SensorID
                LEFT JOIN MetricType mt ON st.MetricType = mt.MetricID
                WHERE (st.TriggerType = 0 or st.TriggerType = 1) 

         ORDER BY SensorID, MetricID, TriggerType";
        $statement = $db->prepare($sql);
        $statement->execute();
        $entries = $statement->fetchAll();

        return $entries;
}


function getTriggersToEvaluate($seconds)
{
        $db = new DB();

        $sql = "SELECT st.*, s.Description as SensorDescription, mt.Description as MetricDescription
                FROM SensorTrigger st
                LEFT JOIN Sensor s ON st.SensorID = s.SensorID
                LEFT JOIN MetricType mt ON st.MetricType = mt.MetricID
                WHERE Enabled = 1 AND (LastEvaluated IS NULL OR LastEvaluated < DATE_SUB(NOW(2), INTERVAL :seconds SECOND))";
        $statement = $db->prepare($sql);
        $statement->bindValue(":seconds", $seconds);
        $statement->execute();
        $triggers = $statement->fetchAll(PDO::FETCH_CLASS, 'Trigger');
        return $triggers;
}

function getTrigger($triggerID)
{
        $db = new DB();
        $sql = "SELECT * FROM SensorTrigger WHERE TriggerID = :triggerID";
        $statement = $db->prepare($sql);
        $statement->bindValue(":triggerID", $triggerID);
        $statement->setFetchMode(PDO::FETCH_CLASS, 'Trigger');
        $statement->execute();
        if($trigger_data = $statement->fetch())
        {
                return $trigger_data;
        }

        throw new Exception("No matching record");
}

function updateTrigger($triggerID, $newSensorID, $newMetricType, $newTriggerType, $newThreshold, $newSecondsBeforeTrigger, $newEnabled)
{
        $db = new DB();
        $sql = "UPDATE SensorTrigger
         SET
          SensorID = :sensorID,
          MetricType = :metricType,
          TriggerType = :triggerType,
          Threshold = :threshold,
          SecondsBeforeTrigger = :secondsBeforeTrigger,
          Enabled = :enabled
         WHERE TriggerID = :triggerID";
        $statement = $db->prepare($sql);
        $statement->execute(array(
                "triggerID" => $triggerID,
                "sensorID" => $newSensorID,
                "triggerType" => $newTriggerType,
                "metricType" => $newMetricType,
                "threshold" => $newThreshold,
                "secondsBeforeTrigger" => $newSecondsBeforeTrigger,
                "enabled" => $newEnabled
        ));
}

function deleteTrigger($triggerID)
{
        $db = new DB();
        $sql = "DELETE FROM SensorTrigger WHERE TriggerID = :triggerID";
        $statement = $db->prepare($sql);
        $statement->execute(array(
                "triggerID" => $triggerID,
        ));
}

function insertTrigger($newSensorID, $newMetricType, $newTriggerType, $newThreshold, $newSecondsBeforeTrigger, $newEnabled)
{
        $db = new DB();
        $sql = "INSERT SensorTrigger
        (SensorID,MetricType,TriggerType,Threshold,SecondsBeforeTrigger,Enabled,Triggered,LastTriggered,LastEvaluated)
        VALUES (
                :sensorID,
                :metricType,
                :triggerType,
                :threshold,
                :secondsBeforeTrigger,
                :enabled,
                :triggered,
                NULL,
                NULL
        )";
        $statement = $db->prepare($sql);
        $statement->execute(array(
                "sensorID" => $newSensorID,
                "triggerType" => $newTriggerType,
                "metricType" => $newMetricType,
                "threshold" => $newThreshold,
                "secondsBeforeTrigger" => $newSecondsBeforeTrigger,
                "enabled" => $newEnabled,
                "triggered" => 0
        ));

        $newId = $db->lastInsertId();
        return $newId;
}

?>
