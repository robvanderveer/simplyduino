<?php

class MetricType
{
    public $MetricID;
    public $Description;
    public $Units;
    public $Low;
    public $High;
}

function getMetricTypesKeyValue()
{
        $db = new DB();
        $sql = "SELECT MetricID, Description FROM MetricType ORDER BY MetricID";
        $statement = $db->prepare($sql);
        $statement->execute();
        $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'MetricType');

        $results = [];
        foreach($entries as $row)
        {
                $results[$row->MetricID ] = $row->Description;
        }

        return $results;
}

function getMetricMeta()
{
    $db = new DB();
    $sql = "SELECT MetricID, Description, Units, Low, High FROM MetricType ORDER BY MetricID";
    $statement = $db->prepare($sql);
    $statement->execute();
    $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'MetricType');

    $results = [];
    foreach($entries as $row)
    {
            $results[$row->MetricID ] = $row;
    }

    return $results;

}

?>
