<?php

require_once(__DIR__.'/../Class/Form.class.php');

class LoginController extends ControllerBase
{
	public function logout()
	{
		Authentication::SignOut();
		return new RedirectResult("dashboard");
	}

	public function index()
	{
		$form = $this->constructForm();
		if($form->isPostBack())
		{
			$form->validate($_POST);
			if($form->isValid())
			{
				$username = $_POST["Username"];
				$pass = $_POST["Password"];

				$db = new DB();
				$user = $db->fetchWith("SELECT * FROM Users WHERE UserName = :name", [":name" => $username]);

				if($user)
				{
					if(password_verify($pass, $user["PasswordHash"] ))
					{
						addLog(LogLevel::Info, "'$username' logged in  ({$_SERVER['REMOTE_ADDR']})" );

						Authentication::Authenticate($user["UserName"]);

						$returnUrl = $this->context->getParam("returnUrl", "dashboard");
						return new RedirectResult($returnUrl);
					}
				}
				addLog(LogLevel::Warning, "Failed login '$username' ({$_SERVER['REMOTE_ADDR']})" );
				$form->addMessage("Invalid credentials");
			}
		}
		else {
			Authentication::SignOut();
		}

		$this->model->pageTitle = "Login";
		$this->model->form = $form;
		return $this->View("LoginView.php");
	}

	private function constructForm()
	{
		$request = $this->context->getParam("returnUrl", "dashboard");
		$redirectUrl = Routing::MakeLink("Login", ["returnUrl" => $request]);

		$form = new EditForm("Configure", $redirectUrl);
		$values = $_POST;
		$form->addField(new StringField("Username", [ "label" => "Username", "maxlength" => 128]));
		$form->addField(new PasswordField("Password", [ "label" => "Password", "maxlength" => 128]));

		$form->addValidator(new RequiredFieldValidator("Username", "Required field"));
		$form->addValidator(new RequiredFieldValidator("Password", "Required field"));
		$form->submitTitle = "Log in";

		$this->model->values = $values;

		return $form;
	}
}
?>
