<?php

require_once __DIR__.'/../Class/LogEntry.class.php';
require_once __DIR__.'/../Class/Configuration.class.php';
require_once __DIR__.'/../Models/Sensor.class.php';
require_once __DIR__.'/../Models/SensorReading.class.php';
require_once __DIR__.'/../Models/SensorTrigger.class.php';
require_once __DIR__.'/../Extras/PHPMailerAutoload.php';

class ScheduleController extends ControllerBase
{
        private $messages = [];

        private function addOutput($message)
        {
                $this->messages[] = $message;
        }

        public function index()
        {
                $db = new DB();

                addLog(LogLevel::Trace, "Running scheduler");

                $triggers = $this->getTriggersToEvaluate();
                if($triggers)
                {
                        foreach($triggers as $trigger)
                        {
                                try
                                {
                                        $db->beginTransaction();

                                        addLog(LogLevel::Trace, "Evaluating trigger $trigger->TriggerID");

                                        //touch the lastEvaluated date to get a 'lock' on the record.
                                        $sql = "UPDATE SensorTrigger SET LastEvaluated = NOW(2) WHERE TriggerID = :triggerID AND (LastEvaluated = :lastEvaluated OR LastEvaluated IS NULL)";
                                        $db->updateSingle($sql, array(
                                                "triggerID" => $trigger->TriggerID,
                                                "lastEvaluated" => isset($trigger->LastEvaluated)?$trigger->LastEvaluated:"NULL"
                                        ));

                                        //get the last reading for the sensor and metric.
                                        if($trigger->SecondsBeforeTrigger == 0)
                                        {
                                            $sql = "SELECT Value FROM SensorReading WHERE SensorID = :sensorID AND MetricType = :metricType ORDER BY TimeStamp DESC LIMIT 1";
                                            $statement = $db->prepare($sql);
                                            $statement->setFetchMode(PDO::FETCH_CLASS, 'SensorReading');
                                            $statement->execute(array("sensorID" => $trigger->SensorID, "metricType" => $trigger->MetricType));
                                        }
                                        else
                                        {
                                            //SELECT AVG(Value) as Average FROM SensorReading WHERE SensorID = 16 AND MetricType = 34 AND TimeStamp > DATE_SUB(NOW(2), INTERVAL 120 SECOND)
                                            $sql = "SELECT AVG(Value) as Value FROM SensorReading WHERE SensorID = :sensorID AND MetricType = :metricType AND TimeStamp > DATE_SUB(NOW(2), INTERVAL :interval SECOND)";
                                            $statement = $db->prepare($sql);
                                            $statement->setFetchMode(PDO::FETCH_CLASS, 'SensorReading');
                                            $statement->execute(array("sensorID" => $trigger->SensorID, "metricType" => $trigger->MetricType, "interval" => $trigger->SecondsBeforeTrigger));
                                        }

                                        $reading = $statement->fetch();
                                        $hasValue = isset($reading) && isset($reading->Value);

                                        $fire = $trigger->Triggered;
                                        switch($trigger->TriggerType)
                                        {
                                                case Trigger::IsAbove:
                                                        if($hasValue)
                                                        {
                                                                $fire = $reading->Value > $trigger->Threshold;
                                                        }
                                                        break;
                                                case Trigger::IsBelow:
                                                        if($hasValue)
                                                        {
                                                                $fire = $reading->Value < $trigger->Threshold;
                                                        }
                                                        break;
                                                case Trigger::IsEqual:
                                                        if($hasValue)
                                                        {
                                                                $fire = $reading->Value == $trigger->Threshold;
                                                        }
                                                        break;
                                                case Trigger::IsNotEqual:
                                                        if($hasValue)
                                                        {
                                                                $fire = $reading->Value != $trigger->Threshold;
                                                        }
                                                        break;
                                                case Trigger::Heartbeat:
                                                        if($hasValue)
                                                        {
                                                                $fire = 0;
                                                        }
                                                        else
                                                        {
                                                                //no reading!
                                                                $fire = 1;
                                                        }
                                                        break;
                                        }

                                        if(isset($hasValue))
                                        {
                                                $this->addOutput("Trigger $trigger->TriggerID / $reading->Value / $trigger->TriggerType / $trigger->Threshold = $fire");
                                        }
                                        if($fire == true)
                                        {
                                                if($trigger->Triggered == false)
                                                {
                                                        //send out the notification. For now, just add a log entry and mark as triggered.

                                                        $this->sendTriggerNotification($trigger, true);

                                                        $sql = "UPDATE SensorTrigger SET Triggered = 1, LastTriggered = NOW(2) WHERE TriggerID = :triggerID AND Triggered = 0";
                                                        $db->updateSingle($sql, array(
                                                                "triggerID" => $trigger->TriggerID
                                                        ));

                                                        addlog(LogLevel::Info, "Trigger $trigger->TriggerID activated");
                                                }
                                                else
                                                {
                                                        $this->addOutput("Nothing's changed");
                                                }
                                        }
                                        else
                                        {
                                                //not fired. Perhaps 'UNFIRE' the trigger?
                                                if($trigger->Triggered)
                                                {
                                                        $this->sendTriggerNotification($trigger, false);

                                                        $sql = "UPDATE SensorTrigger SET Triggered = 0 WHERE TriggerID = :triggerID AND Triggered = 1";
                                                        $db->updateSingle($sql, array(
                                                                "triggerID" => $trigger->TriggerID
                                                        ));

                                                        addlog(LogLevel::Info, "Trigger $trigger->TriggerID cleared");

                                                }
                                        }

                                        $db->commit();
                                }
                                catch(Exception $e)
                                {
                                    addlog(LogLevel::Error, $e);
                                    $this->addOutput("Transaction rolled back: {$e->getMessage()}");
                                    $db->rollback();
                                    throw $e;
                                }
                        }
                }
                else
                {
                        $this->addOutput("No triggers to check");
                }

                $this->truncateLog();
                aggregateSensorMetrics();

                addLog(LogLevel::Trace, "scheduler completed");

                $this->model->messages = $this->messages;
                return $this->View("SchedulerView.php");
        }


        // send mail here.
        function sendTriggerNotification($triggerInfo, $active)
        {
                addLog(LogLevel::Trace, "Sending email");

                if($active)
                {
                        $triggerType = $triggerInfo->activeDescription();
                        $subject = "$triggerInfo->MetricDescription of $triggerInfo->SensorDescription $triggerType";
                }
                else
                {
                        $triggerType = $triggerInfo->inactiveDescription();
                        $subject = "$triggerInfo->MetricDescription of $triggerInfo->SensorDescription $triggerType";
                        $triggerState = "back to normal";

                        if($triggerInfo->TriggerType == Trigger::Heartbeat)
                        {
                            $triggerState = "reporting activity again";
                        }
                }

                $homeUrl = loadConfiguration()->HomeURL;
                $logoImageSrc = $homeUrl."/Images/Sensorium.png";
                $poweredByImageSrc = $homeUrl."/Images/PoweredBySimplyduino.png";
                $logoImageSrc2 = $homeUrl."/Images/Sensorium@2x.png";
                $poweredByImageSrc2 = $homeUrl."/Images/PoweredBySimplyduino@2x.png";

                //construct the message by loading running a template
                ob_start();
                if($active)
                {
                        require __DIR__.'/../Templates/NotificationTemplate.php';
                }
                else
                {
                        require __DIR__.'/../Templates/NotificationTemplateNormal.php';
                }
                $message = ob_get_contents();
                ob_end_clean();

                // $message = '<html><body>';
                // $message = "Trigger $triggerInfo->TriggerID has changed to $active";
                // $message .= '</body></html>';

                $configuration = loadConfiguration();
                $mail = new PHPMailer;
                //$mail->SMTPDebug = 3;                               // Enable verbose debug output

                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = $configuration->SMTPServer;               // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = $configuration->SMTPUsername;                 // SMTP username
                $mail->Password = $configuration->SMTPPassword;                           // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                $mail->From = 'sensorium@simplicate.info';
                $mail->FromName = 'Sensorium';
                $mail->addAddress($configuration->NotificationRecipient);     // Add a recipient
                $mail->isHTML(true);                                  // Set email format to HTML

                $mail->Subject = $configuration->NotificationSubject.' '.$subject;
                $mail->Body    = $message;
                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                if(!$mail->send()) {
                    $this->addOutput('Message could not be sent. Mailer Error: ' . $mail->ErrorInfo);
                    addLog(LogLevel::Error, "Unable to send email: $mail->ErrorInfo");

                } else {
                    $this->addOutput('Message has been sent');
                }
        }

        function getTriggersToEvaluate()
        {
                $db = new DB();

                $sql = "SELECT st.*, s.Description as SensorDescription, mt.Description as MetricDescription
                        FROM SensorTrigger st
                        LEFT JOIN Sensor s ON st.SensorID = s.SensorID
                        LEFT JOIN MetricType mt ON st.MetricType = mt.MetricID
                        WHERE Enabled = 1 AND (LastEvaluated IS NULL OR LastEvaluated < DATE_SUB(NOW(2), INTERVAL 5 SECOND))";
                $statement = $db->prepare($sql);
                $statement->execute();
                $triggers = $statement->fetchAll(PDO::FETCH_CLASS, 'Trigger');
                return $triggers;
        }

        function truncateLog()
        {

                //prune log.
                $configuration = loadConfiguration();
                $hours = 48;
                // switch($configuration->LogLevel)
                // {
                //
                // }

                $db = new DB();
                $sql = "DELETE FROM Log WHERE TimeStamp < DATE_SUB(NOW(2), INTERVAL :interval HOUR)";
                $db->executeWith($sql, ["interval" => $hours]);;
        }
}

?>
