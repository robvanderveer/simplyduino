<?php

require_once __DIR__.'/../Models/SensorTrigger.class.php';
require_once __DIR__.'/../Models/MetricType.class.php';

class DashboardController extends ControllerBase
{
        public function index()
        {
                $triggers = listTriggers();
                $this->model->pageTitle = "Dashboard";
                $this->model->triggers = array_filter($triggers, function($t) { return $t->Triggered; } );

                $this->model->inactiveTriggers = []; //array_filter($triggers, function($t) { return !$t->Triggered; } );
                $this->model->metricTypes = getMetricMeta();

                //see if there
                $todo = getTriggersToEvaluate(10*60);
                if(count($todo) > 0)
                {
                        $this->model->message = "Triggers have not been evaluated for a long time. Check your crontab.";
                }

                $this->model->dashItems = dashboardReport();

                return $this->View("DashboardView.php");
        }


        public function gauges()
        {
            $dashItems = dashboardReport();

            $report = [ "items" => $dashItems, "serverTime" => date('j M Y, H:i:s') ];

            return new JSONViewResult($report);
        }
}
?>
