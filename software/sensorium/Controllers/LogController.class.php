<?php

require_once(__DIR__.'/../Class/db.class.php');
require_once(__DIR__.'/../Class/LogEntry.class.php');

class LogController extends ControllerBase
{
    public function RequiresAuthentication($action)
    {
        return true;
    }

        public function __construct($context)
        {
                parent::__construct($context);
        }

        public function index()
        {
                $this->model->pageTitle = "Log";

                $pageNum = $this->context->getParam("p", 1);
                $logLevel = $this->context->getParam("l", 1);

                $records = $this->getRecordCount($logLevel);
                $pageSize = 20;
                $pages = ceil($records / $pageSize);

                if($pages < 1)
                {
                        $pages = 1;
                }
                if($pageNum < 1)
                {
                        $pageNum = 1;
                }
                if($pageNum > $pages)
                {
                        $pageNum = $pages;
                }

                $start = ($pageNum-1)*$pageSize;

                $entries = $this->getRecords($logLevel, $pageSize, $start);
                $this->model->entries = $entries;
                $this->model->pageNum = $pageNum;
                $this->model->pages = $pages;
                $this->model->logLevel = $logLevel;

                return $this->View("LogView.php");
        }


        function getRecordCount($logLevel)
        {
                $db = new DB();
                $sql = "SELECT COUNT(*) FROM Log WHERE MessageType >= :level";
                $statement = $db->prepare($sql);
                $statement->bindValue("level", $logLevel, PDO::PARAM_INT);
                $statement->execute();
                $count = $statement->fetchColumn();

                return $count;
        }

        function getRecords($logLevel, $pageSize, $start)
        {
                $db = new DB();
                //get an array of all available metrictypes reported for this sensor.
                $sql = "SELECT * FROM Log WHERE MessageType >= :level ORDER BY LogID DESC LIMIT :start, :limit";
                $statement = $db->prepare($sql);
                $statement->bindValue(":start", $start, PDO::PARAM_INT);
                $statement->bindValue(":limit", $pageSize, PDO::PARAM_INT);
                $statement->bindValue(":level", $logLevel, PDO::PARAM_INT);
                $statement->execute();
                $entries = $statement->fetchAll(PDO::FETCH_CLASS, 'LogEntry');
                return $entries;
        }
}
?>
