<?php

require_once(__DIR__.'/../Class/db.class.php');
require_once(__DIR__.'/../Models/Sensor.class.php');

class APIController extends ControllerBase
{
        public function __construct($context)
        {
                parent::__construct($context);
        }

        public function index()
        {
                throw new Exception("no index");
        }

        public function addReading()
        {
            try
            {
                $nodeID = $this->context->getParam("nodeID");
                $metricType = $this->context->getParam("metricID");
                $value = $this->context->getParam("value");

                $sensor_data = getSensorByNodeID($nodeID);

                if(!isset($sensor_data) || $sensor_data == null)
                {
                    //create a new NODE on the fly.
                    $sensorID = insertSensor($nodeID);

                    addlog(LogLevel::Info, "Added new Node with ID '$nodeID'");
                }
                else
                {
                     $sensorID = $sensor_data->SensorID;
                }

                //insert a new sensor reading.
                $db = new DB();
                $statement = $db->prepare("INSERT INTO SensorReading(SensorID, MetricType, TimeStamp, Value)
                       VALUES(:sensorId, :type, NOW(2), :value)");
                   $statement->execute(array(
                       "sensorId" => $sensorID,
                       "type" => $metricType,
                       "value" => $value
                   ));

                $newId = $db->lastInsertId();
                return new JSONViewResult($newId);
            }
            catch(Exception $e)
            {
                addLog(LogLevel::Error, $e->getMessage());
                return new JSONViewResult($e, 400);
            }
        }
}

?>
