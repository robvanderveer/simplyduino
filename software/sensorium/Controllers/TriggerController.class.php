<?php

require_once 'Class/Form.class.php';
require_once 'Models/Sensor.class.php';
require_once 'Models/SensorTrigger.class.php';
require_once 'Models/MetricType.class.php';

class TriggerController extends ControllerBase
{
    public function RequiresAuthentication($action)
    {
        return true;
    }

        public function index()
        {
                $this->model->pageTitle = "Triggers";
                $this->model->triggers = listTriggers();

                return $this->View("TriggerSummaryView.php");
        }

        public function delete()
        {
                $triggerId = $this->context->getParam("triggerId");
                deleteTrigger($triggerId);

                return $this->index();
        }

        public function create()
        {
                $this->model->pageTitle = "Create new trigger";
                $this->model->form = $this->buildForm();
                $this->model->values = [];
                return $this->View("TriggerEditView.php");
        }

        public function edit()
        {
                $triggerId = $this->context->getParam("triggerId");
                $trigger = getTrigger($triggerId);

                $values = [
                        "TriggerID" => $trigger->TriggerID,
                        "SensorID" => $trigger->SensorID,
                        "MetricType" => $trigger->MetricType,
                        "Value" => $trigger->Threshold,
                        "TriggerType" => $trigger->TriggerType,
                        "SecondsBeforeTrigger" => $trigger->SecondsBeforeTrigger,
                        "Triggered" => $trigger->Triggered,
                        "Enabled" => $trigger->Enabled,
                        "LastTriggered" => $trigger->LastTriggered,
                        "LastEvaluated" => $trigger->LastEvaluated
                ];

                $this->model->form = $this->buildForm();
                $this->model->form->addButton("delete", "trigger/delete/{$triggerId}", "Do you really want to remove this trigger?");
                $this->model->values = $values;
                $this->model->pageTitle = "Trigger details";
                return $this->View("TriggerEditView.php");
        }

        //postback only.
        public function save()
        {
                $form = $this->buildForm();
                $form->validate($_POST);
                if($form->isValid())
                {
                        $triggerId = $this->context->getParam("TriggerID", null);

                        if($triggerId == null)
                        {
                                $id = insertTrigger(
                                        $_POST["SensorID"],
                                        $_POST["MetricType"],
                                        $_POST["TriggerType"],
                                        $_POST["Value"],
                                        $_POST["SecondsBeforeTrigger"],
                                        isset($_POST["Enabled"])?1:0
                                );
                                addlog(LogLevel::Info, "Created new trigger '$id'");
                                return new RedirectResult($this->context->appRoot."triggers");
                        }
                        else
                        {
                                $id = $triggerId;
                                updateTrigger($id,
                                        $_POST["SensorID"],
                                        $_POST["MetricType"],
                                        $_POST["TriggerType"],
                                        $_POST["Value"],
                                        $_POST["SecondsBeforeTrigger"],
                                        isset($_POST["Enabled"])?1:0
                                );
                                addlog(LogLevel::Info, "Updated trigger details for '$id'");
                                return new RedirectResult($this->context->appRoot."triggers");
                        }
                }

                $values = $_POST;

                $this->model->pageTitle = "Trigger details";
                $this->model->form = $form;
                $this->model->values = $values;

                return $this->View("TriggerEditView.php");
        }

        private function buildForm()
        {
                $sensors = getSensorsKeyValue();
                $metrics = getMetricTypesKeyValue();

                $form = new EditForm("triggerDetailsForm", "trigger/save");

                $form->addField(new FieldGroup("General", [
                        new StringField("TriggerID",  [ "label" => "ID", "readonly" => 1]),
                        ]));
                $form->addField(new FieldGroup("When", [
                        new SelectionField("SensorID", [ "label" => "Sensor", "options" => $sensors] ),
                        new SelectionField("MetricType", [ "label" => "Metric", "options" => $metrics] ),
                        new OptionField("TriggerType", [ "label" => "Type", "options" => [Trigger::IsAbove => "Above", Trigger::IsBelow => "Below", Trigger::IsEqual => "Equal to", Trigger::IsNotEqual => "Not equal to", Trigger::Heartbeat => "No data received within the delay"] ] ),
                        new StringField("Value", [ "label" => "Threshold value", "placeholder" => "Sensor trigger value", "maxlength" => 4]),
                        new StringField("SecondsBeforeTrigger", [ "label" => "Sample duration", "placeholder" => "Duration in seconds", "maxlength" => 4 ] )
                        ]));
                $form->addField(new FieldGroup("Status", [
                        new CheckboxField("Enabled", [ "label" => "Enabled", "options" => [1 => "Yes", 0 => "No"]]),
                        new CheckboxField("Triggered", [ "label" => "Is triggered", "options" => [1 => "Yes", 0 => "No"], "readonly" => 1]),
                        new StringField("LastTriggered", [ "label" => "Last triggered", "readonly" => 1] ),
                        new StringField("LastEvaluated", [ "label" => "Last evaluated", "readonly" => 1] )
                        ]));

                $form->addValidator(new RequiredFieldValidator("SensorID", "Sensor is required."));
                $form->addValidator(new RequiredFieldValidator("MetricType", "Metric is required."));
                $form->addValidator(new RequiredFieldValidator("SecondsBeforeTrigger", "Delay is required."));
                $form->addValidator(new RegularExpressionFieldValidator("SecondsBeforeTrigger", "Delay must be a positive number.", "/^[0-9]{1,4}$/"));
                $form->addValidator(new RequiredFieldValidator("Value", "Threshold is required."));
                $form->addValidator(new RegularExpressionFieldValidator("Value", "Value must be a positive number.", "/^[0-9]{1,8}$/"));

                return $form;
        }
}
