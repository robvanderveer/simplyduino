<?php

require_once __DIR__.'/../Class/db.class.php';
require_once __DIR__.'/../Class/Form.class.php';
require_once __DIR__.'/../Models/Sensor.class.php';

class SensorController extends ControllerBase
{
    public function RequiresAuthentication($action)
    {
        return true;
    }

        public function index()
        {
                $this->model->pageTitle = "Sensors";
                $this->model->sensors = getSensors();

                return $this->View("SensorSummaryView.php");
        }

        public function delete()
        {
                $sensorId = $this->context->getParam("sensorId");
                deleteSensor($sensorId, true);

                return $this->index();
        }

        public function edit()
        {
                $sensorId = $this->context->getParam("sensorId");

                $sensor = getSensor($sensorId);
                $values = [
                        "SensorID" => $sensor->SensorID,
                        "Description" => $sensor->Description,
                        "NodeID" => $sensor->NodeID,
                        "Added" => $sensor->Added,
                        "LastResponse" => $sensor->LastResponse,
                        "Enabled" => $sensor->IsEnabled,

                ];

                $this->model->form = $this->createForm();
                $this->model->form->addButton("delete", "sensor/delete/{$sensorId}", "All triggers and recorded data for this sensor will be deleted. Are you sure to delete this sensor?");
                $this->model->form->addButton("chart", "chart/{$sensorId}");
                $this->model->pageTitle = "Sensor details";
                $this->model->values = $values;

                return $this->View("SensorEditView.php");
        }

        public function save()
        {
                //Warning: Note the upper case postback parameter. Refactor me.
                $sensorId = $_POST["SensorID"];

                $form = $this->createForm();
                $form->validate($_POST);
                if($form->isValid())
                {
                        $id = $_POST["SensorID"];
                        updateSensor($id, $_POST["Description"], $_POST["NodeID"]);
                        addlog(LogLevel::Info, "Updated sensor details for '$id'");

                        return new RedirectResult($this->context->appRoot."sensors");
                }

                $this->model->pageTitle = "Sensor details";
                $this->model->form = $form;
                $this->model->values = $_POST;
                return $this->View("SensorEditView.php");
        }

        private function createForm()
        {

                $form = new EditForm("sensorDetailsForm", Routing::MakeLink("SensorSave"));

                $form->addField(new FieldGroup("General", [
                        new StringField("SensorID",  [ "label" => "ID", "readonly" => 1]),
                        new StringField("Added", [ "label" => "Date added", "readonly" => 1] ),
                        new StringField("Description", [ "label" => "Description", "maxlength" => 50] )
                        ]));
                $form->addField(new FieldGroup("Network details", [
                        new StringField("NodeID", [ "label" => "Node ID", "maxlength" => 8] ),
                        new StringField("LastResponse", [ "label" => "Last response", "readonly" => 1 ])
                        ]));
                $form->addField(new FieldGroup("Status", [
                        new CheckboxField("Enabled", [ "label" => "Enabled", "options" => [1 => "Yes", 0 => "No"]]),
                        ]));

                $form->addValidator(new RequiredFieldValidator("Description", "A description is required."));
                $form->addValidator(new RequiredFieldValidator("NodeID", "The NodeID is required."));
                $form->addValidator(new RegularExpressionFieldValidator("NodeID", "The NodeID must be 8 hexadecimal characters.", "/^[0-9A-Za-z]{1,8}$/"));
                return $form;
        }
}
