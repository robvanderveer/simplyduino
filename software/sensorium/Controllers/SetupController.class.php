<?php

require_once(__DIR__.'/../Class/Configuration.class.php');
require_once(__DIR__.'/../Class/Form.class.php');

class SetupController extends ControllerBase
{
        public function redirectToSettings()
        {
                return new RedirectResult($this->context->appRoot."settings");
        }

        public function index()
        {
            //TODO: block this when the system is properly configured

                $form = $this->constructForm();

                $this->model->pageTitle = "Configure";
                $this->model->form = $form;
                return $this->View("InstallerView.php");
        }

        public function update()
        {
            $this->model->pageTitle = "Update";

            $form = $this->constructUpdateForm();
            if($form->IsPostBack())
            {
                //perform the update with the arguments.
                $db = new DB(true);
                if($db)
                {
                    $db->beginTransaction();
                    $this->updateSql($db);
                    $db->commit();

                    $this->model->message = "Datebase updated completed.";
                }
                else
                {
                    //build the form.
                    $this->model->message = "Unable to connect.";
                }
            }
            $this->model->form = $form;
            return $this->View("InstallerView.php");
        }

        public function createDatabase()
        {
                $form = $this->constructForm();
                if($form->IsPostBack() == false)
                {
                        return new HttpResponseResult(403);
                }

                $form->validate($_POST);
                if($form->isValid())
                {
                        //connect and create the database.
                        $pdo = $this->connect($_POST["RootName"],$_POST["RootCode"]);
                        if($pdo)
                        {
                                //create the database.
                                $sql = "CREATE DATABASE IF NOT EXISTS `".DATABASE_NAME."`;";
                                $pdo->exec($sql);

                                //grant all on `database`.* to 'user'@'localhost' identified by 'password';
                                $sql = "GRANT ALL PRIVILEGES ON `".DATABASE_NAME."`.* TO '".DATABASE_USERNAME."'@'localhost' IDENTIFIED BY '".DATABASE_PASSWORD."'";
                                $pdo->exec($sql);

                                //make sure.
                                $sql = "GRANT ALL PRIVILEGES ON `".DATABASE_NAME."`.* TO '".DATABASE_USERNAME."'@'%' WITH GRANT OPTION";
                                $pdo->exec($sql);


                                $pdo->beginTransaction();

                                $sql = "USE `".DATABASE_NAME."`";
                                $pdo->exec($sql);

                                $this->createTables($pdo);

                                $pdo->commit();

                                //rebuild the form for the changes.
                                $form = $this->constructForm();
                        }
                        else
                        {
                                //error was populated.
                                //exit("db ok");
                        }
                }

                $this->model->pageTitle = "Configure";
                $this->model->form = $form;
                return $this->View("InstallerView.php");
        }

        private function connect($user, $pass)
        {
                try
                {
                        //we have all the data. Execute the step and refresh.
                        //try again to connect, but use a custom BDO.
                        $dsn = "mysql:host=".DATABASE_HOST;
                        $pdo = new PDO($dsn,$user, $pass);
                        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                        return $pdo;
                }
                catch(Exception $e)
                {
                        $this->model->message = "Connect failed: ".$e->getMessage();
                        return null;
                }
        }

        private function constructForm()
        {
                //print the form to say welcome and proceed.
                $this->model->introduction = "Thank you for installing Sensorium. You have reached the configure assistant for Sensorium. We need just a few simple things to configure the database after we perform a few basic tests.";
                $this->model->message = "";
                $values = $_POST;

                $form = new EditForm("Configure", "configure");
                $form->addField(new FieldGroup("Configuration", [
                        new LiteralField(DATABASE_HOST, [ "label" => "Server"]),
                        new LiteralField(DATABASE_NAME, [ "label" => "Database"]),
                        new LiteralField(DATABASE_USERNAME, [ "label" => "Username"]),
                        new LiteralField("********", [ "label" => "Password"]),
                        ]));

                //manually connect to the database here.
                $db = new DB(true);
                if(!$db->isValid())
                {
                        $form->addField(new FieldGroup("Database", [
                                new LiteralField("Could not connect to the database. Please enter credentials to create table structure."),
                                new StringField("RootName", [ "label" => "Username", "maxlength" => 128]),
                                new PasswordField("RootCode", [ "label" => "Password", "maxlength" => 128] )
                                ]));

                        $form->addValidator(new RequiredFieldValidator("RootName", "Username is required."));
                        $form->addValidator(new RequiredFieldValidator("RootCode", "Password is required."));
                        $form->submitTitle = "Create";
                        $form->setAction("configure/createDatabase");
                }
                else
                {
                        //we have a database that can be connected to.
                        $form->addField(new FieldGroup("Database", [
                                new LiteralField("Database connection succeeded.")
                                ]));

                        $configuration = loadConfiguration();
                        $form->addField(new FieldGroup("Settings", [
                                new LiteralField($configuration->HomeURL, [ "label" => "Site URL"]),
                                ]));

                        $form->addField(new LiteralField("We're almost done! Continue to adjust your settings."));
                        $form->submitTitle = "Finish";
                        $form->setAction("configure/redirectToSettings");
                }

                $this->model->values = $values;
                return $form;
        }

        private function constructUpdateForm()
        {
                //print the form to say welcome and proceed.
                $this->model->introduction = "Enter credentials with update permissions to update the database.";
                $this->model->message = "";
                $values = $_POST;

                $form = new EditForm("Configure", "configure");
                $form->addField(new FieldGroup("Connection", [
                        new LiteralField(DATABASE_HOST, [ "label" => "Server"]),
                        new LiteralField(DATABASE_NAME, [ "label" => "Database"]),
                        new LiteralField(DATABASE_USERNAME, [ "label" => "Username"]),
                        new LiteralField("********", [ "label" => "Password"]),
                        ]));

                $form->submitTitle = "Next";
                $form->setAction("configure/update");

                $this->model->values = $values;
                return $form;
        }

        private function updateSql($db)
        {
            $sql ="CREATE TABLE IF NOT EXISTS Users (
              UserName varchar(40) NOT NULL,
              PasswordHash varchar(255) NOT NULL,
              Email varchar(128) NOT NULL,
              LastLogin timestamp,
              Created DateTime NOT NULL,
              PRIMARY KEY (UserName)
            );";
            $db->execute($sql);

            //insert account when no rows exist.
            $statement = $db->prepare("SELECT * FROM Users");
            $statement->execute();
            $accounts = $statement->fetchAll();
            if(!$accounts)
            {
                $pass = password_hash("admin", PASSWORD_DEFAULT);

                $sql = "INSERT INTO Users (UserName, PasswordHash, Email, Created) VALUES('Admin', :Password, '', NOW(2) );";
                $db->executeWith($sql, ["Password" => $pass]);
            }

            $sql ="CREATE TABLE IF NOT EXISTS `SensorReading_Daily` (
              `SensorID` int(11) NOT NULL,
              `MetricType` int(11) NOT NULL,
              `TimeStampRounded` datetime NOT NULL,
              `Average` decimal(14,4) DEFAULT NULL,
              PRIMARY KEY (SensorID, MetricType, TimeStampRounded)
            );";
            $db->execute($sql);


            $sql ="CREATE TABLE IF NOT EXISTS `SensorReading_Hourly` (
              `SensorID` int(11) NOT NULL,
              `MetricType` int(11) NOT NULL,
              `TimeStampRounded` datetime NOT NULL,
              `Average` decimal(14,4) DEFAULT NULL,
              PRIMARY KEY (SensorID, MetricType, TimeStampRounded)
            );";
            $db->execute($sql);
        }

        private function createTables($db)
        {
            $sql = "CREATE TABLE IF NOT EXISTS Config (
                Name varchar(40) NOT NULL,
                Value varchar(200) NOT NULL,
                PRIMARY KEY (Name)
            );";

            $db->exec($sql);

            $sql ="CREATE table IF NOT EXISTS Sensor(
            SensorID INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
            Description VARCHAR( 50 ) NOT NULL,
            NodeID varchar(8) NOT NULL,
            LastResponse DATETIME,
            Added DATETIME NOT NULL,
            IsEnabled BIT NOT NULL);" ;
            $db->exec($sql);

            $sql = "CREATE TABLE IF NOT EXISTS Log(
            LogID INT(11) AUTO_INCREMENT PRIMARY KEY,
            TimeStamp DATETIME(2) NOT NULL,
            MessageType SMALLINT NOT NULL,
            Message VARCHAR(1000) NOT NULL);";
            $db->exec($sql);

            $sql ="CREATE table IF NOT EXISTS MetricType (
            MetricID INT( 11 ) PRIMARY KEY,
            Description VARCHAR(50) NOT NULL,
            `Units` varchar(20) NOT NULL,
            `Low` int(11) NOT NULL DEFAULT '0',
            `High` int(11) NOT NULL DEFAULT '1023');";
            $db->exec($sql);

            //insert the base sensor types.
            $db->exec("INSERT INTO MetricType (MetricID, Description,Units,Low,High) VALUES
            (1, 'CPU Load', '%', 0, 150),
            (2, 'Available Memory', 'bytes', 0, 1600000),
            (10, 'Boot signal', '', 0, 1023),
            (34, 'Moisture Level', '', 550, 650),
            (35, 'Battery Level', 'volt', 0, 5),
            (100, 'Temperature 1', 'ºC', 0, 100),
            (101, 'Temperature 2', 'ºC', 0, 100),
            (102, 'Temperature 3', 'ºC', 0, 100);");

            $sql ="CREATE table IF NOT EXISTS SensorReading(
            ReadingID INT( 11 ) AUTO_INCREMENT PRIMARY KEY,
            SensorID INT NOT NULL,
            MetricType INT NOT NULL,
            TimeStamp DATETIME(2) NOT NULL,
            Value INT,

            INDEX(SensorID),
            INDEX(MetricType),

            FOREIGN KEY(SensorID)
            REFERENCES Sensor(SensorID),

            FOREIGN KEY(MetricType)
            REFERENCES MetricType(MetricID)

            );" ;
            $db->exec($sql);


            $sql ="CREATE TABLE IF NOT EXISTS `SensorTrigger` (
              `TriggerID` int(11) NOT NULL AUTO_INCREMENT,
              `SensorID` int(11) NOT NULL,
              `MetricType` int(11) NOT NULL,
              `TriggerType` int(11) NOT NULL,
              `Threshold` int(11) NOT NULL,
              `Enabled` tinyint(1) NOT NULL,
              `Triggered` tinyint(1) NOT NULL,
              `LastTriggered` DATETIME(2) DEFAULT NULL,
              `LastEvaluated` DATETIME(2) DEFAULT NULL,
              `SecondsBeforeTrigger` int(11) NOT NULL,
              PRIMARY KEY (`TriggerID`),
              KEY `ix_sensor` (`SensorID`),
              KEY `ix_metric` (`MetricType`),
              CONSTRAINT `fk_metric` FOREIGN KEY (`MetricType`) REFERENCES `MetricType` (`MetricID`),
              CONSTRAINT `fk_sensor` FOREIGN KEY (`SensorID`) REFERENCES `Sensor` (`SensorID`)
            )";
            $db->exec($sql);

        }
}
