<?php

require_once(__DIR__.'/../Class/Configuration.class.php');
require_once(__DIR__.'/../Class/Form.class.php');
require_once(__DIR__.'/../Class/LogEntry.class.php');
require_once __DIR__.'/../Extras/PHPMailerAutoload.php';
require_once __DIR__.'/../Models/SensorTrigger.class.php';

class SettingsController extends ControllerBase
{
        public function __construct($context)
        {
                parent::__construct($context);
        }

        public function RequiresAuthentication($action)
        {
            return true;
        }

        public function index()
        {
            $configuration = loadConfiguration();
            if(!$configuration)
            {
                    //return new RedirectResult()
                    throw new Exception("no config");
            }

            $this->model->pageTitle = "Settings";
            $this->model->form = $this->createForm();
            $this->model->values = [
                     "APIKey" => $configuration->APIKey,
                     "HomeURL" => $configuration->HomeURL,
                     "TimeZone" => $configuration->TimeZone,
                     "SMTPServer" => $configuration->SMTPServer,
                     "SMTPAccount" => $configuration->SMTPUsername,
                     "SMTPPassword" => $configuration->SMTPPassword,
                     "NotificationRecipient" => $configuration->NotificationRecipient,
                     "NotificationSubject" => $configuration->NotificationSubject,
                     "LogLevel" => $configuration->LogLevel
                     ];

            return $this->View("SettingsView.php");
        }

        public function test()
        {
            $configuration = loadConfiguration();
            $this->model->pageTitle = "Settings";
            $this->model->form = $this->createForm();
            $this->model->values = [
                     "APIKey" => $configuration->APIKey,
                     "HomeURL" => $configuration->HomeURL,
                     "TimeZone" => $configuration->TimeZone,
                     "SMTPServer" => $configuration->SMTPServer,
                     "SMTPAccount" => $configuration->SMTPUsername,
                     "SMTPPassword" => $configuration->SMTPPassword,
                     "NotificationRecipient" => $configuration->NotificationRecipient,
                     "NotificationSubject" => $configuration->NotificationSubject,
                     "LogLevel" => $configuration->LogLevel
                     ];


             $triggerType = "TEST TRIGGER";
             $subject = "TEST MESSAGE";
             $homeUrl = loadConfiguration()->HomeURL;
             $logoImageSrc = $homeUrl."/Images/Sensorium.png";
             $poweredByImageSrc = $homeUrl."/Images/PoweredBySimplyduino.png";
             $logoImageSrc2 = $homeUrl."/Images/Sensorium@2x.png";
             $poweredByImageSrc2 = $homeUrl."/Images/PoweredBySimplyduino@2x.png";
             $triggerID = "0";

             //construct the message by loading running a template
             ob_start();
             require __DIR__.'/../Templates/NotificationTemplate.php';
             $message = ob_get_contents();
             ob_end_clean();

            $mail = new PHPMailer;
            //$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $configuration->SMTPServer;               // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $configuration->SMTPUsername;                 // SMTP username
            $mail->Password = $configuration->SMTPPassword;                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->From = 'sensorium@simplicate.info';
            $mail->FromName = 'Sensorium';
            $mail->addAddress($configuration->NotificationRecipient);     // Add a recipient
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $configuration->NotificationSubject.' TEST';
            $mail->Body    = $message;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
            // $mail->AddEmbeddedImage(__DIR__.'/../Images/Sensorium.png', 'logo_sensorium', 'attachment', 'base64', 'image/png');
            // $mail->AddEmbeddedImage(__DIR__.'/../Images/PoweredBySimplyduino.png', 'logo_powered', 'attachment', 'base64', 'image/png');

            if(!$mail->send()) {
                $this->model->form->addMessage('Message could not be sent. Mailer Error: ' . $mail->ErrorInfo);
                addLog(LogLevel::Error, "Unable to send email: $mail->ErrorInfo");

            } else {
                $this->model->form->addMessage('Message has been sent');
            }

            return $this->View("SettingsView.php");
        }

        public function save()
        {
                $form = $this->createForm();
                $form->validate($_POST);

                if($form->isValid())
                {
                        addlog(LogLevel::Info, "Updated settings");

                        $c = loadConfiguration();
                        $c->APIKey = $_POST["APIKey"];
                        $c->TimeZone = $_POST["TimeZone"];
                        $c->LogLevel = $_POST["LogLevel"];
                        $c->SMTPServer = $_POST["SMTPServer"];
                        $c->SMTPUsername = $_POST["SMTPAccount"];
                        if($_POST["SMTPPassword"] != "")
                        {
                            $c->SMTPPassword = $_POST["SMTPPassword"];
                        }
                        $c->HomeURL = $_POST["HomeURL"];
                        $c->NotificationRecipient = $_POST["NotificationRecipient"];
                        $c->NotificationSubject = $_POST["NotificationSubject"];
                        $c->save();

                        $this->model->message = "Changes saved";
                }

                $this->model->pageTitle = "Settings";
                $this->model->form = $form;
                $this->model->values = $_POST;

                return $this->View("SettingsView.php");
        }

        private function createForm()
        {
            $timezones =  DateTimeZone::listIdentifiers(DateTimeZone::ALL);
            //listIdentifiers returns array with [0] => "ID", but we want "ID" => "ID";
            $timezones = array_combine($timezones, $timezones);

                $form = new EditForm("settingsForm", $this->LinkSelf(["action" => "save"]));
                $form->addField(new FieldGroup("System", [
                        new LiteralField(DATABASE_HOST, [ "label" => "Database host"]),
                        new LiteralField(DATABASE_NAME, [ "label" => "Database name"]),
                        new SelectionField("TimeZone", [ "label" => "Time Zone", "options" => $timezones] ),
                        new StringField("HomeURL",  [ "label" => "Public site URL", "maxlength" => 64]),
                        ]));

                $form->addField(new FieldGroup("API Access", [
                        new StringField("APIKey",  [ "label" => "API key", "placeholder" => "API Key", "maxlength" => 8]),
                //        new StringField("APIClient", [ "label" => "Decidated Client", "placeholder" => "IP Address"] )
                        ]));
                $form->addField(new FieldGroup("Mail Server", [
                //        new CheckboxField("SMTPEnabled", [ "label" => "Enabled", "options" => [1 => "Yes", 0 => "No"]]),
                        new StringField("SMTPServer", [ "label" => "Server", "placeholder" => "IP Address"] ),
                        new StringField("SMTPAccount", [ "label" => "Account", "placeholder" => "Username"] ),
                        new PasswordField("SMTPPassword", [ "label" => "Password", "placeholder" => "Password"]),
                        //new HtmlField("<a href='' class='button'>test mail</a>", [ "label" => ""])
                        ]));

                $form->addField(new FieldGroup("Logging", [
                    //    new OptionField("LoggingRetention", [ "label" => "Retention", "options" => [0 => "One day", 1 => "One week", 2 => "One month", 3 => "One year"]]),
                        new OptionField("LogLevel", [ "label" => "Level", "options" => [0 => "Trace", 1 => "Info, warnings and Errors", 2 => "Errors and warnings", 3 => "Errors only"]])
                    //    new CheckboxField("LoggingNotify", [ "label" => "Notify", "placeholder" => "Send email on fatal errors"])
                        ]));

                $form->addField(new FieldGroup("Notifications", [
                    new StringField("NotificationRecipient", [ "label" => "Recipient"]),
                    new StringField("NotificationSubject", [ "label" => "Subject prefix"])
                    ]));

                $form->addValidator(new RequiredFieldValidator("HomeURL", "Home Url is required."));
                $form->addValidator(new RequiredFieldValidator("APIKey", "API Key is required."));
                $form->addValidator(new RegularExpressionFieldValidator("APIKey", "API Key must be 8 hexadecimal characters.", "/^[0-9|A-Z|a-z]{8}$/"));
                $form->addValidator(new RegularExpressionFieldValidator("APIClient", "Not a valid IP address.", "/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\$/"));
                $form->addValidator(new RequiredFieldValidator("SMTPServer", "SMTP Server is required."));
                $form->addValidator(new RequiredFieldValidator("SMTPAccount", "The Account name is required."));
                //$form->addValidator(new RegularExpressionFieldValidator("SMTPServer", "Not a valid IP address.", "/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\$/"));
                //$form->addValidator(new RequiredFieldValidator("SMTPPassword", "A password is required for notifications."));
                $form->addValidator(new RequiredFieldValidator("NotificationRecipient", "An email address is required for notifications."));

                $form->addButton("Test Notify", "settings/test", "This test uses saved changed. Make sure you save your changes first. Yeah, I will get this fixed later.");
                return $form;
        }
}
?>
