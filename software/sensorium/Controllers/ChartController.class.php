<?php

require_once __DIR__.'/../Models/Sensor.class.php';
require_once __DIR__.'/../Models/MetricType.class.php';
require_once __DIR__.'/../Models/SensorReading.class.php';

class ChartController extends ControllerBase
{
        public function index()
        {
                $sensorId = $this->context->getParam("sensorId");
                $hours = $this->context->getParam("h", 8);
                $round = $this->context->getParam("r", 300);

                $toDate = new DateTime();
                $fromDate = new DateTime();
                $fromDate->sub( new DateInterval("PT{$hours}H") ); //P1D

                $sensor_data = getSensor($sensorId);
                $metricTypes = getMetricMeta();


                $charts = [];
                foreach(getMetricsForSensor($sensorId) as $metric)
                {
                        $chart = [];

                        $chart["id"] = $sensorId.'_'.$metric->MetricType;
                        $chart["meta"] = $metricTypes[$metric->MetricType];
                        $chart["metric"] = $metric->MetricType;
                        $chart["description"] = $metric->Description;
                        $chart["data"] = getDataForMetric($sensorId, $metric->MetricType, $fromDate, $toDate, $round);
                        $charts[] = $chart;
                }

                $dateformat = "dd/MM/yyyy";
                if($hours <= 48)
                {
                        $dateformat = "E, HH:mm";
                }
                if($hours <= 24)
                {
                        $dateformat = "HH:mm";
                }

                $this->model->charts = $charts;
                $this->model->fromDate = $fromDate;
                $this->model->toDate = $toDate;
                $this->model->dateFormat = $dateformat;
                $this->model->sensorId = $sensorId;
                $this->model->hours = $hours;
                $this->model->sensorDescription = $sensor_data->Description;
                $this->model->pageTitle = "Chart";
                return $this->View("ChartView.php");
        }
}

?>
