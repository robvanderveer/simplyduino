<?php

//required for commandline Access.

require_once __DIR__.'/Framework/Register.php';

$context = new RequestContext(["PHP_SELF" => __DIR__.'/scheduler.php', "REQUEST_METHOD" => "GET"], __DIR__."/scheduler.php");
$scheduler = new ScheduleController($context);
$output = $scheduler->index();
$output->execute();

?>
