<?php

require_once __DIR__.'/../Framework/Register.php';

$route = new Route("trigger/edit/{triggerId}", "TriggerController", [ "triggerId" => 4] );
assert($route->isMatch("trigger/edit/id") == true, "should match");
assert($route->isMatch("trigger/edit") == true, "Should accept the default");
assert($route->isMatch("trigger") == false, "Missing action");
assert($route->isMatch("home") == false, "wrong controller");
assert($route->isMatch("") == false, "no controller");
assert($route->isMatch("trigger/home/id") == false, "wrong action");

//non matching default.
$route = new Route("trigger/edit/{triggerId}", "TriggerController", [ "id" => 4] );
assert($route->isMatch("trigger/edit") == false, "Missing default value in route");

//do not match surplus parametes
$route = new Route("trigger", "TriggerController");
assert($route->isMatch("trigger/edit") == false, "Should not match additional parameters");

//missing default.
$route = new Route("trigger/edit/{triggerId}", "TriggerController");
assert($route->isMatch("trigger/edit") == false, "Missing default value in route");

//empty substrings.
$route = new Route("trigger/{id1}/{id2}", "TriggerController");
assert($route->isMatch("trigger//2") == false, "Cannot subdirectory no defaults.");

// TODO later: allow NULL optionals.
// assert($route->isMatch("trigger") == true, "optional chain id1 through id2 with no defaults should be allowed");

//global pattern with override action.
$route = new Route("{controller}/{action}/{id}", "TriggerController", [ "action" => "edit", "id" => 4] );
assert($route->isMatch("") == false, "No default controller");
assert($route->isMatch("trigger") == true, "No default action");
assert($route->isMatch("trigger/edit") == true, "Should use default ID");
assert($route->isMatch("trigger/edit/5") == true, "Full route");

$route = new Route("trigger/{id1}/{id2}", "TriggerController");
assert('$route->makeUrl(["id1" => 18, "id2" => "ape"]) == "trigger/18/ape"');
assert('$route->makeUrl(["id1" => 18, "id2" => "ape", "extra" => "data"]) == "trigger/18/ape?extra=data"');
assert('$route->makeUrl(["id1" => 18, "id2" => "ape", "extra" => "data", "more" => "yes"]) == "trigger/18/ape?extra=data&more=yes"');
assert('$route->makeUrl(["id1" => "&?", "id2" => "ape", "encode" => "<html>"]) == "trigger/%26%3F/ape?encode=%3Chtml%3E"');

?>
