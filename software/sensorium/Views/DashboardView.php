<?php

include(__DIR__.'/header.php');

echo "<h1>$pageTitle</h1>";

if(count($triggers) == 0)
{
        echo "<p>Everything is fine<p>";
}
else
{
        echo "<h3>Active notifications</h3>";
        echo "<ul class='objects'>";
        foreach($triggers as $trigger)
        {
                echo "<li class='bad'><label>".$trigger->LastTriggered."</label><span>{$trigger->SensorDescription} reported that {$trigger->MetricDescription} {$trigger->activeDescription()}";
                echo "&nbsp;<a href='".Routing::MakeLink("Chart", ["sensorId" => $trigger->SensorID])."' class='button'>chart</a>";
                echo "</span></li>";
        }
        echo "</ul>";

}

if(count($inactiveTriggers) > 0)
{
    echo "<h3>Inactive</h3>";
    echo "<ul class='objects'>";
    foreach($inactiveTriggers as $trigger)
    {
            echo "<li class='good'><label>".$trigger->LastTriggered."</label><span>{$trigger->MetricDescription} of {$trigger->SensorDescription} {$trigger->inactiveDescription()}</span></li>";
    }
    echo "</ul>";
}

if(isset($message))
{
        echo "<div class='errormessages'>$message</div>";
}

?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="Scripts/highcharts/highcharts.js"></script>
<script src="Scripts/highcharts/highcharts-more.js"></script>
<script src="Scripts/highcharts/modules/solid-gauge.js"></script>
<div></div><!-- this div needs to be here for jQuery bug messing up the anchors -->
<?php

foreach($dashItems as $dashItem)
{
    $chartID = 'gauge'.$dashItem["TriggerID"];
    $meta = $metricTypes[$dashItem["MetricType"]];
?>
<a href="chart/<?php echo $dashItem["SensorID"]; ?>">
<div id="<?php echo $chartID; ?>" class="gauge" ></div>
</a>
<script language="javascript">
$(function () {
    var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            backgroundColor: '#F8F8F8'
        },

        title: null,
        colors: ['#7437CE', 'unused'],

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: '#F1EEF5',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // The speed gauge
    $('#<?php echo $chartID ?>').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: <?php echo $meta->Low; ?>,
            max: <?php echo $meta->High; ?>,
            title: {
                text: '<?php echo str_replace("'", "\'", $dashItem["MetricDescription"]);?> on <?php echo str_replace("'", "\'", $dashItem["SensorDescription"]) ;?>',
                style: { 'font-family': 'Avenir-Book', 'font-size': '11pt' }
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'Value',
            data: [<?php echo $dashItem["LastValue"]; ?>],
            dataLabels: {
                format: '<div style="text-align:center; font-family: \'Avenir-Book\'; font-size: 14pt;">{y}<br/>' +
                       '<?php echo htmlentities($meta->Units); ?></div>'
            },
            tooltip: {
                valueSuffix: ' km/h'
            }
        }]

    }));

    });
</script>
<?php
} //end foreach $dashboardItem
?>
<script>
// Bring life to the dials
    setInterval(function () {

        //perform the query.
        $.ajax({
                url: "dashboard/gauges"
            }).then(function(data) {
                for(g in data.items)
                {
                    chartId = "#gauge"+data.items[g].TriggerID;
                    newValue = parseFloat(data.items[g].LastValue);

                    chart = $(chartId).highcharts();
                    if (chart) {
                        point = chart.series[0].points[0];
                        point.update(newValue);
                    }
                }

                $("#serverTime").text(data.serverTime);

            });

    }, 5000);
</script>
<?php

include(__DIR__.'/footer.php');
?>
