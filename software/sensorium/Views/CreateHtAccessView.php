<?php

include __DIR__.'/header.php';

?>
<h1>Setup</h1>
<p>
Before we can continue, you need to modify or create the .htaccess file in the root of this web application. Copy and paste the contents below.
</p>
<p>
<textarea cols="50" rows="18" onclick="this.select();">
# blocks auto-guessing for .php, .css, etc.
Options -Multiviews

<IfModule mod_rewrite.c>

# map to this folder.
RewriteBase <?php echo dirname($_SERVER["PHP_SELF"]).PHP_EOL;  ?>
RewriteEngine On

SetEnv HTACCESS_CONFIGURED On

RewriteRule \.(css|jpg|png|woff|js)$ - [L]
RewriteRule ^(.*)$ index.php [QSA,NC,L]
</IfModule>

</textarea>
</p>
<p>
<a href="configure" class="button">Try Again</a>
</p>
<?php
include __DIR__.'/footer.php';
?>
