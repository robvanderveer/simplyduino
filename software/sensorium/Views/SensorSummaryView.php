<?php

$PageTitle = "Sensors";

include('header.php');

require_once 'Models/Sensor.class.php';

echo "<h1>$pageTitle</h1>";
echo "<ul class='objects'>";
foreach($sensors as $sensor)
{
    $editLink = Routing::MakeLink("SensorEdit", ["sensorId" => $sensor->SensorID]);
    $chartLink = Routing::MakeLink("Chart", ["sensorId" => $sensor->SensorID]);

        echo "<li><a href='$editLink'>{$sensor->Description}</a>";
        echo " <a class='button' href='$chartLink'>chart</a>";
        echo "</li>";

}
echo "</ul>";
echo "<p>New sensors are automatically registered when they report to the hub.</p>";

include 'footer.php';
?>
