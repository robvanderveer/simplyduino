<?php

include __DIR__.'/header.php';

echo "<h1>$pageTitle</h1>";
echo "<div>$introduction</h1>";

$form->render(true, $values);
if(isset($message))
{
        echo "<div class='errormessages'>$message</a>";
}

include __DIR__.'/footer.php';
?>
