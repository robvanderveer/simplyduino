<?php

include('header.php');

?>
        <!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="Scripts/highcharts/highcharts.js"></script>
<script>
Highcharts.setOptions({
        global: {
            useUTC: false

        }
    });
</script>
<h1><?php echo $sensorDescription; ?></h1>
<?php

foreach($charts as $chart)
{
	$chartID = 'line'.$chart["id"];
    $meta = $chart["meta"];
?>
<!--Div that will hold the chart-->
<div id="<?php echo $chartID ?>" class="chart" style="min-width:220px; height:200px;"></div>


<script type="text/javascript">
$(function () {
$('#<?php echo $chartID ?>').highcharts({
chart: {
	type: 'line',
    plotBackgroundColor: '#F1EEF5',
    backgroundColor: '#F8F8F8',
    marginLeft: 50,
    spacing: [0,0,0,0]
},
credits: {
    enabled: false
  },
title: {
	text: '<?php echo $chart["description"] ?>',
    style: { "font-family": 'Avenir-Book', "fontSize" : '11pt', 'font-weight' :'normal'  },
    align: 'left',
    margin: 0,
    x: 50
},
colors: ['#7437CE','blue','yellow'],
legend: { enabled: false },
xAxis: {
	type: 'datetime',
    labels: { style: { "font-family": 'Avenir-Book', "fontSize" : '9pt', 'font-weight' :'normal'  }},
    title: 'none',
	min: '<?php echo $fromDate->getTimestamp()*1000 ?>',
    max: '<?php echo $toDate->getTimestamp()*1000 ?>'
},
yAxis: {
    title: { enabled: false, text: '<?php echo $meta->Units;?>'},
    labels: { style: { "font-family": 'Avenir-Book', "fontSize" : '9pt', 'font-weight' :'normal'  }},
//	min: 0
    floor: 0
},
tooltip: {
	headerFormat: '<b><?php echo $chart["description"] ?></b><br>',
    useHTML: true,
	pointFormat: '{point.x:%d %B %y %H:%M:%S}<br/><b>{point.y:%f}</b> <?php echo htmlentities($meta->Units);?>'
},

plotOptions: {
	line: {
        animation: false,
        lineWidth: 2,
		marker: {
            radius: 2,
			enabled: true
		}
	}
},

series: [{
	name: '<?php echo $chart["description"] ?>',
	data: [
        <?php

        foreach($chart['data'] as $r) {
                $t = strtotime($r->TimeStampRounded);
                $msecs = $t * 1000;


                echo '['.$msecs.','.$r->Average.'],'.PHP_EOL;
            }
        ?>
	]
}]
});
});
</script>
<?php
} //end foreach $metric

echo "<div align='right'>";
//render scale links.
echo "<a class='button".(($hours==8)?" current":"")."' href='chart/$sensorId'>default</a>";
echo "<a class='button".(($hours==1)?" current":"")."' href='chart/$sensorId?h=1&r=1'>hour</a>";
echo "<a class='button".(($hours==24)?" current":"")."' href='chart/$sensorId?h=24&r=1200'>day</a>";
echo "<a class='button".(($hours==168)?" current":"")."' href='chart/$sensorId?h=168&r=3600'>week</a>";
echo "<a class='button".(($hours==744)?" current":"")."' href='chart/$sensorId?h=744&r=14400'>month</a>";
echo "<a class='button".(($hours==8736)?" current":"")."' href='chart/$sensorId?h=8736&r=86400'>year</a>";
echo "</div>"
;

include 'footer.php';
?>
