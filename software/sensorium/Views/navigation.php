<?php
?>
<nav>
<ul>
	<li><a href="dashboard">dashboard</a></li>
	<li><a href="triggers">triggers</a></li>
	<li><a href="sensors">sensors</a></li>
	<li><a href="log">log</a></li>
<?php
	if(Authentication::IsAuthenticated())
	{
?>
<li><a href="settings">settings</a></li>
<li><a href="logout">sign out</a></li>
<?php
	}
	else
    {
?>
<li><a href="login">sign in</a></li>
<?php
	}
?>

</ul>
</nav>
