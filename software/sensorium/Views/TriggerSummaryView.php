<?php

include 'header.php';

echo "<h1>$pageTitle</h1>";

echo "<ul class='objects'>";
foreach($triggers as $trigger)
{
        echo "<li>{$trigger->MetricDescription} of {$trigger->SensorDescription} ";
        echo " <a href='trigger/edit/{$trigger->TriggerID}'>{$trigger->activeDescription()}</a>";
        if(!$trigger->Enabled)
        {
            echo " DISABLED";
        }
        echo "</li>";
}
echo "</ul>";
echo "<p><a class='button' href='trigger/create'>create new trigger</a></p>";


include 'footer.php';
?>
