<?php

include __DIR__.'/header.php';

?>
<h1>Setup</h1>
<p>You need a config.php with specific settings for your environment. I've filled in the defaults but your mileage may differ.</p>
<p>
<textarea cols="50" rows="18" onclick="this.select();">
<?php echo $contents; ?>
</textarea>
</p>
<p>
Copy this text, adjust it and save it to the root of the sensorium web application.
</p>
<a href="configure" class="button">Try Again</a>
<?php
include __DIR__.'/footer.php';
?>
