<?php

include(__DIR__.'/header.php');

//replaced '/' by '/&shy';
function softwrap($string, $separator)
{
    return str_replace($separator, $separator.'&shy;', $string);
}


function showPages($currentPage, $totalPages, $url, $logLevel)
{
        if($totalPages > 1)
        {
                echo "<nav class='paging'><ul>";
                echo '<li><a href="'.$url.'?p=1" class="'.((1 == $currentPage)?'first':'').'">first</a></li>';

                $maxPages = 10;
                $offset = $currentPage - ($maxPages / 2);
                if($offset < 0)
                {
                        $offset = 0;
                }
                if($totalPages > $maxPages && $offset > $totalPages - $maxPages)
                {
                        $offset = $totalPages - $maxPages;
                }

                $pagesToDisplay = min($maxPages, $totalPages);
                for($item = 1; $item <= $pagesToDisplay; $item++)
                {
                   $counter = $item + $offset;
                   echo '<li><a href="'.$url.'?p='.$counter.'&l='.$logLevel.'" class="'.(($counter == $currentPage)?'current':'').'">'.$counter.'</a></li>';
                }
                echo '<li><a href="'.$url.'?p='.$totalPages.'&l='.$logLevel.'" class="'.(($totalPages == $currentPage)?'last':'').'">last</a></li>';


                echo "</ul></nav>";
        }
}

?>
        <h1>Event log</h1>
      <table class="list" width="100%" style="max-width: 100%;">
              <tr>
                      <th>Date</th>
                      <th>Level</th>
                      <th>Description</th>
              </tr>
      <?php
foreach($entries as $r)
{
?>
<tr>
        <td class="nowrap" width="1%"><?php echo $r->TimeStamp; ?></td>
        <td class="nowrap <?php echo $r->MessageTypeName(); ?>" width="1%"><?php echo $r->MessageTypeName(); ?></td>
        <td class=""><?php echo nl2br(softwrap(htmlentities($r->Message), '/')) ?></td>
</tr>
<?php
}
?>
</table>

<?php
showPages($pageNum, $pages, $applicationRoot.'log', $logLevel);

include(__DIR__.'/footer.php');
?>
