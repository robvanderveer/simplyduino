<!doctype html>
<html>
<head>
        <title><?php echo isset($pageTitle)?$pageTitle:"" ?> - Sensorium</title>
        <!-- <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width"> -->
        <meta name="viewport" content="width=device-width">
        <base href="<?php echo $applicationRoot; ?>"/>

        <link rel="stylesheet" type="text/css" href="<?php echo $applicationRoot; ?>style.css" />

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $applicationRoot; ?>favicon.ico">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $applicationRoot; ?>Images/Icon_57.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $applicationRoot; ?>Images/Icon_76.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $applicationRoot; ?>Images/Icon_120.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $applicationRoot; ?>Images/Icon_152.png"/>
        <link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo $applicationRoot; ?>Images/Icon_180.png"/>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script> 
</head>

<body>
<div id="wrapper">
<div id="header">
      <a href="<?php echo $applicationRoot; ?>"><img class="logo" src="<?php echo $applicationRoot; ?>Images/Sensorium@2x.png"/></a>
      <?php
      include(__DIR__."/navigation.php");
      ?>
</div>
<div id="main">

    <script type="text/javascript">
    $(document).ready(function() {

    $(".confirmLink").click(function(e) {
        e.preventDefault();
        var theHREF = $(this).attr("href");
        var theTitle = $(this).attr("title");

        $('#popup .popovermessage').text(theTitle);
        $('#popup').addClass('visible');

        $("#popup a.confirm").click(function(e) {
            event.preventDefault();
            window.location.href = theHREF;
        });
        $("#popup a.cancel").click(function(e) {
            event.preventDefault();
            $("#popup").removeClass('visible');
        });
    });
  });
    </script>

    <div id="popup" class="popover" role="alert">
    	<div class="popover-container">
            <h1>Confirm</h1>
    		<p class="popovermessage">Are you sure you want to delete this element?</p>
    		<ul class="buttons">
    			<li><a href="#0" class="confirm button big">Ok</a></li>
    			<li><a href="#0" class="cancel button big">Cancel</a></li>
    		</ul>
    	</div> <!-- cd-popup-container -->
    </div> <!-- cd-popup -->
