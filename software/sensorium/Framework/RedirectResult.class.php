<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */
 
class RedirectResult extends ActionResult
{
        private $url;
        private $permanent;

        function __construct($url, $permanent = false)
        {
                $this->url = $url;
                $this->permanent = $permanent;
        }

        function execute()
        {
                header('Location: '.$this->url, true, $this->permanent?301:302);
                exit();
        }
}
?>
