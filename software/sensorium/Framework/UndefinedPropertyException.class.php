<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

class UndefinedPropertyException extends Exception
{
        public function __construct($propertyName)
        {
                $this->propertyName = $propertyName;
                parent::__construct("Property '$this->propertyName' is not defined and access has been blocked. If you want to use this variable you have to declare it explicitely.");

        }
};

?>
