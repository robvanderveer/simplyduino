<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

class RequestContext
{
        use SolidObject;

        public $appRoot;
        public $virtualPath;
        public $method;
        public $route;
        
        public function __construct(array $server, $uri)
        {
                $scriptRoot = dirname($server["PHP_SELF"]).'/';

                //make sure the $script ends with a '/'
                if(substr($uri, 0, strlen($scriptRoot)) != $scriptRoot)
                {
                        echo $uri;
                        throw new Exception("Unexpected virtual path");
                }

                $this->appRoot = $scriptRoot;
                $this->virtualPath = strtok(substr($uri, strlen($scriptRoot)), "?");
                $this->method = $server['REQUEST_METHOD'];

                //TODO: Verify the method.

                //TODO: Read and clean all input (querystrings or POST variables)

        }

        //this function allows to you access route parameters, GET and POST arguments.
        //TODO: Clean the inputs.
        public function getParam($name, $default = null)
        {
                $data = $this->route->getData($this->virtualPath);
                if(isset($data[$name]))
                {
                        return $data[$name];
                }

                if(isset($_GET[$name]))
                {
                        return $_GET[$name];
                }

                if(isset($_POST[$name]))
                {
                        return $_POST[$name];
                }

                if(isset($default))
                {
                        return $default;
                }
                throw new Exception("missing parameter for $name");
        }
}
