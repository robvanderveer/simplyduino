<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

//represents a single entry in the routing table.
//The route contains a URL pattern, a targetController and a optional set of parameters
//@see RoutingTable

class Route
{
        use SolidObject;

        public $pattern;
        public $targetController;
        public $defaults;

        public function __construct($pattern, $targetController, array $defaults = [])
        {
                $this->pattern = explode("/", $pattern);
                $this->targetController = $targetController;
                if(!isset($defaults['action']))
                {
                        $defaults['action'] = 'index';
                }
                $this->defaults = $defaults;
        }

        /*
         * Checks if the give uri matches the route.
         * @returns true if the given app-relative request_url matches the route
         */
        public function isMatch($uri)
        {
                //the first character cannot be a '/' for obvious reasons.
                if(substr($uri, 0, 1) == "/")
                {
                        throw new Exception("Virtual path '$uri' cannot start with a /");
                }

                //split the uri into fragments
                $uriFragments = explode('/', $uri);

                if(count($uriFragments) > count($this->pattern))
                {
                        //can't be right, we provided more segments than the pattern defines.
                        return false;
                }

                for($i = 0; $i < count($this->pattern); $i++)
                {
                        $fragment = ($i < count($uriFragments))?$uriFragments[$i]:null;
                        $shouldMatch = $this->pattern[$i];

                        if(substr($shouldMatch, 0, 1) == "{")
                        {
                                $name = substr($shouldMatch, 1, strlen($shouldMatch) - 2);
                                //it is a pattern. It must be specified or it must have a default.
                                if(!isset($fragment) || strlen($fragment) == 0)
                                {
                                        if(!isset($this->defaults[$name]))
                                        {
                                                return false;
                                        }
                                }
                                else
                                {
                                }
                        }
                        elseif($fragment != $shouldMatch)
                        {
                                return false;
                        }
                }

                //all patterns did match.
                return true;
        }

        /*
         * Returns an array of values based on the pattern of the route including the default values.
         */
        public function getData($uri)
        {
                //add the defaults if not given.
                $arguments = $this->defaults;
                $fragments = explode('/', $uri);

                //split the uri into fragments
                $uriFragments = explode('/', $uri);
                for($i = 0; $i < count($this->pattern); $i++)
                {
                        $fragment = ($i < count($uriFragments))?$uriFragments[$i]:null;
                        $shouldMatch = $this->pattern[$i];

                        if(substr($shouldMatch, 0, 1) == "{")
                        {
                                $name = substr($shouldMatch, 1, strlen($shouldMatch) - 2);

                                //it is a pattern. It must be specified or it must have a default.
                                if(!isset($fragment) || strlen($fragment) == 0)
                                {
                                        //just use the default we loaded above.
                                }
                                else
                                {
                                        //set the value.
                                        $arguments[$name] = $fragment;
                                }
                        }
                }

                return $arguments;
        }

        /*
         * Construct a virtualRoot-relative url based on the pattern and the arguments
         */
        public function makeUrl(array $values)
        {
                $path = [];
                for($i = 0; $i < count($this->pattern); $i++)
                {
                        $shouldMatch = $this->pattern[$i];
                        if(substr($shouldMatch, 0, 1) == "{")
                        {
                                $name = substr($shouldMatch, 1, strlen($shouldMatch) - 2);

                                //it is a pattern. It must be specified or it must have a default.
                                $path[] = urlencode($values[$name]);
                                unset($values[$name]);
                        }
                        else
                        {
                                //literal;
                                $path[] = $shouldMatch;
                        }
                }
                $path = implode('/',$path);
                if(count($values) > 0)
                {
                        $args = [];
                        foreach($values as $key => $value)
                        {
                                $args[] = $key.'='.urlencode($value);
                        }
                        $path .= "?".implode('&', $args);
                }

                return $path;
        }
}

?>
