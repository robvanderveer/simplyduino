<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

abstract class ControllerBase implements IController
{
        use SolidObject;

        protected $model;
        protected $context;

        public function __construct(RequestContext $context)
        {
                $this->model = new ViewData();
                $this->context = $context;
                $this->model->applicationRoot = $context->appRoot;
        }

        public function RequiresAuthentication($action)
        {
            return false;
        }

        //helper function for a controller to return the default ViewResult instance
        public function View($template)
        {
                $result = new HtmlViewResult(realpath(__DIR__.'/../Views/'.$template), $this->model);
                $result->context = $this->context;
                return $result;
        }

        //returns the url of any link with optional parameters.
        public function Link($name, array $params = [])
        {
            throw new Exception("Do not use");
                return Routing::MakeLink($name, $params);
        }

        //returns the url of an action to the current route with optional parameters.
        public function LinkSelf(array $params = [])
        {
                return $this->context->route->makeUrl($params);
        }
}
?>
