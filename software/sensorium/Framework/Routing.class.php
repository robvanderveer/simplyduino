<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

final class Routing
{
	use SolidObject;

	private static $routes = [];

    public static function init()
    {
        Routing::$routes = [];
    }

    public static function addRoute($name, Route $route)
    {
		if(isset(Routing::$routes[$name]))
		{
			throw new Exception("Route '$name' already defined.");
		}

		Routing::$routes[$name] = $route;
    }

	public static function findRoute($virtualPath)
	{
		foreach(Routing::$routes as $name => $route)
		{
				if($route->isMatch($virtualPath))
				{
						return $route;
				}
		}
		return null;
	}
    //returns the url of any link with optional parameters.
    public static function MakeLink($name, array $params = [])
    {
		if(!isset(Routing::$routes[$name]))
		{
			throw new Exception("Unknown route '$name'");
		}

        return Routing::$routes[$name]->makeUrl($params);
    }
}

?>
