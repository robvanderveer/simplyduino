<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */
 
//In a attempt to avoid coding errors, inheriting from this class will
//effectively block access to undefined properties. I know it is against
//the purity and flexibility of php, but for some classes i want this to be tight.
//feel free to drop the dependency.
trait SolidObject
{
        //this is a guard to block undefined variables.
        public function __get($name)
        {
                if(!property_exists($this, $name))
                {
                        throw new UndefinedPropertyException(get_class($this).'.'.$name);
                }
        }

        public function __set($name, $value)
        {
                throw new UndefinedPropertyException(get_class($this).'.'.$name);
        }
}

?>
