<?php

class Authentication
{
	use SolidObject;

	public static function SignOut()
	{
		unset($_SESSION["Username"]);
		session_destroy();
	}

	public static function Authenticate($username)
	{
		$_SESSION["Username"] = $username;
	}

	public static function GetUsername()
	{
		if(Authentication::IsAuthenticated())
		{
			return $_SESSION["Username"];
		}
	}

	public static function IsAuthenticated()
	{
		return isset($_SESSION["Username"]);
	}
}

?>
