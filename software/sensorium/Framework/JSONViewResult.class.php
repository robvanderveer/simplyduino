<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

class JSONViewResult extends ActionResult
{
        use SolidObject;

        private $model;
        private $statusCode;

        public function __construct($model, $status = 200)
        {
                $this->model = $model;
                $this->statusCode = $status;
        }

        //renders a template
        public function execute()
        {
                header("Access-Control-Allow-Orgin: *");
                header("Access-Control-Allow-Methods: *");
                header("Content-Type: application/json");
                header("HTTP/1.1 " . $this->statusCode . " " . $this->_requestStatus($this->statusCode));
                echo json_encode($this->model);
        }

        private function _requestStatus($code) {
            $status = array(
                200 => 'OK',
                400 => 'Bad request',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                500 => 'Internal Server Error',
            );
            return ($status[$code])?$status[$code]:$status[500];
        }
}

?>
