<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

abstract class HttpStatus
{
        const Forbidden = 403;
        const Ok = 200;
        const NotFound = 404;
}

class HttpResponseResult extends ActionResult
{
        private $errorCode;

        function __construct($errorCode)
        {
                $this->errorCode = $errorCode;
        }

        function execute()
        {
                http_response_code($this->errorCode);
                exit("<h1>{$this->errorCode} error</h1>\nUnable to serve you.");
        }
}
?>
