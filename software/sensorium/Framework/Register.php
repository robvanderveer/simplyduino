<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

//throw on all errors, including undeclared variables.
error_reporting(E_ALL);
date_default_timezone_set("GMT");   //override later

define('FRAMEWORK_DIR', __DIR__);

/* register all globals and classes required for the MVC pattern to work */
spl_autoload_register(function ($class) {
        loadIf(FRAMEWORK_DIR.'/'.$class.'.class.php');
        loadIf(__DIR__.'/../Controllers/'.$class.'.class.php');
});


function loadIf($filename)
{
        if(file_exists($filename) && is_readable($filename))
        {
                include $filename;
        }
}

?>
