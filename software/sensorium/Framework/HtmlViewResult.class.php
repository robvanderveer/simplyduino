<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

class HtmlViewResult extends ActionResult
{
        private $templateFile;
        private $model;
        public $context;

        public function __construct($templateFile, ViewData $model)
        {
                $this->templateFile = $templateFile;
                $this->model = $model;
        }

        //renders a template
        public function execute()
        {
                //explode the viewbag to create firstclass variables for the template.
                foreach($this->model->getData() as $key => $value)
                {
                        $$key = $value;
                }

                require $this->templateFile;
        }
}

?>
