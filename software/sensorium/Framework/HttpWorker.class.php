<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

//This class the main entry point for every request in the system.
//The HttpWorker inspects the request and the parameters and decides which
//controller and view to instantiate using a routing table.
//It requires an pageF.php that simply creates an instance of this class

require_once __DIR__.'/../Class/LogEntry.class.php';

class HttpWorker
{
        use SolidObject;

        private $controller;
        private $webRoot;

        //The HttpWorker assumes that your main page.php sits in the root of the web application.
        public function __construct()
        {
                $applicationRoot = dirname($_SERVER["PHP_SELF"]).'/';

                $this->webRoot = $applicationRoot;
        }

        //execute the request.
        //$script is the directory
        public function invoke($request)
        {
                //first, make sure the system is configured properly.
                if ( !isset($_SERVER['REDIRECT_HTACCESS_CONFIGURED']) )
                {
                    $data = new ViewData();
                    $data->applicationRoot = $this->webRoot;
                    $result = new HtmlViewResult("Views/CreateHtAccessView.php", $data);
                    $result->execute();

                    exit();
            	}

                if(!file_exists(__DIR__.'/../config.php'))
                {
                    $data = new ViewData();
                    $data->applicationRoot = $this->webRoot;
                    $data->contents = file_get_contents(__DIR__.'/../config-template.php');
                    $result = new HtmlViewResult("Views/CreateConfigPhpView.php", $data);
                    $result->execute();

                    exit();
                }

                //make sure the $script ends with a '/'
                if(substr($request, 0, strlen($this->webRoot)) != $this->webRoot)
                {
                        throw new Exception("Unexpected virtual path");
                }

                //remove the application root.
                $virtualPath = substr($request, strlen($this->webRoot));

                //now remove the querystring (if any)
                $virtualPath = strtok($virtualPath, "?");

                try
                {
                    $route = Routing::findRoute($virtualPath);
                    if(!$route)
                    {
                            http_response_code(404);
                            exit("<h1>404 Not Found</h1>\nUnable to serve you because a route was not found.");
                    }

                    $newContext = new RequestContext($_SERVER, $request);
                    $newContext->route = $route;

                    //get the data.
                    $parameters = $route->getData($virtualPath);
                    $controllerName = $route->targetController;

                    if(!class_exists($controllerName))
                    {
                            throw new Exception("Controller '$controllerName' does not exist");
                    }
                    $this->controller = new $controllerName($newContext);
                    if(!($this->controller instanceof IController))
                    {
                            throw new Exception("'$controllerName' is not a Controller");
                    }
                    $action = $parameters["action"];

                    //get the response from the controller.
                    if(!method_exists($this->controller, $action))
                    {
                            throw new Exception("Controller '$controllerName' does not have an action name '$action'");
                    }

                    if($this->controller->RequiresAuthentication($action))
                    {
                        if(!Authentication::IsAuthenticated())
                        {
                            $redirectUrl = $this->webRoot.Routing::MakeLink("Login", ["returnUrl" => $request]);
                            $result = new RedirectResult($redirectUrl);
                            $result->execute();
                            return;
                        }
                    }

                    $result = $this->controller->$action();
                    if(!$result instanceof ActionResult)
                    {
                        throw new Exception("'$controllerName'.'$action' did not return ActionResult object.");
                    }

                    $result->execute();
                }
                catch(Exception $e)
                {
                        //addlog(LogLevel::Error, $e->getMessage());

                        $pageTitle = "Fatal error";
                        $applicationRoot = $this->webRoot;
                        $errorDescription = $e->getMessage();
                        $stackTrace = $this->buildStackTrace(dirname(__DIR__), $e);

                        require(__DIR__.'/../Views/ErrorView.php');
                }
        }

        private function buildStackTrace($root, $e)
        {
                return nl2br($e->getTraceAsString());
        }
}

?>
