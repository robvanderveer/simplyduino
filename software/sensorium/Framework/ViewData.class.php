<?php

/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */
 
class ViewData
{
        private $data = [];

        public function __set($name, $value)
        {
                $this->data[$name] = $value;
        }

        public function __get($name)
        {
                if(isset($this->data[$name]))
                {
                        return $this->data[$name];
                }
                return null;
        }

        public function getData()
        {
                return $this->data;
        }
}

?>
