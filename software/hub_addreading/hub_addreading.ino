/*
 * UIPEthernet TcpClient example.
 *
 * UIPEthernet is a TCP/IP stack that can be used with a enc28j60 based
 * Ethernet-shield.
 *
 * UIPEthernet uses the fine uIP stack by Adam Dunkels <adam@sics.se>
 *
 *      -----------------
 *
 * This TcpClient example gets its local ip-address via dhcp and sets
 * up a tcp socket-connection to 192.168.0.1 port 5000 every 5 Seconds.
 * After sending a message it waits for a response. After receiving the
 * response the client disconnects and tries to reconnect after 5 seconds.
 *
 * Copyright (C) 2013 by Norbert Truchsess <norbert.truchsess@t-online.de>
 */

#include <UIPEthernet.h>

#include "TimedEvent.h"

//cannot blink because pin 13 is used by the SPI.
//const int PIN_BLINK = 13;

EthernetClient client;

timedEvent readEvent;

void setup() {

  Serial.begin(9600);
  Serial.println("Boot...");

  uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05};
  Ethernet.begin(mac);

  ethernetInfo();
  
  readEvent.fire();
}

void loop() {
  if (readEvent.hasElapsed())
  {
    readEvent.reset(1000);

    Serial.print("Client connect ");
    Serial.println("...");

    //get an analog reading. which is random as it gets if there is nothing connected.
    int val = analogRead(A0);

    if (client.connect(IPAddress(192, 168, 2, 3), 80))
    {
      Serial.println("Client connected");
         
      char sbuf[80];
      sprintf(sbuf, "GET /sensorium/api/v1/addReading/00000001/34/%d?apiKey=00000000 HTTP/1.1", val);

      client.println(sbuf);
      client.println("Host: depot.simplicate.local");
      client.println("User-Agent: arduino-ethernet");
      client.println("Connection: close");
      client.println();

      while (client.available() == 0)
      {
        if (readEvent.hasElapsed())
        {
          Serial.println("Timeout");
          goto close;

        }
      }

      int size;
      uint8_t msg[128];

      while ((size = client.available()) > 0)
      {
        int bufLen = (size < 128)?size:128;
        size = client.read(msg, bufLen);
        Serial.write(msg, size);
      }
close:
      //disconnect client
      Serial.println("Client disconnect");
      client.stop();

      readEvent.reset(10000);
    }
    else
    {
      //failed to connect, perhaps the DHCP lease expired?
      switch (Ethernet.maintain()) {
        case 0:   Serial.print(F("\n\rDHCP: Nothing happened")); break;
        case 1:   Serial.print(F("\n\rDHCP: Renew failed")); break;
        case 2:   Serial.print(F("\n\rDHCP: Renew success")); break;
        case 3:   Serial.print(F("\n\rDHCP: Rebind fail")); break;
        case 4:   Serial.print(F("\n\rDHCP: Rebind success")); break;
        default:  Serial.print(F("\n\rDHCP: Unexpected number")); break;
      }
      
      ethernetInfo();

      Serial.println("Client connect failed");
    }
  }
}

void ethernetInfo()
{
  Serial.print("localIP: ");
  Serial.println(Ethernet.localIP());
  Serial.print("subnetMask: ");
  Serial.println(Ethernet.subnetMask());
  Serial.print("gatewayIP: ");
  Serial.println(Ethernet.gatewayIP());
  Serial.print("dnsServerIP: ");
  Serial.println(Ethernet.dnsServerIP());
}
