/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

/* power saving includes */
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/io.h>

volatile bool watchdogActivated = true;

const unsigned long numberOfSleepsBeforeWaking = 16;    //16*8 = 128 seconds
unsigned long numberOfSleepsToGo = 0;
byte adcsra_save;

void setup()
{
  clientSetup();
    
  setupSleep();
  watchdogActivated = false;
}

void loop()
{
   bool byInterrupt = !watchdogActivated;
   if(byInterrupt || numberOfSleepsToGo == 0)
   {
     clientLoop(byInterrupt);
     numberOfSleepsToGo = numberOfSleepsBeforeWaking;
   }
   numberOfSleepsToGo--;
  

   watchdogActivated = false;
  
   sleep();
}

void setupSleep()
{
  noInterrupts();
    
  // Set the watchdog reset bit in the MCU status register to 0.
  MCUSR &= ~(1<<WDRF);
  
  // Set WDCE and WDE bits in the watchdog control register.
  WDTCSR |= (1<<WDCE) | (1<<WDE);

  // Set watchdog clock prescaler bits to a value of 8 seconds.
  WDTCSR = (1<<WDP0) | (1<<WDP3);
  
  // Enable watchdog as interrupt only (no reset).
  WDTCSR |= (1<<WDIE);
  
  interrupts();
}


// Define watchdog timer interrupt.
ISR(WDT_vect)
{
  // Set the watchdog activated flag.
  // Note that you shouldn't do much work inside an interrupt handler.
  watchdogActivated = true;
}

void pin2Interrupt(void)
{
  sleep_disable();
  detachInterrupt(0);
}

// Put the Arduino to sleep.
void sleep()
{   
  // Set sleep to full power down.  Only external interrupts or 
  // the watchdog timer can wake the CPU!
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  
  attachInterrupt(0, pin2Interrupt, LOW);
  
  adcsra_save = ADCSRA;
  
  // Turn off the ADC while asleep.
  ADCSRA = 0;  // disable ADC
  power_adc_disable();

  // Enable sleep and enter sleep mode.
  sleep_mode();

  // CPU is now asleep and program execution completely halts!
  // Once awake, execution will resume at this point.
  // <--- here.
  
  // When awake, disable sleep mode and turn on all devices.
  sleep_disable();
  power_all_enable();
  
  ADCSRA = adcsra_save;
  
  detachInterrupt(0);
}
