/*
 Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */


//sanity check
#ifndef ARDUINO_SIMPLYDUINO
#error "Please select 'Simplyduino' from the boards menu
#endif

#include <SPI.h>
#include "RF24.h"
#include "DHT.h"
#include <myRF.h>
#include <avr/eeprom.h>

const uint8_t PIN_SENSOR_1 = A0;
const uint8_t PIN_SENSOR_ENABLE = 4;
const uint8_t PIN_PUMP_1 = 3;
const uint8_t PIN_LED = 7;
const uint8_t PIN_RECONFIGURE = A3;

const uint8_t PIN_SENSOR_2 = A1;

const unsigned long long writingPipe = 0x6666666618LL;
const unsigned long long readingPipe = 0x66666666C0LL;

const uint8_t EEPROM_VALID = 0x77;
const uint8_t DEFAULT_SENDER_ID = 0x70;

typedef struct
{
  uint8_t valid;
  uint8_t nodeId;
} Configuration;

typedef struct
{
  myRFPacketHeader header;
  uint16_t value;
  uint8_t metricId;
} SensoduinoPacket;

RF24 radio(9,10);
myRFClient rfClient(&radio, 5, 150U, &Serial);
uint8_t packetNumber = 1;
Configuration settings;
DHT dht(PIN_SENSOR_2, DHT11, 3);
  
void clientSetup()
{
  Serial.begin(9600);
  Serial.println(F("\r\n\r\nSensorium Client v0.94, (C)2015 Simplicate.info"));

  loadSettings();
  
  Serial.print(F("SensorNode ID "));
  Serial.println(settings.nodeId, HEX);

  pinMode(PIN_LED, OUTPUT);
  pinMode(PIN_SENSOR_1, INPUT);
  pinMode(PIN_SENSOR_2, INPUT);
  pinMode(PIN_SENSOR_ENABLE, OUTPUT);
  pinMode(PIN_PUMP_1, OUTPUT);

  radio.begin();
  
  if(!radio.isPVariant())
  {
    Serial.print(F("ERROR: device is not a NRF24L01+ (or compatible)."));
  }
  
  //we handle the retries ourselves.
  //radio.setRetries(15,15);
  //radio.setPayloadSize(8);
  //radio.setChannel(13);
  radio.setDataRate( RF24_250KBPS ) ;
  radio.setPALevel( RF24_PA_MAX ) ;
  radio.enableDynamicPayloads() ;        //this one is important
  radio.setAutoAck( false ) ;            //so is this
  radio.openWritingPipe(writingPipe);
  radio.openReadingPipe(1, readingPipe);

  //signal wakeup.
  digitalWrite(PIN_LED, HIGH);
  delay(500);
  digitalWrite(PIN_LED, LOW);

  Serial.println(F("Sending boot packet"));

  bool ok = sendData(10, 1);
  if(!ok)
  {
    Serial.println(F("ERROR sending boot packet. Ignoring."));
  }
  Serial.println(F("Boot completed."));
  
  dht.begin();
}

void clientLoop(bool launchedByInterrupt)
{
  radio.powerUp();
  pinMode(PIN_SENSOR_ENABLE, OUTPUT);
  pinMode(PIN_SENSOR_1, INPUT);
  pinMode(PIN_SENSOR_2, INPUT);
  
  for(;;)
  {
    digitalWrite(PIN_LED, HIGH);
    digitalWrite(PIN_SENSOR_ENABLE, HIGH);
    
    //it takes 13mSec for an ADC to settle, go on the safe side without taking too much power.
    delayMicroseconds(150);
    
    //read 2 times to get a good result.
    analogRead(PIN_SENSOR_1);
    int val = analogRead(PIN_SENSOR_1);
    
    //while we were waiting for an analogRead, the dht has had enough time to do a sample.
    float humidity = dht.readHumidity();
    // Read temperature as Celsius
    float temperature = dht.readTemperature(); 
    
    digitalWrite(PIN_SENSOR_ENABLE, LOW);
    digitalWrite(PIN_LED, LOW);

    Serial.println(val);

    if(val < 580)
    {
      digitalWrite(PIN_LED, HIGH);

      //turn on the pump pulse.
      digitalWrite(PIN_PUMP_1, HIGH);
      delay(250);
      digitalWrite(PIN_PUMP_1, LOW);
      digitalWrite(PIN_LED, LOW);
    }
    else
    {
      digitalWrite(PIN_LED, LOW);
      digitalWrite(PIN_PUMP_1, LOW);

      sendData(34, val);
      sendData(35, readVcc());
      
      if(!isnan(humidity) && !isnan(temperature))
      {
        sendData(100, temperature);
        sendData(110, humidity);
      }
      
      rfClient.showStatistics();

      radio.powerDown();
      delayMicroseconds(50);

      return; //go to sleep.
    }
  }
  //will never get here.
}

bool sendData(uint8_t metric, uint16_t value)
{
      SensoduinoPacket packet;
      packet.header.targetId = 0x00;
      packet.header.senderId = settings.nodeId;
      packet.header.packetNumber = packetNumber++;
      packet.value = value;
      packet.metricId = metric;

      bool ok = rfClient.sendPacket( &packet, sizeof(SensoduinoPacket) );
      if(!ok)
      {
        bleepFail();
        Serial.print(metric);
        Serial.print(" = ");
        Serial.println(value);
        return false;
      }
      
      return true;
}

void bleep()
{

  // blink whenever you wake up, then go to sleep.
  digitalWrite(PIN_LED, HIGH);
  delay(1);
  digitalWrite(PIN_LED, LOW);
  delay(200);
  digitalWrite(PIN_LED, HIGH);
  delay(1);
  digitalWrite(PIN_LED, LOW);
}

void bleepFail()
{
  for(int i = 0; i < 5; i++)
  {
  digitalWrite(PIN_LED, HIGH);
  delay(1);
  digitalWrite(PIN_LED, LOW);
  delay(39);
  }
}

void loadSettings()
{
  pinMode(PIN_RECONFIGURE, INPUT_PULLUP);

   //load the configuration from EEPROM
  eeprom_read_block((void*)&settings, (void *)0, sizeof(settings));

  if(digitalRead(PIN_RECONFIGURE) == LOW || settings.valid != EEPROM_VALID)
  {
    Serial.println(F("Resetting ID")); 
    settings.valid = EEPROM_VALID;
    settings.nodeId = DEFAULT_SENDER_ID;
    eeprom_write_block((const void*)&settings, (void*)0, sizeof(settings));  
  }
  else
  {
    Serial.println(F("Connect A3 to GND to rewrite SensorNode ID in flash")); 
  }
}
//
//long readTempHumidity()
//{
//  Serial.println("TEMP/HUMIDITY");
//  
//  float h = dht.readHumidity();
//  // Read temperature as Celsius
//  float t = dht.readTemperature();
//  // Read temperature as Fahrenheit
//  float f = dht.readTemperature(true);
//  
//  Serial.print("Humidity: "); 
//  Serial.print(h);
//  Serial.print(" %\t");
//  Serial.print("Temperature: "); 
//  Serial.print(t);
//  Serial.print(" *C ");
//  Serial.print(f);
//  Serial.print(" *F\t");
//  
//  delay(500);
//}

long readVcc() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result; // Back-calculate AVcc in mV
  return result;
}
